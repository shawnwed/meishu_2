<GameFile>
  <PropertyGroup Name="ui_main" Type="Scene" ID="9e3c86ea-1476-458e-9840-e0977379e5c0" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Scene" Tag="168" ctype="GameNodeObjectData">
        <Size X="960.0000" Y="640.0000" />
        <Children>
          <AbstractNodeData Name="bg" ActionTag="1924982428" Tag="169" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" LeftEage="422" RightEage="422" TopEage="237" BottomEage="237" Scale9OriginX="422" Scale9OriginY="237" Scale9Width="436" Scale9Height="246" ctype="ImageViewObjectData">
            <Size X="960.0000" Y="640.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="480.0000" Y="320.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="1.0000" Y="1.0000" />
            <FileData Type="Normal" Path="bg/bg_info.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="logo" Visible="False" ActionTag="2035358455" Tag="178" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-33.5360" RightMargin="737.5360" TopMargin="-8.6160" BottomMargin="486.6160" ctype="SpriteObjectData">
            <Size X="256.0000" Y="162.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="94.4640" Y="567.6160" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0984" Y="0.8869" />
            <PreSize X="0.2667" Y="0.2531" />
            <FileData Type="Normal" Path="registersdk/logo01.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="mm_1" ActionTag="-148008685" VisibleForFrame="False" Tag="179" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="300.5000" RightMargin="300.5000" TopMargin="-43.8120" BottomMargin="-1.1880" ctype="SpriteObjectData">
            <Size X="359.0000" Y="685.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="480.0000" Y="341.3120" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5333" />
            <PreSize X="0.3740" Y="1.0703" />
            <FileData Type="Default" Path="Default/Sprite.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="btn_wechat" ActionTag="251642145" Tag="180" IconVisible="False" LeftMargin="143.2563" RightMargin="579.7437" TopMargin="488.1795" BottomMargin="48.8205" ctype="SpriteObjectData">
            <Size X="237.0000" Y="103.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="261.7563" Y="100.3205" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.2727" Y="0.1568" />
            <PreSize X="0.2469" Y="0.1609" />
            <FileData Type="Normal" Path="registersdk/DL_weixin.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="btn_mobile" ActionTag="879942925" Tag="181" IconVisible="False" LeftMargin="528.7189" RightMargin="194.2811" TopMargin="488.1795" BottomMargin="48.8205" ctype="SpriteObjectData">
            <Size X="237.0000" Y="103.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="647.2189" Y="100.3205" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.6742" Y="0.1568" />
            <PreSize X="0.2469" Y="0.1609" />
            <FileData Type="Normal" Path="registersdk/DL_shouji.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="btn_guest" ActionTag="-1049992863" Tag="182" IconVisible="False" LeftMargin="914.1813" RightMargin="-191.1813" TopMargin="488.1795" BottomMargin="48.8205" ctype="SpriteObjectData">
            <Size X="237.0000" Y="103.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1032.6813" Y="100.3205" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="1.0757" Y="0.1568" />
            <PreSize X="0.2469" Y="0.1609" />
            <FileData Type="Normal" Path="registersdk/kuaisuyouxi.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>