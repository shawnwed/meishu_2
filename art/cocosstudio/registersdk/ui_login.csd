<GameFile>
  <PropertyGroup Name="ui_login" Type="Scene" ID="20ee2dbd-05f0-4cb0-8cd7-2050593775f5" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Scene" Tag="6" ctype="GameNodeObjectData">
        <Size X="960.0000" Y="640.0000" />
        <Children>
          <AbstractNodeData Name="bg" ActionTag="-1462671596" Alpha="165" Tag="550" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" TouchEnable="True" LeftEage="422" RightEage="422" TopEage="237" BottomEage="237" Scale9OriginX="422" Scale9OriginY="237" Scale9Width="436" Scale9Height="246" ctype="ImageViewObjectData">
            <Size X="960.0000" Y="640.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="480.0000" Y="320.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="1.0000" Y="1.0000" />
            <FileData Type="Normal" Path="bg/login_bg.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="p_login" ActionTag="-322384319" Tag="65" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" LeftEage="19" RightEage="19" TopEage="19" BottomEage="19" Scale9OriginX="19" Scale9OriginY="19" Scale9Width="877" Scale9Height="567" ctype="PanelObjectData">
            <Size X="960.0000" Y="640.0000" />
            <Children>
              <AbstractNodeData Name="login_title_2" ActionTag="-597490048" Tag="66" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="401.8760" RightMargin="391.1240" TopMargin="50.2638" BottomMargin="542.7362" ctype="SpriteObjectData">
                <Size X="167.0000" Y="47.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="485.3760" Y="566.2362" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5056" Y="0.8847" />
                <PreSize X="0.1740" Y="0.0734" />
                <FileData Type="MarkedSubImage" Path="registersdk/login_title.png" Plist="registersdk/regsiter.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="uno" ActionTag="1422137383" Tag="67" IconVisible="False" LeftMargin="259.9004" RightMargin="226.0996" TopMargin="196.7100" BottomMargin="393.2900" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" LeftEage="16" RightEage="16" TopEage="16" BottomEage="16" Scale9OriginX="16" Scale9OriginY="16" Scale9Width="467" Scale9Height="30" ctype="PanelObjectData">
                <Size X="474.0000" Y="50.0000" />
                <Children>
                  <AbstractNodeData Name="Text_1" ActionTag="939392722" Tag="68" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="-114.1292" RightMargin="480.1292" TopMargin="7.0000" BottomMargin="7.0000" FontSize="36" LabelText="账号：" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="108.0000" Y="36.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="-60.1292" Y="25.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="81" G="70" B="69" />
                    <PrePosition X="-0.1269" Y="0.5000" />
                    <PreSize X="0.2278" Y="0.7200" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="input" ActionTag="-938134692" Tag="69" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" LeftMargin="4.7400" RightMargin="4.7400" TouchEnable="True" FontSize="36" IsCustomSize="True" LabelText="749022101@qq.com" PlaceHolderText="用户名/手机号" MaxLengthText="10" ctype="TextFieldObjectData">
                    <Size X="464.5200" Y="50.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="237.0000" Y="25.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="82" G="52" B="49" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="0.9800" Y="1.0000" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5004" ScaleY="0.4874" />
                <Position X="497.0900" Y="417.6600" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5178" Y="0.6526" />
                <PreSize X="0.4938" Y="0.0781" />
                <FileData Type="Normal" Path="registersdk/edt_white.png" Plist="" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="pwd" ActionTag="822634826" Tag="70" IconVisible="False" LeftMargin="259.9000" RightMargin="226.1000" TopMargin="288.5360" BottomMargin="301.4640" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" LeftEage="16" RightEage="16" TopEage="16" BottomEage="16" Scale9OriginX="16" Scale9OriginY="16" Scale9Width="467" Scale9Height="30" ctype="PanelObjectData">
                <Size X="474.0000" Y="50.0000" />
                <Children>
                  <AbstractNodeData Name="Text_1" ActionTag="756352569" Tag="71" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="-114.1290" RightMargin="480.1290" TopMargin="7.0000" BottomMargin="7.0000" FontSize="36" LabelText="密码：" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="108.0000" Y="36.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="-60.1290" Y="25.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="81" G="70" B="69" />
                    <PrePosition X="-0.1269" Y="0.5000" />
                    <PreSize X="0.2278" Y="0.7200" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="input" ActionTag="-188005196" Tag="72" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" LeftMargin="4.7400" RightMargin="4.7400" TouchEnable="True" FontSize="36" IsCustomSize="True" LabelText="123456" PlaceHolderText="请输入密码" MaxLengthEnable="True" MaxLengthText="12" PasswordEnable="True" ctype="TextFieldObjectData">
                    <Size X="464.5200" Y="50.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="237.0000" Y="25.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="82" G="52" B="49" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="0.9800" Y="1.0000" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="496.9000" Y="326.4640" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5176" Y="0.5101" />
                <PreSize X="0.4938" Y="0.0781" />
                <FileData Type="Normal" Path="registersdk/edt_white.png" Plist="" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="login" ActionTag="667650282" Tag="78" IconVisible="False" LeftMargin="535.3600" RightMargin="149.6400" TopMargin="470.8800" BottomMargin="74.1200" LeftEage="16" RightEage="16" TopEage="13" BottomEage="13" Scale9OriginX="16" Scale9OriginY="13" Scale9Width="243" Scale9Height="69" ctype="ImageViewObjectData">
                <Size X="275.0000" Y="95.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="672.8600" Y="121.6200" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7009" Y="0.1900" />
                <PreSize X="0.2865" Y="0.1484" />
                <FileData Type="Normal" Path="registersdk/denglu.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="register" ActionTag="125513458" Tag="265" IconVisible="False" LeftMargin="155.6706" RightMargin="530.3294" TopMargin="470.8836" BottomMargin="74.1164" ctype="SpriteObjectData">
                <Size X="274.0000" Y="95.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="292.6706" Y="121.6164" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3049" Y="0.1900" />
                <PreSize X="0.2854" Y="0.1484" />
                <FileData Type="Normal" Path="registersdk/DL_yiyou2.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="is_remember" ActionTag="-1141703043" Tag="276" IconVisible="False" LeftMargin="243.9043" RightMargin="678.0957" TopMargin="380.1209" BottomMargin="221.8791" TouchEnable="True" CheckedState="True" ctype="CheckBoxObjectData">
                <Size X="38.0000" Y="38.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="262.9043" Y="240.8791" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2739" Y="0.3764" />
                <PreSize X="0.0396" Y="0.0594" />
                <NormalBackFileData Type="Normal" Path="registersdk/gou1.png" Plist="" />
                <PressedBackFileData Type="Normal" Path="registersdk/gou1.png" Plist="" />
                <DisableBackFileData Type="Default" Path="Default/CheckBox_Disable.png" Plist="" />
                <NodeNormalFileData Type="Normal" Path="registersdk/gou.png" Plist="" />
                <NodeDisableFileData Type="Default" Path="Default/CheckBoxNode_Disable.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_39" ActionTag="2071410559" Tag="277" IconVisible="False" LeftMargin="291.7389" RightMargin="548.2611" TopMargin="384.7733" BottomMargin="225.2267" FontSize="30" LabelText="记住密码" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="120.0000" Y="30.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="351.7389" Y="240.2267" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="147" G="77" B="65" />
                <PrePosition X="0.3664" Y="0.3754" />
                <PreSize X="0.1250" Y="0.0469" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="close" ActionTag="1955523826" Tag="681" IconVisible="False" LeftMargin="879.6279" RightMargin="0.3721" TopMargin="24.0695" BottomMargin="534.9305" ctype="SpriteObjectData">
                <Size X="80.0000" Y="81.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="919.6279" Y="575.4305" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.9579" Y="0.8991" />
                <PreSize X="0.0833" Y="0.1266" />
                <FileData Type="Normal" Path="registersdk/x.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="forget" ActionTag="-329038316" Tag="292" IconVisible="False" LeftMargin="570.0425" RightMargin="239.9575" TopMargin="384.7733" BottomMargin="225.2267" FontSize="30" LabelText="忘记密码？" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="150.0000" Y="30.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="645.0425" Y="240.2267" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="147" G="77" B="65" />
                <PrePosition X="0.6719" Y="0.3754" />
                <PreSize X="0.1563" Y="0.0469" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="480.0000" Y="320.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="1.0000" Y="1.0000" />
            <FileData Type="Normal" Path="registersdk/img_bg.png" Plist="" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="logo" ActionTag="-716213357" VisibleForFrame="False" Tag="117" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-33.5360" RightMargin="737.5360" TopMargin="-8.6160" BottomMargin="486.6160" ctype="SpriteObjectData">
            <Size X="256.0000" Y="162.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="94.4640" Y="567.6160" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0984" Y="0.8869" />
            <PreSize X="0.2667" Y="0.2531" />
            <FileData Type="Normal" Path="registersdk/logo01.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>