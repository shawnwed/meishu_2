<GameFile>
  <PropertyGroup Name="test" Type="Scene" ID="85cb2d09-027f-4bf9-a8a4-61d7df034050" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Scene" Tag="6" ctype="GameNodeObjectData">
        <Size X="1440.0000" Y="720.0000" />
        <Children>
          <AbstractNodeData Name="p_login" ActionTag="-322384319" Tag="65" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="273.7411" RightMargin="276.2589" TopMargin="78.7411" BottomMargin="81.2589" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Enable="True" LeftEage="19" RightEage="19" TopEage="19" BottomEage="19" Scale9OriginX="19" Scale9OriginY="19" Scale9Width="22" Scale9Height="22" ctype="PanelObjectData">
            <Size X="730.0000" Y="560.0000" />
            <Children>
              <AbstractNodeData Name="uno0" ActionTag="1422137383" Tag="67" IconVisible="False" LeftMargin="93.6552" RightMargin="96.3447" TopMargin="15.4241" BottomMargin="459.5759" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Enable="True" LeftEage="16" RightEage="16" TopEage="16" BottomEage="16" Scale9OriginX="16" Scale9OriginY="16" Scale9Width="18" Scale9Height="18" ctype="PanelObjectData">
                <Size X="540.0000" Y="85.0000" />
                <Children>
                  <AbstractNodeData Name="Text_1" ActionTag="939392722" Tag="68" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="12.9999" RightMargin="443.0001" TopMargin="28.5000" BottomMargin="28.5000" FontSize="28" LabelText="账号：" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="84.0000" Y="28.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="54.9999" Y="42.5000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="0" G="0" B="0" />
                    <PrePosition X="0.1019" Y="0.5000" />
                    <PreSize X="0.1556" Y="0.3294" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="input" ActionTag="-938134692" Tag="69" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="114.0000" RightMargin="6.0000" TopMargin="24.0025" BottomMargin="27.9975" FontSize="28" IsCustomSize="True" LabelText="749022101@qq.com" PlaceHolderText="" MaxLengthText="10" ctype="TextFieldObjectData">
                    <Size X="420.0000" Y="33.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="324.0000" Y="44.4975" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="0" G="0" B="0" />
                    <PrePosition X="0.6000" Y="0.5235" />
                    <PreSize X="0.7778" Y="0.3882" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint />
                <Position X="93.6552" Y="459.5759" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1283" Y="0.8207" />
                <PreSize X="0.7397" Y="0.1518" />
                <FileData Type="Normal" Path="registersdk/edt_white.png" Plist="" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="uno1" ActionTag="-1323921696" Tag="327" IconVisible="False" LeftMargin="96.8409" RightMargin="93.1592" TopMargin="111.0032" BottomMargin="363.9968" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Enable="True" LeftEage="16" RightEage="16" TopEage="16" BottomEage="16" Scale9OriginX="16" Scale9OriginY="16" Scale9Width="18" Scale9Height="18" ctype="PanelObjectData">
                <Size X="540.0000" Y="85.0000" />
                <Children>
                  <AbstractNodeData Name="Text_1" ActionTag="1311922214" Tag="328" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="12.9999" RightMargin="443.0001" TopMargin="28.5000" BottomMargin="28.5000" FontSize="28" LabelText="账号：" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="84.0000" Y="28.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="54.9999" Y="42.5000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="0" G="0" B="0" />
                    <PrePosition X="0.1019" Y="0.5000" />
                    <PreSize X="0.1556" Y="0.3294" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="input" ActionTag="-1866458257" Tag="329" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="114.0000" RightMargin="6.0000" TopMargin="24.0025" BottomMargin="27.9975" FontSize="28" IsCustomSize="True" LabelText="18576445307" PlaceHolderText="" MaxLengthText="10" ctype="TextFieldObjectData">
                    <Size X="420.0000" Y="33.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="324.0000" Y="44.4975" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="0" G="0" B="0" />
                    <PrePosition X="0.6000" Y="0.5235" />
                    <PreSize X="0.7778" Y="0.3882" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint />
                <Position X="96.8409" Y="363.9968" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1327" Y="0.6500" />
                <PreSize X="0.7397" Y="0.1518" />
                <FileData Type="Normal" Path="registersdk/edt_white.png" Plist="" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="uno2" ActionTag="-2105298943" Tag="330" IconVisible="False" LeftMargin="99.2855" RightMargin="90.7145" TopMargin="204.9067" BottomMargin="270.0933" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Enable="True" LeftEage="16" RightEage="16" TopEage="16" BottomEage="16" Scale9OriginX="16" Scale9OriginY="16" Scale9Width="18" Scale9Height="18" ctype="PanelObjectData">
                <Size X="540.0000" Y="85.0000" />
                <Children>
                  <AbstractNodeData Name="Text_1" ActionTag="1852840487" Tag="331" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="12.9999" RightMargin="443.0001" TopMargin="28.5000" BottomMargin="28.5000" FontSize="28" LabelText="账号：" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="84.0000" Y="28.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="54.9999" Y="42.5000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="0" G="0" B="0" />
                    <PrePosition X="0.1019" Y="0.5000" />
                    <PreSize X="0.1556" Y="0.3294" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="input" ActionTag="41070243" Tag="332" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="114.0000" RightMargin="6.0000" TopMargin="24.0025" BottomMargin="27.9975" FontSize="28" IsCustomSize="True" LabelText="1" PlaceHolderText="" MaxLengthText="10" ctype="TextFieldObjectData">
                    <Size X="420.0000" Y="33.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="324.0000" Y="44.4975" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="0" G="0" B="0" />
                    <PrePosition X="0.6000" Y="0.5235" />
                    <PreSize X="0.7778" Y="0.3882" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint />
                <Position X="99.2855" Y="270.0933" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1360" Y="0.4823" />
                <PreSize X="0.7397" Y="0.1518" />
                <FileData Type="Normal" Path="registersdk/edt_white.png" Plist="" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="uno3" ActionTag="726556598" Tag="333" IconVisible="False" LeftMargin="98.0990" RightMargin="91.9010" TopMargin="307.0478" BottomMargin="167.9522" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Enable="True" LeftEage="16" RightEage="16" TopEage="16" BottomEage="16" Scale9OriginX="16" Scale9OriginY="16" Scale9Width="18" Scale9Height="18" ctype="PanelObjectData">
                <Size X="540.0000" Y="85.0000" />
                <Children>
                  <AbstractNodeData Name="Text_1" ActionTag="54980050" Tag="334" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="12.9999" RightMargin="443.0001" TopMargin="28.5000" BottomMargin="28.5000" FontSize="28" LabelText="账号：" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="84.0000" Y="28.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="54.9999" Y="42.5000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="0" G="0" B="0" />
                    <PrePosition X="0.1019" Y="0.5000" />
                    <PreSize X="0.1556" Y="0.3294" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="input" ActionTag="-757129239" Tag="335" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="114.0000" RightMargin="6.0000" TopMargin="24.0025" BottomMargin="27.9975" FontSize="28" IsCustomSize="True" LabelText="2" PlaceHolderText="" MaxLengthText="10" ctype="TextFieldObjectData">
                    <Size X="420.0000" Y="33.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="324.0000" Y="44.4975" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="0" G="0" B="0" />
                    <PrePosition X="0.6000" Y="0.5235" />
                    <PreSize X="0.7778" Y="0.3882" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint />
                <Position X="98.0990" Y="167.9522" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1344" Y="0.2999" />
                <PreSize X="0.7397" Y="0.1518" />
                <FileData Type="Normal" Path="registersdk/edt_white.png" Plist="" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="uno4" ActionTag="320845178" Tag="336" IconVisible="False" LeftMargin="100.6181" RightMargin="89.3820" TopMargin="405.2461" BottomMargin="69.7539" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Enable="True" LeftEage="16" RightEage="16" TopEage="16" BottomEage="16" Scale9OriginX="16" Scale9OriginY="16" Scale9Width="18" Scale9Height="18" ctype="PanelObjectData">
                <Size X="540.0000" Y="85.0000" />
                <Children>
                  <AbstractNodeData Name="Text_1" ActionTag="-1750110818" Tag="337" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="12.9999" RightMargin="443.0001" TopMargin="28.5000" BottomMargin="28.5000" FontSize="28" LabelText="账号：" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="84.0000" Y="28.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="54.9999" Y="42.5000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="0" G="0" B="0" />
                    <PrePosition X="0.1019" Y="0.5000" />
                    <PreSize X="0.1556" Y="0.3294" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="input" ActionTag="1875554004" Tag="338" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="114.0000" RightMargin="6.0000" TopMargin="24.0025" BottomMargin="27.9975" FontSize="28" IsCustomSize="True" LabelText="3" PlaceHolderText="" MaxLengthText="10" ctype="TextFieldObjectData">
                    <Size X="420.0000" Y="33.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="324.0000" Y="44.4975" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="0" G="0" B="0" />
                    <PrePosition X="0.6000" Y="0.5235" />
                    <PreSize X="0.7778" Y="0.3882" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint />
                <Position X="100.6181" Y="69.7539" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1378" Y="0.1246" />
                <PreSize X="0.7397" Y="0.1518" />
                <FileData Type="Normal" Path="registersdk/edt_white.png" Plist="" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="tip" ActionTag="1291084660" Tag="339" IconVisible="False" LeftMargin="109.3368" RightMargin="490.6632" TopMargin="504.8816" BottomMargin="29.1184" FontSize="26" LabelText="Text Label" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="130.0000" Y="26.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="109.3368" Y="42.1184" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="0" B="0" />
                <PrePosition X="0.1498" Y="0.0752" />
                <PreSize X="0.1781" Y="0.0464" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="638.7411" Y="361.2589" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4990" Y="0.5017" />
            <PreSize X="0.5703" Y="0.7778" />
            <FileData Type="Normal" Path="registersdk/img_bg.png" Plist="" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>