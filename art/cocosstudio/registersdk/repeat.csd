<GameFile>
  <PropertyGroup Name="repeat" Type="Layer" ID="5a0c6aa5-6e73-48fd-adcc-2607ec63287a" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="content" Tag="267" ctype="GameLayerObjectData">
        <Size X="181.0000" Y="54.0000" />
        <Children>
          <AbstractNodeData Name="repeat" ActionTag="-524876258" VisibleForFrame="False" Tag="1190" IconVisible="False" PositionPercentYEnabled="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" LeftEage="48" RightEage="48" TopEage="19" BottomEage="19" Scale9OriginX="48" Scale9OriginY="19" Scale9Width="85" Scale9Height="16" ctype="PanelObjectData">
            <Size X="181.0000" Y="54.0000" />
            <Children>
              <AbstractNodeData Name="Image_1" ActionTag="166542173" Tag="273" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" Scale9Enable="True" LeftEage="13" RightEage="16" TopEage="13" BottomEage="16" Scale9OriginX="13" Scale9OriginY="13" Scale9Width="152" Scale9Height="25" ctype="ImageViewObjectData">
                <Size X="181.0000" Y="54.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="90.5000" Y="27.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5000" />
                <PreSize X="1.0000" Y="1.0000" />
                <FileData Type="Normal" Path="registersdk/kuang.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_3" ActionTag="-942568648" Tag="270" IconVisible="False" LeftMargin="33.2826" RightMargin="47.7174" TopMargin="30.9196" BottomMargin="3.0804" FontSize="20" LabelText="发送验证码" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="100.0000" Y="20.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="83.2826" Y="13.0804" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4601" Y="0.2422" />
                <PreSize X="0.5525" Y="0.3704" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_2" ActionTag="180680106" Tag="271" IconVisible="False" LeftMargin="55.6348" RightMargin="25.3652" TopMargin="4.7276" BottomMargin="29.2724" FontSize="20" LabelText="秒后可重新" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="100.0000" Y="20.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="105.6348" Y="39.2724" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5836" Y="0.7273" />
                <PreSize X="0.5525" Y="0.3704" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="txt" ActionTag="-538201862" Tag="272" IconVisible="False" LeftMargin="27.2399" RightMargin="133.7601" TopMargin="2.0708" BottomMargin="31.9292" FontSize="20" LabelText="60" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="20.0000" Y="20.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="37.2399" Y="41.9292" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="0" B="0" />
                <PrePosition X="0.2057" Y="0.7765" />
                <PreSize X="0.1105" Y="0.3704" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleY="0.5000" />
            <Position Y="27.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition Y="0.5000" />
            <PreSize X="1.0000" Y="1.0000" />
            <FileData Type="Normal" Path="registersdk/kuang.png" Plist="" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="getVerify" ActionTag="-157624182" Tag="274" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftEage="16" RightEage="16" TopEage="13" BottomEage="13" Scale9OriginX="16" Scale9OriginY="13" Scale9Width="149" Scale9Height="28" ctype="ImageViewObjectData">
            <Size X="181.0000" Y="54.0000" />
            <Children>
              <AbstractNodeData Name="txt" ActionTag="-1932418098" VisibleForFrame="False" Tag="275" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="20.5000" RightMargin="20.5000" TopMargin="13.0000" BottomMargin="13.0000" FontSize="28" LabelText="发送验证码" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="140.0000" Y="28.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="90.5000" Y="27.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5000" />
                <PreSize X="0.7735" Y="0.5185" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="txtInfo" ActionTag="-1000396215" Tag="321" IconVisible="False" LeftMargin="13.7928" RightMargin="17.2072" TopMargin="11.3447" BottomMargin="12.6553" FontSize="30" LabelText="发送验证码" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="150.0000" Y="30.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="88.7928" Y="27.6553" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4906" Y="0.5121" />
                <PreSize X="0.8287" Y="0.5556" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleY="0.5000" />
            <Position Y="27.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition Y="0.5000" />
            <PreSize X="1.0000" Y="1.0000" />
            <FileData Type="Normal" Path="registersdk/kuang.png" Plist="" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>