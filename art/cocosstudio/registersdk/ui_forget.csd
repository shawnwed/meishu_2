<GameFile>
  <PropertyGroup Name="ui_forget" Type="Scene" ID="b02bfe97-8f54-448f-8988-6e934b009199" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Scene" Tag="69" ctype="GameNodeObjectData">
        <Size X="960.0000" Y="640.0000" />
        <Children>
          <AbstractNodeData Name="mask" ActionTag="372473664" Alpha="231" Tag="825" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" TouchEnable="True" ClipAble="False" BackColorAlpha="179" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="960.0000" Y="640.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="480.0000" Y="320.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="0" G="0" B="0" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="1.0000" Y="1.0000" />
            <SingleColor A="255" R="0" G="0" B="0" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="bg" ActionTag="-563471515" Tag="777" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="22.5000" RightMargin="22.5000" TopMargin="32.0000" BottomMargin="32.0000" LeftEage="49" RightEage="49" TopEage="68" BottomEage="68" Scale9OriginX="49" Scale9OriginY="68" Scale9Width="817" Scale9Height="440" ctype="ImageViewObjectData">
            <Size X="915.0000" Y="576.0000" />
            <Children>
              <AbstractNodeData Name="closeBtn" ActionTag="-133926108" Tag="778" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="842.2430" RightMargin="-7.2430" TopMargin="-10.5480" BottomMargin="505.5480" ctype="SpriteObjectData">
                <Size X="80.0000" Y="81.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="882.2430" Y="546.0480" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.9642" Y="0.9480" />
                <PreSize X="0.0874" Y="0.1406" />
                <FileData Type="Normal" Path="registersdk/x.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="title" ActionTag="-2144694110" Tag="779" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="377.6110" RightMargin="371.3890" TopMargin="21.5432" BottomMargin="507.4568" ctype="SpriteObjectData">
                <Size X="166.0000" Y="47.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="460.6110" Y="530.9568" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5034" Y="0.9218" />
                <PreSize X="0.1814" Y="0.0816" />
                <FileData Type="Normal" Path="hall/res/info/find_pwd_title.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="480.0000" Y="320.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="0.9531" Y="0.9000" />
            <FileData Type="Normal" Path="hall/res/bingphone/bg.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="uno" ActionTag="2033937186" Tag="780" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="219.9400" RightMargin="241.0600" TopMargin="162.3440" BottomMargin="415.6560" LeftEage="33" RightEage="33" TopEage="18" BottomEage="18" Scale9OriginX="33" Scale9OriginY="18" Scale9Width="433" Scale9Height="26" ctype="ImageViewObjectData">
            <Size X="499.0000" Y="62.0000" />
            <Children>
              <AbstractNodeData Name="input" ActionTag="777640635" Tag="781" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="7.4142" RightMargin="31.5658" TopMargin="6.0248" BottomMargin="5.9752" TouchEnable="True" FontSize="36" IsCustomSize="True" LabelText="" PlaceHolderText="手机号码" MaxLengthText="10" ctype="TextFieldObjectData">
                <Size X="460.0200" Y="50.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="237.4242" Y="30.9752" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="82" G="52" B="49" />
                <PrePosition X="0.4758" Y="0.4996" />
                <PreSize X="0.9219" Y="0.8065" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="469.4400" Y="446.6560" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4890" Y="0.6979" />
            <PreSize X="0.5198" Y="0.0969" />
            <FileData Type="Normal" Path="registersdk/edt_white.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="verify" ActionTag="-17241155" Tag="783" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="219.9356" RightMargin="442.0644" TopMargin="242.6156" BottomMargin="335.3844" Scale9Enable="True" LeftEage="110" RightEage="135" TopEage="18" BottomEage="18" Scale9OriginX="110" Scale9OriginY="18" Scale9Width="254" Scale9Height="26" ctype="ImageViewObjectData">
            <Size X="298.0000" Y="62.0000" />
            <Children>
              <AbstractNodeData Name="input" ActionTag="-588476913" Tag="784" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="7.4402" RightMargin="1.5398" TopMargin="6.0000" BottomMargin="6.0000" TouchEnable="True" FontSize="36" IsCustomSize="True" LabelText="" PlaceHolderText="验证码" MaxLengthText="10" ctype="TextFieldObjectData">
                <Size X="289.0200" Y="50.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="151.9502" Y="31.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="82" G="52" B="49" />
                <PrePosition X="0.5099" Y="0.5000" />
                <PreSize X="0.9699" Y="0.8065" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.8352" ScaleY="0.4660" />
            <Position X="468.8125" Y="364.2758" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4883" Y="0.5692" />
            <PreSize X="0.3104" Y="0.0969" />
            <FileData Type="Normal" Path="registersdk/edt_white.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="confirmBtn" ActionTag="-239548392" Tag="785" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="329.2520" RightMargin="355.7480" TopMargin="477.8760" BottomMargin="67.1240" ctype="SpriteObjectData">
            <Size X="275.0000" Y="95.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="466.7520" Y="114.6240" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4862" Y="0.1791" />
            <PreSize X="0.2865" Y="0.1484" />
            <FileData Type="Normal" Path="registersdk/ok.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="getVerify" ActionTag="-2018018494" Tag="805" IconVisible="True" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="532.8960" RightMargin="246.1040" TopMargin="248.4000" BottomMargin="337.6000" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="181.0000" Y="54.0000" />
            <AnchorPoint />
            <Position X="532.8960" Y="337.6000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5551" Y="0.5275" />
            <PreSize X="0.1885" Y="0.0844" />
            <FileData Type="Normal" Path="registersdk/repeat.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="tip" ActionTag="1708946615" Tag="826" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="270.0480" RightMargin="689.9520" TopMargin="462.4640" BottomMargin="177.5360" FontSize="20" LabelText="" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="270.0480" Y="177.5360" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="0" B="0" />
            <PrePosition X="0.2813" Y="0.2774" />
            <PreSize X="0.0000" Y="0.0000" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="pwd1" ActionTag="-419172525" Tag="876" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="219.9400" RightMargin="241.0600" TopMargin="322.8560" BottomMargin="255.1440" LeftEage="33" RightEage="33" TopEage="18" BottomEage="18" Scale9OriginX="33" Scale9OriginY="18" Scale9Width="433" Scale9Height="26" ctype="ImageViewObjectData">
            <Size X="499.0000" Y="62.0000" />
            <Children>
              <AbstractNodeData Name="input" ActionTag="612175803" Tag="877" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="7.4511" RightMargin="18.5289" TopMargin="6.0000" BottomMargin="6.0000" TouchEnable="True" FontSize="36" IsCustomSize="True" LabelText="" PlaceHolderText="设置新密码(6~20个字符)" MaxLengthText="10" PasswordEnable="True" ctype="TextFieldObjectData">
                <Size X="473.0200" Y="50.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="243.9611" Y="31.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="82" G="52" B="49" />
                <PrePosition X="0.4889" Y="0.5000" />
                <PreSize X="0.9479" Y="0.8065" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="469.4400" Y="286.1440" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4890" Y="0.4471" />
            <PreSize X="0.5198" Y="0.0969" />
            <FileData Type="Normal" Path="registersdk/edt_white.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="pwd2" ActionTag="210965210" Tag="878" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="219.9400" RightMargin="241.0600" TopMargin="403.1120" BottomMargin="174.8880" LeftEage="33" RightEage="33" TopEage="18" BottomEage="18" Scale9OriginX="33" Scale9OriginY="18" Scale9Width="433" Scale9Height="26" ctype="ImageViewObjectData">
            <Size X="499.0000" Y="62.0000" />
            <Children>
              <AbstractNodeData Name="input" ActionTag="1864149805" Tag="879" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="7.4511" RightMargin="18.5289" TopMargin="6.0000" BottomMargin="6.0000" TouchEnable="True" FontSize="36" IsCustomSize="True" LabelText="" PlaceHolderText="确认新密码" MaxLengthText="10" PasswordEnable="True" ctype="TextFieldObjectData">
                <Size X="473.0200" Y="50.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="243.9611" Y="31.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="82" G="52" B="49" />
                <PrePosition X="0.4889" Y="0.5000" />
                <PreSize X="0.9479" Y="0.8065" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="469.4400" Y="205.8880" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4890" Y="0.3217" />
            <PreSize X="0.5198" Y="0.0969" />
            <FileData Type="Normal" Path="registersdk/edt_white.png" Plist="" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>