<GameFile>
  <PropertyGroup Name="ui_register" Type="Scene" ID="888b3b94-7d31-475e-85d7-923c4a35c8df" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Scene" Tag="152" ctype="GameNodeObjectData">
        <Size X="960.0000" Y="640.0000" />
        <Children>
          <AbstractNodeData Name="Panel_bg" ActionTag="1956344757" Tag="551" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" TouchEnable="True" ClipAble="False" BackColorAlpha="153" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="960.0000" Y="640.0000" />
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="1.0000" Y="1.0000" />
            <SingleColor A="255" R="0" G="0" B="0" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="mm_1" ActionTag="-1009484917" VisibleForFrame="False" Tag="91" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="81.0440" RightMargin="519.9560" TopMargin="-45.0000" ctype="SpriteObjectData">
            <Size X="359.0000" Y="685.0000" />
            <AnchorPoint ScaleX="0.5000" />
            <Position X="260.5440" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.2714" />
            <PreSize X="0.3740" Y="1.0703" />
            <FileData Type="Default" Path="Default/Sprite.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="p_register" ActionTag="-1692223393" Tag="153" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" LeftEage="19" RightEage="19" TopEage="205" BottomEage="230" Scale9OriginX="19" Scale9OriginY="205" Scale9Width="877" Scale9Height="141" ctype="PanelObjectData">
            <Size X="960.0000" Y="640.0000" />
            <Children>
              <AbstractNodeData Name="register_title" ActionTag="-1697151318" Tag="154" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="395.4440" RightMargin="397.5560" TopMargin="58.4903" BottomMargin="534.5097" ctype="SpriteObjectData">
                <Size X="167.0000" Y="47.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="478.9440" Y="558.0097" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4989" Y="0.8719" />
                <PreSize X="0.1740" Y="0.0734" />
                <FileData Type="MarkedSubImage" Path="registersdk/mobile_title.png" Plist="registersdk/regsiter.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="uno" ActionTag="-746293537" Tag="155" IconVisible="False" LeftMargin="301.1550" RightMargin="258.8450" TopMargin="177.7734" BottomMargin="412.2266" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" LeftEage="16" RightEage="16" TopEage="16" BottomEage="16" Scale9OriginX="16" Scale9OriginY="16" Scale9Width="467" Scale9Height="30" ctype="PanelObjectData">
                <Size X="400.0000" Y="50.0000" />
                <Children>
                  <AbstractNodeData Name="txt" ActionTag="-2058892899" Tag="156" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="-190.0020" RightMargin="464.0020" TopMargin="7.0000" BottomMargin="7.0000" FontSize="36" LabelText="手机号:" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="126.0000" Y="36.0000" />
                    <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
                    <Position X="-64.0020" Y="25.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="81" G="70" B="69" />
                    <PrePosition X="-0.1600" Y="0.5000" />
                    <PreSize X="0.3150" Y="0.7200" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="input" ActionTag="-1910425206" Tag="157" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" LeftMargin="-21.0000" RightMargin="-19.0000" TopMargin="0.7250" BottomMargin="-0.7250" TouchEnable="True" FontSize="32" IsCustomSize="True" LabelText="" PlaceHolderText="请输入手机号" MaxLengthText="10" ctype="TextFieldObjectData">
                    <Size X="440.0000" Y="50.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="199.0000" Y="24.2750" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="82" G="52" B="49" />
                    <PrePosition X="0.4975" Y="0.4855" />
                    <PreSize X="1.1000" Y="1.0000" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint />
                <Position X="301.1550" Y="412.2266" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3137" Y="0.6441" />
                <PreSize X="0.4167" Y="0.0781" />
                <FileData Type="Normal" Path="registersdk/edt_white.png" Plist="" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="verify" ActionTag="-1550908475" Tag="158" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="251.1440" RightMargin="461.8560" TopMargin="366.0560" BottomMargin="211.9440" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Enable="True" LeftEage="165" RightEage="195" TopEage="24" BottomEage="26" Scale9OriginX="165" Scale9OriginY="24" Scale9Width="139" Scale9Height="12" ctype="PanelObjectData">
                <Size X="247.0000" Y="62.0000" />
                <Children>
                  <AbstractNodeData Name="Text_1" ActionTag="1957352361" Tag="159" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="-139.9910" RightMargin="260.9910" TopMargin="12.5226" BottomMargin="13.4774" FontSize="36" LabelText="验证码:" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="126.0000" Y="36.0000" />
                    <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
                    <Position X="-13.9910" Y="31.4774" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="81" G="70" B="69" />
                    <PrePosition X="-0.0566" Y="0.5077" />
                    <PreSize X="0.5101" Y="0.5806" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="input" ActionTag="1263875788" Tag="160" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" LeftMargin="7.8793" RightMargin="2.0007" TopMargin="4.8918" BottomMargin="-4.8918" TouchEnable="True" FontSize="32" IsCustomSize="True" LabelText="" PlaceHolderText="请输入验证码" MaxLengthEnable="True" MaxLengthText="6" ctype="TextFieldObjectData">
                    <Size X="237.1200" Y="62.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="126.4393" Y="26.1082" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="82" G="52" B="49" />
                    <PrePosition X="0.5119" Y="0.4211" />
                    <PreSize X="0.9600" Y="1.0000" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
                <Position X="498.1440" Y="242.9440" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5189" Y="0.3796" />
                <PreSize X="0.2573" Y="0.0969" />
                <FileData Type="Normal" Path="registersdk/edt_white.png" Plist="" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="register" ActionTag="1765205988" Tag="164" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="331.4600" RightMargin="353.5400" TopMargin="465.9747" BottomMargin="79.0253" Scale9Enable="True" LeftEage="16" RightEage="16" TopEage="13" BottomEage="13" Scale9OriginX="16" Scale9OriginY="13" Scale9Width="243" Scale9Height="69" ctype="ImageViewObjectData">
                <Size X="275.0000" Y="95.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="468.9600" Y="126.5253" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4885" Y="0.1977" />
                <PreSize X="0.2865" Y="0.1484" />
                <FileData Type="Normal" Path="registersdk/zhuceanniu.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="logon" ActionTag="-1307626517" VisibleForFrame="False" Tag="127" IconVisible="False" LeftMargin="93.4183" RightMargin="639.5817" TopMargin="700.8924" BottomMargin="-134.8924" LeftEage="74" RightEage="74" TopEage="24" BottomEage="24" Scale9OriginX="74" Scale9OriginY="24" Scale9Width="79" Scale9Height="26" ctype="ImageViewObjectData">
                <Size X="227.0000" Y="74.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="206.9183" Y="-97.8924" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2155" Y="-0.1530" />
                <PreSize X="0.2365" Y="0.1156" />
                <FileData Type="Normal" Path="registersdk/DL_yiyou.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="pwd" ActionTag="1107385146" Tag="171" IconVisible="False" LeftMargin="301.1550" RightMargin="258.8450" TopMargin="274.4088" BottomMargin="315.5912" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" LeftEage="16" RightEage="16" TopEage="16" BottomEage="16" Scale9OriginX="16" Scale9OriginY="16" Scale9Width="467" Scale9Height="30" ctype="PanelObjectData">
                <Size X="400.0000" Y="50.0000" />
                <Children>
                  <AbstractNodeData Name="Text_1" ActionTag="1285121010" Tag="172" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="-226.0020" RightMargin="464.0020" TopMargin="8.0000" BottomMargin="6.0000" FontSize="36" LabelText="登录密码:" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="162.0000" Y="36.0000" />
                    <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
                    <Position X="-64.0020" Y="24.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="81" G="70" B="69" />
                    <PrePosition X="-0.1600" Y="0.4800" />
                    <PreSize X="0.4050" Y="0.7200" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="input" ActionTag="-1520247920" Tag="173" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" LeftMargin="-39.0000" RightMargin="-45.0000" TopMargin="1.0000" BottomMargin="-1.0000" TouchEnable="True" FontSize="32" IsCustomSize="True" LabelText="" PlaceHolderText="请输入(6-20位字母或数字)密码" MaxLengthEnable="True" MaxLengthText="12" PasswordEnable="True" ctype="TextFieldObjectData">
                    <Size X="484.0000" Y="50.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="203.0000" Y="24.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="82" G="52" B="49" />
                    <PrePosition X="0.5075" Y="0.4800" />
                    <PreSize X="1.2100" Y="1.0000" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint />
                <Position X="301.1550" Y="315.5912" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3137" Y="0.4931" />
                <PreSize X="0.4167" Y="0.0781" />
                <FileData Type="Normal" Path="registersdk/edt_white.png" Plist="" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="getVerify" ActionTag="1537110772" Tag="284" IconVisible="True" LeftMargin="554.5956" RightMargin="224.4044" TopMargin="369.5689" BottomMargin="216.4311" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
                <Size X="181.0000" Y="54.0000" />
                <AnchorPoint />
                <Position X="554.5956" Y="216.4311" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5777" Y="0.3382" />
                <PreSize X="0.1885" Y="0.0844" />
                <FileData Type="Normal" Path="registersdk/repeat.csd" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="inviteCode" ActionTag="1443595480" VisibleForFrame="False" Tag="100" IconVisible="False" LeftMargin="232.0202" RightMargin="327.9798" TopMargin="645.3634" BottomMargin="-55.3634" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Enable="True" LeftEage="16" RightEage="16" TopEage="16" BottomEage="16" Scale9OriginX="16" Scale9OriginY="16" Scale9Width="467" Scale9Height="30" ctype="PanelObjectData">
                <Size X="400.0000" Y="50.0000" />
                <Children>
                  <AbstractNodeData Name="Text_1" ActionTag="-573442680" Tag="101" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="-106.9999" RightMargin="394.9999" TopMargin="11.0000" BottomMargin="11.0000" FontSize="28" LabelText="邀请码：" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="112.0000" Y="28.0000" />
                    <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
                    <Position X="5.0001" Y="25.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.0125" Y="0.5000" />
                    <PreSize X="0.2800" Y="0.5600" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="input" ActionTag="-1944111216" Tag="102" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" LeftMargin="4.0000" RightMargin="4.0000" TouchEnable="True" FontSize="25" IsCustomSize="True" LabelText="" PlaceHolderText="" MaxLengthEnable="True" MaxLengthText="12" PasswordEnable="True" ctype="TextFieldObjectData">
                    <Size X="392.0000" Y="50.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="200.0000" Y="25.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="193" G="193" B="193" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="0.9800" Y="1.0000" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="tip" ActionTag="-1680787715" Tag="97" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="392.3106" RightMargin="-142.3106" TopMargin="10.0000" BottomMargin="10.0000" FontSize="30" LabelText="（非必填）" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="150.0000" Y="30.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="392.3106" Y="25.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="0" B="0" />
                    <PrePosition X="0.9808" Y="0.5000" />
                    <PreSize X="0.3750" Y="0.6000" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint />
                <Position X="232.0202" Y="-55.3634" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2417" Y="-0.0865" />
                <PreSize X="0.4167" Y="0.0781" />
                <FileData Type="Normal" Path="registersdk/edt_white.png" Plist="" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="close" ActionTag="-2101250663" Tag="384" IconVisible="False" LeftMargin="872.3890" RightMargin="7.6110" TopMargin="10.1971" BottomMargin="548.8029" ctype="SpriteObjectData">
                <Size X="80.0000" Y="81.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="912.3890" Y="589.3029" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.9504" Y="0.9208" />
                <PreSize X="0.0833" Y="0.1266" />
                <FileData Type="Normal" Path="registersdk/x.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="tip" ActionTag="-22920762" Tag="655" IconVisible="False" LeftMargin="94.6463" RightMargin="865.3537" TopMargin="503.6713" BottomMargin="136.3287" FontSize="22" LabelText="" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="94.6463" Y="136.3287" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="0" B="0" />
                <PrePosition X="0.0986" Y="0.2130" />
                <PreSize X="0.0000" Y="0.0000" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="480.0000" Y="320.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="1.0000" Y="1.0000" />
            <FileData Type="Normal" Path="hall/res/bingphone/bg.png" Plist="" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="logo" ActionTag="-220714853" VisibleForFrame="False" Tag="126" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-33.5360" RightMargin="737.5360" TopMargin="-8.6160" BottomMargin="486.6160" ctype="SpriteObjectData">
            <Size X="256.0000" Y="162.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="94.4640" Y="567.6160" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0984" Y="0.8869" />
            <PreSize X="0.2667" Y="0.2531" />
            <FileData Type="Normal" Path="registersdk/logo01.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>