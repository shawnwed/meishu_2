<GameFile>
  <PropertyGroup Name="top" Type="Node" ID="35b1c859-b65e-48c4-85fe-ead3e08796e9" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="41" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="btn_banker" ActionTag="180200772" Tag="43" IconVisible="False" LeftMargin="96.0784" RightMargin="-176.0784" TopMargin="57.4842" BottomMargin="-137.4842" ctype="SpriteObjectData">
            <Size X="80.0000" Y="80.0000" />
            <Children>
              <AbstractNodeData Name="txt" ActionTag="-1132641864" Tag="960" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="13.9440" RightMargin="16.0560" TopMargin="26.4520" BottomMargin="28.5480" FontSize="25" LabelText="上莊" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="50.0000" Y="25.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="38.9440" Y="41.0480" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="227" B="74" />
                <PrePosition X="0.4868" Y="0.5131" />
                <PreSize X="0.6250" Y="0.3125" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="136.0784" Y="-97.4842" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="baccarat/res/table/btn_banker_1.png" Plist="baccarat/res/table.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="banker_info_bg" ActionTag="-2059691476" Tag="1900" IconVisible="False" LeftMargin="-335.5009" RightMargin="-335.4991" BottomMargin="-33.0000" LeftEage="100" RightEage="100" TopEage="10" BottomEage="10" Scale9OriginX="100" Scale9OriginY="10" Scale9Width="471" Scale9Height="13" ctype="ImageViewObjectData">
            <Size X="671.0000" Y="33.0000" />
            <AnchorPoint ScaleX="0.5002" ScaleY="1.0000" />
            <Position X="0.1333" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="wenzhouShowHand/res/table/top_bg.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="banker_win_txt" ActionTag="-1621847199" Tag="657" IconVisible="False" LeftMargin="153.5003" RightMargin="-198.5003" TopMargin="7.4172" BottomMargin="-25.4172" FontSize="18" LabelText="12324" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="45.0000" Y="18.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="153.5003" Y="-16.4172" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="242" B="41" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="round_time_txt" ActionTag="-1932693583" Tag="656" IconVisible="False" LeftMargin="-128.4999" RightMargin="38.4999" TopMargin="7.4172" BottomMargin="-25.4172" FontSize="18" LabelText="12局后下庄" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="90.0000" Y="18.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="-128.4999" Y="-16.4172" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="242" B="41" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Text_35_0" ActionTag="986515989" Tag="655" IconVisible="False" LeftMargin="60.0008" RightMargin="-141.0008" TopMargin="7.4172" BottomMargin="-25.4172" FontSize="18" LabelText="莊家收益:" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="81.0000" Y="18.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="60.0008" Y="-16.4172" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Text_35" ActionTag="93176131" Tag="654" IconVisible="False" LeftMargin="-241.0003" RightMargin="160.0003" TopMargin="7.4172" BottomMargin="-25.4172" FontSize="18" LabelText="可連莊數:" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="81.0000" Y="18.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="-241.0003" Y="-16.4172" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="head" ActionTag="-1677152353" Tag="2520" IconVisible="True" LeftMargin="0.0001" RightMargin="-0.0001" TopMargin="95.9994" BottomMargin="-95.9994" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="0.0001" Y="-95.9994" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="baccarat/csb/table/seat.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="banker" ActionTag="-1615338517" Tag="283" IconVisible="False" LeftMargin="-75.0693" RightMargin="34.0693" TopMargin="43.5758" BottomMargin="-85.5758" ctype="SpriteObjectData">
            <Size X="41.0000" Y="42.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-54.5693" Y="-64.5758" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="baccarat/lan/cn/banker_flag.png" Plist="baccarat/lan/cn/game.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>