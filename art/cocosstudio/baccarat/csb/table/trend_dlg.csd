<GameFile>
  <PropertyGroup Name="trend_dlg" Type="Node" ID="97fbdbae-0267-4626-b186-6a9f6144d75e" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="3774" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="bg" ActionTag="-808082714" Tag="3963" IconVisible="False" LeftMargin="-449.8300" RightMargin="-0.1700" TopMargin="-87.4999" BottomMargin="-87.5001" TouchEnable="True" LeftEage="305" RightEage="305" TopEage="180" BottomEage="180" Scale9OriginX="145" Scale9OriginY="-5" Scale9Width="160" Scale9Height="185" ctype="ImageViewObjectData">
            <Size X="450.0000" Y="175.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-224.8300" Y="-0.0001" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="baccarat/res/road_bg.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="btn1" ActionTag="668767690" Tag="1396" IconVisible="False" LeftMargin="-439.6747" RightMargin="393.6747" TopMargin="-76.5000" BottomMargin="-4.5000" ctype="SpriteObjectData">
            <Size X="46.0000" Y="81.0000" />
            <Children>
              <AbstractNodeData Name="txt" ActionTag="2139344890" Tag="1401" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="13.0000" RightMargin="13.0000" TopMargin="10.5000" BottomMargin="10.5000" FontSize="20" LabelText="莊&#xA;閑&#xA;路" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="20.0000" Y="60.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="23.0000" Y="40.5000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5000" />
                <PreSize X="0.4348" Y="0.7407" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-416.6747" Y="36.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="baccarat/res/table/road_btn1.png" Plist="baccarat/res/table.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="btn2" ActionTag="-2061979336" Tag="1397" IconVisible="False" LeftMargin="-439.6747" RightMargin="393.6747" TopMargin="4.5000" BottomMargin="-85.5000" ctype="SpriteObjectData">
            <Size X="46.0000" Y="81.0000" />
            <Children>
              <AbstractNodeData Name="txt" ActionTag="640092375" Tag="1402" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="13.0000" RightMargin="13.0000" TopMargin="20.5000" BottomMargin="20.5000" FontSize="20" LabelText="大&#xA;路" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="20.0000" Y="40.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="23.0000" Y="40.5000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5000" />
                <PreSize X="0.4348" Y="0.4938" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-416.6747" Y="-45.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="baccarat/res/table/road_btn2.png" Plist="baccarat/res/table.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="road_banker_bg_3" ActionTag="-634167987" Tag="1398" IconVisible="False" LeftMargin="-390.8749" RightMargin="355.8749" TopMargin="-76.1981" BottomMargin="27.1981" ctype="SpriteObjectData">
            <Size X="35.0000" Y="49.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-373.3749" Y="51.6981" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="baccarat/res/table/road_banker_bg.png" Plist="baccarat/res/table.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="road_player_bg_4" ActionTag="-391718338" Tag="1399" IconVisible="False" LeftMargin="-390.8751" RightMargin="355.8751" TopMargin="-21.8730" BottomMargin="-27.1270" ctype="SpriteObjectData">
            <Size X="35.0000" Y="49.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-373.3751" Y="-2.6270" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="baccarat/res/table/road_player_bg.png" Plist="baccarat/res/table.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="road_tie_bg_5" ActionTag="1579434987" Tag="1400" IconVisible="False" LeftMargin="-390.8752" RightMargin="355.8752" TopMargin="33.4524" BottomMargin="-82.4524" ctype="SpriteObjectData">
            <Size X="35.0000" Y="49.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-373.3752" Y="-57.9524" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="baccarat/res/table/road_tie_bg.png" Plist="baccarat/res/table.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="tie_txt" ActionTag="495714228" Tag="1046" IconVisible="False" LeftMargin="-386.2650" RightMargin="362.2650" TopMargin="58.6010" BottomMargin="-82.6010" FontSize="24" LabelText="56" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="24.0000" Y="24.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-374.2650" Y="-70.6010" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="tie_lb" ActionTag="-881755582" Tag="1045" IconVisible="False" LeftMargin="-386.2653" RightMargin="362.2653" TopMargin="34.2757" BottomMargin="-58.2757" FontSize="24" LabelText="和" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="24.0000" Y="24.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-374.2653" Y="-46.2757" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="player_txt" ActionTag="-429589927" Tag="1044" IconVisible="False" LeftMargin="-386.2646" RightMargin="362.2646" TopMargin="3.9924" BottomMargin="-27.9924" FontSize="24" LabelText="56" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="24.0000" Y="24.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-374.2646" Y="-15.9924" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="player_lb" ActionTag="-283185621" Tag="1043" IconVisible="False" LeftMargin="-386.2647" RightMargin="362.2647" TopMargin="-21.5342" BottomMargin="-2.4658" FontSize="24" LabelText="闲" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="24.0000" Y="24.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-374.2647" Y="9.5342" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="banker_txt" ActionTag="-853763298" Tag="1042" IconVisible="False" LeftMargin="-386.2648" RightMargin="362.2648" TopMargin="-51.5056" BottomMargin="27.5056" FontSize="24" LabelText="56" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="24.0000" Y="24.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-374.2648" Y="39.5056" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="banker_lb" ActionTag="526619584" Tag="1041" IconVisible="False" LeftMargin="-386.2645" RightMargin="362.2645" TopMargin="-74.5308" BottomMargin="50.5308" FontSize="24" LabelText="庄" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="24.0000" Y="24.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-374.2645" Y="62.5308" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="roadNode" ActionTag="-666078490" Tag="3678" IconVisible="False" LeftMargin="-351.4353" RightMargin="1.4353" TopMargin="-77.0000" BottomMargin="-83.0000" TouchEnable="True" ClipAble="True" BackColorAlpha="100" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" IsBounceEnabled="True" ScrollDirectionType="Horizontal" ctype="ScrollViewObjectData">
            <Size X="350.0000" Y="160.0000" />
            <AnchorPoint ScaleY="1.0000" />
            <Position X="-351.4353" Y="77.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <SingleColor A="255" R="255" G="150" B="100" />
            <FirstColor A="255" R="255" G="150" B="100" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
            <InnerNodeSize Width="600" Height="160" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>