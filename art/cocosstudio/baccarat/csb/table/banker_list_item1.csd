<GameFile>
  <PropertyGroup Name="banker_list_item1" Type="Node" ID="a8763c16-101f-42ea-8275-8d00bcf3fbae" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="3994" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="bg" ActionTag="1382883809" Alpha="184" Tag="4002" IconVisible="False" LeftMargin="-0.8171" RightMargin="-776.1829" TopMargin="-59.6464" BottomMargin="-0.3536" Scale9Enable="True" LeftEage="39" RightEage="39" TopEage="19" BottomEage="19" Scale9OriginX="39" Scale9OriginY="19" Scale9Width="43" Scale9Height="21" ctype="ImageViewObjectData">
            <Size X="777.0000" Y="60.0000" />
            <AnchorPoint />
            <Position X="-0.8171" Y="-0.3536" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="baccarat/res/table/item_bg.png" Plist="baccarat/res/table.plist" />
          </AbstractNodeData>
          <AbstractNodeData Name="name" ActionTag="-131472888" Tag="4001" IconVisible="False" LeftMargin="238.0002" RightMargin="-388.0002" TopMargin="-39.6259" BottomMargin="14.6259" FontSize="25" LabelText="那么多为什么" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="150.0000" Y="25.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="238.0002" Y="27.1259" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="coin_txt" ActionTag="120751057" Tag="63" IconVisible="False" LeftMargin="520.8112" RightMargin="-612.8112" TopMargin="-42.2395" BottomMargin="9.2395" LabelText="54万" ctype="TextBMFontObjectData">
            <Size X="92.0000" Y="33.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="520.8112" Y="25.7395" />
            <Scale ScaleX="0.7500" ScaleY="0.7500" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <LabelBMFontFile_CNB Type="Normal" Path="hall/res/rank_num.fnt" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="txt" ActionTag="1755307849" Tag="4156" IconVisible="False" LeftMargin="39.6607" RightMargin="-139.6607" TopMargin="-39.3385" BottomMargin="14.3385" FontSize="25" LabelText="等待上莊" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="100.0000" Y="25.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="39.6607" Y="26.8385" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="81" G="70" B="69" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="coin_icon_0" ActionTag="133066258" Tag="4157" IconVisible="False" LeftMargin="467.7704" RightMargin="-505.7704" TopMargin="-44.5284" BottomMargin="6.5284" ctype="SpriteObjectData">
            <Size X="38.0000" Y="38.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="467.7704" Y="25.5284" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="game_public/res/coin_small_icon.png" Plist="game_public/res/game.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>