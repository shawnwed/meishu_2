<GameFile>
  <PropertyGroup Name="win_txt" Type="Node" ID="c44f76d9-394b-4f66-af5e-9de427da2a35" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="860" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="txt" ActionTag="1765254329" Tag="861" IconVisible="False" LeftMargin="-87.0000" RightMargin="-87.0000" TopMargin="-57.0000" BottomMargin="-23.0000" LabelText="1234.5万" ctype="TextBMFontObjectData">
            <Size X="174.0000" Y="80.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position Y="17.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <LabelBMFontFile_CNB Type="Normal" Path="baccarat/res/number/num_win.fnt" Plist="" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>