<GameFile>
  <PropertyGroup Name="total_bet_bar" Type="Node" ID="bef1d513-f4a1-47f9-bdd4-2fb08bf21cc8" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="1902" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="bg" ActionTag="-1694141943" Tag="1904" IconVisible="False" LeftMargin="-266.5000" RightMargin="-266.5000" TopMargin="-21.5000" BottomMargin="-21.5000" ctype="SpriteObjectData">
            <Size X="533.0000" Y="43.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="baccarat/res/table/slider_track2.png" Plist="baccarat/res/table.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="bar" ActionTag="1763410936" Tag="1903" IconVisible="False" LeftMargin="-262.2755" RightMargin="-270.7245" TopMargin="-23.0980" BottomMargin="-19.9020" ProgressInfo="18" ctype="LoadingBarObjectData">
            <Size X="533.0000" Y="43.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="-262.2755" Y="1.5980" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <ImageFileData Type="MarkedSubImage" Path="baccarat/res/table/slider_fill2.png" Plist="baccarat/res/table.plist" />
          </AbstractNodeData>
          <AbstractNodeData Name="Text_1" ActionTag="-1906577210" Tag="1906" IconVisible="False" LeftMargin="-127.0000" RightMargin="57.0000" TopMargin="-11.0000" BottomMargin="-9.0000" FontSize="20" LabelText="可下注:" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="70.0000" Y="20.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="-127.0000" Y="1.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="254" G="240" B="132" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="score_txt" ActionTag="287852051" Tag="1907" IconVisible="False" LeftMargin="-49.0000" RightMargin="-56.0000" TopMargin="-15.0000" BottomMargin="-15.0000" FontSize="30" LabelText="1888888" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="105.0000" Y="30.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="-49.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>