<GameFile>
  <PropertyGroup Name="cards_player" Type="Node" ID="6d53c68c-0bf5-4703-8143-7424c847b2fc" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="2393" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="card1" ActionTag="-232588132" Tag="2394" IconVisible="True" LeftMargin="-24.7555" RightMargin="24.7555" TopMargin="72.1026" BottomMargin="-72.1026" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="-24.7555" Y="-72.1026" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="common/card.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="cardShadow1" ActionTag="2086011050" Tag="3763" IconVisible="False" LeftMargin="-58.4000" RightMargin="19.4000" TopMargin="28.0800" BottomMargin="-82.0800" ctype="SpriteObjectData">
            <Size X="39.0000" Y="54.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-38.9000" Y="-55.0800" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="baccarat/res/table/card_shadow.png" Plist="baccarat/res/table.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="card2" ActionTag="858492433" Tag="2400" IconVisible="True" LeftMargin="3.2448" RightMargin="-3.2448" TopMargin="72.1026" BottomMargin="-72.1026" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="3.2448" Y="-72.1026" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="common/card.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="cardShadow2" ActionTag="1178792054" Tag="3764" IconVisible="False" LeftMargin="-30.4998" RightMargin="-8.5002" TopMargin="28.0800" BottomMargin="-82.0800" ctype="SpriteObjectData">
            <Size X="39.0000" Y="54.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-10.9998" Y="-55.0800" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="baccarat/res/table/card_shadow.png" Plist="baccarat/res/table.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="card3" ActionTag="-800426505" Tag="2406" IconVisible="True" LeftMargin="35.7000" RightMargin="-35.7000" TopMargin="72.1000" BottomMargin="-72.1000" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="35.7000" Y="-72.1000" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="common/card.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="cardShadow3" ActionTag="1415352954" Tag="3765" IconVisible="False" LeftMargin="0.5005" RightMargin="-39.5005" TopMargin="28.0800" BottomMargin="-82.0800" ctype="SpriteObjectData">
            <Size X="39.0000" Y="54.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="20.0005" Y="-55.0800" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="baccarat/res/table/card_shadow.png" Plist="baccarat/res/table.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="card4" ActionTag="305887773" Tag="905" IconVisible="True" LeftMargin="47.9996" RightMargin="-47.9996" TopMargin="72.1004" BottomMargin="-72.1004" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="47.9996" Y="-72.1004" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="common/card.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="point_bg" ActionTag="-594151377" Tag="611" IconVisible="False" LeftMargin="-73.0000" RightMargin="-73.0000" TopMargin="95.5000" BottomMargin="-140.5000" ctype="SpriteObjectData">
            <Size X="146.0000" Y="45.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position Y="-118.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="baccarat/res/table/point_bg.png" Plist="baccarat/res/table.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="point_num" ActionTag="704743068" Tag="2427" IconVisible="False" LeftMargin="-40.9057" RightMargin="4.9057" TopMargin="100.2500" BottomMargin="-142.2500" ctype="SpriteObjectData">
            <Size X="36.0000" Y="42.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-22.9057" Y="-121.2500" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="baccarat/res/table/word_point_1.png" Plist="baccarat/res/table.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="point_word" ActionTag="-1055345924" Tag="2428" IconVisible="False" LeftMargin="2.6399" RightMargin="-38.6399" TopMargin="102.2500" BottomMargin="-140.2500" ctype="SpriteObjectData">
            <Size X="36.0000" Y="38.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="20.6399" Y="-121.2500" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="baccarat/lan/cn/word_point_11.png" Plist="baccarat/lan/cn/game.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="win" ActionTag="820945025" Tag="2434" IconVisible="False" LeftMargin="-49.0000" RightMargin="-49.0000" TopMargin="-25.0000" BottomMargin="-23.0000" ctype="SpriteObjectData">
            <Size X="98.0000" Y="48.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position Y="1.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="baccarat/res/table/word_win.png" Plist="baccarat/res/table.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="cardShadow4" ActionTag="-894019371" Tag="3766" IconVisible="False" LeftMargin="14.5715" RightMargin="-53.5715" TopMargin="28.0800" BottomMargin="-82.0800" ctype="SpriteObjectData">
            <Size X="39.0000" Y="54.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="34.0715" Y="-55.0800" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="baccarat/res/table/card_shadow.png" Plist="baccarat/res/table.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>