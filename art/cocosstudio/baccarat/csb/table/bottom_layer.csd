<GameFile>
  <PropertyGroup Name="bottom_layer" Type="Layer" ID="1c5619e7-ad72-40e2-8c3c-e57a746bbced" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="769" ctype="GameLayerObjectData">
        <Size X="722.0000" Y="120.0000" />
        <Children>
          <AbstractNodeData Name="banker_info" Visible="False" ActionTag="-1714644104" Tag="1393" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="89.1670" RightMargin="-17.1670" TopMargin="17.0000" BottomMargin="3.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="0" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="650.0000" Y="100.0000" />
            <Children>
              <AbstractNodeData Name="win_txt" ActionTag="579303459" Tag="1394" IconVisible="False" LeftMargin="304.6052" RightMargin="268.3948" TopMargin="69.3042" BottomMargin="8.6958" FontSize="22" LabelText="9999999" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="77.0000" Y="22.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="304.6052" Y="19.6958" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="227" B="55" />
                <PrePosition X="0.4686" Y="0.1970" />
                <PreSize X="0.1185" Y="0.2200" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="coin_icon" ActionTag="-2061849210" Tag="1395" IconVisible="False" LeftMargin="261.1020" RightMargin="345.8980" TopMargin="58.8042" BottomMargin="-1.8042" ctype="SpriteObjectData">
                <Size X="43.0000" Y="43.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="282.6020" Y="19.6958" />
                <Scale ScaleX="0.5500" ScaleY="0.5500" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4348" Y="0.1970" />
                <PreSize X="0.0662" Y="0.4300" />
                <FileData Type="MarkedSubImage" Path="hall/res/first/coin_money.png" Plist="hall/res/first.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_3_1" ActionTag="914962602" Tag="1396" IconVisible="False" LeftMargin="163.2527" RightMargin="387.7473" TopMargin="69.3042" BottomMargin="8.6958" FontSize="22" LabelText="当前输赢:" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="99.0000" Y="22.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="212.7527" Y="19.6958" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="56" G="211" B="211" />
                <PrePosition X="0.3273" Y="0.1970" />
                <PreSize X="0.1523" Y="0.2200" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="leave_time" ActionTag="1814582289" Tag="1397" IconVisible="False" LeftMargin="506.5958" RightMargin="35.4042" TopMargin="23.3039" BottomMargin="58.6961" FontSize="18" LabelText="(16局后下庄)" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="108.0000" Y="18.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="506.5958" Y="67.6961" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="56" G="211" B="211" />
                <PrePosition X="0.7794" Y="0.6770" />
                <PreSize X="0.1662" Y="0.1800" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="total_time" ActionTag="211486801" Tag="1398" IconVisible="False" LeftMargin="433.4692" RightMargin="156.5308" TopMargin="17.3038" BottomMargin="52.6962" FontSize="30" LabelText="20局" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="60.0000" Y="30.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="463.4692" Y="67.6962" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="227" B="55" />
                <PrePosition X="0.7130" Y="0.6770" />
                <PreSize X="0.0923" Y="0.3000" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_3_0" ActionTag="1737836874" Tag="1399" IconVisible="False" LeftMargin="326.2527" RightMargin="224.7473" TopMargin="21.3039" BottomMargin="56.6961" FontSize="22" LabelText="坐庄局数:" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="99.0000" Y="22.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="375.7527" Y="67.6961" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="56" G="211" B="211" />
                <PrePosition X="0.5781" Y="0.6770" />
                <PreSize X="0.1523" Y="0.2200" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="coin_txt" ActionTag="786023650" Tag="1400" IconVisible="False" LeftMargin="524.6052" RightMargin="48.3948" TopMargin="69.3042" BottomMargin="8.6958" FontSize="22" LabelText="9999999" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="77.0000" Y="22.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="524.6052" Y="19.6958" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="227" B="55" />
                <PrePosition X="0.8071" Y="0.1970" />
                <PreSize X="0.1185" Y="0.2200" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_3" ActionTag="-1029409076" Tag="1401" IconVisible="False" LeftMargin="443.2527" RightMargin="129.7473" TopMargin="69.3042" BottomMargin="8.6958" FontSize="22" LabelText="总带入:" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="77.0000" Y="22.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="481.7527" Y="19.6958" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="56" G="211" B="211" />
                <PrePosition X="0.7412" Y="0.1970" />
                <PreSize X="0.1185" Y="0.2200" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position X="89.1670" Y="3.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.1235" Y="0.0250" />
            <PreSize X="0.9003" Y="0.8333" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="head" ActionTag="-880699350" Tag="1480" IconVisible="True" LeftMargin="74.0000" RightMargin="648.0000" TopMargin="34.0000" BottomMargin="86.0000" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="74.0000" Y="86.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.1025" Y="0.7167" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="baccarat/csb/table/seat.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="bet_btn_1" ActionTag="1318966161" Tag="1506" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="186.0825" RightMargin="416.9175" TopMargin="-0.4960" BottomMargin="1.4960" ctype="SpriteObjectData">
            <Size X="119.0000" Y="119.0000" />
            <Children>
              <AbstractNodeData Name="bet_txt" ActionTag="779448830" Tag="1507" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="27.5000" RightMargin="27.5000" TopMargin="43.0000" BottomMargin="43.0000" FontSize="32" LabelText="1234" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="64.0000" Y="33.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="59.5000" Y="59.5000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5000" />
                <PreSize X="0.5378" Y="0.2773" />
                <FontResource Type="Default" Path="" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="245.5825" Y="60.9960" />
            <Scale ScaleX="1.2000" ScaleY="1.2000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3401" Y="0.5083" />
            <PreSize X="0.1648" Y="0.9917" />
            <FileData Type="MarkedSubImage" Path="baccarat/res/table/btn_bet_1.png" Plist="baccarat/res/table.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="bet_btn_2" ActionTag="1611836992" Tag="1508" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="315.3087" RightMargin="287.6913" TopMargin="-0.4960" BottomMargin="1.4960" ctype="SpriteObjectData">
            <Size X="119.0000" Y="119.0000" />
            <Children>
              <AbstractNodeData Name="bet_txt" ActionTag="6251949" Tag="1509" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="27.5000" RightMargin="27.5000" TopMargin="43.0000" BottomMargin="43.0000" FontSize="32" LabelText="1234" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="64.0000" Y="33.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="59.5000" Y="59.5000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5000" />
                <PreSize X="0.5378" Y="0.2773" />
                <FontResource Type="Default" Path="" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="374.8087" Y="60.9960" />
            <Scale ScaleX="1.2000" ScaleY="1.2000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5191" Y="0.5083" />
            <PreSize X="0.1648" Y="0.9917" />
            <FileData Type="MarkedSubImage" Path="baccarat/res/table/btn_bet_1.png" Plist="baccarat/res/table.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="bet_btn_3" ActionTag="-1916578" Tag="1510" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="444.5350" RightMargin="158.4650" TopMargin="-0.4960" BottomMargin="1.4960" ctype="SpriteObjectData">
            <Size X="119.0000" Y="119.0000" />
            <Children>
              <AbstractNodeData Name="bet_txt" ActionTag="-1453662920" Tag="1511" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="27.5000" RightMargin="27.5000" TopMargin="43.0000" BottomMargin="43.0000" FontSize="32" LabelText="1234" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="64.0000" Y="33.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="59.5000" Y="59.5000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5000" />
                <PreSize X="0.5378" Y="0.2773" />
                <FontResource Type="Default" Path="" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="504.0350" Y="60.9960" />
            <Scale ScaleX="1.2000" ScaleY="1.2000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.6981" Y="0.5083" />
            <PreSize X="0.1648" Y="0.9917" />
            <FileData Type="MarkedSubImage" Path="baccarat/res/table/btn_bet_1.png" Plist="baccarat/res/table.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="bet_btn_4" ActionTag="-1821974040" Tag="1512" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="573.7614" RightMargin="29.2386" TopMargin="-0.4960" BottomMargin="1.4960" ctype="SpriteObjectData">
            <Size X="119.0000" Y="119.0000" />
            <Children>
              <AbstractNodeData Name="bet_txt" ActionTag="747329872" Tag="1513" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="27.5000" RightMargin="27.5000" TopMargin="43.0000" BottomMargin="43.0000" FontSize="32" LabelText="1234" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="64.0000" Y="33.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="59.5000" Y="59.5000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5000" />
                <PreSize X="0.5378" Y="0.2773" />
                <FontResource Type="Default" Path="" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="633.2614" Y="60.9960" />
            <Scale ScaleX="1.2000" ScaleY="1.2000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8771" Y="0.5083" />
            <PreSize X="0.1648" Y="0.9917" />
            <FileData Type="MarkedSubImage" Path="baccarat/res/table/btn_bet_1.png" Plist="baccarat/res/table.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>