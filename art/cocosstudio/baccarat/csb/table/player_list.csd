<GameFile>
  <PropertyGroup Name="player_list" Type="Node" ID="a0d4f0fc-1db9-4e0d-adef-2afd957f4959" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="3783" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="mask" CanEdit="False" ActionTag="-1035608369" Alpha="0" Tag="3960" IconVisible="False" LeftMargin="-640.0000" RightMargin="-640.0000" TopMargin="-361.0000" BottomMargin="-359.0000" ClipAble="False" BackColorAlpha="102" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="1280.0000" Y="720.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position Y="1.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <SingleColor A="255" R="26" G="26" B="26" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="bg" ActionTag="1941148739" Tag="3962" IconVisible="False" LeftMargin="-457.5000" RightMargin="-457.5000" TopMargin="-288.0000" BottomMargin="-288.0000" TouchEnable="True" LeftEage="305" RightEage="305" TopEage="180" BottomEage="180" Scale9OriginX="305" Scale9OriginY="180" Scale9Width="305" Scale9Height="216" ctype="ImageViewObjectData">
            <Size X="915.0000" Y="576.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="baccarat/res/dialog_bg.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="containor" ActionTag="508321815" Tag="991" IconVisible="True" TopMargin="35.0000" BottomMargin="-35.0000" ctype="SingleNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position Y="-35.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="banker_title" ActionTag="1271291258" Tag="4275" IconVisible="False" LeftMargin="-87.5000" RightMargin="-87.5000" TopMargin="-257.5232" BottomMargin="210.5232" ctype="SpriteObjectData">
            <Size X="175.0000" Y="47.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position Y="234.0232" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="baccarat/lan/cn/players_title.png" Plist="baccarat/lan/cn/game.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="closeBtn" ActionTag="1324655776" Tag="4276" IconVisible="False" LeftMargin="406.0110" RightMargin="-486.0110" TopMargin="-304.3285" BottomMargin="223.3285" ctype="SpriteObjectData">
            <Size X="80.0000" Y="81.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="446.0110" Y="263.8285" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="game_public/res/user_info_close.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="line2" ActionTag="-162296834" Tag="4277" IconVisible="False" LeftMargin="102.5000" RightMargin="-243.5000" TopMargin="-242.0000" BottomMargin="220.0000" FlipX="True" ctype="SpriteObjectData">
            <Size X="141.0000" Y="22.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="173.0000" Y="231.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="game_public/res/title_line.png" Plist="game_public/res/game.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="line1" ActionTag="-1528109727" Tag="4278" IconVisible="False" LeftMargin="-242.5000" RightMargin="101.5000" TopMargin="-242.0000" BottomMargin="220.0000" ctype="SpriteObjectData">
            <Size X="141.0000" Y="22.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-172.0000" Y="231.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="game_public/res/title_line.png" Plist="game_public/res/game.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>