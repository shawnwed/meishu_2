<GameFile>
  <PropertyGroup Name="banker_list_item0" Type="Node" ID="4c6a69ec-5f9f-408e-8b4f-af3de025543c" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="3979" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="bg" ActionTag="-196473247" Tag="3987" IconVisible="False" LeftMargin="-0.0002" RightMargin="-772.9998" TopMargin="-103.6963" BottomMargin="-0.3037" Scale9Enable="True" LeftEage="39" RightEage="39" TopEage="19" BottomEage="19" Scale9OriginX="39" Scale9OriginY="19" Scale9Width="43" Scale9Height="21" ctype="ImageViewObjectData">
            <Size X="773.0000" Y="104.0000" />
            <AnchorPoint />
            <Position X="-0.0002" Y="-0.3037" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="baccarat/res/table/item_bg.png" Plist="baccarat/res/table.plist" />
          </AbstractNodeData>
          <AbstractNodeData Name="name" ActionTag="22638187" Tag="3983" IconVisible="False" LeftMargin="221.2490" RightMargin="-371.2490" TopMargin="-89.6011" BottomMargin="64.6011" FontSize="25" LabelText="那么多为什么" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="150.0000" Y="25.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="221.2490" Y="77.1011" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="head" ActionTag="-1112918094" Tag="3985" IconVisible="False" LeftMargin="96.9999" RightMargin="-196.9999" TopMargin="-100.7082" BottomMargin="0.7082" LeftEage="25" RightEage="25" TopEage="25" BottomEage="25" Scale9OriginX="25" Scale9OriginY="25" Scale9Width="30" Scale9Height="30" ctype="ImageViewObjectData">
            <Size X="100.0000" Y="100.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="146.9999" Y="50.7082" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="baccarat/res/table/seat_avatar.png" Plist="baccarat/res/table.plist" />
          </AbstractNodeData>
          <AbstractNodeData Name="banker" ActionTag="-1365625533" Tag="3980" IconVisible="False" LeftMargin="27.2742" RightMargin="-68.2742" TopMargin="-74.5525" BottomMargin="32.5525" ctype="SpriteObjectData">
            <Size X="41.0000" Y="42.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="47.7742" Y="53.5525" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="baccarat/lan/cn/banker_flag.png" Plist="baccarat/lan/cn/game.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="Text_3" ActionTag="860692506" Tag="3988" IconVisible="False" LeftMargin="437.7245" RightMargin="-536.7245" TopMargin="-90.9714" BottomMargin="68.9714" FontSize="22" LabelText="莊家收益:" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="99.0000" Y="22.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="437.7245" Y="79.9714" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="81" G="70" B="69" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="coin_txt" ActionTag="1868262466" Tag="57" IconVisible="False" LeftMargin="257.5816" RightMargin="-349.5816" TopMargin="-46.0253" BottomMargin="13.0253" LabelText="54万" ctype="TextBMFontObjectData">
            <Size X="92.0000" Y="33.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="257.5816" Y="29.5253" />
            <Scale ScaleX="0.7500" ScaleY="0.7500" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <LabelBMFontFile_CNB Type="Normal" Path="hall/res/rank_num.fnt" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Text_3_0" ActionTag="-1094985277" Tag="3991" IconVisible="False" LeftMargin="437.7245" RightMargin="-536.7245" TopMargin="-40.9716" BottomMargin="18.9716" FontSize="22" LabelText="坐莊局數:" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="99.0000" Y="22.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="437.7245" Y="29.9716" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="81" G="70" B="69" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="total_time" ActionTag="1421729428" Tag="3992" IconVisible="False" LeftMargin="542.9438" RightMargin="-602.9438" TopMargin="-44.9718" BottomMargin="14.9718" FontSize="30" LabelText="20局" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="60.0000" Y="30.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="572.9438" Y="29.9718" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="227" B="55" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="leave_time" ActionTag="-919302520" Tag="3993" IconVisible="False" LeftMargin="616.0707" RightMargin="-724.0707" TopMargin="-38.9717" BottomMargin="20.9717" FontSize="18" LabelText="(16局后下庄)" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="108.0000" Y="18.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="616.0707" Y="29.9717" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="81" G="70" B="69" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="win_txt" ActionTag="1011955447" Tag="633" IconVisible="False" LeftMargin="542.0800" RightMargin="-619.0800" TopMargin="-90.9714" BottomMargin="68.9714" FontSize="22" LabelText="9999999" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="77.0000" Y="22.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="542.0800" Y="79.9714" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="227" B="55" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="coin_icon_0" ActionTag="-133750973" Tag="4146" IconVisible="False" LeftMargin="224.5618" RightMargin="-254.5618" TopMargin="-42.9547" BottomMargin="9.9547" ctype="SpriteObjectData">
            <Size X="30.0000" Y="33.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="239.5618" Y="26.4547" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="game_public/res/coin_small_icon.png" Plist="game_public/res/game.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>