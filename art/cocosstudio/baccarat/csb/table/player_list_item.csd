<GameFile>
  <PropertyGroup Name="player_list_item" Type="Node" ID="0278a8e0-8aed-4a15-9143-c89c4d365162" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="3903" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="Image_1" ActionTag="589826508" Tag="925" IconVisible="False" LeftMargin="1.8869" RightMargin="-233.8869" TopMargin="-99.7084" BottomMargin="2.7084" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="15" BottomEage="15" Scale9OriginX="15" Scale9OriginY="15" Scale9Width="91" Scale9Height="29" ctype="ImageViewObjectData">
            <Size X="232.0000" Y="97.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="117.8869" Y="51.2084" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="baccarat/res/table/item_bg.png" Plist="baccarat/res/table.plist" />
          </AbstractNodeData>
          <AbstractNodeData Name="head" ActionTag="1113170595" Tag="3905" IconVisible="False" LeftMargin="10.2083" RightMargin="-90.2083" TopMargin="-92.0442" BottomMargin="12.0442" LeftEage="25" RightEage="25" TopEage="25" BottomEage="25" Scale9OriginX="25" Scale9OriginY="25" Scale9Width="30" Scale9Height="30" ctype="ImageViewObjectData">
            <Size X="80.0000" Y="80.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="50.2083" Y="52.0442" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="baccarat/res/table/seat_avatar.png" Plist="baccarat/res/table.plist" />
          </AbstractNodeData>
          <AbstractNodeData Name="name" ActionTag="-1071807007" Tag="3907" IconVisible="False" LeftMargin="93.4431" RightMargin="-229.4431" TopMargin="-85.9660" BottomMargin="61.9660" IsCustomSize="True" FontSize="24" LabelText="那么多为什么" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="136.0000" Y="24.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="93.4431" Y="73.9660" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="81" G="70" B="69" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="coin_num" ActionTag="755319445" Tag="64" IconVisible="False" LeftMargin="119.5880" RightMargin="-211.5880" TopMargin="-46.0246" BottomMargin="13.0246" LabelText="54万" ctype="TextBMFontObjectData">
            <Size X="92.0000" Y="33.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="119.5880" Y="29.5246" />
            <Scale ScaleX="0.7500" ScaleY="0.7500" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <LabelBMFontFile_CNB Type="Normal" Path="hall/res/rank_num.fnt" Plist="" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>