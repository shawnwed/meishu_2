<GameFile>
  <PropertyGroup Name="table" Type="Scene" ID="4ac3f67d-3034-4a6c-a08d-a1c287212a50" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Scene" Tag="37" ctype="GameNodeObjectData">
        <Size X="960.0000" Y="640.0000" />
        <Children>
          <AbstractNodeData Name="background" CanEdit="False" ActionTag="-2014346228" Tag="39" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" LeftEage="348" RightEage="348" TopEage="198" BottomEage="198" Scale9OriginX="348" Scale9OriginY="198" Scale9Width="584" Scale9Height="324" ctype="ImageViewObjectData">
            <Size X="960.0000" Y="640.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="480.0000" Y="320.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="1.0000" Y="1.0000" />
            <FileData Type="Normal" Path="bg/baccarat_bg.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="bet_bg" CanEdit="False" ActionTag="733607396" Tag="352" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" LeftEage="333" RightEage="333" TopEage="145" BottomEage="145" Scale9OriginX="333" Scale9OriginY="145" Scale9Width="614" Scale9Height="398" ctype="ImageViewObjectData">
            <Size X="960.0000" Y="640.0000" />
            <Children>
              <AbstractNodeData Name="word" ActionTag="777544039" Tag="5687" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="373.0760" RightMargin="371.9240" TopMargin="201.7400" BottomMargin="341.2600" ctype="SpriteObjectData">
                <Size X="215.0000" Y="97.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="480.5760" Y="389.7600" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5006" Y="0.6090" />
                <PreSize X="0.2240" Y="0.1516" />
                <FileData Type="MarkedSubImage" Path="baccarat/lan/cn/table_word.png" Plist="baccarat/lan/cn/game.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="dealer" ActionTag="1180262950" Tag="750" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="540.1520" RightMargin="345.8480" TopMargin="122.2160" BottomMargin="439.7840" ctype="SpriteObjectData">
                <Size X="74.0000" Y="78.0000" />
                <Children>
                  <AbstractNodeData Name="cardCount" ActionTag="1631857873" Tag="2145" RotationSkewY="-0.0001" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="46.7828" RightMargin="-2.7828" TopMargin="53.8352" BottomMargin="4.1648" FontSize="20" LabelText="212" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="30.0000" Y="20.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="46.7828" Y="14.1648" />
                    <Scale ScaleX="1.0000" ScaleY="0.8962" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.6322" Y="0.1816" />
                    <PreSize X="0.4054" Y="0.2564" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="577.1520" Y="478.7840" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.6012" Y="0.7481" />
                <PreSize X="0.0771" Y="0.1219" />
                <FileData Type="MarkedSubImage" Path="baccarat/res/table/dealer.png" Plist="baccarat/res/table.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="time_txt" ActionTag="1515884740" Tag="1542" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="365.0880" RightMargin="384.9120" TopMargin="178.8320" BottomMargin="433.1680" FontSize="28" LabelText="下 注 时 间：15" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="210.0000" Y="28.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="365.0880" Y="447.1680" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3803" Y="0.6987" />
                <PreSize X="0.2188" Y="0.0437" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="bet_node_1" ActionTag="444741357" Tag="3392" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="5.8360" RightMargin="641.1640" TopMargin="181.1840" BottomMargin="298.8160" ctype="SpriteObjectData">
                <Size X="313.0000" Y="160.0000" />
                <Children>
                  <AbstractNodeData Name="self_bg" ActionTag="-1256541933" Tag="5308" IconVisible="False" LeftMargin="95.0000" RightMargin="78.0000" TopMargin="124.0000" ctype="SpriteObjectData">
                    <Size X="140.0000" Y="36.0000" />
                    <Children>
                      <AbstractNodeData Name="txt" ActionTag="558171494" Tag="5309" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="25.0000" RightMargin="25.0000" TopMargin="3.0000" BottomMargin="3.0000" FontSize="30" LabelText="100000" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                        <Size X="90.0000" Y="30.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="70.0000" Y="18.0000" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="239" G="208" B="98" />
                        <PrePosition X="0.5000" Y="0.5000" />
                        <PreSize X="0.6429" Y="0.8333" />
                        <OutlineColor A="255" R="255" G="0" B="0" />
                        <ShadowColor A="255" R="110" G="110" B="110" />
                      </AbstractNodeData>
                    </Children>
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="165.0000" Y="18.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5272" Y="0.1125" />
                    <PreSize X="0.4473" Y="0.2250" />
                    <FileData Type="MarkedSubImage" Path="baccarat/res/table/bet_num_bg.png" Plist="baccarat/res/table.plist" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="bet_txt" ActionTag="243266464" Tag="5303" IconVisible="False" LeftMargin="188.5000" RightMargin="34.5000" TopMargin="-3.0000" BottomMargin="133.0000" FontSize="30" LabelText="100000" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="90.0000" Y="30.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="233.5000" Y="148.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="133" B="136" />
                    <PrePosition X="0.7460" Y="0.9250" />
                    <PreSize X="0.2875" Y="0.1875" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="162.3360" Y="378.8160" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1691" Y="0.5919" />
                <PreSize X="0.3260" Y="0.2500" />
                <FileData Type="MarkedSubImage" Path="baccarat/lan/cn/bet_node_1_2.png" Plist="baccarat/lan/cn/game.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="bet_node_2" ActionTag="-2000696679" Tag="3393" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="315.4360" RightMargin="331.5640" TopMargin="346.2000" BottomMargin="147.8000" ctype="SpriteObjectData">
                <Size X="313.0000" Y="146.0000" />
                <Children>
                  <AbstractNodeData Name="bet_txt" ActionTag="1262518189" Tag="5304" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="111.5000" RightMargin="111.5000" TopMargin="11.0000" BottomMargin="105.0000" FontSize="30" LabelText="100000" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="90.0000" Y="30.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="156.5000" Y="120.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="133" B="136" />
                    <PrePosition X="0.5000" Y="0.8219" />
                    <PreSize X="0.2875" Y="0.2055" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="self_bg" ActionTag="455320439" Tag="5310" IconVisible="False" LeftMargin="87.0000" RightMargin="86.0000" TopMargin="110.0000" ctype="SpriteObjectData">
                    <Size X="140.0000" Y="36.0000" />
                    <Children>
                      <AbstractNodeData Name="txt" ActionTag="681961110" Tag="5311" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="25.0000" RightMargin="25.0000" TopMargin="3.0000" BottomMargin="3.0000" FontSize="30" LabelText="100000" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                        <Size X="90.0000" Y="30.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="70.0000" Y="18.0000" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="239" G="208" B="98" />
                        <PrePosition X="0.5000" Y="0.5000" />
                        <PreSize X="0.6429" Y="0.8333" />
                        <OutlineColor A="255" R="255" G="0" B="0" />
                        <ShadowColor A="255" R="110" G="110" B="110" />
                      </AbstractNodeData>
                    </Children>
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="157.0000" Y="18.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5016" Y="0.1233" />
                    <PreSize X="0.4473" Y="0.2466" />
                    <FileData Type="MarkedSubImage" Path="baccarat/res/table/bet_num_bg.png" Plist="baccarat/res/table.plist" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="471.9360" Y="220.8000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4916" Y="0.3450" />
                <PreSize X="0.3260" Y="0.2281" />
                <FileData Type="MarkedSubImage" Path="baccarat/lan/cn/bet_node_2_2.png" Plist="baccarat/lan/cn/game.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="bet_node_3" ActionTag="-2084513843" Tag="3394" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="627.3400" RightMargin="19.6600" TopMargin="181.1840" BottomMargin="298.8160" ctype="SpriteObjectData">
                <Size X="313.0000" Y="160.0000" />
                <Children>
                  <AbstractNodeData Name="bet_txt" ActionTag="-1596591127" Tag="5305" IconVisible="False" LeftMargin="22.5000" RightMargin="200.5000" TopMargin="-3.0000" BottomMargin="133.0000" FontSize="30" LabelText="100000" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="90.0000" Y="30.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="67.5000" Y="148.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="133" B="136" />
                    <PrePosition X="0.2157" Y="0.9250" />
                    <PreSize X="0.2875" Y="0.1875" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="self_bg" ActionTag="-57481780" Tag="5312" IconVisible="False" LeftMargin="65.0000" RightMargin="108.0000" TopMargin="124.0000" ctype="SpriteObjectData">
                    <Size X="140.0000" Y="36.0000" />
                    <Children>
                      <AbstractNodeData Name="txt" ActionTag="473284334" Tag="5313" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="25.0000" RightMargin="25.0000" TopMargin="3.0000" BottomMargin="3.0000" FontSize="30" LabelText="100000" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                        <Size X="90.0000" Y="30.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="70.0000" Y="18.0000" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="239" G="208" B="98" />
                        <PrePosition X="0.5000" Y="0.5000" />
                        <PreSize X="0.6429" Y="0.8333" />
                        <OutlineColor A="255" R="255" G="0" B="0" />
                        <ShadowColor A="255" R="110" G="110" B="110" />
                      </AbstractNodeData>
                    </Children>
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="135.0000" Y="18.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.4313" Y="0.1125" />
                    <PreSize X="0.4473" Y="0.2250" />
                    <FileData Type="MarkedSubImage" Path="baccarat/res/table/bet_num_bg.png" Plist="baccarat/res/table.plist" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="783.8400" Y="378.8160" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.8165" Y="0.5919" />
                <PreSize X="0.3260" Y="0.2500" />
                <FileData Type="MarkedSubImage" Path="baccarat/lan/cn/bet_node_3_2.png" Plist="baccarat/lan/cn/game.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="bet_node_4" ActionTag="1994504475" Tag="3395" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="4.3000" RightMargin="642.7000" TopMargin="345.6880" BottomMargin="148.3120" ctype="SpriteObjectData">
                <Size X="313.0000" Y="146.0000" />
                <Children>
                  <AbstractNodeData Name="self_bg" ActionTag="887252527" Tag="5314" IconVisible="False" LeftMargin="98.0000" RightMargin="75.0000" TopMargin="110.0000" ctype="SpriteObjectData">
                    <Size X="140.0000" Y="36.0000" />
                    <Children>
                      <AbstractNodeData Name="txt" ActionTag="-1205046535" Tag="5315" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="25.0000" RightMargin="25.0000" TopMargin="3.0000" BottomMargin="3.0000" FontSize="30" LabelText="100000" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                        <Size X="90.0000" Y="30.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="70.0000" Y="18.0000" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="239" G="208" B="98" />
                        <PrePosition X="0.5000" Y="0.5000" />
                        <PreSize X="0.6429" Y="0.8333" />
                        <OutlineColor A="255" R="255" G="0" B="0" />
                        <ShadowColor A="255" R="110" G="110" B="110" />
                      </AbstractNodeData>
                    </Children>
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="168.0000" Y="18.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5367" Y="0.1233" />
                    <PreSize X="0.4473" Y="0.2466" />
                    <FileData Type="MarkedSubImage" Path="baccarat/res/table/bet_num_bg.png" Plist="baccarat/res/table.plist" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="bet_txt" ActionTag="854090593" Tag="5306" IconVisible="False" LeftMargin="194.5000" RightMargin="28.5000" TopMargin="11.0000" BottomMargin="105.0000" FontSize="30" LabelText="100000" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="90.0000" Y="30.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="239.5000" Y="120.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="133" B="136" />
                    <PrePosition X="0.7652" Y="0.8219" />
                    <PreSize X="0.2875" Y="0.2055" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="160.8000" Y="221.3120" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1675" Y="0.3458" />
                <PreSize X="0.3260" Y="0.2281" />
                <FileData Type="MarkedSubImage" Path="baccarat/lan/cn/bet_node_4_2.png" Plist="baccarat/lan/cn/game.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="bet_node_5" ActionTag="-401641587" Tag="3396" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="629.2600" RightMargin="17.7400" TopMargin="345.6880" BottomMargin="148.3120" ctype="SpriteObjectData">
                <Size X="313.0000" Y="146.0000" />
                <Children>
                  <AbstractNodeData Name="self_bg" ActionTag="-182420735" Tag="5316" IconVisible="False" LeftMargin="65.0000" RightMargin="108.0000" TopMargin="110.0000" ctype="SpriteObjectData">
                    <Size X="140.0000" Y="36.0000" />
                    <Children>
                      <AbstractNodeData Name="txt" ActionTag="938410276" Tag="5317" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="25.0000" RightMargin="25.0000" TopMargin="3.0000" BottomMargin="3.0000" FontSize="30" LabelText="100000" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                        <Size X="90.0000" Y="30.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="70.0000" Y="18.0000" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="239" G="208" B="98" />
                        <PrePosition X="0.5000" Y="0.5000" />
                        <PreSize X="0.6429" Y="0.8333" />
                        <OutlineColor A="255" R="255" G="0" B="0" />
                        <ShadowColor A="255" R="110" G="110" B="110" />
                      </AbstractNodeData>
                    </Children>
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="135.0000" Y="18.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.4313" Y="0.1233" />
                    <PreSize X="0.4473" Y="0.2466" />
                    <FileData Type="MarkedSubImage" Path="baccarat/res/table/bet_num_bg.png" Plist="baccarat/res/table.plist" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="bet_txt" ActionTag="1513697250" Tag="5307" IconVisible="False" LeftMargin="24.5000" RightMargin="198.5000" TopMargin="11.0000" BottomMargin="105.0000" FontSize="30" LabelText="100000" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="90.0000" Y="30.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="69.5000" Y="120.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="133" B="136" />
                    <PrePosition X="0.2220" Y="0.8219" />
                    <PreSize X="0.2875" Y="0.2055" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="785.7600" Y="221.3120" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.8185" Y="0.3458" />
                <PreSize X="0.3260" Y="0.2281" />
                <FileData Type="MarkedSubImage" Path="baccarat/lan/cn/bet_node_5_2.png" Plist="baccarat/lan/cn/game.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="480.0000" Y="320.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="1.0000" Y="1.0000" />
            <FileData Type="Normal" Path="bg/baccarat_table.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="top" ActionTag="-403268706" Tag="45" IconVisible="True" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="480.0000" RightMargin="480.0000" BottomMargin="640.0000" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="480.0000" Y="640.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="1.0000" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="baccarat/csb/table/top.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="bottom" ActionTag="-1623653698" Tag="816" IconVisible="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" LeftMargin="115.0000" RightMargin="303.4640" TopMargin="533.3120" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="True" ctype="ProjectNodeObjectData">
            <Size X="541.5360" Y="106.6880" />
            <AnchorPoint />
            <Position X="115.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.1198" />
            <PreSize X="0.5641" Y="0.1667" />
            <FileData Type="Normal" Path="baccarat/csb/table/bottom_layer.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="top_info" ActionTag="509534385" Tag="339" IconVisible="True" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="939.1680" RightMargin="20.8320" TopMargin="11.5200" BottomMargin="628.4800" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="939.1680" Y="628.4800" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.9783" Y="0.9820" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="game_public/csb/top_info.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="btn_menu" ActionTag="41219309" Tag="51" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="6.0480" RightMargin="884.9520" TopMargin="5.8880" BottomMargin="565.1120" ctype="SpriteObjectData">
            <Size X="69.0000" Y="69.0000" />
            <AnchorPoint ScaleY="1.0000" />
            <Position X="6.0480" Y="634.1120" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0063" Y="0.9908" />
            <PreSize X="0.0719" Y="0.1078" />
            <FileData Type="MarkedSubImage" Path="game_public/res/btn_menu.png" Plist="game_public/res/game.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="btn_players" ActionTag="-1770867906" Tag="53" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-5.0240" RightMargin="877.0240" TopMargin="561.9920" BottomMargin="8.0080" ctype="SpriteObjectData">
            <Size X="88.0000" Y="70.0000" />
            <Children>
              <AbstractNodeData Name="txt" ActionTag="193458489" Tag="941" IconVisible="False" LeftMargin="53.5000" RightMargin="7.5000" TopMargin="3.0000" BottomMargin="49.0000" FontSize="18" LabelText="299" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="27.0000" Y="18.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="67.0000" Y="58.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7614" Y="0.8286" />
                <PreSize X="0.3068" Y="0.2571" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="38.9760" Y="43.0080" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0406" Y="0.0672" />
            <PreSize X="0.0917" Y="0.1094" />
            <FileData Type="MarkedSubImage" Path="baccarat/res/table/players_icon.png" Plist="baccarat/res/table.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="seat_0" ActionTag="-1055001859" Tag="630" IconVisible="True" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="288.0000" RightMargin="672.0000" TopMargin="84.3520" BottomMargin="555.6480" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="288.0000" Y="555.6480" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3000" Y="0.8682" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="baccarat/csb/table/seat.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="seat_1" ActionTag="2019554798" Tag="636" IconVisible="True" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="153.6000" RightMargin="806.4000" TopMargin="143.1680" BottomMargin="496.8320" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="153.6000" Y="496.8320" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.1600" Y="0.7763" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="baccarat/csb/table/seat.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="seat_2" ActionTag="1716319930" Tag="642" IconVisible="True" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="57.6000" RightMargin="902.4000" TopMargin="245.4400" BottomMargin="394.5600" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="57.6000" Y="394.5600" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0600" Y="0.6165" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="baccarat/csb/table/seat.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="seat_3" ActionTag="1626912342" Tag="648" IconVisible="True" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="57.6000" RightMargin="902.4000" TopMargin="379.5840" BottomMargin="260.4160" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="57.6000" Y="260.4160" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0600" Y="0.4069" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="baccarat/csb/table/seat.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="seat_7" ActionTag="-818919631" Tag="654" IconVisible="True" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="672.0000" RightMargin="288.0000" TopMargin="84.3520" BottomMargin="555.6480" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="672.0000" Y="555.6480" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.7000" Y="0.8682" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="baccarat/csb/table/seat.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="seat_6" ActionTag="243441931" Tag="660" IconVisible="True" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="806.4000" RightMargin="153.6000" TopMargin="143.1680" BottomMargin="496.8320" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="806.4000" Y="496.8320" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8400" Y="0.7763" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="baccarat/csb/table/seat.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="seat_5" ActionTag="-250905170" Tag="666" IconVisible="True" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="902.4000" RightMargin="57.6000" TopMargin="245.4400" BottomMargin="394.5600" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="902.4000" Y="394.5600" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.9400" Y="0.6165" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="baccarat/csb/table/seat.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="seat_4" ActionTag="-1802574129" Tag="672" IconVisible="True" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="902.4000" RightMargin="57.6000" TopMargin="379.5840" BottomMargin="260.4160" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="902.4000" Y="260.4160" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.9400" Y="0.4069" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="baccarat/csb/table/seat.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="cards_1" ActionTag="-769764317" Tag="2691" IconVisible="True" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="410.7840" RightMargin="549.2160" TopMargin="221.0560" BottomMargin="418.9440" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="410.7840" Y="418.9440" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4279" Y="0.6546" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="baccarat/csb/table/cards_player.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="cards_2" ActionTag="711943241" Tag="3022" IconVisible="True" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="535.1040" RightMargin="424.8960" TopMargin="221.0560" BottomMargin="418.9440" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="535.1040" Y="418.9440" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5574" Y="0.6546" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="baccarat/csb/table/cards_player.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="road" ActionTag="405531353" Tag="1815" IconVisible="True" PositionPercentXEnabled="True" LeftMargin="960.0000" TopMargin="552.4974" BottomMargin="87.5026" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="960.0000" Y="87.5026" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="1.0000" Y="0.1367" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="baccarat/csb/table/trend_dlg.csd" Plist="" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>