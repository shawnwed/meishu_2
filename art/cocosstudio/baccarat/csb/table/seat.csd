<GameFile>
  <PropertyGroup Name="seat" Type="Node" ID="db239171-8d45-44da-8449-5cce9ea642a1" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="469" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="win" ActionTag="444169200" Tag="1497" IconVisible="False" LeftMargin="-50.4088" RightMargin="-46.5912" TopMargin="-56.1397" BottomMargin="-40.8603" ctype="SpriteObjectData">
            <Size X="97.0000" Y="97.0000" />
            <Children>
              <AbstractNodeData Name="light" ActionTag="-69431199" Tag="1498" IconVisible="False" LeftMargin="-27.0526" RightMargin="-25.9474" TopMargin="-25.6408" BottomMargin="-27.3592" ctype="SpriteObjectData">
                <Size X="150.0000" Y="150.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="47.9474" Y="47.6408" />
                <Scale ScaleX="1.1391" ScaleY="1.1391" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4943" Y="0.4911" />
                <PreSize X="1.5464" Y="1.5464" />
                <FileData Type="MarkedSubImage" Path="baccarat/res/table/win_light.png" Plist="baccarat/res/table.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-1.9088" Y="7.6397" />
            <Scale ScaleX="1.0200" ScaleY="1.0200" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="baccarat/res/table/win_frame.png" Plist="baccarat/res/table.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="btn_sit" ActionTag="1207711727" Tag="473" IconVisible="False" LeftMargin="-52.6444" RightMargin="-49.3556" TopMargin="-56.4812" BottomMargin="-45.5188" ctype="SpriteObjectData">
            <Size X="102.0000" Y="102.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-1.6444" Y="5.4812" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="baccarat/res/table/btn_sit.png" Plist="baccarat/res/table.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="score_bg" ActionTag="-325635819" Tag="1234" IconVisible="False" LeftMargin="-72.2475" RightMargin="-59.7525" TopMargin="-57.2694" BottomMargin="-68.7306" ctype="SpriteObjectData">
            <Size X="132.0000" Y="126.0000" />
            <Children>
              <AbstractNodeData Name="coin_icon" ActionTag="411688813" VisibleForFrame="False" Tag="1235" IconVisible="False" LeftMargin="10.3127" RightMargin="83.6873" TopMargin="93.0958" BottomMargin="-5.0958" ctype="SpriteObjectData">
                <Size X="38.0000" Y="38.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="29.3127" Y="13.9042" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2221" Y="0.1104" />
                <PreSize X="0.2879" Y="0.3016" />
                <FileData Type="MarkedSubImage" Path="game_public/res/coin_small_icon.png" Plist="game_public/res/game.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="coin_txt" ActionTag="1765702966" Tag="1236" IconVisible="False" LeftMargin="31.3314" RightMargin="23.6686" TopMargin="94.1117" BottomMargin="9.8883" FontSize="22" LabelText="1234.56" HorizontalAlignmentType="HT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="77.0000" Y="22.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="69.8314" Y="20.8883" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="243" B="176" />
                <PrePosition X="0.5290" Y="0.1658" />
                <PreSize X="0.5833" Y="0.1746" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-6.2475" Y="-5.7306" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="baccarat/res/table/seat_bg.png" Plist="baccarat/res/table.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="frame" ActionTag="1038354490" Tag="3161" IconVisible="False" LeftMargin="-67.0963" RightMargin="-64.9037" TopMargin="-49.7830" BottomMargin="-41.2170" ctype="SpriteObjectData">
            <Size X="132.0000" Y="91.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-1.0963" Y="4.2830" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="baccarat/res/table/banker_head_frame.png" Plist="baccarat/res/table.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="name_txt" ActionTag="942215127" Tag="1233" IconVisible="False" LeftMargin="-49.1373" RightMargin="-46.8627" TopMargin="70.4544" BottomMargin="-94.4544" FontSize="24" LabelText="洞房不败" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="96.0000" Y="24.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-1.1373" Y="-82.4544" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="head" ActionTag="261845795" Tag="14" IconVisible="False" LeftMargin="-41.7041" RightMargin="-38.2959" TopMargin="-46.2466" BottomMargin="-33.7534" LeftEage="25" RightEage="25" TopEage="25" BottomEage="25" Scale9OriginX="25" Scale9OriginY="25" Scale9Width="30" Scale9Height="30" ctype="ImageViewObjectData">
            <Size X="80.0000" Y="80.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-1.7041" Y="6.2466" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="baccarat/res/table/seat_avatar.png" Plist="baccarat/res/table.plist" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>