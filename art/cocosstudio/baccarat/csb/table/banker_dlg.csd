<GameFile>
  <PropertyGroup Name="banker_dlg" Type="Node" ID="3b1905f2-0464-4984-bc7f-26c6225c02e1" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="3964" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="mask" CanEdit="False" ActionTag="-407423925" Alpha="0" Tag="3969" IconVisible="False" LeftMargin="-640.0000" RightMargin="-640.0000" TopMargin="-360.0000" BottomMargin="-360.0000" ClipAble="False" BackColorAlpha="102" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="1280.0000" Y="720.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <SingleColor A="255" R="26" G="26" B="26" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="bg" ActionTag="-1661615276" Tag="3965" IconVisible="False" LeftMargin="-427.0000" RightMargin="-427.0000" TopMargin="-342.0000" BottomMargin="-342.0000" Scale9Enable="True" LeftEage="257" RightEage="257" TopEage="213" BottomEage="213" Scale9OriginX="257" Scale9OriginY="213" Scale9Width="401" Scale9Height="150" ctype="ImageViewObjectData">
            <Size X="854.0000" Y="684.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="baccarat/res/dialog_bg.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="list" ActionTag="2066339052" Tag="3972" IconVisible="False" LeftMargin="-378.3204" RightMargin="-383.6796" TopMargin="-233.4605" BottomMargin="-181.5395" TouchEnable="True" ClipAble="True" BackColorAlpha="45" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" IsBounceEnabled="True" ScrollDirectionType="0" DirectionType="Vertical" ctype="ListViewObjectData">
            <Size X="762.0000" Y="415.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="2.6796" Y="25.9605" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <SingleColor A="255" R="192" G="159" B="143" />
            <FirstColor A="255" R="150" G="150" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="cancel_apply" ActionTag="719645513" Tag="3973" IconVisible="False" LeftMargin="-139.0000" RightMargin="-135.0000" TopMargin="219.7713" BottomMargin="-314.7713" ctype="SpriteObjectData">
            <Size X="274.0000" Y="95.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-2.0000" Y="-267.2713" />
            <Scale ScaleX="0.7700" ScaleY="0.7700" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="baccarat/lan/cn/btn_cancel_apply.png" Plist="baccarat/lan/cn/game.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="apply_panel" ActionTag="1353631251" Tag="3978" IconVisible="False" LeftMargin="-375.0000" RightMargin="-375.0000" TopMargin="177.7712" BottomMargin="-307.7712" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="750.0000" Y="130.0000" />
            <Children>
              <AbstractNodeData Name="slider" ActionTag="680081257" Tag="3970" IconVisible="False" LeftMargin="27.0000" RightMargin="249.0000" TopMargin="71.5000" BottomMargin="13.5000" TouchEnable="True" PercentInfo="50" ctype="SliderObjectData">
                <Size X="474.0000" Y="45.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="264.0000" Y="36.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3520" Y="0.2769" />
                <PreSize X="0.6320" Y="0.3462" />
                <BackGroundData Type="MarkedSubImage" Path="baccarat/res/table/slider_track.png" Plist="baccarat/res/table.plist" />
                <ProgressBarData Type="MarkedSubImage" Path="baccarat/res/table/slider_fill.png" Plist="baccarat/res/table.plist" />
                <BallNormalData Type="MarkedSubImage" Path="baccarat/res/table/slider_btn.png" Plist="baccarat/res/table.plist" />
                <BallPressedData Type="MarkedSubImage" Path="baccarat/res/table/slider_btn.png" Plist="baccarat/res/table.plist" />
                <BallDisabledData Type="MarkedSubImage" Path="baccarat/res/table/slider_btn.png" Plist="baccarat/res/table.plist" />
              </AbstractNodeData>
              <AbstractNodeData Name="min_take_txt" ActionTag="246065444" Tag="3974" IconVisible="False" LeftMargin="570.0000" RightMargin="50.0000" TopMargin="23.0000" BottomMargin="87.0000" FontSize="20" LabelText="至少需要600万" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="130.0000" Y="20.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="570.0000" Y="97.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="81" G="70" B="69" />
                <PrePosition X="0.7600" Y="0.7462" />
                <PreSize X="0.1733" Y="0.1538" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="apply" ActionTag="421886941" Tag="3975" IconVisible="False" LeftMargin="502.0000" RightMargin="-26.0000" TopMargin="42.5000" BottomMargin="-7.5000" ctype="SpriteObjectData">
                <Size X="274.0000" Y="95.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="639.0000" Y="40.0000" />
                <Scale ScaleX="0.7700" ScaleY="0.7700" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.8520" Y="0.3077" />
                <PreSize X="0.3653" Y="0.7308" />
                <FileData Type="MarkedSubImage" Path="baccarat/lan/cn/btn_apply.png" Plist="baccarat/lan/cn/game.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_3" ActionTag="1932866924" Tag="3976" IconVisible="False" LeftMargin="245.0000" RightMargin="379.0000" TopMargin="19.0000" BottomMargin="83.0000" FontSize="28" LabelText="攜帶上莊:" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="126.0000" Y="28.0000" />
                <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
                <Position X="371.0000" Y="97.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="81" G="70" B="69" />
                <PrePosition X="0.4947" Y="0.7462" />
                <PreSize X="0.1680" Y="0.2154" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="cur_take_txt" ActionTag="657318303" Tag="166" IconVisible="False" LeftMargin="380.9486" RightMargin="277.0514" TopMargin="15.3765" BottomMargin="81.6235" LabelText="54万" ctype="TextBMFontObjectData">
                <Size X="92.0000" Y="33.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="380.9486" Y="98.1235" />
                <Scale ScaleX="0.7500" ScaleY="0.7500" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5079" Y="0.7548" />
                <PreSize X="0.1227" Y="0.2538" />
                <LabelBMFontFile_CNB Type="Normal" Path="hall/res/rank_num.fnt" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="auto_check" ActionTag="1290736728" Tag="1153" IconVisible="False" LeftMargin="22.5000" RightMargin="662.5000" TopMargin="0.5000" BottomMargin="64.5000" ctype="SpriteObjectData">
                <Size X="65.0000" Y="65.0000" />
                <Children>
                  <AbstractNodeData Name="txt" ActionTag="428567852" Tag="1154" IconVisible="False" LeftMargin="51.0000" RightMargin="-74.0000" TopMargin="22.0000" BottomMargin="21.0000" FontSize="22" LabelText="自動補齊" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="88.0000" Y="22.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="95.0000" Y="32.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="81" G="70" B="69" />
                    <PrePosition X="1.4615" Y="0.4923" />
                    <PreSize X="1.3538" Y="0.3385" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="55.0000" Y="97.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0733" Y="0.7462" />
                <PreSize X="0.0867" Y="0.5000" />
                <FileData Type="MarkedSubImage" Path="baccarat/res/table/check_0.png" Plist="baccarat/res/table.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position X="-375.0000" Y="-307.7712" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="line1" ActionTag="-1979045964" Tag="4118" IconVisible="False" LeftMargin="-242.5000" RightMargin="101.5000" TopMargin="-300.0000" BottomMargin="278.0000" ctype="SpriteObjectData">
            <Size X="141.0000" Y="22.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-172.0000" Y="289.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="game_public/res/title_line.png" Plist="game_public/res/game.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="line2" ActionTag="-1752715615" Tag="4119" IconVisible="False" LeftMargin="102.5000" RightMargin="-243.5000" TopMargin="-300.0000" BottomMargin="278.0000" FlipX="True" ctype="SpriteObjectData">
            <Size X="141.0000" Y="22.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="173.0000" Y="289.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="game_public/res/title_line.png" Plist="game_public/res/game.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="closeBtn" ActionTag="1250997536" Tag="4120" IconVisible="False" LeftMargin="361.3590" RightMargin="-441.3590" TopMargin="-352.5842" BottomMargin="271.5842" ctype="SpriteObjectData">
            <Size X="80.0000" Y="81.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="401.3590" Y="312.0842" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="game_public/res/user_info_close.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="banker_title" ActionTag="-303487787" Tag="4121" IconVisible="False" LeftMargin="-87.5000" RightMargin="-87.5000" TopMargin="-315.5231" BottomMargin="268.5231" ctype="SpriteObjectData">
            <Size X="175.0000" Y="47.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position Y="292.0231" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="baccarat/lan/cn/banker_title.png" Plist="baccarat/lan/cn/game.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>