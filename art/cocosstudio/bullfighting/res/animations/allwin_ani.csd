<GameFile>
  <PropertyGroup Name="allwin_ani" Type="Node" ID="9f8ceb22-97a7-4e28-86d5-1306ffd5e9e2" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="25" Speed="1.0000">
        <Timeline ActionTag="-370526223" Property="Position">
          <PointFrame FrameIndex="15" X="2.0000" Y="-2.0001">
            <EasingData Type="0" />
          </PointFrame>
          <PointFrame FrameIndex="20" X="0.0000" Y="-0.0001">
            <EasingData Type="0" />
          </PointFrame>
          <PointFrame FrameIndex="25" X="3.0000" Y="-2.0002">
            <EasingData Type="0" />
          </PointFrame>
        </Timeline>
        <Timeline ActionTag="-370526223" Property="Scale">
          <ScaleFrame FrameIndex="15" X="1.0000" Y="1.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="20" X="1.0000" Y="1.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="25" X="1.0000" Y="1.0000">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="-370526223" Property="RotationSkew">
          <ScaleFrame FrameIndex="15" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="20" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="25" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="-370526223" Property="FileData">
          <TextureFrame FrameIndex="0" Tween="False">
            <TextureFile Type="MarkedSubImage" Path="bullfighting/res/animations/flashlighting/flashlighting0001.png" Plist="bullfighting/res/animations/flashlighting/flashlighting.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="5" Tween="False">
            <TextureFile Type="MarkedSubImage" Path="bullfighting/res/animations/flashlighting/flashlighting0002.png" Plist="bullfighting/res/animations/flashlighting/flashlighting.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="10" Tween="False">
            <TextureFile Type="MarkedSubImage" Path="bullfighting/res/animations/flashlighting/flashlighting0003.png" Plist="bullfighting/res/animations/flashlighting/flashlighting.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="15" Tween="False">
            <TextureFile Type="MarkedSubImage" Path="bullfighting/res/animations/flashlighting/flashlighting0003.png" Plist="bullfighting/res/animations/flashlighting/flashlighting.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="20" Tween="False">
            <TextureFile Type="MarkedSubImage" Path="bullfighting/res/animations/flashlighting/flashlighting0003.png" Plist="bullfighting/res/animations/flashlighting/flashlighting.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="25" Tween="False">
            <TextureFile Type="MarkedSubImage" Path="bullfighting/res/animations/flashlighting/flashlighting0003.png" Plist="bullfighting/res/animations/flashlighting/flashlighting.plist" />
          </TextureFrame>
        </Timeline>
      </Animation>
      <ObjectData Name="Node" Tag="3931" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="Sprite_3" ActionTag="-370526223" Tag="3934" IconVisible="False" LeftMargin="-225.0000" RightMargin="-229.0000" TopMargin="-245.9999" BottomMargin="-250.0001" ctype="SpriteObjectData">
            <Size X="454.0000" Y="496.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="2.0000" Y="-2.0001" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="bullfighting/res/animations/flashlighting/flashlighting0001.png" Plist="bullfighting/res/animations/flashlighting/flashlighting.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>