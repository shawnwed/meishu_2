<GameFile>
  <PropertyGroup Name="ani" Type="Node" ID="b72f3779-1ec7-4335-a796-cba091e24e79" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="allwin_1" ActionTag="-57476335" Tag="2" IconVisible="False" LeftMargin="-344.0000" RightMargin="146.0000" TopMargin="-162.0000" BottomMargin="66.0000" ctype="SpriteObjectData">
            <Size X="198.0000" Y="96.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-245.0000" Y="114.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="bullfighting/res/animations/allwin.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="alllose_2" ActionTag="-455596210" Tag="3" IconVisible="False" LeftMargin="-141.5000" RightMargin="-37.5000" TopMargin="-160.0000" BottomMargin="74.0000" ctype="SpriteObjectData">
            <Size X="179.0000" Y="86.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-52.0000" Y="117.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="bullfighting/res/animations/alllose.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>