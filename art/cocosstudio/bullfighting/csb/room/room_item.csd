<GameFile>
  <PropertyGroup Name="room_item" Type="Node" ID="3509220a-a7a6-4fe0-8a62-f408011e3398" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="3972" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="bg" ActionTag="-1665418817" Tag="895" IconVisible="False" LeftMargin="-222.5000" RightMargin="-222.5000" TopMargin="-145.0000" BottomMargin="-145.0000" ctype="SpriteObjectData">
            <Size X="445.0000" Y="290.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="bullfighting/res/room/room_0.png" Plist="bullfighting/res/room.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>