<GameFile>
  <PropertyGroup Name="room1" Type="Layer" ID="bedf6cbb-cae9-4353-9f9c-4569217b6f98" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="1257" ctype="GameLayerObjectData">
        <Size X="1280.0000" Y="513.0000" />
        <Children>
          <AbstractNodeData Name="bg" ActionTag="-1396572664" Tag="1258" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" LeftEage="422" RightEage="422" TopEage="175" BottomEage="175" Scale9OriginX="422" Scale9OriginY="175" Scale9Width="436" Scale9Height="182" ctype="ImageViewObjectData">
            <Size X="1280.0000" Y="513.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="640.0000" Y="256.5000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="1.0000" Y="1.0000" />
            <FileData Type="Normal" Path="bullfighting/res/room/room_bg.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="score_room" ActionTag="1538135878" Tag="1259" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="161.5000" RightMargin="673.5000" TopMargin="111.5000" BottomMargin="111.5000" ctype="SpriteObjectData">
            <Size X="445.0000" Y="290.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="384.0000" Y="256.5000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3000" Y="0.5000" />
            <PreSize X="0.3477" Y="0.5653" />
            <FileData Type="MarkedSubImage" Path="bullfighting/res/room/room_0.png" Plist="bullfighting/res/room.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="money_room" ActionTag="1037439491" Tag="1260" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="873.0000" RightMargin="361.0000" TopMargin="233.5000" BottomMargin="233.5000" ctype="SpriteObjectData">
            <Size X="445.0000" Y="290.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="896.0000" Y="256.5000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.7000" Y="0.5000" />
            <PreSize X="0.0359" Y="0.0897" />
            <FileData Type="MarkedSubImage" Path="bullfighting/res/room/room_1.png" Plist="bullfighting/res/room.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="closeBtn" ActionTag="-1564988096" Tag="1261" IconVisible="False" LeftMargin="1197.8149" RightMargin="2.1851" TopMargin="1.2310" BottomMargin="431.7690" ctype="SpriteObjectData">
            <Size X="80.0000" Y="80.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1237.8149" Y="471.7690" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.9670" Y="0.9196" />
            <PreSize X="0.0359" Y="0.0897" />
            <FileData Type="Normal" Path="bullfighting/res/room/closeBtn.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>