<GameFile>
  <PropertyGroup Name="room" Type="Scene" ID="619932bd-b489-4adf-96e2-82c85e2c3f2b" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Scene" Tag="3963" ctype="GameNodeObjectData">
        <Size X="1440.0000" Y="720.0000" />
        <Children>
          <AbstractNodeData Name="bg" ActionTag="879518081" Tag="3965" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" LeftEage="422" RightEage="422" TopEage="237" BottomEage="237" Scale9OriginX="422" Scale9OriginY="237" Scale9Width="436" Scale9Height="246" ctype="ImageViewObjectData">
            <Size X="1280.0000" Y="720.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="640.0000" Y="360.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="1.0000" Y="1.0000" />
            <FileData Type="Normal" Path="bg/public_bg.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="top" ActionTag="1002529779" Tag="1380" IconVisible="True" PositionPercentXEnabled="True" VerticalEdge="TopEdge" BottomMargin="623.0000" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="1280.0000" Y="97.0000" />
            <AnchorPoint />
            <Position Y="623.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition Y="0.8653" />
            <PreSize X="1.0000" Y="0.1347" />
            <FileData Type="Normal" Path="game_public/csb/roomStyle1_top.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="room_item_1" Visible="False" ActionTag="-1527913657" Tag="897" IconVisible="True" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="358.4000" RightMargin="921.6000" TopMargin="403.2000" BottomMargin="316.8000" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="358.4000" Y="316.8000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.2800" Y="0.4400" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="bullfighting/csb/room/room_item.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="room_item_2" ActionTag="563886237" Tag="901" IconVisible="True" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="640.0000" RightMargin="640.0000" TopMargin="403.2000" BottomMargin="316.8000" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="640.0000" Y="316.8000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.4400" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="bullfighting/csb/room/room_item.csd" Plist="" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>