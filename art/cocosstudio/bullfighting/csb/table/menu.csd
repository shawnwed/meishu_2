<GameFile>
  <PropertyGroup Name="menu" Type="Node" ID="e19e9da4-b3b5-419c-bc23-58c55e441ba1" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="391" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="bg" ActionTag="-1504021837" Tag="411" IconVisible="False" LeftMargin="0.0001" RightMargin="-273.0001" TopMargin="0.8759" BottomMargin="-720.8759" TouchEnable="True" LeftEage="87" RightEage="87" TopEage="117" BottomEage="117" Scale9OriginX="87" Scale9OriginY="117" Scale9Width="80" Scale9Height="486" ctype="ImageViewObjectData">
            <Size X="273.0000" Y="720.0000" />
            <AnchorPoint ScaleY="1.0000" />
            <Position X="0.0001" Y="-0.8759" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="bullfighting/res/table/menu_bg.png" Plist="bullfighting/res/table.plist" />
          </AbstractNodeData>
          <AbstractNodeData Name="prev_score" ActionTag="1340314479" Tag="493" IconVisible="False" LeftMargin="20.3301" RightMargin="-248.3301" TopMargin="413.7767" BottomMargin="-496.7767" ctype="SpriteObjectData">
            <Size X="228.0000" Y="83.0000" />
            <Children>
              <AbstractNodeData Name="title" ActionTag="-1236398715" Tag="494" IconVisible="False" LeftMargin="59.1814" RightMargin="56.8186" TopMargin="-12.9877" BottomMargin="67.9877" FontSize="28" LabelText="上局输赢" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="112.0000" Y="28.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="115.1814" Y="81.9877" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="183" G="96" B="80" />
                <PrePosition X="0.5052" Y="0.9878" />
                <PreSize X="0.4912" Y="0.3373" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="score" ActionTag="1041582302" Tag="496" IconVisible="False" LeftMargin="60.5000" RightMargin="41.5000" TopMargin="25.0000" BottomMargin="22.0000" FontSize="36" LabelText="-300000" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="126.0000" Y="36.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="123.5000" Y="40.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="248" G="219" B="64" />
                <PrePosition X="0.5417" Y="0.4819" />
                <PreSize X="0.5526" Y="0.4337" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="134.3301" Y="-455.2767" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="bullfighting/res/table/menu_line2.png" Plist="bullfighting/res/table.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="room_score" ActionTag="-1970230254" Tag="497" IconVisible="False" LeftMargin="20.3301" RightMargin="-248.3301" TopMargin="513.1011" BottomMargin="-596.1011" ctype="SpriteObjectData">
            <Size X="228.0000" Y="83.0000" />
            <Children>
              <AbstractNodeData Name="title" ActionTag="-66316042" Tag="498" IconVisible="False" LeftMargin="59.1814" RightMargin="56.8186" TopMargin="-12.9878" BottomMargin="67.9878" FontSize="28" LabelText="本房输赢" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="112.0000" Y="28.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="115.1814" Y="81.9878" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="183" G="96" B="80" />
                <PrePosition X="0.5052" Y="0.9878" />
                <PreSize X="0.4912" Y="0.3373" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="score" ActionTag="-264481755" Tag="499" IconVisible="False" LeftMargin="60.5000" RightMargin="41.5000" TopMargin="25.0000" BottomMargin="22.0000" FontSize="36" LabelText="-300000" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="126.0000" Y="36.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="123.5000" Y="40.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="248" G="219" B="64" />
                <PrePosition X="0.5417" Y="0.4819" />
                <PreSize X="0.5526" Y="0.4337" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="134.3301" Y="-554.6011" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="bullfighting/res/table/menu_line2.png" Plist="bullfighting/res/table.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="day_score" ActionTag="279304464" Tag="500" IconVisible="False" LeftMargin="20.3301" RightMargin="-248.3301" TopMargin="612.4258" BottomMargin="-695.4258" ctype="SpriteObjectData">
            <Size X="228.0000" Y="83.0000" />
            <Children>
              <AbstractNodeData Name="title" ActionTag="1643851908" Tag="501" IconVisible="False" LeftMargin="59.1814" RightMargin="56.8186" TopMargin="-12.9877" BottomMargin="67.9877" FontSize="28" LabelText="今日输赢" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="112.0000" Y="28.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="115.1814" Y="81.9877" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="183" G="96" B="80" />
                <PrePosition X="0.5052" Y="0.9878" />
                <PreSize X="0.4912" Y="0.3373" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="score" ActionTag="-1208656321" Tag="502" IconVisible="False" LeftMargin="60.5000" RightMargin="41.5000" TopMargin="25.0000" BottomMargin="22.0000" FontSize="36" LabelText="-300000" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="126.0000" Y="36.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="123.5000" Y="40.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="248" G="219" B="64" />
                <PrePosition X="0.5417" Y="0.4819" />
                <PreSize X="0.5526" Y="0.4337" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="134.3301" Y="-653.9258" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="bullfighting/res/table/menu_line2.png" Plist="bullfighting/res/table.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="bank_btn" ActionTag="799513440" VisibleForFrame="False" Tag="180" IconVisible="False" LeftMargin="57.0000" RightMargin="-235.0000" TopMargin="130.3642" BottomMargin="-181.3642" ctype="SpriteObjectData">
            <Size X="178.0000" Y="51.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="146.0000" Y="-155.8642" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="bullfighting/res/table/menu_bank.png" Plist="bullfighting/res/table.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="stand_btn" ActionTag="1189497855" Tag="181" IconVisible="False" LeftMargin="56.0001" RightMargin="-212.0001" TopMargin="230.5318" BottomMargin="-276.5318" ctype="SpriteObjectData">
            <Size X="156.0000" Y="46.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="134.0001" Y="-253.5318" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="bullfighting/res/table/menu_stand.png" Plist="bullfighting/res/table.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="setting_btn" ActionTag="-920855420" Tag="182" IconVisible="False" LeftMargin="56.5001" RightMargin="-213.5001" TopMargin="129.6964" BottomMargin="-180.6964" ctype="SpriteObjectData">
            <Size X="157.0000" Y="51.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="135.0001" Y="-155.1964" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="bullfighting/res/table/menu_setting.png" Plist="bullfighting/res/table.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="back_btn" ActionTag="-745683953" Tag="183" IconVisible="False" LeftMargin="46.0000" RightMargin="-212.0000" TopMargin="30.5309" BottomMargin="-78.5309" ctype="SpriteObjectData">
            <Size X="166.0000" Y="48.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="129.0000" Y="-54.5309" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="bullfighting/res/table/menu_quit.png" Plist="bullfighting/res/table.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="line_0" ActionTag="-99876612" Tag="184" IconVisible="False" LeftMargin="26.5000" RightMargin="-237.5000" TopMargin="199.6667" BottomMargin="-201.6667" ctype="SpriteObjectData">
            <Size X="211.0000" Y="2.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="132.0000" Y="-200.6667" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="bullfighting/res/table/menu_line1.png" Plist="bullfighting/res/table.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="line_1" ActionTag="35304860" Tag="185" IconVisible="False" LeftMargin="26.5000" RightMargin="-237.5000" TopMargin="102.0000" BottomMargin="-104.0000" ctype="SpriteObjectData">
            <Size X="211.0000" Y="2.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="132.0000" Y="-103.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="bullfighting/res/table/menu_line1.png" Plist="bullfighting/res/table.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="line_2" ActionTag="-987452778" VisibleForFrame="False" Tag="186" IconVisible="False" LeftMargin="26.5000" RightMargin="-237.5000" TopMargin="297.3333" BottomMargin="-299.3333" ctype="SpriteObjectData">
            <Size X="211.0000" Y="2.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="132.0000" Y="-298.3333" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="bullfighting/res/table/menu_line1.png" Plist="bullfighting/res/table.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="menu_close" ActionTag="-47953788" Tag="64" IconVisible="False" LeftMargin="271.4870" RightMargin="-326.4870" TopMargin="269.3786" BottomMargin="-438.3786" ctype="SpriteObjectData">
            <Size X="55.0000" Y="169.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="298.9870" Y="-353.8786" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="bullfighting/res/table/menu_close.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>