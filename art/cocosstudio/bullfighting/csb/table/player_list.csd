<GameFile>
  <PropertyGroup Name="player_list" Type="Node" ID="a0d4f0fc-1db9-4e0d-adef-2afd957f4959" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="3783" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="mask" ActionTag="-1035608369" Alpha="0" Tag="3960" IconVisible="False" LeftMargin="-640.0000" RightMargin="-640.0000" TopMargin="-361.0000" BottomMargin="-359.0000" ClipAble="False" BackColorAlpha="102" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="1280.0000" Y="720.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position Y="1.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <SingleColor A="255" R="26" G="26" B="26" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="bg" ActionTag="1941148739" Tag="3962" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="-457.5000" RightMargin="-457.5000" TopMargin="-320.0000" BottomMargin="-320.0000" TouchEnable="True" LeftEage="305" RightEage="305" TopEage="180" BottomEage="180" Scale9OriginX="305" Scale9OriginY="180" Scale9Width="305" Scale9Height="216" ctype="ImageViewObjectData">
            <Size X="915.0000" Y="640.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="0.9000" ScaleY="0.9000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="bullfighting/res/dialog_bg.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="title" ActionTag="-1540478388" Tag="3787" IconVisible="False" LeftMargin="-76.9177" RightMargin="-67.0823" TopMargin="-257.3607" BottomMargin="221.3607" FontSize="36" LabelText="玩家列表" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="144.0000" Y="36.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-4.9177" Y="239.3607" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="230" B="208" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="containor" ActionTag="1631510671" Tag="997" IconVisible="True" LeftMargin="9.7648" RightMargin="-9.7648" TopMargin="22.4460" BottomMargin="-22.4460" ctype="SingleNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="9.7648" Y="-22.4460" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>