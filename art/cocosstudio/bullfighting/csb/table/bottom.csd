<GameFile>
  <PropertyGroup Name="bottom" Type="Node" ID="712254e8-57c2-44b9-b54d-4072029795ce" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="54" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="bg" ActionTag="1518175679" Tag="55" IconVisible="False" LeftMargin="-436.5000" RightMargin="-436.5000" TopMargin="-88.9996" BottomMargin="-0.0004" ctype="SpriteObjectData">
            <Size X="873.0000" Y="89.0000" />
            <AnchorPoint ScaleX="0.5000" />
            <Position Y="-0.0004" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="bullfighting/res/table/bottom_bg.png" Plist="bullfighting/res/table.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="bet_btn_1" ActionTag="-830500595" Tag="56" IconVisible="False" LeftMargin="-138.0934" RightMargin="27.0934" TopMargin="-112.7901" BottomMargin="-0.2099" ctype="SpriteObjectData">
            <Size X="111.0000" Y="113.0000" />
            <Children>
              <AbstractNodeData Name="bet_txt" ActionTag="-1310796485" Tag="423" IconVisible="False" LeftMargin="25.3662" RightMargin="26.6338" TopMargin="40.1405" BottomMargin="39.8595" LabelText="1234" ctype="TextBMFontObjectData">
                <Size X="59.0000" Y="33.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="54.8662" Y="56.3595" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="0" G="89" B="55" />
                <PrePosition X="0.4943" Y="0.4988" />
                <PreSize X="0.5315" Y="0.2920" />
                <LabelBMFontFile_CNB Type="Normal" Path="hall/res/chips_num.fnt" Plist="" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-82.5934" Y="56.2901" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="bullfighting/res/table/btn_bet_1.png" Plist="bullfighting/res/table.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="bet_btn_2" ActionTag="1632633517" Tag="65" IconVisible="False" LeftMargin="-3.3393" RightMargin="-107.6607" TopMargin="-112.7901" BottomMargin="-0.2099" ctype="SpriteObjectData">
            <Size X="111.0000" Y="113.0000" />
            <Children>
              <AbstractNodeData Name="bet_txt" ActionTag="744660436" Tag="424" IconVisible="False" LeftMargin="26.9324" RightMargin="25.0676" TopMargin="40.1405" BottomMargin="39.8595" LabelText="1234" ctype="TextBMFontObjectData">
                <Size X="59.0000" Y="33.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="56.4324" Y="56.3595" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="0" G="89" B="55" />
                <PrePosition X="0.5084" Y="0.4988" />
                <PreSize X="0.5315" Y="0.2920" />
                <LabelBMFontFile_CNB Type="Normal" Path="hall/res/chips_num.fnt" Plist="" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="52.1607" Y="56.2901" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="bullfighting/res/table/btn_bet_1.png" Plist="bullfighting/res/table.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="bet_btn_3" ActionTag="1949945287" Tag="67" IconVisible="False" LeftMargin="131.4149" RightMargin="-242.4149" TopMargin="-112.7901" BottomMargin="-0.2099" ctype="SpriteObjectData">
            <Size X="111.0000" Y="113.0000" />
            <Children>
              <AbstractNodeData Name="bet_txt" ActionTag="677806711" Tag="425" IconVisible="False" LeftMargin="28.4989" RightMargin="23.5011" TopMargin="40.1405" BottomMargin="39.8595" LabelText="1234" ctype="TextBMFontObjectData">
                <Size X="59.0000" Y="33.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="57.9989" Y="56.3595" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="0" G="89" B="55" />
                <PrePosition X="0.5225" Y="0.4988" />
                <PreSize X="0.5315" Y="0.2920" />
                <LabelBMFontFile_CNB Type="Normal" Path="hall/res/chips_num.fnt" Plist="" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="186.9149" Y="56.2901" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="bullfighting/res/table/btn_bet_1.png" Plist="bullfighting/res/table.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="bet_btn_4" ActionTag="-1270916642" Tag="69" IconVisible="False" LeftMargin="266.1690" RightMargin="-377.1690" TopMargin="-112.7901" BottomMargin="-0.2099" ctype="SpriteObjectData">
            <Size X="111.0000" Y="113.0000" />
            <Children>
              <AbstractNodeData Name="bet_txt" ActionTag="-1182304473" Tag="426" IconVisible="False" LeftMargin="27.3777" RightMargin="24.6223" TopMargin="40.1405" BottomMargin="39.8595" LabelText="1234" ctype="TextBMFontObjectData">
                <Size X="59.0000" Y="33.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="56.8777" Y="56.3595" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="0" G="89" B="55" />
                <PrePosition X="0.5124" Y="0.4988" />
                <PreSize X="0.5315" Y="0.2920" />
                <LabelBMFontFile_CNB Type="Normal" Path="hall/res/chips_num.fnt" Plist="" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="321.6690" Y="56.2901" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="bullfighting/res/table/btn_bet_1.png" Plist="bullfighting/res/table.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="head" ActionTag="-181501821" Tag="265" IconVisible="True" LeftMargin="-348.4272" RightMargin="348.4272" TopMargin="-42.1689" BottomMargin="42.1689" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="-348.4272" Y="42.1689" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="bullfighting/csb/table/player_self.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="banker_info" ActionTag="-455192339" Tag="2284" IconVisible="False" LeftMargin="-215.7401" RightMargin="-434.2599" TopMargin="-100.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="0" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="650.0000" Y="100.0000" />
            <Children>
              <AbstractNodeData Name="win_txt" ActionTag="1323623739" Tag="2285" IconVisible="False" LeftMargin="304.6052" RightMargin="254.3948" TopMargin="67.3042" BottomMargin="6.6958" FontSize="26" LabelText="9999999" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="91.0000" Y="26.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="304.6052" Y="19.6958" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="227" B="55" />
                <PrePosition X="0.4686" Y="0.1970" />
                <PreSize X="0.1400" Y="0.2600" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="coin_icon" ActionTag="-1613018324" Tag="2286" IconVisible="False" LeftMargin="261.1020" RightMargin="345.8980" TopMargin="58.8042" BottomMargin="-1.8042" ctype="SpriteObjectData">
                <Size X="43.0000" Y="43.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="282.6020" Y="19.6958" />
                <Scale ScaleX="0.5500" ScaleY="0.5500" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4348" Y="0.1970" />
                <PreSize X="0.0662" Y="0.4300" />
                <FileData Type="MarkedSubImage" Path="hall/res/first/coin_money.png" Plist="hall/res/first.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_3_1" ActionTag="850754990" Tag="2287" IconVisible="False" LeftMargin="163.2527" RightMargin="387.7473" TopMargin="69.3042" BottomMargin="8.6958" FontSize="22" LabelText="当前输赢:" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="99.0000" Y="22.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="212.7527" Y="19.6958" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="204" />
                <PrePosition X="0.3273" Y="0.1970" />
                <PreSize X="0.1523" Y="0.2200" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="leave_time" ActionTag="1995889298" Tag="2288" IconVisible="False" LeftMargin="423.7663" RightMargin="118.2338" TopMargin="23.9724" BottomMargin="58.0276" FontSize="18" LabelText="(16局后下庄)" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="108.0000" Y="18.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="423.7663" Y="67.0276" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="204" />
                <PrePosition X="0.6519" Y="0.6703" />
                <PreSize X="0.1662" Y="0.1800" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="total_time" ActionTag="1611530730" Tag="2289" IconVisible="False" LeftMargin="348.1957" RightMargin="241.8043" TopMargin="16.7506" BottomMargin="53.2494" FontSize="30" LabelText="20局" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="60.0000" Y="30.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="378.1957" Y="68.2494" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="227" B="55" />
                <PrePosition X="0.5818" Y="0.6825" />
                <PreSize X="0.0923" Y="0.3000" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_3_0" ActionTag="-854165077" Tag="2290" IconVisible="False" LeftMargin="220.9824" RightMargin="312.0176" TopMargin="19.3617" BottomMargin="54.6383" FontSize="26" LabelText="坐庄局数:" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="117.0000" Y="26.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="279.4824" Y="67.6383" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="204" />
                <PrePosition X="0.4300" Y="0.6764" />
                <PreSize X="0.1800" Y="0.2600" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="coin_txt" ActionTag="870725878" Tag="2291" IconVisible="False" LeftMargin="524.6052" RightMargin="34.3948" TopMargin="67.3042" BottomMargin="6.6958" FontSize="26" LabelText="9999999" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="91.0000" Y="26.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="524.6052" Y="19.6958" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="227" B="55" />
                <PrePosition X="0.8071" Y="0.1970" />
                <PreSize X="0.1400" Y="0.2600" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_3" ActionTag="-1034188935" Tag="2292" IconVisible="False" LeftMargin="443.2527" RightMargin="129.7473" TopMargin="69.3042" BottomMargin="8.6958" FontSize="22" LabelText="总带入:" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="77.0000" Y="22.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="481.7527" Y="19.6958" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="204" />
                <PrePosition X="0.7412" Y="0.1970" />
                <PreSize X="0.1185" Y="0.2200" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position X="-215.7401" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>