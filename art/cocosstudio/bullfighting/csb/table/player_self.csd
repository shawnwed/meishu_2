<GameFile>
  <PropertyGroup Name="player_self" Type="Node" ID="da7123eb-83bf-4051-a30d-2f68a4c93891" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="188" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="win" ActionTag="1223193734" Tag="1480" IconVisible="False" LeftMargin="-48.5000" RightMargin="-48.5000" TopMargin="-48.5000" BottomMargin="-48.5000" ctype="SpriteObjectData">
            <Size X="97.0000" Y="97.0000" />
            <Children>
              <AbstractNodeData Name="light" ActionTag="1064377946" Tag="1481" IconVisible="False" LeftMargin="-22.6886" RightMargin="-30.3114" TopMargin="-20.1559" BottomMargin="-32.8441" ctype="SpriteObjectData">
                <Size X="150.0000" Y="150.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="52.3114" Y="42.1559" />
                <Scale ScaleX="1.1391" ScaleY="1.1391" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5393" Y="0.4346" />
                <PreSize X="1.5464" Y="1.5464" />
                <FileData Type="MarkedSubImage" Path="bullfighting/res/table/win_light.png" Plist="bullfighting/res/table.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="0.7667" ScaleY="0.7667" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="bullfighting/res/table/win_frame.png" Plist="bullfighting/res/table.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="head" ActionTag="-2095433036" Tag="189" IconVisible="False" LeftMargin="-38.0000" RightMargin="-38.0000" TopMargin="-38.0000" BottomMargin="-38.0000" LeftEage="25" RightEage="25" TopEage="25" BottomEage="25" Scale9OriginX="25" Scale9OriginY="25" Scale9Width="30" Scale9Height="30" ctype="ImageViewObjectData">
            <Size X="76.0000" Y="76.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="bullfighting/res/table/seat_avatar.png" Plist="bullfighting/res/table.plist" />
          </AbstractNodeData>
          <AbstractNodeData Name="frame" ActionTag="1445185761" Tag="190" IconVisible="False" LeftMargin="-42.0000" RightMargin="-42.0000" TopMargin="-42.0000" BottomMargin="-42.0000" ctype="SpriteObjectData">
            <Size X="84.0000" Y="84.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="bullfighting/res/table/seat_avatar_frame.png" Plist="bullfighting/res/table.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="name" ActionTag="2057378791" Tag="191" IconVisible="False" LeftMargin="47.9790" RightMargin="-173.9790" TopMargin="-32.0044" BottomMargin="4.0044" FontSize="28" LabelText="your name" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="126.0000" Y="28.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="47.9790" Y="18.0044" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="204" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="coin_icon" ActionTag="1939168953" Tag="192" IconVisible="False" LeftMargin="38.4767" RightMargin="-81.4767" TopMargin="1.4955" BottomMargin="-44.4955" ctype="SpriteObjectData">
            <Size X="43.0000" Y="43.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="59.9767" Y="-22.9955" />
            <Scale ScaleX="0.6495" ScaleY="0.6495" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="hall/res/first/coin_money.png" Plist="hall/res/first.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="coin_txt" ActionTag="504418396" Tag="203" IconVisible="False" LeftMargin="76.9627" RightMargin="-167.9627" TopMargin="9.9956" BottomMargin="-35.9956" FontSize="26" LabelText="9999999" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="91.0000" Y="26.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="76.9627" Y="-22.9956" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="234" B="0" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>