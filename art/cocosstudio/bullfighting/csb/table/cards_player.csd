<GameFile>
  <PropertyGroup Name="cards_player" Type="Node" ID="4453ac4d-a870-4853-b98f-d88424a803e7" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="117" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="card_0" ActionTag="-426150186" Tag="215" IconVisible="True" LeftMargin="-56.0001" RightMargin="56.0001" TopMargin="42.0006" BottomMargin="-42.0006" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="-56.0001" Y="-42.0006" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="common/card.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="card_1" ActionTag="-936046118" Tag="221" IconVisible="True" LeftMargin="-27.6146" RightMargin="27.6146" TopMargin="42.0006" BottomMargin="-42.0006" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="-27.6146" Y="-42.0006" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="common/card.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="card_2" ActionTag="-1083838344" Tag="227" IconVisible="True" LeftMargin="0.7700" RightMargin="-0.7700" TopMargin="42.0006" BottomMargin="-42.0006" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="0.7700" Y="-42.0006" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="common/card.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="card_3" ActionTag="1843954681" Tag="233" IconVisible="True" LeftMargin="29.1559" RightMargin="-29.1559" TopMargin="42.0006" BottomMargin="-42.0006" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="29.1559" Y="-42.0006" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="common/card.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="card_4" ActionTag="-1671061138" Tag="239" IconVisible="True" LeftMargin="57.5413" RightMargin="-57.5413" TopMargin="42.0006" BottomMargin="-42.0006" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="57.5413" Y="-42.0006" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="common/card.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="cards_type" ActionTag="2097087314" Tag="245" IconVisible="False" LeftMargin="-86.0000" RightMargin="-86.0000" TopMargin="32.5999" BottomMargin="-84.5999" ctype="SpriteObjectData">
            <Size X="172.0000" Y="52.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position Y="-58.5999" />
            <Scale ScaleX="1.0000" ScaleY="0.9125" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="bullfighting/res/table/cards_type_bg.png" Plist="bullfighting/res/table.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="rate_txt" ActionTag="-31804607" Tag="710" IconVisible="False" LeftMargin="-78.9044" RightMargin="48.9044" TopMargin="96.6170" BottomMargin="-126.6170" FontSize="30" LabelText="x2" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="30.0000" Y="30.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-63.9044" Y="-111.6170" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="214" B="37" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="coin_txt" ActionTag="-2007301002" Tag="711" IconVisible="False" LeftMargin="-18.9086" RightMargin="-86.0914" TopMargin="98.6170" BottomMargin="-128.6170" FontSize="30" LabelText="1000000" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="105.0000" Y="30.0000" />
            <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
            <Position X="86.0914" Y="-113.6170" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="214" B="37" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="no_bet" ActionTag="1685364648" Tag="3467" IconVisible="False" LeftMargin="-35.5627" RightMargin="-52.4373" TopMargin="102.1681" BottomMargin="-124.1681" ctype="SpriteObjectData">
            <Size X="88.0000" Y="22.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="8.4373" Y="-113.1681" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="bullfighting/res/table/word_no_bet.png" Plist="bullfighting/res/table.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>