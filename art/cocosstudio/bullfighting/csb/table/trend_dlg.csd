<GameFile>
  <PropertyGroup Name="trend_dlg" Type="Node" ID="97fbdbae-0267-4626-b186-6a9f6144d75e" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="3774" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="mask" ActionTag="958326213" Alpha="0" Tag="3961" IconVisible="False" LeftMargin="-640.0000" RightMargin="-640.0000" TopMargin="-360.0000" BottomMargin="-360.0000" ClipAble="False" BackColorAlpha="102" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="1280.0000" Y="720.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <SingleColor A="255" R="26" G="26" B="26" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="bg" ActionTag="-808082714" Tag="3963" IconVisible="False" LeftMargin="-482.5000" RightMargin="-482.5000" TopMargin="-270.5000" BottomMargin="-270.5000" TouchEnable="True" Scale9Enable="True" LeftEage="305" RightEage="305" TopEage="180" BottomEage="180" Scale9OriginX="305" Scale9OriginY="180" Scale9Width="305" Scale9Height="216" ctype="ImageViewObjectData">
            <Size X="965.0000" Y="541.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="bullfighting/res/dialog_bg.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="light_bg" ActionTag="-1104438943" Tag="596" IconVisible="False" LeftMargin="-445.4998" RightMargin="344.4998" TopMargin="-169.6052" BottomMargin="-236.3948" ctype="SpriteObjectData">
            <Size X="101.0000" Y="406.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-394.9998" Y="-33.3948" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="bullfighting/res/table/bg_win_lose.png" Plist="bullfighting/res/table.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="Text_1" ActionTag="929332908" Tag="3776" IconVisible="False" LeftMargin="-80.0000" RightMargin="-80.0000" TopMargin="-239.9184" BottomMargin="199.9184" FontSize="40" LabelText="胜负走势" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="160.0000" Y="40.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position Y="219.9184" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="230" B="208" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="flag1" ActionTag="-707518856" Tag="3778" IconVisible="False" LeftMargin="-441.5790" RightMargin="347.5790" TopMargin="-178.0952" BottomMargin="87.0952" ctype="SpriteObjectData">
            <Size X="94.0000" Y="91.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-394.5790" Y="132.5952" />
            <Scale ScaleX="0.6000" ScaleY="0.6000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="bullfighting/res/table/bet_flag_0_0.png" Plist="bullfighting/res/table.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="flag2" ActionTag="1354735423" Tag="3779" IconVisible="False" LeftMargin="-442.0790" RightMargin="347.0790" TopMargin="-66.4808" BottomMargin="-19.5192" ctype="SpriteObjectData">
            <Size X="95.0000" Y="86.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-394.5790" Y="23.4808" />
            <Scale ScaleX="0.6000" ScaleY="0.6000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="bullfighting/res/table/bet_flag_1_0.png" Plist="bullfighting/res/table.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="flag3" ActionTag="1818455127" Tag="3780" IconVisible="False" LeftMargin="-439.0791" RightMargin="350.0791" TopMargin="38.4582" BottomMargin="-127.4582" ctype="SpriteObjectData">
            <Size X="89.0000" Y="89.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-394.5791" Y="-82.9582" />
            <Scale ScaleX="0.6000" ScaleY="0.6000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="bullfighting/res/table/bet_flag_2_0.png" Plist="bullfighting/res/table.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="flag4" ActionTag="-1384150221" Tag="3781" IconVisible="False" LeftMargin="-435.0796" RightMargin="354.0796" TopMargin="152.8968" BottomMargin="-239.8968" ctype="SpriteObjectData">
            <Size X="81.0000" Y="87.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-394.5796" Y="-196.3968" />
            <Scale ScaleX="0.6000" ScaleY="0.6000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="bullfighting/res/table/bet_flag_3_0.png" Plist="bullfighting/res/table.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>