<GameFile>
  <PropertyGroup Name="total_bet_bar" Type="Node" ID="bef1d513-f4a1-47f9-bdd4-2fb08bf21cc8" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="1902" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="bg" ActionTag="-1694141943" Tag="1904" IconVisible="False" LeftMargin="-266.5000" RightMargin="-266.5000" TopMargin="-21.5000" BottomMargin="-21.5000" ctype="SpriteObjectData">
            <Size X="533.0000" Y="43.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="bullfighting/res/table/slider_track2.png" Plist="bullfighting/res/table.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="bar" ActionTag="1763410936" Tag="1903" IconVisible="False" LeftMargin="-266.5000" RightMargin="-266.5000" TopMargin="-21.5004" BottomMargin="-21.4996" ProgressInfo="100" ctype="LoadingBarObjectData">
            <Size X="533.0000" Y="43.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position Y="0.0004" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <ImageFileData Type="MarkedSubImage" Path="bullfighting/res/table/slider_fill2.png" Plist="bullfighting/res/table.plist" />
          </AbstractNodeData>
          <AbstractNodeData Name="Text_1" ActionTag="-1906577210" Tag="1906" IconVisible="False" LeftMargin="-98.1273" RightMargin="14.1273" TopMargin="-12.1359" BottomMargin="-11.8641" FontSize="24" LabelText="可下注:" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="84.0000" Y="24.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="-98.1273" Y="0.1359" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="score_txt" ActionTag="287852051" Tag="1907" IconVisible="False" LeftMargin="-10.5829" RightMargin="-80.4171" TopMargin="-13.1407" BottomMargin="-12.8593" FontSize="26" LabelText="1888888" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="91.0000" Y="26.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="-10.5829" Y="0.1407" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="208" G="255" B="111" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>