<GameFile>
  <PropertyGroup Name="top" Type="Node" ID="35b1c859-b65e-48c4-85fe-ead3e08796e9" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="41" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="bg" ActionTag="1128151891" Tag="42" IconVisible="False" LeftMargin="-298.5605" RightMargin="-295.4395" TopMargin="1.3238" BottomMargin="-98.3238" ctype="SpriteObjectData">
            <Size X="594.0000" Y="97.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-1.5605" Y="-49.8238" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="bullfighting/res/table/top_bg.png" Plist="bullfighting/res/table.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="btn_banker" ActionTag="180200772" Tag="43" IconVisible="False" LeftMargin="135.4570" RightMargin="-275.4570" TopMargin="27.3022" BottomMargin="-86.3022" ctype="SpriteObjectData">
            <Size X="140.0000" Y="59.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="205.4570" Y="-56.8022" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="bullfighting/res/table/btn_banker_1.png" Plist="bullfighting/res/table.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="logo" ActionTag="-1138380326" Tag="44" IconVisible="False" LeftMargin="-38.2001" RightMargin="-126.7999" TopMargin="5.6940" BottomMargin="-88.6940" ctype="SpriteObjectData">
            <Size X="165.0000" Y="83.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="44.2999" Y="-47.1940" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="bullfighting/res/table/top_cards_bg.png" Plist="bullfighting/res/table.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="head" ActionTag="-2127852391" Tag="276" IconVisible="True" LeftMargin="-224.2817" RightMargin="224.2817" TopMargin="50.8590" BottomMargin="-50.8590" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="-224.2817" Y="-50.8590" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="bullfighting/csb/table/player_self.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="banker" ActionTag="-1615338517" Tag="283" IconVisible="False" LeftMargin="-278.3628" RightMargin="237.3628" TopMargin="3.2227" BottomMargin="-45.2227" ctype="SpriteObjectData">
            <Size X="41.0000" Y="42.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-257.8628" Y="-24.2227" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="bullfighting/res/table/banker_flag.png" Plist="bullfighting/res/table.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="cards" ActionTag="-2068952451" Tag="1928" IconVisible="True" LeftMargin="43.7790" RightMargin="-43.7790" TopMargin="12.5625" BottomMargin="-12.5625" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="43.7790" Y="-12.5625" />
            <Scale ScaleX="0.8500" ScaleY="0.8500" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="bullfighting/csb/table/cards_player.csd" Plist="" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>