<GameFile>
  <PropertyGroup Name="bet_node" Type="Node" ID="8af47ef2-64c3-4559-b712-295259b472ac" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="83" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="bg" ActionTag="190807637" Tag="84" IconVisible="False" LeftMargin="-97.0000" RightMargin="-97.0000" TopMargin="-187.0000" ctype="SpriteObjectData">
            <Size X="194.0000" Y="187.0000" />
            <AnchorPoint ScaleX="0.5000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="bullfighting/res/table/bet_area.png" Plist="bullfighting/res/table.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="flag" ActionTag="-1960878708" Tag="85" IconVisible="False" LeftMargin="-41.2165" RightMargin="-39.7835" TopMargin="-135.4615" BottomMargin="48.4615" ctype="SpriteObjectData">
            <Size X="81.0000" Y="87.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-0.7165" Y="91.9615" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="bullfighting/res/table/bet_flag_3_0.png" Plist="bullfighting/res/table.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="self_bet_bg" ActionTag="-649273639" Tag="86" IconVisible="False" LeftMargin="-94.5009" RightMargin="-95.4991" TopMargin="-40.0996" BottomMargin="4.0996" ctype="SpriteObjectData">
            <Size X="190.0000" Y="36.0000" />
            <Children>
              <AbstractNodeData Name="bet_txt" ActionTag="13406251" Tag="87" IconVisible="False" LeftMargin="58.9544" RightMargin="59.0456" TopMargin="1.4950" BottomMargin="-1.4950" FontSize="36" LabelText="9000" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="72.0000" Y="36.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="94.9544" Y="16.5050" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="183" G="255" B="147" />
                <PrePosition X="0.4998" Y="0.4585" />
                <PreSize X="0.3789" Y="1.0000" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="0.4991" Y="22.0996" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="bullfighting/res/table/bet_self_bg.png" Plist="bullfighting/res/table.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="light" ActionTag="-2102443683" Tag="378" IconVisible="False" LeftMargin="-110.1075" RightMargin="-108.8925" TopMargin="-199.9212" BottomMargin="-12.0788" ctype="SpriteObjectData">
            <Size X="219.0000" Y="212.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-0.6075" Y="93.9212" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="bullfighting/res/table/light_frame.png" Plist="bullfighting/res/table.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="total_bet_txt" ActionTag="1600056025" Tag="1462" IconVisible="False" LeftMargin="-45.0002" RightMargin="-44.9998" TopMargin="-187.4019" BottomMargin="151.4019" FontSize="36" LabelText="90000" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="90.0000" Y="36.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-0.0002" Y="169.4019" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="240" B="78" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>