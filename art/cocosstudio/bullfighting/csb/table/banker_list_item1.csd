<GameFile>
  <PropertyGroup Name="banker_list_item1" Type="Node" ID="a8763c16-101f-42ea-8275-8d00bcf3fbae" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="3994" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="bg" ActionTag="1382883809" Tag="4002" IconVisible="False" LeftMargin="-0.8171" RightMargin="-704.1829" TopMargin="-93.6464" BottomMargin="-0.3536" Scale9Enable="True" LeftEage="85" RightEage="85" TopEage="31" BottomEage="31" Scale9OriginX="85" Scale9OriginY="31" Scale9Width="91" Scale9Height="32" ctype="ImageViewObjectData">
            <Size X="705.0000" Y="94.0000" />
            <AnchorPoint />
            <Position X="-0.8171" Y="-0.3536" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="bullfighting/res/table/bg_user.png" Plist="bullfighting/res/table.plist" />
          </AbstractNodeData>
          <AbstractNodeData Name="head" ActionTag="-1051353361" Tag="4000" IconVisible="False" LeftMargin="6.1830" RightMargin="-82.1830" TopMargin="-86.6465" BottomMargin="10.6465" LeftEage="25" RightEage="25" TopEage="25" BottomEage="25" Scale9OriginX="25" Scale9OriginY="25" Scale9Width="30" Scale9Height="30" ctype="ImageViewObjectData">
            <Size X="76.0000" Y="76.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="44.1830" Y="48.6465" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="bullfighting/res/table/seat_avatar.png" Plist="bullfighting/res/table.plist" />
          </AbstractNodeData>
          <AbstractNodeData Name="frame" ActionTag="239499095" Tag="3999" IconVisible="False" LeftMargin="2.1832" RightMargin="-86.1832" TopMargin="-90.6467" BottomMargin="6.6467" ctype="SpriteObjectData">
            <Size X="84.0000" Y="84.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="44.1832" Y="48.6467" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="bullfighting/res/table/seat_avatar_frame.png" Plist="bullfighting/res/table.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="name" ActionTag="-131472888" Tag="4001" IconVisible="False" LeftMargin="99.1800" RightMargin="-249.1800" TopMargin="-82.1463" BottomMargin="57.1463" FontSize="25" LabelText="那么多为什么" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="150.0000" Y="25.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="99.1800" Y="69.6463" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="81" G="70" B="69" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="level" ActionTag="-1175032838" Tag="4043" IconVisible="False" LeftMargin="99.1831" RightMargin="-155.1831" TopMargin="-45.1466" BottomMargin="12.1466" ctype="SpriteObjectData">
            <Size X="56.0000" Y="33.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="99.1831" Y="28.6466" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="hall/res/level/level1.png" Plist="hall/res/level.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="coin_txt" ActionTag="-360493380" Tag="66" IconVisible="False" LeftMargin="326.4776" RightMargin="-471.4776" TopMargin="-59.3182" BottomMargin="26.3182" LabelText="540.32" ctype="TextBMFontObjectData">
            <Size X="145.0000" Y="33.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="326.4776" Y="42.8182" />
            <Scale ScaleX="0.8800" ScaleY="0.8800" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <LabelBMFontFile_CNB Type="Normal" Path="hall/res/rank_num.fnt" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="coin_icon" ActionTag="-2002107417" Tag="4053" IconVisible="False" LeftMargin="274.9639" RightMargin="-317.9639" TopMargin="-67.0231" BottomMargin="24.0231" ctype="SpriteObjectData">
            <Size X="43.0000" Y="43.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="296.4639" Y="45.5231" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="hall/res/first/coin_money.png" Plist="hall/res/first.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="jump_btn" ActionTag="999112704" Tag="4054" IconVisible="False" LeftMargin="516.6829" RightMargin="-681.6829" TopMargin="-61.1466" BottomMargin="2.1466" ctype="SpriteObjectData">
            <Size X="165.0000" Y="59.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="599.1829" Y="31.6466" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="bullfighting/res/table/btn_jump.png" Plist="bullfighting/res/table.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="jump_txt" ActionTag="1945742944" Tag="4087" IconVisible="False" LeftMargin="552.1835" RightMargin="-646.1835" TopMargin="-87.1467" BottomMargin="70.1467" FontSize="17" LabelText="消耗1万金币" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="94.0000" Y="17.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="599.1835" Y="78.6467" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="81" G="70" B="69" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>