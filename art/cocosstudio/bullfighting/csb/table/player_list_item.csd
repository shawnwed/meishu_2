<GameFile>
  <PropertyGroup Name="player_list_item" Type="Node" ID="0278a8e0-8aed-4a15-9143-c89c4d365162" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="3903" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="Image_1" ActionTag="589826508" Tag="925" IconVisible="False" RightMargin="-120.0000" TopMargin="-120.0000" Scale9Enable="True" LeftEage="85" RightEage="85" TopEage="34" BottomEage="37" Scale9OriginX="85" Scale9OriginY="34" Scale9Width="91" Scale9Height="23" ctype="ImageViewObjectData">
            <Size X="120.0000" Y="120.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position Y="60.0000" />
            <Scale ScaleX="0.8600" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="bullfighting/res/table/bg_user.png" Plist="bullfighting/res/table.plist" />
          </AbstractNodeData>
          <AbstractNodeData Name="head" ActionTag="1113170595" Tag="3905" IconVisible="False" LeftMargin="13.1919" RightMargin="-89.1919" TopMargin="-111.4540" BottomMargin="35.4540" LeftEage="25" RightEage="25" TopEage="25" BottomEage="25" Scale9OriginX="25" Scale9OriginY="25" Scale9Width="30" Scale9Height="30" ctype="ImageViewObjectData">
            <Size X="76.0000" Y="76.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="51.1919" Y="73.4540" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="bullfighting/res/table/seat_avatar.png" Plist="bullfighting/res/table.plist" />
          </AbstractNodeData>
          <AbstractNodeData Name="frame" ActionTag="668554122" Tag="3906" IconVisible="False" LeftMargin="8.1924" RightMargin="-92.1924" TopMargin="-113.4541" BottomMargin="29.4541" ctype="SpriteObjectData">
            <Size X="84.0000" Y="84.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="50.1924" Y="71.4541" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="bullfighting/res/table/seat_avatar_frame.png" Plist="bullfighting/res/table.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="name" ActionTag="-1071807007" Tag="3907" IconVisible="False" LeftMargin="2.9404" RightMargin="-102.9404" TopMargin="-24.1453" BottomMargin="4.1453" IsCustomSize="True" FontSize="20" LabelText="那么多为什么" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="100.0000" Y="20.0000" />
            <AnchorPoint ScaleY="0.4869" />
            <Position X="2.9404" Y="13.8824" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="165" G="42" B="42" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>