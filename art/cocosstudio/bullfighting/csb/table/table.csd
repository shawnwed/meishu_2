<GameFile>
  <PropertyGroup Name="table" Type="Scene" ID="4ac3f67d-3034-4a6c-a08d-a1c287212a50" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Scene" Tag="37" ctype="GameNodeObjectData">
        <Size X="960.0000" Y="640.0000" />
        <Children>
          <AbstractNodeData Name="background" ActionTag="-2014346228" Tag="39" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" LeftEage="348" RightEage="348" TopEage="198" BottomEage="198" Scale9OriginX="-302" Scale9OriginY="-152" Scale9Width="650" Scale9Height="350" ctype="ImageViewObjectData">
            <Size X="960.0000" Y="640.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="480.0000" Y="320.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="1.0000" Y="1.0000" />
            <FileData Type="Normal" Path="bullfighting/res/bg.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="table_bg" CanEdit="False" ActionTag="-840615864" Tag="40" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-107.5000" RightMargin="-107.5000" TopMargin="5.2080" BottomMargin="24.7920" ctype="SpriteObjectData">
            <Size X="1175.0000" Y="610.0000" />
            <Children>
              <AbstractNodeData Name="time_txt" ActionTag="1659337160" Tag="2877" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="496.7900" RightMargin="510.2100" TopMargin="107.1460" BottomMargin="474.8540" FontSize="28" LabelText="下注时间: 36" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="168.0000" Y="28.0000" />
                <Children>
                  <AbstractNodeData Name="Image_2" ActionTag="1720382079" Alpha="47" Tag="989" IconVisible="False" LeftMargin="-21.9588" RightMargin="-32.0412" TopMargin="-2.1572" BottomMargin="-3.8428" LeftEage="73" RightEage="73" TopEage="11" BottomEage="11" Scale9OriginX="73" Scale9OriginY="11" Scale9Width="76" Scale9Height="12" ctype="ImageViewObjectData">
                    <Size X="222.0000" Y="34.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="89.0412" Y="13.1572" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="246" B="192" />
                    <PrePosition X="0.5300" Y="0.4699" />
                    <PreSize X="1.3214" Y="1.2143" />
                    <FileData Type="Normal" Path="bullfighting/res/table/bg_user12.png" Plist="" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleY="0.5000" />
                <Position X="496.7900" Y="488.8540" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="249" B="219" />
                <PrePosition X="0.4228" Y="0.8014" />
                <PreSize X="0.1430" Y="0.0459" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="480.0000" Y="329.7920" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5153" />
            <PreSize X="1.2240" Y="0.9531" />
            <FileData Type="Normal" Path="bullfighting/res/table_bg.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="top" ActionTag="-403268706" Tag="45" IconVisible="True" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="480.0000" RightMargin="480.0000" BottomMargin="640.0000" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="480.0000" Y="640.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="1.0000" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="bullfighting/csb/table/top.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="bottom" ActionTag="-1661529776" Tag="71" IconVisible="True" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="480.0000" RightMargin="480.0000" TopMargin="640.0000" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="480.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="bullfighting/csb/table/bottom.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="btn_chat" ActionTag="722406603" Tag="50" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="877.5000" RightMargin="13.5000" TopMargin="558.0120" BottomMargin="12.9880" ctype="SpriteObjectData">
            <Size X="69.0000" Y="69.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="912.0000" Y="47.4880" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.9500" Y="0.0742" />
            <PreSize X="0.0719" Y="0.1078" />
            <FileData Type="MarkedSubImage" Path="bullfighting/res/table/btn_chat.png" Plist="bullfighting/res/table.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="btn_menu" ActionTag="41219309" Tag="51" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="5.1480" RightMargin="885.8520" TopMargin="11.9000" BottomMargin="559.1000" ctype="SpriteObjectData">
            <Size X="69.0000" Y="69.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="39.6480" Y="593.6000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0413" Y="0.9275" />
            <PreSize X="0.0719" Y="0.1078" />
            <FileData Type="MarkedSubImage" Path="bullfighting/res/table/btn_menu.png" Plist="bullfighting/res/table.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="btn_record" ActionTag="-411861834" Tag="52" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="886.6200" RightMargin="4.3800" TopMargin="10.1080" BottomMargin="560.8920" ctype="SpriteObjectData">
            <Size X="69.0000" Y="69.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="921.1200" Y="595.3920" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.9595" Y="0.9303" />
            <PreSize X="0.0719" Y="0.1078" />
            <FileData Type="MarkedSubImage" Path="bullfighting/res/table/btn_record.png" Plist="bullfighting/res/table.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="btn_players" ActionTag="-1770867906" Tag="53" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="11.2920" RightMargin="879.7080" TopMargin="560.7000" BottomMargin="10.3000" ctype="SpriteObjectData">
            <Size X="69.0000" Y="69.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="45.7920" Y="44.8000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0477" Y="0.0700" />
            <PreSize X="0.0719" Y="0.1078" />
            <FileData Type="MarkedSubImage" Path="bullfighting/res/table/btn_players.png" Plist="bullfighting/res/table.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="bet_node_0" ActionTag="1057470186" Tag="89" IconVisible="True" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="238.4640" RightMargin="721.5360" TopMargin="339.3920" BottomMargin="300.6080" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="238.4640" Y="300.6080" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.2484" Y="0.4697" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="bullfighting/csb/table/bet_node.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="bet_node_1" ActionTag="1793823007" Tag="96" IconVisible="True" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="402.0480" RightMargin="557.9520" TopMargin="339.3920" BottomMargin="300.6080" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="402.0480" Y="300.6080" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4188" Y="0.4697" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="bullfighting/csb/table/bet_node.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="bet_node_2" ActionTag="1227076422" Tag="103" IconVisible="True" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="565.6320" RightMargin="394.3680" TopMargin="339.3920" BottomMargin="300.6080" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="565.6320" Y="300.6080" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5892" Y="0.4697" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="bullfighting/csb/table/bet_node.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="bet_node_3" ActionTag="223155827" Tag="110" IconVisible="True" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="729.2160" RightMargin="230.7840" TopMargin="339.3920" BottomMargin="300.6080" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="729.2160" Y="300.6080" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.7596" Y="0.4697" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="bullfighting/csb/table/bet_node.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="cards_1" ActionTag="-1296613917" Tag="246" IconVisible="True" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="238.4640" RightMargin="721.5360" TopMargin="360.8960" BottomMargin="279.1040" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="238.4640" Y="279.1040" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.2484" Y="0.4361" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="bullfighting/csb/table/cards_player.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="cards_2" ActionTag="-1399155765" Tag="279" IconVisible="True" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="401.8560" RightMargin="558.1440" TopMargin="360.8960" BottomMargin="279.1040" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="401.8560" Y="279.1040" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4186" Y="0.4361" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="bullfighting/csb/table/cards_player.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="cards_3" ActionTag="-564920430" Tag="312" IconVisible="True" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="565.2480" RightMargin="394.7520" TopMargin="360.8960" BottomMargin="279.1040" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="565.2480" Y="279.1040" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5888" Y="0.4361" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="bullfighting/csb/table/cards_player.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="cards_4" ActionTag="-1443324011" Tag="345" IconVisible="True" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="728.6400" RightMargin="231.3600" TopMargin="360.8960" BottomMargin="279.1040" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="728.6400" Y="279.1040" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.7590" Y="0.4361" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="bullfighting/csb/table/cards_player.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="seat_0" ActionTag="-1055001859" Tag="630" IconVisible="True" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="115.2000" RightMargin="844.8000" TopMargin="128.0000" BottomMargin="512.0000" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="115.2000" Y="512.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.1200" Y="0.8000" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="bullfighting/csb/table/seat.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="seat_1" ActionTag="2019554798" Tag="636" IconVisible="True" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="48.0000" RightMargin="912.0000" TopMargin="250.5600" BottomMargin="389.4400" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="48.0000" Y="389.4400" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0500" Y="0.6085" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="bullfighting/csb/table/seat.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="seat_2" ActionTag="1716319930" Tag="642" IconVisible="True" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="48.0000" RightMargin="912.0000" TopMargin="373.0560" BottomMargin="266.9440" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="48.0000" Y="266.9440" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0500" Y="0.4171" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="bullfighting/csb/table/seat.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="seat_3" ActionTag="1626912342" Tag="648" IconVisible="True" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="115.2000" RightMargin="844.8000" TopMargin="495.6160" BottomMargin="144.3840" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="115.2000" Y="144.3840" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.1200" Y="0.2256" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="bullfighting/csb/table/seat.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="seat_7" ActionTag="-818919631" Tag="654" IconVisible="True" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="844.8000" RightMargin="115.2000" TopMargin="128.0000" BottomMargin="512.0000" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="844.8000" Y="512.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8800" Y="0.8000" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="bullfighting/csb/table/seat.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="seat_6" ActionTag="243441931" Tag="660" IconVisible="True" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="912.0000" RightMargin="48.0000" TopMargin="250.5600" BottomMargin="389.4400" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="912.0000" Y="389.4400" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.9500" Y="0.6085" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="bullfighting/csb/table/seat.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="seat_5" ActionTag="-250905170" Tag="666" IconVisible="True" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="912.0000" RightMargin="48.0000" TopMargin="373.0560" BottomMargin="266.9440" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="912.0000" Y="266.9440" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.9500" Y="0.4171" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="bullfighting/csb/table/seat.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="seat_4" ActionTag="-1802574129" Tag="672" IconVisible="True" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="844.8000" RightMargin="115.2000" TopMargin="495.6160" BottomMargin="144.3840" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="844.8000" Y="144.3840" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8800" Y="0.2256" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="bullfighting/csb/table/seat.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="chat_bubble_1" ActionTag="412131859" Tag="1682" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="157.9200" RightMargin="707.0800" TopMargin="68.0000" BottomMargin="512.0000" Scale9Enable="True" LeftEage="31" RightEage="31" TopEage="19" BottomEage="19" Scale9OriginX="31" Scale9OriginY="19" Scale9Width="33" Scale9Height="22" ctype="ImageViewObjectData">
            <Size X="95.0000" Y="60.0000" />
            <AnchorPoint />
            <Position X="157.9200" Y="512.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.1645" Y="0.8000" />
            <PreSize X="0.0990" Y="0.0938" />
            <FileData Type="MarkedSubImage" Path="bullfighting/res/table/bull_chat_bubble.png" Plist="bullfighting/res/table.plist" />
          </AbstractNodeData>
          <AbstractNodeData Name="chat_bubble_2" ActionTag="-90343950" Tag="1683" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="91.1040" RightMargin="773.8960" TopMargin="190.6880" BottomMargin="389.3120" Scale9Enable="True" LeftEage="31" RightEage="31" TopEage="19" BottomEage="19" Scale9OriginX="31" Scale9OriginY="19" Scale9Width="33" Scale9Height="22" ctype="ImageViewObjectData">
            <Size X="95.0000" Y="60.0000" />
            <AnchorPoint />
            <Position X="91.1040" Y="389.3120" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0949" Y="0.6083" />
            <PreSize X="0.0990" Y="0.0938" />
            <FileData Type="MarkedSubImage" Path="bullfighting/res/table/bull_chat_bubble.png" Plist="bullfighting/res/table.plist" />
          </AbstractNodeData>
          <AbstractNodeData Name="chat_bubble_3" ActionTag="359890722" Tag="1684" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="91.1040" RightMargin="773.8960" TopMargin="315.1040" BottomMargin="264.8960" Scale9Enable="True" LeftEage="31" RightEage="31" TopEage="19" BottomEage="19" Scale9OriginX="31" Scale9OriginY="19" Scale9Width="33" Scale9Height="22" ctype="ImageViewObjectData">
            <Size X="95.0000" Y="60.0000" />
            <AnchorPoint />
            <Position X="91.1040" Y="264.8960" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0949" Y="0.4139" />
            <PreSize X="0.0990" Y="0.0938" />
            <FileData Type="MarkedSubImage" Path="bullfighting/res/table/bull_chat_bubble.png" Plist="bullfighting/res/table.plist" />
          </AbstractNodeData>
          <AbstractNodeData Name="chat_bubble_4" ActionTag="-1556792007" Tag="1685" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="157.9200" RightMargin="707.0800" TopMargin="436.8960" BottomMargin="143.1040" Scale9Enable="True" LeftEage="31" RightEage="31" TopEage="19" BottomEage="19" Scale9OriginX="31" Scale9OriginY="19" Scale9Width="33" Scale9Height="22" ctype="ImageViewObjectData">
            <Size X="95.0000" Y="60.0000" />
            <AnchorPoint />
            <Position X="157.9200" Y="143.1040" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.1645" Y="0.2236" />
            <PreSize X="0.0990" Y="0.0938" />
            <FileData Type="MarkedSubImage" Path="bullfighting/res/table/bull_chat_bubble.png" Plist="bullfighting/res/table.plist" />
          </AbstractNodeData>
          <AbstractNodeData Name="chat_bubble_8" ActionTag="-1738727902" Tag="1686" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="800.8320" RightMargin="64.1680" TopMargin="68.0000" BottomMargin="512.0000" FlipX="True" Scale9Enable="True" LeftEage="31" RightEage="31" TopEage="19" BottomEage="19" Scale9OriginX="31" Scale9OriginY="19" Scale9Width="33" Scale9Height="22" ctype="ImageViewObjectData">
            <Size X="95.0000" Y="60.0000" />
            <AnchorPoint />
            <Position X="800.8320" Y="512.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8342" Y="0.8000" />
            <PreSize X="0.0990" Y="0.0938" />
            <FileData Type="MarkedSubImage" Path="bullfighting/res/table/bull_chat_bubble.png" Plist="bullfighting/res/table.plist" />
          </AbstractNodeData>
          <AbstractNodeData Name="chat_bubble_7" ActionTag="1211807336" Tag="1687" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="870.6240" RightMargin="-5.6240" TopMargin="190.6880" BottomMargin="389.3120" FlipX="True" Scale9Enable="True" LeftEage="31" RightEage="31" TopEage="19" BottomEage="19" Scale9OriginX="31" Scale9OriginY="19" Scale9Width="33" Scale9Height="22" ctype="ImageViewObjectData">
            <Size X="95.0000" Y="60.0000" />
            <AnchorPoint />
            <Position X="870.6240" Y="389.3120" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.9069" Y="0.6083" />
            <PreSize X="0.0990" Y="0.0938" />
            <FileData Type="MarkedSubImage" Path="bullfighting/res/table/bull_chat_bubble.png" Plist="bullfighting/res/table.plist" />
          </AbstractNodeData>
          <AbstractNodeData Name="chat_bubble_6" ActionTag="682787092" Tag="1688" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="870.6240" RightMargin="-5.6240" TopMargin="315.1040" BottomMargin="264.8960" FlipX="True" Scale9Enable="True" LeftEage="31" RightEage="31" TopEage="19" BottomEage="19" Scale9OriginX="31" Scale9OriginY="19" Scale9Width="33" Scale9Height="22" ctype="ImageViewObjectData">
            <Size X="95.0000" Y="60.0000" />
            <AnchorPoint />
            <Position X="870.6240" Y="264.8960" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.9069" Y="0.4139" />
            <PreSize X="0.0990" Y="0.0938" />
            <FileData Type="MarkedSubImage" Path="bullfighting/res/table/bull_chat_bubble.png" Plist="bullfighting/res/table.plist" />
          </AbstractNodeData>
          <AbstractNodeData Name="chat_bubble_5" ActionTag="-1784686099" Tag="1689" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="800.8320" RightMargin="64.1680" TopMargin="436.8960" BottomMargin="143.1040" FlipX="True" Scale9Enable="True" LeftEage="31" RightEage="31" TopEage="19" BottomEage="19" Scale9OriginX="31" Scale9OriginY="19" Scale9Width="33" Scale9Height="22" ctype="ImageViewObjectData">
            <Size X="95.0000" Y="60.0000" />
            <AnchorPoint />
            <Position X="800.8320" Y="143.1040" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8342" Y="0.2236" />
            <PreSize X="0.0990" Y="0.0938" />
            <FileData Type="MarkedSubImage" Path="bullfighting/res/table/bull_chat_bubble.png" Plist="bullfighting/res/table.plist" />
          </AbstractNodeData>
          <AbstractNodeData Name="chat_bubble_9" ActionTag="124254679" Tag="2783" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="26.1120" RightMargin="838.8880" TopMargin="497.2480" BottomMargin="82.7520" Scale9Enable="True" LeftEage="31" RightEage="31" TopEage="19" BottomEage="19" Scale9OriginX="31" Scale9OriginY="19" Scale9Width="33" Scale9Height="22" ctype="ImageViewObjectData">
            <Size X="95.0000" Y="60.0000" />
            <AnchorPoint />
            <Position X="26.1120" Y="82.7520" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0272" Y="0.1293" />
            <PreSize X="0.0990" Y="0.0938" />
            <FileData Type="MarkedSubImage" Path="bullfighting/res/table/bull_chat_bubble.png" Plist="bullfighting/res/table.plist" />
          </AbstractNodeData>
          <AbstractNodeData Name="chat_bubble_10" ActionTag="-189072075" Tag="907" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="157.9200" RightMargin="707.0800" TopMargin="500.8320" BottomMargin="79.1680" Scale9Enable="True" LeftEage="31" RightEage="31" TopEage="19" BottomEage="19" Scale9OriginX="31" Scale9OriginY="19" Scale9Width="33" Scale9Height="22" ctype="ImageViewObjectData">
            <Size X="95.0000" Y="60.0000" />
            <AnchorPoint />
            <Position X="157.9200" Y="79.1680" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.1645" Y="0.1237" />
            <PreSize X="0.0990" Y="0.0938" />
            <FileData Type="MarkedSubImage" Path="bullfighting/res/table/bull_chat_bubble.png" Plist="bullfighting/res/table.plist" />
          </AbstractNodeData>
          <AbstractNodeData Name="top_info" ActionTag="509534385" Tag="339" IconVisible="True" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="144.0000" RightMargin="816.0000" TopMargin="6.4000" BottomMargin="633.6000" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="144.0000" Y="633.6000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.1500" Y="0.9900" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="game_public/csb/top_info.csd" Plist="" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>