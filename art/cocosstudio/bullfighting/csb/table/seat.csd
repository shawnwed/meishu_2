<GameFile>
  <PropertyGroup Name="seat" Type="Node" ID="db239171-8d45-44da-8449-5cce9ea642a1" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="469" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="bg" ActionTag="-206664660" Tag="470" IconVisible="False" LeftMargin="-69.0000" RightMargin="-63.0000" TopMargin="-62.9998" BottomMargin="-63.0002" ctype="SpriteObjectData">
            <Size X="132.0000" Y="126.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-3.0000" Y="-0.0002" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="bullfighting/res/table/seat_bg.png" Plist="bullfighting/res/table.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="win" ActionTag="444169200" Tag="1497" IconVisible="False" LeftMargin="-46.4160" RightMargin="-50.5840" TopMargin="-61.1650" BottomMargin="-35.8350" ctype="SpriteObjectData">
            <Size X="97.0000" Y="97.0000" />
            <Children>
              <AbstractNodeData Name="light" ActionTag="-69431199" Tag="1498" IconVisible="False" LeftMargin="-25.4897" RightMargin="-27.5103" TopMargin="-25.2013" BottomMargin="-27.7987" ctype="SpriteObjectData">
                <Size X="150.0000" Y="150.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="49.5103" Y="47.2013" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5104" Y="0.4866" />
                <PreSize X="1.5464" Y="1.5464" />
                <FileData Type="MarkedSubImage" Path="bullfighting/res/table/win_light.png" Plist="bullfighting/res/table.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="2.0840" Y="12.6650" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="bullfighting/res/table/win_frame.png" Plist="bullfighting/res/table.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="btn_sit" ActionTag="1207711727" Tag="473" IconVisible="False" LeftMargin="-28.9650" RightMargin="-29.0350" TopMargin="-49.9044" BottomMargin="-26.0956" ctype="SpriteObjectData">
            <Size X="58.0000" Y="76.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="0.0350" Y="11.9044" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="bullfighting/res/table/btn_sit.png" Plist="bullfighting/res/table.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="head" ActionTag="261845795" Tag="14" IconVisible="False" LeftMargin="-36.3917" RightMargin="-39.6083" TopMargin="-50.4624" BottomMargin="-25.5376" LeftEage="25" RightEage="25" TopEage="25" BottomEage="25" Scale9OriginX="25" Scale9OriginY="25" Scale9Width="30" Scale9Height="30" ctype="ImageViewObjectData">
            <Size X="76.0000" Y="76.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1.6083" Y="12.4624" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="bullfighting/res/table/seat_avatar.png" Plist="bullfighting/res/table.plist" />
          </AbstractNodeData>
          <AbstractNodeData Name="coin_txt" ActionTag="1540115746" Tag="472" IconVisible="False" LeftMargin="-48.3194" RightMargin="-47.6806" TopMargin="27.6797" BottomMargin="-51.6797" FontSize="24" LabelText="1000.0万" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="96.0000" Y="24.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-0.3194" Y="-39.6797" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="243" B="176" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>