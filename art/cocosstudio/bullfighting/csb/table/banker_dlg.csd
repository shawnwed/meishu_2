<GameFile>
  <PropertyGroup Name="banker_dlg" Type="Node" ID="3b1905f2-0464-4984-bc7f-26c6225c02e1" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="3964" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="mask" CanEdit="False" ActionTag="-407423925" Alpha="0" Tag="3969" IconVisible="False" LeftMargin="-640.0000" RightMargin="-640.0000" TopMargin="-360.0000" BottomMargin="-360.0000" ClipAble="False" BackColorAlpha="102" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="1280.0000" Y="720.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <SingleColor A="255" R="26" G="26" B="26" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="bg" ActionTag="-1661615276" Tag="3965" IconVisible="False" LeftMargin="-410.5000" RightMargin="-410.5000" TopMargin="-331.5000" BottomMargin="-331.5000" Scale9Enable="True" LeftEage="257" RightEage="257" TopEage="156" BottomEage="196" Scale9OriginX="257" Scale9OriginY="156" Scale9Width="401" Scale9Height="224" ctype="ImageViewObjectData">
            <Size X="821.0000" Y="663.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="bullfighting/res/dialog_bg.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="title" ActionTag="-1419171340" Tag="3971" IconVisible="False" LeftMargin="-81.5477" RightMargin="-78.4523" TopMargin="-304.3945" BottomMargin="264.3945" FontSize="40" LabelText="庄家列表" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="160.0000" Y="40.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-1.5477" Y="284.3945" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="230" B="208" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="list" ActionTag="2066339052" Tag="3972" IconVisible="False" LeftMargin="-361.3350" RightMargin="-363.6650" TopMargin="-229.5000" BottomMargin="-179.5000" TouchEnable="True" ClipAble="True" BackColorAlpha="204" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" IsBounceEnabled="True" ScrollDirectionType="0" DirectionType="Vertical" ctype="ListViewObjectData">
            <Size X="725.0000" Y="409.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1.1650" Y="25.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <SingleColor A="255" R="192" G="159" B="143" />
            <FirstColor A="255" R="150" G="150" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="cancel_apply" ActionTag="719645513" Tag="3973" IconVisible="False" LeftMargin="-135.6005" RightMargin="-138.3995" TopMargin="210.0314" BottomMargin="-305.0314" ctype="SpriteObjectData">
            <Size X="274.0000" Y="95.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1.3995" Y="-257.5314" />
            <Scale ScaleX="0.7700" ScaleY="0.7700" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="bullfighting/res/table/btn_cancel_apply.png" Plist="bullfighting/res/table.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="apply_panel" ActionTag="1353631251" Tag="3978" IconVisible="False" LeftMargin="-375.0000" RightMargin="-375.0000" TopMargin="177.7712" BottomMargin="-307.7712" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="750.0000" Y="130.0000" />
            <Children>
              <AbstractNodeData Name="apply" ActionTag="421886941" Tag="3975" IconVisible="False" LeftMargin="500.8357" RightMargin="-24.8357" TopMargin="41.7669" BottomMargin="-6.7669" ctype="SpriteObjectData">
                <Size X="274.0000" Y="95.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="637.8357" Y="40.7331" />
                <Scale ScaleX="0.7700" ScaleY="0.7700" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.8504" Y="0.3133" />
                <PreSize X="0.3653" Y="0.7308" />
                <FileData Type="MarkedSubImage" Path="bullfighting/res/table/btn_apply.png" Plist="bullfighting/res/table.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="slider" ActionTag="680081257" Tag="3970" IconVisible="False" LeftMargin="15.8521" RightMargin="260.1479" TopMargin="68.3759" BottomMargin="16.6241" TouchEnable="True" PercentInfo="34" ctype="SliderObjectData">
                <Size X="474.0000" Y="45.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="252.8521" Y="39.1241" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3371" Y="0.3010" />
                <PreSize X="0.6320" Y="0.3462" />
                <BackGroundData Type="MarkedSubImage" Path="bullfighting/res/table/slider_track.png" Plist="bullfighting/res/table.plist" />
                <ProgressBarData Type="MarkedSubImage" Path="bullfighting/res/table/slider_fill.png" Plist="bullfighting/res/table.plist" />
                <BallNormalData Type="MarkedSubImage" Path="bullfighting/res/table/slider_btn.png" Plist="bullfighting/res/table.plist" />
                <BallPressedData Type="MarkedSubImage" Path="bullfighting/res/table/slider_btn.png" Plist="bullfighting/res/table.plist" />
                <BallDisabledData Type="MarkedSubImage" Path="bullfighting/res/table/slider_btn.png" Plist="bullfighting/res/table.plist" />
              </AbstractNodeData>
              <AbstractNodeData Name="min_take_txt" ActionTag="246065444" Tag="3974" IconVisible="False" LeftMargin="137.0000" RightMargin="483.0000" TopMargin="6.0000" BottomMargin="104.0000" FontSize="20" LabelText="至少需要600万" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="130.0000" Y="20.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="137.0000" Y="114.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="81" G="70" B="69" />
                <PrePosition X="0.1827" Y="0.8769" />
                <PreSize X="0.1733" Y="0.1538" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_3" ActionTag="1932866924" Tag="3976" IconVisible="False" LeftMargin="137.0000" RightMargin="487.0000" TopMargin="35.0000" BottomMargin="67.0000" FontSize="28" LabelText="携带上庄:" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="126.0000" Y="28.0000" />
                <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
                <Position X="263.0000" Y="81.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="81" G="70" B="69" />
                <PrePosition X="0.3507" Y="0.6231" />
                <PreSize X="0.1680" Y="0.2154" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="cur_take_txt" ActionTag="763568377" Tag="69" IconVisible="False" LeftMargin="270.0474" RightMargin="387.9526" TopMargin="31.3396" BottomMargin="65.6604" LabelText="54万" ctype="TextBMFontObjectData">
                <Size X="92.0000" Y="33.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="270.0474" Y="82.1604" />
                <Scale ScaleX="0.7500" ScaleY="0.7500" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3601" Y="0.6320" />
                <PreSize X="0.1227" Y="0.2538" />
                <LabelBMFontFile_CNB Type="Normal" Path="hall/res/rank_num.fnt" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="auto_check" ActionTag="1290736728" Tag="1153" IconVisible="False" LeftMargin="533.0546" RightMargin="41.9454" TopMargin="11.7596" BottomMargin="84.2404" ctype="SpriteObjectData">
                <Size X="175.0000" Y="34.0000" />
                <Children>
                  <AbstractNodeData Name="txt" ActionTag="428567852" Tag="1154" IconVisible="False" LeftMargin="42.0867" RightMargin="-27.0867" TopMargin="7.8871" BottomMargin="6.1129" FontSize="20" LabelText="自动补齐上庄携带" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="160.0000" Y="20.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="122.0867" Y="16.1129" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="81" G="70" B="69" />
                    <PrePosition X="0.6976" Y="0.4739" />
                    <PreSize X="0.9143" Y="0.5882" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="620.5546" Y="101.2404" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.8274" Y="0.7788" />
                <PreSize X="0.2333" Y="0.2615" />
                <FileData Type="MarkedSubImage" Path="bullfighting/res/table/check_0.png" Plist="bullfighting/res/table.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position X="-375.0000" Y="-307.7712" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>