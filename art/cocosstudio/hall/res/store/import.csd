<GameFile>
  <PropertyGroup Name="import" Type="Node" ID="51cc072e-576c-4144-ae93-83a58eccf010" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="5003" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="store_click_1" ActionTag="2003190744" Tag="5004" IconVisible="False" LeftMargin="-165.3481" RightMargin="-128.6519" TopMargin="-149.1761" BottomMargin="-125.8239" ctype="SpriteObjectData">
            <Size X="294.0000" Y="275.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-18.3481" Y="11.6761" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="hall/res/store/store_click.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="store_normal_2" ActionTag="-754842246" Tag="5005" IconVisible="False" LeftMargin="-165.3481" RightMargin="-128.6519" TopMargin="-149.1761" BottomMargin="-125.8239" ctype="SpriteObjectData">
            <Size X="294.0000" Y="275.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-18.3481" Y="11.6761" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="hall/res/store/store_normal.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>