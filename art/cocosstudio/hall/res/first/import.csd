<GameFile>
  <PropertyGroup Name="import" Type="Node" ID="7438f274-2356-4141-8997-2f2fc63b76dd" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="1839" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="rank_item_bg1_2" ActionTag="2101357097" Tag="2142" IconVisible="False" LeftMargin="-582.8702" RightMargin="-667.1298" TopMargin="-65.8050" BottomMargin="-60.1950" ctype="SpriteObjectData">
            <Size X="1250.0000" Y="126.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="42.1298" Y="2.8050" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="hall/res/first/rank_item_bg1.png" Plist="hall/res/first.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="bet_select_1" ActionTag="1409563357" Tag="650" IconVisible="False" LeftMargin="-249.0000" RightMargin="-43.0000" TopMargin="-309.5000" BottomMargin="98.5000" ctype="SpriteObjectData">
            <Size X="292.0000" Y="211.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-103.0000" Y="204.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="hall/res/first/bet_select.png" Plist="hall/res/first.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="bet_select_no_1" ActionTag="1770987145" Tag="4" IconVisible="False" LeftMargin="64.0000" RightMargin="-314.0000" TopMargin="-305.0000" BottomMargin="75.0000" ctype="SpriteObjectData">
            <Size X="250.0000" Y="230.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="189.0000" Y="190.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="hall/res/first/bet_select_no.png" Plist="hall/res/first.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="rank_item_bg3_1" ActionTag="1651063461" Tag="209" IconVisible="False" LeftMargin="-641.7650" RightMargin="-608.2350" TopMargin="135.6867" BottomMargin="-261.6867" ctype="SpriteObjectData">
            <Size X="1250.0000" Y="126.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-16.7650" Y="-198.6867" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="hall/res/first/rank_item_bg3.png" Plist="hall/res/first.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>