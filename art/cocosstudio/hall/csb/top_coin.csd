<GameFile>
  <PropertyGroup Name="top_coin" Type="Layer" ID="83b4acbe-0340-44b8-be8b-a7af268b340d" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="172" ctype="GameLayerObjectData">
        <Size X="230.0000" Y="46.0000" />
        <Children>
          <AbstractNodeData Name="coin_money" ActionTag="-1367423158" Tag="62" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-1.2590" RightMargin="0.2590" TopMargin="1.5000" BottomMargin="1.5000" ctype="SpriteObjectData">
            <Size X="231.0000" Y="43.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="114.2410" Y="23.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4967" Y="0.5000" />
            <PreSize X="1.0043" Y="0.9348" />
            <FileData Type="MarkedSubImage" Path="hall/res/first/coin_bg.png" Plist="hall/res/first.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="coin_add" ActionTag="1399202430" Tag="2259" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="174.9108" RightMargin="-16.9108" TopMargin="-13.0000" BottomMargin="-13.0000" ctype="SpriteObjectData">
            <Size X="72.0000" Y="72.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="210.9108" Y="23.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.9170" Y="0.5000" />
            <PreSize X="0.3130" Y="1.5652" />
            <FileData Type="MarkedSubImage" Path="hall/res/first/coin_add.png" Plist="hall/res/first.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="coin_money_2" ActionTag="-550971456" Tag="2260" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="-0.2608" RightMargin="187.2608" TopMargin="2.4982" BottomMargin="0.5018" ctype="SpriteObjectData">
            <Size X="43.0000" Y="43.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="21.2392" Y="22.0018" />
            <Scale ScaleX="0.9000" ScaleY="0.9000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0923" Y="0.4783" />
            <PreSize X="0.1870" Y="0.9348" />
            <FileData Type="MarkedSubImage" Path="hall/res/first/coin_money.png" Plist="hall/res/first.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="txt" ActionTag="-1450256680" Tag="107" IconVisible="False" LeftMargin="29.4531" RightMargin="29.5469" TopMargin="5.9482" BottomMargin="7.0518" LabelText="1120.02" ctype="TextBMFontObjectData">
            <Size X="171.0000" Y="33.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="114.9531" Y="23.5518" />
            <Scale ScaleX="0.8700" ScaleY="0.8700" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4998" Y="0.5120" />
            <PreSize X="0.7435" Y="0.7174" />
            <LabelBMFontFile_CNB Type="Normal" Path="hall/res/rank_num.fnt" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="txt" ActionTag="-1569954715" VisibleForFrame="False" Tag="63" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="62.5000" RightMargin="62.5000" TopMargin="8.0000" BottomMargin="8.0000" FontSize="30" LabelText="1200000" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="105.0000" Y="30.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="115.0000" Y="23.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="0.4565" Y="0.6522" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>