<GameFile>
  <PropertyGroup Name="row_item" Type="Layer" ID="0e2db672-2285-4ed0-b66f-7d7cfdeeadf9" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="7" ctype="GameLayerObjectData">
        <Size X="972.0000" Y="315.0000" />
        <Children>
          <AbstractNodeData Name="bg_row_9" ActionTag="1208531397" Tag="28" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" ctype="SpriteObjectData">
            <Size X="972.0000" Y="315.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="486.0000" Y="157.5000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="1.0000" Y="1.0000" />
            <FileData Type="Default" Path="Default/Sprite.png" Plist="" />
            <BlendFunc Src="770" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="item_1" ActionTag="716608817" Tag="84" IconVisible="True" LeftMargin="179.0000" RightMargin="793.0000" TopMargin="165.0000" BottomMargin="150.0000" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="179.0000" Y="150.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.1842" Y="0.4762" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="hall/csb/store/item.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="item_2" ActionTag="-1232056617" Tag="90" IconVisible="True" LeftMargin="487.5000" RightMargin="484.5000" TopMargin="165.0000" BottomMargin="150.0000" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="487.5000" Y="150.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5015" Y="0.4762" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="hall/csb/store/item.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="item_3" ActionTag="1918354506" Tag="96" IconVisible="True" LeftMargin="796.0000" RightMargin="176.0000" TopMargin="165.0000" BottomMargin="150.0000" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="796.0000" Y="150.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8189" Y="0.4762" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="hall/csb/store/item.csd" Plist="" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>