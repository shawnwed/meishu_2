<GameFile>
  <PropertyGroup Name="item" Type="Node" ID="0590d542-5f23-4f52-b7a0-5dbdeb7abd38" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="35" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="item_bg_1" ActionTag="-1294261823" Tag="55" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="-130.0000" RightMargin="-130.0000" TopMargin="-125.0000" BottomMargin="-125.0000" ctype="SpriteObjectData">
            <Size X="260.0000" Y="250.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="hall/res/store/item_bg.png" Plist="hall/res/store.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="price" ActionTag="-408319792" Tag="52" IconVisible="False" LeftMargin="-44.9933" RightMargin="-27.0067" TopMargin="72.0967" BottomMargin="-107.0967" FontSize="35" LabelText="0000" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="72.0000" Y="35.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-8.9933" Y="-89.5967" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="icon" ActionTag="759175006" Tag="53" IconVisible="False" LeftMargin="-75.5000" RightMargin="-75.5000" TopMargin="-97.0168" BottomMargin="-8.9832" ctype="SpriteObjectData">
            <Size X="151.0000" Y="106.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position Y="44.0168" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="hall/res/store/money1.png" Plist="hall/res/store.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="name" ActionTag="1826752083" Tag="54" IconVisible="False" LeftMargin="-54.5550" RightMargin="-41.4450" TopMargin="46.0955" BottomMargin="-79.0955" FontSize="32" LabelText="10筹码" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="96.0000" Y="33.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-6.5550" Y="-62.5955" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="244" G="232" B="119" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="price_icon" ActionTag="-1473961439" Tag="961" IconVisible="False" LeftMargin="-81.3885" RightMargin="34.3885" TopMargin="69.5964" BottomMargin="-109.5964" ctype="SpriteObjectData">
            <Size X="47.0000" Y="40.0000" />
            <AnchorPoint ScaleX="0.4378" ScaleY="0.5101" />
            <Position X="-60.8130" Y="-89.1926" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="game_public/res/diamond_icon.png" Plist="game_public/res/game.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="image" ActionTag="166325605" Tag="3041" IconVisible="False" LeftMargin="-70.0000" RightMargin="-70.0000" TopMargin="-111.5000" BottomMargin="-23.5000" LeftEage="46" RightEage="46" TopEage="44" BottomEage="44" Scale9OriginX="46" Scale9OriginY="44" Scale9Width="59" Scale9Height="18" ctype="ImageViewObjectData">
            <Size X="140.0000" Y="135.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position Y="44.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="hall/res/store/money1.png" Plist="hall/res/store.plist" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>