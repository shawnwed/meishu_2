<GameFile>
  <PropertyGroup Name="store_item" Type="Layer" ID="07d2b048-be64-4c34-abfe-d83c941bd232" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="108" ctype="GameLayerObjectData">
        <Size X="274.0000" Y="284.0000" />
        <Children>
          <AbstractNodeData Name="item_bg_1" ActionTag="-394913350" Tag="109" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="7.0000" RightMargin="7.0000" TopMargin="17.0000" BottomMargin="17.0000" ctype="SpriteObjectData">
            <Size X="260.0000" Y="250.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="137.0000" Y="142.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="0.9489" Y="0.8803" />
            <FileData Type="MarkedSubImage" Path="hall/res/store/item_bg.png" Plist="hall/res/store.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="name" ActionTag="-554515166" Tag="5002" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="86.7818" RightMargin="91.2182" TopMargin="31.8379" BottomMargin="219.1621" FontSize="32" LabelText="10筹码" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="96.0000" Y="33.0000" />
            <AnchorPoint ScaleX="0.4000" ScaleY="0.5000" />
            <Position X="125.1818" Y="235.6621" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="152" G="50" B="0" />
            <PrePosition X="0.4569" Y="0.8298" />
            <PreSize X="0.3504" Y="0.1162" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="icon" ActionTag="521501293" Tag="2359" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="64.6976" RightMargin="58.3024" TopMargin="72.7405" BottomMargin="105.2595" ctype="SpriteObjectData">
            <Size X="151.0000" Y="106.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="140.1976" Y="158.2595" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5117" Y="0.5573" />
            <PreSize X="0.5511" Y="0.3732" />
            <FileData Type="MarkedSubImage" Path="hall/res/store/money1.png" Plist="hall/res/store.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="price" ActionTag="2116604571" Tag="6" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="79.1004" RightMargin="74.8996" TopMargin="199.4513" BottomMargin="44.5487" FontSize="40" LabelText="￥6.00" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="120.0000" Y="40.0000" />
            <AnchorPoint ScaleX="0.4000" ScaleY="0.5000" />
            <Position X="127.1004" Y="64.5487" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4639" Y="0.2273" />
            <PreSize X="0.4380" Y="0.1408" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>