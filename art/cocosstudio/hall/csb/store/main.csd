<GameFile>
  <PropertyGroup Name="main" Type="Scene" ID="cefdbecc-164e-427a-aadb-9c4d38d8f491" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Scene" Tag="308" ctype="GameNodeObjectData">
        <Size X="960.0000" Y="640.0000" />
        <Children>
          <AbstractNodeData Name="bg" ActionTag="-1181955871" Tag="343" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" LeftMargin="-1.6320" RightMargin="1.6320" TouchEnable="True" LeftEage="422" RightEage="422" TopEage="237" BottomEage="237" Scale9OriginX="422" Scale9OriginY="237" Scale9Width="436" Scale9Height="246" ctype="ImageViewObjectData">
            <Size X="960.0000" Y="640.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="478.3680" Y="320.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4983" Y="0.5000" />
            <PreSize X="1.0000" Y="1.0000" />
            <FileData Type="Normal" Path="bg/bg_info.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="left" ActionTag="1927040170" Tag="19" IconVisible="False" LeftMargin="5.5237" RightMargin="656.4763" TopMargin="142.1651" BottomMargin="17.8349" RightEage="30" Scale9Width="268" Scale9Height="566" ctype="ImageViewObjectData">
            <Size X="298.0000" Y="480.0000" />
            <Children>
              <AbstractNodeData Name="btn_buy" ActionTag="-1912414479" Tag="160" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="15.4372" RightMargin="20.5628" TopMargin="7.8099" BottomMargin="362.1901" ctype="SpriteObjectData">
                <Size X="262.0000" Y="110.0000" />
                <Children>
                  <AbstractNodeData Name="txt" ActionTag="1491998471" VisibleForFrame="False" Tag="978" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="67.0000" RightMargin="67.0000" TopMargin="35.7170" BottomMargin="41.2830" FontSize="32" LabelText="金幣購買" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="128.0000" Y="33.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="131.0000" Y="57.7830" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5253" />
                    <PreSize X="0.4885" Y="0.3000" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="146.4372" Y="417.1901" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4914" Y="0.8691" />
                <PreSize X="0.8792" Y="0.2292" />
                <FileData Type="MarkedSubImage" Path="hall/res/store/btn_buy.png" Plist="hall/res/store.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="btn_roomcard" ActionTag="1074096711" Tag="829" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="153.2924" RightMargin="-99.2924" TopMargin="-1077.4240" BottomMargin="1489.4240" ctype="SpriteObjectData">
                <Size X="244.0000" Y="68.0000" />
                <Children>
                  <AbstractNodeData Name="txt" ActionTag="-1769369693" Tag="830" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="58.0000" RightMargin="58.0000" TopMargin="15.5008" BottomMargin="19.4992" FontSize="32" LabelText="房卡购买" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="128.0000" Y="33.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="122.0000" Y="35.9992" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5294" />
                    <PreSize X="0.5246" Y="0.4853" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="275.2924" Y="1523.4240" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.9238" Y="3.1738" />
                <PreSize X="0.8188" Y="0.1417" />
                <FileData Type="MarkedSubImage" Path="hall/res/store/btn_bg.png" Plist="hall/res/store.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="btn_exchange" ActionTag="600825901" Tag="330" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="164.7654" RightMargin="-110.7654" TopMargin="-2010.6401" BottomMargin="2422.6401" ctype="SpriteObjectData">
                <Size X="244.0000" Y="68.0000" />
                <Children>
                  <AbstractNodeData Name="txt" ActionTag="1321421587" Tag="979" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="58.0000" RightMargin="58.0000" TopMargin="15.5008" BottomMargin="19.4992" FontSize="32" LabelText="物品兌換" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="128.0000" Y="33.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="122.0000" Y="35.9992" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5294" />
                    <PreSize X="0.5246" Y="0.4853" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="286.7654" Y="2456.6401" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.9623" Y="5.1180" />
                <PreSize X="0.8188" Y="0.1417" />
                <FileData Type="MarkedSubImage" Path="hall/res/store/btn_bg.png" Plist="hall/res/store.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="icon_record" ActionTag="-814480969" Tag="332" IconVisible="False" LeftMargin="-120.1511" RightMargin="372.1511" TopMargin="610.2162" BottomMargin="-176.2162" ctype="SpriteObjectData">
                <Size X="46.0000" Y="46.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="-97.1511" Y="-153.2162" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="-0.3260" Y="-0.3192" />
                <PreSize X="0.1544" Y="0.0958" />
                <FileData Type="Default" Path="Default/Sprite.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="binding" ActionTag="-1762488015" Tag="206" IconVisible="False" LeftMargin="-316.8046" RightMargin="414.8046" TopMargin="583.5634" BottomMargin="-173.5634" ctype="SpriteObjectData">
                <Size X="200.0000" Y="70.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="-216.8046" Y="-138.5634" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="-0.7275" Y="-0.2887" />
                <PreSize X="0.6711" Y="0.1458" />
                <FileData Type="MarkedSubImage" Path="lan/cn/binding_parentid.png" Plist="lan/cn/game.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="icon_local" ActionTag="-1569438324" Tag="331" IconVisible="False" LeftMargin="-186.2358" RightMargin="438.2358" TopMargin="631.6541" BottomMargin="-197.6541" ctype="SpriteObjectData">
                <Size X="46.0000" Y="46.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="-163.2358" Y="-174.6541" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="-0.5478" Y="-0.3639" />
                <PreSize X="0.1544" Y="0.0958" />
                <FileData Type="Default" Path="Default/Sprite.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="black_diamond" ActionTag="-1755281037" VisibleForFrame="False" Tag="103" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="59.0000" RightMargin="59.0000" TopMargin="438.8344" BottomMargin="11.1656" FontSize="30" LabelText="黑钻石：6000" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="180.0000" Y="30.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="149.0000" Y="26.1656" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.0545" />
                <PreSize X="0.6040" Y="0.0625" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position X="5.5237" Y="17.8349" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0058" Y="0.0279" />
            <PreSize X="0.3104" Y="0.7500" />
            <FileData Type="Normal" Path="bg/bg_info_content.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="right" ActionTag="-1339060454" Tag="20" IconVisible="False" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" LeftMargin="313.3209" RightMargin="-260.3289" TopMargin="140.4842" BottomMargin="13.1158" Scale9Enable="True" LeftEage="15" RightEage="15" Scale9OriginX="15" Scale9Width="877" Scale9Height="539" ctype="ImageViewObjectData">
            <Size X="907.0080" Y="486.4000" />
            <AnchorPoint />
            <Position X="313.3209" Y="13.1158" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3264" Y="0.0205" />
            <PreSize X="0.9448" Y="0.7600" />
            <FileData Type="Normal" Path="hall/res/store/bg_main.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="bg_0" ActionTag="1879726615" Tag="46" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" LeftMargin="-159.9840" RightMargin="-159.9840" TopMargin="87.1680" BottomMargin="467.8400" TouchEnable="True" LeftEage="422" RightEage="422" TopEage="28" BottomEage="28" Scale9OriginX="422" Scale9OriginY="28" Scale9Width="436" Scale9Height="29" ctype="ImageViewObjectData">
            <Size X="1279.9680" Y="84.9920" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="480.0000" Y="510.3360" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.7974" />
            <PreSize X="1.3333" Y="0.1328" />
            <FileData Type="Normal" Path="hall/res/store/bg_left.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="top" ActionTag="-1023825958" Tag="640" IconVisible="True" PercentWidthEnable="True" PercentWidthEnabled="True" LeftMargin="-0.0003" RightMargin="0.0003" TopMargin="-1.2745" BottomMargin="546.2745" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="True" ctype="ProjectNodeObjectData">
            <Size X="960.0000" Y="95.0000" />
            <AnchorPoint />
            <Position X="-0.0003" Y="546.2745" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0000" Y="0.8536" />
            <PreSize X="1.0000" Y="0.1484" />
            <FileData Type="Normal" Path="game_public/csb/hall_top.csd" Plist="" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>