<GameFile>
  <PropertyGroup Name="exchange_record" Type="Layer" ID="ada2d249-48c2-4884-a0c3-6396840084c8" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="595" ctype="GameLayerObjectData">
        <Size X="974.0000" Y="633.6000" />
        <Children>
          <AbstractNodeData Name="title_bg" ActionTag="-1427735781" Tag="837" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="26.0000" RightMargin="26.0000" BottomMargin="569.6000" ctype="SpriteObjectData">
            <Size X="922.0000" Y="64.0000" />
            <Children>
              <AbstractNodeData Name="time" ActionTag="2088083281" Tag="838" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="97.5200" RightMargin="724.4800" TopMargin="17.5000" BottomMargin="17.5000" FontSize="25" LabelText="兌換時間" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="100.0000" Y="29.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="147.5200" Y="32.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="162" G="201" B="252" />
                <PrePosition X="0.1600" Y="0.5000" />
                <PreSize X="0.1085" Y="0.4531" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="name" ActionTag="-22333739" Tag="840" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="436.0000" RightMargin="436.0000" TopMargin="17.5000" BottomMargin="17.5000" FontSize="25" LabelText="商品" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="50.0000" Y="29.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="461.0000" Y="32.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="162" G="201" B="252" />
                <PrePosition X="0.5000" Y="0.5000" />
                <PreSize X="0.0542" Y="0.4531" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="status" ActionTag="420019735" Tag="841" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="724.4800" RightMargin="97.5200" TopMargin="17.5000" BottomMargin="17.5000" FontSize="25" LabelText="發貨狀態" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="100.0000" Y="29.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="774.4800" Y="32.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="162" G="201" B="252" />
                <PrePosition X="0.8400" Y="0.5000" />
                <PreSize X="0.1085" Y="0.4531" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="1.0000" />
            <Position X="487.0000" Y="633.6000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="1.0000" />
            <PreSize X="0.9466" Y="0.1010" />
            <FileData Type="MarkedSubImage" Path="hall/res/store/record_title_bg.png" Plist="hall/res/store.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="arrow" ActionTag="1930493543" Tag="843" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="464.0000" RightMargin="464.0000" TopMargin="595.7600" BottomMargin="15.8400" ctype="SpriteObjectData">
            <Size X="46.0000" Y="22.0000" />
            <AnchorPoint ScaleX="0.5000" />
            <Position X="487.0000" Y="15.8400" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.0250" />
            <PreSize X="0.0472" Y="0.0347" />
            <FileData Type="MarkedSubImage" Path="hall/res/store/icon_arrow.png" Plist="hall/res/store.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>