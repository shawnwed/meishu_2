<GameFile>
  <PropertyGroup Name="exchange_info" Type="Layer" ID="66d90cc6-fc49-42d0-bb3a-735e39464d92" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="476" ctype="GameLayerObjectData">
        <Size X="974.0000" Y="633.0000" />
        <Children>
          <AbstractNodeData Name="content" ActionTag="181255778" Tag="495" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentWidthEnabled="True" LeftMargin="-0.0013" RightMargin="0.0013" TopMargin="99.3177" BottomMargin="183.6823" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="974.0000" Y="350.0000" />
            <Children>
              <AbstractNodeData Name="tip" ActionTag="-2000166793" Tag="543" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="175.0000" RightMargin="175.0000" TopMargin="298.4325" BottomMargin="23.5675" FontSize="24" LabelText="請務必填寫真實有效的聯繫方式，以便工作人員與你聯繫。" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="624.0000" Y="28.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="487.0000" Y="37.5675" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="128" G="179" B="248" />
                <PrePosition X="0.5000" Y="0.1073" />
                <PreSize X="0.6407" Y="0.0800" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="name" ActionTag="1962756611" Tag="613" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="26.0000" RightMargin="26.0000" TopMargin="5.5600" BottomMargin="280.4400" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="922.0000" Y="64.0000" />
                <Children>
                  <AbstractNodeData Name="txt" ActionTag="-1920221885" Tag="541" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="20.0000" RightMargin="756.0000" TopMargin="15.0000" BottomMargin="15.0000" FontSize="30" LabelText="收 貨 人 ：" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="146.0000" Y="34.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="93.0000" Y="32.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.1009" Y="0.5000" />
                    <PreSize X="0.1584" Y="0.5313" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="input" ActionTag="-1178584492" Tag="542" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="170.0001" RightMargin="51.9999" TopMargin="15.0000" BottomMargin="15.0000" TouchEnable="True" FontSize="30" IsCustomSize="True" LabelText="" PlaceHolderText="姓名" MaxLengthText="10" ctype="TextFieldObjectData">
                    <Size X="700.0000" Y="34.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="520.0001" Y="32.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5640" Y="0.5000" />
                    <PreSize X="0.7592" Y="0.5313" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="487.0000" Y="312.4400" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.8927" />
                <PreSize X="0.9466" Y="0.1829" />
                <SingleColor A="255" R="0" G="0" B="0" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="phone" ActionTag="356308472" Tag="614" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="26.0000" RightMargin="26.0000" TopMargin="72.2266" BottomMargin="213.7734" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="922.0000" Y="64.0000" />
                <Children>
                  <AbstractNodeData Name="txt" ActionTag="-12792217" Tag="615" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="18.0000" RightMargin="754.0000" TopMargin="15.0000" BottomMargin="15.0000" FontSize="30" LabelText="手機號碼：" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="150.0000" Y="34.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="93.0000" Y="32.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.1009" Y="0.5000" />
                    <PreSize X="0.1627" Y="0.5313" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="input" ActionTag="-1596281353" Tag="616" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="170.0001" RightMargin="51.9999" TopMargin="15.0000" BottomMargin="15.0000" TouchEnable="True" FontSize="30" IsCustomSize="True" LabelText="" PlaceHolderText="正確的手機號碼" MaxLengthText="10" ctype="TextFieldObjectData">
                    <Size X="700.0000" Y="34.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="520.0001" Y="32.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5640" Y="0.5000" />
                    <PreSize X="0.7592" Y="0.5313" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="487.0000" Y="245.7734" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.7022" />
                <PreSize X="0.9466" Y="0.1829" />
                <SingleColor A="255" R="0" G="0" B="0" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="email" ActionTag="1411352481" Tag="617" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="26.0000" RightMargin="26.0000" TopMargin="138.8933" BottomMargin="147.1067" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="922.0000" Y="64.0000" />
                <Children>
                  <AbstractNodeData Name="txt" ActionTag="-21060400" Tag="618" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="18.0000" RightMargin="754.0000" TopMargin="15.0000" BottomMargin="15.0000" FontSize="30" LabelText="電子郵箱：" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="150.0000" Y="34.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="93.0000" Y="32.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.1009" Y="0.5000" />
                    <PreSize X="0.1627" Y="0.5313" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="input" ActionTag="1389489622" Tag="619" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="170.0001" RightMargin="51.9999" TopMargin="15.0000" BottomMargin="15.0000" TouchEnable="True" FontSize="30" IsCustomSize="True" LabelText="" PlaceHolderText="有效郵箱地址" MaxLengthText="10" ctype="TextFieldObjectData">
                    <Size X="700.0000" Y="34.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="520.0001" Y="32.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5640" Y="0.5000" />
                    <PreSize X="0.7592" Y="0.5313" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="487.0000" Y="179.1067" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5117" />
                <PreSize X="0.9466" Y="0.1829" />
                <SingleColor A="255" R="0" G="0" B="0" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="location" ActionTag="-33765342" Tag="620" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="26.0000" RightMargin="26.0000" TopMargin="205.0000" BottomMargin="81.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="922.0000" Y="64.0000" />
                <Children>
                  <AbstractNodeData Name="txt" ActionTag="1975069626" Tag="621" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="18.0000" RightMargin="754.0000" TopMargin="15.0000" BottomMargin="15.0000" FontSize="30" LabelText="詳細地址：" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="150.0000" Y="34.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="93.0000" Y="32.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.1009" Y="0.5000" />
                    <PreSize X="0.1627" Y="0.5313" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="input" ActionTag="-278743427" Tag="622" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="170.0001" RightMargin="51.9999" TopMargin="15.0000" BottomMargin="15.0000" TouchEnable="True" FontSize="30" IsCustomSize="True" LabelText="" PlaceHolderText="請輸入正確的收貨地址" MaxLengthText="10" ctype="TextFieldObjectData">
                    <Size X="700.0000" Y="34.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="520.0001" Y="32.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5640" Y="0.5000" />
                    <PreSize X="0.7592" Y="0.5313" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="487.0000" Y="113.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.3229" />
                <PreSize X="0.9466" Y="0.1829" />
                <SingleColor A="255" R="0" G="0" B="0" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="line2" ActionTag="751203189" Tag="623" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="26.0000" RightMargin="26.0000" TopMargin="272.0000" BottomMargin="75.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="922.0000" Y="3.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="1.0000" />
                <Position X="487.0000" Y="78.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.2229" />
                <PreSize X="0.9466" Y="0.0086" />
                <SingleColor A="255" R="0" G="0" B="0" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="line1" ActionTag="1573033702" Tag="626" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="26.0000" RightMargin="26.0000" BottomMargin="347.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="922.0000" Y="3.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="1.0000" />
                <Position X="487.0000" Y="350.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="1.0000" />
                <PreSize X="0.9466" Y="0.0086" />
                <SingleColor A="255" R="0" G="0" B="0" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="1.0000" />
            <Position X="486.9987" Y="533.6823" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.8431" />
            <PreSize X="1.0000" Y="0.5529" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="btn_save" ActionTag="1123899540" Tag="544" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="584.2779" RightMargin="155.7221" TopMargin="470.9583" BottomMargin="72.0417" ctype="SpriteObjectData">
            <Size X="234.0000" Y="90.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="701.2779" Y="117.0417" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.7200" Y="0.1849" />
            <PreSize X="0.2402" Y="0.1422" />
            <FileData Type="MarkedSubImage" Path="lan/cn/btn_save.png" Plist="lan/cn/game.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="btn_back" ActionTag="1042165426" Tag="545" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="155.7189" RightMargin="584.2811" TopMargin="470.9583" BottomMargin="72.0417" ctype="SpriteObjectData">
            <Size X="234.0000" Y="90.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="272.7189" Y="117.0417" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.2800" Y="0.1849" />
            <PreSize X="0.2402" Y="0.1422" />
            <FileData Type="MarkedSubImage" Path="lan/cn/btn_back.png" Plist="lan/cn/game.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>