<GameFile>
  <PropertyGroup Name="main1" Type="Scene" ID="0878dc7b-45b2-4504-bcf1-2e19e49e2f00" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Scene" Tag="308" ctype="GameNodeObjectData">
        <Size X="960.0000" Y="640.0000" />
        <Children>
          <AbstractNodeData Name="bg" ActionTag="-1181955871" Tag="343" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" LeftMargin="1.8240" RightMargin="-1.8240" TouchEnable="True" LeftEage="9" RightEage="9" TopEage="237" BottomEage="237" Scale9OriginX="9" Scale9OriginY="237" Scale9Width="1262" Scale9Height="246" ctype="ImageViewObjectData">
            <Size X="960.0000" Y="640.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="481.8240" Y="320.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5019" Y="0.5000" />
            <PreSize X="1.0000" Y="1.0000" />
            <FileData Type="Normal" Path="bg/bg_info.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="bg_left" ActionTag="-311899469" Tag="1740" IconVisible="False" LeftMargin="9.2797" RightMargin="651.7203" TopMargin="86.9229" BottomMargin="15.0771" LeftEage="15" RightEage="15" TopEage="15" BottomEage="15" Scale9OriginX="15" Scale9OriginY="15" Scale9Width="269" Scale9Height="508" ctype="ImageViewObjectData">
            <Size X="299.0000" Y="538.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="9.2797" Y="284.0771" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0097" Y="0.4439" />
            <PreSize X="0.3115" Y="0.8406" />
            <FileData Type="Normal" Path="hall/res/store/bg_main1.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="bg_right" Visible="False" ActionTag="-1869689984" Tag="222" IconVisible="False" PercentWidthEnable="True" PercentWidthEnabled="True" LeftMargin="311.6996" RightMargin="-258.7075" TopMargin="84.2371" BottomMargin="16.7629" LeftEage="6" RightEage="6" TopEage="6" BottomEage="6" Scale9OriginX="6" Scale9OriginY="6" Scale9Width="895" Scale9Height="527" ctype="ImageViewObjectData">
            <Size X="907.0080" Y="539.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="311.6996" Y="286.2629" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3247" Y="0.4473" />
            <PreSize X="0.9448" Y="0.8422" />
            <FileData Type="Normal" Path="hall/res/store/bg_main.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="bg_right_box" Visible="False" ActionTag="1568788534" Tag="248" IconVisible="False" LeftMargin="-445.2869" RightMargin="1111.2869" TopMargin="301.4720" BottomMargin="63.5280" LeftEage="6" RightEage="6" TopEage="6" BottomEage="6" Scale9OriginX="6" Scale9OriginY="6" Scale9Width="282" Scale9Height="263" ctype="ImageViewObjectData">
            <Size X="294.0000" Y="275.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-298.2869" Y="201.0280" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="-0.3107" Y="0.3141" />
            <PreSize X="0.3063" Y="0.4297" />
            <FileData Type="Normal" Path="hall/res/store/store_normal.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="bg_left_box" Visible="False" ActionTag="982258328" Tag="249" IconVisible="False" LeftMargin="-445.2869" RightMargin="1111.2869" TopMargin="301.4720" BottomMargin="63.5280" LeftEage="6" RightEage="6" TopEage="6" BottomEage="6" Scale9OriginX="6" Scale9OriginY="6" Scale9Width="282" Scale9Height="263" ctype="ImageViewObjectData">
            <Size X="294.0000" Y="275.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-298.2869" Y="201.0280" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="-0.3107" Y="0.3141" />
            <PreSize X="0.3063" Y="0.4297" />
            <FileData Type="Normal" Path="hall/res/store/store_normal.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="bg_bottom_box" Visible="False" ActionTag="-133105167" Tag="250" IconVisible="False" LeftMargin="-430.9036" RightMargin="1096.9036" TopMargin="302.9472" BottomMargin="62.0528" LeftEage="6" RightEage="6" TopEage="6" BottomEage="6" Scale9OriginX="6" Scale9OriginY="6" Scale9Width="282" Scale9Height="263" ctype="ImageViewObjectData">
            <Size X="294.0000" Y="275.0000" />
            <AnchorPoint ScaleX="0.4328" ScaleY="0.5105" />
            <Position X="-303.6604" Y="202.4403" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="-0.3163" Y="0.3163" />
            <PreSize X="0.3063" Y="0.4297" />
            <FileData Type="Normal" Path="hall/res/store/store_normal.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="top" ActionTag="-1023825958" Tag="640" IconVisible="True" PercentWidthEnable="True" PercentWidthEnabled="True" TopMargin="-79.6800" BottomMargin="624.6800" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="True" ctype="ProjectNodeObjectData">
            <Size X="960.0000" Y="95.0000" />
            <AnchorPoint />
            <Position Y="624.6800" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition Y="0.9761" />
            <PreSize X="1.0000" Y="0.1484" />
            <FileData Type="Normal" Path="game_public/csb/hall_top.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="ListView" ActionTag="1888265837" Tag="398" IconVisible="False" LeftMargin="15.5428" RightMargin="644.4572" TopMargin="101.9449" BottomMargin="28.0551" TouchEnable="True" ClipAble="True" BackColorAlpha="0" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" IsBounceEnabled="True" ScrollDirectionType="0" ItemMargin="6" DirectionType="Vertical" ctype="ListViewObjectData">
            <Size X="300.0000" Y="510.0000" />
            <AnchorPoint />
            <Position X="15.5428" Y="28.0551" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0162" Y="0.0438" />
            <PreSize X="0.3125" Y="0.7969" />
            <SingleColor A="255" R="255" G="255" B="255" />
            <FirstColor A="255" R="150" G="150" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="Image_24" ActionTag="1126531997" Tag="1365" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="330.3840" RightMargin="233.6160" TopMargin="302.1554" BottomMargin="271.8446" LeftEage="110" RightEage="110" TopEage="19" BottomEage="19" Scale9OriginX="110" Scale9OriginY="19" Scale9Width="176" Scale9Height="28" ctype="ImageViewObjectData">
            <Size X="396.0000" Y="66.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="528.3840" Y="304.8446" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5504" Y="0.4763" />
            <PreSize X="0.4125" Y="0.1031" />
            <FileData Type="Normal" Path="hall/res/store/shurkuan.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="goldInput" ActionTag="1766311024" Tag="1761" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="342.5200" RightMargin="247.4800" TopMargin="308.2973" BottomMargin="275.7027" TouchEnable="True" FontSize="36" IsCustomSize="True" LabelText="" PlaceHolderText="请输入金额" MaxLengthText="10" ctype="TextFieldObjectData">
            <Size X="370.0000" Y="56.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="527.5200" Y="303.7027" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="0" G="0" B="0" />
            <PrePosition X="0.5495" Y="0.4745" />
            <PreSize X="0.3854" Y="0.0875" />
          </AbstractNodeData>
          <AbstractNodeData Name="btn_qingling" ActionTag="-1030982815" Tag="1764" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="726.7800" RightMargin="-3.7800" TopMargin="304.3066" BottomMargin="261.6934" ctype="SpriteObjectData">
            <Size X="237.0000" Y="74.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="845.2800" Y="298.6934" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8805" Y="0.4667" />
            <PreSize X="0.2469" Y="0.1156" />
            <FileData Type="Normal" Path="hall/res/store/qinglin.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="btn_buy" ActionTag="-2119798903" Tag="1766" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="467.1360" RightMargin="180.8640" TopMargin="516.0493" BottomMargin="28.9507" ctype="SpriteObjectData">
            <Size X="312.0000" Y="95.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="623.1360" Y="76.4507" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.6491" Y="0.1195" />
            <PreSize X="0.3250" Y="0.1484" />
            <FileData Type="Normal" Path="hall/res/store/bg_but03.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="txtTips" ActionTag="1556913660" Tag="1763" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="394.6160" RightMargin="275.3840" TopMargin="217.8080" BottomMargin="385.1920" ctype="SpriteObjectData">
            <Size X="290.0000" Y="37.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="539.6160" Y="403.6920" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5621" Y="0.6308" />
            <PreSize X="0.3021" Y="0.0578" />
            <FileData Type="Normal" Path="hall/res/store/qinshurjinbi.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="btn_1" ActionTag="-1825158649" Tag="1767" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="216.8960" RightMargin="567.1040" TopMargin="400.6277" BottomMargin="168.3723" ctype="SpriteObjectData">
            <Size X="176.0000" Y="71.0000" />
            <Children>
              <AbstractNodeData Name="num" ActionTag="1777051281" Tag="1772" IconVisible="False" LeftMargin="66.3474" RightMargin="91.6526" TopMargin="14.0286" BottomMargin="20.9714" FontSize="36" LabelText="1" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="18.0000" Y="36.0000" />
                <AnchorPoint ScaleX="0.4977" ScaleY="0.5037" />
                <Position X="75.3060" Y="39.1046" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4279" Y="0.5508" />
                <PreSize X="0.1023" Y="0.5070" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="304.8960" Y="203.8723" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3176" Y="0.3186" />
            <PreSize X="0.1833" Y="0.1109" />
            <FileData Type="Normal" Path="hall/res/store/jine.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="btn_2" ActionTag="1211448683" Tag="1768" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="398.6240" RightMargin="385.3760" TopMargin="404.2974" BottomMargin="164.7026" ctype="SpriteObjectData">
            <Size X="176.0000" Y="71.0000" />
            <Children>
              <AbstractNodeData Name="num" ActionTag="1412768191" Tag="1773" IconVisible="False" LeftMargin="40.8430" RightMargin="63.1570" TopMargin="15.6929" BottomMargin="19.3071" FontSize="36" LabelText="9999" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="72.0000" Y="36.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="76.8430" Y="37.3071" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4366" Y="0.5255" />
                <PreSize X="0.4091" Y="0.5070" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="486.6240" Y="200.2026" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5069" Y="0.3128" />
            <PreSize X="0.1833" Y="0.1109" />
            <FileData Type="Normal" Path="hall/res/store/jine.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="btn_3" ActionTag="-933092278" Tag="1769" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="580.3520" RightMargin="203.6480" TopMargin="404.2974" BottomMargin="164.7026" ctype="SpriteObjectData">
            <Size X="176.0000" Y="71.0000" />
            <Children>
              <AbstractNodeData Name="num" ActionTag="1441390420" Tag="1774" IconVisible="False" LeftMargin="40.6880" RightMargin="63.3120" TopMargin="16.2383" BottomMargin="18.7617" FontSize="36" LabelText="9999" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="72.0000" Y="36.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="76.6880" Y="36.7617" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4357" Y="0.5178" />
                <PreSize X="0.4091" Y="0.5070" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="668.3520" Y="200.2026" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.6962" Y="0.3128" />
            <PreSize X="0.1833" Y="0.1109" />
            <FileData Type="Normal" Path="hall/res/store/jine.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="btn_4" ActionTag="-51974562" Tag="1770" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="762.1760" RightMargin="21.8240" TopMargin="404.2974" BottomMargin="164.7026" ctype="SpriteObjectData">
            <Size X="176.0000" Y="71.0000" />
            <Children>
              <AbstractNodeData Name="num" ActionTag="-804530423" Tag="1775" IconVisible="False" LeftMargin="44.5991" RightMargin="59.4009" TopMargin="16.2382" BottomMargin="18.7618" FontSize="36" LabelText="9999" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="72.0000" Y="36.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="80.5991" Y="36.7618" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4579" Y="0.5178" />
                <PreSize X="0.4091" Y="0.5070" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="850.1760" Y="200.2026" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8856" Y="0.3128" />
            <PreSize X="0.1833" Y="0.1109" />
            <FileData Type="Normal" Path="hall/res/store/jine.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="btn_5" ActionTag="-1055087949" Tag="1771" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="943.9041" RightMargin="-159.9041" TopMargin="404.2978" BottomMargin="164.7022" ctype="SpriteObjectData">
            <Size X="176.0000" Y="71.0000" />
            <Children>
              <AbstractNodeData Name="num" ActionTag="1481631730" Tag="1776" IconVisible="False" LeftMargin="54.8577" RightMargin="49.1423" TopMargin="16.4564" BottomMargin="18.5436" FontSize="36" LabelText="9999" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="72.0000" Y="36.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="90.8577" Y="36.5436" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5162" Y="0.5147" />
                <PreSize X="0.4091" Y="0.5070" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1031.9041" Y="200.2022" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="1.0749" Y="0.3128" />
            <PreSize X="0.1833" Y="0.1109" />
            <FileData Type="Normal" Path="hall/res/store/jine.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="Text_25" ActionTag="-84991683" Tag="277" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="517.5680" RightMargin="242.4320" TopMargin="427.8080" BottomMargin="172.1920" FontSize="40" LabelText="Text Label" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="200.0000" Y="40.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="617.5680" Y="192.1920" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.6433" Y="0.3003" />
            <PreSize X="0.2083" Y="0.0625" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Image_jinbi" ActionTag="1778516297" Tag="1371" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="274.8640" RightMargin="489.1360" TopMargin="127.8055" BottomMargin="466.1945" LeftEage="64" RightEage="64" TopEage="15" BottomEage="15" Scale9OriginX="64" Scale9OriginY="15" Scale9Width="153" Scale9Height="35" ctype="ImageViewObjectData">
            <Size X="196.0000" Y="46.0000" />
            <Children>
              <AbstractNodeData Name="Text_81" ActionTag="2069966979" Tag="60" IconVisible="False" LeftMargin="55.8926" RightMargin="10.1074" TopMargin="7.0818" BottomMargin="5.9182" LabelText="20000" ctype="TextBMFontObjectData">
                <Size X="130.0000" Y="33.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="120.8926" Y="22.4182" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.6168" Y="0.4874" />
                <PreSize X="0.6633" Y="0.7174" />
                <LabelBMFontFile_CNB Type="Normal" Path="hall/res/rank_num.fnt" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="jinbi_163" ActionTag="1016247446" Tag="1372" IconVisible="False" LeftMargin="2.7401" RightMargin="148.2599" TopMargin="0.2970" BottomMargin="0.7030" ctype="SpriteObjectData">
                <Size X="45.0000" Y="45.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="25.2401" Y="23.2030" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1288" Y="0.5044" />
                <PreSize X="0.2296" Y="0.9783" />
                <FileData Type="Normal" Path="hall/res/store/jinbi.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="Image_25" ActionTag="-1607878348" Tag="1369" IconVisible="False" LeftMargin="16.1765" RightMargin="4.8235" TopMargin="-38.0413" BottomMargin="59.0413" LeftEage="57" RightEage="57" TopEage="8" BottomEage="8" Scale9OriginX="57" Scale9OriginY="8" Scale9Width="74" Scale9Height="12" ctype="ImageViewObjectData">
                <Size X="175.0000" Y="25.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="103.6765" Y="71.5413" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5290" Y="1.5552" />
                <PreSize X="0.8929" Y="0.5435" />
                <FileData Type="Normal" Path="hall/res/store/danqianjinbi.png" Plist="" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="372.8640" Y="489.1945" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3884" Y="0.7644" />
            <PreSize X="0.2042" Y="0.0719" />
            <FileData Type="Normal" Path="hall/res/store/jinbibox.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Image_baoxianbox" Visible="False" ActionTag="143798657" Tag="1375" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="855.9520" RightMargin="-91.9520" TopMargin="58.5156" BottomMargin="535.4844" LeftEage="64" RightEage="64" TopEage="15" BottomEage="15" Scale9OriginX="64" Scale9OriginY="15" Scale9Width="153" Scale9Height="35" ctype="ImageViewObjectData">
            <Size X="196.0000" Y="46.0000" />
            <Children>
              <AbstractNodeData Name="jinbi_163" ActionTag="111411262" Tag="1377" IconVisible="False" LeftMargin="-0.5000" RightMargin="151.5000" TopMargin="3.5000" BottomMargin="-2.5000" ctype="SpriteObjectData">
                <Size X="45.0000" Y="45.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="22.0000" Y="20.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1122" Y="0.4348" />
                <PreSize X="0.2296" Y="0.9783" />
                <FileData Type="Normal" Path="hall/res/store/jinbi.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_82" ActionTag="1537295794" Tag="1840" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="91.2780" RightMargin="59.7220" TopMargin="8.0644" BottomMargin="7.9356" FontSize="30" LabelText="0.1" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="45.0000" Y="30.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="113.7780" Y="22.9356" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5805" Y="0.4986" />
                <PreSize X="0.2296" Y="0.6522" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Image_baoxian" ActionTag="-44056787" Tag="1370" IconVisible="False" LeftMargin="-204.1604" RightMargin="197.1604" TopMargin="10.2303" BottomMargin="10.7697" LeftEage="66" RightEage="66" TopEage="8" BottomEage="8" Scale9OriginX="66" Scale9OriginY="8" Scale9Width="71" Scale9Height="9" ctype="ImageViewObjectData">
                <Size X="203.0000" Y="25.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="-102.6604" Y="23.2697" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="-0.5238" Y="0.5059" />
                <PreSize X="1.0357" Y="0.5435" />
                <FileData Type="Normal" Path="hall/res/store/baoxian_box.png" Plist="" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="953.9520" Y="558.4844" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.9937" Y="0.8726" />
            <PreSize X="0.2042" Y="0.0719" />
            <FileData Type="Normal" Path="hall/res/store/jinbibox.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="range_txt" ActionTag="-1862890396" Tag="1577" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="384.5920" RightMargin="235.4080" TopMargin="275.1938" BottomMargin="344.8062" FontSize="20" LabelText="当前支付渠道支付,支持金额范围xx-xx" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="340.0000" Y="20.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="554.5920" Y="354.8062" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5777" Y="0.5544" />
            <PreSize X="0.3542" Y="0.0313" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="ListView_Vip" Visible="False" ActionTag="-1293857523" Alpha="0" Tag="3814" IconVisible="False" LeftMargin="334.6221" RightMargin="5.3779" TopMargin="168.1700" BottomMargin="31.8300" TouchEnable="True" ClipAble="True" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" IsBounceEnabled="True" ScrollDirectionType="0" ItemMargin="6" DirectionType="Vertical" ctype="ListViewObjectData">
            <Size X="620.0000" Y="440.0000" />
            <AnchorPoint />
            <Position X="334.6221" Y="31.8300" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3486" Y="0.0497" />
            <PreSize X="0.6458" Y="0.6875" />
            <SingleColor A="255" R="255" G="255" B="255" />
            <FirstColor A="255" R="150" G="150" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="Image_vip_txt" Visible="False" ActionTag="-1497546498" Tag="3815" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="353.3600" RightMargin="10.6400" TopMargin="33.1150" BottomMargin="544.8850" LeftEage="196" RightEage="196" TopEage="20" BottomEage="20" Scale9OriginX="196" Scale9OriginY="20" Scale9Width="204" Scale9Height="22" ctype="ImageViewObjectData">
            <Size X="596.0000" Y="62.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="651.3600" Y="575.8850" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.6785" Y="0.8998" />
            <PreSize X="0.6208" Y="0.0969" />
            <FileData Type="Normal" Path="hall/res/store/juesaoTxt.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="fuzhi_vip" Visible="False" ActionTag="-2031953086" Tag="3816" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="784.9160" RightMargin="32.0840" TopMargin="182.7089" BottomMargin="402.2911" ctype="SpriteObjectData">
            <Size X="143.0000" Y="55.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="856.4160" Y="429.7911" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8921" Y="0.6715" />
            <PreSize X="0.1490" Y="0.0859" />
            <FileData Type="Normal" Path="hall/res/store/fuzhi.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="Text_vip_zhanghao" Visible="False" ActionTag="1590913787" Tag="3817" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="362.8640" RightMargin="337.1360" TopMargin="116.1686" BottomMargin="483.8314" FontSize="40" LabelText="我的账号：306" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="260.0000" Y="40.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="492.8640" Y="503.8314" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5134" Y="0.7872" />
            <PreSize X="0.2708" Y="0.0625" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="bg_left_0" ActionTag="-759321586" Tag="168" IconVisible="False" PositionPercentXEnabled="True" PercentWidthEnable="True" PercentWidthEnabled="True" LeftMargin="-159.9840" RightMargin="-159.9840" TopMargin="9.9913" BottomMargin="545.0087" LeftEage="15" RightEage="15" TopEage="15" BottomEage="15" Scale9OriginX="15" Scale9OriginY="15" Scale9Width="1250" Scale9Height="55" ctype="ImageViewObjectData">
            <Size X="1279.9680" Y="85.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="480.0000" Y="587.5087" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.9180" />
            <PreSize X="1.3333" Y="0.1328" />
            <FileData Type="Normal" Path="hall/res/store/bg_left.png" Plist="" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>