<GameFile>
  <PropertyGroup Name="record_item" Type="Layer" ID="41cb1119-0a51-4fcc-97b2-363ed9c8ad7b" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="868" ctype="GameLayerObjectData">
        <Size X="922.0000" Y="64.0000" />
        <Children>
          <AbstractNodeData Name="bg" ActionTag="1841260267" Tag="885" IconVisible="False" ctype="SpriteObjectData">
            <Size X="922.0000" Y="64.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="461.0000" Y="32.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="1.0000" Y="1.0000" />
            <FileData Type="MarkedSubImage" Path="hall/res/store/record_item_bg.png" Plist="hall/res/store.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="status" ActionTag="2032944928" Tag="888" IconVisible="False" LeftMargin="724.4800" RightMargin="97.5200" TopMargin="17.5000" BottomMargin="17.5000" FontSize="25" LabelText="發貨狀態" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="100.0000" Y="29.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="774.4800" Y="32.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8400" Y="0.5000" />
            <PreSize X="0.1085" Y="0.4531" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="name" ActionTag="-1474219606" Tag="887" IconVisible="False" LeftMargin="436.0000" RightMargin="436.0000" TopMargin="17.5000" BottomMargin="17.5000" FontSize="25" LabelText="商品" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="50.0000" Y="29.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="461.0000" Y="32.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="0.0542" Y="0.4531" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="time" ActionTag="979820653" Tag="886" IconVisible="False" LeftMargin="97.5200" RightMargin="724.4800" TopMargin="17.5000" BottomMargin="17.5000" FontSize="25" LabelText="兌換時間" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="100.0000" Y="29.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="147.5200" Y="32.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.1600" Y="0.5000" />
            <PreSize X="0.1085" Y="0.4531" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>