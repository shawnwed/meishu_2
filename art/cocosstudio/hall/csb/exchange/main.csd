<GameFile>
  <PropertyGroup Name="main" Type="Layer" ID="e5d5ede9-3da1-4b1b-b538-0875b996c031" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="327" ctype="GameLayerObjectData">
        <Size X="1171.0000" Y="658.0000" />
        <Children>
          <AbstractNodeData Name="Image_1" ActionTag="55933561" Tag="3320" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" LeftMargin="3.8058" RightMargin="3.8058" TopMargin="7.6657" BottomMargin="7.6657" Scale9Enable="True" LeftEage="179" RightEage="179" TopEage="136" BottomEage="112" Scale9OriginX="179" Scale9OriginY="136" Scale9Width="419" Scale9Height="342" ctype="ImageViewObjectData">
            <Size X="1163.3884" Y="642.6686" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="585.5000" Y="329.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="0.9935" Y="0.9767" />
            <FileData Type="Normal" Path="hall/res/info/bg1.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="close" ActionTag="-1406112145" Tag="410" IconVisible="False" LeftMargin="1105.0851" RightMargin="17.9149" TopMargin="18.1116" BottomMargin="593.8884" ctype="SpriteObjectData">
            <Size X="48.0000" Y="46.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1129.0851" Y="616.8884" />
            <Scale ScaleX="0.8000" ScaleY="0.8000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.9642" Y="0.9375" />
            <PreSize X="0.0410" Y="0.0699" />
            <FileData Type="MarkedSubImage" Path="game_public/res/btn_close.png" Plist="game_public/res/game.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>