<GameFile>
  <PropertyGroup Name="blank" Type="Layer" ID="2ad8bd3a-eb42-4090-8c87-51da80963fb2" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="553" ctype="GameLayerObjectData">
        <Size X="858.0000" Y="490.0000" />
        <Children>
          <AbstractNodeData Name="Panel_2" ActionTag="-649390109" Tag="559" IconVisible="False" LeftMargin="297.8204" RightMargin="360.1796" TopMargin="84.5039" BottomMargin="205.4961" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="200.0000" Y="200.0000" />
            <Children>
              <AbstractNodeData Name="txt" ActionTag="-2040634329" Tag="555" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="36.0000" RightMargin="36.0000" TopMargin="83.5000" BottomMargin="83.5000" FontSize="32" LabelText="暂无消息" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="128.0000" Y="33.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="100.0000" Y="100.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="127" G="127" B="127" />
                <PrePosition X="0.5000" Y="0.5000" />
                <PreSize X="0.6400" Y="0.1650" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position X="297.8204" Y="205.4961" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3471" Y="0.4194" />
            <PreSize X="0.2331" Y="0.4082" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>