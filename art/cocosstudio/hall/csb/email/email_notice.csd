<GameFile>
  <PropertyGroup Name="email_notice" Type="Layer" ID="77533112-c698-4eed-9737-586d80146430" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="36" ctype="GameLayerObjectData">
        <Size X="1010.0000" Y="600.0000" />
        <Children>
          <AbstractNodeData Name="title" ActionTag="1738128363" Tag="43" IconVisible="False" PositionPercentXEnabled="True" VerticalEdge="TopEdge" LeftMargin="433.0000" RightMargin="433.0000" TopMargin="97.1456" BottomMargin="460.8544" FontSize="36" LabelText="我是标题" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="144.0000" Y="42.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="505.0000" Y="481.8544" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="215" B="108" />
            <PrePosition X="0.5000" Y="0.8031" />
            <PreSize X="0.1426" Y="0.0700" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="content" ActionTag="-762032048" Tag="37" IconVisible="False" PositionPercentXEnabled="True" VerticalEdge="TopEdge" LeftMargin="137.5000" RightMargin="137.5000" TopMargin="187.8112" BottomMargin="276.1888" FontSize="30" LabelText="亲爱的玩家：&#xA;&#xA;就发送到发送到防到防守打法就送就发送到sss到防到防&#xA;到防守就发送到发送法就发送到发送" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="735.0000" Y="136.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="1.0000" />
            <Position X="505.0000" Y="412.1888" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.6870" />
            <PreSize X="0.7277" Y="0.2267" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="from" ActionTag="331980185" Tag="38" IconVisible="False" VerticalEdge="BottomEdge" LeftMargin="714.0000" RightMargin="140.0000" TopMargin="424.2990" BottomMargin="82.7010" FontSize="26" LabelText="&#xA;2016-05-02&#xA;游戏运营中心" HorizontalAlignmentType="HT_Right" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="156.0000" Y="93.0000" />
            <AnchorPoint ScaleX="1.0000" ScaleY="1.0000" />
            <Position X="870.0000" Y="175.7010" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8614" Y="0.2928" />
            <PreSize X="0.1545" Y="0.1550" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>