<GameFile>
  <PropertyGroup Name="email_content" Type="Layer" ID="77533112-c698-4eed-9737-586d80146430" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="36" ctype="GameLayerObjectData">
        <Size X="880.0000" Y="490.0000" />
        <Children>
          <AbstractNodeData Name="content" ActionTag="-762032048" Tag="37" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="76.0000" RightMargin="76.0000" TopMargin="78.8124" BottomMargin="318.1876" FontSize="26" LabelText="亲爱的玩家：&#xA;&#xA;就发送到发送到防守打法就发送到发送到防守打法就发送到发送" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="728.0000" Y="93.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="1.0000" />
            <Position X="440.0000" Y="411.1876" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.8392" />
            <PreSize X="0.8273" Y="0.1898" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="from" ActionTag="331980185" Tag="38" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="701.8240" RightMargin="22.1760" TopMargin="338.3450" BottomMargin="58.6550" FontSize="26" LabelText="&#xA;2016-05-02&#xA;游戏运营中心" HorizontalAlignmentType="HT_Right" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="156.0000" Y="93.0000" />
            <AnchorPoint ScaleX="1.0000" ScaleY="1.0000" />
            <Position X="857.8240" Y="151.6550" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.9748" Y="0.3095" />
            <PreSize X="0.1773" Y="0.1898" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="title" ActionTag="1738128363" Tag="43" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="368.0000" RightMargin="368.0000" TopMargin="11.1516" BottomMargin="436.8484" FontSize="36" LabelText="我是标题" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="144.0000" Y="42.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="440.0000" Y="457.8484" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.9344" />
            <PreSize X="0.1636" Y="0.0857" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>