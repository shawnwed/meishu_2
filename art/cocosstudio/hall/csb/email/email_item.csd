<GameFile>
  <PropertyGroup Name="email_item" Type="Layer" ID="883a8859-af91-4871-9714-7a52c7e4b8fa" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="445" ctype="GameLayerObjectData">
        <Size X="935.0000" Y="126.0000" />
        <Children>
          <AbstractNodeData Name="bg" ActionTag="-1034738148" Tag="446" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="9.1330" RightMargin="10.8669" TopMargin="13.7312" BottomMargin="15.2688" ctype="SpriteObjectData">
            <Size X="915.0000" Y="97.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="9.1330" Y="63.7688" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0098" Y="0.5061" />
            <PreSize X="0.9786" Y="0.7698" />
            <FileData Type="MarkedSubImage" Path="hall/res/task/email_item.png" Plist="hall/res/task.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="icon" ActionTag="916219706" Tag="448" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="27.6837" RightMargin="830.3163" TopMargin="22.1974" BottomMargin="26.8026" ctype="SpriteObjectData">
            <Size X="77.0000" Y="77.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="66.1837" Y="65.3026" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0708" Y="0.5183" />
            <PreSize X="0.0824" Y="0.6111" />
            <FileData Type="MarkedSubImage" Path="hall/res/task/emai_no_read.png" Plist="hall/res/task.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="title" ActionTag="1233420154" Tag="449" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="132.0679" RightMargin="698.9321" TopMargin="30.4361" BottomMargin="69.5639" FontSize="26" LabelText="系统通知" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="104.0000" Y="26.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="132.0679" Y="82.5639" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="201" G="95" B="0" />
            <PrePosition X="0.1412" Y="0.6553" />
            <PreSize X="0.1112" Y="0.2063" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="time" ActionTag="2060808639" Tag="451" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="254.5678" RightMargin="452.4323" TopMargin="31.0742" BottomMargin="70.9258" FontSize="24" LabelText="2016-05-12 08:26:12" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="228.0000" Y="24.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="254.5678" Y="82.9258" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="105" G="50" B="32" />
            <PrePosition X="0.2723" Y="0.6581" />
            <PreSize X="0.2439" Y="0.1905" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="from" ActionTag="-94620203" Tag="452" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="133.1354" RightMargin="671.8646" TopMargin="66.2128" BottomMargin="33.7872" FontSize="26" LabelText="来自：系统" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="130.0000" Y="26.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="133.1354" Y="46.7872" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="105" G="50" B="32" />
            <PrePosition X="0.1424" Y="0.3713" />
            <PreSize X="0.1390" Y="0.2063" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="time_0" ActionTag="-543621813" Tag="73" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="819.3857" RightMargin="55.6143" TopMargin="43.9644" BottomMargin="52.0356" FontSize="30" LabelText="查看" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="60.0000" Y="30.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="819.3857" Y="67.0356" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8763" Y="0.5320" />
            <PreSize X="0.0642" Y="0.2381" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>