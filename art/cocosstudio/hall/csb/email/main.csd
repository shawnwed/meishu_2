<GameFile>
  <PropertyGroup Name="main" Type="Scene" ID="9bdd0d79-93ae-4304-b136-d22f2d47d37f" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Scene" Tag="4240" ctype="GameNodeObjectData">
        <Size X="960.0000" Y="640.0000" />
        <Children>
          <AbstractNodeData Name="bg" ActionTag="763756596" Tag="4241" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" TouchEnable="True" Scale9Enable="True" LeftEage="422" RightEage="422" TopEage="237" BottomEage="237" Scale9OriginX="422" Scale9OriginY="237" Scale9Width="436" Scale9Height="246" ctype="ImageViewObjectData">
            <Size X="960.0000" Y="640.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="480.0000" Y="320.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="1.0000" Y="1.0000" />
            <FileData Type="Normal" Path="bg/bg_info.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="bg_info_content" ActionTag="719323636" Tag="892" IconVisible="False" PositionPercentXEnabled="True" RightMargin="662.0200" TopMargin="73.9800" LeftEage="25" RightEage="25" TopEage="25" BottomEage="85" Scale9OriginX="25" Scale9OriginY="25" Scale9Width="248" Scale9Height="456" ctype="ImageViewObjectData">
            <Size X="297.9800" Y="566.0200" />
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.3104" Y="0.8844" />
            <FileData Type="Normal" Path="bg/bg_info_content.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="email" Visible="False" ActionTag="1350470745" Tag="4686" IconVisible="False" PercentWidthEnable="True" PercentWidthEnabled="True" VerticalEdge="BothEdge" LeftMargin="305.3786" RightMargin="-17.3786" TopMargin="102.4000" TouchEnable="True" ClipAble="False" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="672.0000" Y="537.6000" />
            <Children>
              <AbstractNodeData Name="no_content" ActionTag="1512385616" Tag="4687" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="259.7632" RightMargin="252.2368" TopMargin="199.2333" BottomMargin="298.3667" FontSize="40" LabelText="暫無消息" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="160.0000" Y="40.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="339.7632" Y="318.3667" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5056" Y="0.5922" />
                <PreSize X="0.2381" Y="0.0744" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleY="0.5000" />
            <Position X="305.3786" Y="268.8000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3181" Y="0.4200" />
            <PreSize X="0.7000" Y="0.8400" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="notice" Visible="False" ActionTag="-2140037903" Tag="4688" IconVisible="False" PercentWidthEnable="True" PercentWidthEnabled="True" VerticalEdge="BothEdge" LeftMargin="305.3786" RightMargin="-17.3786" TopMargin="100.4500" BottomMargin="-0.0300" TouchEnable="True" ClipAble="False" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="672.0000" Y="539.5800" />
            <Children>
              <AbstractNodeData Name="pageview" ActionTag="65994406" Tag="4689" IconVisible="False" PositionPercentXEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" TouchEnable="True" ClipAble="True" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ScrollDirectionType="0" ctype="PageViewObjectData">
                <Size X="672.0000" Y="539.5800" />
                <Children>
                  <AbstractNodeData Name="Panel_3_0" ActionTag="1287489373" Tag="4690" IconVisible="False" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                    <Size X="672.0000" Y="539.5800" />
                    <Children>
                      <AbstractNodeData Name="Text_5" ActionTag="-791339982" Tag="4691" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="238.1248" RightMargin="273.8752" TopMargin="234.0882" BottomMargin="265.4918" FontSize="40" LabelText="暂无公告" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                        <Size X="160.0000" Y="40.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="318.1248" Y="285.4918" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.4734" Y="0.5291" />
                        <PreSize X="0.2381" Y="0.0741" />
                        <OutlineColor A="255" R="255" G="0" B="0" />
                        <ShadowColor A="255" R="110" G="110" B="110" />
                      </AbstractNodeData>
                    </Children>
                    <AnchorPoint />
                    <Position />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition />
                    <PreSize X="1.0000" Y="1.0000" />
                    <SingleColor A="255" R="150" G="200" B="255" />
                    <FirstColor A="255" R="150" G="200" B="255" />
                    <EndColor A="255" R="255" G="255" B="255" />
                    <ColorVector ScaleY="1.0000" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="1.0000" />
                <Position X="672.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="1.0000" />
                <PreSize X="1.0000" Y="1.0000" />
                <SingleColor A="255" R="150" G="150" B="100" />
                <FirstColor A="255" R="150" G="150" B="100" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleY="0.5000" />
            <Position X="305.3786" Y="269.7600" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3181" Y="0.4215" />
            <PreSize X="0.7000" Y="0.8431" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="feed_back" ActionTag="2022675347" Tag="4745" IconVisible="False" PercentWidthEnable="True" PercentWidthEnabled="True" LeftMargin="299.8724" RightMargin="-11.8724" TopMargin="98.7299" BottomMargin="3.6701" TouchEnable="True" ClipAble="False" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="672.0000" Y="537.6000" />
            <Children>
              <AbstractNodeData Name="no_content" ActionTag="654395519" Tag="4750" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="214.4704" RightMargin="297.5296" TopMargin="206.4371" BottomMargin="291.1629" FontSize="40" LabelText="暫無消息" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="160.0000" Y="40.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="294.4704" Y="311.1629" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4382" Y="0.5788" />
                <PreSize X="0.2381" Y="0.0744" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="p_input" ActionTag="-1609719986" Tag="7615" IconVisible="False" PercentWidthEnable="True" PercentWidthEnabled="True" LeftMargin="33.1766" RightMargin="138.8554" TopMargin="466.6605" BottomMargin="10.9395" Scale9Enable="True" LeftEage="333" RightEage="333" TopEage="20" BottomEage="20" Scale9OriginX="333" Scale9OriginY="20" Scale9Width="344" Scale9Height="20" ctype="ImageViewObjectData">
                <Size X="499.9680" Y="60.0000" />
                <Children>
                  <AbstractNodeData Name="input" ActionTag="-614724465" Tag="4748" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="10.7104" RightMargin="9.2576" TopMargin="9.4080" BottomMargin="14.5920" TouchEnable="True" FontSize="24" IsCustomSize="True" LabelText="" PlaceHolderText="请输入反馈内容，客服会尽快回复的" MaxLengthEnable="True" MaxLengthText="50" ctype="TextFieldObjectData">
                    <Size X="480.0000" Y="36.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="10.7104" Y="32.5920" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.0214" Y="0.5432" />
                    <PreSize X="0.9601" Y="0.6000" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="send" ActionTag="-243458388" Tag="4749" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="511.0612" RightMargin="-201.0932" TopMargin="-10.5900" BottomMargin="-8.4100" ctype="SpriteObjectData">
                    <Size X="190.0000" Y="79.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="1.0000" />
                    <Position X="606.0612" Y="70.5900" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="1.2122" Y="1.1765" />
                    <PreSize X="0.3800" Y="1.3167" />
                    <FileData Type="MarkedSubImage" Path="lan/cn/feedback_send.png" Plist="lan/cn/game.plist" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleY="0.5000" />
                <Position X="33.1766" Y="40.9395" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0494" Y="0.0762" />
                <PreSize X="0.7440" Y="0.1116" />
                <FileData Type="Normal" Path="hall/res/task/feedback_input_bg.png" Plist="" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleY="0.5000" />
            <Position X="299.8724" Y="272.4701" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3124" Y="0.4257" />
            <PreSize X="0.7000" Y="0.8400" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="share" Visible="False" ActionTag="1999702639" Tag="1273" IconVisible="False" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" VerticalEdge="BothEdge" LeftMargin="2.9492" RightMargin="-2.9492" TopMargin="93.7600" BottomMargin="75.1360" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="960.0000" Y="471.1040" />
            <Children>
              <AbstractNodeData Name="Text_6" ActionTag="-740557534" Tag="1296" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="66.0000" RightMargin="642.0000" TopMargin="-34.4459" BottomMargin="477.5499" FontSize="28" LabelText="將遊戲分享給好友：" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="252.0000" Y="28.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="192.0000" Y="491.5499" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="123" G="167" B="235" />
                <PrePosition X="0.2000" Y="1.0434" />
                <PreSize X="0.2625" Y="0.0594" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="qrcode" ActionTag="1818884131" Tag="1295" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="185.1280" RightMargin="728.8720" TopMargin="146.5503" BottomMargin="278.5537" ctype="SpriteObjectData">
                <Size X="46.0000" Y="46.0000" />
                <Children>
                  <AbstractNodeData Name="title" ActionTag="1600282954" Tag="1297" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="23.0000" RightMargin="-145.0000" TopMargin="39.3094" BottomMargin="-21.3094" FontSize="28" LabelText="掃碼下載遊戲" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="168.0000" Y="28.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="107.0000" Y="-7.3094" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="2.3261" Y="-0.1589" />
                    <PreSize X="3.6522" Y="0.6087" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="208.1280" Y="301.5537" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2168" Y="0.6401" />
                <PreSize X="0.0479" Y="0.0976" />
                <FileData Type="Default" Path="Default/Sprite.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="downloadurl" ActionTag="-1188788958" Tag="1274" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="-104.4880" RightMargin="54.4880" TopMargin="399.7577" BottomMargin="11.3463" ctype="SpriteObjectData">
                <Size X="1010.0000" Y="60.0000" />
                <Children>
                  <AbstractNodeData Name="url" ActionTag="-1833914195" Tag="1278" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="30.0000" RightMargin="672.0000" TopMargin="14.0080" BottomMargin="17.9920" FontSize="28" LabelText="游戏下载地址：；xxxxxx" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="308.0000" Y="28.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="30.0000" Y="31.9920" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="123" G="167" B="235" />
                    <PrePosition X="0.0297" Y="0.5332" />
                    <PreSize X="0.3050" Y="0.4667" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="copy" ActionTag="-659721227" Tag="1276" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="1034.5398" RightMargin="-214.5398" TopMargin="-1.5840" BottomMargin="-17.4160" ctype="SpriteObjectData">
                    <Size X="190.0000" Y="79.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="1.0000" />
                    <Position X="1129.5398" Y="61.5840" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="1.1184" Y="1.0264" />
                    <PreSize X="0.1881" Y="1.3167" />
                    <FileData Type="MarkedSubImage" Path="lan/cn/share_copy.png" Plist="lan/cn/game.plist" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="1.0000" />
                <Position X="400.5120" Y="71.3463" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4172" Y="0.1514" />
                <PreSize X="1.0521" Y="0.1274" />
                <FileData Type="Normal" Path="hall/res/task/feedback_input_bg.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="wechat" Visible="False" ActionTag="-378614952" Tag="1298" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="85.0000" RightMargin="661.0000" TopMargin="46.1088" BottomMargin="210.9952" ctype="SpriteObjectData">
                <Size X="214.0000" Y="214.0000" />
                <Children>
                  <AbstractNodeData Name="title" ActionTag="586087097" Tag="1299" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="51.0000" RightMargin="51.0000" TopMargin="234.0000" BottomMargin="-48.0000" FontSize="28" LabelText="微信分享" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="112.0000" Y="28.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="107.0000" Y="-34.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="-0.1589" />
                    <PreSize X="0.5234" Y="0.1308" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="192.0000" Y="317.9952" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2000" Y="0.6750" />
                <PreSize X="0.2229" Y="0.4543" />
                <FileData Type="MarkedSubImage" Path="hall/res/task/share_wechat.png" Plist="hall/res/task.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="qq" ActionTag="-1093767053" Tag="1300" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="754.4400" RightMargin="103.5600" TopMargin="108.5503" BottomMargin="240.5537" ctype="SpriteObjectData">
                <Size X="102.0000" Y="122.0000" />
                <Children>
                  <AbstractNodeData Name="title" ActionTag="1601029949" Tag="1301" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="9.0000" RightMargin="9.0000" TopMargin="142.0000" BottomMargin="-48.0000" FontSize="28" LabelText="QQ分享" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="84.0000" Y="28.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="51.0000" Y="-34.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="-0.2787" />
                    <PreSize X="0.8235" Y="0.2295" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="805.4400" Y="301.5537" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.8390" Y="0.6401" />
                <PreSize X="0.1063" Y="0.2590" />
                <FileData Type="MarkedSubImage" Path="hall/res/task/share_qq.png" Plist="hall/res/task.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="msg" ActionTag="672987854" Tag="1302" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="401.1280" RightMargin="344.8720" TopMargin="62.5503" BottomMargin="194.5537" ctype="SpriteObjectData">
                <Size X="214.0000" Y="214.0000" />
                <Children>
                  <AbstractNodeData Name="title" ActionTag="1635740494" Tag="1303" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="51.0000" RightMargin="51.0000" TopMargin="234.0000" BottomMargin="-48.0000" FontSize="28" LabelText="簡訊分享" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="112.0000" Y="28.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="107.0000" Y="-34.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="-0.1589" />
                    <PreSize X="0.5234" Y="0.1308" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="508.1280" Y="301.5537" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5293" Y="0.6401" />
                <PreSize X="0.2229" Y="0.4543" />
                <FileData Type="MarkedSubImage" Path="hall/res/task/share_msg.png" Plist="hall/res/task.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position X="2.9492" Y="75.1360" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0031" Y="0.1174" />
            <PreSize X="1.0000" Y="0.7361" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="top" ActionTag="-1473542000" Tag="99" IconVisible="True" PercentWidthEnable="True" PercentWidthEnabled="True" TopMargin="-79.6800" BottomMargin="624.6800" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="True" ctype="ProjectNodeObjectData">
            <Size X="960.0000" Y="95.0000" />
            <AnchorPoint />
            <Position Y="624.6800" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition Y="0.9761" />
            <PreSize X="1.0000" Y="0.1484" />
            <FileData Type="Normal" Path="game_public/csb/hall_top.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="tab" ActionTag="2016811688" Tag="482" IconVisible="False" PercentHeightEnable="True" PercentHeightEnabled="True" LeftMargin="-0.1905" RightMargin="642.6205" TopMargin="98.1760" TouchEnable="True" ClipAble="False" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="317.5700" Y="541.8240" />
            <Children>
              <AbstractNodeData Name="tab2" ActionTag="-454170458" Tag="1270" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="28.3258" RightMargin="29.2442" TopMargin="16.8592" BottomMargin="416.9648" ctype="SpriteObjectData">
                <Size X="260.0000" Y="108.0000" />
                <Children>
                  <AbstractNodeData Name="line" ActionTag="2081393592" Tag="1271" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="-0.3125" RightMargin="-1.6875" TopMargin="-0.8424" BottomMargin="0.8424" ctype="SpriteObjectData">
                    <Size X="262.0000" Y="108.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="130.6875" Y="54.8424" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5026" Y="0.5078" />
                    <PreSize X="1.0077" Y="1.0000" />
                    <FileData Type="MarkedSubImage" Path="hall/res/info/tab_line.png" Plist="hall/res/myinfo.plist" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="txt" ActionTag="1742863675" Tag="1272" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="70.0000" RightMargin="70.0000" TopMargin="32.4120" BottomMargin="45.5880" FontSize="30" LabelText="分    享" ShadowOffsetX="-1.0000" ShadowOffsetY="1.0000" ctype="TextObjectData">
                    <Size X="120.0000" Y="30.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="130.0000" Y="60.5880" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5610" />
                    <PreSize X="0.4615" Y="0.2778" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="0" G="0" B="0" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="158.3258" Y="470.9648" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4986" Y="0.8692" />
                <PreSize X="0.8187" Y="0.1993" />
                <FileData Type="MarkedSubImage" Path="hall/res/info/tab_btn_1.png" Plist="hall/res/myinfo.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="tab1" ActionTag="-1211199481" Tag="2448" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="25.0460" RightMargin="32.5240" TopMargin="20.0101" BottomMargin="413.8138" ctype="SpriteObjectData">
                <Size X="260.0000" Y="108.0000" />
                <Children>
                  <AbstractNodeData Name="line_0" ActionTag="2061494721" Tag="3396" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="0.5278" RightMargin="-2.5278" TopMargin="-1.6848" BottomMargin="1.6848" ctype="SpriteObjectData">
                    <Size X="262.0000" Y="108.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="131.5278" Y="55.6848" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5059" Y="0.5156" />
                    <PreSize X="1.0077" Y="1.0000" />
                    <FileData Type="MarkedSubImage" Path="hall/res/info/tab_line.png" Plist="hall/res/myinfo.plist" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="txt" ActionTag="789653683" Tag="2449" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="70.8320" RightMargin="69.1680" TopMargin="30.5760" BottomMargin="47.4240" FontSize="30" LabelText="郵    件" ShadowOffsetX="-1.0000" ShadowOffsetY="1.0000" ctype="TextObjectData">
                    <Size X="120.0000" Y="30.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="130.8320" Y="62.4240" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5032" Y="0.5780" />
                    <PreSize X="0.4615" Y="0.2778" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="0" G="0" B="0" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="155.0460" Y="467.8138" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4882" Y="0.8634" />
                <PreSize X="0.8187" Y="0.1993" />
                <FileData Type="MarkedSubImage" Path="hall/res/info/tab_btn_1.png" Plist="hall/res/myinfo.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="tab3" ActionTag="-402239863" Tag="2445" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="29.9644" RightMargin="27.6056" TopMargin="146.5063" BottomMargin="287.3177" ctype="SpriteObjectData">
                <Size X="260.0000" Y="108.0000" />
                <Children>
                  <AbstractNodeData Name="line" ActionTag="1057893767" Tag="2447" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="-1.1549" RightMargin="-0.8451" TopMargin="-0.8424" BottomMargin="0.8424" ctype="SpriteObjectData">
                    <Size X="262.0000" Y="108.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="129.8451" Y="54.8424" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.4994" Y="0.5078" />
                    <PreSize X="1.0077" Y="1.0000" />
                    <FileData Type="MarkedSubImage" Path="hall/res/info/tab_line.png" Plist="hall/res/myinfo.plist" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="txt" ActionTag="-1434764837" Tag="2446" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="69.0640" RightMargin="70.9360" TopMargin="33.3516" BottomMargin="44.6484" FontSize="30" LabelText="公    告" ShadowOffsetX="-1.0000" ShadowOffsetY="1.0000" ctype="TextObjectData">
                    <Size X="120.0000" Y="30.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="129.0640" Y="59.6484" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.4964" Y="0.5523" />
                    <PreSize X="0.4615" Y="0.2778" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="0" G="0" B="0" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="159.9644" Y="341.3177" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5037" Y="0.6299" />
                <PreSize X="0.8187" Y="0.1993" />
                <FileData Type="MarkedSubImage" Path="hall/res/info/tab_btn_1.png" Plist="hall/res/myinfo.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="tab4" ActionTag="1681398978" Tag="2442" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="26.6855" RightMargin="30.8845" TopMargin="267.9551" BottomMargin="165.8688" ctype="SpriteObjectData">
                <Size X="260.0000" Y="108.0000" />
                <Children>
                  <AbstractNodeData Name="line" ActionTag="315573392" Tag="2443" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="-1.1549" RightMargin="-0.8451" TopMargin="-1.6848" BottomMargin="1.6848" ctype="SpriteObjectData">
                    <Size X="262.0000" Y="108.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="129.8451" Y="55.6848" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.4994" Y="0.5156" />
                    <PreSize X="1.0077" Y="1.0000" />
                    <FileData Type="MarkedSubImage" Path="hall/res/info/tab_line.png" Plist="hall/res/myinfo.plist" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="txt" ActionTag="96923206" Tag="2444" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="70.0000" RightMargin="70.0000" TopMargin="28.6428" BottomMargin="49.3572" FontSize="30" LabelText="反    饋" ShadowOffsetX="-1.0000" ShadowOffsetY="1.0000" ctype="TextObjectData">
                    <Size X="120.0000" Y="30.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="130.0000" Y="64.3572" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5959" />
                    <PreSize X="0.4615" Y="0.2778" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="0" G="0" B="0" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="156.6855" Y="219.8688" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4934" Y="0.4058" />
                <PreSize X="0.8187" Y="0.1993" />
                <FileData Type="MarkedSubImage" Path="hall/res/info/tab_btn_2.png" Plist="hall/res/myinfo.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.0006" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.3308" Y="0.8466" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>