<GameFile>
  <PropertyGroup Name="email_award_get" Type="Layer" ID="74c8f6da-7de6-482e-b497-5623df5655d3" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="47" ctype="GameLayerObjectData">
        <Size X="880.0000" Y="137.0000" />
        <Children>
          <AbstractNodeData Name="tool1" ActionTag="1917986500" Tag="48" IconVisible="False" LeftMargin="15.2494" RightMargin="754.7506" TopMargin="12.0000" BottomMargin="15.0000" ctype="SpriteObjectData">
            <Size X="110.0000" Y="110.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="70.2494" Y="70.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0798" Y="0.5109" />
            <PreSize X="0.1250" Y="0.8029" />
            <FileData Type="MarkedSubImage" Path="hall/res/task/tool1.png" Plist="hall/res/task.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="txt1" ActionTag="224508218" Tag="50" IconVisible="False" LeftMargin="43.2494" RightMargin="788.7506" TopMargin="92.0000" BottomMargin="19.0000" FontSize="22" LabelText="x100" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="48.0000" Y="26.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="67.2494" Y="32.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0764" Y="0.2336" />
            <PreSize X="0.0545" Y="0.1898" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="tool2" ActionTag="-1041145010" Tag="118" IconVisible="False" LeftMargin="129.2494" RightMargin="640.7506" TopMargin="12.0000" BottomMargin="15.0000" ctype="SpriteObjectData">
            <Size X="110.0000" Y="110.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="184.2494" Y="70.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.2094" Y="0.5109" />
            <PreSize X="0.1250" Y="0.8029" />
            <FileData Type="MarkedSubImage" Path="hall/res/task/tool1.png" Plist="hall/res/task.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="txt2" ActionTag="-1976575871" Tag="119" IconVisible="False" LeftMargin="157.2494" RightMargin="674.7506" TopMargin="91.0000" BottomMargin="20.0000" FontSize="22" LabelText="x100" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="48.0000" Y="26.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="181.2494" Y="33.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.2060" Y="0.2409" />
            <PreSize X="0.0545" Y="0.1898" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="tool3" ActionTag="333826961" Tag="120" IconVisible="False" LeftMargin="244.2494" RightMargin="525.7506" TopMargin="12.0000" BottomMargin="15.0000" ctype="SpriteObjectData">
            <Size X="110.0000" Y="110.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="299.2494" Y="70.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3401" Y="0.5109" />
            <PreSize X="0.1250" Y="0.8029" />
            <FileData Type="MarkedSubImage" Path="hall/res/task/tool1.png" Plist="hall/res/task.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="txt3" ActionTag="765750626" Tag="121" IconVisible="False" LeftMargin="272.2494" RightMargin="559.7506" TopMargin="92.0000" BottomMargin="19.0000" FontSize="22" LabelText="x100" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="48.0000" Y="26.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="296.2494" Y="32.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3366" Y="0.2336" />
            <PreSize X="0.0545" Y="0.1898" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="email_get" ActionTag="-317111206" Tag="49" IconVisible="False" LeftMargin="674.2495" RightMargin="15.7505" TopMargin="52.8401" BottomMargin="11.1599" ctype="SpriteObjectData">
            <Size X="190.0000" Y="73.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="769.2495" Y="47.6599" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8741" Y="0.3479" />
            <PreSize X="0.2159" Y="0.5328" />
            <FileData Type="MarkedSubImage" Path="lan/cn/email_get.png" Plist="lan/cn/game.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>