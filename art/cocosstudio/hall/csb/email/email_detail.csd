<GameFile>
  <PropertyGroup Name="email_detail" Type="Layer" ID="002a44ba-7a76-4ec6-afb7-24065c62de52" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="440" ctype="GameLayerObjectData">
        <Size X="774.0000" Y="469.0000" />
        <Children>
          <AbstractNodeData Name="task_bg_1" ActionTag="164400909" Tag="441" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-70.5000" RightMargin="-70.5000" TopMargin="-53.5000" BottomMargin="-53.5000" ctype="SpriteObjectData">
            <Size X="915.0000" Y="576.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="387.0000" Y="234.5000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="1.1822" Y="1.2281" />
            <FileData Type="Normal" Path="hall/res/bingphone/bg.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="title" ActionTag="1780538525" Tag="647" IconVisible="False" LeftMargin="315.8660" RightMargin="298.1340" TopMargin="-22.2591" BottomMargin="451.2591" FontSize="40" LabelText="系统消息" ShadowOffsetX="1.0000" ShadowOffsetY="-1.0000" ctype="TextObjectData">
            <Size X="160.0000" Y="40.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="395.8660" Y="471.2591" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="227" B="178" />
            <PrePosition X="0.5115" Y="1.0048" />
            <PreSize X="0.2067" Y="0.0853" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="0" G="0" B="0" />
          </AbstractNodeData>
          <AbstractNodeData Name="content" ActionTag="1407887778" Tag="48" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="4.2504" RightMargin="97.7496" TopMargin="92.1245" BottomMargin="277.8755" FontSize="32" LabelText="亲爱的玩家：&#xA;&#xA;就发送到发送到防守打法就发送到发送到防守打" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="672.0000" Y="99.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="1.0000" />
            <Position X="340.2504" Y="376.8755" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="81" G="70" B="69" />
            <PrePosition X="0.4396" Y="0.8036" />
            <PreSize X="0.8682" Y="0.2111" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="from" ActionTag="-1198646852" Tag="49" IconVisible="False" LeftMargin="568.0451" RightMargin="13.9549" TopMargin="361.6277" BottomMargin="8.3723" FontSize="32" LabelText="&#xA;2016-05-02&#xA;游戏运营中心" HorizontalAlignmentType="HT_Right" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="192.0000" Y="99.0000" />
            <AnchorPoint ScaleX="1.0000" ScaleY="1.0000" />
            <Position X="760.0451" Y="107.3723" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="81" G="70" B="69" />
            <PrePosition X="0.9820" Y="0.2289" />
            <PreSize X="0.2481" Y="0.2111" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="close" ActionTag="-944140186" Tag="359" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="795.2853" RightMargin="-101.2853" TopMargin="-67.4206" BottomMargin="455.4206" ctype="SpriteObjectData">
            <Size X="80.0000" Y="81.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="835.2853" Y="495.9206" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="1.0792" Y="1.0574" />
            <PreSize X="0.1034" Y="0.1727" />
            <FileData Type="Normal" Path="game_public/res/user_info_close.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="title_line_6" ActionTag="1478191057" Tag="361" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="144.8662" RightMargin="488.1338" TopMargin="-10.6248" BottomMargin="457.6248" ctype="SpriteObjectData">
            <Size X="141.0000" Y="22.0000" />
            <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
            <Position X="285.8662" Y="468.6248" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3693" Y="0.9992" />
            <PreSize X="0.1822" Y="0.0469" />
            <FileData Type="MarkedSubImage" Path="game_public/res/title_line.png" Plist="game_public/res/game.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="title_line_6_0" ActionTag="-1630829101" Tag="360" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="503.8862" RightMargin="129.1138" TopMargin="-10.6248" BottomMargin="457.6248" FlipX="True" ctype="SpriteObjectData">
            <Size X="141.0000" Y="22.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="503.8862" Y="468.6248" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.6510" Y="0.9992" />
            <PreSize X="0.1822" Y="0.0469" />
            <FileData Type="MarkedSubImage" Path="game_public/res/title_line.png" Plist="game_public/res/game.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>