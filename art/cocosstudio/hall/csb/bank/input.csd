<GameFile>
  <PropertyGroup Name="input" Type="Layer" ID="060045da-c230-4db3-a2d7-1be244cdf738" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="236" ctype="GameLayerObjectData">
        <Size X="1200.0000" Y="570.0000" />
        <Children>
          <AbstractNodeData Name="psw" ActionTag="-1533240670" Tag="252" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="320.0000" RightMargin="320.0000" TopMargin="226.6823" BottomMargin="273.3177" Scale9Enable="True" LeftEage="21" RightEage="21" TopEage="11" BottomEage="11" Scale9OriginX="21" Scale9OriginY="11" Scale9Width="535" Scale9Height="46" ctype="ImageViewObjectData">
            <Size X="560.0000" Y="70.0000" />
            <Children>
              <AbstractNodeData Name="Text_3" ActionTag="-584649788" Tag="253" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="15.6945" RightMargin="446.3055" TopMargin="21.0000" BottomMargin="21.0000" FontSize="28" LabelText="密码 : " ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="98.0000" Y="28.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="64.6945" Y="35.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="171" G="221" B="248" />
                <PrePosition X="0.1155" Y="0.5000" />
                <PreSize X="0.1750" Y="0.4000" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="input" ActionTag="-995572934" Tag="254" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="115.8640" RightMargin="206.1360" TopMargin="21.0000" BottomMargin="21.0000" FontSize="28" LabelText="请输入6位数字密码" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="238.0000" Y="28.0000" />
                <Children>
                  <AbstractNodeData Name="cursor" ActionTag="1503480673" VisibleForFrame="False" Tag="366" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="238.0000" RightMargin="-2.0000" TopMargin="-1.0000" BottomMargin="-1.0000" ctype="SpriteObjectData">
                    <Size X="2.0000" Y="30.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="238.0000" Y="14.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="1.0000" Y="0.5000" />
                    <PreSize X="0.0084" Y="1.0714" />
                    <FileData Type="MarkedSubImage" Path="hall/res/bank/cursor.png" Plist="hall/res/bank.plist" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleY="0.5000" />
                <Position X="115.8640" Y="35.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="164" G="205" B="249" />
                <PrePosition X="0.2069" Y="0.5000" />
                <PreSize X="0.4250" Y="0.4000" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="600.0000" Y="308.3177" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5409" />
            <PreSize X="0.4667" Y="0.1228" />
            <FileData Type="MarkedSubImage" Path="hall/res/bank/input.png" Plist="hall/res/bank.plist" />
          </AbstractNodeData>
          <AbstractNodeData Name="tip" ActionTag="511528117" Tag="258" IconVisible="False" LeftMargin="319.9996" RightMargin="592.0004" TopMargin="331.9727" BottomMargin="214.0273" FontSize="24" LabelText="密码错误，请查证后再输入" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="288.0000" Y="24.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="319.9996" Y="226.0273" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="248" G="204" B="9" />
            <PrePosition X="0.2667" Y="0.3965" />
            <PreSize X="0.2400" Y="0.0421" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="ok" ActionTag="1925191246" Tag="259" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="483.0000" RightMargin="483.0000" TopMargin="417.7201" BottomMargin="62.2799" LeftEage="16" RightEage="16" TopEage="13" BottomEage="13" Scale9OriginX="16" Scale9OriginY="13" Scale9Width="202" Scale9Height="64" ctype="ImageViewObjectData">
            <Size X="234.0000" Y="90.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="600.0000" Y="107.2799" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.1882" />
            <PreSize X="0.1950" Y="0.1579" />
            <FileData Type="MarkedSubImage" Path="lan/cn/btn_ok.png" Plist="lan/cn/game.plist" />
          </AbstractNodeData>
          <AbstractNodeData Name="keybord" ActionTag="888618431" VisibleForFrame="False" Tag="395" IconVisible="True" LeftMargin="900.0000" RightMargin="-2.0000" TopMargin="107.0000" BottomMargin="65.0000" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="302.0000" Y="398.0000" />
            <AnchorPoint />
            <Position X="900.0000" Y="65.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.7500" Y="0.1140" />
            <PreSize X="0.2517" Y="0.6982" />
            <FileData Type="Normal" Path="hall/csb/bank/keybord.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="forget" ActionTag="459562686" Tag="148" IconVisible="False" LeftMargin="740.0000" RightMargin="320.0000" TopMargin="316.9726" BottomMargin="193.0274" ctype="SpriteObjectData">
            <Size X="140.0000" Y="60.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="810.0000" Y="223.0274" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.6750" Y="0.3913" />
            <PreSize X="0.1167" Y="0.1053" />
            <FileData Type="MarkedSubImage" Path="lan/cn/btn_forget.png" Plist="lan/cn/game.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="title" ActionTag="1315714672" Tag="4412" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="390.0000" RightMargin="390.0000" TopMargin="120.8528" BottomMargin="389.1472" FontSize="60" LabelText="歡迎使用保險箱" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="420.0000" Y="60.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="600.0000" Y="419.1472" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.7353" />
            <PreSize X="0.3500" Y="0.1053" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>