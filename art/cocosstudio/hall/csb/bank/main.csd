<GameFile>
  <PropertyGroup Name="main" Type="Layer" ID="7cdb7dc6-cf2c-4298-b20b-acf9e57db066" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="76" ctype="GameLayerObjectData">
        <Size X="1010.0000" Y="620.0000" />
        <Children>
          <AbstractNodeData Name="menu_bg" ActionTag="-1650601011" Tag="631" IconVisible="False" PositionPercentXEnabled="True" VerticalEdge="TopEdge" LeftMargin="1.0000" RightMargin="963.0000" TopMargin="-1.0000" BottomMargin="575.0000" ctype="SpriteObjectData">
            <Size X="46.0000" Y="46.0000" />
            <Children>
              <AbstractNodeData Name="access" ActionTag="879711942" Tag="77" IconVisible="False" PositionPercentXEnabled="True" VerticalEdge="TopEdge" LeftMargin="0.3726" RightMargin="-196.3726" BottomMargin="-30.0000" ctype="SpriteObjectData">
                <Size X="242.0000" Y="76.0000" />
                <Children>
                  <AbstractNodeData Name="txt" ActionTag="-1125883313" Tag="81" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="65.0000" RightMargin="65.0000" TopMargin="22.0000" BottomMargin="22.0000" FontSize="28" LabelText="存入取出" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="112.0000" Y="32.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="121.0000" Y="38.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="0.4628" Y="0.4211" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleY="1.0000" />
                <Position X="0.3726" Y="46.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0081" Y="1.0000" />
                <PreSize X="5.2609" Y="1.6522" />
                <FileData Type="MarkedSubImage" Path="hall/res/bank/menu_select.png" Plist="hall/res/bank.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="send" ActionTag="-935814360" Tag="78" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="498.5275" RightMargin="-694.5275" BottomMargin="-30.0000" ctype="SpriteObjectData">
                <Size X="242.0000" Y="76.0000" />
                <Children>
                  <AbstractNodeData Name="txt" ActionTag="1563355273" Tag="82" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="65.0000" RightMargin="65.0000" TopMargin="22.0000" BottomMargin="22.0000" FontSize="28" LabelText="游戏赠送" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="112.0000" Y="32.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="121.0000" Y="38.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="85" G="120" B="152" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="0.4628" Y="0.4211" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleY="1.0000" />
                <Position X="498.5275" Y="46.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="10.8376" Y="1.0000" />
                <PreSize X="5.2609" Y="1.6522" />
                <FileData Type="MarkedSubImage" Path="hall/res/bank/menu_select_no.png" Plist="hall/res/bank.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="record" ActionTag="-1295573585" Tag="79" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="744.7913" RightMargin="-940.7913" BottomMargin="-30.0000" ctype="SpriteObjectData">
                <Size X="242.0000" Y="76.0000" />
                <Children>
                  <AbstractNodeData Name="txt" ActionTag="1075829753" Tag="83" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="65.9922" RightMargin="64.0078" TopMargin="22.0000" BottomMargin="22.0000" FontSize="28" LabelText="交易记录" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="112.0000" Y="32.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="121.9922" Y="38.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="85" G="120" B="152" />
                    <PrePosition X="0.5041" Y="0.5000" />
                    <PreSize X="0.4628" Y="0.4211" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleY="1.0000" />
                <Position X="744.7913" Y="46.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="16.1911" Y="1.0000" />
                <PreSize X="5.2609" Y="1.6522" />
                <FileData Type="MarkedSubImage" Path="hall/res/bank/menu_select_no.png" Plist="hall/res/bank.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="edit_psw" ActionTag="-1705795683" Tag="80" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="256.2635" RightMargin="-452.2635" BottomMargin="-30.0000" ctype="SpriteObjectData">
                <Size X="242.0000" Y="76.0000" />
                <Children>
                  <AbstractNodeData Name="txt" ActionTag="185097672" Tag="84" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="65.0000" RightMargin="65.0000" TopMargin="22.0000" BottomMargin="22.0000" FontSize="28" LabelText="修改密码" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="112.0000" Y="32.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="121.0000" Y="38.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="85" G="120" B="152" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="0.4628" Y="0.4211" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleY="1.0000" />
                <Position X="256.2635" Y="46.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="5.5709" Y="1.0000" />
                <PreSize X="5.2609" Y="1.6522" />
                <FileData Type="MarkedSubImage" Path="hall/res/bank/menu_select_no.png" Plist="hall/res/bank.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="1.0000" />
            <Position X="24.0000" Y="621.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0238" Y="1.0016" />
            <PreSize X="0.0455" Y="0.0742" />
            <FileData Type="Default" Path="Default/Sprite.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>