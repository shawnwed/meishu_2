<GameFile>
  <PropertyGroup Name="access" Type="Layer" ID="60ae352e-c030-4f78-a434-cf550cf0e7de" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="222" ctype="GameLayerObjectData">
        <Size X="1200.0000" Y="570.0000" />
        <Children>
          <AbstractNodeData Name="tip" ActionTag="605743608" Tag="999" IconVisible="False" LeftMargin="472.9200" RightMargin="657.0800" TopMargin="355.6200" BottomMargin="187.3800" FontSize="27" LabelText="11111" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="70.0000" Y="27.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="472.9200" Y="200.8800" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="243" G="209" B="108" />
            <PrePosition X="0.3941" Y="0.3524" />
            <PreSize X="0.0583" Y="0.0474" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="all_btn" ActionTag="-1269983147" Tag="1000" IconVisible="False" LeftMargin="633.0000" RightMargin="333.0000" TopMargin="409.2300" BottomMargin="70.7700" ctype="SpriteObjectData">
            <Size X="234.0000" Y="90.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="750.0000" Y="115.7700" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.6250" Y="0.2031" />
            <PreSize X="0.1950" Y="0.1579" />
            <FileData Type="MarkedSubImage" Path="lan/cn/btn_all.png" Plist="lan/cn/game.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="out_btn" ActionTag="367354152" Tag="3196" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="333.0000" RightMargin="633.0000" TopMargin="409.2300" BottomMargin="70.7700" ctype="SpriteObjectData">
            <Size X="234.0000" Y="90.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="450.0000" Y="115.7700" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3750" Y="0.2031" />
            <PreSize X="0.1950" Y="0.1579" />
            <FileData Type="MarkedSubImage" Path="lan/cn/btn_quchu.png" Plist="lan/cn/game.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="in_btn" ActionTag="-793549834" Tag="1001" IconVisible="False" LeftMargin="333.0000" RightMargin="633.0000" TopMargin="409.2300" BottomMargin="70.7700" ctype="SpriteObjectData">
            <Size X="234.0000" Y="90.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="450.0000" Y="115.7700" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3750" Y="0.2031" />
            <PreSize X="0.1950" Y="0.1579" />
            <FileData Type="MarkedSubImage" Path="lan/cn/btn_cunru.png" Plist="lan/cn/game.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="keybord" ActionTag="-545443973" VisibleForFrame="False" Tag="1304" IconVisible="True" LeftMargin="900.0000" RightMargin="-2.0000" TopMargin="107.0000" BottomMargin="65.0000" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="302.0000" Y="398.0000" />
            <Children>
              <AbstractNodeData Name="txt" ActionTag="-1994291381" Tag="14407" IconVisible="False" LeftMargin="85.0876" RightMargin="116.9124" TopMargin="43.0952" BottomMargin="334.9048" FontSize="20" LabelText="Text Label" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="100.0000" Y="20.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="135.0876" Y="344.9048" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4473" Y="0.8666" />
                <PreSize X="0.3311" Y="0.0503" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position X="900.0000" Y="65.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.7500" Y="0.1140" />
            <PreSize X="0.2517" Y="0.6982" />
            <FileData Type="Normal" Path="hall/csb/bank/keybord.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="selector" ActionTag="-1008080792" Tag="3087" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="12.4371" RightMargin="1119.5629" TopMargin="135.0000" BottomMargin="135.0000" ctype="SpriteObjectData">
            <Size X="68.0000" Y="300.0000" />
            <Children>
              <AbstractNodeData Name="item1" ActionTag="2088635270" Tag="3088" IconVisible="False" LeftMargin="0.1638" RightMargin="-0.1638" TopMargin="0.7637" BottomMargin="149.2363" ctype="SpriteObjectData">
                <Size X="68.0000" Y="150.0000" />
                <Children>
                  <AbstractNodeData Name="txt" ActionTag="435591289" Tag="3090" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="16.5000" RightMargin="16.5000" TopMargin="40.0000" BottomMargin="40.0000" FontSize="35" LabelText="存&#xA;入" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="35.0000" Y="70.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="34.0000" Y="75.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="253" G="218" B="98" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="0.5147" Y="0.4667" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="34.1638" Y="224.2363" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5024" Y="0.7475" />
                <PreSize X="1.0000" Y="0.5000" />
                <FileData Type="MarkedSubImage" Path="hall/res/bank/selector_tiem.png" Plist="hall/res/bank.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="item2" ActionTag="1642158226" Tag="3089" IconVisible="False" LeftMargin="0.1637" RightMargin="-0.1637" TopMargin="150.7638" BottomMargin="-0.7638" ctype="SpriteObjectData">
                <Size X="68.0000" Y="150.0000" />
                <Children>
                  <AbstractNodeData Name="txt" ActionTag="1972869209" Tag="3091" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="16.5000" RightMargin="16.5000" TopMargin="40.0000" BottomMargin="40.0000" FontSize="35" LabelText="取&#xA;出" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="35.0000" Y="70.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="34.0000" Y="75.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="253" G="218" B="98" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="0.5147" Y="0.4667" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="34.1637" Y="74.2362" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5024" Y="0.2475" />
                <PreSize X="1.0000" Y="0.5000" />
                <FileData Type="MarkedSubImage" Path="hall/res/bank/selector_tiem2.png" Plist="hall/res/bank.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="46.4371" Y="285.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0387" Y="0.5000" />
            <PreSize X="0.0567" Y="0.5263" />
            <FileData Type="MarkedSubImage" Path="hall/res/bank/selector_bg.png" Plist="hall/res/bank.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="cur_money" ActionTag="1146063949" Tag="306" IconVisible="False" LeftMargin="304.0500" RightMargin="735.9500" TopMargin="120.8399" BottomMargin="416.1601" FontSize="32" LabelText="攜帶資產：" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="160.0000" Y="33.0000" />
            <Children>
              <AbstractNodeData Name="coin" ActionTag="195909931" Tag="990" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="172.0710" RightMargin="-51.0710" TopMargin="-2.2147" BottomMargin="-5.7853" ctype="SpriteObjectData">
                <Size X="39.0000" Y="41.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="191.5710" Y="14.7147" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="1.1973" Y="0.4459" />
                <PreSize X="0.2438" Y="1.2424" />
                <FileData Type="MarkedSubImage" Path="game_public/res/coin_small_icon2.png" Plist="game_public/res/game.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="txt" ActionTag="-187289781" Tag="983" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="225.9091" RightMargin="-145.9091" FontSize="32" LabelText="11111" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="80.0000" Y="33.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="225.9091" Y="16.5000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="253" G="218" B="108" />
                <PrePosition X="1.4119" Y="0.5000" />
                <PreSize X="0.5000" Y="1.0000" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
            <Position X="464.0500" Y="432.6601" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3867" Y="0.7591" />
            <PreSize X="0.1333" Y="0.0579" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="bank_money" ActionTag="-906821206" Tag="991" IconVisible="False" LeftMargin="304.0540" RightMargin="735.9460" TopMargin="205.8400" BottomMargin="331.1600" FontSize="32" LabelText="银行資產：" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="160.0000" Y="33.0000" />
            <Children>
              <AbstractNodeData Name="coin" ActionTag="1855903780" Tag="993" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="172.0708" RightMargin="-51.0708" TopMargin="-2.2147" BottomMargin="-5.7853" ctype="SpriteObjectData">
                <Size X="39.0000" Y="41.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="191.5708" Y="14.7147" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="1.1973" Y="0.4459" />
                <PreSize X="0.2438" Y="1.2424" />
                <FileData Type="MarkedSubImage" Path="game_public/res/coin_small_icon2.png" Plist="game_public/res/game.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="txt" ActionTag="1199855293" Tag="994" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="225.9090" RightMargin="-145.9090" FontSize="32" LabelText="11111" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="80.0000" Y="33.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="225.9090" Y="16.5000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="253" G="218" B="108" />
                <PrePosition X="1.4119" Y="0.5000" />
                <PreSize X="0.5000" Y="1.0000" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
            <Position X="464.0540" Y="347.6600" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3867" Y="0.6099" />
            <PreSize X="0.1333" Y="0.0579" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="chang_money" ActionTag="-930594735" Tag="995" IconVisible="False" LeftMargin="304.0540" RightMargin="735.9460" TopMargin="290.8400" BottomMargin="246.1600" FontSize="32" LabelText="存取資產：" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="160.0000" Y="33.0000" />
            <Children>
              <AbstractNodeData Name="bg" ActionTag="-1235431051" Tag="996" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="165.2103" RightMargin="-435.2103" TopMargin="-17.5000" BottomMargin="-17.5000" Scale9Enable="True" LeftEage="20" RightEage="20" TopEage="22" BottomEage="22" Scale9OriginX="20" Scale9OriginY="22" Scale9Width="365" Scale9Height="14" ctype="ImageViewObjectData">
                <Size X="430.0000" Y="68.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="165.2103" Y="16.5000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="1.0326" Y="0.5000" />
                <PreSize X="2.6875" Y="2.0606" />
                <FileData Type="MarkedSubImage" Path="hall/res/bank/input.png" Plist="hall/res/bank.plist" />
              </AbstractNodeData>
              <AbstractNodeData Name="coin" ActionTag="1729371131" Tag="997" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="172.0711" RightMargin="-51.0711" TopMargin="-2.2147" BottomMargin="-5.7853" ctype="SpriteObjectData">
                <Size X="39.0000" Y="41.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="191.5711" Y="14.7147" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="1.1973" Y="0.4459" />
                <PreSize X="0.2438" Y="1.2424" />
                <FileData Type="MarkedSubImage" Path="game_public/res/coin_small_icon2.png" Plist="game_public/res/game.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="txt" ActionTag="-1784869714" Tag="4039" IconVisible="False" LeftMargin="225.9100" RightMargin="-415.9100" TopMargin="-11.6658" BottomMargin="-5.3342" TouchEnable="True" ClipAble="False" BackColorAlpha="144" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="350.0000" Y="50.0000" />
                <Children>
                  <AbstractNodeData Name="input" ActionTag="-793520804" Tag="4040" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" RightMargin="334.0000" TopMargin="8.5000" BottomMargin="8.5000" FontSize="32" LabelText=" " ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="16.0000" Y="33.0000" />
                    <Children>
                      <AbstractNodeData Name="cursor" ActionTag="-2048013934" VisibleForFrame="False" Tag="4041" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="16.0000" RightMargin="-2.0000" TopMargin="1.5000" BottomMargin="1.5000" ctype="SpriteObjectData">
                        <Size X="2.0000" Y="30.0000" />
                        <AnchorPoint ScaleY="0.5000" />
                        <Position X="16.0000" Y="16.5000" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="1.0000" Y="0.5000" />
                        <PreSize X="0.1250" Y="0.9091" />
                        <FileData Type="MarkedSubImage" Path="hall/res/bank/cursor.png" Plist="hall/res/bank.plist" />
                        <BlendFunc Src="1" Dst="771" />
                      </AbstractNodeData>
                    </Children>
                    <AnchorPoint ScaleY="0.5000" />
                    <Position Y="25.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="253" G="218" B="108" />
                    <PrePosition Y="0.5000" />
                    <PreSize X="0.0457" Y="0.6600" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleY="0.5000" />
                <Position X="225.9100" Y="19.6658" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="1.4119" Y="0.5959" />
                <PreSize X="2.1875" Y="1.5152" />
                <SingleColor A="255" R="253" G="218" B="108" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
            <Position X="464.0540" Y="262.6600" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3867" Y="0.4608" />
            <PreSize X="0.1333" Y="0.0579" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>