<GameFile>
  <PropertyGroup Name="forget" Type="Layer" ID="060045da-c230-4db3-a2d7-1be244cdf738" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="236" ctype="GameLayerObjectData">
        <Size X="1200.0000" Y="570.0000" />
        <Children>
          <AbstractNodeData Name="bg" ActionTag="375493736" Tag="4461" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="213.0000" RightMargin="213.0000" TopMargin="50.5000" BottomMargin="50.5000" ctype="SpriteObjectData">
            <Size X="774.0000" Y="469.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="600.0000" Y="285.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="0.6450" Y="0.8228" />
            <FileData Type="Normal" Path="game_public/res/setting_bg.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="title" ActionTag="-1533351264" Tag="4457" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="520.0000" RightMargin="520.0000" TopMargin="70.8248" BottomMargin="454.1752" FontSize="40" LabelText="密保郵箱" ShadowOffsetX="1.0000" ShadowOffsetY="-1.0000" ShadowEnabled="True" ctype="TextObjectData">
            <Size X="160.0000" Y="45.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="600.0000" Y="476.6752" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.8363" />
            <PreSize X="0.1333" Y="0.0789" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="0" G="0" B="0" />
          </AbstractNodeData>
          <AbstractNodeData Name="close" ActionTag="-1774234939" Tag="4458" IconVisible="False" LeftMargin="914.5121" RightMargin="225.4879" TopMargin="62.9697" BottomMargin="447.0303" ctype="SpriteObjectData">
            <Size X="60.0000" Y="60.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="944.5121" Y="477.0303" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.7871" Y="0.8369" />
            <PreSize X="0.0500" Y="0.1053" />
            <FileData Type="Normal" Path="game_public/res/user_info_close.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="title_line_6_0" ActionTag="1974422765" Tag="4459" IconVisible="False" LeftMargin="708.0001" RightMargin="351.9999" TopMargin="84.5001" BottomMargin="466.4999" FlipX="True" ctype="SpriteObjectData">
            <Size X="140.0000" Y="19.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="778.0001" Y="475.9999" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.6483" Y="0.8351" />
            <PreSize X="0.1167" Y="0.0333" />
            <FileData Type="MarkedSubImage" Path="game_public/res/title_line.png" Plist="game_public/res/game.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="title_line_6" ActionTag="-1835877724" Tag="4460" IconVisible="False" LeftMargin="350.0034" RightMargin="709.9966" TopMargin="84.5001" BottomMargin="466.4999" ctype="SpriteObjectData">
            <Size X="140.0000" Y="19.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="420.0034" Y="475.9999" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3500" Y="0.8351" />
            <PreSize X="0.1167" Y="0.0333" />
            <FileData Type="MarkedSubImage" Path="game_public/res/title_line.png" Plist="game_public/res/game.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="verify" ActionTag="-1533240670" Tag="252" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="379.9600" RightMargin="570.0400" TopMargin="217.7802" BottomMargin="282.2198" Scale9Enable="True" LeftEage="21" RightEage="21" TopEage="11" BottomEage="11" Scale9OriginX="21" Scale9OriginY="11" Scale9Width="535" Scale9Height="46" ctype="ImageViewObjectData">
            <Size X="250.0000" Y="70.0000" />
            <Children>
              <AbstractNodeData Name="input" ActionTag="-995572934" Tag="254" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="12.9250" RightMargin="228.0750" TopMargin="18.0000" BottomMargin="18.0000" FontSize="30" LabelText=" " ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="9.0000" Y="34.0000" />
                <Children>
                  <AbstractNodeData Name="cursor" ActionTag="1366468581" VisibleForFrame="False" Tag="364" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="9.0000" RightMargin="-2.0000" TopMargin="2.0000" BottomMargin="2.0000" ctype="SpriteObjectData">
                    <Size X="2.0000" Y="30.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="9.0000" Y="17.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="1.0000" Y="0.5000" />
                    <PreSize X="0.2222" Y="0.8824" />
                    <FileData Type="MarkedSubImage" Path="hall/res/bank/cursor.png" Plist="hall/res/bank.plist" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleY="0.5000" />
                <Position X="12.9250" Y="35.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0517" Y="0.5000" />
                <PreSize X="0.0360" Y="0.4857" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="504.9600" Y="317.2198" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4208" Y="0.5565" />
            <PreSize X="0.2083" Y="0.1228" />
            <FileData Type="MarkedSubImage" Path="hall/res/bank/input.png" Plist="hall/res/bank.plist" />
          </AbstractNodeData>
          <AbstractNodeData Name="psw1" ActionTag="813188323" Tag="255" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="380.0000" RightMargin="380.0000" TopMargin="301.4851" BottomMargin="198.5149" Scale9Enable="True" LeftEage="21" RightEage="21" TopEage="11" BottomEage="11" Scale9OriginX="21" Scale9OriginY="11" Scale9Width="535" Scale9Height="46" ctype="ImageViewObjectData">
            <Size X="440.0000" Y="70.0000" />
            <Children>
              <AbstractNodeData Name="input" ActionTag="1409634192" Tag="257" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="12.8920" RightMargin="131.1080" TopMargin="18.0000" BottomMargin="18.0000" FontSize="30" LabelText=" 設定您的6位數字密碼" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="296.0000" Y="34.0000" />
                <Children>
                  <AbstractNodeData Name="cursor" ActionTag="-63118992" VisibleForFrame="False" Tag="365" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="296.0000" RightMargin="-2.0000" TopMargin="2.0000" BottomMargin="2.0000" ctype="SpriteObjectData">
                    <Size X="2.0000" Y="30.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="296.0000" Y="17.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="1.0000" Y="0.5000" />
                    <PreSize X="0.0068" Y="0.8824" />
                    <FileData Type="MarkedSubImage" Path="hall/res/bank/cursor.png" Plist="hall/res/bank.plist" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleY="0.5000" />
                <Position X="12.8920" Y="35.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="100" G="130" B="154" />
                <PrePosition X="0.0293" Y="0.5000" />
                <PreSize X="0.6727" Y="0.4857" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="600.0000" Y="233.5149" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.4097" />
            <PreSize X="0.3667" Y="0.1228" />
            <FileData Type="MarkedSubImage" Path="hall/res/bank/input.png" Plist="hall/res/bank.plist" />
          </AbstractNodeData>
          <AbstractNodeData Name="email" ActionTag="1620384376" Tag="4462" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="380.0000" RightMargin="380.0000" TopMargin="134.0750" BottomMargin="365.9250" Scale9Enable="True" LeftEage="21" RightEage="21" TopEage="11" BottomEage="11" Scale9OriginX="21" Scale9OriginY="11" Scale9Width="535" Scale9Height="46" ctype="ImageViewObjectData">
            <Size X="440.0000" Y="70.0000" />
            <Children>
              <AbstractNodeData Name="txt" ActionTag="-1728627190" Tag="4553" IconVisible="False" LeftMargin="20.0005" RightMargin="140.9995" TopMargin="19.0003" BottomMargin="16.9997" FontSize="30" LabelText="hel****rld@xxoo.com" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="279.0000" Y="34.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="20.0005" Y="33.9997" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0455" Y="0.4857" />
                <PreSize X="0.6341" Y="0.4857" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="600.0000" Y="400.9250" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.7034" />
            <PreSize X="0.3667" Y="0.1228" />
            <FileData Type="MarkedSubImage" Path="hall/res/bank/input.png" Plist="hall/res/bank.plist" />
          </AbstractNodeData>
          <AbstractNodeData Name="tip" ActionTag="511528117" Tag="258" IconVisible="False" LeftMargin="380.0001" RightMargin="675.9999" TopMargin="385.4029" BottomMargin="156.5971" FontSize="24" LabelText="验证码错误！" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="144.0000" Y="28.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="380.0001" Y="170.5971" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="248" G="204" B="9" />
            <PrePosition X="0.3167" Y="0.2993" />
            <PreSize X="0.1200" Y="0.0491" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="save" ActionTag="1925191246" Tag="259" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="483.0000" RightMargin="483.0000" TopMargin="417.7191" BottomMargin="62.2809" Scale9Enable="True" LeftEage="16" RightEage="16" TopEage="13" BottomEage="13" Scale9OriginX="16" Scale9OriginY="13" Scale9Width="202" Scale9Height="64" ctype="ImageViewObjectData">
            <Size X="234.0000" Y="90.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="600.0000" Y="107.2809" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.1882" />
            <PreSize X="0.1950" Y="0.1579" />
            <FileData Type="MarkedSubImage" Path="lan/cn/btn_ok.png" Plist="lan/cn/game.plist" />
          </AbstractNodeData>
          <AbstractNodeData Name="getVerify" ActionTag="-611166191" Tag="713" IconVisible="True" LeftMargin="634.0001" RightMargin="379.9999" TopMargin="213.5682" BottomMargin="277.4318" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="True" ctype="ProjectNodeObjectData">
            <Size X="186.0000" Y="79.0000" />
            <AnchorPoint />
            <Position X="634.0001" Y="277.4318" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5283" Y="0.4867" />
            <PreSize X="0.1550" Y="0.1386" />
            <FileData Type="Normal" Path="hall/csb/bank/repeat_getVerify.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="keybord" ActionTag="1456532209" VisibleForFrame="False" Tag="805" IconVisible="True" LeftMargin="900.0000" RightMargin="-2.0000" TopMargin="107.0000" BottomMargin="65.0000" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="302.0000" Y="398.0000" />
            <AnchorPoint />
            <Position X="900.0000" Y="65.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.7500" Y="0.1140" />
            <PreSize X="0.2517" Y="0.6982" />
            <FileData Type="Normal" Path="hall/csb/bank/keybord.csd" Plist="" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>