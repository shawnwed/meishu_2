<GameFile>
  <PropertyGroup Name="send_detail" Type="Layer" ID="a584b32c-09c1-4fb2-8c27-2ad6a59173c2" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="634" ctype="GameLayerObjectData">
        <Size X="774.0000" Y="469.0000" />
        <Children>
          <AbstractNodeData Name="bg" ActionTag="-799470851" Tag="439" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-70.5000" RightMargin="-70.5000" TopMargin="-53.5000" BottomMargin="-53.5000" ctype="SpriteObjectData">
            <Size X="915.0000" Y="576.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="387.0000" Y="234.5000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="1.1822" Y="1.2281" />
            <FileData Type="Normal" Path="game_public/res/setting_bg.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="title_line_6" ActionTag="-539070095" Tag="435" IconVisible="False" LeftMargin="130.0185" RightMargin="502.9815" TopMargin="-21.6321" BottomMargin="468.6321" ctype="SpriteObjectData">
            <Size X="141.0000" Y="22.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="200.5185" Y="479.6321" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.2591" Y="1.0227" />
            <PreSize X="0.1822" Y="0.0469" />
            <FileData Type="MarkedSubImage" Path="game_public/res/title_line.png" Plist="game_public/res/game.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="title_line_6_0" ActionTag="-961722259" Tag="436" IconVisible="False" LeftMargin="488.0179" RightMargin="144.9821" TopMargin="-21.6321" BottomMargin="468.6321" FlipX="True" ctype="SpriteObjectData">
            <Size X="141.0000" Y="22.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="558.5179" Y="479.6321" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.7216" Y="1.0227" />
            <PreSize X="0.1822" Y="0.0469" />
            <FileData Type="MarkedSubImage" Path="game_public/res/title_line.png" Plist="game_public/res/game.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="close" ActionTag="1418345945" Tag="437" IconVisible="False" LeftMargin="767.4193" RightMargin="-73.4193" TopMargin="-63.1616" BottomMargin="451.1617" ctype="SpriteObjectData">
            <Size X="80.0000" Y="81.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="807.4193" Y="491.6617" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="1.0432" Y="1.0483" />
            <PreSize X="0.1034" Y="0.1727" />
            <FileData Type="Normal" Path="game_public/res/user_info_close.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="title" ActionTag="1901527008" Tag="438" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="305.5194" RightMargin="308.4806" TopMargin="-31.3055" BottomMargin="460.3055" FontSize="40" LabelText="交易詳情" ShadowOffsetX="1.0000" ShadowOffsetY="-1.0000" ShadowEnabled="True" ctype="TextObjectData">
            <Size X="160.0000" Y="40.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="385.5194" Y="480.3055" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4981" Y="1.0241" />
            <PreSize X="0.2067" Y="0.0853" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="0" G="0" B="0" />
          </AbstractNodeData>
          <AbstractNodeData Name="sname" ActionTag="944398266" Tag="641" IconVisible="False" LeftMargin="118.7593" RightMargin="479.2407" TopMargin="109.8287" BottomMargin="326.1713" FontSize="32" LabelText="轉  賬  者:" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="176.0000" Y="33.0000" />
            <Children>
              <AbstractNodeData Name="txt" ActionTag="604172688" Tag="642" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="176.0000" RightMargin="-128.0000" FontSize="32" LabelText="南宫紫菱" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="128.0000" Y="33.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="176.0000" Y="16.5000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="1.0000" Y="0.5000" />
                <PreSize X="0.7273" Y="1.0000" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
            <Position X="294.7593" Y="342.6713" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="126" G="89" B="192" />
            <PrePosition X="0.3808" Y="0.7306" />
            <PreSize X="0.2274" Y="0.0704" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="rname" ActionTag="64845543" Tag="645" IconVisible="False" LeftMargin="118.7561" RightMargin="479.2439" TopMargin="172.1898" BottomMargin="263.8102" FontSize="32" LabelText="接  收  者:" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="176.0000" Y="33.0000" />
            <Children>
              <AbstractNodeData Name="txt" ActionTag="-549683654" Tag="646" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="176.0000" RightMargin="-128.0000" FontSize="32" LabelText="绝命贪狼" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="128.0000" Y="33.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="176.0000" Y="16.5000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="1.0000" Y="0.5000" />
                <PreSize X="0.7273" Y="1.0000" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
            <Position X="294.7561" Y="280.3102" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="126" G="89" B="192" />
            <PrePosition X="0.3808" Y="0.5977" />
            <PreSize X="0.2274" Y="0.0704" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="count" ActionTag="-852735905" Tag="649" IconVisible="False" LeftMargin="150.8153" RightMargin="479.1847" TopMargin="234.5507" BottomMargin="201.4493" FontSize="32" LabelText="轉賬金額:" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="144.0000" Y="33.0000" />
            <Children>
              <AbstractNodeData Name="txt" ActionTag="-1201590503" Tag="650" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="144.0000" RightMargin="-160.0000" FontSize="32" LabelText="10,000,000" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="160.0000" Y="33.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="144.0000" Y="16.5000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="1.0000" Y="0.5000" />
                <PreSize X="1.1111" Y="1.0000" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
            <Position X="294.8153" Y="217.9493" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="126" G="89" B="192" />
            <PrePosition X="0.3809" Y="0.4647" />
            <PreSize X="0.1860" Y="0.0704" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="count_c" ActionTag="1446022445" Tag="651" IconVisible="False" LeftMargin="150.8153" RightMargin="479.1847" TopMargin="296.9117" BottomMargin="139.0883" FontSize="32" LabelText="金額大寫:" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="144.0000" Y="33.0000" />
            <Children>
              <AbstractNodeData Name="txt" ActionTag="-560738093" Tag="652" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="144.0000" RightMargin="-128.0000" FontSize="32" LabelText="壹仟万整" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="128.0000" Y="33.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="144.0000" Y="16.5000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="1.0000" Y="0.5000" />
                <PreSize X="0.8889" Y="1.0000" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
            <Position X="294.8153" Y="155.5883" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="126" G="89" B="192" />
            <PrePosition X="0.3809" Y="0.3317" />
            <PreSize X="0.1860" Y="0.0704" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="date" ActionTag="228838931" Tag="653" IconVisible="False" LeftMargin="150.8154" RightMargin="479.1846" TopMargin="359.2728" BottomMargin="76.7272" FontSize="32" LabelText="轉賬時間:" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="144.0000" Y="33.0000" />
            <Children>
              <AbstractNodeData Name="txt" ActionTag="986622259" Tag="654" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="144.0000" RightMargin="-304.0000" FontSize="32" LabelText="2016-04-25 16:24:56" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="304.0000" Y="33.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="144.0000" Y="16.5000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="1.0000" Y="0.5000" />
                <PreSize X="2.1111" Y="1.0000" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
            <Position X="294.8154" Y="93.2272" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="126" G="89" B="192" />
            <PrePosition X="0.3809" Y="0.1988" />
            <PreSize X="0.1860" Y="0.0704" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="send_success_5" ActionTag="-876493337" Tag="655" IconVisible="False" LeftMargin="572.1268" RightMargin="50.8732" TopMargin="256.6164" BottomMargin="78.3836" ctype="SpriteObjectData">
            <Size X="151.0000" Y="134.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="647.6268" Y="145.3836" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8367" Y="0.3100" />
            <PreSize X="0.1951" Y="0.2857" />
            <FileData Type="MarkedSubImage" Path="lan/cn/send_success.png" Plist="lan/cn/game.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>