<GameFile>
  <PropertyGroup Name="send" Type="Layer" ID="060045da-c230-4db3-a2d7-1be244cdf738" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="236" ctype="GameLayerObjectData">
        <Size X="1200.0000" Y="570.0000" />
        <Children>
          <AbstractNodeData Name="keybord" ActionTag="1592414814" VisibleForFrame="False" Tag="1470" IconVisible="True" LeftMargin="900.0000" RightMargin="-2.0000" TopMargin="107.0000" BottomMargin="65.0000" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="302.0000" Y="398.0000" />
            <AnchorPoint />
            <Position X="900.0000" Y="65.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.7500" Y="0.1140" />
            <PreSize X="0.2517" Y="0.6982" />
            <FileData Type="Normal" Path="hall/csb/bank/keybord.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="money" ActionTag="-1842888802" Tag="1499" IconVisible="False" LeftMargin="304.0500" RightMargin="735.9500" TopMargin="288.8400" BottomMargin="244.1600" FontSize="32" LabelText="贈送金額：" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="160.0000" Y="37.0000" />
            <Children>
              <AbstractNodeData Name="bg" ActionTag="453148855" Tag="1500" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="165.2103" RightMargin="-435.2103" TopMargin="-15.5000" BottomMargin="-15.5000" Scale9Enable="True" LeftEage="20" RightEage="20" TopEage="22" BottomEage="22" Scale9OriginX="20" Scale9OriginY="22" Scale9Width="365" Scale9Height="14" ctype="ImageViewObjectData">
                <Size X="430.0000" Y="68.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="165.2103" Y="18.5000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="1.0326" Y="0.5000" />
                <PreSize X="2.6875" Y="1.8378" />
                <FileData Type="MarkedSubImage" Path="hall/res/bank/input.png" Plist="hall/res/bank.plist" />
              </AbstractNodeData>
              <AbstractNodeData Name="coin" ActionTag="2002914627" Tag="1501" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="172.0711" RightMargin="-51.0711" TopMargin="0.0017" BottomMargin="-4.0017" ctype="SpriteObjectData">
                <Size X="39.0000" Y="41.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="191.5711" Y="16.4983" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="1.1973" Y="0.4459" />
                <PreSize X="0.2438" Y="1.1081" />
                <FileData Type="MarkedSubImage" Path="game_public/res/coin_small_icon2.png" Plist="game_public/res/game.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="txt" ActionTag="-128379944" Tag="1502" IconVisible="False" LeftMargin="225.9088" RightMargin="-415.9088" TopMargin="-7.6658" BottomMargin="-5.3342" TouchEnable="True" ClipAble="False" BackColorAlpha="0" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="350.0000" Y="50.0000" />
                <Children>
                  <AbstractNodeData Name="input" ActionTag="-911239745" Tag="1503" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" RightMargin="341.0000" TopMargin="6.5000" BottomMargin="6.5000" FontSize="32" LabelText=" " ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="9.0000" Y="37.0000" />
                    <Children>
                      <AbstractNodeData Name="cursor" ActionTag="2065944011" VisibleForFrame="False" Tag="1504" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="9.0000" RightMargin="-2.0000" TopMargin="3.5000" BottomMargin="3.5000" ctype="SpriteObjectData">
                        <Size X="2.0000" Y="30.0000" />
                        <AnchorPoint ScaleY="0.5000" />
                        <Position X="9.0000" Y="18.5000" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="1.0000" Y="0.5000" />
                        <PreSize X="0.2222" Y="0.8108" />
                        <FileData Type="MarkedSubImage" Path="hall/res/bank/cursor.png" Plist="hall/res/bank.plist" />
                        <BlendFunc Src="1" Dst="771" />
                      </AbstractNodeData>
                    </Children>
                    <AnchorPoint ScaleY="0.5000" />
                    <Position Y="25.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="253" G="218" B="108" />
                    <PrePosition Y="0.5000" />
                    <PreSize X="0.0257" Y="0.7400" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint />
                <Position X="225.9088" Y="-5.3342" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="1.4119" Y="-0.1442" />
                <PreSize X="2.1875" Y="1.3514" />
                <SingleColor A="255" R="253" G="218" B="108" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
            <Position X="464.0500" Y="262.6600" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3867" Y="0.4608" />
            <PreSize X="0.1333" Y="0.0649" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="target_id" ActionTag="-137441565" Tag="1505" IconVisible="False" LeftMargin="304.0500" RightMargin="735.9500" TopMargin="203.8399" BottomMargin="329.1601" FontSize="32" LabelText="接收者ID：" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="160.0000" Y="37.0000" />
            <Children>
              <AbstractNodeData Name="bg" ActionTag="311499484" Tag="1506" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="165.2101" RightMargin="-435.2101" TopMargin="-15.5000" BottomMargin="-15.5000" Scale9Enable="True" LeftEage="20" RightEage="20" TopEage="22" BottomEage="22" Scale9OriginX="20" Scale9OriginY="22" Scale9Width="365" Scale9Height="14" ctype="ImageViewObjectData">
                <Size X="430.0000" Y="68.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="165.2101" Y="18.5000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="1.0326" Y="0.5000" />
                <PreSize X="2.6875" Y="1.8378" />
                <FileData Type="MarkedSubImage" Path="hall/res/bank/input.png" Plist="hall/res/bank.plist" />
              </AbstractNodeData>
              <AbstractNodeData Name="txt" ActionTag="-1905437737" Tag="1515" IconVisible="False" LeftMargin="205.9088" RightMargin="-395.9088" TopMargin="-7.6658" BottomMargin="-5.3342" TouchEnable="True" ClipAble="False" BackColorAlpha="0" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="350.0000" Y="50.0000" />
                <Children>
                  <AbstractNodeData Name="input" ActionTag="801032032" Tag="1516" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" RightMargin="341.0000" TopMargin="6.5000" BottomMargin="6.5000" FontSize="32" LabelText=" " ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="9.0000" Y="37.0000" />
                    <Children>
                      <AbstractNodeData Name="cursor" ActionTag="857111590" VisibleForFrame="False" Tag="1517" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="9.0000" RightMargin="-2.0000" TopMargin="3.5000" BottomMargin="3.5000" ctype="SpriteObjectData">
                        <Size X="2.0000" Y="30.0000" />
                        <AnchorPoint ScaleY="0.5000" />
                        <Position X="9.0000" Y="18.5000" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="1.0000" Y="0.5000" />
                        <PreSize X="0.2222" Y="0.8108" />
                        <FileData Type="MarkedSubImage" Path="hall/res/bank/cursor.png" Plist="hall/res/bank.plist" />
                        <BlendFunc Src="1" Dst="771" />
                      </AbstractNodeData>
                    </Children>
                    <AnchorPoint ScaleY="0.5000" />
                    <Position Y="25.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="253" G="218" B="108" />
                    <PrePosition Y="0.5000" />
                    <PreSize X="0.0257" Y="0.7400" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint />
                <Position X="205.9088" Y="-5.3342" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="1.2869" Y="-0.1442" />
                <PreSize X="2.1875" Y="1.3514" />
                <SingleColor A="255" R="253" G="218" B="108" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
            <Position X="464.0500" Y="347.6601" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3867" Y="0.6099" />
            <PreSize X="0.1333" Y="0.0649" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="bank_money" ActionTag="1368996211" Tag="1509" IconVisible="False" LeftMargin="304.0500" RightMargin="735.9500" TopMargin="118.8398" BottomMargin="414.1602" FontSize="32" LabelText="銀行資產：" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="160.0000" Y="37.0000" />
            <Children>
              <AbstractNodeData Name="coin" ActionTag="-763153883" Tag="1511" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="172.0710" RightMargin="-51.0710" TopMargin="0.0017" BottomMargin="-4.0017" ctype="SpriteObjectData">
                <Size X="39.0000" Y="41.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="191.5710" Y="16.4983" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="1.1973" Y="0.4459" />
                <PreSize X="0.2438" Y="1.1081" />
                <FileData Type="MarkedSubImage" Path="game_public/res/coin_small_icon2.png" Plist="game_public/res/game.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="txt" ActionTag="2006866942" Tag="1512" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="225.9091" RightMargin="-145.9091" FontSize="32" LabelText="11111" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="80.0000" Y="37.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="225.9091" Y="18.5000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="253" G="218" B="108" />
                <PrePosition X="1.4119" Y="0.5000" />
                <PreSize X="0.5000" Y="1.0000" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
            <Position X="464.0500" Y="432.6602" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3867" Y="0.7591" />
            <PreSize X="0.1333" Y="0.0649" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="tip" ActionTag="940921202" Tag="1513" IconVisible="False" LeftMargin="462.9200" RightMargin="669.0800" TopMargin="353.6200" BottomMargin="185.3800" FontSize="27" LabelText="11111" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="68.0000" Y="31.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="462.9200" Y="200.8800" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="243" G="209" B="108" />
            <PrePosition X="0.3858" Y="0.3524" />
            <PreSize X="0.0567" Y="0.0544" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="send_btn" ActionTag="1372810220" Tag="1497" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="483.0000" RightMargin="483.0000" TopMargin="409.2300" BottomMargin="70.7700" ctype="SpriteObjectData">
            <Size X="234.0000" Y="90.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="600.0000" Y="115.7700" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.2031" />
            <PreSize X="0.1950" Y="0.1579" />
            <FileData Type="MarkedSubImage" Path="lan/cn/btn_give.png" Plist="lan/cn/game.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="Text_43" ActionTag="-1848947233" Tag="1518" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="194.0000" RightMargin="194.0000" TopMargin="516.6596" BottomMargin="24.3404" FontSize="25" LabelText="溫馨提示：只能贈送銀行內的金幣，收取2%手續費，從贈送金額中扣除。" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="812.0000" Y="29.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="600.0000" Y="38.8404" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="126" G="89" B="192" />
            <PrePosition X="0.5000" Y="0.0681" />
            <PreSize X="0.6767" Y="0.0509" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>