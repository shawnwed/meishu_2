<GameFile>
  <PropertyGroup Name="record" Type="Layer" ID="f8420dc7-2cb8-41d2-add5-f831de350d49" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="236" ctype="GameLayerObjectData">
        <Size X="1245.0000" Y="570.0000" />
        <Children>
          <AbstractNodeData Name="tip" ActionTag="-173220574" Tag="413" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="542.5000" RightMargin="542.5000" TopMargin="282.9956" BottomMargin="242.0044" FontSize="40" LabelText="暫無數據" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="160.0000" Y="45.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="622.5000" Y="264.5044" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.4640" />
            <PreSize X="0.1285" Y="0.0789" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="title" ActionTag="1690660537" Tag="537" IconVisible="True" PercentWidthEnable="True" PercentWidthEnabled="True" VerticalEdge="TopEdge" LeftMargin="0.0001" RightMargin="-0.0001" TopMargin="0.5466" BottomMargin="509.4534" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="1245.0000" Y="60.0000" />
            <AnchorPoint />
            <Position X="0.0001" Y="509.4534" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0000" Y="0.8938" />
            <PreSize X="1.0000" Y="0.1053" />
            <FileData Type="Normal" Path="hall/csb/bank/record_title.csd" Plist="" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>