<GameFile>
  <PropertyGroup Name="edit_psw" Type="Layer" ID="39a86a9f-e1ca-4096-9fce-8e1c3d72251e" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="236" ctype="GameLayerObjectData">
        <Size X="1010.0000" Y="570.0000" />
        <Children>
          <AbstractNodeData Name="psw1" ActionTag="-1533240670" Tag="252" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="232.8840" RightMargin="337.1160" TopMargin="151.6830" BottomMargin="348.3170" Scale9Enable="True" LeftEage="21" RightEage="21" TopEage="11" BottomEage="11" Scale9OriginX="21" Scale9OriginY="11" Scale9Width="363" Scale9Height="36" ctype="ImageViewObjectData">
            <Size X="440.0000" Y="70.0000" />
            <Children>
              <AbstractNodeData Name="Text_3" ActionTag="-584649788" Tag="253" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="22.6942" RightMargin="281.3058" TopMargin="19.0000" BottomMargin="19.0000" FontSize="28" LabelText="设定密码 : " ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="136.0000" Y="32.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="90.6942" Y="35.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="126" G="89" B="192" />
                <PrePosition X="0.2061" Y="0.5000" />
                <PreSize X="0.3091" Y="0.4571" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="input" ActionTag="-995572934" Tag="254" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="148.1480" RightMargin="51.8520" TopMargin="19.0000" BottomMargin="19.0000" FontSize="28" LabelText="请输入6位数字密码" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="240.0000" Y="32.0000" />
                <Children>
                  <AbstractNodeData Name="cursor" ActionTag="1366468581" VisibleForFrame="False" Tag="364" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="240.0000" RightMargin="-2.0000" TopMargin="1.0000" BottomMargin="1.0000" ctype="SpriteObjectData">
                    <Size X="2.0000" Y="30.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="240.0000" Y="16.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="1.0000" Y="0.5000" />
                    <PreSize X="0.0083" Y="0.9375" />
                    <FileData Type="MarkedSubImage" Path="hall/res/bank/cursor.png" Plist="hall/res/bank.plist" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleY="0.5000" />
                <Position X="148.1480" Y="35.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="126" G="89" B="192" />
                <PrePosition X="0.3367" Y="0.5000" />
                <PreSize X="0.5455" Y="0.4571" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="452.8840" Y="383.3170" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4484" Y="0.6725" />
            <PreSize X="0.4356" Y="0.1228" />
            <FileData Type="MarkedSubImage" Path="hall/res/bank/input.png" Plist="hall/res/bank.plist" />
          </AbstractNodeData>
          <AbstractNodeData Name="psw2" ActionTag="813188323" Tag="255" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="232.8840" RightMargin="337.1160" TopMargin="240.0750" BottomMargin="259.9250" Scale9Enable="True" LeftEage="21" RightEage="21" TopEage="11" BottomEage="11" Scale9OriginX="21" Scale9OriginY="11" Scale9Width="363" Scale9Height="36" ctype="ImageViewObjectData">
            <Size X="440.0000" Y="70.0000" />
            <Children>
              <AbstractNodeData Name="Text_3" ActionTag="-1461876361" Tag="256" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="22.6942" RightMargin="281.3058" TopMargin="19.0000" BottomMargin="19.0000" FontSize="28" LabelText="确认密码 : " ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="136.0000" Y="32.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="90.6942" Y="35.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="126" G="89" B="192" />
                <PrePosition X="0.2061" Y="0.5000" />
                <PreSize X="0.3091" Y="0.4571" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="input" ActionTag="1409634192" Tag="257" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="149.6000" RightMargin="282.4000" TopMargin="19.0000" BottomMargin="19.0000" FontSize="28" LabelText=" " ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="8.0000" Y="32.0000" />
                <Children>
                  <AbstractNodeData Name="cursor" ActionTag="-63118992" VisibleForFrame="False" Tag="365" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="8.0000" RightMargin="-2.0000" TopMargin="1.0000" BottomMargin="1.0000" ctype="SpriteObjectData">
                    <Size X="2.0000" Y="30.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="8.0000" Y="16.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="1.0000" Y="0.5000" />
                    <PreSize X="0.2500" Y="0.9375" />
                    <FileData Type="MarkedSubImage" Path="hall/res/bank/cursor.png" Plist="hall/res/bank.plist" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleY="0.5000" />
                <Position X="149.6000" Y="35.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="126" G="89" B="192" />
                <PrePosition X="0.3400" Y="0.5000" />
                <PreSize X="0.0182" Y="0.4571" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="452.8840" Y="294.9250" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4484" Y="0.5174" />
            <PreSize X="0.4356" Y="0.1228" />
            <FileData Type="MarkedSubImage" Path="hall/res/bank/input.png" Plist="hall/res/bank.plist" />
          </AbstractNodeData>
          <AbstractNodeData Name="tip" ActionTag="511528117" Tag="258" IconVisible="False" LeftMargin="237.8508" RightMargin="564.1492" TopMargin="319.9038" BottomMargin="219.0962" FontSize="26" LabelText="两次密码不一致！" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="208.0000" Y="31.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="237.8508" Y="234.5962" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="248" G="204" B="9" />
            <PrePosition X="0.2355" Y="0.4116" />
            <PreSize X="0.2059" Y="0.0544" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="save" ActionTag="1925191246" Tag="259" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="232.8840" RightMargin="337.1160" TopMargin="366.7189" BottomMargin="123.2811" Scale9Enable="True" LeftEage="16" RightEage="16" TopEage="13" BottomEage="13" Scale9OriginX="16" Scale9OriginY="13" Scale9Width="48" Scale9Height="54" ctype="ImageViewObjectData">
            <Size X="440.0000" Y="80.0000" />
            <Children>
              <AbstractNodeData Name="Text_8" ActionTag="754005830" Tag="260" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="192.0000" RightMargin="192.0000" TopMargin="19.9520" BottomMargin="28.0480" FontSize="28" LabelText="修改" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="56.0000" Y="32.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="220.0000" Y="44.0480" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5506" />
                <PreSize X="0.1273" Y="0.4000" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="452.8840" Y="163.2811" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4484" Y="0.2865" />
            <PreSize X="0.4356" Y="0.1404" />
            <FileData Type="MarkedSubImage" Path="hall/res/bank/keybord_circle.png" Plist="hall/res/bank.plist" />
          </AbstractNodeData>
          <AbstractNodeData Name="keybord" ActionTag="498737494" VisibleForFrame="False" Tag="287" IconVisible="True" LeftMargin="694.2019" RightMargin="13.7981" TopMargin="99.9941" BottomMargin="72.0059" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="302.0000" Y="398.0000" />
            <AnchorPoint />
            <Position X="694.2019" Y="72.0059" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.6873" Y="0.1263" />
            <PreSize X="0.2990" Y="0.6982" />
            <FileData Type="Normal" Path="hall/csb/bank/keybord.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="forget" ActionTag="940608999" Tag="628" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="232.8840" RightMargin="337.1160" TopMargin="455.1729" BottomMargin="34.8271" Scale9Enable="True" LeftEage="16" RightEage="16" TopEage="13" BottomEage="13" Scale9OriginX="16" Scale9OriginY="13" Scale9Width="48" Scale9Height="54" ctype="ImageViewObjectData">
            <Size X="440.0000" Y="80.0000" />
            <Children>
              <AbstractNodeData Name="txt" ActionTag="-946558343" Tag="629" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="156.0000" RightMargin="156.0000" TopMargin="20.9600" BottomMargin="27.0400" FontSize="28" LabelText="忘记密码?" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="128.0000" Y="32.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="220.0000" Y="43.0400" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5380" />
                <PreSize X="0.2909" Y="0.4000" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="452.8840" Y="74.8271" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4484" Y="0.1313" />
            <PreSize X="0.4356" Y="0.1404" />
            <FileData Type="MarkedSubImage" Path="hall/res/bank/keybord_circle.png" Plist="hall/res/bank.plist" />
          </AbstractNodeData>
          <AbstractNodeData Name="old_psw" ActionTag="822738986" Tag="630" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="232.8840" RightMargin="337.1160" TopMargin="66.6577" BottomMargin="433.3423" Scale9Enable="True" LeftEage="21" RightEage="21" TopEage="11" BottomEage="11" Scale9OriginX="21" Scale9OriginY="11" Scale9Width="363" Scale9Height="36" ctype="ImageViewObjectData">
            <Size X="440.0000" Y="70.0000" />
            <Children>
              <AbstractNodeData Name="Text_3" ActionTag="-563665696" Tag="631" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="29.1942" RightMargin="287.8058" TopMargin="19.0000" BottomMargin="19.0000" FontSize="28" LabelText="原 密 码 : " ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="123.0000" Y="32.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="90.6942" Y="35.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="126" G="89" B="192" />
                <PrePosition X="0.2061" Y="0.5000" />
                <PreSize X="0.2795" Y="0.4571" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="input" ActionTag="1460198378" Tag="632" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="148.1480" RightMargin="51.8520" TopMargin="19.0000" BottomMargin="19.0000" FontSize="28" LabelText="请输入6位数字密码" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="240.0000" Y="32.0000" />
                <Children>
                  <AbstractNodeData Name="cursor" ActionTag="1640882166" VisibleForFrame="False" Tag="633" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="240.0000" RightMargin="-2.0000" TopMargin="1.0000" BottomMargin="1.0000" ctype="SpriteObjectData">
                    <Size X="2.0000" Y="30.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="240.0000" Y="16.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="1.0000" Y="0.5000" />
                    <PreSize X="0.0083" Y="0.9375" />
                    <FileData Type="MarkedSubImage" Path="hall/res/bank/cursor.png" Plist="hall/res/bank.plist" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleY="0.5000" />
                <Position X="148.1480" Y="35.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="126" G="89" B="192" />
                <PrePosition X="0.3367" Y="0.5000" />
                <PreSize X="0.5455" Y="0.4571" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="452.8840" Y="468.3423" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4484" Y="0.8217" />
            <PreSize X="0.4356" Y="0.1228" />
            <FileData Type="MarkedSubImage" Path="hall/res/bank/input.png" Plist="hall/res/bank.plist" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>