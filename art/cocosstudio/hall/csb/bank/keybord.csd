<GameFile>
  <PropertyGroup Name="keybord" Type="Layer" ID="1a31705c-7e32-4ba9-a9b0-38ae2eb00ba0" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="261" ctype="GameLayerObjectData">
        <Size X="302.0000" Y="398.0000" />
        <Children>
          <AbstractNodeData Name="keybord_bg_2" ActionTag="-1625148963" Tag="263" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="21.5027" RightMargin="21.4973" TopMargin="25.4995" BottomMargin="25.5005" ctype="SpriteObjectData">
            <Size X="259.0000" Y="347.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="151.0027" Y="199.0005" />
            <Scale ScaleX="1.1572" ScaleY="1.1264" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="0.8576" Y="0.8719" />
            <FileData Type="MarkedSubImage" Path="hall/res/bank/keybord_bg.png" Plist="hall/res/bank.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="ok" ActionTag="471294478" Tag="286" IconVisible="False" LeftMargin="196.9997" RightMargin="7.0003" TopMargin="297.0001" BottomMargin="2.9999" ctype="SpriteObjectData">
            <Size X="80.0000" Y="80.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="245.9997" Y="51.9999" />
            <Scale ScaleX="1.1500" ScaleY="1.1500" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8146" Y="0.1307" />
            <PreSize X="0.3245" Y="0.2462" />
            <FileData Type="MarkedSubImage" Path="hall/res/bank/ok.png" Plist="hall/res/bank.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="num7" ActionTag="-2031429967" Tag="312" IconVisible="False" LeftMargin="6.0001" RightMargin="197.9999" TopMargin="9.9999" BottomMargin="290.0001" ctype="SpriteObjectData">
            <Size X="80.0000" Y="80.0000" />
            <Children>
              <AbstractNodeData Name="txt" ActionTag="-530039134" Tag="552" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="28.0652" RightMargin="30.9348" TopMargin="20.4347" BottomMargin="17.5653" FontSize="36" LabelText="7" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="21.0000" Y="42.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="38.5652" Y="38.5653" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4821" Y="0.4821" />
                <PreSize X="0.2143" Y="0.4286" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="55.0001" Y="339.0001" />
            <Scale ScaleX="1.1500" ScaleY="1.1500" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.1821" Y="0.8518" />
            <PreSize X="0.3245" Y="0.2462" />
            <FileData Type="MarkedSubImage" Path="hall/res/bank/keybord_circle.png" Plist="hall/res/bank.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="num8" ActionTag="1010854804" Tag="313" IconVisible="False" LeftMargin="101.5000" RightMargin="102.5000" TopMargin="9.9999" BottomMargin="290.0001" ctype="SpriteObjectData">
            <Size X="80.0000" Y="80.0000" />
            <Children>
              <AbstractNodeData Name="txt" ActionTag="-33918650" Tag="553" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="28.0649" RightMargin="30.9351" TopMargin="20.4348" BottomMargin="17.5652" FontSize="36" LabelText="8" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="21.0000" Y="42.0000" />
                <AnchorPoint ScaleX="0.4361" ScaleY="0.4167" />
                <Position X="37.2226" Y="35.0670" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4653" Y="0.4383" />
                <PreSize X="0.2143" Y="0.4286" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="150.5000" Y="339.0001" />
            <Scale ScaleX="1.1500" ScaleY="1.1500" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4983" Y="0.8518" />
            <PreSize X="0.3245" Y="0.2462" />
            <FileData Type="MarkedSubImage" Path="hall/res/bank/keybord_derect.png" Plist="hall/res/bank.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="num9" ActionTag="-602352562" Tag="314" IconVisible="False" LeftMargin="196.9997" RightMargin="7.0003" TopMargin="9.9999" BottomMargin="290.0001" FlipX="True" ctype="SpriteObjectData">
            <Size X="80.0000" Y="80.0000" />
            <Children>
              <AbstractNodeData Name="txt" ActionTag="-2006452600" Tag="554" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="28.0651" RightMargin="30.9349" TopMargin="20.4349" BottomMargin="17.5651" FontSize="36" LabelText="9" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="21.0000" Y="42.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="38.5651" Y="38.5651" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4821" Y="0.4821" />
                <PreSize X="0.2143" Y="0.4286" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="245.9997" Y="339.0001" />
            <Scale ScaleX="1.1500" ScaleY="1.1500" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8146" Y="0.8518" />
            <PreSize X="0.3245" Y="0.2462" />
            <FileData Type="MarkedSubImage" Path="hall/res/bank/keybord_circle.png" Plist="hall/res/bank.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="num4" ActionTag="553555237" Tag="315" IconVisible="False" LeftMargin="5.9999" RightMargin="198.0001" TopMargin="105.9996" BottomMargin="194.0004" ctype="SpriteObjectData">
            <Size X="80.0000" Y="80.0000" />
            <Children>
              <AbstractNodeData Name="txt" ActionTag="1115230747" Tag="556" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="28.0632" RightMargin="30.9368" TopMargin="20.4351" BottomMargin="17.5649" FontSize="36" LabelText="4" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="21.0000" Y="42.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="38.5632" Y="38.5649" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4820" Y="0.4821" />
                <PreSize X="0.2143" Y="0.4286" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="54.9999" Y="243.0004" />
            <Scale ScaleX="1.1500" ScaleY="1.1500" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.1821" Y="0.6106" />
            <PreSize X="0.3245" Y="0.2462" />
            <FileData Type="MarkedSubImage" Path="hall/res/bank/keybord_derect.png" Plist="hall/res/bank.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="num5" ActionTag="1380629842" Tag="316" IconVisible="False" LeftMargin="101.5000" RightMargin="102.5000" TopMargin="105.9996" BottomMargin="194.0004" ctype="SpriteObjectData">
            <Size X="80.0000" Y="80.0000" />
            <Children>
              <AbstractNodeData Name="txt" ActionTag="-542476853" Tag="557" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="28.0631" RightMargin="30.9369" TopMargin="20.4351" BottomMargin="17.5649" FontSize="36" LabelText="5" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="21.0000" Y="42.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="38.5631" Y="38.5649" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4820" Y="0.4821" />
                <PreSize X="0.2143" Y="0.4286" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="150.5000" Y="243.0004" />
            <Scale ScaleX="1.1500" ScaleY="1.1500" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4983" Y="0.6106" />
            <PreSize X="0.3245" Y="0.2462" />
            <FileData Type="MarkedSubImage" Path="hall/res/bank/keybord_derect.png" Plist="hall/res/bank.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="num6" ActionTag="-1113232681" Tag="317" IconVisible="False" LeftMargin="196.9997" RightMargin="7.0003" TopMargin="105.9996" BottomMargin="194.0004" ctype="SpriteObjectData">
            <Size X="80.0000" Y="80.0000" />
            <Children>
              <AbstractNodeData Name="txt" ActionTag="-540857350" Tag="558" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="28.0651" RightMargin="30.9349" TopMargin="20.4351" BottomMargin="17.5649" FontSize="36" LabelText="6" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="21.0000" Y="42.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="38.5651" Y="38.5649" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4821" Y="0.4821" />
                <PreSize X="0.2143" Y="0.4286" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="245.9997" Y="243.0004" />
            <Scale ScaleX="1.1500" ScaleY="1.1500" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8146" Y="0.6106" />
            <PreSize X="0.3245" Y="0.2462" />
            <FileData Type="MarkedSubImage" Path="hall/res/bank/keybord_derect.png" Plist="hall/res/bank.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="num1" ActionTag="-1002190069" Tag="318" IconVisible="False" LeftMargin="5.9999" RightMargin="198.0001" TopMargin="201.0001" BottomMargin="98.9999" ctype="SpriteObjectData">
            <Size X="80.0000" Y="80.0000" />
            <Children>
              <AbstractNodeData Name="txt" ActionTag="-946085384" Tag="559" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="28.0632" RightMargin="30.9368" TopMargin="20.4350" BottomMargin="17.5650" FontSize="36" LabelText="1" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="21.0000" Y="42.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="38.5632" Y="38.5650" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4820" Y="0.4821" />
                <PreSize X="0.2143" Y="0.4286" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="54.9999" Y="147.9999" />
            <Scale ScaleX="1.1500" ScaleY="1.1500" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.1821" Y="0.3719" />
            <PreSize X="0.3245" Y="0.2462" />
            <FileData Type="MarkedSubImage" Path="hall/res/bank/keybord_derect.png" Plist="hall/res/bank.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="num2" ActionTag="365563276" Tag="319" IconVisible="False" LeftMargin="101.5000" RightMargin="102.5000" TopMargin="201.0001" BottomMargin="98.9999" ctype="SpriteObjectData">
            <Size X="80.0000" Y="80.0000" />
            <Children>
              <AbstractNodeData Name="txt" ActionTag="2049254498" Tag="560" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="28.0631" RightMargin="30.9369" TopMargin="20.4350" BottomMargin="17.5650" FontSize="36" LabelText="2" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="21.0000" Y="42.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="38.5631" Y="38.5650" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4820" Y="0.4821" />
                <PreSize X="0.2143" Y="0.4286" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="150.5000" Y="147.9999" />
            <Scale ScaleX="1.1500" ScaleY="1.1500" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4983" Y="0.3719" />
            <PreSize X="0.3245" Y="0.2462" />
            <FileData Type="MarkedSubImage" Path="hall/res/bank/keybord_derect.png" Plist="hall/res/bank.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="num3" ActionTag="-884641270" Tag="320" IconVisible="False" LeftMargin="196.9997" RightMargin="7.0003" TopMargin="201.0001" BottomMargin="98.9999" ctype="SpriteObjectData">
            <Size X="80.0000" Y="80.0000" />
            <Children>
              <AbstractNodeData Name="txt" ActionTag="-272530102" Tag="561" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="28.0651" RightMargin="30.9349" TopMargin="20.4350" BottomMargin="17.5650" FontSize="36" LabelText="3" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="21.0000" Y="42.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="38.5651" Y="38.5650" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4821" Y="0.4821" />
                <PreSize X="0.2143" Y="0.4286" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="245.9997" Y="147.9999" />
            <Scale ScaleX="1.1500" ScaleY="1.1500" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8146" Y="0.3719" />
            <PreSize X="0.3245" Y="0.2462" />
            <FileData Type="MarkedSubImage" Path="hall/res/bank/keybord_derect.png" Plist="hall/res/bank.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="num0" ActionTag="647786832" Tag="321" IconVisible="False" LeftMargin="5.9999" RightMargin="198.0001" TopMargin="297.0001" BottomMargin="2.9999" ctype="SpriteObjectData">
            <Size X="80.0000" Y="80.0000" />
            <Children>
              <AbstractNodeData Name="txt" ActionTag="-657089618" Tag="562" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="28.0632" RightMargin="30.9368" TopMargin="20.4346" BottomMargin="17.5654" FontSize="36" LabelText="0" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="21.0000" Y="42.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="38.5632" Y="38.5654" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4820" Y="0.4821" />
                <PreSize X="0.2143" Y="0.4286" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="54.9999" Y="51.9999" />
            <Scale ScaleX="1.1500" ScaleY="1.1500" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.1821" Y="0.1307" />
            <PreSize X="0.3245" Y="0.2462" />
            <FileData Type="MarkedSubImage" Path="hall/res/bank/keybord_circle.png" Plist="hall/res/bank.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="delete" ActionTag="1201231126" Tag="551" IconVisible="False" LeftMargin="101.4999" RightMargin="102.5001" TopMargin="297.0001" BottomMargin="2.9999" ctype="SpriteObjectData">
            <Size X="80.0000" Y="80.0000" />
            <Children>
              <AbstractNodeData Name="delete_3" ActionTag="420542727" Tag="285" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="20.0641" RightMargin="20.9359" TopMargin="24.9358" BottomMargin="28.0642" ctype="SpriteObjectData">
                <Size X="39.0000" Y="27.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="39.5641" Y="41.5642" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4946" Y="0.5196" />
                <PreSize X="0.4592" Y="0.3163" />
                <FileData Type="MarkedSubImage" Path="hall/res/bank/delete.png" Plist="hall/res/bank.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="150.4999" Y="51.9999" />
            <Scale ScaleX="1.1500" ScaleY="1.1500" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4983" Y="0.1307" />
            <PreSize X="0.3245" Y="0.2462" />
            <FileData Type="MarkedSubImage" Path="hall/res/bank/keybord_derect.png" Plist="hall/res/bank.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>