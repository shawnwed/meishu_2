<GameFile>
  <PropertyGroup Name="record_title" Type="Layer" ID="723a44e9-351e-4f2a-beaf-5273605f8734" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="500" ctype="GameLayerObjectData">
        <Size X="1245.0000" Y="60.0000" />
        <Children>
          <AbstractNodeData Name="name" ActionTag="1725368009" Tag="503" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="528.8085" RightMargin="620.1915" TopMargin="18.0000" BottomMargin="18.0000" FontSize="24" LabelText="銀行餘額" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="96.0000" Y="24.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="576.8085" Y="30.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="162" G="197" B="253" />
            <PrePosition X="0.4633" Y="0.5000" />
            <PreSize X="0.0771" Y="0.4000" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="type" ActionTag="996281604" Tag="505" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="230.9760" RightMargin="966.0240" TopMargin="18.0000" BottomMargin="18.0000" FontSize="24" LabelText="類型" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="48.0000" Y="24.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="254.9760" Y="30.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="162" G="197" B="253" />
            <PrePosition X="0.2048" Y="0.5000" />
            <PreSize X="0.0386" Y="0.4000" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="num" ActionTag="-266540665" Tag="506" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="372.7815" RightMargin="824.2185" TopMargin="18.0000" BottomMargin="18.0000" FontSize="24" LabelText="金額" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="48.0000" Y="24.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="396.7815" Y="30.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="162" G="197" B="253" />
            <PrePosition X="0.3187" Y="0.5000" />
            <PreSize X="0.0386" Y="0.4000" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="date" ActionTag="-1428153171" Tag="507" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="75.9735" RightMargin="1121.0265" TopMargin="18.0000" BottomMargin="18.0000" FontSize="24" LabelText="日期" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="48.0000" Y="24.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="99.9735" Y="30.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="162" G="197" B="253" />
            <PrePosition X="0.0803" Y="0.5000" />
            <PreSize X="0.0386" Y="0.4000" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="id" ActionTag="1368345679" Tag="508" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="802.3065" RightMargin="394.6935" TopMargin="18.0000" BottomMargin="18.0000" FontSize="24" LabelText="備註" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="48.0000" Y="24.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="826.3065" Y="30.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="162" G="197" B="253" />
            <PrePosition X="0.6637" Y="0.5000" />
            <PreSize X="0.0386" Y="0.4000" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="id_0" ActionTag="1338687242" Tag="536" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="1084.2056" RightMargin="112.7944" TopMargin="18.0000" BottomMargin="18.0000" FontSize="24" LabelText="操作" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="48.0000" Y="24.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1108.2056" Y="30.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="162" G="197" B="253" />
            <PrePosition X="0.8901" Y="0.5000" />
            <PreSize X="0.0386" Y="0.4000" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="line" ActionTag="187403775" Tag="1663" IconVisible="False" PercentWidthEnable="True" PercentWidthEnabled="True" TopMargin="56.0000" BottomMargin="2.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="1245.0000" Y="2.0000" />
            <AnchorPoint />
            <Position Y="2.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition Y="0.0333" />
            <PreSize X="1.0000" Y="0.0333" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>