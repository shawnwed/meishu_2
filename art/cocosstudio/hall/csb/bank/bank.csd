<GameFile>
  <PropertyGroup Name="bank" Type="Scene" ID="9b3c9b2f-1a95-48cf-a768-c5a7c87f08b4" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Scene" Tag="342" ctype="GameNodeObjectData">
        <Size X="960.0000" Y="640.0000" />
        <Children>
          <AbstractNodeData Name="bg" ActionTag="1978672851" Tag="410" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" TouchEnable="True" Scale9Enable="True" TopEage="90" Scale9OriginY="90" Scale9Width="1280" Scale9Height="630" ctype="ImageViewObjectData">
            <Size X="960.0000" Y="640.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="480.0000" Y="320.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="1.0000" Y="1.0000" />
            <FileData Type="Normal" Path="bg/bg_info.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="bg_info_content" ActionTag="531853565" Tag="2911" IconVisible="False" PositionPercentXEnabled="True" PercentWidthEnable="True" PercentWidthEnabled="True" LeftMargin="6.0000" RightMargin="6.0000" TopMargin="21.0000" BottomMargin="5.0000" Scale9Enable="True" LeftEage="25" RightEage="25" TopEage="25" BottomEage="85" Scale9OriginX="25" Scale9OriginY="25" Scale9Width="248" Scale9Height="456" ctype="ImageViewObjectData">
            <Size X="948.0000" Y="614.0000" />
            <AnchorPoint ScaleX="0.5000" />
            <Position X="480.0000" Y="5.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.0078" />
            <PreSize X="0.9875" Y="0.9594" />
            <FileData Type="Normal" Path="bg/bg_info_content.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="top" ActionTag="302368502" Tag="393" IconVisible="True" PercentWidthEnable="True" PercentWidthEnabled="True" TopMargin="-79.6800" BottomMargin="624.6800" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="True" ctype="ProjectNodeObjectData">
            <Size X="960.0000" Y="95.0000" />
            <AnchorPoint />
            <Position Y="624.6800" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition Y="0.9761" />
            <PreSize X="1.0000" Y="0.1484" />
            <FileData Type="Normal" Path="game_public/csb/hall_top.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="tab" ActionTag="1084399273" Tag="386" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="455.9760" RightMargin="9.0240" TopMargin="554.0000" BottomMargin="5.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="0" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="495.0000" Y="81.0000" />
            <Children>
              <AbstractNodeData Name="tab1" ActionTag="-1805775384" Tag="2423" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="-273.5343" RightMargin="508.5343" TopMargin="-21.2731" BottomMargin="-5.7269" ctype="SpriteObjectData">
                <Size X="260.0000" Y="108.0000" />
                <Children>
                  <AbstractNodeData Name="line" ActionTag="-1141894370" Tag="3293" IconVisible="False" LeftMargin="0.2665" RightMargin="-2.2665" TopMargin="0.0000" BottomMargin="0.0000" ctype="SpriteObjectData">
                    <Size X="262.0000" Y="108.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="131.2665" Y="54.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5049" Y="0.5000" />
                    <PreSize X="1.0077" Y="1.0000" />
                    <FileData Type="MarkedSubImage" Path="hall/res/info/tab_line.png" Plist="hall/res/myinfo.plist" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="txt" ActionTag="-187630604" Tag="2424" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="70.0000" RightMargin="70.0000" TopMargin="34.0000" BottomMargin="44.0000" FontSize="30" LabelText="存    取" ShadowOffsetX="-1.0000" ShadowOffsetY="1.0000" ShadowEnabled="True" ctype="TextObjectData">
                    <Size X="120.0000" Y="30.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="130.0000" Y="59.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5463" />
                    <PreSize X="0.4615" Y="0.2778" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="0" G="0" B="0" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="-143.5343" Y="48.2731" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="-0.2900" Y="0.5960" />
                <PreSize X="0.5253" Y="1.3333" />
                <FileData Type="MarkedSubImage" Path="hall/res/info/tab_btn_1.png" Plist="hall/res/myinfo.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="tab2" ActionTag="-540668461" Tag="2421" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-17.6954" RightMargin="252.6954" TopMargin="-21.2731" BottomMargin="-5.7269" ctype="SpriteObjectData">
                <Size X="260.0000" Y="108.0000" />
                <Children>
                  <AbstractNodeData Name="txt" ActionTag="-601234004" Tag="2422" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="70.0000" RightMargin="70.0000" TopMargin="31.4884" BottomMargin="46.5116" FontSize="30" LabelText="贈    送" ShadowOffsetX="-1.0000" ShadowOffsetY="1.0000" ShadowEnabled="True" ctype="TextObjectData">
                    <Size X="120.0000" Y="30.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="130.0000" Y="61.5116" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5696" />
                    <PreSize X="0.4615" Y="0.2778" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="0" G="0" B="0" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="line" ActionTag="-789878597" Tag="2425" IconVisible="False" LeftMargin="-0.3955" RightMargin="-1.6046" TopMargin="-0.2568" BottomMargin="0.2568" ctype="SpriteObjectData">
                    <Size X="262.0000" Y="108.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="130.6045" Y="54.2568" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5023" Y="0.5024" />
                    <PreSize X="1.0077" Y="1.0000" />
                    <FileData Type="MarkedSubImage" Path="hall/res/info/tab_line.png" Plist="hall/res/myinfo.plist" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="112.3046" Y="48.2731" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2269" Y="0.5960" />
                <PreSize X="0.5253" Y="1.3333" />
                <FileData Type="MarkedSubImage" Path="hall/res/info/tab_btn_1.png" Plist="hall/res/myinfo.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="tab3" ActionTag="-275723924" Tag="2418" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="235.7712" RightMargin="-0.7712" TopMargin="-21.2731" BottomMargin="-5.7269" ctype="SpriteObjectData">
                <Size X="260.0000" Y="108.0000" />
                <Children>
                  <AbstractNodeData Name="line" ActionTag="1421100916" Tag="2419" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="-0.3983" RightMargin="-1.6017" TopMargin="-3.1673" BottomMargin="3.1673" ctype="SpriteObjectData">
                    <Size X="262.0000" Y="108.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="130.6017" Y="57.1673" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5023" Y="0.5293" />
                    <PreSize X="1.0077" Y="1.0000" />
                    <FileData Type="MarkedSubImage" Path="hall/res/info/tab_line.png" Plist="hall/res/myinfo.plist" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="txt" ActionTag="1226149306" Tag="2420" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="73.7556" RightMargin="66.2444" TopMargin="30.5493" BottomMargin="47.4507" FontSize="30" LabelText="記    錄" ShadowOffsetX="-1.0000" ShadowOffsetY="1.0000" ShadowEnabled="True" ctype="TextObjectData">
                    <Size X="120.0000" Y="30.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="133.7556" Y="62.4507" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5144" Y="0.5782" />
                    <PreSize X="0.4615" Y="0.2778" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="0" G="0" B="0" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="365.7712" Y="48.2731" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7389" Y="0.5960" />
                <PreSize X="0.5253" Y="1.3333" />
                <FileData Type="MarkedSubImage" Path="hall/res/info/tab_btn_2.png" Plist="hall/res/myinfo.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="1.0000" />
            <Position X="950.9760" Y="5.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.9906" Y="0.0078" />
            <PreSize X="0.5156" Y="0.1266" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="container" CanEdit="False" ActionTag="-1617746298" Tag="871" IconVisible="False" PositionPercentXEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" LeftMargin="9.6000" RightMargin="9.6000" TopMargin="86.8898" BottomMargin="105.1102" ClipAble="False" BackColorAlpha="127" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="940.8000" Y="448.0000" />
            <AnchorPoint />
            <Position X="9.6000" Y="105.1102" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0100" Y="0.1642" />
            <PreSize X="0.9800" Y="0.7000" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="container2" CanEdit="False" ActionTag="-1876011733" Tag="784" IconVisible="False" PositionPercentXEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" LeftMargin="9.6000" RightMargin="9.6000" TopMargin="97.4000" BottomMargin="5.0000" ClipAble="False" BackColorAlpha="127" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="940.8000" Y="537.6000" />
            <AnchorPoint />
            <Position X="9.6000" Y="5.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0100" Y="0.0078" />
            <PreSize X="0.9800" Y="0.8400" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="my_total" ActionTag="1177269754" Tag="1705" IconVisible="False" LeftMargin="42.0457" RightMargin="737.9543" TopMargin="580.5583" BottomMargin="29.4417" FontSize="30" LabelText="我的總資產：" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="180.0000" Y="30.0000" />
            <Children>
              <AbstractNodeData Name="txt" ActionTag="-998765372" Tag="1706" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="174.1119" RightMargin="-90.1119" TopMargin="-1.5000" BottomMargin="-1.5000" FontSize="32" LabelText="100000" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="96.0000" Y="33.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="174.1119" Y="15.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="253" G="218" B="106" />
                <PrePosition X="0.9673" Y="0.5000" />
                <PreSize X="0.5333" Y="1.1000" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleY="0.5000" />
            <Position X="42.0457" Y="44.4417" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0438" Y="0.0694" />
            <PreSize X="0.1875" Y="0.0469" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>