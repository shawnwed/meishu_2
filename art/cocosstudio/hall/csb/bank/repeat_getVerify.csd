<GameFile>
  <PropertyGroup Name="repeat_getVerify" Type="Layer" ID="5a0c6aa5-6e73-48fd-adcc-2607ec63287a" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="content" Tag="267" ctype="GameLayerObjectData">
        <Size X="240.0000" Y="70.0000" />
        <Children>
          <AbstractNodeData Name="repeat" ActionTag="166542173" Tag="273" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" Scale9Enable="True" LeftEage="13" RightEage="16" TopEage="13" BottomEage="16" Scale9OriginX="13" Scale9OriginY="13" Scale9Width="51" Scale9Height="51" ctype="ImageViewObjectData">
            <Size X="240.0000" Y="70.0000" />
            <Children>
              <AbstractNodeData Name="Text_3" ActionTag="-942568648" Tag="270" IconVisible="False" LeftMargin="57.0613" RightMargin="82.9387" TopMargin="35.1220" BottomMargin="11.8780" FontSize="20" LabelText="发送验证码" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="100.0000" Y="23.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="107.0613" Y="23.3780" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4461" Y="0.3340" />
                <PreSize X="0.4167" Y="0.3286" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_2" ActionTag="180680106" Tag="271" IconVisible="False" LeftMargin="80.1713" RightMargin="59.8287" TopMargin="11.2470" BottomMargin="35.7530" FontSize="20" LabelText="秒后可重新" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="100.0000" Y="23.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="130.1713" Y="47.2530" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5424" Y="0.6750" />
                <PreSize X="0.4167" Y="0.3286" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="txt" ActionTag="-538201862" Tag="272" IconVisible="False" LeftMargin="54.1198" RightMargin="162.8802" TopMargin="11.4301" BottomMargin="35.5699" FontSize="20" LabelText="60" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="23.0000" Y="23.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="65.6198" Y="47.0699" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="248" G="204" B="9" />
                <PrePosition X="0.2734" Y="0.6724" />
                <PreSize X="0.0958" Y="0.3286" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="120.0000" Y="35.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="1.0000" Y="1.0000" />
            <FileData Type="MarkedSubImage" Path="hall/res/bank/keybord_circle.png" Plist="hall/res/bank.plist" />
          </AbstractNodeData>
          <AbstractNodeData Name="getVerify" Visible="False" ActionTag="-157624182" Tag="274" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftEage="16" RightEage="16" TopEage="13" BottomEage="13" Scale9OriginX="16" Scale9OriginY="13" Scale9Width="149" Scale9Height="28" ctype="ImageViewObjectData">
            <Size X="240.0000" Y="70.0000" />
            <Children>
              <AbstractNodeData Name="txt" ActionTag="-1932418098" VisibleForFrame="False" Tag="275" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="55.0000" RightMargin="55.0000" TopMargin="16.7350" BottomMargin="22.2650" FontSize="26" LabelText="發送驗證碼" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="130.0000" Y="31.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="120.0000" Y="37.7650" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5395" />
                <PreSize X="0.5417" Y="0.4429" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="120.0000" Y="35.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="1.0000" Y="1.0000" />
            <FileData Type="MarkedSubImage" Path="hall/res/bank/btn_get_verify.png" Plist="hall/res/bank.plist" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>