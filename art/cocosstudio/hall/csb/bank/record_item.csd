<GameFile>
  <PropertyGroup Name="record_item" Type="Layer" ID="118186fe-8502-4efb-ae1c-74b0b67a892c" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="500" ctype="GameLayerObjectData">
        <Size X="1245.0000" Y="64.0000" />
        <Children>
          <AbstractNodeData Name="bg" ActionTag="1878039473" Tag="1662" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" ClipAble="False" BackColorAlpha="76" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="1245.0000" Y="64.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="622.5000" Y="32.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="1.0000" Y="1.0000" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="mark" ActionTag="21541410" Tag="504" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="719.8890" RightMargin="291.1110" TopMargin="19.0832" BottomMargin="18.9168" FontSize="26" LabelText="接收者ID:542365123" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="234.0000" Y="26.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="836.8890" Y="31.9168" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.6722" Y="0.4987" />
            <PreSize X="0.1880" Y="0.4063" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="type" ActionTag="996281604" Tag="505" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="228.9760" RightMargin="964.0240" TopMargin="19.0832" BottomMargin="18.9168" FontSize="26" LabelText="赠送" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="52.0000" Y="26.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="254.9760" Y="31.9168" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="252" G="216" B="0" />
            <PrePosition X="0.2048" Y="0.4987" />
            <PreSize X="0.0418" Y="0.4063" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="num" ActionTag="-266540665" Tag="506" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="325.2815" RightMargin="776.7185" TopMargin="19.0832" BottomMargin="18.9168" FontSize="26" LabelText="100,000,000" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="143.0000" Y="26.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="396.7815" Y="31.9168" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3187" Y="0.4987" />
            <PreSize X="0.1149" Y="0.4063" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="date" ActionTag="-1428153171" Tag="507" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="1.2025" RightMargin="1035.7975" TopMargin="16.9136" BottomMargin="21.0864" FontSize="26" LabelText="2016-05-25 23:55" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="208.0000" Y="26.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="105.2025" Y="34.0864" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0845" Y="0.5326" />
            <PreSize X="0.1671" Y="0.4063" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="send" ActionTag="643695639" Tag="510" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="969.7305" RightMargin="155.2695" TopMargin="6.9984" BottomMargin="-2.9984" ctype="SpriteObjectData">
            <Size X="120.0000" Y="60.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="969.7305" Y="27.0016" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.7789" Y="0.4219" />
            <PreSize X="0.0964" Y="0.9375" />
            <FileData Type="MarkedSubImage" Path="lan/cn/btn_give2.png" Plist="lan/cn/game.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="detail" ActionTag="1480147578" Tag="509" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="1100.9535" RightMargin="24.0465" TopMargin="6.9984" BottomMargin="-2.9984" ctype="SpriteObjectData">
            <Size X="120.0000" Y="60.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="1100.9535" Y="27.0016" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8843" Y="0.4219" />
            <PreSize X="0.0964" Y="0.9375" />
            <FileData Type="MarkedSubImage" Path="lan/cn/btn_xiangqing.png" Plist="lan/cn/game.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="balance" ActionTag="2086814662" Tag="1530" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="505.3085" RightMargin="596.6915" TopMargin="19.0832" BottomMargin="18.9168" FontSize="26" LabelText="100,000,000" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="143.0000" Y="26.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="576.8085" Y="31.9168" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4633" Y="0.4987" />
            <PreSize X="0.1149" Y="0.4063" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>