<GameFile>
  <PropertyGroup Name="setting" Type="Layer" ID="060045da-c230-4db3-a2d7-1be244cdf738" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="236" ctype="GameLayerObjectData">
        <Size X="1200.0000" Y="570.0000" />
        <Children>
          <AbstractNodeData Name="getVerify" ActionTag="-549811902" Tag="574" IconVisible="True" LeftMargin="574.4800" RightMargin="385.5200" TopMargin="214.2893" BottomMargin="276.7107" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="True" ctype="ProjectNodeObjectData">
            <Size X="240.0000" Y="79.0000" />
            <AnchorPoint />
            <Position X="574.4800" Y="276.7107" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4787" Y="0.4855" />
            <PreSize X="0.2000" Y="0.1386" />
            <FileData Type="Normal" Path="hall/csb/bank/repeat_getVerify.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="save" ActionTag="-431560484" Tag="582" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="477.1200" RightMargin="488.8800" TopMargin="419.4403" BottomMargin="60.5597" Scale9Enable="True" LeftEage="16" RightEage="16" TopEage="13" BottomEage="13" Scale9OriginX="16" Scale9OriginY="13" Scale9Width="202" Scale9Height="64" ctype="ImageViewObjectData">
            <Size X="234.0000" Y="90.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="594.1200" Y="105.5597" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4951" Y="0.1852" />
            <PreSize X="0.1950" Y="0.1579" />
            <FileData Type="Normal" Path="hall/res/bank/zhucewancheng.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="tip" ActionTag="-1648040657" Tag="583" IconVisible="False" LeftMargin="374.4800" RightMargin="681.5200" TopMargin="381.1242" BottomMargin="160.8758" FontSize="24" LabelText="验证码错误！" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="144.0000" Y="28.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="374.4800" Y="174.8758" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="248" G="204" B="9" />
            <PrePosition X="0.3121" Y="0.3068" />
            <PreSize X="0.1200" Y="0.0491" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="email" ActionTag="2105111371" Tag="584" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="374.4800" RightMargin="385.5200" TopMargin="135.7960" BottomMargin="364.2040" Scale9Enable="True" LeftEage="21" RightEage="21" TopEage="11" BottomEage="11" Scale9OriginX="21" Scale9OriginY="11" Scale9Width="363" Scale9Height="36" ctype="ImageViewObjectData">
            <Size X="440.0000" Y="70.0000" />
            <Children>
              <AbstractNodeData Name="input" ActionTag="78840746" Tag="586" IconVisible="False" LeftMargin="20.0005" RightMargin="19.9995" TopMargin="18.5003" BottomMargin="16.4997" TouchEnable="True" FontSize="30" IsCustomSize="True" LabelText="" PlaceHolderText="請輸入您的郵箱地址" MaxLengthText="10" ctype="TextFieldObjectData">
                <Size X="400.0000" Y="35.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="20.0005" Y="33.9997" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="126" G="89" B="192" />
                <PrePosition X="0.0455" Y="0.4857" />
                <PreSize X="0.9091" Y="0.5000" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="594.4800" Y="399.2040" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4954" Y="0.7004" />
            <PreSize X="0.3667" Y="0.1228" />
            <FileData Type="MarkedSubImage" Path="hall/res/bank/input.png" Plist="hall/res/bank.plist" />
          </AbstractNodeData>
          <AbstractNodeData Name="psw1" ActionTag="1690366346" Tag="587" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="374.4800" RightMargin="385.5200" TopMargin="303.2061" BottomMargin="196.7939" Scale9Enable="True" LeftEage="21" RightEage="21" TopEage="11" BottomEage="11" Scale9OriginX="21" Scale9OriginY="11" Scale9Width="363" Scale9Height="36" ctype="ImageViewObjectData">
            <Size X="440.0000" Y="70.0000" />
            <Children>
              <AbstractNodeData Name="input" ActionTag="-1905729325" Tag="588" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="12.8920" RightMargin="131.1080" TopMargin="18.0000" BottomMargin="18.0000" FontSize="30" LabelText=" 設定您的6位數字密碼" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="296.0000" Y="34.0000" />
                <Children>
                  <AbstractNodeData Name="cursor" ActionTag="-585567724" VisibleForFrame="False" Tag="589" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="296.0000" RightMargin="-2.0000" TopMargin="2.0000" BottomMargin="2.0000" ctype="SpriteObjectData">
                    <Size X="2.0000" Y="30.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="296.0000" Y="17.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="1.0000" Y="0.5000" />
                    <PreSize X="0.0068" Y="0.8824" />
                    <FileData Type="MarkedSubImage" Path="hall/res/bank/cursor.png" Plist="hall/res/bank.plist" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleY="0.5000" />
                <Position X="12.8920" Y="35.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="126" G="89" B="192" />
                <PrePosition X="0.0293" Y="0.5000" />
                <PreSize X="0.6727" Y="0.4857" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="594.4800" Y="231.7939" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4954" Y="0.4067" />
            <PreSize X="0.3667" Y="0.1228" />
            <FileData Type="MarkedSubImage" Path="hall/res/bank/input.png" Plist="hall/res/bank.plist" />
          </AbstractNodeData>
          <AbstractNodeData Name="verify" ActionTag="1039174042" Tag="590" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="374.4800" RightMargin="645.5200" TopMargin="219.5012" BottomMargin="280.4988" Scale9Enable="True" LeftEage="21" RightEage="21" TopEage="11" BottomEage="11" Scale9OriginX="21" Scale9OriginY="11" Scale9Width="363" Scale9Height="36" ctype="ImageViewObjectData">
            <Size X="180.0000" Y="70.0000" />
            <Children>
              <AbstractNodeData Name="input" ActionTag="-1093921086" Tag="591" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="9.3060" RightMargin="161.6940" TopMargin="18.0000" BottomMargin="18.0000" FontSize="30" LabelText=" " ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="9.0000" Y="34.0000" />
                <Children>
                  <AbstractNodeData Name="cursor" ActionTag="12751911" VisibleForFrame="False" Tag="592" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="9.0000" RightMargin="-2.0000" TopMargin="2.0000" BottomMargin="2.0000" ctype="SpriteObjectData">
                    <Size X="2.0000" Y="30.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="9.0000" Y="17.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="1.0000" Y="0.5000" />
                    <PreSize X="0.2222" Y="0.8824" />
                    <FileData Type="MarkedSubImage" Path="hall/res/bank/cursor.png" Plist="hall/res/bank.plist" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleY="0.5000" />
                <Position X="9.3060" Y="35.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="126" G="89" B="192" />
                <PrePosition X="0.0517" Y="0.5000" />
                <PreSize X="0.0500" Y="0.4857" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="464.4800" Y="315.4988" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3871" Y="0.5535" />
            <PreSize X="0.1500" Y="0.1228" />
            <FileData Type="MarkedSubImage" Path="hall/res/bank/input.png" Plist="hall/res/bank.plist" />
          </AbstractNodeData>
          <AbstractNodeData Name="title" ActionTag="-269490523" Tag="700" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="390.0000" RightMargin="390.0000" TopMargin="42.0000" BottomMargin="460.0000" FontSize="60" LabelText="歡迎使用保險箱" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="420.0000" Y="68.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="600.0000" Y="494.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.8667" />
            <PreSize X="0.3500" Y="0.1193" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="keybord" ActionTag="-860731503" VisibleForFrame="False" Tag="547" IconVisible="True" LeftMargin="900.0000" RightMargin="-2.0000" TopMargin="107.0000" BottomMargin="65.0000" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="302.0000" Y="398.0000" />
            <AnchorPoint />
            <Position X="900.0000" Y="65.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.7500" Y="0.1140" />
            <PreSize X="0.2517" Y="0.6982" />
            <FileData Type="Normal" Path="hall/csb/bank/keybord.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Text_43" ActionTag="-63920162" Tag="241" IconVisible="False" LeftMargin="374.4800" RightMargin="350.5200" TopMargin="511.6596" BottomMargin="29.3404" FontSize="25" LabelText="溫馨提示：首次使用須綁定郵箱并設置密碼" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="475.0000" Y="29.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="611.9800" Y="43.8404" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="126" G="89" B="192" />
            <PrePosition X="0.5100" Y="0.0769" />
            <PreSize X="0.3958" Y="0.0509" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>