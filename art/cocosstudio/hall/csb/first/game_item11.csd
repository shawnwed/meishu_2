<GameFile>
  <PropertyGroup Name="game_item11" Type="Layer" ID="a736e9ae-42e1-45ef-9992-82bfe182bdd4" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="280" Speed="1.0000">
        <Timeline ActionTag="-197333913" Property="Position">
          <PointFrame FrameIndex="0" X="119.0000" Y="119.0000">
            <EasingData Type="0" />
          </PointFrame>
          <PointFrame FrameIndex="280" X="119.0000" Y="119.0000">
            <EasingData Type="0" />
          </PointFrame>
        </Timeline>
        <Timeline ActionTag="-197333913" Property="Scale">
          <ScaleFrame FrameIndex="0" X="1.0000" Y="1.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="280" X="1.0000" Y="1.0000">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="-197333913" Property="RotationSkew">
          <ScaleFrame FrameIndex="0" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="280" X="360.0000" Y="360.0000">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="1240566545" Property="Position">
          <PointFrame FrameIndex="0" X="213.0000" Y="111.0000">
            <EasingData Type="0" />
          </PointFrame>
          <PointFrame FrameIndex="120" X="213.0000" Y="121.0000">
            <EasingData Type="0" />
          </PointFrame>
          <PointFrame FrameIndex="280" X="213.0000" Y="111.0000">
            <EasingData Type="0" />
          </PointFrame>
        </Timeline>
        <Timeline ActionTag="1240566545" Property="Scale">
          <ScaleFrame FrameIndex="0" X="1.0000" Y="1.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="120" X="1.0000" Y="1.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="280" X="1.0000" Y="1.0000">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="1240566545" Property="RotationSkew">
          <ScaleFrame FrameIndex="0" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="120" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="280" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="1945228764" Property="Position">
          <PointFrame FrameIndex="0" X="46.0000" Y="98.0000">
            <EasingData Type="0" />
          </PointFrame>
          <PointFrame FrameIndex="120" X="46.0000" Y="88.0000">
            <EasingData Type="0" />
          </PointFrame>
          <PointFrame FrameIndex="280" X="46.0000" Y="98.0000">
            <EasingData Type="0" />
          </PointFrame>
        </Timeline>
        <Timeline ActionTag="1945228764" Property="Scale">
          <ScaleFrame FrameIndex="0" X="1.0000" Y="1.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="120" X="1.0000" Y="1.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="280" X="1.0000" Y="1.0000">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="1945228764" Property="RotationSkew">
          <ScaleFrame FrameIndex="0" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="120" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="280" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="-1369177883" Property="Position">
          <PointFrame FrameIndex="0" X="82.0000" Y="216.0000">
            <EasingData Type="0" />
          </PointFrame>
          <PointFrame FrameIndex="60" X="82.0000" Y="216.0000">
            <EasingData Type="0" />
          </PointFrame>
          <PointFrame FrameIndex="120" X="82.0000" Y="216.0000">
            <EasingData Type="0" />
          </PointFrame>
        </Timeline>
        <Timeline ActionTag="-1369177883" Property="Scale">
          <ScaleFrame FrameIndex="0" X="0.0010" Y="0.0010">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="60" X="1.0000" Y="1.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="120" X="0.0010" Y="0.0010">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="-1369177883" Property="RotationSkew">
          <ScaleFrame FrameIndex="0" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="60" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="120" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="516380202" Property="Position">
          <PointFrame FrameIndex="0" X="154.0000" Y="171.0000">
            <EasingData Type="0" />
          </PointFrame>
          <PointFrame FrameIndex="139" X="154.0000" Y="171.0000">
            <EasingData Type="0" />
          </PointFrame>
          <PointFrame FrameIndex="140" X="154.0000" Y="171.0000">
            <EasingData Type="0" />
          </PointFrame>
          <PointFrame FrameIndex="200" X="154.0000" Y="171.0000">
            <EasingData Type="0" />
          </PointFrame>
        </Timeline>
        <Timeline ActionTag="516380202" Property="Scale">
          <ScaleFrame FrameIndex="0" X="0.0010" Y="0.0010">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="139" X="0.0010" Y="0.0010">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="140" X="1.0000" Y="1.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="200" X="0.0010" Y="0.0010">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="516380202" Property="RotationSkew">
          <ScaleFrame FrameIndex="0" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="139" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="140" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="200" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="698262283" Property="Position">
          <PointFrame FrameIndex="0" X="162.0000" Y="102.0000">
            <EasingData Type="0" />
          </PointFrame>
          <PointFrame FrameIndex="199" X="162.0000" Y="102.0000">
            <EasingData Type="0" />
          </PointFrame>
          <PointFrame FrameIndex="200" X="162.0000" Y="102.0000">
            <EasingData Type="0" />
          </PointFrame>
          <PointFrame FrameIndex="280" X="162.0000" Y="102.0000">
            <EasingData Type="0" />
          </PointFrame>
        </Timeline>
        <Timeline ActionTag="698262283" Property="Scale">
          <ScaleFrame FrameIndex="0" X="0.0010" Y="0.0010">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="199" X="0.0010" Y="0.0010">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="200" X="1.0000" Y="1.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="280" X="0.0010" Y="0.0010">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="698262283" Property="RotationSkew">
          <ScaleFrame FrameIndex="0" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="199" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="200" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="280" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
      </Animation>
      <ObjectData Name="Layer" Tag="95" ctype="GameLayerObjectData">
        <Size X="238.0000" Y="238.0000" />
        <Children>
          <AbstractNodeData Name="bg" ActionTag="1304699152" Tag="96" IconVisible="False" ctype="SpriteObjectData">
            <Size X="238.0000" Y="238.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="119.0000" Y="119.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="1.0000" Y="1.0000" />
            <FileData Type="Normal" Path="hall/res/gameitem/cdd1.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="line" ActionTag="-197333913" Tag="101" IconVisible="False" LeftMargin="6.0000" RightMargin="6.0000" TopMargin="15.5000" BottomMargin="15.5000" ctype="SpriteObjectData">
            <Size X="226.0000" Y="207.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="119.0000" Y="119.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="0.9496" Y="0.8697" />
            <FileData Type="Normal" Path="hall/res/gameitem/ddz2.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="word2" ActionTag="1277372120" Tag="97" IconVisible="False" LeftMargin="28.5000" RightMargin="28.5000" TopMargin="1.0000" BottomMargin="67.0000" ctype="SpriteObjectData">
            <Size X="181.0000" Y="170.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="119.0000" Y="152.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.6387" />
            <PreSize X="0.7605" Y="0.7143" />
            <FileData Type="Normal" Path="hall/res/gameitem/cdd2.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="card1" ActionTag="1240566545" Tag="98" IconVisible="False" LeftMargin="187.0000" RightMargin="-1.0000" TopMargin="104.0000" BottomMargin="88.0000" ctype="SpriteObjectData">
            <Size X="52.0000" Y="46.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="213.0000" Y="111.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8950" Y="0.4664" />
            <PreSize X="0.2185" Y="0.1933" />
            <FileData Type="MarkedSubImage" Path="hall/res/gameitem/cdd3.png" Plist="hall/res/gameitem.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="card2" ActionTag="1945228764" Tag="99" IconVisible="False" LeftMargin="14.0000" RightMargin="160.0000" TopMargin="114.0000" BottomMargin="72.0000" ctype="SpriteObjectData">
            <Size X="64.0000" Y="52.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="46.0000" Y="98.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.1933" Y="0.4118" />
            <PreSize X="0.2689" Y="0.2185" />
            <FileData Type="MarkedSubImage" Path="hall/res/gameitem/cdd4.png" Plist="hall/res/gameitem.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="star1" ActionTag="-1369177883" Tag="100" IconVisible="False" LeftMargin="38.5000" RightMargin="112.5000" TopMargin="-21.5000" BottomMargin="172.5000" ctype="SpriteObjectData">
            <Size X="87.0000" Y="87.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="82.0000" Y="216.0000" />
            <Scale ScaleX="0.0010" ScaleY="0.0010" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3445" Y="0.9076" />
            <PreSize X="0.3655" Y="0.3655" />
            <FileData Type="MarkedSubImage" Path="hall/res/gameitem/star1.png" Plist="hall/res/gameitem.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="word" ActionTag="2055856096" Tag="102" IconVisible="False" LeftMargin="98.0000" RightMargin="94.0000" TopMargin="169.0000" BottomMargin="23.0000" ctype="SpriteObjectData">
            <Size X="166.0000" Y="66.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="121.0000" Y="46.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5084" Y="0.1933" />
            <PreSize X="0.1933" Y="0.1933" />
            <FileData Type="Default" Path="Default/Sprite.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="star1_0" ActionTag="516380202" Tag="103" IconVisible="False" LeftMargin="110.5000" RightMargin="40.5000" TopMargin="23.5000" BottomMargin="127.5000" ctype="SpriteObjectData">
            <Size X="87.0000" Y="87.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="154.0000" Y="171.0000" />
            <Scale ScaleX="0.0010" ScaleY="0.0010" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.6471" Y="0.7185" />
            <PreSize X="0.3655" Y="0.3655" />
            <FileData Type="MarkedSubImage" Path="hall/res/gameitem/star1.png" Plist="hall/res/gameitem.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="star1_0_0" ActionTag="698262283" Tag="104" IconVisible="False" LeftMargin="118.5000" RightMargin="32.5000" TopMargin="92.5000" BottomMargin="58.5000" ctype="SpriteObjectData">
            <Size X="87.0000" Y="87.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="162.0000" Y="102.0000" />
            <Scale ScaleX="0.0010" ScaleY="0.0010" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.6807" Y="0.4286" />
            <PreSize X="0.3655" Y="0.3655" />
            <FileData Type="MarkedSubImage" Path="hall/res/gameitem/star1.png" Plist="hall/res/gameitem.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>