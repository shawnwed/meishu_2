<GameFile>
  <PropertyGroup Name="game_item1" Type="Layer" ID="0ca06214-9e6d-4b87-a52f-367a101f1789" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="242" Speed="1.0000">
        <Timeline ActionTag="-666966918" Property="Position">
          <PointFrame FrameIndex="0" X="120.0161" Y="134.6953">
            <EasingData Type="0" />
          </PointFrame>
          <PointFrame FrameIndex="125" X="114.9537" Y="134.6961">
            <EasingData Type="0" />
          </PointFrame>
          <PointFrame FrameIndex="242" X="120.0161" Y="134.6953">
            <EasingData Type="0" />
          </PointFrame>
        </Timeline>
        <Timeline ActionTag="-666966918" Property="Scale">
          <ScaleFrame FrameIndex="0" X="1.0000" Y="1.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="125" X="1.0000" Y="1.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="242" X="1.0000" Y="1.0000">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="-666966918" Property="RotationSkew">
          <ScaleFrame FrameIndex="0" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="125" X="-9.0061" Y="-9.0061">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="242" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="-409938554" Property="Position">
          <PointFrame FrameIndex="0" X="164.4120" Y="139.2367">
            <EasingData Type="0" />
          </PointFrame>
          <PointFrame FrameIndex="125" X="164.4127" Y="132.1585">
            <EasingData Type="0" />
          </PointFrame>
          <PointFrame FrameIndex="242" X="164.4120" Y="139.2367">
            <EasingData Type="0" />
          </PointFrame>
        </Timeline>
        <Timeline ActionTag="-409938554" Property="Scale">
          <ScaleFrame FrameIndex="0" X="1.0000" Y="1.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="125" X="1.0000" Y="1.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="242" X="1.0000" Y="1.0000">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="-409938554" Property="RotationSkew">
          <ScaleFrame FrameIndex="0" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="125" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="242" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="-1401441681" Property="Position">
          <PointFrame FrameIndex="0" X="254.6279" Y="158.5532">
            <EasingData Type="0" />
          </PointFrame>
          <PointFrame FrameIndex="125" X="254.6284" Y="160.5794">
            <EasingData Type="0" />
          </PointFrame>
          <PointFrame FrameIndex="242" X="254.6279" Y="158.5532">
            <EasingData Type="0" />
          </PointFrame>
        </Timeline>
        <Timeline ActionTag="-1401441681" Property="Scale">
          <ScaleFrame FrameIndex="0" X="1.0000" Y="1.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="125" X="1.0000" Y="1.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="242" X="1.0000" Y="1.0000">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="-1401441681" Property="RotationSkew">
          <ScaleFrame FrameIndex="0" X="5.0001" Y="5.0001">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="125" X="5.0001" Y="5.0001">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="242" X="5.0001" Y="5.0001">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
      </Animation>
      <ObjectData Name="Layer" Tag="84" ctype="GameLayerObjectData">
        <Size X="292.0000" Y="230.0000" />
        <Children>
          <AbstractNodeData Name="bg" ActionTag="56100765" Tag="90" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="29.3768" RightMargin="24.6232" TopMargin="93.6402" BottomMargin="-0.6402" ctype="SpriteObjectData">
            <Size X="238.0000" Y="137.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="148.3768" Y="67.8598" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5081" Y="0.2950" />
            <PreSize X="0.8151" Y="0.5957" />
            <FileData Type="MarkedSubImage" Path="hall/res/gameitem/ddz1.png" Plist="hall/res/gameitem.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="line" ActionTag="-480025537" VisibleForFrame="False" Tag="91" IconVisible="False" LeftMargin="6.0000" RightMargin="60.0000" TopMargin="7.5000" BottomMargin="15.5000" ctype="SpriteObjectData">
            <Size X="226.0000" Y="207.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="119.0000" Y="119.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4075" Y="0.5174" />
            <PreSize X="0.7740" Y="0.9000" />
            <FileData Type="Normal" Path="hall/res/gameitem/ddz2.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="cards" ActionTag="-666966918" Tag="93" RotationSkewX="-5.9801" RotationSkewY="-5.9801" IconVisible="False" LeftMargin="43.6547" RightMargin="102.3453" TopMargin="17.8042" BottomMargin="57.1958" ctype="SpriteObjectData">
            <Size X="146.0000" Y="155.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="116.6547" Y="134.6958" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3995" Y="0.5856" />
            <PreSize X="0.5000" Y="0.6739" />
            <FileData Type="MarkedSubImage" Path="hall/res/gameitem/ddz4.png" Plist="hall/res/gameitem.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="boom" ActionTag="-31443371" VisibleForFrame="False" Tag="92" IconVisible="False" LeftMargin="154.3102" RightMargin="48.6898" TopMargin="30.3576" BottomMargin="87.6424" ctype="SpriteObjectData">
            <Size X="89.0000" Y="112.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="198.8102" Y="143.6424" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.6809" Y="0.6245" />
            <PreSize X="0.3048" Y="0.4870" />
            <FileData Type="MarkedSubImage" Path="hall/res/gameitem/ddz5.png" Plist="hall/res/gameitem.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="ren" ActionTag="-409938554" Tag="3723" IconVisible="False" LeftMargin="82.9125" RightMargin="46.0875" TopMargin="-0.0367" BottomMargin="39.0367" ctype="SpriteObjectData">
            <Size X="163.0000" Y="191.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="164.4125" Y="134.5367" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5631" Y="0.5849" />
            <PreSize X="0.5582" Y="0.8304" />
            <FileData Type="MarkedSubImage" Path="hall/res/first/ddz6.png" Plist="hall/res/first.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="san" ActionTag="-1401441681" Tag="3724" RotationSkewX="5.0001" RotationSkewY="5.0001" IconVisible="False" LeftMargin="172.6282" RightMargin="37.3718" TopMargin="70.1014" BottomMargin="55.8986" ctype="SpriteObjectData">
            <Size X="82.0000" Y="104.0000" />
            <AnchorPoint ScaleX="1.0000" ScaleY="1.0000" />
            <Position X="254.6282" Y="159.8986" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8720" Y="0.6952" />
            <PreSize X="0.2808" Y="0.4522" />
            <FileData Type="MarkedSubImage" Path="hall/res/first/ddz7.png" Plist="hall/res/first.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="word" ActionTag="-1769036724" Tag="94" IconVisible="False" LeftMargin="67.1133" RightMargin="67.8867" TopMargin="156.5580" BottomMargin="11.4420" ctype="SpriteObjectData">
            <Size X="157.0000" Y="62.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="145.6133" Y="42.4420" />
            <Scale ScaleX="1.0500" ScaleY="1.0500" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4987" Y="0.1845" />
            <PreSize X="0.5377" Y="0.2696" />
            <FileData Type="MarkedSubImage" Path="lan/cn/game_item1.png" Plist="lan/cn/game.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>