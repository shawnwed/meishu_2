<GameFile>
  <PropertyGroup Name="game_item3" Type="Layer" ID="229b30a1-249c-4484-81a4-e104b924a45b" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="257" Speed="1.0000">
        <Timeline ActionTag="56100765" Property="Position">
          <PointFrame FrameIndex="0" X="145.5766" Y="146.3585">
            <EasingData Type="0" />
          </PointFrame>
          <PointFrame FrameIndex="128" X="145.5767" Y="144.3585">
            <EasingData Type="0" />
          </PointFrame>
          <PointFrame FrameIndex="257" X="145.5766" Y="146.3585">
            <EasingData Type="0" />
          </PointFrame>
        </Timeline>
        <Timeline ActionTag="56100765" Property="Scale">
          <ScaleFrame FrameIndex="0" X="1.0000" Y="1.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="128" X="1.0000" Y="1.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="257" X="1.0000" Y="1.0000">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="56100765" Property="RotationSkew">
          <ScaleFrame FrameIndex="0" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="128" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="257" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="-31443371" Property="Position">
          <PointFrame FrameIndex="0" X="203.5972" Y="81.0577">
            <EasingData Type="0" />
          </PointFrame>
          <PointFrame FrameIndex="128" X="203.5972" Y="83.0577">
            <EasingData Type="0" />
          </PointFrame>
          <PointFrame FrameIndex="257" X="203.5972" Y="81.0577">
            <EasingData Type="0" />
          </PointFrame>
        </Timeline>
        <Timeline ActionTag="-31443371" Property="Scale">
          <ScaleFrame FrameIndex="0" X="1.0000" Y="1.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="128" X="1.0000" Y="1.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="257" X="1.0000" Y="1.0000">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="-31443371" Property="RotationSkew">
          <ScaleFrame FrameIndex="0" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="128" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="257" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
      </Animation>
      <ObjectData Name="Layer" Tag="84" ctype="GameLayerObjectData">
        <Size X="292.0000" Y="230.0000" />
        <Children>
          <AbstractNodeData Name="line" ActionTag="-480025537" VisibleForFrame="False" Tag="91" RotationSkewX="360.0000" RotationSkewY="360.0000" IconVisible="False" LeftMargin="6.0000" RightMargin="60.0000" TopMargin="7.5000" BottomMargin="15.5000" ctype="SpriteObjectData">
            <Size X="226.0000" Y="207.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="119.0000" Y="119.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4075" Y="0.5174" />
            <PreSize X="0.7740" Y="0.9000" />
            <FileData Type="Normal" Path="hall/res/gameitem/ddz2.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="bg_0" ActionTag="506290598" Tag="3725" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="27.0000" RightMargin="27.0000" TopMargin="100.6320" BottomMargin="-7.6320" ctype="SpriteObjectData">
            <Size X="238.0000" Y="137.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="146.0000" Y="60.8680" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.2646" />
            <PreSize X="0.8151" Y="0.5957" />
            <FileData Type="MarkedSubImage" Path="hall/res/gameitem/ddz1.png" Plist="hall/res/gameitem.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="bg" ActionTag="56100765" Tag="90" IconVisible="False" LeftMargin="43.5767" RightMargin="44.4233" TopMargin="-8.4670" BottomMargin="50.4670" ctype="SpriteObjectData">
            <Size X="204.0000" Y="188.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="145.5767" Y="144.4670" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4986" Y="0.6281" />
            <PreSize X="0.6986" Y="0.8174" />
            <FileData Type="MarkedSubImage" Path="hall/res/gameitem/bn1.png" Plist="hall/res/gameitem.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="boom" ActionTag="-31443371" Tag="92" IconVisible="False" LeftMargin="155.5972" RightMargin="40.4028" TopMargin="119.0508" BottomMargin="54.9492" ctype="SpriteObjectData">
            <Size X="96.0000" Y="56.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="203.5972" Y="82.9492" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.6973" Y="0.3606" />
            <PreSize X="0.3288" Y="0.2435" />
            <FileData Type="MarkedSubImage" Path="hall/res/gameitem/bn2.png" Plist="hall/res/gameitem.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="cards" ActionTag="-666966918" VisibleForFrame="False" Tag="93" IconVisible="False" LeftMargin="42.0000" RightMargin="104.0000" TopMargin="55.5000" BottomMargin="19.5000" ctype="SpriteObjectData">
            <Size X="146.0000" Y="155.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="115.0000" Y="97.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3938" Y="0.4217" />
            <PreSize X="0.5000" Y="0.6739" />
            <FileData Type="MarkedSubImage" Path="hall/res/gameitem/ddz4.png" Plist="hall/res/gameitem.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="word" ActionTag="-1769036724" Tag="94" IconVisible="False" LeftMargin="62.6841" RightMargin="72.3159" TopMargin="154.3142" BottomMargin="13.6858" ctype="SpriteObjectData">
            <Size X="157.0000" Y="62.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="141.1841" Y="44.6858" />
            <Scale ScaleX="1.0500" ScaleY="1.0500" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4835" Y="0.1943" />
            <PreSize X="0.5377" Y="0.2696" />
            <FileData Type="MarkedSubImage" Path="lan/cn/game_item3.png" Plist="lan/cn/game.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>