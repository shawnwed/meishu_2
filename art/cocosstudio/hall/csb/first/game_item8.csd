<GameFile>
  <PropertyGroup Name="game_item8" Type="Layer" ID="6c9578b4-4652-4db0-9d46-7090d0ed398d" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="250" Speed="1.0000">
        <Timeline ActionTag="-197333913" Property="Position">
          <PointFrame FrameIndex="0" X="119.0000" Y="119.0000">
            <EasingData Type="0" />
          </PointFrame>
          <PointFrame FrameIndex="250" X="119.0000" Y="119.0000">
            <EasingData Type="0" />
          </PointFrame>
        </Timeline>
        <Timeline ActionTag="-197333913" Property="Scale">
          <ScaleFrame FrameIndex="0" X="1.0000" Y="1.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="250" X="1.0000" Y="1.0000">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="-197333913" Property="RotationSkew">
          <ScaleFrame FrameIndex="0" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="250" X="360.0000" Y="360.0000">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="292021285" Property="Position">
          <PointFrame FrameIndex="0" X="70.0000" Y="49.5000">
            <EasingData Type="0" />
          </PointFrame>
          <PointFrame FrameIndex="125" X="70.0000" Y="49.5000">
            <EasingData Type="0" />
          </PointFrame>
          <PointFrame FrameIndex="250" X="70.0000" Y="49.5000">
            <EasingData Type="0" />
          </PointFrame>
        </Timeline>
        <Timeline ActionTag="292021285" Property="Scale">
          <ScaleFrame FrameIndex="0" X="1.0000" Y="1.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="125" X="1.0000" Y="1.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="250" X="1.0000" Y="1.0000">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="292021285" Property="RotationSkew">
          <ScaleFrame FrameIndex="0" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="125" X="-7.0000" Y="-7.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="250" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="955998957" Property="Position">
          <PointFrame FrameIndex="0" X="142.0000" Y="40.5000">
            <EasingData Type="0" />
          </PointFrame>
          <PointFrame FrameIndex="125" X="142.0000" Y="40.5000">
            <EasingData Type="0" />
          </PointFrame>
          <PointFrame FrameIndex="250" X="142.0000" Y="40.5000">
            <EasingData Type="0" />
          </PointFrame>
        </Timeline>
        <Timeline ActionTag="955998957" Property="Scale">
          <ScaleFrame FrameIndex="0" X="1.0000" Y="1.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="125" X="1.0000" Y="1.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="250" X="1.0000" Y="1.0000">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="955998957" Property="RotationSkew">
          <ScaleFrame FrameIndex="0" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="125" X="5.0000" Y="5.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="250" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
      </Animation>
      <ObjectData Name="Layer" Tag="95" ctype="GameLayerObjectData">
        <Size X="238.0000" Y="238.0000" />
        <Children>
          <AbstractNodeData Name="bg" ActionTag="1304699152" Tag="96" IconVisible="False" ctype="SpriteObjectData">
            <Size X="238.0000" Y="238.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="119.0000" Y="119.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="1.0000" Y="1.0000" />
            <FileData Type="Normal" Path="hall/res/gameitem/sg02.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="line" ActionTag="-197333913" Tag="101" IconVisible="False" LeftMargin="6.0000" RightMargin="6.0000" TopMargin="15.5000" BottomMargin="15.5000" ctype="SpriteObjectData">
            <Size X="226.0000" Y="207.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="119.0000" Y="119.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="0.9496" Y="0.8697" />
            <FileData Type="Normal" Path="hall/res/gameitem/ddz2.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="card" ActionTag="292021285" Tag="200" IconVisible="False" LeftMargin="-1.5000" RightMargin="96.5000" TopMargin="27.5000" BottomMargin="49.5000" ctype="SpriteObjectData">
            <Size X="143.0000" Y="161.0000" />
            <AnchorPoint ScaleX="0.5000" />
            <Position X="70.0000" Y="49.5000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.2941" Y="0.2080" />
            <PreSize X="0.6008" Y="0.6765" />
            <FileData Type="Normal" Path="hall/res/gameitem/sg05.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="pep" ActionTag="955998957" Tag="199" IconVisible="False" LeftMargin="46.0000" TopMargin="8.5000" BottomMargin="40.5000" ctype="SpriteObjectData">
            <Size X="192.0000" Y="189.0000" />
            <AnchorPoint ScaleX="0.5000" />
            <Position X="142.0000" Y="40.5000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5966" Y="0.1702" />
            <PreSize X="0.8067" Y="0.7941" />
            <FileData Type="Normal" Path="hall/res/gameitem/sg04.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="word" ActionTag="2055856096" Tag="102" IconVisible="False" LeftMargin="98.0000" RightMargin="94.0000" TopMargin="169.0000" BottomMargin="23.0000" ctype="SpriteObjectData">
            <Size X="131.0000" Y="64.0000" />
            <AnchorPoint ScaleX="0.4915" ScaleY="0.6471" />
            <Position X="120.6090" Y="52.7664" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5068" Y="0.2217" />
            <PreSize X="0.1933" Y="0.1933" />
            <FileData Type="Default" Path="Default/Sprite.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>