<GameFile>
  <PropertyGroup Name="game_item6" Type="Layer" ID="d540bbd3-1e6c-4833-8815-96a88968b497" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="240" Speed="1.0000">
        <Timeline ActionTag="-197333913" Property="Position">
          <PointFrame FrameIndex="0" X="119.0000" Y="119.0000">
            <EasingData Type="0" />
          </PointFrame>
          <PointFrame FrameIndex="240" X="119.0000" Y="119.0000">
            <EasingData Type="0" />
          </PointFrame>
        </Timeline>
        <Timeline ActionTag="-197333913" Property="Scale">
          <ScaleFrame FrameIndex="0" X="1.0000" Y="1.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="240" X="1.0000" Y="1.0000">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="-197333913" Property="RotationSkew">
          <ScaleFrame FrameIndex="0" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="240" X="360.0000" Y="360.0000">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="-738696294" Property="Position">
          <PointFrame FrameIndex="0" X="18.0000" Y="58.0000">
            <EasingData Type="0" />
          </PointFrame>
          <PointFrame FrameIndex="120" X="8.0000" Y="58.0000">
            <EasingData Type="0" />
          </PointFrame>
          <PointFrame FrameIndex="240" X="18.0000" Y="58.0000">
            <EasingData Type="0" />
          </PointFrame>
        </Timeline>
        <Timeline ActionTag="-738696294" Property="Scale">
          <ScaleFrame FrameIndex="0" X="1.0000" Y="1.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="120" X="1.0000" Y="1.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="240" X="1.0000" Y="1.0000">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="-738696294" Property="RotationSkew">
          <ScaleFrame FrameIndex="0" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="120" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="240" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="738633843" Property="Position">
          <PointFrame FrameIndex="0" X="120.0000" Y="58.0001">
            <EasingData Type="0" />
          </PointFrame>
          <PointFrame FrameIndex="120" X="130.0000" Y="58.0000">
            <EasingData Type="0" />
          </PointFrame>
          <PointFrame FrameIndex="240" X="120.0000" Y="58.0000">
            <EasingData Type="0" />
          </PointFrame>
        </Timeline>
        <Timeline ActionTag="738633843" Property="Scale">
          <ScaleFrame FrameIndex="0" X="1.0000" Y="1.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="120" X="1.0000" Y="1.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="240" X="1.0000" Y="1.0000">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="738633843" Property="RotationSkew">
          <ScaleFrame FrameIndex="0" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="120" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="240" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="944235900" Property="Position">
          <PointFrame FrameIndex="0" X="90.0000" Y="235.0000">
            <EasingData Type="0" />
          </PointFrame>
          <PointFrame FrameIndex="60" X="90.0000" Y="235.0000">
            <EasingData Type="0" />
          </PointFrame>
          <PointFrame FrameIndex="120" X="90.0000" Y="235.0000">
            <EasingData Type="0" />
          </PointFrame>
        </Timeline>
        <Timeline ActionTag="944235900" Property="Scale">
          <ScaleFrame FrameIndex="0" X="0.0010" Y="0.0010">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="60" X="1.0000" Y="1.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="120" X="0.0010" Y="0.0010">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="944235900" Property="RotationSkew">
          <ScaleFrame FrameIndex="0" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="60" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="120" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="1397433797" Property="Position">
          <PointFrame FrameIndex="0" X="150.0000" Y="235.0000">
            <EasingData Type="0" />
          </PointFrame>
          <PointFrame FrameIndex="119" X="150.0000" Y="235.0000">
            <EasingData Type="0" />
          </PointFrame>
          <PointFrame FrameIndex="120" X="150.0000" Y="235.0000">
            <EasingData Type="0" />
          </PointFrame>
          <PointFrame FrameIndex="240" X="150.0000" Y="235.0000">
            <EasingData Type="0" />
          </PointFrame>
        </Timeline>
        <Timeline ActionTag="1397433797" Property="Scale">
          <ScaleFrame FrameIndex="0" X="0.0010" Y="0.0010">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="119" X="0.0010" Y="0.0010">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="120" X="1.0000" Y="1.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="240" X="0.0010" Y="0.0010">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="1397433797" Property="RotationSkew">
          <ScaleFrame FrameIndex="0" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="119" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="120" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="240" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="-981768495" Property="Position">
          <PointFrame FrameIndex="0" X="105.0000" Y="180.0000">
            <EasingData Type="0" />
          </PointFrame>
          <PointFrame FrameIndex="199" X="105.0000" Y="180.0000">
            <EasingData Type="0" />
          </PointFrame>
          <PointFrame FrameIndex="200" X="105.0000" Y="180.0000">
            <EasingData Type="0" />
          </PointFrame>
          <PointFrame FrameIndex="240" X="105.0000" Y="180.0000">
            <EasingData Type="0" />
          </PointFrame>
        </Timeline>
        <Timeline ActionTag="-981768495" Property="Scale">
          <ScaleFrame FrameIndex="0" X="0.0010" Y="0.0010">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="199" X="0.0010" Y="0.0010">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="200" X="1.0000" Y="1.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="240" X="0.0010" Y="0.0010">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="-981768495" Property="RotationSkew">
          <ScaleFrame FrameIndex="0" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="199" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="200" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="240" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
      </Animation>
      <ObjectData Name="Layer" Tag="95" ctype="GameLayerObjectData">
        <Size X="238.0000" Y="238.0000" />
        <Children>
          <AbstractNodeData Name="bg" ActionTag="1304699152" Tag="96" IconVisible="False" ctype="SpriteObjectData">
            <Size X="238.0000" Y="238.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="119.0000" Y="119.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="1.0000" Y="1.0000" />
            <FileData Type="Normal" Path="hall/res/gameitem/nn01.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="line" ActionTag="-197333913" Tag="101" RotationSkewX="69.0000" RotationSkewY="69.0000" IconVisible="False" LeftMargin="6.0000" RightMargin="6.0000" TopMargin="15.5000" BottomMargin="15.5000" ctype="SpriteObjectData">
            <Size X="226.0000" Y="207.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="119.0000" Y="119.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="0.9496" Y="0.8697" />
            <FileData Type="Normal" Path="hall/res/gameitem/ddz2.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="cup" ActionTag="-2060233736" Tag="253" IconVisible="False" LeftMargin="28.0000" RightMargin="49.0000" TopMargin="-17.0000" BottomMargin="48.0000" ctype="SpriteObjectData">
            <Size X="161.0000" Y="207.0000" />
            <AnchorPoint />
            <Position X="28.0000" Y="48.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.1176" Y="0.2017" />
            <PreSize X="0.6765" Y="0.8697" />
            <FileData Type="Normal" Path="hall/res/gameitem/nn07.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="beer1" ActionTag="-738696294" Alpha="229" Tag="252" IconVisible="False" LeftMargin="14.1667" RightMargin="121.8333" TopMargin="70.0000" BottomMargin="58.0000" ctype="SpriteObjectData">
            <Size X="102.0000" Y="110.0000" />
            <AnchorPoint />
            <Position X="14.1667" Y="58.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0595" Y="0.2437" />
            <PreSize X="0.4286" Y="0.4622" />
            <FileData Type="Normal" Path="hall/res/gameitem/nn06.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="beer2" ActionTag="738633843" Tag="254" IconVisible="False" LeftMargin="123.8333" RightMargin="12.1667" TopMargin="69.9999" BottomMargin="58.0001" ctype="SpriteObjectData">
            <Size X="102.0000" Y="110.0000" />
            <AnchorPoint />
            <Position X="123.8333" Y="58.0001" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5203" Y="0.2437" />
            <PreSize X="0.4286" Y="0.4622" />
            <FileData Type="MarkedSubImage" Path="hall/res/gameitem/nn05.png" Plist="hall/res/gameitem.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="word" ActionTag="2055856096" Tag="102" IconVisible="False" LeftMargin="13.0000" RightMargin="9.0000" TopMargin="159.0000" BottomMargin="13.0000" ctype="SpriteObjectData">
            <Size X="216.0000" Y="66.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="121.0000" Y="46.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5084" Y="0.1933" />
            <PreSize X="0.9076" Y="0.2773" />
            <FileData Type="Default" Path="Default/Sprite.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="star1" ActionTag="944235900" Tag="52" IconVisible="False" LeftMargin="69.5000" RightMargin="127.5000" TopMargin="-17.5000" BottomMargin="214.5000" ctype="SpriteObjectData">
            <Size X="41.0000" Y="41.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="90.0000" Y="235.0000" />
            <Scale ScaleX="0.7669" ScaleY="0.7669" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3782" Y="0.9874" />
            <PreSize X="0.1723" Y="0.1723" />
            <FileData Type="MarkedSubImage" Path="hall/res/gameitem/nn04.png" Plist="hall/res/gameitem.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="star2" ActionTag="1397433797" Tag="53" IconVisible="False" LeftMargin="129.5000" RightMargin="67.5000" TopMargin="-17.5000" BottomMargin="214.5000" ctype="SpriteObjectData">
            <Size X="41.0000" Y="41.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="150.0000" Y="235.0000" />
            <Scale ScaleX="0.0010" ScaleY="0.0010" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.6303" Y="0.9874" />
            <PreSize X="0.1723" Y="0.1723" />
            <FileData Type="MarkedSubImage" Path="hall/res/gameitem/nn04.png" Plist="hall/res/gameitem.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="star3" ActionTag="-981768495" Tag="54" IconVisible="False" LeftMargin="84.5000" RightMargin="112.5000" TopMargin="37.5000" BottomMargin="159.5000" ctype="SpriteObjectData">
            <Size X="41.0000" Y="41.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="105.0000" Y="180.0000" />
            <Scale ScaleX="0.0010" ScaleY="0.0010" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4412" Y="0.7563" />
            <PreSize X="0.1723" Y="0.1723" />
            <FileData Type="MarkedSubImage" Path="hall/res/gameitem/nn04.png" Plist="hall/res/gameitem.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>