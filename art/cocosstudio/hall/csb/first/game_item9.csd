<GameFile>
  <PropertyGroup Name="game_item9" Type="Layer" ID="44c0fb41-7033-4016-9b7c-8fa0a1144f24" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="245" Speed="1.0000">
        <Timeline ActionTag="1887239600" Property="Position">
          <PointFrame FrameIndex="0" X="150.2527" Y="141.9305">
            <EasingData Type="0" />
          </PointFrame>
          <PointFrame FrameIndex="125" X="150.2529" Y="139.9044">
            <EasingData Type="0" />
          </PointFrame>
          <PointFrame FrameIndex="245" X="150.2527" Y="141.9305">
            <EasingData Type="0" />
          </PointFrame>
        </Timeline>
        <Timeline ActionTag="1887239600" Property="Scale">
          <ScaleFrame FrameIndex="0" X="1.0500" Y="1.0500">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="125" X="1.0500" Y="1.0500">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="245" X="1.0500" Y="1.0500">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="1887239600" Property="RotationSkew">
          <ScaleFrame FrameIndex="0" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="125" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="245" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="56100765" Property="Position">
          <PointFrame FrameIndex="0" X="194.7348" Y="89.0100">
            <EasingData Type="0" />
          </PointFrame>
          <PointFrame FrameIndex="125" X="194.7348" Y="91.0362">
            <EasingData Type="0" />
          </PointFrame>
          <PointFrame FrameIndex="245" X="194.7348" Y="89.0100">
            <EasingData Type="0" />
          </PointFrame>
        </Timeline>
        <Timeline ActionTag="56100765" Property="Scale">
          <ScaleFrame FrameIndex="0" X="1.0000" Y="1.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="125" X="1.0000" Y="1.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="245" X="1.0000" Y="1.0000">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="56100765" Property="RotationSkew">
          <ScaleFrame FrameIndex="0" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="125" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="245" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
      </Animation>
      <ObjectData Name="Layer" Tag="84" ctype="GameLayerObjectData">
        <Size X="292.0000" Y="230.0000" />
        <Children>
          <AbstractNodeData Name="bg_0" ActionTag="-1447724181" Tag="3726" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="28.6352" RightMargin="25.3648" TopMargin="100.9180" BottomMargin="-7.9180" ctype="SpriteObjectData">
            <Size X="238.0000" Y="137.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="147.6352" Y="60.5820" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5056" Y="0.2634" />
            <PreSize X="0.8151" Y="0.5957" />
            <FileData Type="MarkedSubImage" Path="hall/res/gameitem/ddz1.png" Plist="hall/res/gameitem.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="pai" ActionTag="1887239600" Tag="1524" IconVisible="False" LeftMargin="68.7529" RightMargin="60.2471" TopMargin="0.0722" BottomMargin="50.9278" ctype="SpriteObjectData">
            <Size X="163.0000" Y="179.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="150.2529" Y="140.4278" />
            <Scale ScaleX="1.0500" ScaleY="1.0500" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5146" Y="0.6106" />
            <PreSize X="0.5582" Y="0.7783" />
            <FileData Type="MarkedSubImage" Path="hall/res/gameitem/pj2.png" Plist="hall/res/gameitem.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="bg" ActionTag="56100765" Tag="90" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="143.7348" RightMargin="46.2652" TopMargin="93.9872" BottomMargin="45.0128" ctype="SpriteObjectData">
            <Size X="102.0000" Y="91.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="194.7348" Y="90.5128" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.6669" Y="0.3935" />
            <PreSize X="0.3493" Y="0.3957" />
            <FileData Type="MarkedSubImage" Path="hall/res/gameitem/pj1.png" Plist="hall/res/gameitem.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="line" ActionTag="-480025537" VisibleForFrame="False" Tag="91" IconVisible="False" LeftMargin="6.0000" RightMargin="60.0000" TopMargin="7.5000" BottomMargin="15.5000" ctype="SpriteObjectData">
            <Size X="226.0000" Y="207.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="119.0000" Y="119.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4075" Y="0.5174" />
            <PreSize X="0.7740" Y="0.9000" />
            <FileData Type="Normal" Path="hall/res/gameitem/ddz2.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="boom" ActionTag="-31443371" VisibleForFrame="False" Tag="92" IconVisible="False" LeftMargin="88.5000" RightMargin="114.5000" TopMargin="18.0000" BottomMargin="100.0000" ctype="SpriteObjectData">
            <Size X="89.0000" Y="112.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="133.0000" Y="156.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4555" Y="0.6783" />
            <PreSize X="0.3048" Y="0.4870" />
            <FileData Type="MarkedSubImage" Path="hall/res/gameitem/ddz5.png" Plist="hall/res/gameitem.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="cards" ActionTag="-666966918" VisibleForFrame="False" Tag="93" IconVisible="False" LeftMargin="42.0000" RightMargin="104.0000" TopMargin="55.5000" BottomMargin="19.5000" ctype="SpriteObjectData">
            <Size X="146.0000" Y="155.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="115.0000" Y="97.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3938" Y="0.4217" />
            <PreSize X="0.5000" Y="0.6739" />
            <FileData Type="MarkedSubImage" Path="hall/res/gameitem/ddz4.png" Plist="hall/res/gameitem.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="word" ActionTag="-1769036724" Tag="94" IconVisible="False" LeftMargin="68.3701" RightMargin="66.6299" TopMargin="159.3124" BottomMargin="9.6876" ctype="SpriteObjectData">
            <Size X="157.0000" Y="61.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="146.8701" Y="40.1876" />
            <Scale ScaleX="1.0500" ScaleY="1.0500" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5030" Y="0.1747" />
            <PreSize X="0.5377" Y="0.2652" />
            <FileData Type="MarkedSubImage" Path="lan/cn/game_item9.png" Plist="lan/cn/game.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>