<GameFile>
  <PropertyGroup Name="game_item10" Type="Layer" ID="a7ae6605-75ec-41d5-a108-867898bc79e0" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="300" Speed="1.0000">
        <Timeline ActionTag="-197333913" Property="Position">
          <PointFrame FrameIndex="0" X="119.0000" Y="119.0000">
            <EasingData Type="0" />
          </PointFrame>
          <PointFrame FrameIndex="300" X="119.0000" Y="119.0000">
            <EasingData Type="0" />
          </PointFrame>
        </Timeline>
        <Timeline ActionTag="-197333913" Property="Scale">
          <ScaleFrame FrameIndex="0" X="1.0000" Y="1.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="300" X="1.0000" Y="1.0000">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="-197333913" Property="RotationSkew">
          <ScaleFrame FrameIndex="0" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="300" X="360.0000" Y="360.0000">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="555862491" Property="Position">
          <PointFrame FrameIndex="0" X="119.0000" Y="25.0000">
            <EasingData Type="0" />
          </PointFrame>
          <PointFrame FrameIndex="150" X="119.0000" Y="25.0000">
            <EasingData Type="0" />
          </PointFrame>
          <PointFrame FrameIndex="300" X="119.0000" Y="25.0000">
            <EasingData Type="0" />
          </PointFrame>
        </Timeline>
        <Timeline ActionTag="555862491" Property="Scale">
          <ScaleFrame FrameIndex="0" X="1.0000" Y="1.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="150" X="1.0000" Y="1.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="300" X="1.0000" Y="1.0000">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="555862491" Property="RotationSkew">
          <ScaleFrame FrameIndex="0" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="150" X="5.0000" Y="5.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="300" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="-2121768759" Property="Position">
          <PointFrame FrameIndex="0" X="126.0000" Y="70.0000">
            <EasingData Type="0" />
          </PointFrame>
          <PointFrame FrameIndex="150" X="126.0000" Y="80.0000">
            <EasingData Type="0" />
          </PointFrame>
          <PointFrame FrameIndex="300" X="126.0000" Y="70.0000">
            <EasingData Type="0" />
          </PointFrame>
        </Timeline>
        <Timeline ActionTag="-2121768759" Property="Scale">
          <ScaleFrame FrameIndex="0" X="1.0000" Y="1.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="150" X="1.0000" Y="1.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="300" X="1.0000" Y="1.0000">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="-2121768759" Property="RotationSkew">
          <ScaleFrame FrameIndex="0" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="150" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="300" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
      </Animation>
      <ObjectData Name="Layer" Tag="95" ctype="GameLayerObjectData">
        <Size X="238.0000" Y="238.0000" />
        <Children>
          <AbstractNodeData Name="bg" ActionTag="1304699152" Tag="96" IconVisible="False" ctype="SpriteObjectData">
            <Size X="238.0000" Y="238.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="119.0000" Y="119.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="1.0000" Y="1.0000" />
            <FileData Type="Normal" Path="hall/res/gameitem/poker13_1.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="line" ActionTag="-197333913" Tag="101" RotationSkewX="271.2000" RotationSkewY="271.2000" IconVisible="False" LeftMargin="6.0000" RightMargin="6.0000" TopMargin="15.5000" BottomMargin="15.5000" ctype="SpriteObjectData">
            <Size X="226.0000" Y="207.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="119.0000" Y="119.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="0.9496" Y="0.8697" />
            <FileData Type="Normal" Path="hall/res/gameitem/ddz2.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="card" ActionTag="555862491" Tag="90" RotationSkewX="2.4667" RotationSkewY="2.4667" IconVisible="False" LeftMargin="16.5000" RightMargin="16.5000" TopMargin="2.0000" BottomMargin="25.0000" ctype="SpriteObjectData">
            <Size X="205.0000" Y="211.0000" />
            <AnchorPoint ScaleX="0.5000" />
            <Position X="119.0000" Y="25.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.1050" />
            <PreSize X="0.8613" Y="0.8866" />
            <FileData Type="Normal" Path="hall/res/gameitem/poker13_8.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="coin" ActionTag="-2121768759" Tag="91" IconVisible="False" LeftMargin="29.0000" RightMargin="15.0000" TopMargin="104.0667" BottomMargin="15.9333" ctype="SpriteObjectData">
            <Size X="194.0000" Y="118.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="126.0000" Y="74.9333" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5294" Y="0.3148" />
            <PreSize X="0.8151" Y="0.4958" />
            <FileData Type="Normal" Path="hall/res/gameitem/poker13_3.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="word" ActionTag="2055856096" Tag="102" IconVisible="False" LeftMargin="41.0000" RightMargin="37.0000" TopMargin="159.0000" BottomMargin="13.0000" ctype="SpriteObjectData">
            <Size X="160.0000" Y="66.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="121.0000" Y="46.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5084" Y="0.1933" />
            <PreSize X="0.6723" Y="0.2773" />
            <FileData Type="Default" Path="Default/Sprite.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>