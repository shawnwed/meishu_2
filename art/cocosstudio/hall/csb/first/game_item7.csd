<GameFile>
  <PropertyGroup Name="game_item7" Type="Layer" ID="bbc6664c-55de-426c-8115-2a805121645f" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="303" Speed="1.0000">
        <Timeline ActionTag="955998957" Property="Position">
          <PointFrame FrameIndex="0" X="86.9541" Y="79.0272">
            <EasingData Type="0" />
          </PointFrame>
          <PointFrame FrameIndex="155" X="85.9414" Y="84.0926">
            <EasingData Type="0" />
          </PointFrame>
          <PointFrame FrameIndex="303" X="86.9541" Y="79.0272">
            <EasingData Type="0" />
          </PointFrame>
        </Timeline>
        <Timeline ActionTag="955998957" Property="Scale">
          <ScaleFrame FrameIndex="0" X="1.0000" Y="1.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="155" X="1.0000" Y="1.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="303" X="1.0000" Y="1.0000">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="955998957" Property="RotationSkew">
          <ScaleFrame FrameIndex="0" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="155" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="303" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="292021285" Property="Position">
          <PointFrame FrameIndex="0" X="169.6687" Y="142.3737">
            <EasingData Type="0" />
          </PointFrame>
          <PointFrame FrameIndex="155" X="169.6694" Y="137.3085">
            <EasingData Type="0" />
          </PointFrame>
          <PointFrame FrameIndex="303" X="169.6687" Y="142.3737">
            <EasingData Type="0" />
          </PointFrame>
        </Timeline>
        <Timeline ActionTag="292021285" Property="Scale">
          <ScaleFrame FrameIndex="0" X="1.0000" Y="1.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="155" X="1.0000" Y="1.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="303" X="1.0000" Y="1.0000">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="292021285" Property="RotationSkew">
          <ScaleFrame FrameIndex="0" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="155" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="303" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
      </Animation>
      <ObjectData Name="Layer" Tag="95" ctype="GameLayerObjectData">
        <Size X="292.0000" Y="230.0000" />
        <Children>
          <AbstractNodeData Name="bg" ActionTag="1304699152" Tag="96" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="20.2931" RightMargin="33.7069" TopMargin="100.0943" BottomMargin="-7.0943" ctype="SpriteObjectData">
            <Size X="238.0000" Y="137.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="139.2931" Y="61.4057" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4770" Y="0.2670" />
            <PreSize X="0.8151" Y="0.5957" />
            <FileData Type="MarkedSubImage" Path="hall/res/gameitem/ddz1.png" Plist="hall/res/gameitem.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="chip" ActionTag="955998957" Tag="199" IconVisible="False" LeftMargin="42.9541" RightMargin="161.0459" TopMargin="118.9728" BottomMargin="47.0272" ctype="SpriteObjectData">
            <Size X="88.0000" Y="64.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="86.9541" Y="79.0272" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.2978" Y="0.3436" />
            <PreSize X="0.3014" Y="0.2783" />
            <FileData Type="MarkedSubImage" Path="hall/res/gameitem/bjl4.png" Plist="hall/res/gameitem.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="line" ActionTag="-197333913" VisibleForFrame="False" Tag="101" IconVisible="False" LeftMargin="6.0000" RightMargin="60.0000" TopMargin="7.5000" BottomMargin="15.5000" ctype="SpriteObjectData">
            <Size X="226.0000" Y="207.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="119.0000" Y="119.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4075" Y="0.5174" />
            <PreSize X="0.7740" Y="0.9000" />
            <FileData Type="Normal" Path="hall/res/gameitem/ddz2.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="chip_0" ActionTag="-1601651362" VisibleForFrame="False" Tag="3727" IconVisible="False" LeftMargin="37.0000" RightMargin="117.0000" TopMargin="33.5000" BottomMargin="59.5000" ctype="SpriteObjectData">
            <Size X="138.0000" Y="137.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="106.0000" Y="128.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3630" Y="0.5565" />
            <PreSize X="0.4726" Y="0.5957" />
            <FileData Type="Normal" Path="hall/res/gameitem/bjl5.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="card" ActionTag="292021285" Tag="200" IconVisible="False" LeftMargin="78.1687" RightMargin="30.8313" TopMargin="-7.8737" BottomMargin="46.8737" ctype="SpriteObjectData">
            <Size X="183.0000" Y="191.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="169.6687" Y="142.3737" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5811" Y="0.6190" />
            <PreSize X="0.6267" Y="0.8304" />
            <FileData Type="MarkedSubImage" Path="hall/res/gameitem/bjl1.png" Plist="hall/res/gameitem.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="word" ActionTag="2055856096" Tag="102" IconVisible="False" LeftMargin="61.2339" RightMargin="73.7661" TopMargin="157.1786" BottomMargin="10.8214" ctype="SpriteObjectData">
            <Size X="157.0000" Y="62.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="139.7339" Y="41.8214" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4785" Y="0.1818" />
            <PreSize X="0.5377" Y="0.2696" />
            <FileData Type="MarkedSubImage" Path="lan/cn/game_item7.png" Plist="lan/cn/game.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>