<GameFile>
  <PropertyGroup Name="game_item5" Type="Layer" ID="19d011f1-30ac-490b-94a1-e9b40a866a20" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="295" Speed="1.0000">
        <Timeline ActionTag="-2058929357" Property="Position">
          <PointFrame FrameIndex="0" X="40.8944" Y="37.5742">
            <EasingData Type="0" />
          </PointFrame>
          <PointFrame FrameIndex="125" X="40.8957" Y="44.8154">
            <EasingData Type="0" />
          </PointFrame>
          <PointFrame FrameIndex="295" X="40.8944" Y="37.5742">
            <EasingData Type="0" />
          </PointFrame>
        </Timeline>
        <Timeline ActionTag="-2058929357" Property="Scale">
          <ScaleFrame FrameIndex="0" X="1.0000" Y="1.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="125" X="1.0000" Y="1.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="295" X="1.0000" Y="1.0000">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="-2058929357" Property="RotationSkew">
          <ScaleFrame FrameIndex="0" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="125" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="295" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="738633843" Property="Position">
          <PointFrame FrameIndex="0" X="147.2926" Y="43.0612">
            <EasingData Type="0" />
          </PointFrame>
          <PointFrame FrameIndex="125" X="147.2926" Y="40.0612">
            <EasingData Type="0" />
          </PointFrame>
          <PointFrame FrameIndex="295" X="147.2926" Y="43.0612">
            <EasingData Type="0" />
          </PointFrame>
        </Timeline>
        <Timeline ActionTag="738633843" Property="Scale">
          <ScaleFrame FrameIndex="0" X="1.0000" Y="1.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="125" X="1.0000" Y="1.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="295" X="1.0000" Y="1.0000">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="738633843" Property="RotationSkew">
          <ScaleFrame FrameIndex="0" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="125" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="295" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
      </Animation>
      <ObjectData Name="Layer" Tag="95" ctype="GameLayerObjectData">
        <Size X="292.0000" Y="230.0000" />
        <Children>
          <AbstractNodeData Name="bg" ActionTag="1304699152" Tag="96" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="34.2107" RightMargin="19.7893" TopMargin="101.1685" BottomMargin="-8.1685" ctype="SpriteObjectData">
            <Size X="238.0000" Y="137.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="153.2107" Y="60.3315" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5247" Y="0.2623" />
            <PreSize X="0.8151" Y="0.5957" />
            <FileData Type="MarkedSubImage" Path="hall/res/gameitem/ddz1.png" Plist="hall/res/gameitem.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="line" ActionTag="-197333913" VisibleForFrame="False" Tag="101" RotationSkewX="360.0000" RotationSkewY="360.0000" IconVisible="False" LeftMargin="6.0000" RightMargin="60.0000" TopMargin="7.5000" BottomMargin="15.5000" ctype="SpriteObjectData">
            <Size X="226.0000" Y="207.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="119.0000" Y="119.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4075" Y="0.5174" />
            <PreSize X="0.7740" Y="0.9000" />
            <FileData Type="Normal" Path="hall/res/gameitem/ddz2.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="card3_0" ActionTag="-2058929357" Tag="3729" IconVisible="False" LeftMargin="40.8946" RightMargin="76.1054" TopMargin="-1.6749" BottomMargin="38.6749" ctype="SpriteObjectData">
            <Size X="175.0000" Y="193.0000" />
            <AnchorPoint />
            <Position X="40.8946" Y="38.6749" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.1401" Y="0.1682" />
            <PreSize X="0.5993" Y="0.8391" />
            <FileData Type="MarkedSubImage" Path="hall/res/gameitem/zjh1.png" Plist="hall/res/gameitem.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="card3" ActionTag="738633843" Tag="254" IconVisible="False" LeftMargin="147.2926" RightMargin="40.7074" TopMargin="119.3948" BottomMargin="42.6052" ctype="SpriteObjectData">
            <Size X="104.0000" Y="68.0000" />
            <AnchorPoint />
            <Position X="147.2926" Y="42.6052" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5044" Y="0.1852" />
            <PreSize X="0.3562" Y="0.2957" />
            <FileData Type="MarkedSubImage" Path="hall/res/gameitem/zjh2.png" Plist="hall/res/gameitem.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="word" ActionTag="2055856096" Tag="102" IconVisible="False" LeftMargin="75.9467" RightMargin="59.0533" TopMargin="159.4936" BottomMargin="8.5064" ctype="SpriteObjectData">
            <Size X="157.0000" Y="62.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="154.4467" Y="39.5064" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5289" Y="0.1718" />
            <PreSize X="0.5377" Y="0.2696" />
            <FileData Type="MarkedSubImage" Path="lan/cn/game_item5.png" Plist="lan/cn/game.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>