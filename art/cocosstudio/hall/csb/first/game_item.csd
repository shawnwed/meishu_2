<GameFile>
  <PropertyGroup Name="game_item" Type="Layer" ID="4db61965-14e0-4905-bc2d-b17b9940f8f3" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="2189" ctype="GameLayerObjectData">
        <Size X="292.0000" Y="230.0000" />
        <Children>
          <AbstractNodeData Name="game" ActionTag="1426791502" Tag="2190" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="123.0000" RightMargin="123.0000" TopMargin="92.0000" BottomMargin="92.0000" ctype="SpriteObjectData">
            <Size X="240.0000" Y="240.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="146.0000" Y="115.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="0.1575" Y="0.2000" />
            <FileData Type="Default" Path="Default/Sprite.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>