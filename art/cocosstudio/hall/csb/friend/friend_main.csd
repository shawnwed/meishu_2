<GameFile>
  <PropertyGroup Name="friend_main" Type="Scene" ID="709026e8-a761-458a-8a94-33f5cd7f10dd" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Scene" Tag="1677" ctype="GameNodeObjectData">
        <Size X="960.0000" Y="640.0000" />
        <Children>
          <AbstractNodeData Name="bg" ActionTag="-404003473" Tag="1732" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" TouchEnable="True" Scale9Enable="True" LeftEage="422" RightEage="422" TopEage="237" BottomEage="237" Scale9OriginX="422" Scale9OriginY="237" Scale9Width="436" Scale9Height="242" ctype="ImageViewObjectData">
            <Size X="960.0000" Y="640.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="480.0000" Y="320.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="1.0000" Y="1.0000" />
            <FileData Type="Normal" Path="bg/bg_info.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="top" ActionTag="220528768" Tag="1715" IconVisible="True" PercentWidthEnable="True" PercentWidthEnabled="True" TopMargin="-79.6800" BottomMargin="624.6800" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="True" ctype="ProjectNodeObjectData">
            <Size X="960.0000" Y="95.0000" />
            <AnchorPoint />
            <Position Y="624.6800" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition Y="0.9761" />
            <PreSize X="1.0000" Y="0.1484" />
            <FileData Type="Normal" Path="game_public/csb/hall_top.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="container" ActionTag="-1428939010" Tag="1736" IconVisible="False" PositionPercentXEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" LeftMargin="9.6000" RightMargin="9.6000" TopMargin="208.6000" BottomMargin="9.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="940.8000" Y="422.4000" />
            <AnchorPoint ScaleX="0.5000" />
            <Position X="480.0000" Y="9.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.0141" />
            <PreSize X="0.9800" Y="0.6600" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="banner" ActionTag="1654776638" Tag="1733" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="457.0000" RightMargin="457.0000" TopMargin="89.6000" BottomMargin="504.4000" ctype="SpriteObjectData">
            <Size X="46.0000" Y="46.0000" />
            <Children>
              <AbstractNodeData Name="invite_btn" ActionTag="-798479439" Tag="1734" IconVisible="False" LeftMargin="1091.0652" RightMargin="-1091.0652" TopMargin="-59.8566" BottomMargin="59.8566" ctype="SpriteObjectData">
                <Size X="46.0000" Y="46.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="1114.0652" Y="82.8566" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="24.2188" Y="1.8012" />
                <PreSize X="1.0000" Y="1.0000" />
                <FileData Type="Default" Path="Default/Sprite.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="1.0000" />
            <Position X="480.0000" Y="550.4000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.8600" />
            <PreSize X="0.0479" Y="0.0719" />
            <FileData Type="Default" Path="Default/Sprite.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="tip" ActionTag="393955908" Tag="1735" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="304.0000" RightMargin="304.0000" TopMargin="367.5000" BottomMargin="239.5000" FontSize="32" LabelText="暫無好友，快去邀請吧！" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="352.0000" Y="33.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="480.0000" Y="256.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.4000" />
            <PreSize X="0.3667" Y="0.0516" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>