<GameFile>
  <PropertyGroup Name="friend_row_item" Type="Layer" ID="17a32ee3-44f7-4d5b-ae52-0cc17564aa1c" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="109" ctype="GameLayerObjectData">
        <Size X="1240.0000" Y="175.0000" />
        <Children>
          <AbstractNodeData Name="item_1" ActionTag="-1841268401" Tag="110" IconVisible="True" RightMargin="630.0000" TopMargin="2.0000" BottomMargin="3.0000" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="610.0000" Y="170.0000" />
            <AnchorPoint />
            <Position Y="3.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition Y="0.0171" />
            <PreSize X="0.4919" Y="0.9714" />
            <FileData Type="Normal" Path="hall/csb/friend/friend_item.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="item_2" ActionTag="-1765002327" Tag="121" IconVisible="True" LeftMargin="630.0000" TopMargin="2.0000" BottomMargin="3.0000" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="610.0000" Y="170.0000" />
            <AnchorPoint />
            <Position X="630.0000" Y="3.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5081" Y="0.0171" />
            <PreSize X="0.4919" Y="0.9714" />
            <FileData Type="Normal" Path="hall/csb/friend/friend_item.csd" Plist="" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>