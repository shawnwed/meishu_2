<GameFile>
  <PropertyGroup Name="friend_item" Type="Layer" ID="2313c576-20bf-4f90-8f69-a584f0caa5df" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="1737" ctype="GameLayerObjectData">
        <Size X="610.0000" Y="170.0000" />
        <Children>
          <AbstractNodeData Name="bg" CanEdit="False" ActionTag="-129537038" Tag="1738" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-7.0000" RightMargin="-7.0000" ctype="SpriteObjectData">
            <Size X="624.0000" Y="170.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="305.0000" Y="85.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="1.0230" Y="1.0000" />
            <FileData Type="MarkedSubImage" Path="hall/res/friend/friend_item_bg.png" Plist="hall/res/friend.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="head" ActionTag="-954438999" Tag="1838" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="38.1439" RightMargin="446.8561" TopMargin="19.5080" BottomMargin="25.4920" LeftEage="28" RightEage="28" TopEage="28" BottomEage="28" Scale9OriginX="28" Scale9OriginY="28" Scale9Width="30" Scale9Height="30" ctype="ImageViewObjectData">
            <Size X="125.0000" Y="125.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="100.6439" Y="87.9920" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.1650" Y="0.5176" />
            <PreSize X="0.2049" Y="0.7353" />
            <FileData Type="MarkedSubImage" Path="hall/res/first/head_bg.png" Plist="hall/res/first.plist" />
          </AbstractNodeData>
          <AbstractNodeData Name="name" ActionTag="389535543" Tag="1839" IconVisible="False" LeftMargin="196.0639" RightMargin="268.9361" TopMargin="24.2943" BottomMargin="108.7057" FontSize="32" LabelText="helloworld" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="145.0000" Y="37.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="196.0639" Y="127.2057" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3214" Y="0.7483" />
            <PreSize X="0.2377" Y="0.2176" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="diamond_icon" ActionTag="-287032371" Tag="1841" IconVisible="False" LeftMargin="189.6415" RightMargin="373.3585" TopMargin="104.7377" BottomMargin="25.2623" ctype="SpriteObjectData">
            <Size X="47.0000" Y="40.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="213.1415" Y="45.2623" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3494" Y="0.2662" />
            <PreSize X="0.0770" Y="0.2353" />
            <FileData Type="MarkedSubImage" Path="game_public/res/diamond_icon.png" Plist="game_public/res/game.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="coin_icon" ActionTag="-1560506037" Tag="1842" IconVisible="False" LeftMargin="191.0600" RightMargin="379.9400" TopMargin="63.5160" BottomMargin="65.4840" ctype="SpriteObjectData">
            <Size X="39.0000" Y="41.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="210.5600" Y="85.9840" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3452" Y="0.5058" />
            <PreSize X="0.0639" Y="0.2412" />
            <FileData Type="MarkedSubImage" Path="game_public/res/coin_small_icon2.png" Plist="game_public/res/game.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="coin_txt" ActionTag="693041140" Tag="1843" IconVisible="False" LeftMargin="239.1589" RightMargin="289.8411" TopMargin="68.2660" BottomMargin="68.7340" FontSize="29" LabelText="10000" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="81.0000" Y="33.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="239.1589" Y="85.2340" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3921" Y="0.5014" />
            <PreSize X="0.1328" Y="0.1941" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="diamond_txt" ActionTag="919472466" Tag="1844" IconVisible="False" LeftMargin="239.1589" RightMargin="305.8411" TopMargin="108.2376" BottomMargin="28.7624" FontSize="29" LabelText="1000" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="65.0000" Y="33.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="239.1589" Y="45.2624" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3921" Y="0.2662" />
            <PreSize X="0.1066" Y="0.1941" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="btn_give" ActionTag="-249110485" Tag="1845" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="443.0333" RightMargin="72.9667" TopMargin="30.0000" BottomMargin="30.0000" ctype="SpriteObjectData">
            <Size X="94.0000" Y="110.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="490.0333" Y="85.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8033" Y="0.5000" />
            <PreSize X="0.1541" Y="0.6471" />
            <FileData Type="Default" Path="Default/Sprite.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="btn_request" ActionTag="1352925940" VisibleForFrame="False" Tag="1846" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="481.8239" RightMargin="34.1761" TopMargin="30.0000" BottomMargin="30.0000" ctype="SpriteObjectData">
            <Size X="94.0000" Y="110.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="528.8239" Y="85.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8669" Y="0.5000" />
            <PreSize X="0.1541" Y="0.6471" />
            <FileData Type="Default" Path="Default/Sprite.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>