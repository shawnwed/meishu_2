<GameFile>
  <PropertyGroup Name="item" Type="Layer" ID="38bfb86c-be7e-4bee-9f63-ecbc3dd0ee96" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="31" ctype="GameLayerObjectData">
        <Size X="1280.0000" Y="70.0000" />
        <Children>
          <AbstractNodeData Name="icon_1" ActionTag="-1586089777" Tag="32" IconVisible="False" LeftMargin="10.5281" RightMargin="1221.4719" TopMargin="6.7180" BottomMargin="23.2820" ctype="SpriteObjectData">
            <Size X="48.0000" Y="40.0000" />
            <AnchorPoint ScaleX="1.0000" ScaleY="1.0000" />
            <Position X="58.5281" Y="63.2820" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0457" Y="0.9040" />
            <PreSize X="0.0375" Y="0.5714" />
            <FileData Type="Normal" Path="hall/res/speak/icon.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="name" ActionTag="-1023461236" Tag="33" IconVisible="False" LeftMargin="78.2193" RightMargin="1041.7808" TopMargin="9.5106" BottomMargin="27.4894" FontSize="32" LabelText="我是大魔王" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="160.0000" Y="33.0000" />
            <AnchorPoint ScaleY="1.0000" />
            <Position X="78.2193" Y="60.4894" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="82" G="255" B="0" />
            <PrePosition X="0.0611" Y="0.8641" />
            <PreSize X="0.1250" Y="0.4714" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>