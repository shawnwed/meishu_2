<GameFile>
  <PropertyGroup Name="speakrun" Type="Layer" ID="d8fbe17b-425e-4126-bdda-05ac628c60af" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="35" ctype="GameLayerObjectData">
        <Size X="1020.0000" Y="54.0000" />
        <Children>
          <AbstractNodeData Name="Image_1" ActionTag="-1243803316" Tag="1191" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentWidthEnabled="True" LeftMargin="1.7340" RightMargin="470.2200" TopMargin="6.0388" BottomMargin="7.9612" LeftEage="231" RightEage="231" TopEage="13" BottomEage="13" Scale9OriginX="231" Scale9OriginY="13" Scale9Width="86" Scale9Height="14" ctype="ImageViewObjectData">
            <Size X="548.0460" Y="40.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="1.7340" Y="27.9612" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0017" Y="0.5178" />
            <PreSize X="0.5373" Y="0.7407" />
            <FileData Type="MarkedSubImage" Path="hall/res/first/notice.png" Plist="hall/res/first.plist" />
          </AbstractNodeData>
          <AbstractNodeData Name="p" ActionTag="-2106850221" Tag="79" IconVisible="False" PositionPercentXEnabled="True" PercentHeightEnable="True" PercentHeightEnabled="True" LeftMargin="74.5620" RightMargin="345.4380" TopMargin="-0.7518" BottomMargin="0.7518" ClipAble="True" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="600.0000" Y="54.0000" />
            <Children>
              <AbstractNodeData Name="txt" ActionTag="55994517" Tag="78" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="-6.0048" RightMargin="494.0048" TopMargin="14.0800" BottomMargin="11.9200" FontSize="28" LabelText=" 1111111" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="112.0000" Y="28.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="-6.0048" Y="25.9200" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="-0.0100" Y="0.4800" />
                <PreSize X="0.1867" Y="0.5185" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position X="74.5620" Y="0.7518" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0731" Y="0.0139" />
            <PreSize X="0.5882" Y="1.0000" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="icon_1" ActionTag="1896574511" Tag="618" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="6.6882" RightMargin="965.3118" TopMargin="6.8950" BottomMargin="7.1050" ctype="SpriteObjectData">
            <Size X="48.0000" Y="40.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="30.6882" Y="27.1050" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0301" Y="0.5019" />
            <PreSize X="0.0471" Y="0.7407" />
            <FileData Type="Normal" Path="hall/res/speak/icon.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>