<GameFile>
  <PropertyGroup Name="bindphone" Type="Scene" ID="80a595be-d48c-47ef-aa4e-b310c04bb4f3" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Scene" Tag="951" ctype="GameNodeObjectData">
        <Size X="960.0000" Y="640.0000" />
        <Children>
          <AbstractNodeData Name="Panel_1" ActionTag="-1876698156" Tag="952" IconVisible="False" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="960.0000" Y="640.0000" />
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="1.0000" Y="1.0000" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="Image_bg" ActionTag="-1946396066" Tag="953" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" LeftEage="211" RightEage="211" TopEage="146" BottomEage="146" Scale9OriginX="211" Scale9OriginY="146" Scale9Width="493" Scale9Height="284" ctype="ImageViewObjectData">
            <Size X="960.0000" Y="640.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="480.0000" Y="320.0000" />
            <Scale ScaleX="0.7200" ScaleY="0.7200" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="1.0000" Y="1.0000" />
            <FileData Type="Normal" Path="hall/res/bingphone/bg.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Image_1" ActionTag="196953336" Tag="73" IconVisible="False" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" LeftMargin="176.3061" RightMargin="170.4459" TopMargin="184.5260" BottomMargin="223.0900" Scale9Enable="True" LeftEage="21" RightEage="21" TopEage="21" BottomEage="21" Scale9OriginX="21" Scale9OriginY="21" Scale9Width="24" Scale9Height="24" ctype="ImageViewObjectData">
            <Size X="613.2480" Y="232.3840" />
            <AnchorPoint />
            <Position X="176.3061" Y="223.0900" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.1837" Y="0.3486" />
            <PreSize X="0.6388" Y="0.3631" />
            <FileData Type="Normal" Path="hall/res/bingphone/bd2.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Text_2" ActionTag="-190000729" Tag="957" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="200.0600" RightMargin="634.9400" TopMargin="201.0485" BottomMargin="413.9515" FontSize="25" LabelText="温馨提示：" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="125.0000" Y="25.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="262.5600" Y="426.4515" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="81" G="70" B="69" />
            <PrePosition X="0.2735" Y="0.6663" />
            <PreSize X="0.1302" Y="0.0391" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="txt" ActionTag="-638125968" Tag="954" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="361.2640" RightMargin="210.7360" TopMargin="201.9684" BottomMargin="413.0316" FontSize="25" LabelText="绑定手机后可以免费领取3金币哦！" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="388.0000" Y="25.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="1.0000" />
            <Position X="555.2640" Y="438.0316" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="81" G="70" B="69" />
            <PrePosition X="0.5784" Y="0.6844" />
            <PreSize X="0.4042" Y="0.0391" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="title" ActionTag="1808199569" Tag="955" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="413.7720" RightMargin="369.2280" TopMargin="104.8645" BottomMargin="488.1355" ctype="SpriteObjectData">
            <Size X="177.0000" Y="47.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="502.2720" Y="511.6355" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5232" Y="0.7994" />
            <PreSize X="0.1844" Y="0.0734" />
            <FileData Type="Normal" Path="hall/res/bingphone/bd1.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="close" ActionTag="-2133849760" Tag="2991" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="765.6000" RightMargin="146.4000" TopMargin="104.8493" BottomMargin="489.1507" ctype="SpriteObjectData">
            <Size X="48.0000" Y="46.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="789.6000" Y="512.1507" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8225" Y="0.8002" />
            <PreSize X="0.0500" Y="0.0719" />
            <FileData Type="MarkedSubImage" Path="game_public/res/btn_close.png" Plist="game_public/res/game.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="play" ActionTag="-648768558" VisibleForFrame="False" Tag="1030" IconVisible="False" LeftMargin="391.9827" RightMargin="341.0173" TopMargin="397.0360" BottomMargin="168.9640" ctype="SpriteObjectData">
            <Size X="227.0000" Y="74.0000" />
            <Children>
              <AbstractNodeData Name="Text_6" ActionTag="437782517" Tag="1032" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="65.5000" RightMargin="65.5000" TopMargin="20.5000" BottomMargin="20.5000" FontSize="32" LabelText="去玩玩" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="96.0000" Y="33.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="113.5000" Y="37.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="95" G="43" B="43" />
                <PrePosition X="0.5000" Y="0.5000" />
                <PreSize X="0.4229" Y="0.4459" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="505.4827" Y="205.9640" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5265" Y="0.3218" />
            <PreSize X="0.2365" Y="0.1156" />
            <FileData Type="Normal" Path="hall/res/bingphone/btn_left.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="Sprite_5" ActionTag="-1715246944" Tag="1029" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="397.7280" RightMargin="394.2720" TopMargin="277.7656" BottomMargin="214.2344" ctype="SpriteObjectData">
            <Size X="168.0000" Y="148.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="481.7280" Y="288.2344" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5018" Y="0.4504" />
            <PreSize X="0.1750" Y="0.2313" />
            <FileData Type="Normal" Path="hall/res/bingphone/bd5.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="bind" ActionTag="1474531015" Tag="1031" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="331.7480" RightMargin="353.2520" TopMargin="430.5056" BottomMargin="114.4944" ctype="SpriteObjectData">
            <Size X="275.0000" Y="95.0000" />
            <Children>
              <AbstractNodeData Name="Text_7" ActionTag="-792007475" VisibleForFrame="False" Tag="1033" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="73.5000" RightMargin="73.5000" TopMargin="31.0000" BottomMargin="31.0000" FontSize="32" LabelText="马上绑定" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="128.0000" Y="33.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="137.5000" Y="47.5000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="95" G="43" B="43" />
                <PrePosition X="0.5000" Y="0.5000" />
                <PreSize X="0.4655" Y="0.3474" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="469.2480" Y="161.9944" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4888" Y="0.2531" />
            <PreSize X="0.2865" Y="0.1484" />
            <FileData Type="Normal" Path="hall/res/bingphone/bd4.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>