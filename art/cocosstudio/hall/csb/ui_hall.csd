<GameFile>
  <PropertyGroup Name="ui_hall" Type="Scene" ID="e0b7399a-c021-4c29-99cd-21c61e40fc28" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Scene" Tag="190" ctype="GameNodeObjectData">
        <Size X="960.0000" Y="640.0000" />
        <Children>
          <AbstractNodeData Name="bg" ActionTag="-146549975" Tag="150" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" LeftMargin="-48.0000" RightMargin="-48.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="1056.0000" Y="640.0000" />
            <Children>
              <AbstractNodeData Name="top" ActionTag="-728315493" VisibleForFrame="False" Tag="19" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" BottomMargin="203.9680" TouchEnable="True" LeftEage="316" RightEage="316" TopEage="211" BottomEage="211" Scale9OriginX="-270" Scale9OriginY="-165" Scale9Width="586" Scale9Height="376" ctype="ImageViewObjectData">
                <Size X="1056.0000" Y="436.0320" />
                <AnchorPoint ScaleX="0.5000" ScaleY="1.0000" />
                <Position X="528.0000" Y="640.0000" />
                <Scale ScaleX="1.0000" ScaleY="0.2748" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="1.0000" />
                <PreSize X="1.0000" Y="0.6813" />
                <FileData Type="Default" Path="Default/ImageFile.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="bottom" ActionTag="1114134507" VisibleForFrame="False" Tag="149" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" TopMargin="356.0320" TouchEnable="True" LeftEage="316" RightEage="316" TopEage="88" BottomEage="88" Scale9OriginX="316" Scale9OriginY="88" Scale9Width="648" Scale9Height="108" ctype="ImageViewObjectData">
                <Size X="1056.0000" Y="283.9680" />
                <AnchorPoint ScaleX="0.5000" />
                <Position X="528.0000" />
                <Scale ScaleX="1.0000" ScaleY="0.3908" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" />
                <PreSize X="1.0000" Y="0.4437" />
                <FileData Type="Normal" Path="bg/bg_hall_2.jpg" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="moon" ActionTag="-670622051" VisibleForFrame="False" Tag="140" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="592.0144" RightMargin="417.9856" TopMargin="27.9440" BottomMargin="566.0560" ctype="SpriteObjectData">
                <Size X="46.0000" Y="46.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="615.0144" Y="589.0560" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5824" Y="0.9204" />
                <PreSize X="0.0436" Y="0.0719" />
                <FileData Type="Default" Path="Default/Sprite.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="bg" ActionTag="-482321000" Tag="3722" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" LeftEage="422" RightEage="422" TopEage="237" BottomEage="237" Scale9OriginX="422" Scale9OriginY="237" Scale9Width="436" Scale9Height="246" ctype="ImageViewObjectData">
                <Size X="1056.0000" Y="640.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="528.0000" Y="320.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5000" />
                <PreSize X="1.0000" Y="1.0000" />
                <FileData Type="Normal" Path="bg/bgbg.png" Plist="" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="480.0000" Y="320.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="1.1000" Y="1.0000" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="top" ActionTag="1191973458" Tag="1279" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentWidthEnabled="True" BottomMargin="540.0000" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" LeftEage="422" RightEage="422" TopEage="32" BottomEage="32" Scale9OriginX="-422" Scale9OriginY="-32" Scale9Width="844" Scale9Height="64" ctype="PanelObjectData">
            <Size X="960.0000" Y="100.0000" />
            <Children>
              <AbstractNodeData Name="coin_bg" ActionTag="-483323823" VisibleForFrame="False" Tag="525" IconVisible="False" LeftMargin="257.5240" RightMargin="656.4760" TopMargin="1.0705" BottomMargin="52.9295" ctype="SpriteObjectData">
                <Size X="46.0000" Y="46.0000" />
                <AnchorPoint ScaleY="1.0000" />
                <Position X="257.5240" Y="98.9295" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2683" Y="0.9893" />
                <PreSize X="0.0479" Y="0.4600" />
                <FileData Type="Default" Path="Default/Sprite.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="info_bg" ActionTag="-1269318901" Tag="2163" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" RightMargin="280.0000" BottomMargin="19.0000" ctype="SpriteObjectData">
                <Size X="680.0000" Y="81.0000" />
                <AnchorPoint ScaleY="1.0000" />
                <Position Y="100.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition Y="1.0000" />
                <PreSize X="0.7083" Y="0.8100" />
                <FileData Type="MarkedSubImage" Path="hall/res/first/info_bg.png" Plist="hall/res/first.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="coin" ActionTag="-236659675" Tag="2287" IconVisible="True" LeftMargin="292.0958" RightMargin="437.9042" TopMargin="11.4502" BottomMargin="42.5498" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
                <Size X="230.0000" Y="46.0000" />
                <AnchorPoint />
                <Position X="292.0958" Y="42.5498" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3043" Y="0.4255" />
                <PreSize X="0.2396" Y="0.4600" />
                <FileData Type="Normal" Path="hall/csb/top_coin.csd" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="head" ActionTag="-1906813774" Tag="2167" IconVisible="False" LeftMargin="21.9593" RightMargin="858.0407" TopMargin="12.3228" BottomMargin="7.6772" ctype="SpriteObjectData">
                <Size X="80.0000" Y="80.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="61.9593" Y="47.6772" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0645" Y="0.4768" />
                <PreSize X="0.0833" Y="0.8000" />
                <FileData Type="Normal" Path="game_public/res/avatar_bg.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="head_bord" ActionTag="-1366889685" VisibleForFrame="False" Tag="2168" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="20.0160" RightMargin="855.9840" TopMargin="15.3431" BottomMargin="0.6569" ctype="SpriteObjectData">
                <Size X="84.0000" Y="84.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="62.0160" Y="42.6569" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0646" Y="0.4266" />
                <PreSize X="0.0875" Y="0.8400" />
                <FileData Type="Normal" Path="game_public/res/avatar_frame.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="name" ActionTag="-775423081" Tag="29" IconVisible="False" PositionPercentXEnabled="True" VerticalEdge="BothEdge" LeftMargin="85.5360" RightMargin="742.4640" TopMargin="13.2700" BottomMargin="62.7300" FontSize="24" LabelText="南宫紫菱..." VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="132.0000" Y="24.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="85.5360" Y="74.7300" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0891" Y="0.7473" />
                <PreSize X="0.1375" Y="0.2400" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="account_type" ActionTag="906254269" Tag="82" IconVisible="False" PositionPercentXEnabled="True" VerticalEdge="BothEdge" LeftMargin="87.0720" RightMargin="740.9280" TopMargin="46.0700" BottomMargin="31.9300" FontSize="22" LabelText="Facebook賬號" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="132.0000" Y="22.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="87.0720" Y="42.9300" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="0" />
                <PrePosition X="0.0907" Y="0.4293" />
                <PreSize X="0.1375" Y="0.2200" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="roomcard" ActionTag="-603059167" VisibleForFrame="False" Tag="533" IconVisible="True" PositionPercentXEnabled="True" LeftMargin="442.1760" RightMargin="287.8240" TopMargin="-112.9594" BottomMargin="166.9594" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
                <Size X="230.0000" Y="46.0000" />
                <AnchorPoint />
                <Position X="442.1760" Y="166.9594" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4606" Y="1.6696" />
                <PreSize X="0.2396" Y="0.4600" />
                <FileData Type="Normal" Path="hall/csb/top_coin.csd" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="diamond" ActionTag="500359267" VisibleForFrame="False" Tag="83" IconVisible="True" PositionPercentXEnabled="True" LeftMargin="457.2480" RightMargin="272.7520" TopMargin="-91.4294" BottomMargin="145.4294" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
                <Size X="230.0000" Y="46.0000" />
                <AnchorPoint />
                <Position X="457.2480" Y="145.4294" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4763" Y="1.4543" />
                <PreSize X="0.2396" Y="0.4600" />
                <FileData Type="Normal" Path="hall/csb/top_coin.csd" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="activity_pay" ActionTag="-2101528464" VisibleForFrame="False" Tag="561" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="634.7680" RightMargin="225.2320" TopMargin="-137.3066" BottomMargin="137.3066" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="100.0000" Y="100.0000" />
                <Children>
                  <AbstractNodeData Name="light" CanEdit="False" ActionTag="-543137492" Tag="562" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-2.5000" RightMargin="-2.5000" TopMargin="-3.5000" BottomMargin="-3.5000" ctype="SpriteObjectData">
                    <Size X="105.0000" Y="107.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="50.0000" Y="50.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="1.0500" Y="1.0700" />
                    <FileData Type="MarkedSubImage" Path="hall/res/first/btn_light.png" Plist="hall/res/first.plist" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="btn" ActionTag="-1464550696" Tag="1545" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="27.0000" RightMargin="27.0000" TopMargin="27.0000" BottomMargin="27.0000" ctype="SpriteObjectData">
                    <Size X="46.0000" Y="46.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="50.0000" Y="50.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="0.4600" Y="0.4600" />
                    <FileData Type="Default" Path="Default/Sprite.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="684.7680" Y="187.3066" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7133" Y="1.8731" />
                <PreSize X="0.1042" Y="1.0000" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="activity_login" ActionTag="-281410176" VisibleForFrame="False" Tag="563" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="711.9520" RightMargin="148.0480" TopMargin="-143.0455" BottomMargin="143.0455" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="100.0000" Y="100.0000" />
                <Children>
                  <AbstractNodeData Name="light" CanEdit="False" ActionTag="-892955771" Tag="564" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-2.5000" RightMargin="-2.5000" TopMargin="-3.5000" BottomMargin="-3.5000" ctype="SpriteObjectData">
                    <Size X="105.0000" Y="107.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="50.0000" Y="50.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="1.0500" Y="1.0700" />
                    <FileData Type="MarkedSubImage" Path="hall/res/first/btn_light.png" Plist="hall/res/first.plist" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="btn" ActionTag="-1063218197" Tag="1544" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="27.0000" RightMargin="27.0000" TopMargin="27.0000" BottomMargin="27.0000" ctype="SpriteObjectData">
                    <Size X="46.0000" Y="46.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="50.0000" Y="50.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="0.4600" Y="0.4600" />
                    <FileData Type="Default" Path="Default/Sprite.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="761.9520" Y="193.0455" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7937" Y="1.9305" />
                <PreSize X="0.1042" Y="1.0000" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="activity_broken" ActionTag="939695739" Tag="569" IconVisible="False" HorizontalEdge="RightEdge" LeftMargin="848.9441" RightMargin="11.0559" TopMargin="3.0000" BottomMargin="-3.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="100.0000" Y="100.0000" />
                <Children>
                  <AbstractNodeData Name="light" ActionTag="-1776931846" Tag="570" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-2.5000" RightMargin="-2.5000" TopMargin="-3.5000" BottomMargin="-3.5000" ctype="SpriteObjectData">
                    <Size X="105.0000" Y="107.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="50.0000" Y="50.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="1.0500" Y="1.0700" />
                    <FileData Type="MarkedSubImage" Path="hall/res/first/btn_light.png" Plist="hall/res/first.plist" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="btn" ActionTag="-2040924759" Tag="1541" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="5.0000" RightMargin="5.0000" TopMargin="5.0000" BottomMargin="5.0000" ctype="SpriteObjectData">
                    <Size X="90.0000" Y="90.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="50.0000" Y="50.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="0.9000" Y="0.9000" />
                    <FileData Type="MarkedSubImage" Path="lan/cn/activity_broken.png" Plist="lan/cn/game.plist" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="898.9441" Y="47.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.9364" Y="0.4700" />
                <PreSize X="0.1042" Y="1.0000" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="activity_diamond" ActionTag="1763995150" VisibleForFrame="False" Tag="603" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="785.6801" RightMargin="74.3199" TopMargin="-126.8205" BottomMargin="126.8205" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="100.0000" Y="100.0000" />
                <Children>
                  <AbstractNodeData Name="light" CanEdit="False" ActionTag="-1607956546" Tag="604" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-2.5000" RightMargin="-2.5000" TopMargin="-3.5000" BottomMargin="-3.5000" ctype="SpriteObjectData">
                    <Size X="105.0000" Y="107.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="50.0000" Y="50.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="1.0500" Y="1.0700" />
                    <FileData Type="MarkedSubImage" Path="hall/res/first/btn_light.png" Plist="hall/res/first.plist" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="btn" ActionTag="1404750119" Tag="605" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="27.0000" RightMargin="27.0000" TopMargin="27.0000" BottomMargin="27.0000" ctype="SpriteObjectData">
                    <Size X="46.0000" Y="46.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="50.0000" Y="50.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="0.4600" Y="0.4600" />
                    <FileData Type="Default" Path="Default/Sprite.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="835.6801" Y="176.8205" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.8705" Y="1.7682" />
                <PreSize X="0.1042" Y="1.0000" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="btn_wxshare" ActionTag="2033763403" VisibleForFrame="False" Tag="910" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="681.4480" RightMargin="232.5520" TopMargin="-97.8205" BottomMargin="151.8205" ctype="SpriteObjectData">
                <Size X="46.0000" Y="46.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="704.4480" Y="174.8205" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7338" Y="1.7482" />
                <PreSize X="0.0479" Y="0.4600" />
                <FileData Type="Default" Path="Default/Sprite.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="btn_agent" ActionTag="-2138921781" VisibleForFrame="False" Tag="911" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="774.0880" RightMargin="139.9120" TopMargin="-97.8205" BottomMargin="151.8205" ctype="SpriteObjectData">
                <Size X="46.0000" Y="46.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="797.0880" Y="174.8205" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.8303" Y="1.7482" />
                <PreSize X="0.0479" Y="0.4600" />
                <FileData Type="Default" Path="Default/Sprite.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="btn_service" ActionTag="-464660929" VisibleForFrame="False" Tag="912" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="866.7280" RightMargin="47.2720" TopMargin="-97.8205" BottomMargin="151.8205" ctype="SpriteObjectData">
                <Size X="46.0000" Y="46.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="889.7280" Y="174.8205" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.9268" Y="1.7482" />
                <PreSize X="0.0479" Y="0.4600" />
                <FileData Type="Default" Path="Default/Sprite.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="activity_friend" Visible="False" ActionTag="1862066008" Tag="2116" IconVisible="False" LeftMargin="873.0239" RightMargin="-13.0239" TopMargin="-135.9163" BottomMargin="135.9163" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="100.0000" Y="100.0000" />
                <Children>
                  <AbstractNodeData Name="btn" ActionTag="70574398" Tag="2118" IconVisible="False" LeftMargin="-23.0000" RightMargin="77.0000" TopMargin="17.7100" BottomMargin="36.2900" ctype="SpriteObjectData">
                    <Size X="46.0000" Y="46.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position Y="59.2900" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition Y="0.5929" />
                    <PreSize X="0.4600" Y="0.4600" />
                    <FileData Type="Default" Path="Default/Sprite.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="923.0239" Y="185.9163" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.9615" Y="1.8592" />
                <PreSize X="0.1042" Y="1.0000" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="btn_exchange" ActionTag="1479915804" Tag="526" IconVisible="False" HorizontalEdge="RightEdge" LeftMargin="743.6001" RightMargin="116.3999" TopMargin="3.0000" BottomMargin="-3.0000" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="100.0000" Y="100.0000" />
                <Children>
                  <AbstractNodeData Name="light" CanEdit="False" ActionTag="1911962435" Tag="527" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-2.5000" RightMargin="-2.5000" TopMargin="-3.5000" BottomMargin="-3.5000" ctype="SpriteObjectData">
                    <Size X="105.0000" Y="107.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="50.0000" Y="50.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="1.0500" Y="1.0700" />
                    <FileData Type="MarkedSubImage" Path="hall/res/first/btn_light.png" Plist="hall/res/first.plist" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="btn" ActionTag="1061658026" Tag="528" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="8.0000" RightMargin="8.0000" TopMargin="7.9900" BottomMargin="2.0100" ctype="SpriteObjectData">
                    <Size X="84.0000" Y="90.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="50.0000" Y="47.0100" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.4701" />
                    <PreSize X="0.8400" Y="0.9000" />
                    <FileData Type="MarkedSubImage" Path="lan/cn/exchange.png" Plist="lan/cn/game.plist" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="793.6001" Y="47.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.8267" Y="0.4700" />
                <PreSize X="0.1042" Y="1.0000" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="1.0000" />
            <Position X="480.0000" Y="640.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="1.0000" />
            <PreSize X="1.0000" Y="0.1563" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="content_1" ActionTag="-791259663" Tag="60" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentWidthEnabled="True" LeftMargin="95.4240" RightMargin="19.7760" TopMargin="119.0000" BottomMargin="71.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="844.8000" Y="450.0000" />
            <Children>
              <AbstractNodeData Name="sc" ActionTag="2064370299" Tag="2195" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentWidthEnabled="True" LeftMargin="228.0960" TopMargin="-61.8900" BottomMargin="-13.1100" TouchEnable="True" ClipAble="True" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ScrollDirectionType="0" ctype="PageViewObjectData">
                <Size X="616.7040" Y="525.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="228.0960" Y="249.3900" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2700" Y="0.5542" />
                <PreSize X="0.7300" Y="1.1667" />
                <SingleColor A="255" R="150" G="150" B="100" />
                <FirstColor A="255" R="150" G="150" B="100" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="create_room" ActionTag="843270068" VisibleForFrame="False" Tag="495" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentWidthEnabled="True" RightMargin="624.3072" TopMargin="3.9600" BottomMargin="260.0400" LeftEage="97" RightEage="97" TopEage="61" BottomEage="61" Scale9OriginX="-51" Scale9OriginY="-15" Scale9Width="148" Scale9Height="76" ctype="ImageViewObjectData">
                <Size X="220.4928" Y="186.0000" />
                <AnchorPoint ScaleY="1.0000" />
                <Position Y="446.0400" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition Y="0.9912" />
                <PreSize X="0.2610" Y="0.4133" />
                <FileData Type="Default" Path="Default/ImageFile.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="join_room" ActionTag="1360763501" VisibleForFrame="False" Tag="496" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentWidthEnabled="True" LeftMargin="1.1827" RightMargin="623.1245" TopMargin="239.9700" BottomMargin="24.0300" LeftEage="97" RightEage="97" TopEage="61" BottomEage="61" Scale9OriginX="-51" Scale9OriginY="-15" Scale9Width="148" Scale9Height="76" ctype="ImageViewObjectData">
                <Size X="220.4928" Y="186.0000" />
                <AnchorPoint />
                <Position X="1.1827" Y="24.0300" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0014" Y="0.0534" />
                <PreSize X="0.2610" Y="0.4133" />
                <FileData Type="Default" Path="Default/ImageFile.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="share" ActionTag="-1181333316" Tag="220" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-45.9230" RightMargin="690.7230" TopMargin="19.3442" BottomMargin="32.6558" LeftEage="97" RightEage="97" TopEage="61" BottomEage="61" Scale9OriginX="97" Scale9OriginY="61" Scale9Width="190" Scale9Height="528" ctype="ImageViewObjectData">
                <Size X="200.0000" Y="398.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="54.0770" Y="231.6558" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0640" Y="0.5148" />
                <PreSize X="0.2367" Y="0.8844" />
                <FileData Type="MarkedSubImage" Path="hall/res/first/share_banner.png" Plist="hall/res/first.plist" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="517.8240" Y="296.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5394" Y="0.4625" />
            <PreSize X="0.8800" Y="0.7031" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="content_2" Visible="False" ActionTag="-1472889625" Tag="515" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentWidthEnabled="True" LeftMargin="76.8000" RightMargin="38.4000" TopMargin="119.0000" BottomMargin="71.0000" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="844.8000" Y="450.0000" />
            <Children>
              <AbstractNodeData Name="create_room" ActionTag="-1388359352" Tag="517" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="274.1096" RightMargin="270.7304" TopMargin="5.0200" BottomMargin="44.9800" LeftEage="97" RightEage="97" TopEage="61" BottomEage="61" Scale9OriginX="-51" Scale9OriginY="-15" Scale9Width="148" Scale9Height="76" ctype="ImageViewObjectData">
                <Size X="299.9600" Y="400.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="424.0896" Y="244.9800" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5020" Y="0.5444" />
                <PreSize X="0.3551" Y="0.8889" />
                <FileData Type="Default" Path="Default/ImageFile.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="join_room" ActionTag="-966872600" Tag="518" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="555.4280" RightMargin="-10.5880" TopMargin="5.0200" BottomMargin="44.9800" LeftEage="97" RightEage="97" TopEage="61" BottomEage="61" Scale9OriginX="-51" Scale9OriginY="-15" Scale9Width="148" Scale9Height="76" ctype="ImageViewObjectData">
                <Size X="299.9600" Y="400.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="705.4080" Y="244.9800" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.8350" Y="0.5444" />
                <PreSize X="0.3551" Y="0.8889" />
                <FileData Type="Default" Path="Default/ImageFile.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="agent" ActionTag="943545820" Tag="519" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-10.6080" RightMargin="555.4080" TopMargin="5.0200" BottomMargin="44.9800" LeftEage="97" RightEage="97" TopEage="61" BottomEage="61" Scale9OriginX="-51" Scale9OriginY="-15" Scale9Width="148" Scale9Height="76" ctype="ImageViewObjectData">
                <Size X="300.0000" Y="400.0000" />
                <Children>
                  <AbstractNodeData Name="txt" ActionTag="-1261818424" Tag="207" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="73.0000" RightMargin="73.0000" TopMargin="287.6215" BottomMargin="90.3785" FontSize="22" LabelText="公众号：123456" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                    <Size X="154.0000" Y="22.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="150.0000" Y="101.3785" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="240" G="234" B="200" />
                    <PrePosition X="0.5000" Y="0.2534" />
                    <PreSize X="0.5133" Y="0.0550" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="34" G="34" B="34" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="139.3920" Y="244.9800" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1650" Y="0.5444" />
                <PreSize X="0.3551" Y="0.8889" />
                <FileData Type="Default" Path="Default/ImageFile.png" Plist="" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="499.2000" Y="296.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5200" Y="0.4625" />
            <PreSize X="0.8800" Y="0.7031" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="content_3" Visible="False" ActionTag="-1851893339" Tag="1538" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentWidthEnabled="True" LeftMargin="57.6000" RightMargin="57.6000" TopMargin="127.8960" BottomMargin="62.1040" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="844.8000" Y="450.0000" />
            <Children>
              <AbstractNodeData Name="sc" CanEdit="False" ActionTag="1050220738" Tag="1539" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentWidthEnabled="True" TopMargin="-10.4050" BottomMargin="35.4050" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ScrollDirectionType="0" ctype="PageViewObjectData">
                <Size X="844.8000" Y="425.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position Y="247.9050" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition Y="0.5509" />
                <PreSize X="1.0000" Y="0.9444" />
                <SingleColor A="255" R="150" G="150" B="100" />
                <FirstColor A="255" R="150" G="150" B="100" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="480.0000" Y="287.1040" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.4486" />
            <PreSize X="0.8800" Y="0.7031" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="tab_1" ActionTag="-929801019" VisibleForFrame="False" Tag="513" IconVisible="False" PositionPercentYEnabled="True" RightMargin="914.0000" TopMargin="276.7600" BottomMargin="317.2400" ctype="SpriteObjectData">
            <Size X="46.0000" Y="46.0000" />
            <AnchorPoint ScaleY="0.0600" />
            <Position Y="320.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition Y="0.5000" />
            <PreSize X="0.0479" Y="0.0719" />
            <FileData Type="Default" Path="Default/Sprite.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="tab_2" ActionTag="2072987833" VisibleForFrame="False" Tag="514" IconVisible="False" PositionPercentYEnabled="True" RightMargin="914.0000" TopMargin="317.7000" BottomMargin="276.3000" ctype="SpriteObjectData">
            <Size X="46.0000" Y="46.0000" />
            <AnchorPoint ScaleY="0.9500" />
            <Position Y="320.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition Y="0.5000" />
            <PreSize X="0.0479" Y="0.0719" />
            <FileData Type="Default" Path="Default/Sprite.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="bottom" ActionTag="1202982316" Tag="22" IconVisible="False" PositionPercentXEnabled="True" PercentWidthEnable="True" PercentWidthEnabled="True" LeftMargin="2.5920" RightMargin="-2.5920" TopMargin="551.7731" BottomMargin="5.2269" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" LeftEage="422" RightEage="422" TopEage="36" BottomEage="36" Scale9OriginX="-422" Scale9OriginY="-36" Scale9Width="844" Scale9Height="72" ctype="PanelObjectData">
            <Size X="960.0000" Y="83.0000" />
            <Children>
              <AbstractNodeData Name="bg" CanEdit="False" ActionTag="845478995" VisibleForFrame="False" Tag="208" IconVisible="False" PositionPercentXEnabled="True" PercentWidthEnable="True" PercentWidthEnabled="True" TopMargin="2.3110" BottomMargin="-0.3110" LeftEage="422" RightEage="422" TopEage="26" BottomEage="26" Scale9OriginX="-376" Scale9OriginY="20" Scale9Width="798" Scale9Height="6" ctype="ImageViewObjectData">
                <Size X="960.0000" Y="81.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="480.0000" Y="40.1890" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.4842" />
                <PreSize X="1.0000" Y="0.9759" />
                <FileData Type="Default" Path="Default/ImageFile.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="email" ActionTag="-1230257136" Tag="2041" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="632.9760" RightMargin="171.0240" TopMargin="14.4205" BottomMargin="0.5795" ctype="SpriteObjectData">
                <Size X="156.0000" Y="68.0000" />
                <Children>
                  <AbstractNodeData Name="tip" ActionTag="1881797183" Tag="659" IconVisible="True" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="80.6988" RightMargin="75.3012" TopMargin="11.3764" BottomMargin="56.6236" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
                    <Size X="0.0000" Y="0.0000" />
                    <AnchorPoint />
                    <Position X="80.6988" Y="56.6236" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5173" Y="0.8327" />
                    <PreSize X="0.0000" Y="0.0000" />
                    <FileData Type="Normal" Path="red_tip.csd" Plist="" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="710.9760" Y="34.5795" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7406" Y="0.4166" />
                <PreSize X="0.1625" Y="0.8193" />
                <FileData Type="MarkedSubImage" Path="lan/cn/bottom_email.png" Plist="lan/cn/game.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="store_tab" ActionTag="1110139909" Tag="2230" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="1.9680" RightMargin="802.0320" TopMargin="14.4205" BottomMargin="0.5795" ctype="SpriteObjectData">
                <Size X="156.0000" Y="68.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="79.9680" Y="34.5795" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0833" Y="0.4166" />
                <PreSize X="0.1625" Y="0.8193" />
                <FileData Type="MarkedSubImage" Path="lan/cn/bottom_store.png" Plist="lan/cn/game.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="Panel_baiguan" CanEdit="False" ActionTag="1210762032" VisibleForFrame="False" Tag="3977" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="25.1920" RightMargin="744.8080" TopMargin="-26.0787" BottomMargin="19.0787" ClipAble="True" BackColorAlpha="0" ComboBoxIndex="1" ColorAngle="90.0000" LeftEage="71" RightEage="71" TopEage="37" BottomEage="37" Scale9OriginX="-71" Scale9OriginY="-37" Scale9Width="142" Scale9Height="74" ctype="PanelObjectData">
                <Size X="190.0000" Y="90.0000" />
                <Children>
                  <AbstractNodeData Name="Image_2" CanEdit="False" ActionTag="-316437357" VisibleForFrame="False" Tag="3976" IconVisible="False" LeftMargin="-39.9989" RightMargin="149.9989" TopMargin="-39.5001" BottomMargin="-5.4999" LeftEage="26" RightEage="26" TopEage="44" BottomEage="44" Scale9OriginX="26" Scale9OriginY="44" Scale9Width="28" Scale9Height="47" ctype="ImageViewObjectData">
                    <Size X="80.0000" Y="135.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="0.0011" Y="62.0001" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.0000" Y="0.6889" />
                    <PreSize X="0.4211" Y="1.5000" />
                    <FileData Type="Normal" Path="hall/res/store/baiguang.png" Plist="" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="120.1920" Y="64.0787" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1252" Y="0.7720" />
                <PreSize X="0.1979" Y="1.0843" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="rank_tab" ActionTag="-193230909" Tag="2231" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="317.5200" RightMargin="486.4800" TopMargin="14.4205" BottomMargin="0.5795" ctype="SpriteObjectData">
                <Size X="156.0000" Y="68.0000" />
                <Children>
                  <AbstractNodeData Name="tip" ActionTag="-1968596703" VisibleForFrame="False" Tag="1220" IconVisible="True" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="24.9600" RightMargin="131.0400" TopMargin="20.5836" BottomMargin="47.4164" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
                    <Size X="0.0000" Y="0.0000" />
                    <AnchorPoint />
                    <Position X="24.9600" Y="47.4164" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.1600" Y="0.6973" />
                    <PreSize X="0.0000" Y="0.0000" />
                    <FileData Type="Normal" Path="red_tip.csd" Plist="" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="395.5200" Y="34.5795" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4120" Y="0.4166" />
                <PreSize X="0.1625" Y="0.8193" />
                <FileData Type="MarkedSubImage" Path="lan/cn/bottom_rank.png" Plist="lan/cn/game.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="setting" ActionTag="-1899709090" Tag="53" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="790.8000" RightMargin="13.2000" TopMargin="14.4205" BottomMargin="0.5795" ctype="SpriteObjectData">
                <Size X="156.0000" Y="68.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="868.8000" Y="34.5795" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.9050" Y="0.4166" />
                <PreSize X="0.1625" Y="0.8193" />
                <FileData Type="MarkedSubImage" Path="lan/cn/bottom_setting.png" Plist="lan/cn/game.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="bank_tab" ActionTag="1079311074" Tag="101" IconVisible="False" LeftMargin="475.2403" RightMargin="328.7597" TopMargin="14.4205" BottomMargin="0.5795" ctype="SpriteObjectData">
                <Size X="156.0000" Y="68.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="553.2403" Y="34.5795" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5763" Y="0.4166" />
                <PreSize X="0.1625" Y="0.8193" />
                <FileData Type="MarkedSubImage" Path="lan/cn/bottom_bank.png" Plist="lan/cn/game.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="invite_tab" ActionTag="-609748318" VisibleForFrame="False" Tag="102" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="537.6400" RightMargin="376.3600" TopMargin="1.6035" BottomMargin="35.3965" ctype="SpriteObjectData">
                <Size X="46.0000" Y="46.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="560.6400" Y="58.3965" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5840" Y="0.7036" />
                <PreSize X="0.0479" Y="0.5542" />
                <FileData Type="Default" Path="Default/Sprite.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="history_tab" ActionTag="-1553441176" VisibleForFrame="False" Tag="467" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="159.3040" RightMargin="610.6960" TopMargin="-25.4000" BottomMargin="8.4000" ctype="SpriteObjectData">
                <Size X="190.0000" Y="100.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="254.3040" Y="58.4000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2649" Y="0.7036" />
                <PreSize X="0.1979" Y="1.2048" />
                <FileData Type="MarkedSubImage" Path="lan/cn/bottom_score.png" Plist="lan/cn/game.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="activity_tab" ActionTag="643311484" Tag="603" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="159.6960" RightMargin="644.3040" TopMargin="14.4205" BottomMargin="0.5795" ctype="SpriteObjectData">
                <Size X="156.0000" Y="68.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="237.6960" Y="34.5795" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2476" Y="0.4166" />
                <PreSize X="0.1625" Y="0.8193" />
                <FileData Type="MarkedSubImage" Path="lan/cn/bottom_activity.png" Plist="lan/cn/game.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="rule_tab" ActionTag="-1073982910" VisibleForFrame="False" Tag="491" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="772.0720" RightMargin="-2.0720" TopMargin="-25.4000" BottomMargin="8.4000" ctype="SpriteObjectData">
                <Size X="190.0000" Y="100.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="867.0720" Y="58.4000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.9032" Y="0.7036" />
                <PreSize X="0.1979" Y="1.2048" />
                <FileData Type="MarkedSubImage" Path="lan/cn/bottom_rule.png" Plist="lan/cn/game.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" />
            <Position X="482.5920" Y="5.2269" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5027" Y="0.0082" />
            <PreSize X="1.0000" Y="0.1297" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>