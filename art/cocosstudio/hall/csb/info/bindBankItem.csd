<GameFile>
  <PropertyGroup Name="bindBankItem" Type="Layer" ID="db391931-1d10-4c39-bdc5-8dece3c02ac1" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="203" ctype="GameLayerObjectData">
        <Size X="700.0000" Y="700.0000" />
        <Children>
          <AbstractNodeData Name="city" ActionTag="310968148" Tag="216" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="237.1396" RightMargin="62.8604" TopMargin="335.1754" BottomMargin="314.8246" Scale9Enable="True" LeftEage="33" RightEage="33" TopEage="18" BottomEage="18" Scale9OriginX="33" Scale9OriginY="18" Scale9Width="433" Scale9Height="26" ctype="ImageViewObjectData">
            <Size X="400.0000" Y="50.0000" />
            <Children>
              <AbstractNodeData Name="input" ActionTag="-2049194274" Tag="217" IconVisible="False" PositionPercentYEnabled="True" PercentHeightEnable="True" PercentHeightEnabled="True" HorizontalEdge="BothEdge" LeftMargin="3.0000" RightMargin="53.0000" StretchWidthEnable="True" FontSize="30" IsCustomSize="True" LabelText="" PlaceHolderText="请选择开户市、区、县" MaxLengthText="10" ctype="TextFieldObjectData">
                <Size X="344.0000" Y="50.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="3.0000" Y="25.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0075" Y="0.5000" />
                <PreSize X="0.8600" Y="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="selector" ActionTag="-1169831708" Tag="218" IconVisible="False" PositionPercentYEnabled="True" HorizontalEdge="RightEdge" LeftMargin="351.0000" RightMargin="1.0000" ctype="SpriteObjectData">
                <Size X="48.0000" Y="50.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="375.0000" Y="25.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.9375" Y="0.5000" />
                <PreSize X="0.1200" Y="1.0000" />
                <FileData Type="MarkedSubImage" Path="hall/res/info/arrow_down.png" Plist="hall/res/myinfo.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleY="0.5000" />
            <Position X="237.1396" Y="339.8246" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3388" Y="0.4855" />
            <PreSize X="0.5714" Y="0.0714" />
            <FileData Type="Normal" Path="hall/res/info/edt_white.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="province" ActionTag="865894457" Tag="219" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="237.1396" RightMargin="62.8604" TopMargin="273.1742" BottomMargin="376.8258" Scale9Enable="True" LeftEage="33" RightEage="33" TopEage="18" BottomEage="18" Scale9OriginX="33" Scale9OriginY="18" Scale9Width="433" Scale9Height="26" ctype="ImageViewObjectData">
            <Size X="400.0000" Y="50.0000" />
            <Children>
              <AbstractNodeData Name="input" ActionTag="-992035299" Tag="220" IconVisible="False" PositionPercentYEnabled="True" PercentHeightEnable="True" PercentHeightEnabled="True" HorizontalEdge="BothEdge" LeftMargin="3.0000" RightMargin="53.0000" StretchWidthEnable="True" FontSize="30" IsCustomSize="True" LabelText="" PlaceHolderText="请选择开户省、直辖市" MaxLengthText="10" ctype="TextFieldObjectData">
                <Size X="344.0000" Y="50.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="3.0000" Y="25.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0075" Y="0.5000" />
                <PreSize X="0.8600" Y="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="selector" ActionTag="1662175721" Tag="221" IconVisible="False" PositionPercentYEnabled="True" HorizontalEdge="RightEdge" LeftMargin="351.0000" RightMargin="1.0000" ctype="SpriteObjectData">
                <Size X="48.0000" Y="50.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="375.0000" Y="25.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.9375" Y="0.5000" />
                <PreSize X="0.1200" Y="1.0000" />
                <FileData Type="MarkedSubImage" Path="hall/res/info/arrow_down.png" Plist="hall/res/myinfo.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleY="0.5000" />
            <Position X="237.1396" Y="401.8258" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3388" Y="0.5740" />
            <PreSize X="0.5714" Y="0.0714" />
            <FileData Type="Normal" Path="hall/res/info/edt_white.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Text_2" ActionTag="-1275499764" Tag="222" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="104.2294" RightMargin="469.7706" TopMargin="284.1754" BottomMargin="387.8246" FontSize="28" LabelText="开户省市:" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="126.0000" Y="28.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="167.2294" Y="401.8246" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="243" G="201" B="41" />
            <PrePosition X="0.2389" Y="0.5740" />
            <PreSize X="0.1800" Y="0.0400" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="bank" ActionTag="2011234164" Tag="223" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="237.1396" RightMargin="62.8604" TopMargin="211.1741" BottomMargin="438.8259" Scale9Enable="True" LeftEage="33" RightEage="33" TopEage="18" BottomEage="18" Scale9OriginX="33" Scale9OriginY="18" Scale9Width="433" Scale9Height="26" ctype="ImageViewObjectData">
            <Size X="400.0000" Y="50.0000" />
            <Children>
              <AbstractNodeData Name="input" ActionTag="782335305" Tag="224" IconVisible="False" PositionPercentYEnabled="True" PercentHeightEnable="True" PercentHeightEnabled="True" HorizontalEdge="BothEdge" LeftMargin="3.0000" RightMargin="53.0000" StretchWidthEnable="True" FontSize="30" IsCustomSize="True" LabelText="" PlaceHolderText="" MaxLengthText="10" ctype="TextFieldObjectData">
                <Size X="344.0000" Y="50.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="3.0000" Y="25.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0075" Y="0.5000" />
                <PreSize X="0.8600" Y="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="selector" ActionTag="812685089" Tag="225" IconVisible="False" PositionPercentYEnabled="True" HorizontalEdge="RightEdge" LeftMargin="351.0000" RightMargin="1.0000" ctype="SpriteObjectData">
                <Size X="48.0000" Y="50.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="375.0000" Y="25.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.9375" Y="0.5000" />
                <PreSize X="0.1200" Y="1.0000" />
                <FileData Type="MarkedSubImage" Path="hall/res/info/arrow_down.png" Plist="hall/res/myinfo.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleY="0.5000" />
            <Position X="237.1396" Y="463.8259" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3388" Y="0.6626" />
            <PreSize X="0.5714" Y="0.0714" />
            <FileData Type="Normal" Path="hall/res/info/edt_white.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Text_1" ActionTag="462070564" Tag="226" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="104.2294" RightMargin="469.7706" TopMargin="222.1761" BottomMargin="449.8239" FontSize="28" LabelText="选择银行:" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="126.0000" Y="28.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="167.2294" Y="463.8239" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="243" G="201" B="41" />
            <PrePosition X="0.2389" Y="0.6626" />
            <PreSize X="0.1800" Y="0.0400" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Text_27" ActionTag="44366303" Tag="227" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="141.1772" RightMargin="208.8228" TopMargin="175.0128" BottomMargin="499.9872" FontSize="25" LabelText="开户姓名必须与银行卡姓名一致" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="350.0000" Y="25.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="141.1772" Y="512.4872" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.2017" Y="0.7321" />
            <PreSize X="0.5000" Y="0.0357" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Sprite_2" ActionTag="1971713873" Tag="228" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="102.1134" RightMargin="572.8866" TopMargin="174.5128" BottomMargin="499.4872" ctype="SpriteObjectData">
            <Size X="25.0000" Y="26.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="114.6134" Y="512.4872" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.1637" Y="0.7321" />
            <PreSize X="0.0357" Y="0.0371" />
            <FileData Type="Normal" Path="hall/res/info/decorate.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="name" ActionTag="779683441" Tag="229" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="237.1396" RightMargin="62.8604" TopMargin="110.6440" BottomMargin="539.3560" Scale9Enable="True" LeftEage="33" RightEage="33" TopEage="18" BottomEage="18" Scale9OriginX="33" Scale9OriginY="18" Scale9Width="433" Scale9Height="26" ctype="ImageViewObjectData">
            <Size X="400.0000" Y="50.0000" />
            <Children>
              <AbstractNodeData Name="input" ActionTag="-1575859399" Tag="230" IconVisible="False" PositionPercentYEnabled="True" PercentHeightEnable="True" PercentHeightEnabled="True" HorizontalEdge="BothEdge" LeftMargin="3.0000" RightMargin="3.0000" TouchEnable="True" StretchWidthEnable="True" FontSize="30" IsCustomSize="True" LabelText="" PlaceHolderText="" MaxLengthText="10" ctype="TextFieldObjectData">
                <Size X="394.0000" Y="50.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="3.0000" Y="25.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0075" Y="0.5000" />
                <PreSize X="0.9850" Y="1.0000" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleY="0.5000" />
            <Position X="237.1396" Y="564.3560" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3388" Y="0.8062" />
            <PreSize X="0.5714" Y="0.0714" />
            <FileData Type="Normal" Path="hall/res/info/edt_white.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Text_10" ActionTag="1081568150" Tag="231" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="104.2294" RightMargin="469.7706" TopMargin="121.6463" BottomMargin="550.3537" FontSize="28" LabelText="开户姓名:" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="126.0000" Y="28.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="167.2294" Y="564.3537" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="243" G="201" B="41" />
            <PrePosition X="0.2389" Y="0.8062" />
            <PreSize X="0.1800" Y="0.0400" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="tip" ActionTag="1808003156" Tag="204" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="118.0100" RightMargin="581.9900" TopMargin="605.9849" BottomMargin="94.0151" FontSize="20" LabelText="" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="118.0100" Y="94.0151" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="0" B="0" />
            <PrePosition X="0.1686" Y="0.1343" />
            <PreSize X="0.0000" Y="0.0000" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="confirmBtn" ActionTag="1953111669" Tag="205" IconVisible="False" VerticalEdge="BottomEdge" LeftMargin="212.5000" RightMargin="212.5000" TopMargin="603.1402" BottomMargin="1.8598" ctype="SpriteObjectData">
            <Size X="275.0000" Y="95.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="350.0000" Y="49.3598" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.0705" />
            <PreSize X="0.3929" Y="0.1357" />
            <FileData Type="Normal" Path="hall/res/info/ok.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="payPwd" ActionTag="641920890" Tag="206" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="237.1390" RightMargin="62.8610" TopMargin="535.1753" BottomMargin="114.8247" Scale9Enable="True" LeftEage="33" RightEage="33" TopEage="18" BottomEage="18" Scale9OriginX="33" Scale9OriginY="18" Scale9Width="433" Scale9Height="26" ctype="ImageViewObjectData">
            <Size X="400.0000" Y="50.0000" />
            <Children>
              <AbstractNodeData Name="input" ActionTag="-895002005" Tag="207" IconVisible="False" PositionPercentYEnabled="True" PercentHeightEnable="True" PercentHeightEnabled="True" HorizontalEdge="BothEdge" LeftMargin="3.0000" RightMargin="151.0000" TouchEnable="True" StretchWidthEnable="True" FontSize="30" IsCustomSize="True" LabelText="" PlaceHolderText="" MaxLengthEnable="True" MaxLengthText="6" PasswordEnable="True" ctype="TextFieldObjectData">
                <Size X="246.0000" Y="50.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="3.0000" Y="25.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0075" Y="0.5000" />
                <PreSize X="0.6150" Y="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="forgetBtn" ActionTag="920297804" Tag="208" IconVisible="False" PositionPercentYEnabled="True" HorizontalEdge="RightEdge" LeftMargin="251.4859" RightMargin="-0.4859" TopMargin="2.0000" BottomMargin="-4.0000" ctype="SpriteObjectData">
                <Size X="149.0000" Y="52.0000" />
                <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
                <Position X="400.4859" Y="22.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="1.0012" Y="0.4400" />
                <PreSize X="0.3725" Y="1.0400" />
                <FileData Type="Normal" Path="hall/res/info/forgetPwdBtn.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleY="0.5000" />
            <Position X="237.1390" Y="139.8247" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3388" Y="0.1997" />
            <PreSize X="0.5714" Y="0.0714" />
            <FileData Type="Normal" Path="hall/res/info/edt_white.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Text_5" ActionTag="1478668500" Tag="209" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="104.2294" RightMargin="469.7706" TopMargin="546.1744" BottomMargin="125.8256" FontSize="28" LabelText="支付密码:" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="126.0000" Y="28.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="167.2294" Y="139.8256" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="243" G="201" B="41" />
            <PrePosition X="0.2389" Y="0.1998" />
            <PreSize X="0.1800" Y="0.0400" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="bankCardNo" ActionTag="-367303452" Tag="210" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="237.1396" RightMargin="62.8604" TopMargin="468.1752" BottomMargin="181.8248" Scale9Enable="True" LeftEage="33" RightEage="33" TopEage="18" BottomEage="18" Scale9OriginX="33" Scale9OriginY="18" Scale9Width="433" Scale9Height="26" ctype="ImageViewObjectData">
            <Size X="400.0000" Y="50.0000" />
            <Children>
              <AbstractNodeData Name="input" ActionTag="-1990502828" Tag="211" IconVisible="False" PositionPercentYEnabled="True" PercentHeightEnable="True" PercentHeightEnabled="True" HorizontalEdge="BothEdge" LeftMargin="3.0000" RightMargin="3.0000" TouchEnable="True" StretchWidthEnable="True" FontSize="30" IsCustomSize="True" LabelText="" PlaceHolderText="" MaxLengthText="10" ctype="TextFieldObjectData">
                <Size X="394.0000" Y="50.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="3.0000" Y="25.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0075" Y="0.5000" />
                <PreSize X="0.9850" Y="1.0000" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleY="0.5000" />
            <Position X="237.1396" Y="206.8248" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3388" Y="0.2955" />
            <PreSize X="0.5714" Y="0.0714" />
            <FileData Type="Normal" Path="hall/res/info/edt_white.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Text_4" ActionTag="1126923397" Tag="212" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="104.2294" RightMargin="469.7706" TopMargin="479.1746" BottomMargin="192.8254" FontSize="28" LabelText="银行卡号:" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="126.0000" Y="28.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="167.2294" Y="206.8254" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="243" G="201" B="41" />
            <PrePosition X="0.2389" Y="0.2955" />
            <PreSize X="0.1800" Y="0.0400" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="bankSite" ActionTag="-15926705" Tag="213" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="237.1396" RightMargin="62.8604" TopMargin="401.1743" BottomMargin="248.8257" Scale9Enable="True" LeftEage="33" RightEage="33" TopEage="18" BottomEage="18" Scale9OriginX="33" Scale9OriginY="18" Scale9Width="433" Scale9Height="26" ctype="ImageViewObjectData">
            <Size X="400.0000" Y="50.0000" />
            <Children>
              <AbstractNodeData Name="input" ActionTag="-735645276" Tag="214" IconVisible="False" PositionPercentYEnabled="True" HorizontalEdge="BothEdge" LeftMargin="3.0000" RightMargin="3.0000" TouchEnable="True" StretchWidthEnable="True" FontSize="30" IsCustomSize="True" LabelText="" PlaceHolderText="请输入开户银行名称" MaxLengthText="10" ctype="TextFieldObjectData">
                <Size X="394.0000" Y="50.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="3.0000" Y="25.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0075" Y="0.5000" />
                <PreSize X="0.9850" Y="1.0000" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleY="0.5000" />
            <Position X="237.1396" Y="273.8257" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3388" Y="0.3912" />
            <PreSize X="0.5714" Y="0.0714" />
            <FileData Type="Normal" Path="hall/res/info/edt_white.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Text_3" ActionTag="620800098" Tag="215" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="104.2294" RightMargin="469.7706" TopMargin="412.1756" BottomMargin="259.8244" FontSize="28" LabelText="开户银行:" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="126.0000" Y="28.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="167.2294" Y="273.8244" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="243" G="201" B="41" />
            <PrePosition X="0.2389" Y="0.3912" />
            <PreSize X="0.1800" Y="0.0400" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>