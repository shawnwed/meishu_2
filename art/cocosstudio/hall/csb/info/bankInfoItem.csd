<GameFile>
  <PropertyGroup Name="bankInfoItem" Type="Node" ID="aa996d0b-f268-4a17-9380-089c7546b3f1" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="31" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="bg" ActionTag="250256215" Tag="32" IconVisible="False" RightMargin="-600.0000" TopMargin="-60.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" ctype="PanelObjectData">
            <Size X="600.0000" Y="60.0000" />
            <Children>
              <AbstractNodeData Name="bankName" ActionTag="-1698323299" Tag="33" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="115.9999" RightMargin="349.0001" TopMargin="15.0000" BottomMargin="15.0000" FontSize="30" LabelText="招商银行:" HorizontalAlignmentType="HT_Right" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="135.0000" Y="30.0000" />
                <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
                <Position X="250.9999" Y="30.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="243" G="201" B="41" />
                <PrePosition X="0.4183" Y="0.5000" />
                <PreSize X="0.2250" Y="0.5000" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="cardNo" ActionTag="-448979936" Tag="37" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="258.4637" RightMargin="86.5363" TopMargin="15.0000" BottomMargin="15.0000" FontSize="30" LabelText="12345678912144555" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="255.0000" Y="30.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="258.4637" Y="30.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4308" Y="0.5000" />
                <PreSize X="0.4250" Y="0.5000" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>