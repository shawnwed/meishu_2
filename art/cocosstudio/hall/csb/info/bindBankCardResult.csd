<GameFile>
  <PropertyGroup Name="bindBankCardResult" Type="Layer" ID="0ab14265-bca6-474e-9c85-64eadff39637" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="858" ctype="GameLayerObjectData">
        <Size X="1280.0000" Y="720.0000" />
        <Children>
          <AbstractNodeData Name="mask" CanEdit="False" ActionTag="-581453933" Tag="859" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" TouchEnable="True" ClipAble="False" BackColorAlpha="177" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="1280.0000" Y="720.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="640.0000" Y="360.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="1.0000" Y="1.0000" />
            <SingleColor A="255" R="0" G="0" B="0" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="bg" ActionTag="633494839" Tag="892" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="290.0000" RightMargin="290.0000" TopMargin="10.0000" BottomMargin="10.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="0" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Enable="True" LeftEage="238" RightEage="220" TopEage="138" BottomEage="86" Scale9OriginX="238" Scale9OriginY="138" Scale9Width="457" Scale9Height="352" ctype="PanelObjectData">
            <Size X="700.0000" Y="700.0000" />
            <Children>
              <AbstractNodeData Name="title" ActionTag="-821728694" Tag="893" IconVisible="False" PositionPercentXEnabled="True" VerticalEdge="TopEdge" LeftMargin="267.2100" RightMargin="259.7900" TopMargin="32.4587" BottomMargin="630.5413" ctype="SpriteObjectData">
                <Size X="173.0000" Y="37.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="353.7100" Y="649.0413" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5053" Y="0.9272" />
                <PreSize X="0.2471" Y="0.0529" />
                <FileData Type="MarkedSubImage" Path="hall/res/info/mimaguanlibiaoti4.png" Plist="hall/res/info/bindBank.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_9" ActionTag="-595638056" Tag="894" IconVisible="False" PositionPercentXEnabled="True" VerticalEdge="TopEdge" LeftMargin="193.5000" RightMargin="193.5000" TopMargin="99.7163" BottomMargin="575.2837" FontSize="25" LabelText="同一姓名最多绑定5张银行卡" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="313.0000" Y="25.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="350.0000" Y="587.7837" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="0" B="0" />
                <PrePosition X="0.5000" Y="0.8397" />
                <PreSize X="0.4471" Y="0.0357" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="bindCardBtn" ActionTag="-2100744840" Tag="983" IconVisible="False" PositionPercentXEnabled="True" VerticalEdge="BottomEdge" LeftMargin="72.5000" RightMargin="352.5000" TopMargin="562.8800" BottomMargin="42.1200" ctype="SpriteObjectData">
                <Size X="275.0000" Y="95.0000" />
                <Children>
                  <AbstractNodeData Name="Text_12" ActionTag="555870763" Tag="29" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="66.1050" RightMargin="68.8950" TopMargin="24.2825" BottomMargin="35.7175" FontSize="35" LabelText="继续绑卡" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="140.0000" Y="35.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="136.1050" Y="53.2175" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.4949" Y="0.5602" />
                    <PreSize X="0.5091" Y="0.3684" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="210.0000" Y="89.6200" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3000" Y="0.1280" />
                <PreSize X="0.3929" Y="0.1357" />
                <FileData Type="Normal" Path="hall/res/info/btn_left.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="accumulatePointBtn" ActionTag="-80760386" Tag="984" IconVisible="False" PositionPercentXEnabled="True" VerticalEdge="BottomEdge" LeftMargin="352.5000" RightMargin="72.5000" TopMargin="562.8800" BottomMargin="42.1200" ctype="SpriteObjectData">
                <Size X="275.0000" Y="95.0000" />
                <Children>
                  <AbstractNodeData Name="Text_13" ActionTag="-569432191" Tag="30" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="70.5251" RightMargin="64.4749" TopMargin="24.2825" BottomMargin="35.7175" FontSize="35" LabelText="筹码兑换" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="140.0000" Y="35.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="140.5251" Y="53.2175" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5110" Y="0.5602" />
                    <PreSize X="0.5091" Y="0.3684" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="490.0000" Y="89.6200" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7000" Y="0.1280" />
                <PreSize X="0.3929" Y="0.1357" />
                <FileData Type="Normal" Path="hall/res/info/btn_left.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="containor" ActionTag="1931437185" Tag="985" IconVisible="False" PositionPercentXEnabled="True" VerticalEdge="TopEdge" LeftMargin="50.0000" RightMargin="50.0000" TopMargin="140.9481" BottomMargin="159.0519" TouchEnable="True" ClipAble="True" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="600.0000" Y="400.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="1.0000" />
                <Position X="350.0000" Y="559.0519" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.7986" />
                <PreSize X="0.8571" Y="0.5714" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="closeBtn" ActionTag="4088068" Tag="191" IconVisible="False" HorizontalEdge="RightEdge" VerticalEdge="TopEdge" LeftMargin="632.0107" RightMargin="-12.0107" TopMargin="-11.0912" BottomMargin="630.0912" ctype="SpriteObjectData">
                <Size X="80.0000" Y="81.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="672.0107" Y="670.5912" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.9600" Y="0.9580" />
                <PreSize X="0.1143" Y="0.1157" />
                <FileData Type="Normal" Path="hall/res/info/x.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="640.0000" Y="360.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="0.5469" Y="0.9722" />
            <FileData Type="Normal" Path="hall/res/bingphone/bg.png" Plist="" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>