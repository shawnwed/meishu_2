<GameFile>
  <PropertyGroup Name="gameRecordItem" Type="Node" ID="abc04b2e-c6b6-4f3e-83c7-6899ee081bdf" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="210" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="containor" ActionTag="-1367611316" Tag="211" IconVisible="False" PositionPercentYEnabled="True" RightMargin="-858.0000" TopMargin="-80.0000" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="858.0000" Y="80.0000" />
            <Children>
              <AbstractNodeData Name="Text_1" ActionTag="-1374279949" Tag="212" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="28.9146" RightMargin="699.0854" TopMargin="27.5000" BottomMargin="27.5000" FontSize="25" LabelText="Text Label" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="130.0000" Y="25.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="28.9146" Y="40.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="149" G="104" B="74" />
                <PrePosition X="0.0337" Y="0.5000" />
                <PreSize X="0.1515" Y="0.3125" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_2" ActionTag="-1788479902" Tag="213" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="187.7668" RightMargin="540.2332" TopMargin="27.5000" BottomMargin="27.5000" FontSize="25" LabelText="Text Label" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="130.0000" Y="25.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="252.7668" Y="40.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="149" G="104" B="74" />
                <PrePosition X="0.2946" Y="0.5000" />
                <PreSize X="0.1515" Y="0.3125" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_3" ActionTag="-2144023170" Tag="214" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="374.5534" RightMargin="353.4466" TopMargin="27.5000" BottomMargin="27.5000" FontSize="25" LabelText="Text Label" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="130.0000" Y="25.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="439.5534" Y="40.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="149" G="104" B="74" />
                <PrePosition X="0.5123" Y="0.5000" />
                <PreSize X="0.1515" Y="0.3125" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_4" ActionTag="1599035890" Tag="215" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="549.3280" RightMargin="178.6720" TopMargin="27.5000" BottomMargin="27.5000" FontSize="25" LabelText="Text Label" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="130.0000" Y="25.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="614.3280" Y="40.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="149" G="104" B="74" />
                <PrePosition X="0.7160" Y="0.5000" />
                <PreSize X="0.1515" Y="0.3125" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_5" ActionTag="-801437039" Tag="216" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="705.5698" RightMargin="22.4302" TopMargin="27.5000" BottomMargin="27.5000" FontSize="25" LabelText="Text Label" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="130.0000" Y="25.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="770.5698" Y="40.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="149" G="104" B="74" />
                <PrePosition X="0.8981" Y="0.5000" />
                <PreSize X="0.1515" Y="0.3125" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="line" ActionTag="-1689964611" Tag="217" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" TopMargin="77.0000" ctype="SpriteObjectData">
                <Size X="858.0000" Y="3.0000" />
                <AnchorPoint ScaleX="0.5000" />
                <Position X="429.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" />
                <PreSize X="1.0000" Y="0.0375" />
                <FileData Type="Normal" Path="hall/res/info/line.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_7" ActionTag="157722025" Tag="30" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="329.0000" RightMargin="329.0000" TopMargin="-60.0000" BottomMargin="80.0000" IsCustomSize="True" FontSize="25" LabelText="下拉刷新..." HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="200.0000" Y="60.0000" />
                <AnchorPoint ScaleX="0.5000" />
                <Position X="429.0000" Y="80.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="1.0000" />
                <PreSize X="0.2331" Y="0.7500" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_6" ActionTag="1196855612" Tag="29" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="329.0000" RightMargin="329.0000" TopMargin="80.0000" BottomMargin="-60.0000" IsCustomSize="True" FontSize="25" LabelText="上拉加载更多..." HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="200.0000" Y="60.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="1.0000" />
                <Position X="429.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" />
                <PreSize X="0.2331" Y="0.7500" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" />
            <Position X="429.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <SingleColor A="255" R="0" G="128" B="0" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>