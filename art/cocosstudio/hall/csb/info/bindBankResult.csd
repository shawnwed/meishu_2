<GameFile>
  <PropertyGroup Name="bindBankResult" Type="Layer" ID="462ab43f-7257-4311-9fbf-d95f64727cc0" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="261" ctype="GameLayerObjectData">
        <Size X="700.0000" Y="700.0000" />
        <Children>
          <AbstractNodeData Name="Text_9" ActionTag="551324432" Tag="267" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="193.4998" RightMargin="193.5002" TopMargin="153.4354" BottomMargin="521.5646" FontSize="25" LabelText="同一姓名最多绑定5张银行卡" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="313.0000" Y="25.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="349.9998" Y="534.0646" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="0" B="0" />
            <PrePosition X="0.5000" Y="0.7629" />
            <PreSize X="0.4471" Y="0.0357" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="bindCardBtn" ActionTag="-263055828" Tag="265" IconVisible="False" VerticalEdge="BottomEdge" LeftMargin="72.5000" RightMargin="352.5000" TopMargin="588.8801" BottomMargin="16.1199" ctype="SpriteObjectData">
            <Size X="275.0000" Y="95.0000" />
            <Children>
              <AbstractNodeData Name="Text_12" ActionTag="-1064251604" Tag="266" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="66.7652" RightMargin="68.2348" TopMargin="24.5832" BottomMargin="35.4168" FontSize="35" LabelText="继续绑卡" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="140.0000" Y="35.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="136.7652" Y="52.9168" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4973" Y="0.5570" />
                <PreSize X="0.5091" Y="0.3684" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="210.0000" Y="63.6199" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3000" Y="0.0909" />
            <PreSize X="0.3929" Y="0.1357" />
            <FileData Type="Normal" Path="hall/res/info/btn_left.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="accumulatePointBtn" ActionTag="1742935180" Tag="263" IconVisible="False" VerticalEdge="BottomEdge" LeftMargin="352.5000" RightMargin="72.5000" TopMargin="588.8801" BottomMargin="16.1199" ctype="SpriteObjectData">
            <Size X="275.0000" Y="95.0000" />
            <Children>
              <AbstractNodeData Name="Text_13" ActionTag="-117798116" Tag="264" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="69.1847" RightMargin="65.8153" TopMargin="24.5832" BottomMargin="35.4168" FontSize="35" LabelText="筹码兑换" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="140.0000" Y="35.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="139.1847" Y="52.9168" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5061" Y="0.5570" />
                <PreSize X="0.5091" Y="0.3684" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="490.0000" Y="63.6199" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.7000" Y="0.0909" />
            <PreSize X="0.3929" Y="0.1357" />
            <FileData Type="Normal" Path="hall/res/info/btn_left.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="containor" ActionTag="1063959513" Tag="262" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="50.0000" RightMargin="50.0000" TopMargin="197.6681" BottomMargin="102.3319" TouchEnable="True" ClipAble="True" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="600.0000" Y="400.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="1.0000" />
            <Position X="350.0000" Y="502.3319" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.7176" />
            <PreSize X="0.8571" Y="0.5714" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>