<GameFile>
  <PropertyGroup Name="pwdManage" Type="Layer" ID="758a96b2-f43f-4aed-b852-85fee9963894" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="968" ctype="GameLayerObjectData">
        <Size X="1280.0000" Y="720.0000" />
        <Children>
          <AbstractNodeData Name="mask" CanEdit="False" ActionTag="282365721" Tag="969" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" ClipAble="False" BackColorAlpha="177" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="1280.0000" Y="720.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="640.0000" Y="360.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="1.0000" Y="1.0000" />
            <SingleColor A="255" R="0" G="0" B="0" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="bg" ActionTag="-1258852952" Tag="970" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="341.4080" RightMargin="338.5920" TopMargin="86.4400" BottomMargin="83.5600" TouchEnable="True" ClipAble="False" BackColorAlpha="0" ColorAngle="90.0000" Scale9Enable="True" LeftEage="146" RightEage="128" TopEage="144" BottomEage="156" Scale9OriginX="146" Scale9OriginY="144" Scale9Width="641" Scale9Height="276" ctype="PanelObjectData">
            <Size X="600.0000" Y="550.0000" />
            <Children>
              <AbstractNodeData Name="title" ActionTag="-775280940" Tag="973" IconVisible="False" PositionPercentXEnabled="True" VerticalEdge="TopEdge" LeftMargin="214.5000" RightMargin="214.5000" TopMargin="14.5700" BottomMargin="483.4300" ctype="SpriteObjectData">
                <Size X="171.0000" Y="52.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="300.0000" Y="509.4300" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.9262" />
                <PreSize X="0.2850" Y="0.0945" />
                <FileData Type="Normal" Path="hall/res/info/mimaguanlibiaoti.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="Sprite_1" ActionTag="1105838888" Tag="972" IconVisible="False" PositionPercentXEnabled="True" VerticalEdge="TopEdge" LeftMargin="65.1146" RightMargin="61.8854" TopMargin="108.9938" BottomMargin="373.0062" ctype="SpriteObjectData">
                <Size X="473.0000" Y="68.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="301.6146" Y="407.0062" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5027" Y="0.7400" />
                <PreSize X="0.7883" Y="0.1236" />
                <FileData Type="Normal" Path="hall/res/info/zhaohuizhifumima.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="Sprite_2" ActionTag="1601256047" Tag="974" IconVisible="False" PositionPercentXEnabled="True" VerticalEdge="TopEdge" LeftMargin="61.8862" RightMargin="65.1138" TopMargin="194.4907" BottomMargin="287.5093" ctype="SpriteObjectData">
                <Size X="473.0000" Y="68.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="298.3862" Y="321.5093" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4973" Y="0.5846" />
                <PreSize X="0.7883" Y="0.1236" />
                <FileData Type="Normal" Path="hall/res/info/zhaohuidenglumima.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="Sprite_3" ActionTag="1581586607" Tag="975" IconVisible="False" PositionPercentXEnabled="True" VerticalEdge="TopEdge" LeftMargin="65.1143" RightMargin="61.8857" TopMargin="282.7565" BottomMargin="199.2435" ctype="SpriteObjectData">
                <Size X="473.0000" Y="68.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="301.6143" Y="233.2435" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5027" Y="0.4241" />
                <PreSize X="0.7883" Y="0.1236" />
                <FileData Type="Normal" Path="hall/res/info/xiugaizhifumima.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="Sprite_4" ActionTag="-1774846781" Tag="976" IconVisible="False" PositionPercentXEnabled="True" VerticalEdge="TopEdge" LeftMargin="63.5002" RightMargin="63.4998" TopMargin="375.7357" BottomMargin="106.2643" ctype="SpriteObjectData">
                <Size X="473.0000" Y="68.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="300.0002" Y="140.2643" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.2550" />
                <PreSize X="0.7883" Y="0.1236" />
                <FileData Type="Normal" Path="hall/res/info/xiugaidenglumima.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_1" ActionTag="1259338086" Tag="9" IconVisible="False" PositionPercentXEnabled="True" VerticalEdge="BottomEdge" LeftMargin="28.0480" RightMargin="-3.0480" TopMargin="478.1299" BottomMargin="46.8701" FontSize="25" LabelText="如果以上方法均不能帮您找回密码，请联系在线客服" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="575.0000" Y="25.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="315.5480" Y="59.3701" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="0" B="0" />
                <PrePosition X="0.5259" Y="0.1079" />
                <PreSize X="0.9583" Y="0.0455" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="closeBtn" ActionTag="-1814885156" Tag="185" IconVisible="False" HorizontalEdge="RightEdge" VerticalEdge="TopEdge" LeftMargin="538.1423" RightMargin="-18.1423" TopMargin="-20.1436" BottomMargin="489.1436" ctype="SpriteObjectData">
                <Size X="80.0000" Y="81.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="578.1423" Y="529.6436" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.9636" Y="0.9630" />
                <PreSize X="0.1333" Y="0.1473" />
                <FileData Type="Normal" Path="hall/res/info/x.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="641.4080" Y="358.5600" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5011" Y="0.4980" />
            <PreSize X="0.4688" Y="0.7639" />
            <FileData Type="Normal" Path="hall/res/bingphone/bg.png" Plist="" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>