<GameFile>
  <PropertyGroup Name="item_1" Type="Node" ID="3ee12432-abc2-4fcf-b6e9-60305dcaf89a" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="229" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="bg" ActionTag="-67057964" Tag="241" IconVisible="False" LeftMargin="34.5000" RightMargin="-785.5000" TopMargin="-59.5000" BottomMargin="4.5000" ctype="SpriteObjectData">
            <Size X="751.0000" Y="55.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="410.0000" Y="32.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="hall/res/info/bg3.png" Plist="hall/res/myinfo.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="load_more_txt" ActionTag="975629819" Tag="234" IconVisible="False" LeftMargin="305.0000" RightMargin="-515.0000" TopMargin="27.0000" BottomMargin="-55.0000" FontSize="28" LabelText="上拉下載更多..." ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="210.0000" Y="28.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="410.0000" Y="-41.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="165" G="42" B="42" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="refresh" ActionTag="-1665046387" Tag="235" IconVisible="False" LeftMargin="354.0000" RightMargin="-466.0000" TopMargin="-127.0000" BottomMargin="99.0000" FontSize="28" LabelText="下拉刷新" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="112.0000" Y="28.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="410.0000" Y="113.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="165" G="42" B="42" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="room" ActionTag="626931174" Tag="236" IconVisible="False" LeftMargin="642.3971" RightMargin="-759.3971" TopMargin="-45.0000" BottomMargin="19.0000" FontSize="26" LabelText="122222222" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="117.0000" Y="26.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="700.8971" Y="32.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="64" G="30" B="0" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="fee" ActionTag="392727951" Tag="237" IconVisible="False" LeftMargin="513.7500" RightMargin="-630.7500" TopMargin="-45.0000" BottomMargin="19.0000" FontSize="26" LabelText="122222222" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="117.0000" Y="26.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="572.2500" Y="32.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="64" G="30" B="0" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="win" ActionTag="-1862684365" Tag="238" IconVisible="False" LeftMargin="373.0459" RightMargin="-490.0459" TopMargin="-45.8971" BottomMargin="19.8971" FontSize="26" LabelText="122222222" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="117.0000" Y="26.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="431.5459" Y="32.8971" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="64" G="30" B="0" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="take" ActionTag="844119007" Tag="239" IconVisible="False" LeftMargin="220.6787" RightMargin="-337.6787" TopMargin="-45.0000" BottomMargin="19.0000" FontSize="26" LabelText="122222222" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="117.0000" Y="26.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="279.1787" Y="32.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="64" G="30" B="0" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="time" ActionTag="-115992987" Tag="240" IconVisible="False" LeftMargin="48.6202" RightMargin="-191.6202" TopMargin="-46.7943" BottomMargin="20.7943" FontSize="26" LabelText="03-08 12:45" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="143.0000" Y="26.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="120.1202" Y="33.7943" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="64" G="30" B="0" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>