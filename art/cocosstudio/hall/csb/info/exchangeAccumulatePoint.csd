<GameFile>
  <PropertyGroup Name="exchangeAccumulatePoint" Type="Layer" ID="2c015a38-8f6f-4a09-95a6-176a4b143888" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="997" ctype="GameLayerObjectData">
        <Size X="1280.0000" Y="720.0000" />
        <Children>
          <AbstractNodeData Name="mask" CanEdit="False" ActionTag="1441272825" Tag="998" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" ClipAble="True" BackColorAlpha="179" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="1280.0000" Y="720.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="640.0000" Y="360.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="1.0000" Y="1.0000" />
            <SingleColor A="255" R="0" G="0" B="0" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="bg" CanEdit="False" ActionTag="523264665" Tag="999" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="290.0000" RightMargin="290.0000" TopMargin="10.0000" BottomMargin="10.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="0" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Enable="True" LeftEage="49" RightEage="49" TopEage="49" BottomEage="49" Scale9OriginX="49" Scale9OriginY="49" Scale9Width="52" Scale9Height="52" ctype="PanelObjectData">
            <Size X="700.0000" Y="700.0000" />
            <Children>
              <AbstractNodeData Name="title" ActionTag="-2076872130" Tag="1000" IconVisible="False" PositionPercentXEnabled="True" VerticalEdge="TopEdge" LeftMargin="288.5000" RightMargin="288.5000" TopMargin="18.1700" BottomMargin="629.8300" ctype="SpriteObjectData">
                <Size X="123.0000" Y="52.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="350.0000" Y="655.8300" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.9369" />
                <PreSize X="0.1757" Y="0.0743" />
                <FileData Type="Normal" Path="hall/res/info/jifen.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_1" ActionTag="-719958001" Tag="1001" IconVisible="False" PositionPercentXEnabled="True" VerticalEdge="TopEdge" LeftMargin="15.0000" RightMargin="15.0000" TopMargin="84.9140" BottomMargin="595.0860" FontSize="20" LabelText="兑换所产生的银行手续费由平台为您免除， 兑换限额:最低100，最高100000" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="670.0000" Y="20.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="350.0000" Y="605.0860" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="37" G="179" B="37" />
                <PrePosition X="0.5000" Y="0.8644" />
                <PreSize X="0.9571" Y="0.0286" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_2" ActionTag="-1562290579" Tag="1002" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="113.3000" RightMargin="460.7000" TopMargin="152.7400" BottomMargin="519.2600" FontSize="28" LabelText="当前积分:" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="126.0000" Y="28.0000" />
                <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
                <Position X="239.3000" Y="533.2600" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="243" G="201" B="41" />
                <PrePosition X="0.3419" Y="0.7618" />
                <PreSize X="0.1800" Y="0.0400" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="currentPoints" ActionTag="-670597700" Tag="1035" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="252.8500" RightMargin="432.1500" TopMargin="150.4474" BottomMargin="519.5527" FontSize="30" LabelText="0" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="15.0000" Y="30.0000" />
                <AnchorPoint ScaleY="0.4569" />
                <Position X="252.8500" Y="533.2596" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3612" Y="0.7618" />
                <PreSize X="0.0214" Y="0.0429" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_4" ActionTag="-1891848230" Tag="1036" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="85.3000" RightMargin="460.7000" TopMargin="209.4900" BottomMargin="462.5100" FontSize="28" LabelText="可兑换积分:" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="154.0000" Y="28.0000" />
                <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
                <Position X="239.3000" Y="476.5100" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="243" G="201" B="41" />
                <PrePosition X="0.3419" Y="0.6807" />
                <PreSize X="0.2200" Y="0.0400" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="exchangedPoints" ActionTag="1578423020" Tag="1037" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="252.8500" RightMargin="432.1500" TopMargin="208.4900" BottomMargin="461.5100" FontSize="30" LabelText="0" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="15.0000" Y="30.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="252.8500" Y="476.5100" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3612" Y="0.6807" />
                <PreSize X="0.0214" Y="0.0429" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_6" ActionTag="-692682036" Tag="1038" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="85.3000" RightMargin="460.7000" TopMargin="273.5170" BottomMargin="398.4830" FontSize="28" LabelText="选择银行卡:" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="154.0000" Y="28.0000" />
                <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
                <Position X="239.3000" Y="412.4830" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="243" G="201" B="41" />
                <PrePosition X="0.3419" Y="0.5893" />
                <PreSize X="0.2200" Y="0.0400" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="bankCard" ActionTag="-1074694627" Tag="1039" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="252.8500" RightMargin="47.1500" TopMargin="257.2419" BottomMargin="392.7581" Scale9Enable="True" LeftEage="33" RightEage="33" TopEage="18" BottomEage="18" Scale9OriginX="33" Scale9OriginY="18" Scale9Width="214" Scale9Height="18" ctype="ImageViewObjectData">
                <Size X="400.0000" Y="50.0000" />
                <Children>
                  <AbstractNodeData Name="bankCard" ActionTag="326832989" Tag="1040" IconVisible="False" PositionPercentYEnabled="True" PercentHeightEnable="True" PercentHeightEnabled="True" HorizontalEdge="BothEdge" LeftMargin="3.0000" RightMargin="53.0000" StretchWidthEnable="True" IsCustomSize="True" FontSize="30" LabelText="" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="344.0000" Y="50.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="3.0000" Y="25.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.0075" Y="0.5000" />
                    <PreSize X="0.8600" Y="1.0000" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="selector" ActionTag="1408776895" Tag="1041" IconVisible="False" PositionPercentYEnabled="True" HorizontalEdge="RightEdge" LeftMargin="351.4600" RightMargin="0.5400" TopMargin="1.0000" BottomMargin="-1.0000" ctype="SpriteObjectData">
                    <Size X="48.0000" Y="50.0000" />
                    <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
                    <Position X="399.4600" Y="24.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.9986" Y="0.4800" />
                    <PreSize X="0.1200" Y="1.0000" />
                    <FileData Type="MarkedSubImage" Path="hall/res/info/arrow_down.png" Plist="hall/res/myinfo.plist" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleY="0.5000" />
                <Position X="252.8500" Y="417.7581" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3612" Y="0.5968" />
                <PreSize X="0.5714" Y="0.0714" />
                <FileData Type="Normal" Path="hall/res/info/edt_white.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_8" ActionTag="1016321494" Tag="1042" IconVisible="False" LeftMargin="113.2997" RightMargin="460.7003" TopMargin="434.7498" BottomMargin="237.2502" FontSize="28" LabelText="总共积分:" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="126.0000" Y="28.0000" />
                <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
                <Position X="239.2997" Y="251.2502" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="243" G="201" B="41" />
                <PrePosition X="0.3419" Y="0.3589" />
                <PreSize X="0.1800" Y="0.0400" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="accumulatePoints" ActionTag="182989942" Tag="1043" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="252.8497" RightMargin="47.1503" TopMargin="423.7496" BottomMargin="226.2504" Scale9Enable="True" LeftEage="33" RightEage="33" TopEage="18" BottomEage="18" Scale9OriginX="33" Scale9OriginY="18" Scale9Width="214" Scale9Height="18" ctype="ImageViewObjectData">
                <Size X="400.0000" Y="50.0000" />
                <Children>
                  <AbstractNodeData Name="input" ActionTag="1695239143" Tag="1044" IconVisible="False" PositionPercentYEnabled="True" PercentHeightEnable="True" PercentHeightEnabled="True" HorizontalEdge="BothEdge" LeftMargin="3.0000" RightMargin="3.0000" TouchEnable="True" StretchWidthEnable="True" FontSize="30" IsCustomSize="True" LabelText="" PlaceHolderText="" MaxLengthText="10" ctype="TextFieldObjectData">
                    <Size X="394.0000" Y="50.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="3.0000" Y="25.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.0075" Y="0.5000" />
                    <PreSize X="0.9850" Y="1.0000" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleY="0.5000" />
                <Position X="252.8497" Y="251.2504" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3612" Y="0.3589" />
                <PreSize X="0.5714" Y="0.0714" />
                <FileData Type="Normal" Path="hall/res/info/edt_white.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="submitBtn" ActionTag="-715737868" Tag="1045" IconVisible="False" PositionPercentXEnabled="True" VerticalEdge="BottomEdge" LeftMargin="236.5000" RightMargin="236.5000" TopMargin="598.6062" BottomMargin="27.3938" ctype="SpriteObjectData">
                <Size X="227.0000" Y="74.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="350.0000" Y="64.3938" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.0920" />
                <PreSize X="0.3243" Y="0.1057" />
                <FileData Type="Normal" Path="hall/res/info/commit.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="getMaxPoints" ActionTag="18092502" Tag="1046" IconVisible="False" LeftMargin="152.6700" RightMargin="296.3300" TopMargin="338.3732" BottomMargin="336.6268" FontSize="25" LabelText="您本次最多可领积分%d" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="251.0000" Y="25.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="152.6700" Y="349.1268" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="37" G="179" B="37" />
                <PrePosition X="0.2181" Y="0.4988" />
                <PreSize X="0.3586" Y="0.0357" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="getCount" ActionTag="-478654981" Tag="1047" IconVisible="False" LeftMargin="152.6700" RightMargin="296.3300" TopMargin="379.4402" BottomMargin="295.5598" FontSize="25" LabelText="今日剩余领取次数%d次" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="251.0000" Y="25.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="152.6700" Y="308.0598" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="37" G="179" B="37" />
                <PrePosition X="0.2181" Y="0.4401" />
                <PreSize X="0.3586" Y="0.0357" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="tip" ActionTag="1885226141" Tag="41" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="112.3263" RightMargin="587.6737" TopMargin="520.2012" BottomMargin="179.7988" FontSize="20" LabelText="" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="112.3263" Y="179.7988" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="0" B="0" />
                <PrePosition X="0.1605" Y="0.2569" />
                <PreSize X="0.0000" Y="0.0000" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="backBtn" ActionTag="-391208914" Tag="213" IconVisible="False" HorizontalEdge="LeftEdge" VerticalEdge="TopEdge" LeftMargin="8.2683" RightMargin="621.7317" TopMargin="3.3267" BottomMargin="624.6733" ctype="SpriteObjectData">
                <Size X="70.0000" Y="72.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="43.2683" Y="660.6733" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0618" Y="0.9438" />
                <PreSize X="0.1000" Y="0.1029" />
                <FileData Type="MarkedSubImage" Path="hall/res/info/backBtn.png" Plist="hall/res/myinfo.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="640.0000" Y="360.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="0.5469" Y="0.9722" />
            <FileData Type="Normal" Path="hall/res/info/popup_window_bg.png" Plist="" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>