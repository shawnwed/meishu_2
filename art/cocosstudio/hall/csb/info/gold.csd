<GameFile>
  <PropertyGroup Name="gold" Type="Layer" ID="6dca6f92-01a2-4a3e-b380-c3d9406a3a2b" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="367" ctype="GameLayerObjectData">
        <Size X="250.0000" Y="40.0000" />
        <Children>
          <AbstractNodeData Name="txt" ActionTag="-2038379723" Tag="370" IconVisible="False" LeftMargin="44.7700" RightMargin="45.2300" TopMargin="3.9838" BottomMargin="3.0162" IsCustomSize="True" FontSize="32" LabelText="1234567890" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="160.0000" Y="33.0000" />
            <AnchorPoint />
            <Position X="44.7700" Y="3.0162" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="240" G="203" B="52" />
            <PrePosition X="0.1791" Y="0.0754" />
            <PreSize X="0.6400" Y="0.8250" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>