<GameFile>
  <PropertyGroup Name="bindBankCard" Type="Layer" ID="ea82402c-d472-49df-b8ac-739e46e0dc11" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="1336" ctype="GameLayerObjectData">
        <Size X="1280.0000" Y="720.0000" />
        <Children>
          <AbstractNodeData Name="mask" ActionTag="43117576" Tag="1337" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" ClipAble="False" BackColorAlpha="177" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="1280.0000" Y="720.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="640.0000" Y="360.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="1.0000" Y="1.0000" />
            <SingleColor A="255" R="0" G="0" B="0" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="bg" ActionTag="26188227" Tag="1338" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="290.0000" RightMargin="290.0000" TopMargin="10.0000" BottomMargin="10.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Enable="True" LeftEage="256" RightEage="256" TopEage="179" BottomEage="156" Scale9OriginX="256" Scale9OriginY="179" Scale9Width="403" Scale9Height="241" ctype="PanelObjectData">
            <Size X="700.0000" Y="700.0000" />
            <Children>
              <AbstractNodeData Name="title" ActionTag="1484331646" Tag="1347" IconVisible="False" PositionPercentXEnabled="True" VerticalEdge="TopEdge" LeftMargin="262.1602" RightMargin="264.8398" TopMargin="31.2485" BottomMargin="631.7515" ctype="SpriteObjectData">
                <Size X="173.0000" Y="37.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="348.6602" Y="650.2515" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4981" Y="0.9289" />
                <PreSize X="0.2471" Y="0.0529" />
                <FileData Type="MarkedSubImage" Path="hall/res/info/mimaguanlibiaoti4.png" Plist="hall/res/info/bindBank.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_10" ActionTag="996263456" Tag="1356" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="61.8099" RightMargin="512.1901" TopMargin="110.9792" BottomMargin="561.0208" FontSize="28" LabelText="开户姓名:" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="126.0000" Y="28.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="124.8099" Y="575.0208" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="165" G="42" B="42" />
                <PrePosition X="0.1783" Y="0.8215" />
                <PreSize X="0.1800" Y="0.0400" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="name" ActionTag="-974222408" Tag="1381" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="194.7199" RightMargin="105.2801" TopMargin="99.9788" BottomMargin="550.0212" Scale9Enable="True" LeftEage="33" RightEage="33" TopEage="18" BottomEage="18" Scale9OriginX="33" Scale9OriginY="18" Scale9Width="433" Scale9Height="26" ctype="ImageViewObjectData">
                <Size X="400.0000" Y="50.0000" />
                <Children>
                  <AbstractNodeData Name="input" ActionTag="808598929" Tag="1382" IconVisible="False" PositionPercentYEnabled="True" PercentHeightEnable="True" PercentHeightEnabled="True" HorizontalEdge="BothEdge" LeftMargin="3.0000" RightMargin="3.0000" TouchEnable="True" StretchWidthEnable="True" FontSize="30" IsCustomSize="True" LabelText="" PlaceHolderText="" MaxLengthText="10" ctype="TextFieldObjectData">
                    <Size X="394.0000" Y="50.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="3.0000" Y="25.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.0075" Y="0.5000" />
                    <PreSize X="0.9850" Y="1.0000" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleY="0.5000" />
                <Position X="194.7199" Y="575.0212" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2782" Y="0.8215" />
                <PreSize X="0.5714" Y="0.0714" />
                <FileData Type="Normal" Path="hall/res/info/edt_white.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="Sprite_2" ActionTag="994535430" Tag="1383" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="61.5148" RightMargin="613.4852" TopMargin="163.2316" BottomMargin="510.7684" ctype="SpriteObjectData">
                <Size X="25.0000" Y="26.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="74.0148" Y="523.7684" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1057" Y="0.7482" />
                <PreSize X="0.0357" Y="0.0371" />
                <FileData Type="Normal" Path="hall/res/info/decorate.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_27" ActionTag="-1801647012" Tag="1392" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="90.5648" RightMargin="259.4352" TopMargin="164.0073" BottomMargin="510.9927" FontSize="25" LabelText="开户姓名必须与银行卡姓名一致" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="350.0000" Y="25.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="90.5648" Y="523.4927" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="81" G="70" B="69" />
                <PrePosition X="0.1294" Y="0.7478" />
                <PreSize X="0.5000" Y="0.0357" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_1" ActionTag="-600630215" Tag="45" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="61.8099" RightMargin="512.1901" TopMargin="211.5088" BottomMargin="460.4912" FontSize="28" LabelText="选择银行:" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="126.0000" Y="28.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="124.8099" Y="474.4912" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="165" G="42" B="42" />
                <PrePosition X="0.1783" Y="0.6778" />
                <PreSize X="0.1800" Y="0.0400" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="bank" ActionTag="-1658465434" Tag="46" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="194.7199" RightMargin="105.2801" TopMargin="200.5087" BottomMargin="449.4913" Scale9Enable="True" LeftEage="33" RightEage="33" TopEage="18" BottomEage="18" Scale9OriginX="33" Scale9OriginY="18" Scale9Width="433" Scale9Height="26" ctype="ImageViewObjectData">
                <Size X="400.0000" Y="50.0000" />
                <Children>
                  <AbstractNodeData Name="input" ActionTag="683145915" Tag="47" IconVisible="False" PositionPercentYEnabled="True" PercentHeightEnable="True" PercentHeightEnabled="True" HorizontalEdge="BothEdge" LeftMargin="3.0000" RightMargin="53.0000" StretchWidthEnable="True" FontSize="30" IsCustomSize="True" LabelText="" PlaceHolderText="" MaxLengthText="10" ctype="TextFieldObjectData">
                    <Size X="344.0000" Y="50.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="3.0000" Y="25.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.0075" Y="0.5000" />
                    <PreSize X="0.8600" Y="1.0000" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="selector" ActionTag="-256932789" Tag="48" IconVisible="False" PositionPercentYEnabled="True" HorizontalEdge="RightEdge" LeftMargin="351.0000" RightMargin="1.0000" ctype="SpriteObjectData">
                    <Size X="48.0000" Y="50.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="375.0000" Y="25.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.9375" Y="0.5000" />
                    <PreSize X="0.1200" Y="1.0000" />
                    <FileData Type="MarkedSubImage" Path="hall/res/info/arrow_down.png" Plist="hall/res/myinfo.plist" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleY="0.5000" />
                <Position X="194.7199" Y="474.4913" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2782" Y="0.6778" />
                <PreSize X="0.5714" Y="0.0714" />
                <FileData Type="Normal" Path="hall/res/info/edt_white.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_2" ActionTag="780279785" Tag="49" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="61.8099" RightMargin="512.1901" TopMargin="273.5088" BottomMargin="398.4912" FontSize="28" LabelText="开户省市:" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="126.0000" Y="28.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="124.8099" Y="412.4912" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="165" G="42" B="42" />
                <PrePosition X="0.1783" Y="0.5893" />
                <PreSize X="0.1800" Y="0.0400" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="province" ActionTag="-207730461" Tag="50" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="194.7199" RightMargin="105.2801" TopMargin="262.5081" BottomMargin="387.4919" Scale9Enable="True" LeftEage="33" RightEage="33" TopEage="18" BottomEage="18" Scale9OriginX="33" Scale9OriginY="18" Scale9Width="433" Scale9Height="26" ctype="ImageViewObjectData">
                <Size X="400.0000" Y="50.0000" />
                <Children>
                  <AbstractNodeData Name="input" ActionTag="-835102615" Tag="51" IconVisible="False" PositionPercentYEnabled="True" PercentHeightEnable="True" PercentHeightEnabled="True" HorizontalEdge="BothEdge" LeftMargin="3.0000" RightMargin="53.0000" StretchWidthEnable="True" FontSize="30" IsCustomSize="True" LabelText="" PlaceHolderText="请选择开户省、直辖市" MaxLengthText="10" ctype="TextFieldObjectData">
                    <Size X="344.0000" Y="50.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="3.0000" Y="25.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.0075" Y="0.5000" />
                    <PreSize X="0.8600" Y="1.0000" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="selector" ActionTag="-2068861193" Tag="52" IconVisible="False" PositionPercentYEnabled="True" HorizontalEdge="RightEdge" LeftMargin="351.0000" RightMargin="1.0000" ctype="SpriteObjectData">
                    <Size X="48.0000" Y="50.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="375.0000" Y="25.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.9375" Y="0.5000" />
                    <PreSize X="0.1200" Y="1.0000" />
                    <FileData Type="MarkedSubImage" Path="hall/res/info/arrow_down.png" Plist="hall/res/myinfo.plist" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleY="0.5000" />
                <Position X="194.7199" Y="412.4919" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2782" Y="0.5893" />
                <PreSize X="0.5714" Y="0.0714" />
                <FileData Type="Normal" Path="hall/res/info/edt_white.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="city" ActionTag="-974452766" Tag="53" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="194.7199" RightMargin="105.2801" TopMargin="324.5089" BottomMargin="325.4911" Scale9Enable="True" LeftEage="33" RightEage="33" TopEage="18" BottomEage="18" Scale9OriginX="33" Scale9OriginY="18" Scale9Width="433" Scale9Height="26" ctype="ImageViewObjectData">
                <Size X="400.0000" Y="50.0000" />
                <Children>
                  <AbstractNodeData Name="input" ActionTag="-373977143" Tag="54" IconVisible="False" PositionPercentYEnabled="True" PercentHeightEnable="True" PercentHeightEnabled="True" HorizontalEdge="BothEdge" LeftMargin="3.0000" RightMargin="53.0000" StretchWidthEnable="True" FontSize="30" IsCustomSize="True" LabelText="" PlaceHolderText="请选择开户市、区、县" MaxLengthText="10" ctype="TextFieldObjectData">
                    <Size X="344.0000" Y="50.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="3.0000" Y="25.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.0075" Y="0.5000" />
                    <PreSize X="0.8600" Y="1.0000" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="selector" ActionTag="-5852942" Tag="55" IconVisible="False" PositionPercentYEnabled="True" HorizontalEdge="RightEdge" LeftMargin="351.0000" RightMargin="1.0000" ctype="SpriteObjectData">
                    <Size X="48.0000" Y="50.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="375.0000" Y="25.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.9375" Y="0.5000" />
                    <PreSize X="0.1200" Y="1.0000" />
                    <FileData Type="MarkedSubImage" Path="hall/res/info/arrow_down.png" Plist="hall/res/myinfo.plist" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleY="0.5000" />
                <Position X="194.7199" Y="350.4911" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2782" Y="0.5007" />
                <PreSize X="0.5714" Y="0.0714" />
                <FileData Type="Normal" Path="hall/res/info/edt_white.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_3" ActionTag="835476929" Tag="56" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="61.8099" RightMargin="512.1901" TopMargin="401.5079" BottomMargin="270.4921" FontSize="28" LabelText="开户银行:" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="126.0000" Y="28.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="124.8099" Y="284.4921" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="165" G="42" B="42" />
                <PrePosition X="0.1783" Y="0.4064" />
                <PreSize X="0.1800" Y="0.0400" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="bankSite" ActionTag="-1073824806" Tag="57" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="194.7199" RightMargin="105.2801" TopMargin="390.5074" BottomMargin="259.4926" Scale9Enable="True" LeftEage="33" RightEage="33" TopEage="18" BottomEage="18" Scale9OriginX="33" Scale9OriginY="18" Scale9Width="433" Scale9Height="26" ctype="ImageViewObjectData">
                <Size X="400.0000" Y="50.0000" />
                <Children>
                  <AbstractNodeData Name="input" ActionTag="-1649754818" Tag="58" IconVisible="False" PositionPercentYEnabled="True" HorizontalEdge="BothEdge" LeftMargin="3.0000" RightMargin="3.0000" TouchEnable="True" StretchWidthEnable="True" FontSize="30" IsCustomSize="True" LabelText="" PlaceHolderText="请输入开户银行名称" MaxLengthText="10" ctype="TextFieldObjectData">
                    <Size X="394.0000" Y="50.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="3.0000" Y="25.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.0075" Y="0.5000" />
                    <PreSize X="0.9850" Y="1.0000" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleY="0.5000" />
                <Position X="194.7199" Y="284.4926" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2782" Y="0.4064" />
                <PreSize X="0.5714" Y="0.0714" />
                <FileData Type="Normal" Path="hall/res/info/edt_white.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_4" ActionTag="-2127275558" Tag="59" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="61.8099" RightMargin="512.1901" TopMargin="468.5077" BottomMargin="203.4923" FontSize="28" LabelText="银行卡号:" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="126.0000" Y="28.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="124.8099" Y="217.4923" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="165" G="42" B="42" />
                <PrePosition X="0.1783" Y="0.3107" />
                <PreSize X="0.1800" Y="0.0400" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="bankCardNo" ActionTag="-463327361" Tag="60" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="194.7199" RightMargin="105.2801" TopMargin="457.5075" BottomMargin="192.4925" Scale9Enable="True" LeftEage="33" RightEage="33" TopEage="18" BottomEage="18" Scale9OriginX="33" Scale9OriginY="18" Scale9Width="433" Scale9Height="26" ctype="ImageViewObjectData">
                <Size X="400.0000" Y="50.0000" />
                <Children>
                  <AbstractNodeData Name="input" ActionTag="-707284114" Tag="61" IconVisible="False" PositionPercentYEnabled="True" PercentHeightEnable="True" PercentHeightEnabled="True" HorizontalEdge="BothEdge" LeftMargin="3.0000" RightMargin="3.0000" TouchEnable="True" StretchWidthEnable="True" FontSize="30" IsCustomSize="True" LabelText="" PlaceHolderText="" MaxLengthText="10" ctype="TextFieldObjectData">
                    <Size X="394.0000" Y="50.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="3.0000" Y="25.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.0075" Y="0.5000" />
                    <PreSize X="0.9850" Y="1.0000" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleY="0.5000" />
                <Position X="194.7199" Y="217.4925" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2782" Y="0.3107" />
                <PreSize X="0.5714" Y="0.0714" />
                <FileData Type="Normal" Path="hall/res/info/edt_white.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_5" ActionTag="-1242349277" Tag="62" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="61.8099" RightMargin="512.1901" TopMargin="535.5072" BottomMargin="136.4928" FontSize="28" LabelText="支付密码:" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="126.0000" Y="28.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="124.8099" Y="150.4928" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="165" G="42" B="42" />
                <PrePosition X="0.1783" Y="0.2150" />
                <PreSize X="0.1800" Y="0.0400" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="payPwd" ActionTag="1492885566" Tag="63" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="194.7194" RightMargin="105.2806" TopMargin="524.5076" BottomMargin="125.4924" Scale9Enable="True" LeftEage="33" RightEage="33" TopEage="18" BottomEage="18" Scale9OriginX="33" Scale9OriginY="18" Scale9Width="433" Scale9Height="26" ctype="ImageViewObjectData">
                <Size X="400.0000" Y="50.0000" />
                <Children>
                  <AbstractNodeData Name="input" ActionTag="1660841932" Tag="64" IconVisible="False" PositionPercentYEnabled="True" PercentHeightEnable="True" PercentHeightEnabled="True" HorizontalEdge="BothEdge" LeftMargin="3.0000" RightMargin="151.0000" TouchEnable="True" StretchWidthEnable="True" FontSize="30" IsCustomSize="True" LabelText="" PlaceHolderText="" MaxLengthEnable="True" MaxLengthText="6" PasswordEnable="True" ctype="TextFieldObjectData">
                    <Size X="246.0000" Y="50.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="3.0000" Y="25.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.0075" Y="0.5000" />
                    <PreSize X="0.6150" Y="1.0000" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="forgetBtn" ActionTag="535151449" Tag="66" IconVisible="False" PositionPercentYEnabled="True" HorizontalEdge="RightEdge" LeftMargin="251.4859" RightMargin="-0.4859" TopMargin="2.0000" BottomMargin="-4.0000" ctype="SpriteObjectData">
                    <Size X="149.0000" Y="52.0000" />
                    <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
                    <Position X="400.4859" Y="22.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="1.0012" Y="0.4400" />
                    <PreSize X="0.3725" Y="1.0400" />
                    <FileData Type="Normal" Path="hall/res/info/forgetPwdBtn.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleY="0.5000" />
                <Position X="194.7194" Y="150.4924" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2782" Y="0.2150" />
                <PreSize X="0.5714" Y="0.0714" />
                <FileData Type="Normal" Path="hall/res/info/edt_white.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="confirmBtn" ActionTag="266507489" Tag="65" IconVisible="False" PositionPercentXEnabled="True" VerticalEdge="BottomEdge" LeftMargin="206.6428" RightMargin="218.3572" TopMargin="582.0789" BottomMargin="22.9212" ctype="SpriteObjectData">
                <Size X="275.0000" Y="95.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="344.1428" Y="70.4212" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4916" Y="0.1006" />
                <PreSize X="0.3929" Y="0.1357" />
                <FileData Type="Normal" Path="hall/res/info/ok.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="tip" ActionTag="2007756896" Tag="747" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="75.5915" RightMargin="624.4085" TopMargin="595.3176" BottomMargin="104.6824" FontSize="20" LabelText="" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="75.5915" Y="104.6824" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="0" B="0" />
                <PrePosition X="0.1080" Y="0.1495" />
                <PreSize X="0.0000" Y="0.0000" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="closeBtn" ActionTag="1984849026" Tag="180" IconVisible="False" HorizontalEdge="RightEdge" VerticalEdge="TopEdge" LeftMargin="634.8894" RightMargin="-14.8894" TopMargin="-1.7241" BottomMargin="620.7241" ctype="SpriteObjectData">
                <Size X="80.0000" Y="81.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="674.8894" Y="661.2241" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.9641" Y="0.9446" />
                <PreSize X="0.1143" Y="0.1157" />
                <FileData Type="Normal" Path="hall/res/info/x.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="640.0000" Y="360.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="0.5469" Y="0.9722" />
            <FileData Type="Normal" Path="hall/res/bingphone/bg.png" Plist="" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>