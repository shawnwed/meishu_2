<GameFile>
  <PropertyGroup Name="setPayPwd" Type="Layer" ID="95a6bcec-b113-4432-8bae-ea211ba9adcc" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="1242" ctype="GameLayerObjectData">
        <Size X="1280.0000" Y="720.0000" />
        <Children>
          <AbstractNodeData Name="mask" CanEdit="False" ActionTag="-926641184" Tag="1243" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" ClipAble="False" BackColorAlpha="179" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="1280.0000" Y="720.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="640.0000" Y="360.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="1.0000" Y="1.0000" />
            <SingleColor A="255" R="0" G="0" B="0" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="bg" ActionTag="-1875014092" Tag="1244" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="290.0000" RightMargin="290.0000" TopMargin="10.0000" BottomMargin="10.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="0" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Enable="True" LeftEage="275" RightEage="238" TopEage="156" BottomEage="184" Scale9OriginX="275" Scale9OriginY="156" Scale9Width="402" Scale9Height="236" ctype="PanelObjectData">
            <Size X="700.0000" Y="700.0000" />
            <Children>
              <AbstractNodeData Name="title" ActionTag="664068017" Tag="1245" IconVisible="False" PositionPercentXEnabled="True" VerticalEdge="TopEdge" LeftMargin="218.5300" RightMargin="228.4700" TopMargin="22.5787" BottomMargin="625.4213" ctype="SpriteObjectData">
                <Size X="253.0000" Y="52.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="345.0300" Y="651.4213" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4929" Y="0.9306" />
                <PreSize X="0.3614" Y="0.0743" />
                <FileData Type="Normal" Path="hall/res/info/shezhizhifumima.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_1" ActionTag="1108275470" Tag="1246" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="66.8217" RightMargin="333.1783" TopMargin="101.5819" BottomMargin="573.4181" FontSize="25" LabelText="设置支付密码，兑换时使用" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="300.0000" Y="25.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="66.8217" Y="585.9181" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="81" G="70" B="69" />
                <PrePosition X="0.0955" Y="0.8370" />
                <PreSize X="0.4286" Y="0.0357" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_2" ActionTag="-1417817541" Tag="1247" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="79.7905" RightMargin="494.2095" TopMargin="148.0490" BottomMargin="523.9510" FontSize="28" LabelText="支付密码:" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="126.0000" Y="28.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="142.7905" Y="537.9510" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="165" G="42" B="42" />
                <PrePosition X="0.2040" Y="0.7685" />
                <PreSize X="0.1800" Y="0.0400" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="pwd" ActionTag="-1644005065" Tag="1277" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="211.8312" RightMargin="88.1688" TopMargin="138.2296" BottomMargin="511.7704" Scale9Enable="True" LeftEage="33" RightEage="33" TopEage="18" BottomEage="18" Scale9OriginX="33" Scale9OriginY="18" Scale9Width="433" Scale9Height="26" ctype="ImageViewObjectData">
                <Size X="400.0000" Y="50.0000" />
                <Children>
                  <AbstractNodeData Name="input" ActionTag="-956389231" Tag="1278" IconVisible="False" PositionPercentYEnabled="True" PercentHeightEnable="True" PercentHeightEnabled="True" HorizontalEdge="BothEdge" LeftMargin="3.0000" RightMargin="3.0000" TouchEnable="True" StretchWidthEnable="True" FontSize="30" IsCustomSize="True" LabelText="" PlaceHolderText="" MaxLengthText="6" ctype="TextFieldObjectData">
                    <Size X="394.0000" Y="50.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="200.0000" Y="25.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="0.9850" Y="1.0000" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleY="0.5000" />
                <Position X="211.8312" Y="536.7704" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3026" Y="0.7668" />
                <PreSize X="0.5714" Y="0.0714" />
                <FileData Type="Normal" Path="hall/res/info/edt_white.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_5" ActionTag="1022586259" Tag="1279" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="79.7905" RightMargin="494.2095" TopMargin="207.5795" BottomMargin="464.4205" FontSize="28" LabelText="确认密码:" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="126.0000" Y="28.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="142.7905" Y="478.4205" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="165" G="42" B="42" />
                <PrePosition X="0.2040" Y="0.6835" />
                <PreSize X="0.1800" Y="0.0400" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="pwd1" ActionTag="605193085" Tag="1280" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="211.8322" RightMargin="88.1678" TopMargin="196.5795" BottomMargin="453.4205" Scale9Enable="True" LeftEage="33" RightEage="33" TopEage="18" BottomEage="18" Scale9OriginX="33" Scale9OriginY="18" Scale9Width="433" Scale9Height="26" ctype="ImageViewObjectData">
                <Size X="400.0000" Y="50.0000" />
                <Children>
                  <AbstractNodeData Name="input" ActionTag="-112141995" Tag="1281" IconVisible="False" PositionPercentYEnabled="True" PercentHeightEnable="True" PercentHeightEnabled="True" HorizontalEdge="BothEdge" LeftMargin="3.0000" RightMargin="3.0000" TouchEnable="True" StretchWidthEnable="True" FontSize="30" IsCustomSize="True" LabelText="" PlaceHolderText="" MaxLengthText="6" ctype="TextFieldObjectData">
                    <Size X="394.0000" Y="50.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="200.0000" Y="25.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="0.9850" Y="1.0000" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleY="0.5000" />
                <Position X="211.8322" Y="478.4205" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3026" Y="0.6835" />
                <PreSize X="0.5714" Y="0.0714" />
                <FileData Type="Normal" Path="hall/res/info/edt_white.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_6" ActionTag="-687201037" Tag="1282" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="66.8217" RightMargin="108.1783" TopMargin="256.4891" BottomMargin="418.5109" FontSize="25" LabelText="为了方便您找回支付密码，请设置以下安全问题" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="525.0000" Y="25.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="66.8217" Y="431.0109" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="81" G="70" B="69" />
                <PrePosition X="0.0955" Y="0.6157" />
                <PreSize X="0.7500" Y="0.0357" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_7" ActionTag="-2096550254" Tag="1283" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="79.7905" RightMargin="494.2095" TopMargin="305.8588" BottomMargin="366.1412" FontSize="28" LabelText="安全问题:" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="126.0000" Y="28.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="142.7905" Y="380.1412" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="165" G="42" B="42" />
                <PrePosition X="0.2040" Y="0.5431" />
                <PreSize X="0.1800" Y="0.0400" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="security" ActionTag="755127482" Tag="1284" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="211.8322" RightMargin="88.1678" TopMargin="294.8588" BottomMargin="355.1412" Scale9Enable="True" LeftEage="33" RightEage="33" TopEage="18" BottomEage="18" Scale9OriginX="33" Scale9OriginY="18" Scale9Width="433" Scale9Height="26" ctype="ImageViewObjectData">
                <Size X="400.0000" Y="50.0000" />
                <Children>
                  <AbstractNodeData Name="input" ActionTag="-111540225" Tag="1285" IconVisible="False" PositionPercentYEnabled="True" PercentHeightEnable="True" PercentHeightEnabled="True" HorizontalEdge="BothEdge" LeftMargin="3.0000" RightMargin="50.0000" TopMargin="-1.3550" BottomMargin="1.3550" StretchWidthEnable="True" FontSize="30" IsCustomSize="True" LabelText="" PlaceHolderText="" MaxLengthText="10" ctype="TextFieldObjectData">
                    <Size X="347.0000" Y="50.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="3.0000" Y="26.3550" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.0075" Y="0.5271" />
                    <PreSize X="0.8675" Y="1.0000" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="selector" ActionTag="-1133229148" Tag="52" IconVisible="False" PositionPercentYEnabled="True" HorizontalEdge="RightEdge" LeftMargin="351.0002" RightMargin="0.9998" ctype="SpriteObjectData">
                    <Size X="48.0000" Y="50.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="375.0002" Y="25.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.9375" Y="0.5000" />
                    <PreSize X="0.1200" Y="1.0000" />
                    <FileData Type="MarkedSubImage" Path="hall/res/info/arrow_down.png" Plist="hall/res/myinfo.plist" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleY="0.5000" />
                <Position X="211.8322" Y="380.1412" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3026" Y="0.5431" />
                <PreSize X="0.5714" Y="0.0714" />
                <FileData Type="Normal" Path="hall/res/info/edt_white.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_8" ActionTag="589741237" Tag="1286" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="79.7905" RightMargin="494.2095" TopMargin="364.5788" BottomMargin="307.4212" FontSize="28" LabelText="问题答案:" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="126.0000" Y="28.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="142.7905" Y="321.4212" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="165" G="42" B="42" />
                <PrePosition X="0.2040" Y="0.4592" />
                <PreSize X="0.1800" Y="0.0400" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="answer" ActionTag="129453613" Tag="1287" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="211.8322" RightMargin="88.1678" TopMargin="353.5787" BottomMargin="296.4213" Scale9Enable="True" LeftEage="33" RightEage="33" TopEage="18" BottomEage="18" Scale9OriginX="33" Scale9OriginY="18" Scale9Width="433" Scale9Height="26" ctype="ImageViewObjectData">
                <Size X="400.0000" Y="50.0000" />
                <Children>
                  <AbstractNodeData Name="input" ActionTag="147900308" Tag="1288" IconVisible="False" PositionPercentYEnabled="True" HorizontalEdge="BothEdge" LeftMargin="3.0000" RightMargin="3.0000" TouchEnable="True" StretchWidthEnable="True" FontSize="30" IsCustomSize="True" LabelText="" PlaceHolderText="" MaxLengthText="10" ctype="TextFieldObjectData">
                    <Size X="394.0000" Y="50.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="200.0000" Y="25.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="0.9850" Y="1.0000" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleY="0.5000" />
                <Position X="211.8322" Y="321.4213" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3026" Y="0.4592" />
                <PreSize X="0.5714" Y="0.0714" />
                <FileData Type="Normal" Path="hall/res/info/edt_white.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_9" ActionTag="-431825066" Tag="1289" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="66.8217" RightMargin="183.1783" TopMargin="415.0791" BottomMargin="259.9209" FontSize="25" LabelText="为了保护您的账户安全，请验证手机号码" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="450.0000" Y="25.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="66.8217" Y="272.4209" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="81" G="70" B="69" />
                <PrePosition X="0.0955" Y="0.3892" />
                <PreSize X="0.6429" Y="0.0357" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_10" ActionTag="-1062128649" Tag="1290" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="79.7905" RightMargin="494.2095" TopMargin="461.5790" BottomMargin="210.4210" FontSize="28" LabelText="手机号码:" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="126.0000" Y="28.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="142.7905" Y="224.4210" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="165" G="42" B="42" />
                <PrePosition X="0.2040" Y="0.3206" />
                <PreSize X="0.1800" Y="0.0400" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="phoneNo" ActionTag="326270481" Tag="1291" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="211.8322" RightMargin="88.1678" TopMargin="450.5790" BottomMargin="199.4210" Scale9Enable="True" LeftEage="33" RightEage="33" TopEage="18" BottomEage="18" Scale9OriginX="33" Scale9OriginY="18" Scale9Width="433" Scale9Height="26" ctype="ImageViewObjectData">
                <Size X="400.0000" Y="50.0000" />
                <Children>
                  <AbstractNodeData Name="input" ActionTag="-1087283416" Tag="1292" IconVisible="False" PositionPercentYEnabled="True" PercentHeightEnable="True" PercentHeightEnabled="True" HorizontalEdge="BothEdge" LeftMargin="3.0000" RightMargin="3.0000" TouchEnable="True" StretchWidthEnable="True" FontSize="30" IsCustomSize="True" LabelText="" PlaceHolderText="" MaxLengthText="10" ctype="TextFieldObjectData">
                    <Size X="394.0000" Y="50.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="200.0000" Y="25.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="0.9850" Y="1.0000" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleY="0.5000" />
                <Position X="211.8322" Y="224.4210" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3026" Y="0.3206" />
                <PreSize X="0.5714" Y="0.0714" />
                <FileData Type="Normal" Path="hall/res/info/edt_white.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_11" ActionTag="2011732437" Tag="1293" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="79.7905" RightMargin="494.2095" TopMargin="522.7888" BottomMargin="149.2112" FontSize="28" LabelText="验 证 码:" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="126.0000" Y="28.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="142.7905" Y="163.2112" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="165" G="42" B="42" />
                <PrePosition X="0.2040" Y="0.2332" />
                <PreSize X="0.1800" Y="0.0400" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="verify" ActionTag="-1329039135" Tag="1294" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="211.8322" RightMargin="292.1678" TopMargin="511.7888" BottomMargin="138.2112" Scale9Enable="True" LeftEage="33" RightEage="33" TopEage="18" BottomEage="18" Scale9OriginX="33" Scale9OriginY="18" Scale9Width="433" Scale9Height="26" ctype="ImageViewObjectData">
                <Size X="196.0000" Y="50.0000" />
                <Children>
                  <AbstractNodeData Name="input" ActionTag="-1743078036" Tag="1295" IconVisible="False" PositionPercentYEnabled="True" HorizontalEdge="BothEdge" LeftMargin="3.0000" RightMargin="1.0000" TouchEnable="True" StretchWidthEnable="True" FontSize="30" IsCustomSize="True" LabelText="" PlaceHolderText="" MaxLengthText="10" ctype="TextFieldObjectData">
                    <Size X="192.0000" Y="50.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="3.0000" Y="25.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.0153" Y="0.5000" />
                    <PreSize X="0.9796" Y="1.0000" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleY="0.5000" />
                <Position X="211.8322" Y="163.2112" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3026" Y="0.2332" />
                <PreSize X="0.2800" Y="0.0714" />
                <FileData Type="Normal" Path="hall/res/info/edt_white.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="getVerifyBtn" ActionTag="1779951468" Tag="1319" IconVisible="True" VerticalEdge="TopEdge" LeftMargin="420.5753" RightMargin="89.4247" TopMargin="508.1090" BottomMargin="127.8910" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
                <Size X="190.0000" Y="64.0000" />
                <AnchorPoint />
                <Position X="420.5753" Y="127.8910" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.6008" Y="0.1827" />
                <PreSize X="0.2714" Y="0.0914" />
                <FileData Type="Normal" Path="hall/csb/info/repeat.csd" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="confirmBtn" ActionTag="1666999593" Tag="1296" IconVisible="False" PositionPercentXEnabled="True" VerticalEdge="BottomEdge" LeftMargin="225.1000" RightMargin="199.9000" TopMargin="582.1609" BottomMargin="22.8391" ctype="SpriteObjectData">
                <Size X="275.0000" Y="95.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="362.6000" Y="70.3391" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5180" Y="0.1005" />
                <PreSize X="0.3929" Y="0.1357" />
                <FileData Type="Normal" Path="hall/res/info/ok.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="tip" ActionTag="-799254701" Tag="172" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="91.4650" RightMargin="608.5350" TopMargin="584.8009" BottomMargin="115.1991" FontSize="20" LabelText="" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="91.4650" Y="115.1991" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="0" B="0" />
                <PrePosition X="0.1307" Y="0.1646" />
                <PreSize X="0.0000" Y="0.0000" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="backBtn" ActionTag="-775939196" Tag="175" IconVisible="False" HorizontalEdge="LeftEdge" VerticalEdge="TopEdge" LeftMargin="637.7761" RightMargin="-17.7761" TopMargin="-4.1800" BottomMargin="623.1800" ctype="SpriteObjectData">
                <Size X="80.0000" Y="81.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="677.7761" Y="663.6800" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.9683" Y="0.9481" />
                <PreSize X="0.1143" Y="0.1157" />
                <FileData Type="Normal" Path="hall/res/info/x.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="640.0000" Y="360.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="0.5469" Y="0.9722" />
            <FileData Type="Normal" Path="hall/res/bingphone/bg.png" Plist="" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>