<GameFile>
  <PropertyGroup Name="spinnerItem" Type="Node" ID="89f46c8d-fc98-462f-b420-4a1fb67bd9a1" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="32" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="bg" ActionTag="2028048035" Tag="33" IconVisible="False" RightMargin="-400.0000" TopMargin="-52.0000" ctype="SpriteObjectData">
            <Size X="400.0000" Y="52.0000" />
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="hall/res/info/menu_item_bg.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="txt" ActionTag="67900572" Tag="34" IconVisible="False" LeftMargin="46.0000" RightMargin="-136.0000" TopMargin="-40.0000" BottomMargin="10.0000" FontSize="30" LabelText="南山区" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="90.0000" Y="30.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="46.0000" Y="25.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>