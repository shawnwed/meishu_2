<GameFile>
  <PropertyGroup Name="gameRecord" Type="Layer" ID="7e49ce42-e1e5-44eb-a41d-a2f657530ba2" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="192" ctype="GameLayerObjectData">
        <Size X="942.0000" Y="593.0000" />
        <Children>
          <AbstractNodeData Name="chipCount_tip" ActionTag="-1403487045" Tag="197" IconVisible="False" LeftMargin="313.8289" RightMargin="493.1711" TopMargin="53.8310" BottomMargin="509.1690" FontSize="30" LabelText="目前筹码:" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="135.0000" Y="30.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="381.3289" Y="524.1690" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="203" G="102" B="46" />
            <PrePosition X="0.4048" Y="0.8839" />
            <PreSize X="0.1433" Y="0.0506" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="chipCountNum" ActionTag="869208465" Tag="198" IconVisible="False" LeftMargin="451.0587" RightMargin="430.9413" TopMargin="53.8310" BottomMargin="509.1690" FontSize="30" LabelText="1000" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="60.0000" Y="30.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="451.0587" Y="524.1690" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="203" G="102" B="46" />
            <PrePosition X="0.4788" Y="0.8839" />
            <PreSize X="0.0637" Y="0.0506" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="game_selector" ActionTag="-1820240615" Tag="199" IconVisible="False" LeftMargin="682.5200" RightMargin="72.4800" TopMargin="48.5200" BottomMargin="501.4800" ctype="SpriteObjectData">
            <Size X="187.0000" Y="43.0000" />
            <Children>
              <AbstractNodeData Name="txt" ActionTag="-817233872" Tag="200" IconVisible="False" LeftMargin="36.9996" RightMargin="75.0004" TopMargin="9.0005" BottomMargin="8.9995" FontSize="25" LabelText="炸金花" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="75.0000" Y="25.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="74.4996" Y="21.4995" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3984" Y="0.5000" />
                <PreSize X="0.4011" Y="0.5814" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="776.0200" Y="522.9800" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8238" Y="0.8819" />
            <PreSize X="0.1985" Y="0.0725" />
            <FileData Type="Normal" Path="hall/res/info/date_select_bg.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="header" ActionTag="1859986379" Tag="201" IconVisible="False" LeftMargin="42.0000" RightMargin="42.0000" TopMargin="92.0000" BottomMargin="421.0000" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="858.0000" Y="80.0000" />
            <Children>
              <AbstractNodeData Name="timeTip" ActionTag="-325335405" Tag="202" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="28.9146" RightMargin="754.0854" TopMargin="25.0000" BottomMargin="25.0000" FontSize="30" LabelText="时 间" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="75.0000" Y="30.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="28.9146" Y="40.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="146" B="0" />
                <PrePosition X="0.0337" Y="0.5000" />
                <PreSize X="0.0874" Y="0.3750" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="enterChipNumTip" ActionTag="549151340" Tag="203" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="192.7668" RightMargin="545.2332" TopMargin="25.0000" BottomMargin="25.0000" FontSize="30" LabelText="当局携带" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="120.0000" Y="30.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="252.7668" Y="40.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="146" B="0" />
                <PrePosition X="0.2946" Y="0.5000" />
                <PreSize X="0.1399" Y="0.3750" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="exitChipNumTip" ActionTag="1284486510" Tag="204" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="379.5534" RightMargin="358.4466" TopMargin="25.0000" BottomMargin="25.0000" FontSize="30" LabelText="输赢金额" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="120.0000" Y="30.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="439.5534" Y="40.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="146" B="0" />
                <PrePosition X="0.5123" Y="0.5000" />
                <PreSize X="0.1399" Y="0.3750" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="commissionTip" ActionTag="-1299177709" Tag="205" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="554.3280" RightMargin="183.6720" TopMargin="25.0000" BottomMargin="25.0000" FontSize="30" LabelText="抽水金额" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="120.0000" Y="30.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="614.3280" Y="40.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="146" B="0" />
                <PrePosition X="0.7160" Y="0.5000" />
                <PreSize X="0.1399" Y="0.3750" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="roomTip" ActionTag="-1919485029" Tag="206" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="710.5698" RightMargin="27.4302" TopMargin="25.0000" BottomMargin="25.0000" FontSize="30" LabelText="游戏房间" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="120.0000" Y="30.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="770.5698" Y="40.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="146" B="0" />
                <PrePosition X="0.8981" Y="0.5000" />
                <PreSize X="0.1399" Y="0.3750" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="line" ActionTag="464078514" Tag="207" IconVisible="False" PositionPercentYEnabled="True" TopMargin="77.0000" ctype="SpriteObjectData">
                <Size X="858.0000" Y="3.0000" />
                <AnchorPoint ScaleX="0.5000" />
                <Position X="429.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" />
                <PreSize X="1.0000" Y="0.0375" />
                <FileData Type="Normal" Path="hall/res/info/line.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="471.0000" Y="461.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.7774" />
            <PreSize X="0.9108" Y="0.1349" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="containor" ActionTag="488212232" Tag="321" IconVisible="False" LeftMargin="42.0000" RightMargin="42.0000" TopMargin="172.0000" BottomMargin="67.0000" ClipAble="True" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="858.0000" Y="354.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="1.0000" />
            <Position X="471.0000" Y="421.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.7099" />
            <PreSize X="0.9108" Y="0.5970" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>