<GameFile>
  <PropertyGroup Name="popupWindow" Type="Node" ID="ceef4f4a-3347-4705-9f2b-efce809addc3" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="493" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="containor" CanEdit="False" ActionTag="1529847688" Tag="428" IconVisible="False" LeftMargin="-75.0000" RightMargin="-75.0000" BottomMargin="-480.0000" TouchEnable="True" ClipAble="True" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="150.0000" Y="480.0000" />
            <Children>
              <AbstractNodeData Name="bg" CanEdit="False" ActionTag="28903505" Tag="158" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="12.5000" RightMargin="12.5000" TopMargin="1.5840" BottomMargin="-1.5840" Scale9Enable="True" LeftEage="49" RightEage="54" TopEage="68" BottomEage="68" Scale9OriginX="49" Scale9OriginY="68" Scale9Width="22" Scale9Height="70" ctype="ImageViewObjectData">
                <Size X="125.0000" Y="480.0000" />
                <Children>
                  <AbstractNodeData Name="item_1" ActionTag="1460143055" Tag="977" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="0.6443" RightMargin="-0.6443" TopMargin="17.9282" BottomMargin="422.0718" IsCustomSize="True" FontSize="24" LabelText="近两天" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="125.0000" Y="40.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="63.1443" Y="442.0718" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5052" Y="0.9210" />
                    <PreSize X="1.0000" Y="0.0833" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="item_2" ActionTag="1188693146" Tag="746" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="0.6443" RightMargin="-0.6443" TopMargin="71.6525" BottomMargin="368.3475" IsCustomSize="True" FontSize="24" LabelText="近两天" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="125.0000" Y="40.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="63.1443" Y="388.3475" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5052" Y="0.8091" />
                    <PreSize X="1.0000" Y="0.0833" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="item_3" ActionTag="-381834296" Tag="747" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="0.6443" RightMargin="-0.6443" TopMargin="125.3768" BottomMargin="314.6232" IsCustomSize="True" FontSize="24" LabelText="近3个月" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="125.0000" Y="40.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="63.1443" Y="334.6232" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5052" Y="0.6971" />
                    <PreSize X="1.0000" Y="0.0833" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="item_4" ActionTag="-491780499" Tag="1699" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="-1.3125" RightMargin="1.3125" TopMargin="164.4665" BottomMargin="275.5335" IsCustomSize="True" FontSize="24" LabelText="" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="125.0000" Y="40.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="61.1875" Y="295.5335" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.4895" Y="0.6157" />
                    <PreSize X="1.0000" Y="0.0833" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="item_5" ActionTag="746208365" Tag="998" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="-1.3125" RightMargin="1.3125" TopMargin="217.6053" BottomMargin="222.3947" IsCustomSize="True" FontSize="24" LabelText="" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="125.0000" Y="40.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="61.1875" Y="242.3947" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.4895" Y="0.5050" />
                    <PreSize X="1.0000" Y="0.0833" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="item_6" ActionTag="1940254043" Tag="999" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="-1.3125" RightMargin="1.3125" TopMargin="270.7443" BottomMargin="169.2557" IsCustomSize="True" FontSize="24" LabelText="" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="125.0000" Y="40.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="61.1875" Y="189.2557" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.4895" Y="0.3943" />
                    <PreSize X="1.0000" Y="0.0833" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="item_7" ActionTag="260109785" Tag="3223" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="-1.3125" RightMargin="1.3125" TopMargin="323.8832" BottomMargin="116.1168" IsCustomSize="True" FontSize="24" LabelText="" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="125.0000" Y="40.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="61.1875" Y="136.1168" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.4895" Y="0.2836" />
                    <PreSize X="1.0000" Y="0.0833" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="item_8" ActionTag="653441327" Tag="667" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="-1.3125" RightMargin="1.3125" TopMargin="377.0221" BottomMargin="62.9779" IsCustomSize="True" FontSize="24" LabelText="" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="125.0000" Y="40.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="61.1875" Y="82.9779" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.4895" Y="0.1729" />
                    <PreSize X="1.0000" Y="0.0833" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="item_9" ActionTag="-1122304505" Tag="1945" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="-1.3125" RightMargin="1.3125" TopMargin="430.1610" BottomMargin="9.8390" IsCustomSize="True" FontSize="24" LabelText="" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="125.0000" Y="40.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="61.1875" Y="29.8390" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.4895" Y="0.0622" />
                    <PreSize X="1.0000" Y="0.0833" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="1.0000" />
                <Position X="75.0000" Y="478.4160" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.9967" />
                <PreSize X="0.8333" Y="1.0000" />
                <FileData Type="Normal" Path="hall/res/info/popupWindow_bg.png" Plist="" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="1.0000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>