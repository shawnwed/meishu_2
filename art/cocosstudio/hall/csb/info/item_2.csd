<GameFile>
  <PropertyGroup Name="item_2" Type="Node" ID="c9e80ccb-257f-47d8-9143-e5a71345734a" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="242" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="bg" ActionTag="1224417451" Tag="249" IconVisible="False" LeftMargin="34.5000" RightMargin="-785.5000" TopMargin="-59.5000" BottomMargin="4.5000" ctype="SpriteObjectData">
            <Size X="751.0000" Y="55.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="410.0000" Y="32.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="hall/res/info/bg3.png" Plist="hall/res/myinfo.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="refresh" ActionTag="-2113393073" Tag="243" IconVisible="False" LeftMargin="333.3654" RightMargin="-445.3654" TopMargin="-110.3370" BottomMargin="82.3370" FontSize="28" LabelText="下拉刷新" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="112.0000" Y="28.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="389.3654" Y="96.3370" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="165" G="42" B="42" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="load_more_txt" ActionTag="418576154" Tag="244" IconVisible="False" LeftMargin="300.5143" RightMargin="-510.5143" TopMargin="10.8513" BottomMargin="-38.8513" FontSize="28" LabelText="上拉下載更多..." ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="210.0000" Y="28.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="405.5143" Y="-24.8513" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="165" G="42" B="42" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="status" ActionTag="-335782715" Tag="245" IconVisible="False" LeftMargin="669.1179" RightMargin="-721.1179" TopMargin="-43.5720" BottomMargin="17.5720" FontSize="26" LabelText="完成" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="52.0000" Y="26.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="695.1179" Y="30.5720" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="64" G="30" B="0" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="type" ActionTag="-989640090" Tag="246" IconVisible="False" LeftMargin="430.9320" RightMargin="-547.9320" TopMargin="-42.4284" BottomMargin="16.4284" FontSize="26" LabelText="122222222" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="117.0000" Y="26.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="489.4320" Y="29.4284" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="64" G="30" B="0" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="win" ActionTag="-863809668" Tag="247" IconVisible="False" LeftMargin="284.7990" RightMargin="-323.7990" TopMargin="-42.9996" BottomMargin="16.9996" FontSize="26" LabelText="100" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="39.0000" Y="26.0000" />
            <AnchorPoint ScaleY="0.4801" />
            <Position X="284.7990" Y="29.4822" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="64" G="30" B="0" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="icon" ActionTag="-945771880" Tag="711" IconVisible="False" LeftMargin="241.3209" RightMargin="-284.3209" TopMargin="-52.5664" BottomMargin="9.5664" ctype="SpriteObjectData">
            <Size X="43.0000" Y="43.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="262.8209" Y="31.0664" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="hall/res/first/coin_money.png" Plist="hall/res/first.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="time" ActionTag="369836724" Tag="248" IconVisible="False" LeftMargin="75.2162" RightMargin="-218.2162" TopMargin="-43.5333" BottomMargin="17.5333" FontSize="26" LabelText="03-08 12:45" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="143.0000" Y="26.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="146.7162" Y="30.5333" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="64" G="30" B="0" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>