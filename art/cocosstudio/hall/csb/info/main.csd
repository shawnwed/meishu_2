<GameFile>
  <PropertyGroup Name="main" Type="Scene" ID="0461593f-2ab1-4d17-bf65-a071c4c97582" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Scene" Tag="308" ctype="GameNodeObjectData">
        <Size X="960.0000" Y="640.0000" />
        <Children>
          <AbstractNodeData Name="bg" ActionTag="1090619622" Tag="19" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" TouchEnable="True" LeftEage="422" RightEage="422" TopEage="237" BottomEage="237" Scale9OriginX="422" Scale9OriginY="237" Scale9Width="436" Scale9Height="246" ctype="ImageViewObjectData">
            <Size X="960.0000" Y="640.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="480.0000" Y="320.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="1.0000" Y="1.0000" />
            <FileData Type="Normal" Path="bg/bg_info.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="top" ActionTag="1441727622" Tag="6" IconVisible="True" PercentWidthEnable="True" PercentWidthEnabled="True" TopMargin="-79.6800" BottomMargin="624.6800" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="True" ctype="ProjectNodeObjectData">
            <Size X="960.0000" Y="95.0000" />
            <AnchorPoint />
            <Position Y="624.6800" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition Y="0.9761" />
            <PreSize X="1.0000" Y="0.1484" />
            <FileData Type="Normal" Path="game_public/csb/hall_top.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="left" ActionTag="-782881577" Tag="50" IconVisible="False" RightMargin="576.0000" TopMargin="40.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="0" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="384.0000" Y="600.0000" />
            <Children>
              <AbstractNodeData Name="bg" ActionTag="1735177689" Tag="142" IconVisible="False" PercentHeightEnable="True" PercentHeightEnabled="True" LeftMargin="-13.0000" RightMargin="-3.0000" TopMargin="10.0200" TopEage="208" Scale9OriginY="208" Scale9Width="459" Scale9Height="382" ctype="ImageViewObjectData">
                <Size X="400.0000" Y="589.9800" />
                <AnchorPoint ScaleX="0.5000" />
                <Position X="187.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4870" />
                <PreSize X="1.0417" Y="0.9833" />
                <FileData Type="MarkedSubImage" Path="hall/res/info/bg2.png" Plist="hall/res/myinfo.plist" />
              </AbstractNodeData>
              <AbstractNodeData Name="head" ActionTag="1761385170" Tag="176" IconVisible="False" LeftMargin="118.1038" RightMargin="179.8962" TopMargin="58.2200" BottomMargin="455.7800" LeftEage="28" RightEage="28" TopEage="28" BottomEage="28" Scale9OriginX="28" Scale9OriginY="28" Scale9Width="24" Scale9Height="24" ctype="ImageViewObjectData">
                <Size X="86.0000" Y="86.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="161.1038" Y="498.7800" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4195" Y="0.8313" />
                <PreSize X="0.2240" Y="0.1433" />
                <FileData Type="MarkedSubImage" Path="hall/res/first/head_bg.png" Plist="hall/res/first.plist" />
              </AbstractNodeData>
              <AbstractNodeData Name="id_txt" ActionTag="181612456" Tag="95" IconVisible="False" LeftMargin="110.6223" RightMargin="147.3777" TopMargin="193.1500" BottomMargin="378.8500" FontSize="28" LabelText="ID:154236" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="126.0000" Y="28.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="173.6223" Y="392.8500" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="201" G="95" B="0" />
                <PrePosition X="0.4521" Y="0.6547" />
                <PreSize X="0.3281" Y="0.0467" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="nick_title" ActionTag="-2001034278" Tag="96" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="6.9960" RightMargin="242.0040" TopMargin="268.5000" BottomMargin="304.5000" FontSize="27" LabelText="遊戲暱稱：" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="135.0000" Y="27.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="74.4960" Y="318.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="64" G="30" B="0" />
                <PrePosition X="0.1940" Y="0.5300" />
                <PreSize X="0.3516" Y="0.0450" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="nick_txt" ActionTag="-1820767372" Tag="97" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="152.2176" RightMargin="96.7824" TopMargin="267.7200" BottomMargin="305.2800" FontSize="27" LabelText="失落的灵魂" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="135.0000" Y="27.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="152.2176" Y="318.7800" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="64" G="30" B="0" />
                <PrePosition X="0.3964" Y="0.5313" />
                <PreSize X="0.3516" Y="0.0450" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="switch_user" ActionTag="-983508671" Tag="101" IconVisible="False" LeftMargin="59.4892" RightMargin="86.5108" TopMargin="477.9100" BottomMargin="48.0900" ctype="SpriteObjectData">
                <Size X="238.0000" Y="74.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="178.4892" Y="85.0900" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4648" Y="0.1418" />
                <PreSize X="0.6198" Y="0.1233" />
                <FileData Type="MarkedSubImage" Path="lan/cn/switch_user.png" Plist="lan/cn/game.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="icon_invite" ActionTag="-1529440768" VisibleForFrame="False" Tag="100" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="297.0752" RightMargin="6.9248" TopMargin="332.8800" BottomMargin="219.1200" ctype="SpriteObjectData">
                <Size X="80.0000" Y="48.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="337.0752" Y="243.1200" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.8778" Y="0.4052" />
                <PreSize X="0.2083" Y="0.0800" />
                <FileData Type="MarkedSubImage" Path="hall/res/info/icon_invite.png" Plist="hall/res/myinfo.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="icon_edit" ActionTag="704478252" Tag="99" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="297.0752" RightMargin="6.9248" TopMargin="267.7200" BottomMargin="284.2800" ctype="SpriteObjectData">
                <Size X="80.0000" Y="48.0000" />
                <Children>
                  <AbstractNodeData Name="Text_1" ActionTag="-1676679301" Tag="64" IconVisible="False" LeftMargin="20.3754" RightMargin="19.6246" TopMargin="9.1599" BottomMargin="18.8401" FontSize="20" LabelText="修改" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="40.0000" Y="20.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="40.3754" Y="28.8401" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5047" Y="0.6008" />
                    <PreSize X="0.5000" Y="0.4167" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="337.0752" Y="308.2800" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.8778" Y="0.5138" />
                <PreSize X="0.2083" Y="0.0800" />
                <FileData Type="MarkedSubImage" Path="hall/res/info/icon_edit.png" Plist="hall/res/myinfo.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="info_sex" ActionTag="1460182156" Tag="98" IconVisible="False" LeftMargin="64.9956" RightMargin="281.0044" TopMargin="186.4500" BottomMargin="374.5500" ctype="SpriteObjectData">
                <Size X="38.0000" Y="39.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="83.9956" Y="394.0500" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2187" Y="0.6567" />
                <PreSize X="0.0990" Y="0.0650" />
                <FileData Type="MarkedSubImage" Path="game_public/res/info_sex1.png" Plist="game_public/res/game.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="buddy_title" ActionTag="-2076634333" VisibleForFrame="False" Tag="102" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="6.9960" RightMargin="242.0040" TopMargin="337.9200" BottomMargin="235.0800" FontSize="27" LabelText="好友人數：" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="135.0000" Y="27.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="74.4960" Y="248.5800" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="64" G="30" B="0" />
                <PrePosition X="0.1940" Y="0.4143" />
                <PreSize X="0.3516" Y="0.0450" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="buddy_txt" ActionTag="321742338" VisibleForFrame="False" Tag="103" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="152.2176" RightMargin="190.7824" TopMargin="336.7200" BottomMargin="236.2800" FontSize="27" LabelText="5人" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="41.0000" Y="27.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="152.2176" Y="249.7800" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="64" G="30" B="0" />
                <PrePosition X="0.3964" Y="0.4163" />
                <PreSize X="0.1068" Y="0.0450" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="account_title" ActionTag="-1737735432" Tag="104" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="6.9957" RightMargin="242.0043" TopMargin="348.2697" BottomMargin="224.7303" FontSize="27" LabelText="賬號類型：" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="135.0000" Y="27.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="74.4957" Y="238.2303" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="64" G="30" B="0" />
                <PrePosition X="0.1940" Y="0.3971" />
                <PreSize X="0.3516" Y="0.0450" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="account_txt" ActionTag="-2062649069" Tag="105" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="152.2177" RightMargin="65.7823" TopMargin="346.9182" BottomMargin="226.0818" FontSize="27" LabelText="Facebook賬號" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="166.0000" Y="27.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="152.2177" Y="239.5818" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="201" G="95" B="0" />
                <PrePosition X="0.3964" Y="0.3993" />
                <PreSize X="0.4323" Y="0.0450" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.4000" Y="0.9375" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="right" ActionTag="-147403674" Tag="40" IconVisible="False" PercentWidthEnable="True" PercentWidthEnabled="True" LeftMargin="369.0583" RightMargin="-177.0583" TopMargin="40.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="76" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="768.0000" Y="600.0000" />
            <Children>
              <AbstractNodeData Name="bg_info_content" ActionTag="667962585" Tag="312" IconVisible="False" PositionPercentXEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" TopMargin="12.0000" LeftEage="25" RightEage="25" TopEage="25" BottomEage="85" Scale9OriginX="25" Scale9OriginY="25" Scale9Width="727" Scale9Height="480" ctype="ImageViewObjectData">
                <Size X="768.0000" Y="588.0000" />
                <AnchorPoint />
                <Position />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="1.0000" Y="0.9800" />
                <FileData Type="Normal" Path="hall/res/info/bg1.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="title2" ActionTag="646843506" Tag="46" IconVisible="False" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentWidthEnabled="True" LeftMargin="16.4117" RightMargin="21.9883" TopMargin="84.5000" BottomMargin="460.5000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="729.6000" Y="55.0000" />
                <Children>
                  <AbstractNodeData Name="Image_2" ActionTag="-530172208" Tag="66" IconVisible="False" PercentWidthEnable="True" PercentWidthEnabled="True" LeftEage="247" RightEage="247" TopEage="18" BottomEage="18" Scale9OriginX="247" Scale9OriginY="18" Scale9Width="257" Scale9Height="19" ctype="ImageViewObjectData">
                    <Size X="729.6000" Y="55.0000" />
                    <AnchorPoint />
                    <Position />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition />
                    <PreSize X="1.0000" Y="1.0000" />
                    <FileData Type="MarkedSubImage" Path="hall/res/info/bg3.png" Plist="hall/res/myinfo.plist" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="time" ActionTag="-1189672614" Tag="41" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="46.9600" RightMargin="630.6400" TopMargin="9.7056" BottomMargin="19.2944" FontSize="26" LabelText="時間" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="52.0000" Y="26.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="72.9600" Y="32.2944" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="194" G="106" B="52" />
                    <PrePosition X="0.1000" Y="0.5872" />
                    <PreSize X="0.0713" Y="0.4727" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="win" ActionTag="1720371527" Tag="42" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="243.9520" RightMargin="433.6480" TopMargin="8.5272" BottomMargin="20.4728" FontSize="26" LabelText="收益" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="52.0000" Y="26.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="269.9520" Y="33.4728" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="194" G="106" B="52" />
                    <PrePosition X="0.3700" Y="0.6086" />
                    <PreSize X="0.0713" Y="0.4727" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="type" ActionTag="-1726432179" Tag="43" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="433.6480" RightMargin="243.9520" TopMargin="9.7056" BottomMargin="19.2944" FontSize="26" LabelText="方式" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="52.0000" Y="26.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="459.6480" Y="32.2944" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="194" G="106" B="52" />
                    <PrePosition X="0.6300" Y="0.5872" />
                    <PreSize X="0.0713" Y="0.4727" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="status" ActionTag="2065155324" Tag="45" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="630.6400" RightMargin="46.9600" TopMargin="9.7056" BottomMargin="19.2944" FontSize="26" LabelText="狀態" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="52.0000" Y="26.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="656.6400" Y="32.2944" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="194" G="106" B="52" />
                    <PrePosition X="0.9000" Y="0.5872" />
                    <PreSize X="0.0713" Y="0.4727" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint />
                <Position X="16.4117" Y="460.5000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0214" Y="0.7675" />
                <PreSize X="0.9500" Y="0.0917" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="title1" ActionTag="209991776" Tag="47" IconVisible="False" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentWidthEnabled="True" LeftMargin="16.4117" RightMargin="21.9883" TopMargin="84.5000" BottomMargin="460.5000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="729.6000" Y="55.0000" />
                <Children>
                  <AbstractNodeData Name="Image_1" ActionTag="-61816219" Tag="65" IconVisible="False" PercentWidthEnable="True" PercentWidthEnabled="True" LeftEage="247" RightEage="247" TopEage="18" BottomEage="18" Scale9OriginX="247" Scale9OriginY="18" Scale9Width="257" Scale9Height="19" ctype="ImageViewObjectData">
                    <Size X="729.6000" Y="55.0000" />
                    <AnchorPoint />
                    <Position />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition />
                    <PreSize X="1.0000" Y="1.0000" />
                    <FileData Type="MarkedSubImage" Path="hall/res/info/bg3.png" Plist="hall/res/myinfo.plist" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="time" ActionTag="2075569182" Tag="48" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="46.9600" RightMargin="630.6400" TopMargin="10.9997" BottomMargin="18.0003" FontSize="26" LabelText="時間" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="52.0000" Y="26.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="72.9600" Y="31.0003" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="194" G="106" B="52" />
                    <PrePosition X="0.1000" Y="0.5636" />
                    <PreSize X="0.0713" Y="0.4727" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="take" ActionTag="-335755355" Tag="49" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="189.6040" RightMargin="487.9960" TopMargin="10.9994" BottomMargin="18.0006" FontSize="26" LabelText="攜帶" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="52.0000" Y="26.0000" />
                    <AnchorPoint ScaleX="0.5630" ScaleY="0.3345" />
                    <Position X="218.8800" Y="26.6976" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="194" G="106" B="52" />
                    <PrePosition X="0.3000" Y="0.4854" />
                    <PreSize X="0.0713" Y="0.4727" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="win" ActionTag="-1101760090" Tag="50" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="327.8560" RightMargin="349.7440" TopMargin="10.9997" BottomMargin="18.0003" FontSize="26" LabelText="收益" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="52.0000" Y="26.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="353.8560" Y="31.0003" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="194" G="106" B="52" />
                    <PrePosition X="0.4850" Y="0.5636" />
                    <PreSize X="0.0713" Y="0.4727" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="fee" ActionTag="-1944501401" Tag="51" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="464.4240" RightMargin="187.1760" TopMargin="10.9997" BottomMargin="18.0003" FontSize="26" LabelText="服務費" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="78.0000" Y="26.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="503.4240" Y="31.0003" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="194" G="106" B="52" />
                    <PrePosition X="0.6900" Y="0.5636" />
                    <PreSize X="0.1069" Y="0.4727" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="room" ActionTag="409158360" Tag="52" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="630.6400" RightMargin="46.9600" TopMargin="13.4890" BottomMargin="15.5110" FontSize="26" LabelText="房間" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="52.0000" Y="26.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="656.6400" Y="28.5110" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="194" G="106" B="52" />
                    <PrePosition X="0.9000" Y="0.5184" />
                    <PreSize X="0.0713" Y="0.4727" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint />
                <Position X="16.4117" Y="460.5000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0214" Y="0.7675" />
                <PreSize X="0.9500" Y="0.0917" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="list" ActionTag="955607778" Tag="207" IconVisible="False" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" LeftMargin="21.3972" RightMargin="17.0028" TopMargin="155.7000" BottomMargin="34.3200" TouchEnable="True" ClipAble="False" BackColorAlpha="127" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="729.6000" Y="409.9800" />
                <AnchorPoint />
                <Position X="21.3972" Y="34.3200" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0279" Y="0.0572" />
                <PreSize X="0.9500" Y="0.6833" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position X="369.0583" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3844" />
            <PreSize X="0.8000" Y="0.9375" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="tab" ActionTag="617158156" Tag="341" IconVisible="False" LeftMargin="386.0853" RightMargin="173.9147" TopMargin="41.3137" BottomMargin="541.6863" TouchEnable="True" ClipAble="False" BackColorAlpha="0" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="400.0000" Y="57.0000" />
            <Children>
              <AbstractNodeData Name="tab2" ActionTag="-359418584" Tag="2536" IconVisible="False" LeftMargin="233.5965" RightMargin="-33.5965" TopMargin="4.1800" BottomMargin="-4.1800" ctype="SpriteObjectData">
                <Size X="200.0000" Y="57.0000" />
                <Children>
                  <AbstractNodeData Name="line" ActionTag="1124076442" Tag="2537" IconVisible="False" LeftMargin="-0.3811" RightMargin="0.3811" TopMargin="1.6996" BottomMargin="-1.6996" ctype="SpriteObjectData">
                    <Size X="200.0000" Y="57.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="99.6189" Y="26.8004" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.4981" Y="0.4702" />
                    <PreSize X="1.0000" Y="1.0000" />
                    <FileData Type="MarkedSubImage" Path="hall/res/info/tab1.png" Plist="hall/res/myinfo.plist" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="txt" ActionTag="-430827472" Tag="2538" IconVisible="False" LeftMargin="35.4600" RightMargin="44.5400" TopMargin="14.9790" BottomMargin="12.0210" FontSize="30" LabelText="收益記錄" ShadowOffsetX="-1.0000" ShadowOffsetY="1.0000" ctype="TextObjectData">
                    <Size X="120.0000" Y="30.0000" />
                    <AnchorPoint ScaleX="0.4820" ScaleY="0.4943" />
                    <Position X="93.3000" Y="26.8500" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="161" G="77" B="50" />
                    <PrePosition X="0.4665" Y="0.4711" />
                    <PreSize X="0.6000" Y="0.5263" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="0" G="0" B="0" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="333.5965" Y="24.3200" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.8340" Y="0.4267" />
                <PreSize X="0.5000" Y="1.0000" />
                <FileData Type="MarkedSubImage" Path="hall/res/info/tab.png" Plist="hall/res/myinfo.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="tab1" ActionTag="698221203" Tag="2539" IconVisible="False" LeftMargin="0.0400" RightMargin="199.9600" TopMargin="4.1800" BottomMargin="-4.1800" ctype="SpriteObjectData">
                <Size X="200.0000" Y="57.0000" />
                <Children>
                  <AbstractNodeData Name="line_0" ActionTag="906697360" Tag="3363" IconVisible="False" LeftMargin="-0.4727" RightMargin="0.4727" TopMargin="1.6998" BottomMargin="-1.6998" ctype="SpriteObjectData">
                    <Size X="200.0000" Y="57.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="99.5273" Y="26.8002" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.4976" Y="0.4702" />
                    <PreSize X="1.0000" Y="1.0000" />
                    <FileData Type="MarkedSubImage" Path="hall/res/info/tab1.png" Plist="hall/res/myinfo.plist" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="txt" ActionTag="-1651429816" Tag="2540" IconVisible="False" LeftMargin="36.3000" RightMargin="43.7000" TopMargin="14.0700" BottomMargin="12.9300" FontSize="30" LabelText="牌局記錄" ShadowOffsetX="-1.0000" ShadowOffsetY="1.0000" ctype="TextObjectData">
                    <Size X="120.0000" Y="30.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="96.3000" Y="27.9300" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="161" G="77" B="50" />
                    <PrePosition X="0.4815" Y="0.4900" />
                    <PreSize X="0.6000" Y="0.5263" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="0" G="0" B="0" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="100.0400" Y="24.3200" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2501" Y="0.4267" />
                <PreSize X="0.5000" Y="1.0000" />
                <FileData Type="MarkedSubImage" Path="hall/res/info/tab.png" Plist="hall/res/myinfo.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleY="0.5000" />
            <Position X="386.0853" Y="570.1863" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4022" Y="0.8909" />
            <PreSize X="0.4167" Y="0.0891" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>