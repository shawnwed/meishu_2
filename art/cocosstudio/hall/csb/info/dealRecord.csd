<GameFile>
  <PropertyGroup Name="dealRecord" Type="Layer" ID="7e49ce42-e1e5-44eb-a41d-a2f657530ba2" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="192" ctype="GameLayerObjectData">
        <Size X="942.0000" Y="593.0000" />
        <Children>
          <AbstractNodeData Name="chipCount_tip" ActionTag="-1403487045" Tag="197" IconVisible="False" LeftMargin="313.8289" RightMargin="493.1711" TopMargin="53.8310" BottomMargin="509.1690" FontSize="30" LabelText="目前筹码:" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="135.0000" Y="30.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="381.3289" Y="524.1690" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="203" G="102" B="46" />
            <PrePosition X="0.4048" Y="0.8839" />
            <PreSize X="0.1433" Y="0.0506" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="chipCountNum" ActionTag="869208465" Tag="198" IconVisible="False" LeftMargin="451.0587" RightMargin="430.9413" TopMargin="53.8310" BottomMargin="509.1690" FontSize="30" LabelText="1000" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="60.0000" Y="30.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="451.0587" Y="524.1690" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="203" G="102" B="46" />
            <PrePosition X="0.4788" Y="0.8839" />
            <PreSize X="0.0637" Y="0.0506" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="date_selector" ActionTag="-1820240615" Tag="199" IconVisible="False" LeftMargin="682.5181" RightMargin="72.4819" TopMargin="48.5216" BottomMargin="501.4784" ctype="SpriteObjectData">
            <Size X="187.0000" Y="43.0000" />
            <Children>
              <AbstractNodeData Name="txt" ActionTag="-817233872" Tag="200" IconVisible="False" LeftMargin="38.0020" RightMargin="73.9980" TopMargin="6.5005" BottomMargin="6.4995" FontSize="30" LabelText="当 天" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="75.0000" Y="30.0000" />
                <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
                <Position X="113.0020" Y="21.4995" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.6043" Y="0.5000" />
                <PreSize X="0.4011" Y="0.6977" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="776.0181" Y="522.9784" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8238" Y="0.8819" />
            <PreSize X="0.1985" Y="0.0725" />
            <FileData Type="Normal" Path="hall/res/info/date_select_bg.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="header" ActionTag="1859986379" Tag="201" IconVisible="False" LeftMargin="42.0001" RightMargin="41.9999" TopMargin="118.5571" BottomMargin="394.4429" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="858.0000" Y="80.0000" />
            <Children>
              <AbstractNodeData Name="timeTip" ActionTag="-325335405" Tag="202" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="67.6050" RightMargin="715.3950" TopMargin="25.0000" BottomMargin="25.0000" FontSize="30" LabelText="时 间" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="75.0000" Y="30.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="105.1050" Y="40.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="146" B="0" />
                <PrePosition X="0.1225" Y="0.5000" />
                <PreSize X="0.0874" Y="0.3750" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="amountTip" ActionTag="549151340" Tag="203" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="261.7500" RightMargin="476.2500" TopMargin="25.0000" BottomMargin="25.0000" FontSize="30" LabelText="金额(￥)" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="120.0000" Y="30.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="321.7500" Y="40.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="146" B="0" />
                <PrePosition X="0.3750" Y="0.5000" />
                <PreSize X="0.1399" Y="0.3750" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="tradeTypeTip" ActionTag="1284486510" Tag="204" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="476.2500" RightMargin="261.7500" TopMargin="23.9120" BottomMargin="26.0880" FontSize="30" LabelText="交易方式" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="120.0000" Y="30.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="536.2500" Y="41.0880" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="146" B="0" />
                <PrePosition X="0.6250" Y="0.5136" />
                <PreSize X="0.1399" Y="0.3750" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="statusTip" ActionTag="-1299177709" Tag="205" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="720.7500" RightMargin="77.2500" TopMargin="25.0000" BottomMargin="25.0000" FontSize="30" LabelText="状态" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="60.0000" Y="30.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="750.7500" Y="40.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="146" B="0" />
                <PrePosition X="0.8750" Y="0.5000" />
                <PreSize X="0.0699" Y="0.3750" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="line" ActionTag="464078514" Tag="207" IconVisible="False" PositionPercentYEnabled="True" TopMargin="77.0000" ctype="SpriteObjectData">
                <Size X="858.0000" Y="3.0000" />
                <AnchorPoint ScaleX="0.5000" />
                <Position X="429.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" />
                <PreSize X="1.0000" Y="0.0375" />
                <FileData Type="Normal" Path="hall/res/info/line.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="471.0001" Y="434.4429" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.7326" />
            <PreSize X="0.9108" Y="0.1349" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="containor" ActionTag="488212232" Tag="321" IconVisible="False" LeftMargin="42.0000" RightMargin="42.0000" TopMargin="218.0041" BottomMargin="54.9959" TouchEnable="True" ClipAble="True" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="858.0000" Y="320.0000" />
            <AnchorPoint ScaleX="0.5000" />
            <Position X="471.0000" Y="54.9959" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.0927" />
            <PreSize X="0.9108" Y="0.5396" />
            <SingleColor A="255" R="255" G="0" B="0" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>