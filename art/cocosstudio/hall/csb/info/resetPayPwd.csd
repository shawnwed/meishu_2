<GameFile>
  <PropertyGroup Name="resetPayPwd" Type="Layer" ID="9aa40c49-172d-4cc7-bac1-1e1b527d56fb" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="1047" ctype="GameLayerObjectData">
        <Size X="1280.0000" Y="720.0000" />
        <Children>
          <AbstractNodeData Name="mask" ActionTag="857748001" Tag="1048" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" ClipAble="False" BackColorAlpha="178" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="1280.0000" Y="720.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="640.0000" Y="360.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="1.0000" Y="1.0000" />
            <SingleColor A="255" R="0" G="0" B="0" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="bg" ActionTag="-1464458476" Tag="1049" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" LeftMargin="227.3280" RightMargin="227.3280" TopMargin="21.6000" BottomMargin="21.6000" TouchEnable="True" Scale9Enable="True" LeftEage="220" RightEage="220" TopEage="150" BottomEage="104" Scale9OriginX="220" Scale9OriginY="150" Scale9Width="475" Scale9Height="322" ctype="ImageViewObjectData">
            <Size X="825.3440" Y="676.8000" />
            <Children>
              <AbstractNodeData Name="title" ActionTag="582415075" Tag="1050" IconVisible="False" PositionPercentXEnabled="True" VerticalEdge="TopEdge" LeftMargin="327.1721" RightMargin="327.1719" TopMargin="17.5085" BottomMargin="607.2914" ctype="SpriteObjectData">
                <Size X="171.0000" Y="52.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="412.6721" Y="633.2914" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.9357" />
                <PreSize X="0.2072" Y="0.0768" />
                <FileData Type="Normal" Path="hall/res/info/mimaguanlibiaoti.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="tip1" ActionTag="-1262387949" Tag="99" IconVisible="False" PositionPercentXEnabled="True" VerticalEdge="TopEdge" LeftMargin="414.1827" RightMargin="48.1613" TopMargin="107.6238" BottomMargin="544.1762" FontSize="25" LabelText="通过安全问题+手机验证双重校验" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="363.0000" Y="25.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="595.6827" Y="556.6762" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="0" B="0" />
                <PrePosition X="0.7217" Y="0.8225" />
                <PreSize X="0.4398" Y="0.0369" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_2" ActionTag="1746369575" Tag="100" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="41.0000" RightMargin="634.3440" TopMargin="116.0009" BottomMargin="535.7991" FontSize="25" LabelText="安全问题校验" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="150.0000" Y="25.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="41.0000" Y="548.2991" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="81" G="70" B="69" />
                <PrePosition X="0.0497" Y="0.8101" />
                <PreSize X="0.1817" Y="0.0369" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_3" ActionTag="-2049813993" Tag="101" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="67.0699" RightMargin="654.2741" TopMargin="152.0699" BottomMargin="501.7301" FontSize="23" LabelText="安全问题:" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="104.0000" Y="23.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="119.0699" Y="513.2301" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="165" G="42" B="42" />
                <PrePosition X="0.1443" Y="0.7583" />
                <PreSize X="0.1260" Y="0.0340" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="security" ActionTag="1244029246" Tag="102" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="174.1898" RightMargin="444.1542" TopMargin="152.0699" BottomMargin="501.7301" FontSize="23" LabelText="我的狗叫什么名字？" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="207.0000" Y="23.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="174.1898" Y="513.2301" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="165" G="42" B="42" />
                <PrePosition X="0.2111" Y="0.7583" />
                <PreSize X="0.2508" Y="0.0340" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_5" ActionTag="-640058911" Tag="103" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="51.9997" RightMargin="647.3443" TopMargin="198.4804" BottomMargin="450.3196" FontSize="28" LabelText="问题答案:" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="126.0000" Y="28.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="51.9997" Y="464.3196" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="165" G="42" B="42" />
                <PrePosition X="0.0630" Y="0.6861" />
                <PreSize X="0.1527" Y="0.0414" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="answer" ActionTag="1613328046" Tag="104" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="184.2499" RightMargin="241.0941" TopMargin="187.4800" BottomMargin="439.3200" Scale9Enable="True" LeftEage="33" RightEage="33" TopEage="18" BottomEage="18" Scale9OriginX="33" Scale9OriginY="18" Scale9Width="433" Scale9Height="26" ctype="ImageViewObjectData">
                <Size X="400.0000" Y="50.0000" />
                <Children>
                  <AbstractNodeData Name="input" ActionTag="-248450363" Tag="105" IconVisible="False" PositionPercentYEnabled="True" PercentHeightEnable="True" PercentHeightEnabled="True" HorizontalEdge="BothEdge" LeftMargin="3.0000" RightMargin="5.0000" TouchEnable="True" StretchWidthEnable="True" FontSize="30" IsCustomSize="True" LabelText="" PlaceHolderText="" MaxLengthText="10" ctype="TextFieldObjectData">
                    <Size X="392.0000" Y="50.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="3.0000" Y="25.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.0075" Y="0.5000" />
                    <PreSize X="0.9800" Y="1.0000" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleY="0.5000" />
                <Position X="184.2499" Y="464.3200" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2232" Y="0.6861" />
                <PreSize X="0.4846" Y="0.0739" />
                <FileData Type="Normal" Path="hall/res/info/edt_white.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_6" ActionTag="-1959011391" Tag="106" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="40.9995" RightMargin="634.3445" TopMargin="252.5186" BottomMargin="399.2814" FontSize="25" LabelText="绑定手机校验" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="150.0000" Y="25.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="40.9995" Y="411.7814" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="81" G="70" B="69" />
                <PrePosition X="0.0497" Y="0.6084" />
                <PreSize X="0.1817" Y="0.0369" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_7" ActionTag="1440115370" Tag="107" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="67.0698" RightMargin="654.2742" TopMargin="292.5702" BottomMargin="361.2298" FontSize="23" LabelText="手机号码:" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="104.0000" Y="23.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="119.0698" Y="372.7298" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="165" G="42" B="42" />
                <PrePosition X="0.1443" Y="0.5507" />
                <PreSize X="0.1260" Y="0.0340" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="phoneNo" ActionTag="1793296523" Tag="108" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="174.1897" RightMargin="519.1543" TopMargin="292.5702" BottomMargin="361.2298" FontSize="23" LabelText="139****1234" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="132.0000" Y="23.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="174.1897" Y="372.7298" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="165" G="42" B="42" />
                <PrePosition X="0.2111" Y="0.5507" />
                <PreSize X="0.1599" Y="0.0340" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_9" ActionTag="1141891134" Tag="109" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="51.9992" RightMargin="647.3448" TopMargin="340.2699" BottomMargin="308.5301" FontSize="28" LabelText="验 证 码:" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="126.0000" Y="28.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="51.9992" Y="322.5301" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="165" G="42" B="42" />
                <PrePosition X="0.0630" Y="0.4766" />
                <PreSize X="0.1527" Y="0.0414" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="verify" ActionTag="-115870579" Tag="110" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="184.2495" RightMargin="441.0945" TopMargin="329.2699" BottomMargin="297.5301" Scale9Enable="True" LeftEage="33" RightEage="33" TopEage="18" BottomEage="18" Scale9OriginX="33" Scale9OriginY="18" Scale9Width="433" Scale9Height="26" ctype="ImageViewObjectData">
                <Size X="200.0000" Y="50.0000" />
                <Children>
                  <AbstractNodeData Name="input" ActionTag="-745553372" Tag="137" IconVisible="False" PositionPercentYEnabled="True" PercentHeightEnable="True" PercentHeightEnabled="True" HorizontalEdge="BothEdge" LeftMargin="3.0000" RightMargin="3.0000" TouchEnable="True" StretchWidthEnable="True" FontSize="30" IsCustomSize="True" LabelText="" PlaceHolderText="" MaxLengthText="10" ctype="TextFieldObjectData">
                    <Size X="194.0000" Y="50.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="3.0000" Y="25.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.0150" Y="0.5000" />
                    <PreSize X="0.9700" Y="1.0000" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleY="0.5000" />
                <Position X="184.2495" Y="322.5301" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2232" Y="0.4766" />
                <PreSize X="0.2423" Y="0.0739" />
                <FileData Type="Normal" Path="hall/res/info/edt_white.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="getVerifyBtn" ActionTag="-1842173160" Tag="111" IconVisible="True" VerticalEdge="TopEdge" LeftMargin="400.3549" RightMargin="234.9891" TopMargin="321.7200" BottomMargin="291.0800" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
                <Size X="190.0000" Y="64.0000" />
                <AnchorPoint />
                <Position X="400.3549" Y="291.0800" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4851" Y="0.4301" />
                <PreSize X="0.2302" Y="0.0946" />
                <FileData Type="Normal" Path="hall/csb/info/repeat.csd" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_18" ActionTag="1801473754" Tag="136" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="40.9995" RightMargin="634.3445" TopMargin="398.1949" BottomMargin="253.6051" FontSize="25" LabelText="重置支付密码" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="150.0000" Y="25.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="40.9995" Y="266.1051" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="81" G="70" B="69" />
                <PrePosition X="0.0497" Y="0.3932" />
                <PreSize X="0.1817" Y="0.0369" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_19" ActionTag="-876864918" Tag="138" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="51.9995" RightMargin="647.3445" TopMargin="453.5802" BottomMargin="195.2198" FontSize="28" LabelText="重置密码:" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="126.0000" Y="28.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="51.9995" Y="209.2198" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="165" G="42" B="42" />
                <PrePosition X="0.0630" Y="0.3091" />
                <PreSize X="0.1527" Y="0.0414" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="pwd" ActionTag="1162538046" Tag="139" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="184.2495" RightMargin="241.0945" TopMargin="442.5802" BottomMargin="184.2198" Scale9Enable="True" LeftEage="33" RightEage="33" TopEage="18" BottomEage="18" Scale9OriginX="33" Scale9OriginY="18" Scale9Width="433" Scale9Height="26" ctype="ImageViewObjectData">
                <Size X="400.0000" Y="50.0000" />
                <Children>
                  <AbstractNodeData Name="input" ActionTag="2113140872" Tag="140" IconVisible="False" PositionPercentYEnabled="True" PercentHeightEnable="True" PercentHeightEnabled="True" HorizontalEdge="BothEdge" LeftMargin="3.0000" RightMargin="3.0000" TouchEnable="True" StretchWidthEnable="True" FontSize="30" IsCustomSize="True" LabelText="" PlaceHolderText="支付密码的长度为6位" MaxLengthEnable="True" MaxLengthText="6" PasswordEnable="True" ctype="TextFieldObjectData">
                    <Size X="394.0000" Y="50.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="3.0000" Y="25.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.0075" Y="0.5000" />
                    <PreSize X="0.9850" Y="1.0000" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleY="0.5000" />
                <Position X="184.2495" Y="209.2198" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2232" Y="0.3091" />
                <PreSize X="0.4846" Y="0.0739" />
                <FileData Type="Normal" Path="hall/res/info/edt_white.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_20" ActionTag="847504373" Tag="141" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="51.9995" RightMargin="647.3445" TopMargin="522.1300" BottomMargin="126.6700" FontSize="28" LabelText="确认密码:" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="126.0000" Y="28.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="51.9995" Y="140.6700" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="165" G="42" B="42" />
                <PrePosition X="0.0630" Y="0.2078" />
                <PreSize X="0.1527" Y="0.0414" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="pwd1" ActionTag="1839528340" Tag="142" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="184.2495" RightMargin="241.0945" TopMargin="511.1302" BottomMargin="115.6698" Scale9Enable="True" LeftEage="33" RightEage="33" TopEage="18" BottomEage="18" Scale9OriginX="33" Scale9OriginY="18" Scale9Width="433" Scale9Height="26" ctype="ImageViewObjectData">
                <Size X="400.0000" Y="50.0000" />
                <Children>
                  <AbstractNodeData Name="input" ActionTag="1879384276" Tag="143" IconVisible="False" PercentHeightEnable="True" PercentHeightEnabled="True" HorizontalEdge="BothEdge" VerticalEdge="TopEdge" LeftMargin="3.0000" RightMargin="3.0000" TouchEnable="True" StretchWidthEnable="True" FontSize="30" IsCustomSize="True" LabelText="" PlaceHolderText="" MaxLengthEnable="True" MaxLengthText="6" PasswordEnable="True" ctype="TextFieldObjectData">
                    <Size X="394.0000" Y="50.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="3.0000" Y="25.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.0075" Y="0.5000" />
                    <PreSize X="0.9850" Y="1.0000" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleY="0.5000" />
                <Position X="184.2495" Y="140.6698" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2232" Y="0.2078" />
                <PreSize X="0.4846" Y="0.0739" />
                <FileData Type="Normal" Path="hall/res/info/edt_white.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="confirmBtn" ActionTag="-2114770514" Tag="144" IconVisible="False" PositionPercentXEnabled="True" VerticalEdge="BottomEdge" LeftMargin="163.2193" RightMargin="189.1247" TopMargin="576.5815" BottomMargin="32.2185" ctype="SpriteObjectData">
                <Size X="473.0000" Y="68.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="399.7193" Y="66.2185" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4843" Y="0.0978" />
                <PreSize X="0.5731" Y="0.1005" />
                <FileData Type="Normal" Path="hall/res/info/xiugaizhifumima.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="tip" ActionTag="1006816012" Tag="686" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="59.0054" RightMargin="766.3386" TopMargin="582.1794" BottomMargin="94.6206" FontSize="20" LabelText="" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="59.0054" Y="94.6206" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="0" B="0" />
                <PrePosition X="0.0715" Y="0.1398" />
                <PreSize X="0.0000" Y="0.0000" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="backBtn" ActionTag="837429671" Tag="119" IconVisible="False" HorizontalEdge="LeftEdge" VerticalEdge="TopEdge" LeftMargin="763.7772" RightMargin="-18.4332" TopMargin="-4.3314" BottomMargin="600.1313" ctype="SpriteObjectData">
                <Size X="80.0000" Y="81.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="803.7772" Y="640.6313" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.9739" Y="0.9466" />
                <PreSize X="0.0969" Y="0.1197" />
                <FileData Type="Normal" Path="hall/res/info/x.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="640.0000" Y="360.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="0.6448" Y="0.9400" />
            <FileData Type="Normal" Path="hall/res/bingphone/bg.png" Plist="" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>