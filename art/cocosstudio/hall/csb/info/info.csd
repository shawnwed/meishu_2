<GameFile>
  <PropertyGroup Name="info" Type="Layer" ID="8f7dffd9-0e84-477b-b3af-2b5ad6eb3c83" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="808" ctype="GameLayerObjectData">
        <Size X="942.0000" Y="593.0000" />
        <Children>
          <AbstractNodeData Name="head" ActionTag="-243206365" Tag="821" IconVisible="True" LeftMargin="140.1290" RightMargin="659.8710" TopMargin="108.6268" BottomMargin="342.3732" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="True" ctype="ProjectNodeObjectData">
            <Size X="142.0000" Y="142.0000" />
            <Children>
              <AbstractNodeData Name="head_edit_1" ActionTag="159167029" Tag="1154" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="3.0000" RightMargin="3.0000" TopMargin="95.0038" BottomMargin="2.9962" ctype="SpriteObjectData">
                <Size X="136.0000" Y="44.0000" />
                <AnchorPoint ScaleX="0.5000" />
                <Position X="71.0000" Y="2.9962" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.0211" />
                <PreSize X="0.9577" Y="0.3099" />
                <FileData Type="MarkedSubImage" Path="hall/res/info/head_edit.png" Plist="hall/res/myinfo.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position X="140.1290" Y="342.3732" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.1488" Y="0.5774" />
            <PreSize X="0.1507" Y="0.2395" />
            <FileData Type="Normal" Path="hall/csb/head.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="name_label" ActionTag="99108664" Tag="832" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="138.8184" RightMargin="659.1816" TopMargin="268.3179" BottomMargin="291.6821" FontSize="32" LabelText="南宫紫菱 " ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="144.0000" Y="33.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="210.8184" Y="308.1821" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="140" G="88" B="62" />
            <PrePosition X="0.2238" Y="0.5197" />
            <PreSize X="0.1529" Y="0.0556" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="diamond" ActionTag="-759283249" Tag="843" IconVisible="False" LeftMargin="-34.3939" RightMargin="976.3939" TopMargin="391.9288" BottomMargin="201.0712" FontSize="28" LabelText="" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="-34.3939" Y="201.0712" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="165" G="219" B="255" />
            <PrePosition X="-0.0365" Y="0.3391" />
            <PreSize X="0.0000" Y="0.0000" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="gold" ActionTag="1023114636" Tag="836" IconVisible="False" LeftMargin="12.4529" RightMargin="929.5471" TopMargin="445.1636" BottomMargin="147.8364" FontSize="28" LabelText="" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="12.4529" Y="147.8364" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="165" G="219" B="255" />
            <PrePosition X="0.0132" Y="0.2493" />
            <PreSize X="0.0000" Y="0.0000" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="coin_bg_1" ActionTag="192522376" Alpha="178" Tag="128" IconVisible="False" LeftMargin="90.0325" RightMargin="571.9675" TopMargin="396.6436" BottomMargin="146.3564" ctype="SpriteObjectData">
            <Size X="280.0000" Y="50.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="230.0325" Y="171.3564" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.2442" Y="0.2890" />
            <PreSize X="0.2972" Y="0.0843" />
            <FileData Type="Normal" Path="hall/res/info/coin_bg.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="coin" ActionTag="388493778" Tag="834" IconVisible="False" LeftMargin="151.6064" RightMargin="764.3936" TopMargin="412.1642" BottomMargin="155.8358" FontSize="25" LabelText="55" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="26.0000" Y="25.0000" />
            <AnchorPoint />
            <Position X="151.6064" Y="155.8358" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.1609" Y="0.2628" />
            <PreSize X="0.0276" Y="0.0422" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="invited_2" ActionTag="-715641839" Tag="129" IconVisible="False" LeftMargin="100.9975" RightMargin="814.0025" TopMargin="406.6436" BottomMargin="156.3564" ctype="SpriteObjectData">
            <Size X="27.0000" Y="30.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="114.4975" Y="171.3564" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.1215" Y="0.2890" />
            <PreSize X="0.0287" Y="0.0506" />
            <FileData Type="MarkedSubImage" Path="hall/res/first/coin_money.png" Plist="hall/res/first.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="id" ActionTag="1485910578" Tag="131" IconVisible="False" LeftMargin="206.6985" RightMargin="699.3015" TopMargin="320.7557" BottomMargin="236.2443" FontSize="36" LabelText="55" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="36.0000" Y="36.0000" />
            <AnchorPoint />
            <Position X="206.6985" Y="236.2443" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="167" G="139" B="124" />
            <PrePosition X="0.2194" Y="0.3984" />
            <PreSize X="0.0382" Y="0.0607" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="sp_id" ActionTag="-122817695" Tag="237" IconVisible="False" LeftMargin="138.3668" RightMargin="763.6332" TopMargin="311.6527" BottomMargin="241.3473" ctype="SpriteObjectData">
            <Size X="40.0000" Y="40.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="158.3668" Y="261.3473" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.1681" Y="0.4407" />
            <PreSize X="0.0425" Y="0.0675" />
            <FileData Type="Normal" Path="hall/res/info/invited.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="btn_psw" ActionTag="412994828" Tag="223" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="668.7258" RightMargin="46.2742" TopMargin="107.9885" BottomMargin="411.0115" ctype="SpriteObjectData">
            <Size X="227.0000" Y="74.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="668.7258" Y="448.0115" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.7099" Y="0.7555" />
            <PreSize X="0.2410" Y="0.1248" />
            <FileData Type="MarkedSubImage" Path="hall/res/info/baseinfo_sjbd.png" Plist="hall/res/myinfo.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="btn_logout" ActionTag="-1172774430" Tag="250" IconVisible="False" LeftMargin="668.6878" RightMargin="46.3122" TopMargin="235.0093" BottomMargin="283.9907" ctype="SpriteObjectData">
            <Size X="227.0000" Y="74.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="782.1878" Y="320.9907" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8303" Y="0.5413" />
            <PreSize X="0.2410" Y="0.1248" />
            <FileData Type="MarkedSubImage" Path="hall/res/info/baseinfo_logout.png" Plist="hall/res/myinfo.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="btn_exchange" ActionTag="328007573" Tag="252" IconVisible="False" LeftMargin="668.6878" RightMargin="46.3122" TopMargin="362.0300" BottomMargin="156.9700" ctype="SpriteObjectData">
            <Size X="227.0000" Y="74.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="782.1878" Y="193.9700" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8303" Y="0.3271" />
            <PreSize X="0.2410" Y="0.1248" />
            <FileData Type="MarkedSubImage" Path="hall/res/info/baseinfo_exchange.png" Plist="hall/res/myinfo.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="head_tip" ActionTag="1604546311" VisibleForFrame="False" Tag="23" IconVisible="False" LeftMargin="283.4992" RightMargin="378.5008" TopMargin="118.5033" BottomMargin="354.4967" ctype="SpriteObjectData">
            <Size X="280.0000" Y="120.0000" />
            <Children>
              <AbstractNodeData Name="Text_1" ActionTag="-58913199" Tag="24" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="35.9920" RightMargin="10.0080" TopMargin="34.0000" BottomMargin="34.0000" FontSize="26" LabelText="完成修改头像和修改&#xA;昵称可领取奖励哦!" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="234.0000" Y="52.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="152.9920" Y="60.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5464" Y="0.5000" />
                <PreSize X="0.8357" Y="0.4333" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="423.4992" Y="414.4967" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4496" Y="0.6990" />
            <PreSize X="0.2972" Y="0.2024" />
            <FileData Type="Normal" Path="hall/res/info/head_tip.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="txt_ma" ActionTag="-801424227" Tag="186" IconVisible="False" LeftMargin="115.3566" RightMargin="738.6434" TopMargin="352.7618" BottomMargin="218.2382" FontSize="22" LabelText="(邀请码)" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="88.0000" Y="22.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="159.3566" Y="229.2382" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="167" G="139" B="124" />
            <PrePosition X="0.1692" Y="0.3866" />
            <PreSize X="0.0934" Y="0.0371" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>