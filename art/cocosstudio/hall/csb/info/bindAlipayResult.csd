<GameFile>
  <PropertyGroup Name="bindAlipayResult" Type="Layer" ID="b1f5308c-8d96-499f-86b2-127840733bde" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="268" ctype="GameLayerObjectData">
        <Size X="700.0000" Y="700.0000" />
        <Children>
          <AbstractNodeData Name="tipTxt" ActionTag="352145447" Tag="81" IconVisible="False" LeftMargin="84.0000" RightMargin="84.0000" TopMargin="309.0478" BottomMargin="362.9522" FontSize="28" LabelText="支付宝兑换正在维护中，请使用网银兑换。" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="532.0000" Y="28.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="350.0000" Y="376.9522" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5385" />
            <PreSize X="0.7600" Y="0.0400" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="content" ActionTag="-1806747670" Tag="83" IconVisible="False" TouchEnable="True" ClipAble="False" BackColorAlpha="0" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="700.0000" Y="700.0000" />
            <Children>
              <AbstractNodeData Name="bankName" ActionTag="1103365699" Tag="271" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="70.9253" RightMargin="475.0747" TopMargin="252.4835" BottomMargin="419.5165" FontSize="28" LabelText="支付宝账号:" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="154.0000" Y="28.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="147.9253" Y="433.5165" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="243" G="201" B="41" />
                <PrePosition X="0.2113" Y="0.6193" />
                <PreSize X="0.2200" Y="0.0400" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="bankCardNo" ActionTag="-2110402602" Tag="269" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="229.8395" RightMargin="70.1605" TopMargin="241.4833" BottomMargin="408.5167" Scale9Enable="True" LeftEage="33" RightEage="33" TopEage="18" BottomEage="18" Scale9OriginX="33" Scale9OriginY="18" Scale9Width="214" Scale9Height="18" ctype="ImageViewObjectData">
                <Size X="400.0000" Y="50.0000" />
                <Children>
                  <AbstractNodeData Name="cardNo" ActionTag="1555813284" Tag="89" IconVisible="False" LeftMargin="25.4828" RightMargin="119.5172" TopMargin="9.7677" BottomMargin="10.2323" FontSize="30" LabelText="15458787845452121" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="255.0000" Y="30.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="25.4828" Y="25.2323" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.0637" Y="0.5046" />
                    <PreSize X="0.6375" Y="0.6000" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleY="0.5000" />
                <Position X="229.8395" Y="433.5167" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3283" Y="0.6193" />
                <PreSize X="0.5714" Y="0.0714" />
                <FileData Type="Normal" Path="hall/res/info/edt_white.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="accumulatePointBtn" ActionTag="-2001723011" Tag="272" IconVisible="False" VerticalEdge="BottomEdge" LeftMargin="326.9996" RightMargin="327.0004" TopMargin="604.0527" BottomMargin="49.9473" ctype="SpriteObjectData">
                <Size X="227.0000" Y="74.0000" />
                <Children>
                  <AbstractNodeData Name="Text_13" ActionTag="-821136127" Tag="273" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-46.4940" RightMargin="-47.5060" TopMargin="6.1210" BottomMargin="4.8790" FontSize="35" LabelText="筹码兑换" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="140.0000" Y="35.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="23.5060" Y="22.3790" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="50" G="24" B="24" />
                    <PrePosition X="0.5110" Y="0.4865" />
                    <PreSize X="3.0435" Y="0.7609" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="349.9996" Y="72.9473" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.1042" />
                <PreSize X="0.0657" Y="0.0657" />
                <FileData Type="Normal" Path="hall/res/info/btn_left.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="1.0000" Y="1.0000" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>