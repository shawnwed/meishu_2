<GameFile>
  <PropertyGroup Name="bindAlipayItem" Type="Layer" ID="6c891518-8396-46c6-96b0-de17f75020ce" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="232" ctype="GameLayerObjectData">
        <Size X="700.0000" Y="700.0000" />
        <Children>
          <AbstractNodeData Name="tipTxt" ActionTag="394809650" Tag="56" IconVisible="False" LeftMargin="84.0000" RightMargin="84.0000" TopMargin="309.0478" BottomMargin="362.9522" FontSize="28" LabelText="支付宝兑换正在维护中，请使用网银兑换。" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="532.0000" Y="28.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="350.0000" Y="376.9522" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5385" />
            <PreSize X="0.7600" Y="0.0400" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="content" ActionTag="396024733" Tag="16" IconVisible="False" LeftMargin="1.4021" RightMargin="-1.4021" TopMargin="-1.4021" BottomMargin="1.4021" TouchEnable="True" ClipAble="False" BackColorAlpha="0" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="700.0000" Y="700.0000" />
            <Children>
              <AbstractNodeData Name="Text_4" ActionTag="2084747179" Tag="236" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="92.1894" RightMargin="453.8106" TopMargin="287.9100" BottomMargin="384.0900" FontSize="28" LabelText="支付宝账号:" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="154.0000" Y="28.0000" />
                <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
                <Position X="246.1894" Y="398.0900" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="243" G="201" B="41" />
                <PrePosition X="0.3517" Y="0.5687" />
                <PreSize X="0.2200" Y="0.0400" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="bankCardNo" ActionTag="-867837673" Tag="237" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="255.8314" RightMargin="44.1686" TopMargin="273.5100" BottomMargin="372.4900" Scale9Enable="True" LeftEage="33" RightEage="33" TopEage="18" BottomEage="18" Scale9OriginX="33" Scale9OriginY="18" Scale9Width="214" Scale9Height="18" ctype="ImageViewObjectData">
                <Size X="400.0000" Y="54.0000" />
                <Children>
                  <AbstractNodeData Name="input" ActionTag="-796448648" Tag="238" IconVisible="False" PositionPercentYEnabled="True" PercentHeightEnable="True" PercentHeightEnabled="True" HorizontalEdge="BothEdge" LeftMargin="3.0000" RightMargin="3.0000" TouchEnable="True" StretchWidthEnable="True" FontSize="30" IsCustomSize="True" LabelText="" PlaceHolderText="" MaxLengthText="10" ctype="TextFieldObjectData">
                    <Size X="394.0000" Y="54.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="3.0000" Y="27.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.0075" Y="0.5000" />
                    <PreSize X="0.9850" Y="1.0000" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleY="0.5000" />
                <Position X="255.8314" Y="399.4900" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3655" Y="0.5707" />
                <PreSize X="0.5714" Y="0.0771" />
                <FileData Type="Normal" Path="hall/res/info/edt_white.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_5" ActionTag="-515130137" Tag="239" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="64.1891" RightMargin="453.8109" TopMargin="380.0300" BottomMargin="291.9700" FontSize="28" LabelText="设置支付密码:" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="182.0000" Y="28.0000" />
                <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
                <Position X="246.1891" Y="305.9700" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="243" G="201" B="41" />
                <PrePosition X="0.3517" Y="0.4371" />
                <PreSize X="0.2600" Y="0.0400" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="payPwd" ActionTag="370207057" Tag="240" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="255.8314" RightMargin="44.1686" TopMargin="370.7800" BottomMargin="279.2200" Scale9Enable="True" LeftEage="33" RightEage="33" TopEage="18" BottomEage="18" Scale9OriginX="33" Scale9OriginY="18" Scale9Width="214" Scale9Height="18" ctype="ImageViewObjectData">
                <Size X="400.0000" Y="50.0000" />
                <Children>
                  <AbstractNodeData Name="input" ActionTag="378921081" Tag="241" IconVisible="False" PositionPercentYEnabled="True" PercentHeightEnable="True" PercentHeightEnabled="True" HorizontalEdge="BothEdge" LeftMargin="3.0000" RightMargin="151.0000" TouchEnable="True" StretchWidthEnable="True" FontSize="30" IsCustomSize="True" LabelText="" PlaceHolderText="" MaxLengthEnable="True" MaxLengthText="6" PasswordEnable="True" ctype="TextFieldObjectData">
                    <Size X="246.0000" Y="50.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="3.0000" Y="25.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.0075" Y="0.5000" />
                    <PreSize X="0.6150" Y="1.0000" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="forgetBtn" ActionTag="-1650579403" Tag="242" IconVisible="False" PositionPercentYEnabled="True" HorizontalEdge="RightEdge" LeftMargin="251.4859" RightMargin="-0.4859" TopMargin="2.0000" BottomMargin="-4.0000" ctype="SpriteObjectData">
                    <Size X="149.0000" Y="52.0000" />
                    <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
                    <Position X="400.4859" Y="22.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="1.0012" Y="0.4400" />
                    <PreSize X="0.3725" Y="1.0400" />
                    <FileData Type="Normal" Path="hall/res/info/forgetPwdBtn.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleY="0.5000" />
                <Position X="255.8314" Y="304.2200" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3655" Y="0.4346" />
                <PreSize X="0.5714" Y="0.0714" />
                <FileData Type="Normal" Path="hall/res/info/edt_white.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="confirmBtn" ActionTag="-724349913" Tag="243" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="254.8807" RightMargin="218.1193" TopMargin="471.2700" BottomMargin="154.7300" ctype="SpriteObjectData">
                <Size X="227.0000" Y="74.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="368.3807" Y="191.7300" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5263" Y="0.2739" />
                <PreSize X="0.3243" Y="0.1057" />
                <FileData Type="Normal" Path="hall/res/info/ok.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="tip" ActionTag="-1061109636" Tag="244" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="114.4792" RightMargin="585.5208" TopMargin="443.2298" BottomMargin="256.7702" FontSize="20" LabelText="" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="114.4792" Y="256.7702" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="0" B="0" />
                <PrePosition X="0.1635" Y="0.3668" />
                <PreSize X="0.0000" Y="0.0000" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_10" ActionTag="-1410537315" Tag="245" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="120.1898" RightMargin="453.8102" TopMargin="195.7900" BottomMargin="476.2100" FontSize="28" LabelText="帐户姓名:" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="126.0000" Y="28.0000" />
                <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
                <Position X="246.1898" Y="490.2100" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="243" G="201" B="41" />
                <PrePosition X="0.3517" Y="0.7003" />
                <PreSize X="0.1800" Y="0.0400" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="name" ActionTag="267786164" Tag="246" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="255.8314" RightMargin="44.1686" TopMargin="180.2953" BottomMargin="469.7047" Scale9Enable="True" LeftEage="33" RightEage="33" TopEage="18" BottomEage="18" Scale9OriginX="33" Scale9OriginY="18" Scale9Width="214" Scale9Height="18" ctype="ImageViewObjectData">
                <Size X="400.0000" Y="50.0000" />
                <Children>
                  <AbstractNodeData Name="input" ActionTag="1389196643" Tag="247" IconVisible="False" PositionPercentYEnabled="True" PercentHeightEnable="True" PercentHeightEnabled="True" HorizontalEdge="BothEdge" LeftMargin="3.0000" RightMargin="3.0000" TouchEnable="True" StretchWidthEnable="True" FontSize="30" IsCustomSize="True" LabelText="" PlaceHolderText="" MaxLengthText="10" ctype="TextFieldObjectData">
                    <Size X="394.0000" Y="50.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="3.0000" Y="25.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.0075" Y="0.5000" />
                    <PreSize X="0.9850" Y="1.0000" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleY="0.5000" />
                <Position X="255.8314" Y="494.7047" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3655" Y="0.7067" />
                <PreSize X="0.5714" Y="0.0714" />
                <FileData Type="Normal" Path="hall/res/info/edt_white.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_27" ActionTag="-1827921719" Tag="249" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="44.6300" RightMargin="104.3700" TopMargin="607.6000" BottomMargin="17.4000" FontSize="25" LabelText="重要提示:&#xA;1.支付宝账号绑定后不可修改,请谨慎填写。&#xA;2.不论绑定支付宝还是网银，支付密码只有一个。" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="551.0000" Y="75.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="44.6300" Y="54.9000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0638" Y="0.0784" />
                <PreSize X="0.7871" Y="0.1071" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position X="1.4021" Y="1.4021" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0020" Y="0.0020" />
            <PreSize X="1.0000" Y="1.0000" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>