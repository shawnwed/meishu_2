<GameFile>
  <PropertyGroup Name="popupWindow1" Type="Node" ID="ca122e6a-5545-4f85-8db4-d4f39091d806" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="7" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="containor" ActionTag="52925635" Tag="8" IconVisible="False" LeftMargin="-75.0000" RightMargin="-75.0000" BottomMargin="-100.0000" TouchEnable="True" ClipAble="True" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="150.0000" Y="100.0000" />
            <Children>
              <AbstractNodeData Name="bg" ActionTag="1086043659" Tag="9" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="2.0000" RightMargin="2.0000" TopMargin="-1.0000" BottomMargin="23.0000" LeftEage="41" RightEage="41" TopEage="21" BottomEage="21" Scale9OriginX="41" Scale9OriginY="21" Scale9Width="64" Scale9Height="36" ctype="ImageViewObjectData">
                <Size X="146.0000" Y="78.0000" />
                <Children>
                  <AbstractNodeData Name="item_1" ActionTag="210890402" Tag="10" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="10.5000" RightMargin="10.5000" TopMargin="6.5976" BottomMargin="11.4024" IsCustomSize="True" FontSize="30" LabelText="哈哈" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="125.0000" Y="60.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="73.0000" Y="41.4024" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5308" />
                    <PreSize X="0.8562" Y="0.7692" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="1.0000" />
                <Position X="75.0000" Y="101.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="1.0100" />
                <PreSize X="0.9733" Y="0.7800" />
                <FileData Type="Normal" Path="hall/res/info/kuang4.png" Plist="" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="1.0000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>