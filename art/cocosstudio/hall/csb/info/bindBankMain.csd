<GameFile>
  <PropertyGroup Name="bindBankMain" Type="Layer" ID="c09ab9e7-1135-4c24-b91e-e4cab7339bc5" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="170" ctype="GameLayerObjectData">
        <Size X="1280.0000" Y="720.0000" />
        <Children>
          <AbstractNodeData Name="mask" ActionTag="966800877" Tag="202" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" ClipAble="False" BackColorAlpha="177" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="1280.0000" Y="720.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="640.0000" Y="360.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="1.0000" Y="1.0000" />
            <SingleColor A="255" R="0" G="0" B="0" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="bg" ActionTag="571966077" Tag="171" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="290.0000" RightMargin="290.0000" TopMargin="10.0000" BottomMargin="10.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Enable="True" LeftEage="229" RightEage="210" TopEage="127" BottomEage="121" Scale9OriginX="229" Scale9OriginY="127" Scale9Width="476" Scale9Height="328" ctype="PanelObjectData">
            <Size X="700.0000" Y="700.0000" />
            <Children>
              <AbstractNodeData Name="content" ActionTag="-1289195626" Tag="311" IconVisible="False" TouchEnable="True" ClipAble="False" BackColorAlpha="0" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="700.0000" Y="700.0000" />
                <AnchorPoint />
                <Position />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="1.0000" Y="1.0000" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="closeBtn" ActionTag="-1455615635" Tag="201" IconVisible="False" HorizontalEdge="RightEdge" VerticalEdge="TopEdge" LeftMargin="634.0784" RightMargin="-14.0784" TopMargin="-3.7688" BottomMargin="622.7688" ctype="SpriteObjectData">
                <Size X="80.0000" Y="81.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="674.0784" Y="663.2688" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.9630" Y="0.9475" />
                <PreSize X="0.1143" Y="0.1157" />
                <FileData Type="Normal" Path="hall/res/info/x.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="titleBG" ActionTag="849765168" Tag="274" IconVisible="False" LeftMargin="92.6001" RightMargin="114.3999" TopMargin="16.0377" BottomMargin="623.9623" ctype="SpriteObjectData">
                <Size X="493.0000" Y="60.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="339.1001" Y="653.9623" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4844" Y="0.9342" />
                <PreSize X="0.7043" Y="0.0857" />
                <FileData Type="MarkedSubImage" Path="hall/res/info/zy_ui_01.png" Plist="hall/res/info/bindBank.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="bankBG" ActionTag="-150055351" Tag="309" IconVisible="False" LeftMargin="336.9661" RightMargin="117.0339" TopMargin="17.1714" BottomMargin="627.8286" ctype="SpriteObjectData">
                <Size X="246.0000" Y="55.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="459.9661" Y="655.3286" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.6571" Y="0.9362" />
                <PreSize X="0.3514" Y="0.0786" />
                <FileData Type="MarkedSubImage" Path="hall/res/info/zy_ui_02.png" Plist="hall/res/info/bindBank.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="btn_bank2" ActionTag="1438318379" Tag="276" IconVisible="False" LeftMargin="375.6246" RightMargin="151.3754" TopMargin="26.9606" BottomMargin="636.0394" ctype="SpriteObjectData">
                <Size X="173.0000" Y="37.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="462.1246" Y="654.5394" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.6602" Y="0.9351" />
                <PreSize X="0.2471" Y="0.0529" />
                <FileData Type="MarkedSubImage" Path="hall/res/info/mimaguanlibiaoti4a.png" Plist="hall/res/info/bindBank.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="aliBg" ActionTag="-1660427546" Tag="310" IconVisible="False" LeftMargin="94.2324" RightMargin="359.7676" TopMargin="17.1714" BottomMargin="627.8286" ctype="SpriteObjectData">
                <Size X="246.0000" Y="55.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="217.2324" Y="655.3286" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3103" Y="0.9362" />
                <PreSize X="0.3514" Y="0.0786" />
                <FileData Type="MarkedSubImage" Path="hall/res/info/zy_ui_02.png" Plist="hall/res/info/bindBank.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="btn_bank1" ActionTag="36966926" Tag="275" IconVisible="False" LeftMargin="122.7707" RightMargin="403.2293" TopMargin="26.9606" BottomMargin="636.0394" ctype="SpriteObjectData">
                <Size X="174.0000" Y="37.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="209.7707" Y="654.5394" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2997" Y="0.9351" />
                <PreSize X="0.2486" Y="0.0529" />
                <FileData Type="MarkedSubImage" Path="hall/res/info/mimaguanlibiaoti5a.png" Plist="hall/res/info/bindBank.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="640.0000" Y="360.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="0.5469" Y="0.9722" />
            <FileData Type="Normal" Path="hall/res/bingphone/bg.png" Plist="" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>