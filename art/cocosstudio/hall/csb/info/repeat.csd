<GameFile>
  <PropertyGroup Name="repeat" Type="Layer" ID="5a0c6aa5-6e73-48fd-adcc-2607ec63287a" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="content" Tag="267" ctype="GameLayerObjectData">
        <Size X="190.0000" Y="64.0000" />
        <Children>
          <AbstractNodeData Name="repeat" ActionTag="-524876258" VisibleForFrame="False" Tag="1190" IconVisible="False" TopMargin="-1.0000" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" LeftEage="66" RightEage="66" TopEage="24" BottomEage="24" Scale9OriginX="66" Scale9OriginY="24" Scale9Width="111" Scale9Height="16" ctype="PanelObjectData">
            <Size X="190.0000" Y="65.0000" />
            <Children>
              <AbstractNodeData Name="Image_1" ActionTag="166542173" Tag="273" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" LeftMargin="-4.9970" RightMargin="-4.9970" TopMargin="-4.5013" BottomMargin="-4.5012" Scale9Enable="True" LeftEage="13" RightEage="16" TopEage="13" BottomEage="16" Scale9OriginX="13" Scale9OriginY="13" Scale9Width="214" Scale9Height="35" ctype="ImageViewObjectData">
                <Size X="199.9940" Y="74.0025" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="95.0000" Y="32.5000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5000" />
                <PreSize X="1.0526" Y="1.1385" />
                <FileData Type="Normal" Path="hall/res/info/kuang.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_3" ActionTag="-942568648" Tag="270" IconVisible="False" LeftMargin="32.3624" RightMargin="57.6376" TopMargin="34.5589" BottomMargin="10.4411" FontSize="20" LabelText="发送验证码" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="100.0000" Y="20.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="82.3624" Y="20.4411" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4335" Y="0.3145" />
                <PreSize X="0.5263" Y="0.3077" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_2" ActionTag="180680106" Tag="271" IconVisible="False" LeftMargin="55.1747" RightMargin="34.8253" TopMargin="9.7470" BottomMargin="35.2530" FontSize="20" LabelText="秒后可重新" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="100.0000" Y="20.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="105.1747" Y="45.2530" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5536" Y="0.6962" />
                <PreSize X="0.5263" Y="0.3077" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="txt" ActionTag="-538201862" Tag="272" IconVisible="False" LeftMargin="28.6223" RightMargin="141.3777" TopMargin="8.9304" BottomMargin="36.0696" FontSize="20" LabelText="60" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="20.0000" Y="20.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="38.6223" Y="46.0696" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="0" B="0" />
                <PrePosition X="0.2033" Y="0.7088" />
                <PreSize X="0.1053" Y="0.3077" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleY="0.5000" />
            <Position Y="32.5000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition Y="0.5078" />
            <PreSize X="1.0000" Y="1.0156" />
            <FileData Type="Normal" Path="hall/res/info/kuang.png" Plist="" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="getVerify" ActionTag="-157624182" Tag="274" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" RightMargin="2.1090" TopMargin="2.8800" BottomMargin="2.8800" Scale9Enable="True" LeftEage="83" RightEage="73" TopEage="17" BottomEage="21" Scale9OriginX="83" Scale9OriginY="17" Scale9Width="87" Scale9Height="26" ctype="ImageViewObjectData">
            <Size X="187.8910" Y="58.2400" />
            <Children>
              <AbstractNodeData Name="txt" ActionTag="-1932418098" Tag="275" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="23.9455" RightMargin="23.9455" TopMargin="15.1200" BottomMargin="15.1200" FontSize="28" LabelText="发送验证码" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="140.0000" Y="28.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="93.9455" Y="29.1200" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5000" />
                <PreSize X="0.7451" Y="0.4808" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleY="0.5000" />
            <Position Y="32.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition Y="0.5000" />
            <PreSize X="0.9889" Y="0.9100" />
            <FileData Type="Normal" Path="hall/res/info/kuang.png" Plist="" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>