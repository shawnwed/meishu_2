<GameFile>
  <PropertyGroup Name="revisePwd" Type="Layer" ID="2c525385-5a10-4af2-abe7-80748b6bdfc7" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="1051" ctype="GameLayerObjectData">
        <Size X="1280.0000" Y="720.0000" />
        <Children>
          <AbstractNodeData Name="mask" ActionTag="-1865356422" Tag="1052" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" TouchEnable="True" ClipAble="False" BackColorAlpha="179" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="1280.0000" Y="720.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="640.0000" Y="360.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="1.0000" Y="1.0000" />
            <SingleColor A="255" R="0" G="0" B="0" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="bg" ActionTag="1268266430" Tag="1061" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="390.0000" RightMargin="390.0000" TopMargin="133.9920" BottomMargin="136.0080" TouchEnable="True" ClipAble="False" BackColorAlpha="0" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Enable="True" LeftEage="275" RightEage="284" TopEage="242" BottomEage="132" Scale9OriginX="275" Scale9OriginY="242" Scale9Width="356" Scale9Height="202" ctype="PanelObjectData">
            <Size X="500.0000" Y="450.0000" />
            <Children>
              <AbstractNodeData Name="title" ActionTag="306721013" Tag="1062" IconVisible="False" PositionPercentXEnabled="True" VerticalEdge="TopEdge" LeftMargin="164.5000" RightMargin="164.5000" TopMargin="21.4730" BottomMargin="376.5270" ctype="SpriteObjectData">
                <Size X="171.0000" Y="52.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="250.0000" Y="402.5270" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.8945" />
                <PreSize X="0.3420" Y="0.1156" />
                <FileData Type="Normal" Path="hall/res/info/mimaguanlibiaoti2.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="currentPwd" ActionTag="357690431" Tag="1187" IconVisible="False" PositionPercentXEnabled="True" VerticalEdge="TopEdge" LeftMargin="54.0000" RightMargin="46.0000" TopMargin="114.2419" BottomMargin="281.7581" Scale9Enable="True" LeftEage="33" RightEage="33" TopEage="18" BottomEage="18" Scale9OriginX="33" Scale9OriginY="18" Scale9Width="433" Scale9Height="26" ctype="ImageViewObjectData">
                <Size X="400.0000" Y="54.0000" />
                <Children>
                  <AbstractNodeData Name="input" ActionTag="-1306822790" Tag="1188" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" LeftMargin="4.0000" RightMargin="4.0000" TopMargin="0.5400" BottomMargin="0.5400" TouchEnable="True" FontSize="25" IsCustomSize="True" LabelText="" PlaceHolderText="请输入当前密码" MaxLengthText="10" PasswordEnable="True" ctype="TextFieldObjectData">
                    <Size X="392.0000" Y="52.9200" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="200.0000" Y="27.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="0.9800" Y="0.9800" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="254.0000" Y="308.7581" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5080" Y="0.6861" />
                <PreSize X="0.8000" Y="0.1200" />
                <FileData Type="Normal" Path="hall/res/info/edt_white.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="newPwd" ActionTag="-1492260749" Tag="1189" IconVisible="False" PositionPercentXEnabled="True" VerticalEdge="TopEdge" LeftMargin="54.0000" RightMargin="46.0000" TopMargin="187.0000" BottomMargin="209.0000" Scale9Enable="True" LeftEage="33" RightEage="33" TopEage="18" BottomEage="18" Scale9OriginX="33" Scale9OriginY="18" Scale9Width="433" Scale9Height="26" ctype="ImageViewObjectData">
                <Size X="400.0000" Y="54.0000" />
                <Children>
                  <AbstractNodeData Name="input" ActionTag="1460901095" Tag="1190" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" LeftMargin="4.0000" RightMargin="4.0000" TouchEnable="True" FontSize="25" IsCustomSize="True" LabelText="" PlaceHolderText="请输入新密码(长度：6~20字符)" MaxLengthText="10" PasswordEnable="True" ctype="TextFieldObjectData">
                    <Size X="392.0000" Y="54.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="200.0000" Y="27.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="0.9800" Y="1.0000" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="254.0000" Y="236.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5080" Y="0.5244" />
                <PreSize X="0.8000" Y="0.1200" />
                <FileData Type="Normal" Path="hall/res/info/edt_white.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="newPwd1" ActionTag="-914405180" Tag="1191" IconVisible="False" PositionPercentXEnabled="True" VerticalEdge="TopEdge" LeftMargin="54.0000" RightMargin="46.0000" TopMargin="260.6517" BottomMargin="135.3483" Scale9Enable="True" LeftEage="33" RightEage="33" TopEage="18" BottomEage="18" Scale9OriginX="33" Scale9OriginY="18" Scale9Width="433" Scale9Height="26" ctype="ImageViewObjectData">
                <Size X="400.0000" Y="54.0000" />
                <Children>
                  <AbstractNodeData Name="input" ActionTag="1544918415" Tag="1192" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" LeftMargin="4.0000" RightMargin="4.0000" TouchEnable="True" FontSize="25" IsCustomSize="True" LabelText="" PlaceHolderText="请确认新密码(长度：6~20字符)" MaxLengthText="10" PasswordEnable="True" ctype="TextFieldObjectData">
                    <Size X="392.0000" Y="54.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="200.0000" Y="27.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="0.9800" Y="1.0000" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="254.0000" Y="162.3483" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5080" Y="0.3608" />
                <PreSize X="0.8000" Y="0.1200" />
                <FileData Type="Normal" Path="hall/res/info/edt_white.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="confirmBtn" ActionTag="-80751197" Tag="1193" IconVisible="False" PositionPercentXEnabled="True" VerticalEdge="BottomEdge" LeftMargin="112.5000" RightMargin="112.5000" TopMargin="326.6676" BottomMargin="28.3324" ctype="SpriteObjectData">
                <Size X="275.0000" Y="95.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="250.0000" Y="75.8324" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.1685" />
                <PreSize X="0.5500" Y="0.2111" />
                <FileData Type="Normal" Path="hall/res/info/ok.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="closeBtn" ActionTag="-1096410472" Tag="1194" IconVisible="False" HorizontalEdge="LeftEdge" VerticalEdge="TopEdge" LeftMargin="431.4866" RightMargin="-11.4866" TopMargin="-9.9525" BottomMargin="378.9525" ctype="SpriteObjectData">
                <Size X="80.0000" Y="81.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="471.4866" Y="419.4525" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.9430" Y="0.9321" />
                <PreSize X="0.1600" Y="0.1800" />
                <FileData Type="Normal" Path="hall/res/info/x.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="tip" ActionTag="-825888464" Tag="715" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="49.5255" RightMargin="450.4745" TopMargin="343.3642" BottomMargin="106.6358" FontSize="20" LabelText="" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="49.5255" Y="106.6358" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="0" B="0" />
                <PrePosition X="0.0991" Y="0.2370" />
                <PreSize X="0.0000" Y="0.0000" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="640.0000" Y="361.0080" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5014" />
            <PreSize X="0.3906" Y="0.6250" />
            <FileData Type="Normal" Path="hall/res/bingphone/bg.png" Plist="" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>