<GameFile>
  <PropertyGroup Name="coin" Type="Layer" ID="c785f43f-c186-4ceb-b9d5-bf0e4f76cd67" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="30" ctype="GameLayerObjectData">
        <Size X="300.0000" Y="52.0000" />
        <Children>
          <AbstractNodeData Name="coin_bg_1" ActionTag="-760931093" Tag="31" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="34.5000" RightMargin="34.5000" TopMargin="4.5000" BottomMargin="4.5000" ctype="SpriteObjectData">
            <Size X="231.0000" Y="43.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="150.0000" Y="26.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="0.7700" Y="0.8269" />
            <FileData Type="MarkedSubImage" Path="hall/res/first/coin_bg.png" Plist="hall/res/first.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="txt" ActionTag="1459326778" Tag="32" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="50.5974" RightMargin="84.4026" TopMargin="11.0000" BottomMargin="11.0000" FontSize="30" LabelText="100,000,000" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="165.0000" Y="30.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="133.0974" Y="26.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4437" Y="0.5000" />
            <PreSize X="0.5500" Y="0.5769" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="coin_add_2" ActionTag="1975750307" Tag="33" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="207.1789" RightMargin="20.8211" TopMargin="-9.5431" BottomMargin="-10.4569" ctype="SpriteObjectData">
            <Size X="72.0000" Y="72.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="243.1789" Y="25.5431" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8106" Y="0.4912" />
            <PreSize X="0.2400" Y="1.3846" />
            <FileData Type="MarkedSubImage" Path="hall/res/first/coin_add.png" Plist="hall/res/first.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>