<GameFile>
  <PropertyGroup Name="feed_back_item" Type="Layer" ID="88d88c24-27a7-46fc-83dc-024bd6f503cb" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="153" ctype="GameLayerObjectData">
        <Size X="1230.0000" Y="100.0000" />
        <Children>
          <AbstractNodeData Name="my" ActionTag="1624293212" Tag="252" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" ClipAble="False" BackColorAlpha="0" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="1230.0000" Y="100.0000" />
            <Children>
              <AbstractNodeData Name="head" ActionTag="1333827917" Tag="248" IconVisible="True" HorizontalEdge="RightEdge" VerticalEdge="TopEdge" LeftMargin="1128.0000" RightMargin="30.0000" TopMargin="20.0008" BottomMargin="7.9992" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
                <Size X="72.0000" Y="72.0000" />
                <AnchorPoint />
                <Position X="1128.0000" Y="7.9992" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.9171" Y="0.0800" />
                <PreSize X="0.0585" Y="0.7200" />
                <FileData Type="Normal" Path="hall/csb/head.csd" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="time" ActionTag="760792952" Tag="245" IconVisible="False" HorizontalEdge="RightEdge" VerticalEdge="TopEdge" LeftMargin="959.2078" RightMargin="141.7922" TopMargin="13.9999" BottomMargin="63.0001" FontSize="20" LabelText="4月12日 16:20" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="129.0000" Y="23.0000" />
                <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
                <Position X="1088.2078" Y="74.5001" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="191" G="191" B="191" />
                <PrePosition X="0.8847" Y="0.7450" />
                <PreSize X="0.1049" Y="0.2300" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="bg" ActionTag="4761470" Tag="246" IconVisible="False" HorizontalEdge="RightEdge" VerticalEdge="TopEdge" LeftMargin="852.2062" RightMargin="110.7938" TopMargin="38.0000" BottomMargin="14.0000" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Enable="True" LeftEage="10" RightEage="25" TopEage="37" BottomEage="17" Scale9OriginX="-25" Scale9OriginY="-17" Scale9Width="35" Scale9Height="54" ctype="PanelObjectData">
                <Size X="267.0000" Y="48.0000" />
                <Children>
                  <AbstractNodeData Name="txt" ActionTag="1802793068" Tag="247" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="-191.9976" RightMargin="30.9976" TopMargin="8.0002" BottomMargin="2.9998" FontSize="32" LabelText="ghfhgfdgd1213123123213213" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="428.0000" Y="37.0000" />
                    <AnchorPoint ScaleX="1.0000" ScaleY="1.0000" />
                    <Position X="236.0024" Y="39.9998" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.8839" Y="0.8333" />
                    <PreSize X="1.6030" Y="0.7708" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="1.0000" ScaleY="1.0000" />
                <Position X="1119.2062" Y="62.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.9099" Y="0.6200" />
                <PreSize X="0.2171" Y="0.4800" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="1.0000" Y="1.0000" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="other" CanEdit="False" ActionTag="1748400867" Tag="253" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="1230.0000" Y="100.0000" />
            <Children>
              <AbstractNodeData Name="head" ActionTag="1351752129" Tag="270" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="30.0000" RightMargin="1128.0000" TopMargin="20.0000" BottomMargin="8.0000" ctype="SpriteObjectData">
                <Size X="72.0000" Y="72.0000" />
                <AnchorPoint />
                <Position X="30.0000" Y="8.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0244" Y="0.0800" />
                <PreSize X="0.0585" Y="0.7200" />
                <FileData Type="MarkedSubImage" Path="hall/res/task/feed_icon.png" Plist="hall/res/task.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="time" ActionTag="-1818040859" Tag="271" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="127.9997" RightMargin="973.0003" TopMargin="14.0000" BottomMargin="63.0000" FontSize="20" LabelText="4月12日 16:20" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="129.0000" Y="23.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="127.9997" Y="74.5000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="191" G="191" B="191" />
                <PrePosition X="0.1041" Y="0.7450" />
                <PreSize X="0.1049" Y="0.2300" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="bg" ActionTag="299995926" Tag="272" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="105.9996" RightMargin="857.0004" TopMargin="38.0000" BottomMargin="14.0000" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Enable="True" LeftEage="25" RightEage="25" TopEage="37" BottomEage="17" Scale9OriginX="-25" Scale9OriginY="-17" Scale9Width="50" Scale9Height="54" ctype="PanelObjectData">
                <Size X="267.0000" Y="48.0000" />
                <Children>
                  <AbstractNodeData Name="txt" ActionTag="103126145" Tag="273" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="22.0001" RightMargin="30.9999" TopMargin="7.9998" BottomMargin="3.0002" FontSize="32" LabelText="100块都不给我" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="214.0000" Y="37.0000" />
                    <AnchorPoint ScaleY="1.0000" />
                    <Position X="22.0001" Y="40.0002" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.0824" Y="0.8333" />
                    <PreSize X="0.8015" Y="0.7708" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleY="1.0000" />
                <Position X="105.9996" Y="62.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0862" Y="0.6200" />
                <PreSize X="0.2171" Y="0.4800" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="1.0000" Y="1.0000" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>