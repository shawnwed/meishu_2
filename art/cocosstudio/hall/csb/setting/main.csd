<GameFile>
  <PropertyGroup Name="main" Type="Scene" ID="fff91ce5-0d37-4c13-a0fe-93a34e10665f" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Scene" Tag="95" ctype="GameNodeObjectData">
        <Size X="960.0000" Y="640.0000" />
        <Children>
          <AbstractNodeData Name="bg_0" ActionTag="-501045492" Tag="128" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" TouchEnable="True" LeftEage="422" RightEage="422" TopEage="237" BottomEage="237" Scale9OriginX="422" Scale9OriginY="237" Scale9Width="436" Scale9Height="246" ctype="ImageViewObjectData">
            <Size X="960.0000" Y="640.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="480.0000" Y="320.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="1.0000" Y="1.0000" />
            <FileData Type="Normal" Path="bg/bg_info.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="top" ActionTag="1620170603" Tag="112" IconVisible="True" PercentWidthEnable="True" PercentWidthEnabled="True" TopMargin="-79.6800" BottomMargin="624.6800" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="True" ctype="ProjectNodeObjectData">
            <Size X="960.0000" Y="95.0000" />
            <AnchorPoint />
            <Position Y="624.6800" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition Y="0.9761" />
            <PreSize X="1.0000" Y="0.1484" />
            <FileData Type="Normal" Path="game_public/csb/hall_top.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="item_0" ActionTag="-959632128" Tag="87" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-33.2680" RightMargin="592.2680" TopMargin="89.0840" BottomMargin="17.9160" ctype="SpriteObjectData">
            <Size X="401.0000" Y="533.0000" />
            <Children>
              <AbstractNodeData Name="switch_shake" ActionTag="436278759" Tag="81" IconVisible="False" LeftMargin="151.0025" RightMargin="42.9975" TopMargin="368.7701" BottomMargin="94.2299" ctype="SpriteObjectData">
                <Size X="207.0000" Y="70.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="254.5025" Y="129.2299" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.6347" Y="0.2425" />
                <PreSize X="0.5162" Y="0.1313" />
                <FileData Type="MarkedSubImage" Path="game_public/lan/cn/setting_switch_1.png" Plist="game_public/lan/cn/game.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="shake_txt" ActionTag="507566988" Tag="82" IconVisible="False" LeftMargin="43.4926" RightMargin="293.5074" TopMargin="386.0228" BottomMargin="113.9772" FontSize="32" LabelText="震動" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="64.0000" Y="33.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="75.4926" Y="130.4772" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="64" G="30" B="0" />
                <PrePosition X="0.1883" Y="0.2448" />
                <PreSize X="0.1596" Y="0.0619" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="switch_sound" ActionTag="1270132156" Tag="83" IconVisible="False" LeftMargin="151.0025" RightMargin="42.9975" TopMargin="264.7706" BottomMargin="198.2294" ctype="SpriteObjectData">
                <Size X="207.0000" Y="70.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="254.5025" Y="233.2294" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.6347" Y="0.4376" />
                <PreSize X="0.5162" Y="0.1313" />
                <FileData Type="MarkedSubImage" Path="game_public/lan/cn/setting_switch_1.png" Plist="game_public/lan/cn/game.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="switch_music" ActionTag="-1838579744" Tag="84" IconVisible="False" LeftMargin="151.0025" RightMargin="42.9975" TopMargin="160.7711" BottomMargin="302.2289" ctype="SpriteObjectData">
                <Size X="207.0000" Y="70.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="254.5025" Y="337.2289" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.6347" Y="0.6327" />
                <PreSize X="0.5162" Y="0.1313" />
                <FileData Type="MarkedSubImage" Path="game_public/lan/cn/setting_switch_1.png" Plist="game_public/lan/cn/game.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="sound_txt" ActionTag="-2130229924" Tag="85" IconVisible="False" LeftMargin="43.4926" RightMargin="293.5074" TopMargin="282.0234" BottomMargin="217.9766" FontSize="32" LabelText="音效" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="64.0000" Y="33.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="75.4926" Y="234.4766" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="64" G="30" B="0" />
                <PrePosition X="0.1883" Y="0.4399" />
                <PreSize X="0.1596" Y="0.0619" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="music_txt" ActionTag="-891894281" Tag="86" IconVisible="False" LeftMargin="43.4926" RightMargin="293.5074" TopMargin="178.0243" BottomMargin="321.9757" FontSize="32" LabelText="音樂" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="64.0000" Y="33.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="75.4926" Y="338.4757" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="64" G="30" B="0" />
                <PrePosition X="0.1883" Y="0.6350" />
                <PreSize X="0.1596" Y="0.0619" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="title" ActionTag="-847117546" Tag="92" IconVisible="False" LeftMargin="144.3841" RightMargin="104.6159" TopMargin="39.6577" BottomMargin="455.3423" FontSize="38" LabelText="音效設置" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="152.0000" Y="38.0000" />
                <Children>
                  <AbstractNodeData Name="Image_1" ActionTag="-1457188159" Tag="149" IconVisible="False" LeftMargin="-42.9971" RightMargin="161.9971" TopMargin="-0.1670" BottomMargin="-2.8330" LeftEage="10" RightEage="10" TopEage="13" BottomEage="13" Scale9OriginX="10" Scale9OriginY="13" Scale9Width="13" Scale9Height="15" ctype="ImageViewObjectData">
                    <Size X="33.0000" Y="41.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="-26.4971" Y="17.6670" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="-0.1743" Y="0.4649" />
                    <PreSize X="0.2171" Y="1.0789" />
                    <FileData Type="Normal" Path="hall/res/setting/item_bg1.png" Plist="" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="220.3841" Y="474.3423" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="201" G="95" B="0" />
                <PrePosition X="0.5496" Y="0.8899" />
                <PreSize X="0.3791" Y="0.0713" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="167.2320" Y="284.4160" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.1742" Y="0.4444" />
            <PreSize X="0.4177" Y="0.8328" />
            <FileData Type="Normal" Path="hall/res/setting/item_bg.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="item_1" ActionTag="989465993" Tag="90" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="281.5160" RightMargin="277.4840" TopMargin="89.0840" BottomMargin="17.9160" ctype="SpriteObjectData">
            <Size X="401.0000" Y="533.0000" />
            <Children>
              <AbstractNodeData Name="lan2" ActionTag="-1351110592" Tag="79" IconVisible="False" LeftMargin="72.2097" RightMargin="257.7903" TopMargin="259.9492" BottomMargin="209.0509" ctype="SpriteObjectData">
                <Size X="71.0000" Y="64.0000" />
                <Children>
                  <AbstractNodeData Name="title" ActionTag="-798382496" Tag="80" IconVisible="False" LeftMargin="106.8572" RightMargin="-107.8572" TopMargin="18.0002" BottomMargin="9.9998" FontSize="36" LabelText="粵語" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="72.0000" Y="36.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="106.8572" Y="27.9998" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="64" G="30" B="0" />
                    <PrePosition X="1.5050" Y="0.4375" />
                    <PreSize X="1.0141" Y="0.5625" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5080" ScaleY="0.4587" />
                <Position X="108.2777" Y="238.4077" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2700" Y="0.4473" />
                <PreSize X="0.1771" Y="0.1201" />
                <FileData Type="MarkedSubImage" Path="game_public/res/setting_check1.png" Plist="game_public/res/game.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="lan1" ActionTag="758423821" Tag="77" IconVisible="False" LeftMargin="72.2097" RightMargin="257.7903" TopMargin="155.2239" BottomMargin="313.7761" ctype="SpriteObjectData">
                <Size X="71.0000" Y="64.0000" />
                <Children>
                  <AbstractNodeData Name="title" ActionTag="227197351" Tag="78" IconVisible="False" LeftMargin="106.8571" RightMargin="-143.8571" TopMargin="15.0083" BottomMargin="12.9917" FontSize="36" LabelText="普通話" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="108.0000" Y="36.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="106.8571" Y="30.9917" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="64" G="30" B="0" />
                    <PrePosition X="1.5050" Y="0.4842" />
                    <PreSize X="1.5211" Y="0.5625" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="107.7097" Y="345.7761" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2686" Y="0.6487" />
                <PreSize X="0.1771" Y="0.1201" />
                <FileData Type="MarkedSubImage" Path="game_public/res/setting_check1.png" Plist="game_public/res/game.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="lan3" ActionTag="-1396704662" Tag="75" IconVisible="False" LeftMargin="72.2097" RightMargin="257.7903" TopMargin="370.9131" BottomMargin="98.0869" ctype="SpriteObjectData">
                <Size X="71.0000" Y="64.0000" />
                <Children>
                  <AbstractNodeData Name="title" ActionTag="1013136430" Tag="76" IconVisible="False" LeftMargin="106.8572" RightMargin="-143.8572" TopMargin="15.8729" BottomMargin="12.1271" FontSize="36" LabelText="臺灣話" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="108.0000" Y="36.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="106.8572" Y="30.1271" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="64" G="30" B="0" />
                    <PrePosition X="1.5050" Y="0.4707" />
                    <PreSize X="1.5211" Y="0.5625" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="107.7097" Y="130.0869" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2686" Y="0.2441" />
                <PreSize X="0.1771" Y="0.1201" />
                <FileData Type="MarkedSubImage" Path="game_public/res/setting_check1.png" Plist="game_public/res/game.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="title" ActionTag="-2084222555" Tag="93" IconVisible="False" LeftMargin="135.6181" RightMargin="113.3819" TopMargin="36.3589" BottomMargin="458.6411" FontSize="38" LabelText="遊戲語音" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="152.0000" Y="38.0000" />
                <Children>
                  <AbstractNodeData Name="Image_2" ActionTag="844089158" Tag="150" IconVisible="False" LeftMargin="-35.2954" RightMargin="157.2954" TopMargin="1.7143" BottomMargin="-4.7143" LeftEage="9" RightEage="9" TopEage="13" BottomEage="13" Scale9OriginX="9" Scale9OriginY="13" Scale9Width="12" Scale9Height="15" ctype="ImageViewObjectData">
                    <Size X="30.0000" Y="41.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="-20.2954" Y="15.7857" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="-0.1335" Y="0.4154" />
                    <PreSize X="0.1974" Y="1.0789" />
                    <FileData Type="Normal" Path="hall/res/setting/item_bg3.png" Plist="" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="211.6181" Y="477.6411" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="201" G="95" B="0" />
                <PrePosition X="0.5277" Y="0.8961" />
                <PreSize X="0.3791" Y="0.0713" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="482.0160" Y="284.4160" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5021" Y="0.4444" />
            <PreSize X="0.4177" Y="0.8328" />
            <FileData Type="Normal" Path="hall/res/setting/item_bg.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="item_2" ActionTag="-1444650982" Tag="91" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="597.6440" RightMargin="-38.6440" TopMargin="89.0840" BottomMargin="17.9160" ctype="SpriteObjectData">
            <Size X="401.0000" Y="533.0000" />
            <Children>
              <AbstractNodeData Name="title" ActionTag="1563331399" Tag="94" IconVisible="False" LeftMargin="140.6995" RightMargin="108.3005" TopMargin="34.0213" BottomMargin="460.9787" FontSize="38" LabelText="其他功能" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="152.0000" Y="38.0000" />
                <Children>
                  <AbstractNodeData Name="Image_4" ActionTag="958225767" Tag="195" IconVisible="False" LeftMargin="-42.8993" RightMargin="156.8993" TopMargin="1.9510" BottomMargin="-0.9510" LeftEage="12" RightEage="12" TopEage="12" BottomEage="12" Scale9OriginX="12" Scale9OriginY="12" Scale9Width="14" Scale9Height="13" ctype="ImageViewObjectData">
                    <Size X="38.0000" Y="37.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="-23.8993" Y="17.5490" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="-0.1572" Y="0.4618" />
                    <PreSize X="0.2500" Y="0.9737" />
                    <FileData Type="Normal" Path="hall/res/setting/item_bg2.png" Plist="" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="216.6995" Y="479.9787" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="201" G="95" B="0" />
                <PrePosition X="0.5404" Y="0.9005" />
                <PreSize X="0.3791" Y="0.0713" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="update" ActionTag="-1255485176" VisibleForFrame="False" Tag="95" IconVisible="False" LeftMargin="257.1992" RightMargin="43.8008" TopMargin="189.4554" BottomMargin="273.5446" ctype="SpriteObjectData">
                <Size X="100.0000" Y="70.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="307.1992" Y="308.5446" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7661" Y="0.5789" />
                <PreSize X="0.2494" Y="0.1313" />
                <FileData Type="MarkedSubImage" Path="lan/cn/update.png" Plist="lan/cn/game.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="version_txt" ActionTag="-2113883225" Tag="96" IconVisible="False" LeftMargin="113.8218" RightMargin="143.1782" TopMargin="166.7114" BottomMargin="333.2886" FontSize="32" LabelText="版本：123" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="144.0000" Y="33.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="113.8218" Y="349.7886" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="64" G="30" B="0" />
                <PrePosition X="0.2838" Y="0.6563" />
                <PreSize X="0.3591" Y="0.0619" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="appraise" ActionTag="934204144" VisibleForFrame="False" Tag="97" IconVisible="False" LeftMargin="272.1992" RightMargin="58.8008" TopMargin="293.4550" BottomMargin="169.5450" ctype="SpriteObjectData">
                <Size X="70.0000" Y="70.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="307.1992" Y="204.5450" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7661" Y="0.3838" />
                <PreSize X="0.1746" Y="0.1313" />
                <FileData Type="Normal" Path="hall/res/setting/arrow.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="appraise_txt" ActionTag="-889090777" VisibleForFrame="False" Tag="98" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="41.4293" RightMargin="231.5707" TopMargin="266.4697" BottomMargin="233.5303" FontSize="32" LabelText="遊戲評價" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="128.0000" Y="33.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="41.4293" Y="250.0303" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1033" Y="0.4691" />
                <PreSize X="0.3192" Y="0.0619" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="fans" ActionTag="447422259" VisibleForFrame="False" Tag="99" IconVisible="False" LeftMargin="272.1992" RightMargin="58.8008" TopMargin="397.4546" BottomMargin="65.5454" ctype="SpriteObjectData">
                <Size X="70.0000" Y="70.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="307.1992" Y="100.5454" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7661" Y="0.1886" />
                <PreSize X="0.1746" Y="0.1313" />
                <FileData Type="Normal" Path="hall/res/setting/arrow.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="fans_txt" ActionTag="81716755" VisibleForFrame="False" Tag="100" IconVisible="False" LeftMargin="41.4294" RightMargin="135.5706" TopMargin="415.9546" BottomMargin="84.0454" FontSize="32" LabelText="Facebook粉絲頁" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="224.0000" Y="33.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="41.4294" Y="100.5454" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1033" Y="0.1886" />
                <PreSize X="0.5586" Y="0.0619" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="798.1440" Y="284.4160" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8314" Y="0.4444" />
            <PreSize X="0.4177" Y="0.8328" />
            <FileData Type="Normal" Path="hall/res/setting/item_bg.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>