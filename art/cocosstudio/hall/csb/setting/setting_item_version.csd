<GameFile>
  <PropertyGroup Name="setting_item_version" Type="Layer" ID="4bf43634-2db6-43ed-88d2-14089be49cc4" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="138" ctype="GameLayerObjectData">
        <Size X="910.0000" Y="100.0000" />
        <Children>
          <AbstractNodeData Name="txt" ActionTag="249191621" Tag="4593" IconVisible="False" LeftMargin="43.1700" RightMargin="738.8300" TopMargin="33.5653" BottomMargin="33.4347" FontSize="32" LabelText="V1.0.0.8" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="128.0000" Y="33.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="43.1700" Y="49.9347" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0474" Y="0.4993" />
            <PreSize X="0.1407" Y="0.3300" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="about" ActionTag="-728978653" VisibleForFrame="False" Tag="4702" IconVisible="False" LeftMargin="501.1700" RightMargin="248.8300" TopMargin="33.5653" BottomMargin="33.4347" FontSize="32" LabelText="服务条款&gt;&gt;" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="160.0000" Y="33.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="501.1700" Y="49.9347" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5507" Y="0.4993" />
            <PreSize X="0.1758" Y="0.3300" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>