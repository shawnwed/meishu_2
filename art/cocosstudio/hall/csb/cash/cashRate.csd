<GameFile>
  <PropertyGroup Name="cashRate" Type="Layer" ID="a65f4d1b-9ebc-42bb-8f93-a33b4c33cf18" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="493" ctype="GameLayerObjectData">
        <Size X="710.0000" Y="526.0000" />
        <Children>
          <AbstractNodeData Name="bg_0" ActionTag="1293609614" Tag="494" IconVisible="False" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="710.0000" Y="526.0000" />
            <Children>
              <AbstractNodeData Name="top" ActionTag="1370469283" Tag="495" IconVisible="False" LeftMargin="-1.0575" RightMargin="0.0575" TopMargin="55.1400" BottomMargin="416.8600" LeftEage="224" RightEage="224" TopEage="16" BottomEage="16" Scale9OriginX="224" Scale9OriginY="16" Scale9Width="263" Scale9Height="22" ctype="ImageViewObjectData">
                <Size X="711.0000" Y="54.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="354.4425" Y="443.8600" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4992" Y="0.8438" />
                <PreSize X="1.0014" Y="0.1027" />
                <FileData Type="MarkedSubImage" Path="hall/res/cash/tx_ui_kuang_03.png" Plist="hall/res/cash.plist" />
              </AbstractNodeData>
              <AbstractNodeData Name="containor" ActionTag="-1701929232" Tag="496" IconVisible="False" LeftMargin="14.0983" RightMargin="14.9017" TopMargin="121.6209" BottomMargin="33.3791" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="681.0000" Y="371.0000" />
                <AnchorPoint ScaleX="0.5000" />
                <Position X="354.5983" Y="33.3791" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4994" Y="0.0635" />
                <PreSize X="0.9592" Y="0.7053" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="kefu" ActionTag="58302309" VisibleForFrame="False" Tag="497" IconVisible="False" LeftMargin="213.6121" RightMargin="221.3879" TopMargin="417.1061" BottomMargin="13.8939" ctype="SpriteObjectData">
                <Size X="275.0000" Y="95.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="351.1121" Y="61.3939" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4945" Y="0.1167" />
                <PreSize X="0.3873" Y="0.1806" />
                <FileData Type="MarkedSubImage" Path="hall/res/cash/tx_ui_but_b02.png" Plist="hall/res/cash.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="date_selector" ActionTag="1149087200" Tag="112" IconVisible="False" LeftMargin="513.4772" RightMargin="-0.4772" TopMargin="3.1784" BottomMargin="469.8216" ctype="SpriteObjectData">
                <Size X="197.0000" Y="53.0000" />
                <Children>
                  <AbstractNodeData Name="txt" ActionTag="-985793829" Tag="113" IconVisible="False" LeftMargin="37.1235" RightMargin="84.8765" TopMargin="7.9256" BottomMargin="15.0744" FontSize="30" LabelText="当 天" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="75.0000" Y="30.0000" />
                    <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
                    <Position X="112.1235" Y="30.0744" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5692" Y="0.5674" />
                    <PreSize X="0.3807" Y="0.5660" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="611.9772" Y="496.3216" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.8619" Y="0.9436" />
                <PreSize X="0.2775" Y="0.1008" />
                <FileData Type="Normal" Path="hall/res/info/date_select_bg.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="355.0000" Y="263.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="1.0000" Y="1.0000" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>