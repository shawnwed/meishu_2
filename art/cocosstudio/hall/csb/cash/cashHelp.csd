<GameFile>
  <PropertyGroup Name="cashHelp" Type="Scene" ID="f436d140-6097-4b3c-aa48-556ded1d0bae" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Scene" Tag="324" ctype="GameNodeObjectData">
        <Size X="960.0000" Y="640.0000" />
        <Children>
          <AbstractNodeData Name="Panel_1" ActionTag="-1360335239" Tag="325" IconVisible="False" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="960.0000" Y="640.0000" />
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="1.0000" Y="1.0000" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="bg" ActionTag="-349839167" Tag="360" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="20.2920" RightMargin="24.7080" TopMargin="28.2198" BottomMargin="35.7802" LeftEage="211" RightEage="211" TopEage="146" BottomEage="146" Scale9OriginX="211" Scale9OriginY="146" Scale9Width="493" Scale9Height="284" ctype="ImageViewObjectData">
            <Size X="915.0000" Y="576.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="477.7920" Y="323.7802" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4977" Y="0.5059" />
            <PreSize X="0.9531" Y="0.9000" />
            <FileData Type="Normal" Path="hall/res/bingphone/bg.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="title" ActionTag="-1712963391" Tag="361" IconVisible="False" LeftMargin="400.6754" RightMargin="393.3246" TopMargin="58.9114" BottomMargin="534.0886" ctype="SpriteObjectData">
            <Size X="166.0000" Y="47.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="483.6754" Y="557.5886" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5038" Y="0.8712" />
            <PreSize X="0.1729" Y="0.0734" />
            <FileData Type="MarkedSubImage" Path="hall/res/cash/tx_logo_03.png" Plist="hall/res/cash.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="confirm" ActionTag="1762742492" Tag="362" IconVisible="False" LeftMargin="332.5463" RightMargin="353.4537" TopMargin="467.8949" BottomMargin="77.1051" ctype="SpriteObjectData">
            <Size X="274.0000" Y="95.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="469.5463" Y="124.6051" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4891" Y="0.1947" />
            <PreSize X="0.2854" Y="0.1484" />
            <FileData Type="MarkedSubImage" Path="hall/res/cash/tx_ui_but_b03.png" Plist="hall/res/cash.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="Text_8" ActionTag="1316233457" Tag="363" IconVisible="False" LeftMargin="108.5070" RightMargin="101.4930" TopMargin="165.2665" BottomMargin="174.7335" FontSize="30" LabelText="1、提现请求成功后，需要等待人工审核。可以在“提现&#xA;   进度”中查看处理进度。&#xA;&#xA;2、每天前两次兑换免收手续费，后续兑换筹码收取2%手&#xA;   续费。&#xA;&#xA;3、提现数字必须是100的整数倍。例：100、200····&#xA;&#xA;4、账户提现必须留底6元。例：账户大于或等于106元可&#xA;   以提现100元。" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="750.0000" Y="300.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="108.5070" Y="324.7335" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="81" G="70" B="69" />
            <PrePosition X="0.1130" Y="0.5074" />
            <PreSize X="0.7813" Y="0.4688" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>