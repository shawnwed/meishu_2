<GameFile>
  <PropertyGroup Name="cashMoney" Type="Layer" ID="2062a376-3d6e-4786-8a5c-694fc2d01b39" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="463" ctype="GameLayerObjectData">
        <Size X="710.0000" Y="526.0000" />
        <Children>
          <AbstractNodeData Name="bg_0" ActionTag="1564170106" Tag="469" IconVisible="False" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="710.0000" Y="526.0000" />
            <Children>
              <AbstractNodeData Name="mycoin" ActionTag="1645401327" Tag="470" IconVisible="False" LeftMargin="29.9080" RightMargin="468.0920" TopMargin="66.3117" BottomMargin="421.6883" LeftEage="88" RightEage="88" TopEage="13" BottomEage="13" Scale9OriginX="88" Scale9OriginY="13" Scale9Width="36" Scale9Height="12" ctype="ImageViewObjectData">
                <Size X="212.0000" Y="38.0000" />
                <Children>
                  <AbstractNodeData Name="txt" ActionTag="-1141496725" Tag="471" IconVisible="False" LeftMargin="85.5007" RightMargin="62.4993" TopMargin="1.8375" BottomMargin="3.1625" FontSize="32" LabelText="1000" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="64.0000" Y="33.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="117.5007" Y="19.6625" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="183" G="60" B="0" />
                    <PrePosition X="0.5542" Y="0.5174" />
                    <PreSize X="0.3019" Y="0.8684" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="Image_3" ActionTag="407734946" Tag="472" IconVisible="False" LeftMargin="5.0862" RightMargin="157.9138" TopMargin="-3.6258" BottomMargin="1.6258" LeftEage="14" RightEage="14" TopEage="14" BottomEage="14" Scale9OriginX="14" Scale9OriginY="14" Scale9Width="21" Scale9Height="12" ctype="ImageViewObjectData">
                    <Size X="49.0000" Y="40.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="29.5862" Y="21.6258" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.1396" Y="0.5691" />
                    <PreSize X="0.2311" Y="1.0526" />
                    <FileData Type="MarkedSubImage" Path="hall/res/cash/tx_ui_obj_01.png" Plist="hall/res/cash.plist" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="135.9080" Y="440.6883" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1914" Y="0.8378" />
                <PreSize X="0.2986" Y="0.0722" />
                <FileData Type="MarkedSubImage" Path="hall/res/cash/tx_ui_kuang_01.png" Plist="hall/res/cash.plist" />
              </AbstractNodeData>
              <AbstractNodeData Name="cashcoin" ActionTag="1652475380" Tag="473" IconVisible="False" LeftMargin="464.3621" RightMargin="33.6379" TopMargin="63.1308" BottomMargin="424.8692" LeftEage="88" RightEage="88" TopEage="13" BottomEage="13" Scale9OriginX="88" Scale9OriginY="13" Scale9Width="36" Scale9Height="12" ctype="ImageViewObjectData">
                <Size X="212.0000" Y="38.0000" />
                <Children>
                  <AbstractNodeData Name="txt" ActionTag="-276585191" Tag="474" IconVisible="False" LeftMargin="89.5828" RightMargin="58.4172" TopMargin="1.7944" BottomMargin="3.2056" FontSize="32" LabelText="1000" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="64.0000" Y="33.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="121.5828" Y="19.7056" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="183" G="60" B="0" />
                    <PrePosition X="0.5735" Y="0.5186" />
                    <PreSize X="0.3019" Y="0.8684" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="Image_4" ActionTag="-1047125512" Tag="475" IconVisible="False" LeftMargin="-4.6500" RightMargin="165.6500" TopMargin="2.0995" BottomMargin="0.9005" LeftEage="16" RightEage="16" TopEage="11" BottomEage="11" Scale9OriginX="16" Scale9OriginY="11" Scale9Width="19" Scale9Height="13" ctype="ImageViewObjectData">
                    <Size X="51.0000" Y="35.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="20.8500" Y="18.4005" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.0983" Y="0.4842" />
                    <PreSize X="0.2406" Y="0.9211" />
                    <FileData Type="MarkedSubImage" Path="hall/res/cash/tx_ui_obj_02.png" Plist="hall/res/cash.plist" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="570.3621" Y="443.8692" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.8033" Y="0.8439" />
                <PreSize X="0.2986" Y="0.0722" />
                <FileData Type="MarkedSubImage" Path="hall/res/cash/tx_ui_kuang_01.png" Plist="hall/res/cash.plist" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_3" ActionTag="-1084955241" Tag="476" IconVisible="False" LeftMargin="44.6752" RightMargin="483.3248" TopMargin="19.5312" BottomMargin="478.4688" FontSize="28" LabelText="当前持有金币:" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="182.0000" Y="28.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="44.6752" Y="492.4688" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="106" G="50" B="0" />
                <PrePosition X="0.0629" Y="0.9363" />
                <PreSize X="0.2563" Y="0.0532" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_4" ActionTag="-1559367656" Tag="477" IconVisible="False" LeftMargin="506.4980" RightMargin="35.5020" TopMargin="20.2512" BottomMargin="477.7488" FontSize="28" LabelText="可兑换金币：" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="168.0000" Y="28.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="506.4980" Y="491.7488" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="106" G="50" B="0" />
                <PrePosition X="0.7134" Y="0.9349" />
                <PreSize X="0.2366" Y="0.0532" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Image_bg" ActionTag="1144809323" Tag="479" IconVisible="False" LeftMargin="16.5260" RightMargin="207.4740" TopMargin="208.4107" BottomMargin="259.5893" LeftEage="160" RightEage="160" TopEage="19" BottomEage="19" Scale9OriginX="160" Scale9OriginY="19" Scale9Width="125" Scale9Height="24" ctype="ImageViewObjectData">
                <Size X="486.0000" Y="58.0000" />
                <Children>
                  <AbstractNodeData Name="input" ActionTag="313015473" Tag="480" IconVisible="False" LeftMargin="12.8164" RightMargin="3.1836" TopMargin="6.7194" BottomMargin="6.2806" TouchEnable="True" FontSize="40" IsCustomSize="True" LabelText="" PlaceHolderText="请输入100的整数倍" MaxLengthEnable="True" MaxLengthText="10" ctype="TextFieldObjectData">
                    <Size X="470.0000" Y="45.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="247.8164" Y="28.7806" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="63" G="10" B="1" />
                    <PrePosition X="0.5099" Y="0.4962" />
                    <PreSize X="0.9671" Y="0.7759" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="259.5260" Y="288.5893" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3655" Y="0.5486" />
                <PreSize X="0.6845" Y="0.1103" />
                <FileData Type="MarkedSubImage" Path="hall/res/cash/tx_ui_kuang_02.png" Plist="hall/res/cash.plist" />
              </AbstractNodeData>
              <AbstractNodeData Name="clear" ActionTag="-745674598" Tag="481" IconVisible="False" LeftMargin="508.5698" RightMargin="9.4302" TopMargin="204.7624" BottomMargin="254.2376" ctype="SpriteObjectData">
                <Size X="192.0000" Y="67.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="604.5698" Y="287.7376" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.8515" Y="0.5470" />
                <PreSize X="0.2704" Y="0.1274" />
                <FileData Type="MarkedSubImage" Path="hall/res/cash/tx_ui_but_a01.png" Plist="hall/res/cash.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="btn100" ActionTag="727041025" Tag="482" IconVisible="False" LeftMargin="11.0786" RightMargin="543.9214" TopMargin="290.2646" BottomMargin="173.7354" ctype="SpriteObjectData">
                <Size X="155.0000" Y="62.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="88.5786" Y="204.7354" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1248" Y="0.3892" />
                <PreSize X="0.2183" Y="0.1179" />
                <FileData Type="MarkedSubImage" Path="hall/res/cash/tx_ui_but_a02.png" Plist="hall/res/cash.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="btn101" ActionTag="-1623755199" Tag="483" IconVisible="False" LeftMargin="189.3312" RightMargin="364.6688" TopMargin="290.2646" BottomMargin="173.7354" ctype="SpriteObjectData">
                <Size X="156.0000" Y="62.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="267.3312" Y="204.7354" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3765" Y="0.3892" />
                <PreSize X="0.2197" Y="0.1179" />
                <FileData Type="MarkedSubImage" Path="hall/res/cash/tx_ui_but_a03.png" Plist="hall/res/cash.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="btn102" ActionTag="-972481403" Tag="484" IconVisible="False" LeftMargin="368.5845" RightMargin="186.4155" TopMargin="290.2646" BottomMargin="173.7354" ctype="SpriteObjectData">
                <Size X="155.0000" Y="62.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="446.0845" Y="204.7354" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.6283" Y="0.3892" />
                <PreSize X="0.2183" Y="0.1179" />
                <FileData Type="MarkedSubImage" Path="hall/res/cash/tx_ui_but_a04.png" Plist="hall/res/cash.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="btn103" ActionTag="1411669092" Tag="485" IconVisible="False" LeftMargin="546.8370" RightMargin="8.1630" TopMargin="290.2646" BottomMargin="173.7354" ctype="SpriteObjectData">
                <Size X="155.0000" Y="62.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="624.3370" Y="204.7354" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.8793" Y="0.3892" />
                <PreSize X="0.2183" Y="0.1179" />
                <FileData Type="MarkedSubImage" Path="hall/res/cash/tx_ui_but_a05.png" Plist="hall/res/cash.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="btncash" ActionTag="1297395828" Tag="486" IconVisible="False" LeftMargin="222.0416" RightMargin="212.9584" TopMargin="364.3444" BottomMargin="66.6556" ctype="SpriteObjectData">
                <Size X="275.0000" Y="95.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="359.5416" Y="114.1556" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5064" Y="0.2170" />
                <PreSize X="0.3873" Y="0.1806" />
                <FileData Type="MarkedSubImage" Path="hall/res/cash/tx_ui_but_b01.png" Plist="hall/res/cash.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_tishi" ActionTag="1108867298" Tag="487" IconVisible="False" LeftMargin="10.5936" RightMargin="169.4064" TopMargin="441.2439" BottomMargin="4.7561" FontSize="20" LabelText="提示：&#xA;1、每天前两次兑换免收手续费，后续兑换筹码收取2%手续费&#xA;2、账户余额最少保留6筹码&#xA;3、支付宝兑换单笔最高支持2W" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="530.0000" Y="80.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="10.5936" Y="44.7561" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="81" G="70" B="69" />
                <PrePosition X="0.0149" Y="0.0851" />
                <PreSize X="0.7465" Y="0.1521" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="help" ActionTag="-974416439" Tag="488" IconVisible="False" LeftMargin="640.7031" RightMargin="15.2969" TopMargin="456.6194" BottomMargin="14.3806" ctype="SpriteObjectData">
                <Size X="54.0000" Y="55.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="667.7031" Y="41.8806" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.9404" Y="0.0796" />
                <PreSize X="0.0761" Y="0.1046" />
                <FileData Type="MarkedSubImage" Path="hall/res/cash/tx_ui_but_a06.png" Plist="hall/res/cash.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="bankCard" ActionTag="883731498" Tag="489" IconVisible="False" LeftMargin="177.1844" RightMargin="20.8156" TopMargin="125.6846" BottomMargin="346.3154" LeftEage="88" RightEage="88" TopEage="13" BottomEage="13" Scale9OriginX="88" Scale9OriginY="13" Scale9Width="336" Scale9Height="28" ctype="ImageViewObjectData">
                <Size X="512.0000" Y="54.0000" />
                <Children>
                  <AbstractNodeData Name="bankCard" ActionTag="1105887031" Tag="490" IconVisible="False" LeftMargin="5.1147" RightMargin="106.8853" TopMargin="15.3227" BottomMargin="3.6773" IsCustomSize="True" FontSize="30" LabelText="" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="400.0000" Y="35.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="205.1147" Y="21.1773" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.4006" Y="0.3922" />
                    <PreSize X="0.7813" Y="0.6481" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="selector" ActionTag="-119597024" Tag="491" IconVisible="False" LeftMargin="465.9516" RightMargin="5.0484" TopMargin="6.4645" BottomMargin="5.5355" ctype="SpriteObjectData">
                    <Size X="41.0000" Y="42.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="486.4516" Y="26.5355" />
                    <Scale ScaleX="1.3129" ScaleY="1.3129" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.9501" Y="0.4914" />
                    <PreSize X="0.0801" Y="0.7778" />
                    <FileData Type="MarkedSubImage" Path="hall/res/cash/tx_ui_but_d01.png" Plist="hall/res/cash.plist" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleY="0.5000" />
                <Position X="177.1844" Y="373.3154" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2496" Y="0.7097" />
                <PreSize X="0.7211" Y="0.1027" />
                <FileData Type="MarkedSubImage" Path="hall/res/cash/tx_ui_kuang_00.png" Plist="hall/res/cash.plist" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_4_0" ActionTag="959752457" Tag="492" IconVisible="False" LeftMargin="23.4025" RightMargin="551.5975" TopMargin="136.9508" BottomMargin="359.0492" FontSize="30" LabelText="提现方式:" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="135.0000" Y="30.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="23.4025" Y="374.0492" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="106" G="50" B="0" />
                <PrePosition X="0.0330" Y="0.7111" />
                <PreSize X="0.1901" Y="0.0570" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="355.0000" Y="263.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="1.0000" Y="1.0000" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>