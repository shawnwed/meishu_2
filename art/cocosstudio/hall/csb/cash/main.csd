<GameFile>
  <PropertyGroup Name="main" Type="Scene" ID="9f7bd439-b9a9-4ae3-9ce5-761a624207bd" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Scene" Tag="352" ctype="GameNodeObjectData">
        <Size X="960.0000" Y="640.0000" />
        <Children>
          <AbstractNodeData Name="Panel_1" ActionTag="1616651054" Tag="630" IconVisible="False" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="960.0000" Y="640.0000" />
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="1.0000" Y="1.0000" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="bg" ActionTag="869207583" Tag="631" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-49.0100" RightMargin="-49.0100" TopMargin="-29.5050" BottomMargin="-29.5050" LeftEage="337" RightEage="337" TopEage="201" BottomEage="218" Scale9OriginX="337" Scale9OriginY="201" Scale9Width="384" Scale9Height="280" ctype="ImageViewObjectData">
            <Size X="1058.0200" Y="699.0100" />
            <Children>
              <AbstractNodeData Name="close" ActionTag="-1745303356" Tag="659" IconVisible="False" LeftMargin="1006.3800" RightMargin="-28.3600" TopMargin="20.7332" BottomMargin="597.2768" ctype="SpriteObjectData">
                <Size X="80.0000" Y="81.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="1046.3800" Y="637.7768" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.9890" Y="0.9124" />
                <PreSize X="0.0756" Y="0.1159" />
                <FileData Type="Normal" Path="hall/res/info/x.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="btn1" ActionTag="-929175768" Tag="633" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="27.2798" RightMargin="768.7402" TopMargin="119.8331" BottomMargin="470.1769" ctype="SpriteObjectData">
                <Size X="262.0000" Y="109.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="158.2798" Y="524.6769" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1496" Y="0.7506" />
                <PreSize X="0.2476" Y="0.1559" />
                <FileData Type="MarkedSubImage" Path="hall/res/cash/tx_ui_but_01b.png" Plist="hall/res/cash.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="btn2" ActionTag="-1969089316" Tag="657" IconVisible="False" LeftMargin="25.9000" RightMargin="770.1200" TopMargin="231.4600" BottomMargin="358.5500" ctype="SpriteObjectData">
                <Size X="262.0000" Y="109.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="156.9000" Y="413.0500" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1483" Y="0.5909" />
                <PreSize X="0.2476" Y="0.1559" />
                <FileData Type="MarkedSubImage" Path="hall/res/cash/tx_ui_but_02b.png" Plist="hall/res/cash.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="content" ActionTag="1686999158" Tag="678" IconVisible="False" LeftMargin="305.5200" RightMargin="42.5000" TopMargin="124.6700" BottomMargin="48.3400" TouchEnable="True" ClipAble="False" BackColorAlpha="0" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="710.0000" Y="526.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="660.5200" Y="311.3400" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.6243" Y="0.4454" />
                <PreSize X="0.6711" Y="0.7525" />
                <SingleColor A="255" R="0" G="0" B="0" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="480.0000" Y="320.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="1.1021" Y="1.0922" />
            <FileData Type="Normal" Path="hall/res/cash/email_notice_bg.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="title1" ActionTag="317731301" Tag="9948" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="393.1400" RightMargin="399.8600" TopMargin="-16.5842" BottomMargin="609.5842" ctype="SpriteObjectData">
            <Size X="93.0000" Y="43.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="476.6400" Y="633.0842" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4965" Y="0.9892" />
            <PreSize X="0.1740" Y="0.0734" />
            <FileData Type="Normal" Path="hall/res/cash/tx_logo_01.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="title2" Visible="False" ActionTag="-1760379401" Tag="9949" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="396.5000" RightMargin="396.5000" TopMargin="-16.5842" BottomMargin="609.5842" ctype="SpriteObjectData">
            <Size X="167.0000" Y="47.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="480.0000" Y="633.0842" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.9892" />
            <PreSize X="0.1740" Y="0.0734" />
            <FileData Type="MarkedSubImage" Path="hall/res/cash/tx_logo_02.png" Plist="hall/res/cash.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>