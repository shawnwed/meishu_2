<GameFile>
  <PropertyGroup Name="edit_msg" Type="Node" ID="6a94ac02-6f04-4c83-8089-d8cde0ddf590" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="2138" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="mask" ActionTag="188553861" Alpha="0" Tag="2159" IconVisible="False" LeftMargin="-640.0000" RightMargin="-640.0000" TopMargin="-360.0000" BottomMargin="-360.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="1280.0000" Y="720.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="bg" ActionTag="2037343913" Tag="2158" IconVisible="False" LeftMargin="-387.0000" RightMargin="-387.0000" TopMargin="-234.5000" BottomMargin="-234.5000" TouchEnable="True" Scale9Enable="True" LeftEage="49" RightEage="49" TopEage="49" BottomEage="49" Scale9OriginX="49" Scale9OriginY="49" Scale9Width="676" Scale9Height="371" ctype="ImageViewObjectData">
            <Size X="774.0000" Y="469.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="game_public/res/setting_bg.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="input_bg" ActionTag="955321649" Tag="2240" IconVisible="False" LeftMargin="-356.9189" RightMargin="-362.0811" TopMargin="-208.3373" BottomMargin="-69.6627" ctype="SpriteObjectData">
            <Size X="719.0000" Y="278.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="2.5811" Y="69.3373" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="hall/res/first/rank_edit_bg.png" Plist="hall/res/first.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="close_btn" ActionTag="-298974648" Tag="2241" IconVisible="False" LeftMargin="-317.0000" RightMargin="83.0000" TopMargin="104.6329" BottomMargin="-194.6329" ctype="SpriteObjectData">
            <Size X="234.0000" Y="90.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-200.0000" Y="-149.6329" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="lan/cn/rank_cancel_btn.png" Plist="lan/cn/game.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="ok_btn" ActionTag="1225150721" Tag="2242" IconVisible="False" LeftMargin="177.0000" RightMargin="-223.0000" TopMargin="126.6329" BottomMargin="-172.6329" ctype="SpriteObjectData">
            <Size X="234.0000" Y="90.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="200.0000" Y="-149.6329" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="lan/cn/ok.png" Plist="lan/cn/game.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="input" ActionTag="2021373885" Tag="2243" IconVisible="False" LeftMargin="-337.4189" RightMargin="-342.5811" TopMargin="-194.3373" BottomMargin="-55.6627" TouchEnable="True" FontSize="34" IsCustomSize="True" LabelText="" PlaceHolderText="請輸入您的個性簽名" MaxLengthText="10" ctype="TextFieldObjectData">
            <Size X="680.0000" Y="250.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="2.5811" Y="69.3373" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>