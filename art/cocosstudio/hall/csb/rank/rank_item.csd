<GameFile>
  <PropertyGroup Name="rank_item" Type="Layer" ID="0f5afa56-2651-4a5c-9ebe-a45819fba608" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="72" ctype="GameLayerObjectData">
        <Size X="950.0000" Y="126.0000" />
        <Children>
          <AbstractNodeData Name="bg" ActionTag="-2144313443" Tag="521" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="15.3300" RightMargin="12.6700" TopMargin="13.2388" BottomMargin="9.7612" ctype="SpriteObjectData">
            <Size X="922.0000" Y="103.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="476.3300" Y="61.2612" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5014" Y="0.4862" />
            <PreSize X="0.9705" Y="0.8175" />
            <FileData Type="MarkedSubImage" Path="hall/res/first/rank_item_bg1.png" Plist="hall/res/first.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="head" ActionTag="1286250426" Tag="451" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="102.5377" RightMargin="777.4623" TopMargin="26.2456" BottomMargin="28.7544" LeftEage="28" RightEage="28" TopEage="28" BottomEage="28" Scale9OriginX="28" Scale9OriginY="28" Scale9Width="14" Scale9Height="15" ctype="ImageViewObjectData">
            <Size X="70.0000" Y="71.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="137.5377" Y="64.2544" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.1448" Y="0.5100" />
            <PreSize X="0.0737" Y="0.5635" />
            <FileData Type="MarkedSubImage" Path="hall/res/first/head_bg1.png" Plist="hall/res/first.plist" />
          </AbstractNodeData>
          <AbstractNodeData Name="rank_num" ActionTag="-992751310" Tag="74" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="21.6039" RightMargin="869.3961" TopMargin="36.3010" BottomMargin="34.6990" ctype="SpriteObjectData">
            <Size X="59.0000" Y="55.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="51.1039" Y="62.1990" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0538" Y="0.4936" />
            <PreSize X="0.0621" Y="0.4365" />
            <FileData Type="MarkedSubImage" Path="hall/res/first/rank1.png" Plist="hall/res/first.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="name" ActionTag="521944247" Tag="77" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="189.5414" RightMargin="640.4586" TopMargin="47.8938" BottomMargin="48.1062" FontSize="30" LabelText="我是土豪" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="120.0000" Y="30.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="189.5414" Y="63.1062" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="201" G="95" B="0" />
            <PrePosition X="0.1995" Y="0.5008" />
            <PreSize X="0.1263" Y="0.2381" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="coin" ActionTag="-683422234" Tag="230" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="421.8443" RightMargin="369.1556" TopMargin="44.1016" BottomMargin="48.8984" LabelText="30.00万" ctype="TextBMFontObjectData">
            <Size X="159.0000" Y="33.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="421.8443" Y="65.3984" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4440" Y="0.5190" />
            <PreSize X="0.1674" Y="0.2619" />
            <LabelBMFontFile_CNB Type="Normal" Path="hall/res/rank_num.fnt" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="rank_num1" ActionTag="-141728328" Tag="18" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="39.1039" RightMargin="886.8961" TopMargin="51.0487" BottomMargin="50.9513" FontSize="24" LabelText="45" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="24.0000" Y="24.0000" />
            <Children>
              <AbstractNodeData Name="Image_1" ActionTag="-1762231957" Tag="19" IconVisible="False" LeftMargin="-9.7268" RightMargin="-10.2732" TopMargin="-8.5988" BottomMargin="-11.4012" LeftEage="14" RightEage="14" TopEage="14" BottomEage="14" Scale9OriginX="14" Scale9OriginY="14" Scale9Width="16" Scale9Height="16" ctype="ImageViewObjectData">
                <Size X="44.0000" Y="44.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="12.2732" Y="10.5988" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5114" Y="0.4416" />
                <PreSize X="1.8333" Y="1.8333" />
                <FileData Type="MarkedSubImage" Path="hall/res/first/rank_item_bg4.png" Plist="hall/res/first.plist" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="51.1039" Y="62.9513" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="126" G="81" B="45" />
            <PrePosition X="0.0538" Y="0.4996" />
            <PreSize X="0.0253" Y="0.1905" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="yesterdayCoinIcon" ActionTag="117034771" Tag="162" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="371.2124" RightMargin="535.7876" TopMargin="38.9888" BottomMargin="44.0112" ctype="SpriteObjectData">
            <Size X="43.0000" Y="43.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="371.2124" Y="65.5112" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3907" Y="0.5199" />
            <PreSize X="0.0453" Y="0.3413" />
            <FileData Type="MarkedSubImage" Path="hall/res/first/coin_money.png" Plist="hall/res/first.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="msg_bg" ActionTag="1348562831" Tag="140" IconVisible="False" LeftMargin="722.0942" RightMargin="-3.0942" TopMargin="24.8040" BottomMargin="27.1960" ctype="SpriteObjectData">
            <Size X="231.0000" Y="74.0000" />
            <Children>
              <AbstractNodeData Name="txt" ActionTag="1849417721" Tag="142" IconVisible="False" LeftMargin="-151.0192" RightMargin="286.0192" TopMargin="3.2860" BottomMargin="-1.2860" FontSize="24" LabelText="获奖感言&#xA;&#xA;111" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="96.0000" Y="72.0000" />
                <AnchorPoint ScaleY="1.0000" />
                <Position X="-151.0192" Y="70.7140" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="64" G="49" B="49" />
                <PrePosition X="-0.6538" Y="0.9556" />
                <PreSize X="0.4156" Y="0.9730" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleY="0.5000" />
            <Position X="722.0942" Y="64.1960" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.7601" Y="0.5095" />
            <PreSize X="0.2432" Y="0.5873" />
            <FileData Type="MarkedSubImage" Path="hall/res/first/rank_msg.png" Plist="hall/res/first.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>