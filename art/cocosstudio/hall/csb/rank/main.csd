<GameFile>
  <PropertyGroup Name="main" Type="Scene" ID="3224adee-e185-4a47-a86b-e5bc5804225d" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Scene" Tag="1765" ctype="GameNodeObjectData">
        <Size X="960.0000" Y="640.0000" />
        <Children>
          <AbstractNodeData Name="bg" ActionTag="-524905953" Tag="207" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" TouchEnable="True" Scale9Enable="True" TopEage="92" Scale9OriginY="92" Scale9Width="1280" Scale9Height="628" ctype="ImageViewObjectData">
            <Size X="960.0000" Y="640.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="480.0000" Y="320.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="1.0000" Y="1.0000" />
            <FileData Type="Normal" Path="bg/bg_info.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="bg_info_content" ActionTag="1042981365" Tag="875" IconVisible="False" PositionPercentXEnabled="True" PercentHeightEnable="True" PercentHeightEnabled="True" LeftMargin="9.6000" RightMargin="652.4000" TopMargin="103.7386" BottomMargin="24.2614" LeftEage="25" RightEage="25" TopEage="25" BottomEage="85" Scale9OriginX="25" Scale9OriginY="25" Scale9Width="248" Scale9Height="456" ctype="ImageViewObjectData">
            <Size X="298.0000" Y="512.0000" />
            <AnchorPoint />
            <Position X="9.6000" Y="24.2614" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0100" Y="0.0379" />
            <PreSize X="0.3104" Y="0.8000" />
            <FileData Type="Normal" Path="bg/bg_info_content.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="top" ActionTag="1164458721" Tag="190" IconVisible="True" PercentWidthEnable="True" PercentWidthEnabled="True" TopMargin="-79.6800" BottomMargin="624.6800" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="True" ctype="ProjectNodeObjectData">
            <Size X="960.0000" Y="95.0000" />
            <AnchorPoint />
            <Position Y="624.6800" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition Y="0.9761" />
            <PreSize X="1.0000" Y="0.1484" />
            <FileData Type="Normal" Path="game_public/csb/hall_top.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="container" ActionTag="-375445601" Alpha="0" Tag="88" IconVisible="False" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" LeftMargin="329.4720" RightMargin="0.4800" TopMargin="98.6240" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="630.0480" Y="541.3760" />
            <AnchorPoint />
            <Position X="329.4720" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3432" />
            <PreSize X="0.6563" Y="0.8459" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="tab" ActionTag="665571512" Tag="293" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="22.4494" RightMargin="667.5505" TopMargin="128.0000" BottomMargin="112.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="0" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="270.0000" Y="400.0000" />
            <Children>
              <AbstractNodeData Name="tab1" ActionTag="-1952101503" Tag="122" IconVisible="False" PositionPercentYEnabled="True" HorizontalEdge="RightEdge" LeftMargin="5.3464" RightMargin="4.6536" TopMargin="25.3920" BottomMargin="266.6080" ctype="SpriteObjectData">
                <Size X="260.0000" Y="108.0000" />
                <Children>
                  <AbstractNodeData Name="line_0" ActionTag="89638992" Tag="123" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="-0.6484" RightMargin="-1.3516" TopMargin="-2.2572" BottomMargin="2.2572" ctype="SpriteObjectData">
                    <Size X="262.0000" Y="108.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="130.3516" Y="56.2572" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5014" Y="0.5209" />
                    <PreSize X="1.0077" Y="1.0000" />
                    <FileData Type="MarkedSubImage" Path="hall/res/info/tab_line.png" Plist="hall/res/myinfo.plist" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="txt" ActionTag="-1554977493" Tag="124" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="79.0080" RightMargin="75.9920" TopMargin="24.4688" BottomMargin="48.5312" FontSize="35" LabelText="財富榜" ShadowOffsetX="-1.0000" ShadowOffsetY="1.0000" ctype="TextObjectData">
                    <Size X="105.0000" Y="35.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="131.5080" Y="66.0312" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="254" B="176" />
                    <PrePosition X="0.5058" Y="0.6114" />
                    <PreSize X="0.4038" Y="0.3241" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="0" G="0" B="0" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="135.3464" Y="320.6080" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5013" Y="0.8015" />
                <PreSize X="0.9630" Y="0.2700" />
                <FileData Type="MarkedSubImage" Path="hall/res/info/tab_btn_1.png" Plist="hall/res/myinfo.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="tab2" ActionTag="509476545" Tag="296" IconVisible="False" PositionPercentYEnabled="True" HorizontalEdge="RightEdge" LeftMargin="5.3464" RightMargin="4.6536" TopMargin="145.3712" BottomMargin="146.6288" ctype="SpriteObjectData">
                <Size X="260.0000" Y="108.0000" />
                <Children>
                  <AbstractNodeData Name="line_0" ActionTag="2142700135" Tag="3310" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="-0.4001" RightMargin="-1.5999" TopMargin="-0.7560" BottomMargin="0.7560" ctype="SpriteObjectData">
                    <Size X="262.0000" Y="108.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="130.5999" Y="54.7560" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5023" Y="0.5070" />
                    <PreSize X="1.0077" Y="1.0000" />
                    <FileData Type="MarkedSubImage" Path="hall/res/info/tab_line.png" Plist="hall/res/myinfo.plist" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="txt" ActionTag="2104478276" Tag="297" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="59.2460" RightMargin="60.7540" TopMargin="28.4756" BottomMargin="44.5244" FontSize="35" LabelText="昨日盈利" ShadowOffsetX="-1.0000" ShadowOffsetY="1.0000" ctype="TextObjectData">
                    <Size X="140.0000" Y="35.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="129.2460" Y="62.0244" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="254" B="176" />
                    <PrePosition X="0.4971" Y="0.5743" />
                    <PreSize X="0.5385" Y="0.3241" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="0" G="0" B="0" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="135.3464" Y="200.6288" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5013" Y="0.5016" />
                <PreSize X="0.9630" Y="0.2700" />
                <FileData Type="MarkedSubImage" Path="hall/res/info/tab_btn_1.png" Plist="hall/res/myinfo.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="tab3" ActionTag="-1730858580" Tag="169" IconVisible="False" PositionPercentYEnabled="True" HorizontalEdge="RightEdge" LeftMargin="5.3466" RightMargin="4.6534" TopMargin="265.3508" BottomMargin="26.6492" ctype="SpriteObjectData">
                <Size X="260.0000" Y="108.0000" />
                <Children>
                  <AbstractNodeData Name="line" ActionTag="-638051153" Tag="171" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="0.1029" RightMargin="-2.1029" TopMargin="-2.2572" BottomMargin="2.2572" ctype="SpriteObjectData">
                    <Size X="262.0000" Y="108.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="131.1029" Y="56.2572" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5042" Y="0.5209" />
                    <PreSize X="1.0077" Y="1.0000" />
                    <FileData Type="MarkedSubImage" Path="hall/res/info/tab_line.png" Plist="hall/res/myinfo.plist" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="txt" ActionTag="-1679617581" Tag="170" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="66.7540" RightMargin="65.2460" TopMargin="27.7260" BottomMargin="47.2740" FontSize="32" LabelText="累計盈利" ShadowOffsetX="-1.0000" ShadowOffsetY="1.0000" ctype="TextObjectData">
                    <Size X="128.0000" Y="33.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="130.7540" Y="63.7740" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="254" B="176" />
                    <PrePosition X="0.5029" Y="0.5905" />
                    <PreSize X="0.4923" Y="0.3056" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="0" G="0" B="0" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="135.3466" Y="80.6492" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5013" Y="0.2016" />
                <PreSize X="0.9630" Y="0.2700" />
                <FileData Type="MarkedSubImage" Path="hall/res/info/tab_btn_1.png" Plist="hall/res/myinfo.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleY="1.0000" />
            <Position X="22.4494" Y="512.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0234" Y="0.8000" />
            <PreSize X="0.2813" Y="0.6250" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="my_rank" ActionTag="63730761" Tag="159" IconVisible="False" HorizontalEdge="LeftEdge" VerticalEdge="BottomEdge" LeftMargin="14.6176" RightMargin="720.3824" TopMargin="570.1456" BottomMargin="39.8543" FontSize="30" LabelText="我的排名:未上榜" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="225.0000" Y="30.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="14.6176" Y="54.8543" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0152" Y="0.0857" />
            <PreSize X="0.2344" Y="0.0469" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="rank_edit" ActionTag="1198847372" Tag="1271" IconVisible="False" HorizontalEdge="LeftEdge" VerticalEdge="BottomEdge" LeftMargin="234.4944" RightMargin="645.5056" TopMargin="544.3296" BottomMargin="15.6704" ctype="SpriteObjectData">
            <Size X="80.0000" Y="80.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="274.4944" Y="55.6704" />
            <Scale ScaleX="1.2000" ScaleY="1.2000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.2859" Y="0.0870" />
            <PreSize X="0.0833" Y="0.1250" />
            <FileData Type="MarkedSubImage" Path="hall/res/first/rank_edit.png" Plist="hall/res/first.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>