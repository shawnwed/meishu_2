<GameFile>
  <PropertyGroup Name="top" Type="Node" ID="e8891095-420d-4210-9b62-88783a39d318" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="8326" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="head" ActionTag="1744410204" Tag="8835" IconVisible="True" LeftMargin="0.4078" RightMargin="-0.4078" TopMargin="76.6999" BottomMargin="-76.6999" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="0.4078" Y="-76.6999" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="wenzhouShowHand/csb/table/player_self.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="banker" ActionTag="-864003278" Tag="9260" IconVisible="False" LeftMargin="-57.0995" RightMargin="16.0995" TopMargin="33.9658" BottomMargin="-75.9658" ctype="SpriteObjectData">
            <Size X="41.0000" Y="42.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-36.5995" Y="-54.9658" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="wenzhouShowHand/res/table/banker.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="btn_banker" ActionTag="-867025720" Tag="9414" IconVisible="False" LeftMargin="87.0067" RightMargin="-227.0067" TopMargin="52.5838" BottomMargin="-111.5838" ctype="SpriteObjectData">
            <Size X="140.0000" Y="59.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="157.0067" Y="-82.0838" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="wenzhouShowHand/res/table/btn_banker_1.png" Plist="wenzhouShowHand/res/table.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="banker_info_bg" ActionTag="-522118638" Tag="9780" IconVisible="False" LeftMargin="-335.5009" RightMargin="-335.4991" BottomMargin="-33.0000" LeftEage="100" RightEage="100" TopEage="10" BottomEage="10" Scale9OriginX="100" Scale9OriginY="10" Scale9Width="471" Scale9Height="13" ctype="ImageViewObjectData">
            <Size X="671.0000" Y="33.0000" />
            <Children>
              <AbstractNodeData Name="Text_7" ActionTag="1378616702" Tag="9665" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="357.3316" RightMargin="223.6684" TopMargin="5.5034" BottomMargin="7.4966" FontSize="20" LabelText="可连庄数:" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="90.0000" Y="20.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="402.3316" Y="17.4966" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="230" G="230" B="250" />
                <PrePosition X="0.5996" Y="0.5302" />
                <PreSize X="0.1341" Y="0.6061" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_8" ActionTag="-334200117" Tag="9666" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="116.0400" RightMargin="464.9600" TopMargin="6.7244" BottomMargin="6.2756" FontSize="20" LabelText="庄家收益:" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="90.0000" Y="20.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="161.0400" Y="16.2756" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="230" G="230" B="250" />
                <PrePosition X="0.2400" Y="0.4932" />
                <PreSize X="0.1341" Y="0.6061" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="banker_bank_count_txt" ActionTag="407837009" Tag="9667" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="462.6545" RightMargin="108.3455" TopMargin="5.5034" BottomMargin="7.4966" FontSize="20" LabelText="12局后下庄" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="100.0000" Y="20.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="462.6545" Y="17.4966" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="0" />
                <PrePosition X="0.6895" Y="0.5302" />
                <PreSize X="0.1490" Y="0.6061" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="banker_win_txt" ActionTag="443169070" Tag="9668" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="223.1746" RightMargin="397.8254" TopMargin="6.7211" BottomMargin="6.2789" FontSize="20" LabelText="12345" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="50.0000" Y="20.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="223.1746" Y="16.2789" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="0" />
                <PrePosition X="0.3326" Y="0.4933" />
                <PreSize X="0.0745" Y="0.6061" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5002" ScaleY="1.0000" />
            <Position X="0.1333" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="wenzhouShowHand/res/table/top_bg.png" Plist="" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>