<GameFile>
  <PropertyGroup Name="player_list_item" Type="Node" ID="0278a8e0-8aed-4a15-9143-c89c4d365162" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="3903" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="Image_1" ActionTag="589826508" Tag="925" IconVisible="False" RightMargin="-260.0000" TopMargin="-94.0000" Scale9Enable="True" LeftEage="85" RightEage="85" TopEage="37" BottomEage="37" Scale9OriginX="85" Scale9OriginY="37" Scale9Width="91" Scale9Height="20" ctype="ImageViewObjectData">
            <Size X="260.0000" Y="94.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position Y="47.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="wenzhouShowHand/res/table/bg_user.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="head" ActionTag="1113170595" Tag="3905" IconVisible="False" LeftMargin="13.0000" RightMargin="-89.0000" TopMargin="-85.0000" BottomMargin="9.0000" LeftEage="25" RightEage="25" TopEage="25" BottomEage="25" Scale9OriginX="25" Scale9OriginY="25" Scale9Width="30" Scale9Height="30" ctype="ImageViewObjectData">
            <Size X="76.0000" Y="76.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="51.0000" Y="47.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="wenzhouShowHand/res/table/seat_avatar.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="frame" ActionTag="668554122" Tag="3906" IconVisible="False" LeftMargin="9.0004" RightMargin="-93.0004" TopMargin="-89.0002" BottomMargin="5.0002" ctype="SpriteObjectData">
            <Size X="84.0000" Y="84.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="51.0004" Y="47.0002" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="wenzhouShowHand/res/table/seat_avatar_frame.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="name" ActionTag="-1071807007" Tag="3907" IconVisible="False" LeftMargin="97.8800" RightMargin="-247.8800" TopMargin="-75.6452" BottomMargin="50.6452" FontSize="25" LabelText="那么多为什么" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="150.0000" Y="25.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="97.8800" Y="63.1452" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="81" G="70" B="69" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="coin_type" ActionTag="-389734305" Tag="5676" IconVisible="False" LeftMargin="97.8801" RightMargin="-126.8801" TopMargin="-39.4331" BottomMargin="9.4331" ctype="SpriteObjectData">
            <Size X="38.0000" Y="38.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="97.8801" Y="24.4331" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="wenzhouShowHand/res/table/userinfo_coin.png" Plist="wenzhouShowHand/res/table.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="coin_num" ActionTag="783357198" Tag="1651" IconVisible="False" LeftMargin="142.5571" RightMargin="-234.5571" TopMargin="-42.1588" BottomMargin="9.1588" LabelText="54万" ctype="TextBMFontObjectData">
            <Size X="92.0000" Y="33.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="142.5571" Y="25.6588" />
            <Scale ScaleX="0.7500" ScaleY="0.7500" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <LabelBMFontFile_CNB Type="Normal" Path="hall/res/rank_num.fnt" Plist="" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>