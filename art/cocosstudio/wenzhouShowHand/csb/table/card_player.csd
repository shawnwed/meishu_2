<GameFile>
  <PropertyGroup Name="card_player" Type="Node" ID="61cd846b-61ab-4674-aa84-93935670c08a" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="6918" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="card_1" ActionTag="1431340250" Tag="6919" IconVisible="True" LeftMargin="-13.9310" RightMargin="13.9310" TopMargin="-3.3890" BottomMargin="3.3890" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="-13.9310" Y="3.3890" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="common/card.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="card_2" ActionTag="-1292331405" Tag="6822" IconVisible="True" LeftMargin="13.6093" RightMargin="-13.6093" TopMargin="-3.3890" BottomMargin="3.3890" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="13.6093" Y="3.3890" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="common/card.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="cards_type" ActionTag="1844524321" Tag="16111" IconVisible="False" LeftMargin="-56.0026" RightMargin="-55.9974" TopMargin="-3.9999" BottomMargin="-42.0001" ctype="SpriteObjectData">
            <Size X="112.0000" Y="46.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-0.0026" Y="-19.0001" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="wenzhouShowHand/res/table/card_type_bg.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>