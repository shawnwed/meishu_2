<GameFile>
  <PropertyGroup Name="user_info" Type="Node" ID="f95e2afc-644f-47ee-8532-7f66795bb094" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="318" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="mask" ActionTag="-452366622" Alpha="0" Tag="361" IconVisible="False" LeftMargin="-641.0000" RightMargin="-639.0000" TopMargin="-360.0000" BottomMargin="-360.0000" ClipAble="False" BackColorAlpha="102" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="1280.0000" Y="720.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-1.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <SingleColor A="255" R="26" G="26" B="26" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="bg" ActionTag="-430077352" Tag="360" IconVisible="False" LeftMargin="-457.5000" RightMargin="-457.5000" TopMargin="-288.0000" BottomMargin="-288.0000" TouchEnable="True" LeftEage="15" RightEage="15" TopEage="15" BottomEage="15" Scale9OriginX="15" Scale9OriginY="15" Scale9Width="885" Scale9Height="546" ctype="ImageViewObjectData">
            <Size X="915.0000" Y="576.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="wenzhouShowHand/res/dialog_bg.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="userinfo_win_8" ActionTag="-279234599" Tag="342" IconVisible="False" LeftMargin="-209.5000" RightMargin="176.5000" TopMargin="84.0000" BottomMargin="-117.0000" ctype="SpriteObjectData">
            <Size X="33.0000" Y="33.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-193.0000" Y="-100.5000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="wenzhouShowHand/res/table/userinfo_win.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="userinfo_times_7" ActionTag="1037470205" Tag="343" IconVisible="False" LeftMargin="-212.0000" RightMargin="174.0000" TopMargin="32.0000" BottomMargin="-66.0000" ctype="SpriteObjectData">
            <Size X="38.0000" Y="34.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-193.0000" Y="-49.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="wenzhouShowHand/res/table/userinfo_times.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="userinfo_score_5" ActionTag="363089538" Tag="345" IconVisible="False" LeftMargin="-207.5000" RightMargin="178.5000" TopMargin="-16.0000" BottomMargin="-14.0000" ctype="SpriteObjectData">
            <Size X="29.0000" Y="30.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-193.0000" Y="1.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="wenzhouShowHand/res/table/userinfo_score.png" Plist="wenzhouShowHand/res/table.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="name_txt" ActionTag="-1229865771" Tag="346" IconVisible="False" LeftMargin="-150.0000" RightMargin="-90.0000" TopMargin="-172.0000" BottomMargin="142.0000" FontSize="30" LabelText="玩家昵称一二三四" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="240.0000" Y="30.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="-150.0000" Y="157.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="237" B="207" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="paiju" ActionTag="-402556818" Tag="348" IconVisible="False" LeftMargin="-297.0000" RightMargin="225.0000" TopMargin="35.9583" BottomMargin="-59.9583" FontSize="24" LabelText="牌局数" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="72.0000" Y="24.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-261.0000" Y="-47.9583" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="0" G="208" B="250" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="shenglv" ActionTag="47176815" Tag="349" IconVisible="False" LeftMargin="-297.0000" RightMargin="225.0000" TopMargin="85.5417" BottomMargin="-109.5417" FontSize="24" LabelText="胜  率" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="72.0000" Y="24.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-261.0000" Y="-97.5417" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="0" G="208" B="250" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="times_txt" ActionTag="-691069905" Tag="351" IconVisible="False" LeftMargin="-158.0000" RightMargin="26.0000" TopMargin="35.9583" BottomMargin="-59.9583" FontSize="24" LabelText="200,000,000" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="132.0000" Y="24.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="-158.0000" Y="-47.9583" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="237" B="207" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="win_rate_txt" ActionTag="541646215" Tag="352" IconVisible="False" LeftMargin="-158.0000" RightMargin="26.0000" TopMargin="85.5417" BottomMargin="-109.5417" FontSize="24" LabelText="200,000,000" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="132.0000" Y="24.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="-158.0000" Y="-97.5417" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="237" B="207" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="head" ActionTag="-545856396" Tag="353" IconVisible="True" LeftMargin="-310.9998" RightMargin="198.9998" TopMargin="-165.9999" BottomMargin="53.9999" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="True" ctype="ProjectNodeObjectData">
            <Size X="112.0000" Y="112.0000" />
            <AnchorPoint />
            <Position X="-310.9998" Y="53.9999" />
            <Scale ScaleX="1.1806" ScaleY="1.1806" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="hall/csb/head.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="level" ActionTag="806667748" Tag="357" IconVisible="False" LeftMargin="-149.5000" RightMargin="93.5000" TopMargin="-108.5000" BottomMargin="75.5000" ctype="SpriteObjectData">
            <Size X="56.0000" Y="33.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="-149.5000" Y="92.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="hall/res/level/level1.png" Plist="hall/res/level.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="score" ActionTag="882373240" Tag="358" IconVisible="False" LeftMargin="-297.0000" RightMargin="225.0000" TopMargin="-13.6250" BottomMargin="-10.3750" FontSize="24" LabelText="积分值" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="72.0000" Y="24.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-261.0000" Y="1.6250" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="0" G="208" B="250" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="score_txt" ActionTag="-523388266" Tag="359" IconVisible="False" LeftMargin="-158.0000" RightMargin="26.0000" TopMargin="-13.6250" BottomMargin="-10.3750" FontSize="24" LabelText="200,000,000" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="132.0000" Y="24.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="-158.0000" Y="1.6250" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="237" B="207" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>