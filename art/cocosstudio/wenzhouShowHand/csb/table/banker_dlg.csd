<GameFile>
  <PropertyGroup Name="banker_dlg" Type="Node" ID="3b1905f2-0464-4984-bc7f-26c6225c02e1" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="3964" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="mask" CanEdit="False" ActionTag="-407423925" Alpha="0" Tag="3969" IconVisible="False" LeftMargin="-640.0000" RightMargin="-640.0000" TopMargin="-360.0000" BottomMargin="-360.0000" ClipAble="False" BackColorAlpha="102" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="1280.0000" Y="720.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <SingleColor A="255" R="26" G="26" B="26" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="bg" ActionTag="-1661615276" Tag="3965" IconVisible="False" LeftMargin="-410.5000" RightMargin="-410.5000" TopMargin="-331.5000" BottomMargin="-331.5000" Scale9Enable="True" LeftEage="220" RightEage="247" TopEage="190" BottomEage="184" Scale9OriginX="220" Scale9OriginY="190" Scale9Width="448" Scale9Height="202" ctype="ImageViewObjectData">
            <Size X="821.0000" Y="663.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="wenzhouShowHand/res/dialog_bg.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="title" ActionTag="-1419171340" Tag="3971" IconVisible="False" LeftMargin="-85.2103" RightMargin="-106.7897" TopMargin="-303.7080" BottomMargin="255.7080" FontSize="48" LabelText="上庄列表" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="192.0000" Y="48.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="10.7897" Y="279.7080" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="230" B="208" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="list" ActionTag="2066339052" Tag="3972" IconVisible="False" LeftMargin="-363.0000" RightMargin="-363.0000" TopMargin="-228.0000" BottomMargin="-178.0000" TouchEnable="True" ClipAble="True" BackColorAlpha="115" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" IsBounceEnabled="True" ScrollDirectionType="0" DirectionType="Vertical" ctype="ListViewObjectData">
            <Size X="726.0000" Y="406.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position Y="25.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <SingleColor A="255" R="192" G="159" B="143" />
            <FirstColor A="255" R="150" G="150" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="cancel_apply" ActionTag="719645513" Tag="3973" IconVisible="False" LeftMargin="-136.9999" RightMargin="-137.0001" TopMargin="204.3856" BottomMargin="-299.3856" ctype="SpriteObjectData">
            <Size X="274.0000" Y="95.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="0.0001" Y="-251.8856" />
            <Scale ScaleX="0.7700" ScaleY="0.7700" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="wenzhouShowHand/res/table/btn_cancel_apply.png" Plist="wenzhouShowHand/res/table.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="apply_panel" CanEdit="False" ActionTag="1353631251" Tag="3978" IconVisible="False" LeftMargin="-375.0000" RightMargin="-375.0000" TopMargin="177.7712" BottomMargin="-307.7712" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="750.0000" Y="130.0000" />
            <Children>
              <AbstractNodeData Name="slider" ActionTag="680081257" Tag="3970" IconVisible="False" LeftMargin="27.0000" RightMargin="249.0000" TopMargin="71.5000" BottomMargin="13.5000" TouchEnable="True" PercentInfo="50" ctype="SliderObjectData">
                <Size X="474.0000" Y="45.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="264.0000" Y="36.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3520" Y="0.2769" />
                <PreSize X="0.6320" Y="0.3462" />
                <BackGroundData Type="Normal" Path="wenzhouShowHand/res/table/slider_track.png" Plist="" />
                <ProgressBarData Type="Normal" Path="wenzhouShowHand/res/table/slider_fill.png" Plist="" />
                <BallNormalData Type="Normal" Path="wenzhouShowHand/res/table/slider_btn.png" Plist="" />
                <BallPressedData Type="Normal" Path="wenzhouShowHand/res/table/slider_btn.png" Plist="" />
                <BallDisabledData Type="Normal" Path="wenzhouShowHand/res/table/slider_btn.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="min_take_txt" ActionTag="246065444" Tag="3974" IconVisible="False" LeftMargin="143.0000" RightMargin="477.0000" TopMargin="6.0000" BottomMargin="104.0000" FontSize="20" LabelText="至少需要600万" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="130.0000" Y="20.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="143.0000" Y="114.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="81" G="70" B="69" />
                <PrePosition X="0.1907" Y="0.8769" />
                <PreSize X="0.1733" Y="0.1538" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="apply" ActionTag="421886941" Tag="3975" IconVisible="False" LeftMargin="502.0000" RightMargin="-26.0000" TopMargin="42.5000" BottomMargin="-7.5000" ctype="SpriteObjectData">
                <Size X="274.0000" Y="95.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="639.0000" Y="40.0000" />
                <Scale ScaleX="0.7700" ScaleY="0.7700" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.8520" Y="0.3077" />
                <PreSize X="0.3653" Y="0.7308" />
                <FileData Type="MarkedSubImage" Path="wenzhouShowHand/res/table/btn_apply.png" Plist="wenzhouShowHand/res/table.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_3" ActionTag="1932866924" Tag="3976" IconVisible="False" LeftMargin="137.0000" RightMargin="487.0000" TopMargin="35.0000" BottomMargin="67.0000" FontSize="28" LabelText="携带上庄:" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="126.0000" Y="28.0000" />
                <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
                <Position X="263.0000" Y="81.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="81" G="70" B="69" />
                <PrePosition X="0.3507" Y="0.6231" />
                <PreSize X="0.1680" Y="0.2154" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="cur_take_txt" ActionTag="2027809792" Tag="1364" IconVisible="False" LeftMargin="270.0474" RightMargin="387.9526" TopMargin="31.3396" BottomMargin="65.6604" LabelText="54万" ctype="TextBMFontObjectData">
                <Size X="92.0000" Y="33.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="270.0474" Y="82.1604" />
                <Scale ScaleX="0.7500" ScaleY="0.7500" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3601" Y="0.6320" />
                <PreSize X="0.1227" Y="0.2538" />
                <LabelBMFontFile_CNB Type="Normal" Path="hall/res/rank_num.fnt" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="auto_check" ActionTag="1290736728" Tag="1153" IconVisible="False" LeftMargin="548.5000" RightMargin="26.5000" TopMargin="12.0000" BottomMargin="84.0000" ctype="SpriteObjectData">
                <Size X="175.0000" Y="34.0000" />
                <Children>
                  <AbstractNodeData Name="txt" ActionTag="428567852" Tag="1154" IconVisible="False" LeftMargin="35.0000" RightMargin="-4.0000" TopMargin="8.0000" BottomMargin="8.0000" FontSize="18" LabelText="自动补齐上庄携带" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="144.0000" Y="18.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="107.0000" Y="17.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="165" G="42" B="42" />
                    <PrePosition X="0.6114" Y="0.5000" />
                    <PreSize X="0.8229" Y="0.5294" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="636.0000" Y="101.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.8480" Y="0.7769" />
                <PreSize X="0.2333" Y="0.2615" />
                <FileData Type="MarkedSubImage" Path="wenzhouShowHand/res/table/check_0.png" Plist="wenzhouShowHand/res/table.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position X="-375.0000" Y="-307.7712" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="closeBtn" ActionTag="-377384956" VisibleForFrame="False" Tag="771" IconVisible="False" LeftMargin="317.6864" RightMargin="-397.6864" TopMargin="-321.5493" BottomMargin="241.5493" ctype="SpriteObjectData">
            <Size X="80.0000" Y="80.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="357.6864" Y="281.5493" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="wenzhouShowHand/res/table/close.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>