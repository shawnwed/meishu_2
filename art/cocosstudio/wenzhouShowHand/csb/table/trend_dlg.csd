<GameFile>
  <PropertyGroup Name="trend_dlg" Type="Node" ID="97fbdbae-0267-4626-b186-6a9f6144d75e" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="3774" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="mask" ActionTag="958326213" Alpha="0" Tag="3961" IconVisible="False" LeftMargin="-640.0001" RightMargin="-639.9999" TopMargin="-361.4528" BottomMargin="-358.5472" ClipAble="False" BackColorAlpha="102" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="1280.0000" Y="720.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-0.0001" Y="1.4528" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <SingleColor A="255" R="26" G="26" B="26" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="bg" ActionTag="-808082714" Tag="3963" IconVisible="False" LeftMargin="260.0000" RightMargin="-640.0000" TopMargin="-360.0000" BottomMargin="-360.0000" TouchEnable="True" Scale9Enable="True" LeftEage="305" RightEage="305" TopEage="180" BottomEage="180" Scale9OriginX="-15" Scale9OriginY="180" Scale9Width="320" Scale9Height="360" ctype="ImageViewObjectData">
            <Size X="380.0000" Y="720.0000" />
            <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
            <Position X="640.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="wenzhouShowHand/res/table/bg.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="card_type_containor" ActionTag="-396694875" Tag="65" IconVisible="False" LeftMargin="269.5400" RightMargin="-635.5400" TopMargin="-360.0000" BottomMargin="-278.0000" TouchEnable="True" ClipAble="True" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" IsBounceEnabled="True" ScrollDirectionType="Vertical" ctype="ScrollViewObjectData">
            <Size X="366.0000" Y="638.0000" />
            <Children>
              <AbstractNodeData Name="card_type" ActionTag="1298175617" Tag="66" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="-5.0000" RightMargin="-5.0000" ctype="SpriteObjectData">
                <Size X="376.0000" Y="688.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="1.0000" />
                <Position X="183.0000" Y="688.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="1.0000" />
                <PreSize X="1.0273" Y="1.0000" />
                <FileData Type="Normal" Path="wenzhouShowHand/res/table/card_type.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="1.0000" ScaleY="1.0000" />
            <Position X="635.5400" Y="360.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <SingleColor A="255" R="255" G="150" B="100" />
            <FirstColor A="255" R="255" G="150" B="100" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
            <InnerNodeSize Width="366" Height="700" />
          </AbstractNodeData>
          <AbstractNodeData Name="card_record_containor" ActionTag="-1639645711" Tag="343" IconVisible="False" LeftMargin="269.5400" RightMargin="-635.5400" TopMargin="-360.0000" BottomMargin="-278.0000" TouchEnable="True" ClipAble="True" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="366.0000" Y="638.0000" />
            <Children>
              <AbstractNodeData Name="flag1" ActionTag="1354735423" Alpha="153" Tag="3779" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="23.5122" RightMargin="267.4878" TopMargin="18.5000" BottomMargin="574.5000" ctype="SpriteObjectData">
                <Size X="75.0000" Y="45.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="61.0122" Y="597.0000" />
                <Scale ScaleX="0.8000" ScaleY="0.8000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1667" Y="0.9357" />
                <PreSize X="0.2049" Y="0.0705" />
                <FileData Type="Normal" Path="wenzhouShowHand/res/table/shunmen.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="flag2" ActionTag="1818455127" Alpha="153" Tag="3780" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="147.5130" RightMargin="143.4870" TopMargin="18.5000" BottomMargin="574.5000" ctype="SpriteObjectData">
                <Size X="75.0000" Y="45.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="185.0130" Y="597.0000" />
                <Scale ScaleX="0.8000" ScaleY="0.8000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5055" Y="0.9357" />
                <PreSize X="0.2049" Y="0.0705" />
                <FileData Type="Normal" Path="wenzhouShowHand/res/table/duimen.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="flag3" ActionTag="-1384150221" Alpha="153" Tag="3781" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="267.4878" RightMargin="23.5122" TopMargin="18.5000" BottomMargin="574.5000" ctype="SpriteObjectData">
                <Size X="75.0000" Y="45.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="304.9878" Y="597.0000" />
                <Scale ScaleX="0.8000" ScaleY="0.8000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.8333" Y="0.9357" />
                <PreSize X="0.2049" Y="0.0705" />
                <FileData Type="Normal" Path="wenzhouShowHand/res/table/daomen.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
            <Position X="635.5400" Y="41.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="card_type_btn" ActionTag="-398086133" Tag="339" IconVisible="False" LeftMargin="276.0000" RightMargin="-455.0000" TopMargin="290.0000" BottomMargin="-350.0000" ctype="SpriteObjectData">
            <Size X="179.0000" Y="60.0000" />
            <Children>
              <AbstractNodeData Name="Text" ActionTag="901521031" Tag="340" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="37.5000" RightMargin="37.5000" TopMargin="17.0000" BottomMargin="17.0000" FontSize="26" LabelText="牌型介绍" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="104.0000" Y="26.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="89.5000" Y="30.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5000" />
                <PreSize X="0.5810" Y="0.4333" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
            <Position X="455.0000" Y="-320.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="wenzhouShowHand/res/table/record_btn_1_1.png" Plist="wenzhouShowHand/res/table.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="card_record_btn" ActionTag="-1543988970" Tag="341" IconVisible="False" LeftMargin="455.0000" RightMargin="-634.0000" TopMargin="290.0000" BottomMargin="-350.0000" ctype="SpriteObjectData">
            <Size X="179.0000" Y="60.0000" />
            <Children>
              <AbstractNodeData Name="Text" ActionTag="-1101864643" Tag="342" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="37.5000" RightMargin="37.5000" TopMargin="17.0000" BottomMargin="17.0000" FontSize="26" LabelText="上局回顾" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="104.0000" Y="26.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="89.5000" Y="30.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5000" />
                <PreSize X="0.5810" Y="0.4333" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleY="0.5000" />
            <Position X="455.0000" Y="-320.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="wenzhouShowHand/res/table/record_btn_2_1.png" Plist="wenzhouShowHand/res/table.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>