<GameFile>
  <PropertyGroup Name="banker_result_dlg" Type="Node" ID="e87d4100-5366-4b03-aed1-a93565dde0e4" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="1860" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="mask" CanEdit="False" ActionTag="1256035271" Alpha="0" Tag="1875" IconVisible="False" LeftMargin="-640.0000" RightMargin="-640.0000" TopMargin="-360.0000" BottomMargin="-360.0000" ClipAble="False" BackColorAlpha="102" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="1280.0000" Y="720.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <SingleColor A="255" R="26" G="26" B="26" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="bg" ActionTag="1213046519" Tag="1874" IconVisible="False" LeftMargin="-456.5000" RightMargin="-458.5000" TopMargin="-288.0000" BottomMargin="-288.0000" LeftEage="257" RightEage="257" TopEage="213" BottomEage="213" Scale9OriginX="257" Scale9OriginY="213" Scale9Width="401" Scale9Height="150" ctype="ImageViewObjectData">
            <Size X="915.0000" Y="576.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1.0000" />
            <Scale ScaleX="0.8000" ScaleY="0.8000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="wenzhouShowHand/res/dialog_bg.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="title" ActionTag="1818688082" Tag="1888" IconVisible="False" LeftMargin="-60.0000" RightMargin="-60.0000" TopMargin="-201.9183" BottomMargin="171.9183" FontSize="30" LabelText="庄家结算" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="120.0000" Y="30.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position Y="186.9183" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="230" B="208" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="txt1" ActionTag="-576615847" Tag="1889" IconVisible="False" LeftMargin="-306.0000" RightMargin="252.0000" TopMargin="-113.0000" BottomMargin="86.0000" FontSize="27" LabelText="坐庄" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="54.0000" Y="27.0000" />
            <Children>
              <AbstractNodeData Name="total_time" ActionTag="-1174891601" Tag="1894" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="60.9984" RightMargin="-82.9984" TopMargin="-5.5000" BottomMargin="-5.5000" FontSize="38" LabelText=" 99 " ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="76.0000" Y="38.0000" />
                <Children>
                  <AbstractNodeData Name="txt2" ActionTag="-560407090" Tag="2313" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="72.4356" RightMargin="-133.4356" TopMargin="5.5000" BottomMargin="5.5000" FontSize="27" LabelText="局 ,  胜利" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="137.0000" Y="27.0000" />
                    <Children>
                      <AbstractNodeData Name="win_time" ActionTag="-763768302" Tag="1895" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="144.3432" RightMargin="-83.3432" TopMargin="-5.5000" BottomMargin="-5.5000" FontSize="38" LabelText=" 99 " ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                        <Size X="76.0000" Y="38.0000" />
                        <Children>
                          <AbstractNodeData Name="txt3" ActionTag="-772675309" Tag="2314" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="74.8144" RightMargin="-162.8144" TopMargin="5.5000" BottomMargin="5.5000" FontSize="27" LabelText="局 ,  总带入" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                            <Size X="164.0000" Y="27.0000" />
                            <Children>
                              <AbstractNodeData Name="take" ActionTag="921635282" Tag="1897" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="164.0000" RightMargin="-70.0000" FontSize="27" LabelText=" 800w" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                                <Size X="70.0000" Y="27.0000" />
                                <AnchorPoint ScaleY="0.5000" />
                                <Position X="164.0000" Y="13.5000" />
                                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                                <CColor A="255" R="165" G="42" B="42" />
                                <PrePosition X="1.0000" Y="0.5000" />
                                <PreSize X="0.4268" Y="1.0000" />
                                <OutlineColor A="255" R="255" G="0" B="0" />
                                <ShadowColor A="255" R="110" G="110" B="110" />
                              </AbstractNodeData>
                            </Children>
                            <AnchorPoint ScaleY="0.5000" />
                            <Position X="74.8144" Y="19.0000" />
                            <Scale ScaleX="1.0000" ScaleY="1.0000" />
                            <CColor A="255" R="81" G="70" B="69" />
                            <PrePosition X="0.9844" Y="0.5000" />
                            <PreSize X="2.1579" Y="0.7105" />
                            <OutlineColor A="255" R="255" G="0" B="0" />
                            <ShadowColor A="255" R="110" G="110" B="110" />
                          </AbstractNodeData>
                        </Children>
                        <AnchorPoint ScaleY="0.5000" />
                        <Position X="144.3432" Y="13.5000" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="165" G="42" B="42" />
                        <PrePosition X="1.0536" Y="0.5000" />
                        <PreSize X="0.5547" Y="1.4074" />
                        <OutlineColor A="255" R="255" G="0" B="0" />
                        <ShadowColor A="255" R="110" G="110" B="110" />
                      </AbstractNodeData>
                    </Children>
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="72.4356" Y="19.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="81" G="70" B="69" />
                    <PrePosition X="0.9531" Y="0.5000" />
                    <PreSize X="1.8026" Y="0.7105" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleY="0.5000" />
                <Position X="60.9984" Y="13.5000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="165" G="42" B="42" />
                <PrePosition X="1.1296" Y="0.5000" />
                <PreSize X="1.4074" Y="1.4074" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleY="0.5000" />
            <Position X="-306.0000" Y="99.5000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="81" G="70" B="69" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Text_3" ActionTag="205119797" Tag="1891" IconVisible="False" LeftMargin="-306.0000" RightMargin="252.0000" TopMargin="10.0000" BottomMargin="-37.0000" FontSize="27" LabelText="盈亏" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="54.0000" Y="27.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="-306.0000" Y="-23.5000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="81" G="70" B="69" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Text_2" ActionTag="-1438100356" Tag="1892" IconVisible="False" LeftMargin="-306.0000" RightMargin="198.0000" TopMargin="132.5000" BottomMargin="-159.5000" FontSize="27" LabelText="最佳赢币" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="108.0000" Y="27.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="-306.0000" Y="-146.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="81" G="70" B="69" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Text_1" ActionTag="1435391802" Tag="1893" IconVisible="False" LeftMargin="32.0000" RightMargin="-140.0000" TopMargin="132.5000" BottomMargin="-159.5000" FontSize="27" LabelText="最惨输币" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="108.0000" Y="27.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="32.0000" Y="-146.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="81" G="70" B="69" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="win_score" ActionTag="294779503" Tag="1898" IconVisible="False" LeftMargin="-226.0000" RightMargin="-44.0000" TopMargin="-6.5000" BottomMargin="-53.5000" FontSize="60" LabelText="+10000000" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="270.0000" Y="60.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="-226.0000" Y="-23.5000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="165" G="42" B="42" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="max_win" ActionTag="-155494548" Tag="1900" IconVisible="False" LeftMargin="-183.5000" RightMargin="145.5000" TopMargin="127.5000" BottomMargin="-165.5000" FontSize="38" LabelText="99" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="38.0000" Y="38.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="-183.5000" Y="-146.5000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="165" G="42" B="42" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="max_lose" ActionTag="127317189" Tag="1901" IconVisible="False" LeftMargin="153.5000" RightMargin="-191.5000" TopMargin="127.5000" BottomMargin="-165.5000" FontSize="38" LabelText="99" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="38.0000" Y="38.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="153.5000" Y="-146.5000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="165" G="42" B="42" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>