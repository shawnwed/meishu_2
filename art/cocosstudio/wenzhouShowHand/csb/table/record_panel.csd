<GameFile>
  <PropertyGroup Name="record_panel" Type="Node" ID="b20abcc8-cbd6-4d0f-90c7-4d7588a03833" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="866" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="bg" ActionTag="1534297742" Tag="1025" IconVisible="False" LeftMargin="-372.0000" BottomMargin="-720.0000" TouchEnable="True" ClipAble="False" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="372.0000" Y="720.0000" />
            <AnchorPoint ScaleX="1.0000" ScaleY="1.0000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <SingleColor A="255" R="10" G="45" B="66" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="cards_type" Visible="False" ActionTag="-1230213044" Tag="941" IconVisible="False" LeftMargin="-373.0000" BottomMargin="-720.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="0" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="373.0000" Y="720.0000" />
            <Children>
              <AbstractNodeData Name="record_dark_10" ActionTag="-2137062529" Tag="942" IconVisible="False" LeftMargin="0.2014" RightMargin="-3.2014" TopMargin="69.0251" BottomMargin="588.9749" ctype="SpriteObjectData">
                <Size X="376.0000" Y="62.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="188.2014" Y="619.9749" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5046" Y="0.8611" />
                <PreSize X="1.0080" Y="0.0861" />
                <FileData Type="MarkedSubImage" Path="showhand/res/table/record_dark.png" Plist="showhand/res/table.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="record_dark_10_0" ActionTag="412594012" Tag="943" IconVisible="False" LeftMargin="0.2014" RightMargin="-3.2014" TopMargin="192.7197" BottomMargin="465.2803" ctype="SpriteObjectData">
                <Size X="376.0000" Y="62.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="188.2014" Y="496.2803" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5046" Y="0.6893" />
                <PreSize X="1.0080" Y="0.0861" />
                <FileData Type="MarkedSubImage" Path="showhand/res/table/record_dark.png" Plist="showhand/res/table.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="record_dark_10_0_0" ActionTag="1990083562" Tag="944" IconVisible="False" LeftMargin="0.2014" RightMargin="-3.2014" TopMargin="316.4142" BottomMargin="341.5858" ctype="SpriteObjectData">
                <Size X="376.0000" Y="62.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="188.2014" Y="372.5858" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5046" Y="0.5175" />
                <PreSize X="1.0080" Y="0.0861" />
                <FileData Type="MarkedSubImage" Path="showhand/res/table/record_dark.png" Plist="showhand/res/table.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="record_dark_10_0_1" ActionTag="-1883850" Tag="945" IconVisible="False" LeftMargin="0.2014" RightMargin="-3.2014" TopMargin="440.1087" BottomMargin="217.8913" ctype="SpriteObjectData">
                <Size X="376.0000" Y="62.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="188.2014" Y="248.8913" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5046" Y="0.3457" />
                <PreSize X="1.0080" Y="0.0861" />
                <FileData Type="MarkedSubImage" Path="showhand/res/table/record_dark.png" Plist="showhand/res/table.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="record_dark_10_0_3" ActionTag="-1098851818" Tag="946" IconVisible="False" LeftMargin="0.2014" RightMargin="-3.2014" TopMargin="563.8033" BottomMargin="94.1967" ctype="SpriteObjectData">
                <Size X="376.0000" Y="62.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="188.2014" Y="125.1967" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5046" Y="0.1739" />
                <PreSize X="1.0080" Y="0.0861" />
                <FileData Type="MarkedSubImage" Path="showhand/res/table/record_dark.png" Plist="showhand/res/table.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="card_type_introduce_9" ActionTag="-260666847" Tag="947" IconVisible="False" LeftMargin="15.9615" RightMargin="159.0385" TopMargin="11.2806" BottomMargin="100.7194" ctype="SpriteObjectData">
                <Size X="198.0000" Y="608.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="114.9615" Y="404.7194" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3082" Y="0.5621" />
                <PreSize X="0.5308" Y="0.8444" />
                <FileData Type="MarkedSubImage" Path="showhand/res/table/card_type_introduce.png" Plist="showhand/res/table.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="txt10" ActionTag="-1190794425" Tag="948" IconVisible="False" LeftMargin="215.9164" RightMargin="7.0836" TopMargin="21.3320" BottomMargin="668.6680" FontSize="30" LabelText="皇家同花顺" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="150.0000" Y="30.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="290.9164" Y="683.6680" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7799" Y="0.9495" />
                <PreSize X="0.4021" Y="0.0417" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="txt9" ActionTag="29437994" Tag="949" IconVisible="False" LeftMargin="245.9164" RightMargin="37.0836" TopMargin="83.5035" BottomMargin="606.4965" FontSize="30" LabelText="同花顺" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="90.0000" Y="30.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="290.9164" Y="621.4965" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7799" Y="0.8632" />
                <PreSize X="0.2413" Y="0.0417" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="txt8" ActionTag="500544971" Tag="950" IconVisible="False" LeftMargin="230.9164" RightMargin="22.0836" TopMargin="145.6747" BottomMargin="544.3253" FontSize="30" LabelText="四    条" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="120.0000" Y="30.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="290.9164" Y="559.3253" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7799" Y="0.7768" />
                <PreSize X="0.3217" Y="0.0417" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="txt7" ActionTag="-1743867174" Tag="951" IconVisible="False" LeftMargin="230.9164" RightMargin="22.0836" TopMargin="207.8462" BottomMargin="482.1538" FontSize="30" LabelText="葫    芦" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="120.0000" Y="30.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="290.9164" Y="497.1538" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7799" Y="0.6905" />
                <PreSize X="0.3217" Y="0.0417" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="txt6" ActionTag="-1915625909" Tag="952" IconVisible="False" LeftMargin="230.9164" RightMargin="22.0836" TopMargin="270.0175" BottomMargin="419.9825" FontSize="30" LabelText="同    花" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="120.0000" Y="30.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="290.9164" Y="434.9825" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7799" Y="0.6041" />
                <PreSize X="0.3217" Y="0.0417" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="txt5" ActionTag="1932104505" Tag="953" IconVisible="False" LeftMargin="230.9164" RightMargin="22.0836" TopMargin="332.1888" BottomMargin="357.8112" FontSize="30" LabelText="顺    子" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="120.0000" Y="30.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="290.9164" Y="372.8112" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7799" Y="0.5178" />
                <PreSize X="0.3217" Y="0.0417" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="txt4" ActionTag="1990310748" Tag="954" IconVisible="False" LeftMargin="230.9164" RightMargin="22.0836" TopMargin="394.3603" BottomMargin="295.6397" FontSize="30" LabelText="三    条" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="120.0000" Y="30.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="290.9164" Y="310.6397" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7799" Y="0.4314" />
                <PreSize X="0.3217" Y="0.0417" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="txt3" ActionTag="281106840" Tag="956" IconVisible="False" LeftMargin="230.9164" RightMargin="22.0836" TopMargin="456.5317" BottomMargin="233.4683" FontSize="30" LabelText="两    对" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="120.0000" Y="30.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="290.9164" Y="248.4683" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7799" Y="0.3451" />
                <PreSize X="0.3217" Y="0.0417" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="txt2" ActionTag="874606829" Tag="957" IconVisible="False" LeftMargin="230.9164" RightMargin="22.0836" TopMargin="518.7030" BottomMargin="171.2970" FontSize="30" LabelText="对    子" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="120.0000" Y="30.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="290.9164" Y="186.2970" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7799" Y="0.2587" />
                <PreSize X="0.3217" Y="0.0417" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="txt1" ActionTag="820857023" Tag="955" IconVisible="False" LeftMargin="230.9164" RightMargin="22.0836" TopMargin="580.8744" BottomMargin="109.1256" FontSize="30" LabelText="高    牌" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="120.0000" Y="30.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="290.9164" Y="124.1256" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7799" Y="0.1724" />
                <PreSize X="0.3217" Y="0.0417" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position X="-373.0000" Y="-720.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="prev_record" ActionTag="-614883806" Tag="871" IconVisible="False" LeftMargin="-373.0000" BottomMargin="-720.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="0" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="373.0000" Y="720.0000" />
            <Children>
              <AbstractNodeData Name="list" ActionTag="-815659366" Tag="720" IconVisible="False" BottomMargin="70.0000" TouchEnable="True" ClipAble="True" BackColorAlpha="0" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" IsBounceEnabled="True" ScrollDirectionType="0" DirectionType="Vertical" ctype="ListViewObjectData">
                <Size X="373.0000" Y="650.0000" />
                <AnchorPoint />
                <Position Y="70.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition Y="0.0972" />
                <PreSize X="1.0000" Y="0.9028" />
                <SingleColor A="255" R="150" G="150" B="255" />
                <FirstColor A="255" R="150" G="150" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="tips" ActionTag="-1506019996" Tag="940" IconVisible="False" LeftMargin="117.7003" RightMargin="111.2997" TopMargin="284.2611" BottomMargin="399.7389" FontSize="36" LabelText="暂无牌局" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="144.0000" Y="36.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="189.7003" Y="417.7389" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5086" Y="0.5802" />
                <PreSize X="0.3861" Y="0.0500" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position X="-373.0000" Y="-720.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="btn_2" ActionTag="-2057940985" Tag="867" IconVisible="False" LeftMargin="-187.0336" RightMargin="8.0336" TopMargin="655.5000" BottomMargin="-715.5000" ctype="SpriteObjectData">
            <Size X="179.0000" Y="60.0000" />
            <Children>
              <AbstractNodeData Name="txt" ActionTag="-2033229802" Tag="868" IconVisible="False" LeftMargin="31.0479" RightMargin="27.9521" TopMargin="13.1575" BottomMargin="16.8425" FontSize="30" LabelText="上局回顾" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="120.0000" Y="30.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="91.0479" Y="31.8425" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="0" G="0" B="0" />
                <PrePosition X="0.5086" Y="0.5307" />
                <PreSize X="0.6704" Y="0.5000" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-97.5336" Y="-685.5000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="texas/res/table/record_btn_2_1.png" Plist="texas/res/table.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="btn_1" ActionTag="35075208" Tag="869" IconVisible="False" LeftMargin="-365.6562" RightMargin="186.6562" TopMargin="655.5000" BottomMargin="-715.5000" ctype="SpriteObjectData">
            <Size X="179.0000" Y="60.0000" />
            <Children>
              <AbstractNodeData Name="txt" ActionTag="-1989001976" Tag="870" IconVisible="False" LeftMargin="31.0479" RightMargin="27.9521" TopMargin="13.1575" BottomMargin="16.8425" FontSize="30" LabelText="牌型介绍" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="120.0000" Y="30.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="91.0479" Y="31.8425" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="0" G="0" B="0" />
                <PrePosition X="0.5086" Y="0.5307" />
                <PreSize X="0.6704" Y="0.5000" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-276.1562" Y="-685.5000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="texas/res/table/record_btn_1_1.png" Plist="texas/res/table.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>