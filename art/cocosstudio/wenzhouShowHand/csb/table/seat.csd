<GameFile>
  <PropertyGroup Name="seat" Type="Node" ID="db239171-8d45-44da-8449-5cce9ea642a1" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="469" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="bg" ActionTag="-206664660" Tag="470" IconVisible="False" LeftMargin="-66.0000" RightMargin="-66.0000" TopMargin="-63.0000" BottomMargin="-63.0000" ctype="SpriteObjectData">
            <Size X="132.0000" Y="126.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="wenzhouShowHand/res/table/seat_bg.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="win" ActionTag="444169200" Tag="1497" IconVisible="False" LeftMargin="-44.5517" RightMargin="-52.4483" TopMargin="-62.4871" BottomMargin="-34.5129" ctype="SpriteObjectData">
            <Size X="97.0000" Y="97.0000" />
            <Children>
              <AbstractNodeData Name="light" ActionTag="-69431199" Tag="1498" IconVisible="False" LeftMargin="-24.7608" RightMargin="-28.2392" TopMargin="-28.0215" BottomMargin="-24.9785" ctype="SpriteObjectData">
                <Size X="150.0000" Y="150.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="50.2392" Y="50.0215" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5179" Y="0.5157" />
                <PreSize X="1.5464" Y="1.5464" />
                <FileData Type="Normal" Path="wenzhouShowHand/res/table/win_light.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="3.9483" Y="13.9871" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="wenzhouShowHand/res/table/win_frame.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="btn_sit" ActionTag="1207711727" Tag="473" IconVisible="False" LeftMargin="-26.0389" RightMargin="-31.9611" TopMargin="-42.4422" BottomMargin="-33.5578" ctype="SpriteObjectData">
            <Size X="58.0000" Y="76.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="2.9611" Y="4.4422" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="wenzhouShowHand/res/table/btn_sit.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="head" ActionTag="261845795" Tag="14" IconVisible="False" LeftMargin="-36.0516" RightMargin="-43.9484" TopMargin="-54.0128" BottomMargin="-25.9872" LeftEage="25" RightEage="25" TopEage="25" BottomEage="25" Scale9OriginX="25" Scale9OriginY="25" Scale9Width="30" Scale9Height="30" ctype="ImageViewObjectData">
            <Size X="80.0000" Y="80.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="3.9484" Y="14.0128" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="wenzhouShowHand/res/table/seat_avatar.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="coin_txt" ActionTag="1540115746" Tag="472" IconVisible="False" LeftMargin="-43.5581" RightMargin="-52.4419" TopMargin="27.8778" BottomMargin="-51.8778" FontSize="24" LabelText="1000.0万" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="96.0000" Y="24.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="4.4419" Y="-39.8778" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="243" B="176" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>