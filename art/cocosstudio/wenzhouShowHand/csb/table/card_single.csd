<GameFile>
  <PropertyGroup Name="card_single" Type="Node" ID="c1fdb05d-ee43-4d54-8ae9-9536315c6b57" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="202" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="white_bg" ActionTag="-119653700" Tag="214" IconVisible="False" LeftMargin="-62.3343" RightMargin="-97.6657" TopMargin="-105.7433" BottomMargin="-106.2567" ctype="SpriteObjectData">
            <Size X="160.0000" Y="212.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="17.6657" Y="-0.2567" />
            <Scale ScaleX="0.3750" ScaleY="0.3750" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="common/cards/cards_white_bg.png" Plist="common/cards.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="cards_suit_2" ActionTag="-1947945397" Tag="210" IconVisible="False" LeftMargin="11.1030" RightMargin="-45.1030" TopMargin="-3.1836" BottomMargin="-37.8164" ctype="SpriteObjectData">
            <Size X="34.0000" Y="41.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="28.1030" Y="-17.3164" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="common/cards/cards_suit_0.png" Plist="common/cards.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="cards_suit_1" ActionTag="2134094436" Tag="211" IconVisible="False" LeftMargin="-15.5919" RightMargin="-18.4081" TopMargin="-19.1882" BottomMargin="-21.8118" ctype="SpriteObjectData">
            <Size X="34.0000" Y="41.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1.4081" Y="-1.3118" />
            <Scale ScaleX="0.6000" ScaleY="0.6000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="common/cards/cards_suit_0.png" Plist="common/cards.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="cards_points_1" ActionTag="1795458725" Tag="213" IconVisible="False" LeftMargin="-18.3741" RightMargin="-20.6259" TopMargin="-50.1781" BottomMargin="-3.8219" ctype="SpriteObjectData">
            <Size X="39.0000" Y="54.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1.1259" Y="23.1781" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="common/cards/cards_points_1_1.png" Plist="common/cards.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>