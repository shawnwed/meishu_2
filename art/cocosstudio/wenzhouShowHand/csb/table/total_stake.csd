<GameFile>
  <PropertyGroup Name="total_stake" Type="Node" ID="1d35a219-c1eb-4257-8f45-4cf46002a99b" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="3627" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="containor" ActionTag="38144673" Tag="3629" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-100.0000" RightMargin="-100.0000" TopMargin="-100.0000" BottomMargin="-100.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="200.0000" Y="200.0000" />
            <Children>
              <AbstractNodeData Name="Text_1" ActionTag="-45673518" Tag="3009" IconVisible="False" LeftMargin="33.1879" RightMargin="94.8121" TopMargin="75.1377" BottomMargin="100.8623" FontSize="24" LabelText="总注：" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="72.0000" Y="24.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="69.1879" Y="112.8623" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="228" B="80" />
                <PrePosition X="0.3459" Y="0.5643" />
                <PreSize X="0.3600" Y="0.1200" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="stake_tip" ActionTag="-115801802" VisibleForFrame="False" Tag="3628" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="26.1400" RightMargin="104.8600" TopMargin="84.5000" BottomMargin="84.5000" ctype="SpriteObjectData">
                <Size X="69.0000" Y="31.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="60.6400" Y="100.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3032" Y="0.5000" />
                <PreSize X="0.3450" Y="0.1550" />
                <FileData Type="Normal" Path="wenzhouShowHand/res/table/all_stake.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="stake" ActionTag="1583306927" Tag="3010" IconVisible="False" LeftMargin="106.1836" RightMargin="9.8164" TopMargin="74.7131" BottomMargin="101.2869" FontSize="24" LabelText="999.99w" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="84.0000" Y="24.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="106.1836" Y="113.2869" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="228" B="80" />
                <PrePosition X="0.5309" Y="0.5664" />
                <PreSize X="0.4200" Y="0.1200" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>