<GameFile>
  <PropertyGroup Name="table" Type="Scene" ID="73be9952-97bc-4281-8f64-d18b2b0437d1" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Scene" Tag="46" ctype="GameNodeObjectData">
        <Size X="960.0000" Y="640.0000" />
        <Children>
          <AbstractNodeData Name="bg" ActionTag="-1304746002" Tag="391" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" LeftEage="422" RightEage="422" TopEage="237" BottomEage="237" Scale9OriginX="422" Scale9OriginY="237" Scale9Width="436" Scale9Height="246" ctype="ImageViewObjectData">
            <Size X="960.0000" Y="640.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="480.0000" Y="320.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="1.0000" Y="1.0000" />
            <FileData Type="Normal" Path="wenzhouShowHand/res/table/bj.jpg" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="window" ActionTag="-2018615739" Tag="1067" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" LeftMargin="134.4000" RightMargin="134.4000" TopMargin="132.6720" BottomMargin="155.3280" LeftEage="328" RightEage="328" TopEage="146" BottomEage="146" Scale9OriginX="328" Scale9OriginY="146" Scale9Width="263" Scale9Height="93" ctype="ImageViewObjectData">
            <Size X="691.2000" Y="352.0000" />
            <Children>
              <AbstractNodeData Name="bet_node_0" ActionTag="26624231" Tag="7219" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" LeftMargin="4.9766" RightMargin="461.5834" TopMargin="5.4912" BottomMargin="235.6288" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" LeftEage="109" RightEage="109" TopEage="49" BottomEage="49" Scale9OriginX="-109" Scale9OriginY="-49" Scale9Width="218" Scale9Height="98" ctype="PanelObjectData">
                <Size X="224.6400" Y="110.8800" />
                <Children>
                  <AbstractNodeData Name="name" ActionTag="1652569576" Tag="3620" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="82.3200" RightMargin="82.3200" TopMargin="27.9400" BottomMargin="27.9400" ctype="SpriteObjectData">
                    <Size X="60.0000" Y="55.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="112.3200" Y="55.4400" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="0.2671" Y="0.4960" />
                    <FileData Type="Normal" Path="wenzhouShowHand/res/table/door5.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="total_stake_txt" ActionTag="-172388989" Tag="7283" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="4.8522" RightMargin="174.7878" TopMargin="88.4666" BottomMargin="7.4134" LabelText="200,000" ctype="TextBMFontObjectData">
                    <Size X="45.0000" Y="15.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="4.8522" Y="14.9134" />
                    <Scale ScaleX="1.5000" ScaleY="1.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.0216" Y="0.1345" />
                    <PreSize X="0.2003" Y="0.1353" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="117.2966" Y="291.0688" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1697" Y="0.8269" />
                <PreSize X="0.3250" Y="0.3150" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="bet_node_1" ActionTag="-895088508" Tag="7199" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" LeftMargin="4.9766" RightMargin="461.5834" TopMargin="120.5600" BottomMargin="120.5600" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="224.6400" Y="110.8800" />
                <Children>
                  <AbstractNodeData Name="name" ActionTag="-1627157735" Tag="3622" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="58.3200" RightMargin="58.3200" TopMargin="30.9400" BottomMargin="30.9400" ctype="SpriteObjectData">
                    <Size X="108.0000" Y="49.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="112.3200" Y="55.4400" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="0.4808" Y="0.4419" />
                    <FileData Type="Normal" Path="wenzhouShowHand/res/table/door1.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="total_stake_txt" ActionTag="1667286042" Tag="7218" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="4.8522" RightMargin="174.7878" TopMargin="88.4666" BottomMargin="7.4134" LabelText="200,000" ctype="TextBMFontObjectData">
                    <Size X="45.0000" Y="15.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="4.8522" Y="14.9134" />
                    <Scale ScaleX="1.5000" ScaleY="1.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.0216" Y="0.1345" />
                    <PreSize X="0.2003" Y="0.1353" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="117.2966" Y="176.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1697" Y="0.5000" />
                <PreSize X="0.3250" Y="0.3150" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="bet_node_2" ActionTag="-1445196574" Tag="5867" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" LeftMargin="4.9766" RightMargin="461.5834" TopMargin="236.4736" BottomMargin="4.6464" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="224.6400" Y="110.8800" />
                <Children>
                  <AbstractNodeData Name="name" ActionTag="18037338" Tag="3624" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="90.3200" RightMargin="90.3200" TopMargin="27.9400" BottomMargin="27.9400" ctype="SpriteObjectData">
                    <Size X="44.0000" Y="55.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="112.3200" Y="55.4400" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="0.1959" Y="0.4960" />
                    <FileData Type="Normal" Path="wenzhouShowHand/res/table/door4.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="total_stake_txt" ActionTag="-1009688697" Tag="5875" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="4.8522" RightMargin="174.7878" TopMargin="88.4666" BottomMargin="7.4134" LabelText="200,000" ctype="TextBMFontObjectData">
                    <Size X="45.0000" Y="15.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="4.8522" Y="14.9134" />
                    <Scale ScaleX="1.5000" ScaleY="1.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.0216" Y="0.1345" />
                    <PreSize X="0.2003" Y="0.1353" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="117.2966" Y="60.0864" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1697" Y="0.1707" />
                <PreSize X="0.3250" Y="0.3150" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="bet_node_3" ActionTag="-24766158" Tag="7157" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" LeftMargin="233.2800" RightMargin="233.2800" TopMargin="236.4736" BottomMargin="4.6464" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="224.6400" Y="110.8800" />
                <Children>
                  <AbstractNodeData Name="name" ActionTag="1776203566" Tag="3626" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="58.8200" RightMargin="58.8200" TopMargin="28.4400" BottomMargin="28.4400" ctype="SpriteObjectData">
                    <Size X="107.0000" Y="54.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="112.3200" Y="55.4400" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="0.4763" Y="0.4870" />
                    <FileData Type="Normal" Path="wenzhouShowHand/res/table/door3.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="total_stake_txt" ActionTag="591252948" Tag="7178" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="4.8522" RightMargin="174.7878" TopMargin="88.4666" BottomMargin="7.4134" LabelText="200,000" ctype="TextBMFontObjectData">
                    <Size X="45.0000" Y="15.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="4.8522" Y="14.9134" />
                    <Scale ScaleX="1.5000" ScaleY="1.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.0216" Y="0.1345" />
                    <PreSize X="0.2003" Y="0.1353" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="345.6000" Y="60.0864" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.1707" />
                <PreSize X="0.3250" Y="0.3150" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="bet_node_4" ActionTag="-1718712677" Tag="7179" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" LeftMargin="462.1363" RightMargin="4.4237" TopMargin="234.8192" BottomMargin="2.7808" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="224.6400" Y="114.4000" />
                <Children>
                  <AbstractNodeData Name="name" ActionTag="856636155" Tag="3625" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="90.3200" RightMargin="90.3200" TopMargin="29.7000" BottomMargin="29.7000" ctype="SpriteObjectData">
                    <Size X="44.0000" Y="55.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="112.3200" Y="57.2000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="0.1959" Y="0.4808" />
                    <FileData Type="Normal" Path="wenzhouShowHand/res/table/door4.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="total_stake_txt" ActionTag="1516377868" Tag="7198" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="174.3609" RightMargin="5.2791" TopMargin="91.5132" BottomMargin="7.8868" LabelText="200,000" ctype="TextBMFontObjectData">
                    <Size X="45.0000" Y="15.0000" />
                    <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
                    <Position X="219.3609" Y="15.3868" />
                    <Scale ScaleX="1.5000" ScaleY="1.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.9765" Y="0.1345" />
                    <PreSize X="0.2003" Y="0.1311" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="574.4563" Y="59.9808" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.8311" Y="0.1704" />
                <PreSize X="0.3250" Y="0.3250" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="bet_node_5" ActionTag="516933068" Tag="7222" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" LeftMargin="462.1363" RightMargin="4.4237" TopMargin="118.8000" BottomMargin="118.8000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="224.6400" Y="114.4000" />
                <Children>
                  <AbstractNodeData Name="name" ActionTag="-1420860882" Tag="3623" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="58.3200" RightMargin="58.3200" TopMargin="30.7000" BottomMargin="30.7000" ctype="SpriteObjectData">
                    <Size X="108.0000" Y="53.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="112.3200" Y="57.2000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="0.4808" Y="0.4633" />
                    <FileData Type="Normal" Path="wenzhouShowHand/res/table/door2.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="total_stake_txt" ActionTag="2072398485" Tag="7294" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="174.3609" RightMargin="5.2791" TopMargin="91.5132" BottomMargin="7.8868" LabelText="200,000" ctype="TextBMFontObjectData">
                    <Size X="45.0000" Y="15.0000" />
                    <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
                    <Position X="219.3609" Y="15.3868" />
                    <Scale ScaleX="1.5000" ScaleY="1.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.9765" Y="0.1345" />
                    <PreSize X="0.2003" Y="0.1311" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="574.4563" Y="176.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.8311" Y="0.5000" />
                <PreSize X="0.3250" Y="0.3250" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="bet_node_6" ActionTag="-505242312" Tag="7221" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" LeftMargin="462.1363" RightMargin="4.4237" TopMargin="5.4912" BottomMargin="235.6288" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="224.6400" Y="110.8800" />
                <Children>
                  <AbstractNodeData Name="name" ActionTag="-1009228499" Tag="3621" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="82.3200" RightMargin="82.3200" TopMargin="27.9400" BottomMargin="27.9400" ctype="SpriteObjectData">
                    <Size X="60.0000" Y="55.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="112.3200" Y="55.4400" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="0.2671" Y="0.4960" />
                    <FileData Type="Normal" Path="wenzhouShowHand/res/table/door5.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="total_stake_txt" ActionTag="-947311430" Tag="7290" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="174.3609" RightMargin="5.2791" TopMargin="88.4666" BottomMargin="7.4134" LabelText="200,000" ctype="TextBMFontObjectData">
                    <Size X="45.0000" Y="15.0000" />
                    <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
                    <Position X="219.3609" Y="14.9134" />
                    <Scale ScaleX="1.5000" ScaleY="1.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.9765" Y="0.1345" />
                    <PreSize X="0.2003" Y="0.1353" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="574.4563" Y="291.0688" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.8311" Y="0.8269" />
                <PreSize X="0.3250" Y="0.3150" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="bet_node_7" ActionTag="761034729" Tag="7220" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" LeftMargin="233.2800" RightMargin="233.2800" TopMargin="5.4912" BottomMargin="235.6288" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="224.6400" Y="110.8800" />
                <Children>
                  <AbstractNodeData Name="name" ActionTag="-1805738975" Tag="3619" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="87.5164" RightMargin="86.1236" TopMargin="18.2110" BottomMargin="43.6690" ctype="SpriteObjectData">
                    <Size X="51.0000" Y="49.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="113.0164" Y="68.1690" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5031" Y="0.6148" />
                    <PreSize X="0.2270" Y="0.4419" />
                    <FileData Type="Normal" Path="wenzhouShowHand/res/table/door6.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="total_stake" ActionTag="632504890" Tag="3631" IconVisible="True" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="88.1795" RightMargin="136.4605" TopMargin="103.4919" BottomMargin="7.3881" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
                    <Size X="0.0000" Y="0.0000" />
                    <AnchorPoint />
                    <Position X="88.1795" Y="7.3881" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.3925" Y="0.0666" />
                    <PreSize X="0.0000" Y="0.0000" />
                    <FileData Type="Normal" Path="wenzhouShowHand/csb/table/total_stake.csd" Plist="" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="345.6000" Y="291.0688" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.8269" />
                <PreSize X="0.3250" Y="0.3150" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="480.0000" Y="331.3280" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5177" />
            <PreSize X="0.7200" Y="0.5500" />
            <FileData Type="Normal" Path="wenzhouShowHand/res/table/window.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="seat_0" ActionTag="1293723104" Tag="4165" IconVisible="True" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="50.4960" RightMargin="909.5040" TopMargin="129.2160" BottomMargin="510.7840" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="50.4960" Y="510.7840" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0526" Y="0.7981" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="wenzhouShowHand/csb/table/seat.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="seat_1" ActionTag="15573213" Tag="4173" IconVisible="True" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="50.4960" RightMargin="909.5040" TopMargin="251.7760" BottomMargin="388.2240" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="50.4960" Y="388.2240" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0526" Y="0.6066" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="wenzhouShowHand/csb/table/seat.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="seat_2" ActionTag="-648852230" Tag="4181" IconVisible="True" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="50.4960" RightMargin="909.5040" TopMargin="376.1920" BottomMargin="263.8080" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="50.4960" Y="263.8080" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0526" Y="0.4122" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="wenzhouShowHand/csb/table/seat.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="seat_3" ActionTag="-1364174373" Tag="4189" IconVisible="True" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="50.4960" RightMargin="909.5040" TopMargin="500.1600" BottomMargin="139.8400" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="50.4960" Y="139.8400" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0526" Y="0.2185" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="wenzhouShowHand/csb/table/seat.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="seat_4" ActionTag="833575033" Tag="4197" IconVisible="True" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="905.4720" RightMargin="54.5280" TopMargin="129.2160" BottomMargin="510.7840" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="905.4720" Y="510.7840" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.9432" Y="0.7981" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="wenzhouShowHand/csb/table/seat.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="seat_5" ActionTag="-1841659317" Tag="4205" IconVisible="True" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="905.4720" RightMargin="54.5280" TopMargin="251.7760" BottomMargin="388.2240" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="905.4720" Y="388.2240" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.9432" Y="0.6066" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="wenzhouShowHand/csb/table/seat.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="seat_6" ActionTag="-2074862856" Tag="4213" IconVisible="True" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="905.4720" RightMargin="54.5280" TopMargin="376.1920" BottomMargin="263.8080" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="905.4720" Y="263.8080" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.9432" Y="0.4122" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="wenzhouShowHand/csb/table/seat.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="seat_7" ActionTag="1071133282" Tag="4221" IconVisible="True" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="905.4720" RightMargin="54.5280" TopMargin="500.1600" BottomMargin="139.8400" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="905.4720" Y="139.8400" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.9432" Y="0.2185" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="wenzhouShowHand/csb/table/seat.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="top" ActionTag="-901208399" Tag="659" IconVisible="True" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="480.0000" RightMargin="480.0000" BottomMargin="640.0000" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="480.0000" Y="640.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="1.0000" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="wenzhouShowHand/csb/table/top.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="bottom" ActionTag="270796848" Tag="6768" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" LeftMargin="96.0000" RightMargin="96.0000" TopMargin="533.3120" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="768.0000" Y="106.6880" />
            <Children>
              <AbstractNodeData Name="content" ActionTag="608957867" Tag="6769" IconVisible="True" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="True" ctype="ProjectNodeObjectData">
                <Size X="768.0000" Y="106.6880" />
                <AnchorPoint />
                <Position />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="1.0000" Y="1.0000" />
                <FileData Type="Normal" Path="wenzhouShowHand/csb/table/bottom_layer.csd" Plist="" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" />
            <Position X="480.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" />
            <PreSize X="0.8000" Y="0.1667" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="self_bet_containor" CanEdit="False" ActionTag="-1737947343" Tag="3381" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" LeftMargin="107.3280" RightMargin="107.1360" TopMargin="112.6720" BottomMargin="132.6400" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="745.5360" Y="394.6880" />
            <Children>
              <AbstractNodeData Name="bet_node_0" ActionTag="-461027020" Tag="3382" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" LeftMargin="5.3679" RightMargin="497.8690" TopMargin="6.1571" BottomMargin="264.2041" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="242.2992" Y="124.3267" />
                <Children>
                  <AbstractNodeData Name="bord" ActionTag="1650981283" Tag="4194" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" LeftMargin="-3.7314" RightMargin="-3.7314" TopMargin="-4.0593" BottomMargin="-4.0593" LeftEage="109" RightEage="109" TopEage="49" BottomEage="49" Scale9OriginX="109" Scale9OriginY="49" Scale9Width="103" Scale9Height="39" ctype="ImageViewObjectData">
                    <Size X="249.7620" Y="132.4453" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="121.1496" Y="62.1634" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="1.0308" Y="1.0653" />
                    <FileData Type="Normal" Path="wenzhouShowHand/res/table/win_ligth_bord2.png" Plist="" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="self_bet_bg" ActionTag="-789723849" Tag="7281" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="6.3432" RightMargin="16.9559" TopMargin="-2.1612" BottomMargin="76.4879" ctype="SpriteObjectData">
                    <Size X="219.0000" Y="50.0000" />
                    <Children>
                      <AbstractNodeData Name="bet_txt" ActionTag="41951333" Tag="14922" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="79.5000" RightMargin="79.5000" TopMargin="10.0000" BottomMargin="10.0000" FontSize="30" LabelText="1000" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                        <Size X="60.0000" Y="30.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="109.5000" Y="25.0000" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="214" B="37" />
                        <PrePosition X="0.5000" Y="0.5000" />
                        <PreSize X="0.2740" Y="0.6000" />
                        <OutlineColor A="255" R="255" G="0" B="0" />
                        <ShadowColor A="255" R="110" G="110" B="110" />
                      </AbstractNodeData>
                    </Children>
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="115.8432" Y="101.4879" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.4781" Y="0.8163" />
                    <PreSize X="0.9038" Y="0.4022" />
                    <FileData Type="Normal" Path="wenzhouShowHand/res/table/own_stake_bg.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="126.5175" Y="326.3675" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1697" Y="0.8269" />
                <PreSize X="0.3250" Y="0.3150" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="bet_node_1" ActionTag="528731655" Tag="3383" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" LeftMargin="5.3679" RightMargin="497.8690" TopMargin="135.1806" BottomMargin="135.1806" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="242.2992" Y="124.3267" />
                <Children>
                  <AbstractNodeData Name="bord" ActionTag="1168614289" Tag="4195" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" LeftMargin="-3.7314" RightMargin="-3.7314" TopMargin="-4.0593" BottomMargin="-4.0593" LeftEage="109" RightEage="109" TopEage="49" BottomEage="49" Scale9OriginX="109" Scale9OriginY="49" Scale9Width="103" Scale9Height="40" ctype="ImageViewObjectData">
                    <Size X="249.7620" Y="132.4453" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="121.1496" Y="62.1634" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="1.0308" Y="1.0653" />
                    <FileData Type="Normal" Path="wenzhouShowHand/res/table/win_ligth_bord5.png" Plist="" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="self_bet_bg" ActionTag="-2081209730" Tag="7216" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="8.8147" RightMargin="14.4845" TopMargin="-2.3601" BottomMargin="76.6868" ctype="SpriteObjectData">
                    <Size X="219.0000" Y="50.0000" />
                    <Children>
                      <AbstractNodeData Name="bet_txt" ActionTag="-303005169" Tag="15147" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="79.5000" RightMargin="79.5000" TopMargin="10.0000" BottomMargin="10.0000" FontSize="30" LabelText="1000" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                        <Size X="60.0000" Y="30.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="109.5000" Y="25.0000" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="214" B="37" />
                        <PrePosition X="0.5000" Y="0.5000" />
                        <PreSize X="0.2740" Y="0.6000" />
                        <OutlineColor A="255" R="255" G="0" B="0" />
                        <ShadowColor A="255" R="110" G="110" B="110" />
                      </AbstractNodeData>
                    </Children>
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="118.3147" Y="101.6868" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.4883" Y="0.8179" />
                    <PreSize X="0.9038" Y="0.4022" />
                    <FileData Type="Normal" Path="wenzhouShowHand/res/table/own_stake_bg.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="126.5175" Y="197.3440" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1697" Y="0.5000" />
                <PreSize X="0.3250" Y="0.3150" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="bet_node_2" ActionTag="-95410717" Tag="3384" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" LeftMargin="5.3679" RightMargin="497.8690" TopMargin="265.1514" BottomMargin="5.2099" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="242.2992" Y="124.3267" />
                <Children>
                  <AbstractNodeData Name="bord" ActionTag="211966816" Tag="4196" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" LeftMargin="-3.7314" RightMargin="-3.7314" TopMargin="-4.0593" BottomMargin="-4.0593" LeftEage="109" RightEage="109" TopEage="49" BottomEage="49" Scale9OriginX="109" Scale9OriginY="49" Scale9Width="103" Scale9Height="39" ctype="ImageViewObjectData">
                    <Size X="249.7620" Y="132.4453" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="121.1496" Y="62.1634" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="1.0308" Y="1.0653" />
                    <FileData Type="Normal" Path="wenzhouShowHand/res/table/win_ligth_bord4.png" Plist="" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="self_bet_bg" ActionTag="29087272" Tag="5872" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="7.9909" RightMargin="15.3083" TopMargin="-4.1380" BottomMargin="78.4647" ctype="SpriteObjectData">
                    <Size X="219.0000" Y="50.0000" />
                    <Children>
                      <AbstractNodeData Name="bet_txt" ActionTag="-1356253917" Tag="15148" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="79.5000" RightMargin="79.5000" TopMargin="10.0000" BottomMargin="10.0000" FontSize="30" LabelText="1000" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                        <Size X="60.0000" Y="30.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="109.5000" Y="25.0000" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="214" B="37" />
                        <PrePosition X="0.5000" Y="0.5000" />
                        <PreSize X="0.2740" Y="0.6000" />
                        <OutlineColor A="255" R="255" G="0" B="0" />
                        <ShadowColor A="255" R="110" G="110" B="110" />
                      </AbstractNodeData>
                    </Children>
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="117.4909" Y="103.4647" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.4849" Y="0.8322" />
                    <PreSize X="0.9038" Y="0.4022" />
                    <FileData Type="Normal" Path="wenzhouShowHand/res/table/own_stake_bg.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="126.5175" Y="67.3732" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1697" Y="0.1707" />
                <PreSize X="0.3250" Y="0.3150" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="bet_node_3" ActionTag="-979593864" Tag="3385" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" LeftMargin="251.6184" RightMargin="251.6184" TopMargin="265.1514" BottomMargin="5.2099" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="242.2992" Y="124.3267" />
                <Children>
                  <AbstractNodeData Name="bord" ActionTag="-1251592765" Tag="4197" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" LeftMargin="-3.7314" RightMargin="-3.7314" TopMargin="-4.0593" BottomMargin="-4.0593" LeftEage="109" RightEage="109" TopEage="49" BottomEage="49" Scale9OriginX="109" Scale9OriginY="49" Scale9Width="103" Scale9Height="40" ctype="ImageViewObjectData">
                    <Size X="249.7620" Y="132.4453" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="121.1496" Y="62.1634" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="1.0308" Y="1.0653" />
                    <FileData Type="Normal" Path="wenzhouShowHand/res/table/win_ligth_bord5.png" Plist="" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="self_bet_bg" ActionTag="1695474144" Tag="7176" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="17.0044" RightMargin="6.2948" TopMargin="-4.3493" BottomMargin="78.6760" ctype="SpriteObjectData">
                    <Size X="219.0000" Y="50.0000" />
                    <Children>
                      <AbstractNodeData Name="bet_txt" ActionTag="17249778" Tag="15149" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="79.5000" RightMargin="79.5000" TopMargin="10.0000" BottomMargin="10.0000" FontSize="30" LabelText="1000" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                        <Size X="60.0000" Y="30.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="109.5000" Y="25.0000" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="214" B="37" />
                        <PrePosition X="0.5000" Y="0.5000" />
                        <PreSize X="0.2740" Y="0.6000" />
                        <OutlineColor A="255" R="255" G="0" B="0" />
                        <ShadowColor A="255" R="110" G="110" B="110" />
                      </AbstractNodeData>
                    </Children>
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="126.5044" Y="103.6760" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5221" Y="0.8339" />
                    <PreSize X="0.9038" Y="0.4022" />
                    <FileData Type="Normal" Path="wenzhouShowHand/res/table/own_stake_bg.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="372.7680" Y="67.3732" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.1707" />
                <PreSize X="0.3250" Y="0.3150" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="bet_node_4" ActionTag="856717375" Tag="3386" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" LeftMargin="498.4654" RightMargin="4.7714" TopMargin="263.2964" BottomMargin="3.1180" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="242.2992" Y="128.2736" />
                <Children>
                  <AbstractNodeData Name="bord" ActionTag="-27594394" Tag="4198" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" LeftMargin="-3.7314" RightMargin="-3.7314" TopMargin="-2.0844" BottomMargin="-2.0844" LeftEage="109" RightEage="109" TopEage="49" BottomEage="49" Scale9OriginX="109" Scale9OriginY="49" Scale9Width="103" Scale9Height="39" ctype="ImageViewObjectData">
                    <Size X="249.7620" Y="132.4425" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="121.1496" Y="64.1368" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="1.0308" Y="1.0325" />
                    <FileData Type="Normal" Path="wenzhouShowHand/res/table/win_ligth_bord3.png" Plist="" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="self_bet_bg" ActionTag="279824104" Tag="7196" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="17.5617" RightMargin="5.7375" TopMargin="-2.3212" BottomMargin="80.5948" ctype="SpriteObjectData">
                    <Size X="219.0000" Y="50.0000" />
                    <Children>
                      <AbstractNodeData Name="bet_txt" ActionTag="1476845118" Tag="15150" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="79.5000" RightMargin="79.5000" TopMargin="10.0000" BottomMargin="10.0000" FontSize="30" LabelText="1000" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                        <Size X="60.0000" Y="30.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="109.5000" Y="25.0000" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="214" B="37" />
                        <PrePosition X="0.5000" Y="0.5000" />
                        <PreSize X="0.2740" Y="0.6000" />
                        <OutlineColor A="255" R="255" G="0" B="0" />
                        <ShadowColor A="255" R="110" G="110" B="110" />
                      </AbstractNodeData>
                    </Children>
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="127.0617" Y="105.5948" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5244" Y="0.8232" />
                    <PreSize X="0.9038" Y="0.3898" />
                    <FileData Type="Normal" Path="wenzhouShowHand/res/table/own_stake_bg.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="619.6150" Y="67.2548" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.8311" Y="0.1704" />
                <PreSize X="0.3250" Y="0.3250" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="bet_node_5" ActionTag="2011970561" Tag="3387" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" LeftMargin="498.4654" RightMargin="4.7714" TopMargin="133.2072" BottomMargin="133.2072" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="242.2992" Y="128.2736" />
                <Children>
                  <AbstractNodeData Name="bord" ActionTag="1988288889" Tag="4199" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" LeftMargin="-3.7314" RightMargin="-3.7314" TopMargin="-2.5270" BottomMargin="-2.5270" LeftEage="109" RightEage="109" TopEage="49" BottomEage="49" Scale9OriginX="109" Scale9OriginY="49" Scale9Width="103" Scale9Height="40" ctype="ImageViewObjectData">
                    <Size X="249.7620" Y="133.3276" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="121.1496" Y="64.1368" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="1.0308" Y="1.0394" />
                    <FileData Type="Normal" Path="wenzhouShowHand/res/table/win_ligth_bord5.png" Plist="" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="self_bet_bg" ActionTag="-15794816" Tag="7293" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="10.1716" RightMargin="13.1276" TopMargin="-0.5511" BottomMargin="78.8246" ctype="SpriteObjectData">
                    <Size X="219.0000" Y="50.0000" />
                    <Children>
                      <AbstractNodeData Name="bet_txt" ActionTag="429678003" Tag="15151" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="79.5000" RightMargin="79.5000" TopMargin="10.0000" BottomMargin="10.0000" FontSize="30" LabelText="1000" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                        <Size X="60.0000" Y="30.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="109.5000" Y="25.0000" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="214" B="37" />
                        <PrePosition X="0.5000" Y="0.5000" />
                        <PreSize X="0.2740" Y="0.6000" />
                        <OutlineColor A="255" R="255" G="0" B="0" />
                        <ShadowColor A="255" R="110" G="110" B="110" />
                      </AbstractNodeData>
                    </Children>
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="119.6716" Y="103.8246" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.4939" Y="0.8094" />
                    <PreSize X="0.9038" Y="0.3898" />
                    <FileData Type="Normal" Path="wenzhouShowHand/res/table/own_stake_bg.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="619.6150" Y="197.3440" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.8311" Y="0.5000" />
                <PreSize X="0.3250" Y="0.3250" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="bet_node_6" ActionTag="955428326" Tag="3388" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" LeftMargin="498.4654" RightMargin="4.7714" TopMargin="6.1571" BottomMargin="264.2041" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="242.2992" Y="124.3267" />
                <Children>
                  <AbstractNodeData Name="bord" ActionTag="1153307855" Tag="4200" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" LeftMargin="-3.7314" RightMargin="-3.7314" TopMargin="-4.0593" BottomMargin="-4.0593" LeftEage="109" RightEage="109" TopEage="49" BottomEage="49" Scale9OriginX="109" Scale9OriginY="49" Scale9Width="103" Scale9Height="39" ctype="ImageViewObjectData">
                    <Size X="249.7620" Y="132.4453" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="121.1496" Y="62.1634" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="1.0308" Y="1.0653" />
                    <FileData Type="Normal" Path="wenzhouShowHand/res/table/win_ligth_bord1.png" Plist="" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="self_bet_bg" ActionTag="-980390298" Tag="7288" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="23.3042" RightMargin="-0.0050" TopMargin="-1.2660" BottomMargin="75.5927" ctype="SpriteObjectData">
                    <Size X="219.0000" Y="50.0000" />
                    <Children>
                      <AbstractNodeData Name="bet_txt" ActionTag="-1040885578" Tag="15152" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="79.5000" RightMargin="79.5000" TopMargin="10.0000" BottomMargin="10.0000" FontSize="30" LabelText="1000" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                        <Size X="60.0000" Y="30.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="109.5000" Y="25.0000" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="214" B="37" />
                        <PrePosition X="0.5000" Y="0.5000" />
                        <PreSize X="0.2740" Y="0.6000" />
                        <OutlineColor A="255" R="255" G="0" B="0" />
                        <ShadowColor A="255" R="110" G="110" B="110" />
                      </AbstractNodeData>
                    </Children>
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="132.8042" Y="100.5927" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5481" Y="0.8091" />
                    <PreSize X="0.9038" Y="0.4022" />
                    <FileData Type="Normal" Path="wenzhouShowHand/res/table/own_stake_bg.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="619.6150" Y="326.3675" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.8311" Y="0.8269" />
                <PreSize X="0.3250" Y="0.3150" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="480.0960" Y="329.9840" />
            <Scale ScaleX="0.9385" ScaleY="0.8686" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5001" Y="0.5156" />
            <PreSize X="0.7766" Y="0.6167" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="cards_0" ActionTag="-537855181" Tag="4357" IconVisible="True" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="474.9120" RightMargin="485.0880" TopMargin="187.4560" BottomMargin="452.5440" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="474.9120" Y="452.5440" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4947" Y="0.7071" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="wenzhouShowHand/csb/table/card_player.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="cards_1" ActionTag="2086708457" Tag="4312" IconVisible="True" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="252.6720" RightMargin="707.3280" TopMargin="313.0240" BottomMargin="326.9760" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="252.6720" Y="326.9760" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.2632" Y="0.5109" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="wenzhouShowHand/csb/table/card_player.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="cards_2" ActionTag="-691371115" Tag="4327" IconVisible="True" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="480.8640" RightMargin="479.1360" TopMargin="436.4800" BottomMargin="203.5200" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="480.8640" Y="203.5200" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5009" Y="0.3180" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="wenzhouShowHand/csb/table/card_player.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="cards_3" ActionTag="-1893622446" Tag="4342" IconVisible="True" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="721.6320" RightMargin="238.3680" TopMargin="311.4880" BottomMargin="328.5120" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="721.6320" Y="328.5120" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.7517" Y="0.5133" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="wenzhouShowHand/csb/table/card_player.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="menu" ActionTag="915648550" Tag="1068" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="2.3640" RightMargin="888.6360" TopMargin="1.9800" BottomMargin="569.0200" ctype="SpriteObjectData">
            <Size X="69.0000" Y="69.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="36.8640" Y="603.5200" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0384" Y="0.9430" />
            <PreSize X="0.0719" Y="0.1078" />
            <FileData Type="MarkedSubImage" Path="bullfighting/res/table/btn_menu.png" Plist="bullfighting/res/table.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="chat_btn" ActionTag="454957115" Tag="5196" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="873.9480" RightMargin="17.0520" TopMargin="564.7320" BottomMargin="6.2680" ctype="SpriteObjectData">
            <Size X="69.0000" Y="69.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="908.4480" Y="40.7680" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.9463" Y="0.0637" />
            <PreSize X="0.0719" Y="0.1078" />
            <FileData Type="MarkedSubImage" Path="bullfighting/res/table/btn_chat.png" Plist="bullfighting/res/table.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="record" ActionTag="2080829846" Tag="5868" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="882.7800" RightMargin="8.2200" TopMargin="1.0840" BottomMargin="569.9160" ctype="SpriteObjectData">
            <Size X="69.0000" Y="69.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="917.2800" Y="604.4160" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.9555" Y="0.9444" />
            <PreSize X="0.0719" Y="0.1078" />
            <FileData Type="Normal" Path="wenzhouShowHand/res/table/record.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="others" ActionTag="284806059" Tag="5869" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="8.8920" RightMargin="882.1080" TopMargin="565.2440" BottomMargin="5.7560" ctype="SpriteObjectData">
            <Size X="69.0000" Y="69.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="43.3920" Y="40.2560" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0452" Y="0.0629" />
            <PreSize X="0.0719" Y="0.1078" />
            <FileData Type="Normal" Path="wenzhouShowHand/res/table/others.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="chat_bubble_1" ActionTag="1098382051" Tag="7803" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="91.2000" RightMargin="773.8200" TopMargin="69.6840" BottomMargin="510.3360" Scale9Enable="True" LeftEage="31" RightEage="31" TopEage="19" BottomEage="19" Scale9OriginX="31" Scale9OriginY="19" Scale9Width="33" Scale9Height="22" ctype="ImageViewObjectData">
            <Size X="94.9800" Y="59.9800" />
            <AnchorPoint />
            <Position X="91.2000" Y="510.3360" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0950" Y="0.7974" />
            <PreSize X="0.0989" Y="0.0937" />
            <FileData Type="Normal" Path="wenzhouShowHand/res/table/chat_bubble.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="chat_bubble_2" ActionTag="905890577" Tag="8319" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="90.8160" RightMargin="774.2040" TopMargin="197.5560" BottomMargin="382.4640" Scale9Enable="True" LeftEage="31" RightEage="31" TopEage="19" BottomEage="19" Scale9OriginX="31" Scale9OriginY="19" Scale9Width="33" Scale9Height="22" ctype="ImageViewObjectData">
            <Size X="94.9800" Y="59.9800" />
            <AnchorPoint />
            <Position X="90.8160" Y="382.4640" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0946" Y="0.5976" />
            <PreSize X="0.0989" Y="0.0937" />
            <FileData Type="Normal" Path="wenzhouShowHand/res/table/chat_bubble.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="chat_bubble_3" ActionTag="1427113714" Tag="8320" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="90.6240" RightMargin="774.3960" TopMargin="322.6760" BottomMargin="257.3440" Scale9Enable="True" LeftEage="31" RightEage="31" TopEage="19" BottomEage="19" Scale9OriginX="31" Scale9OriginY="19" Scale9Width="33" Scale9Height="22" ctype="ImageViewObjectData">
            <Size X="94.9800" Y="59.9800" />
            <AnchorPoint />
            <Position X="90.6240" Y="257.3440" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0944" Y="0.4021" />
            <PreSize X="0.0989" Y="0.0937" />
            <FileData Type="Normal" Path="wenzhouShowHand/res/table/chat_bubble.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="chat_bubble_4" ActionTag="-1281057337" Tag="8321" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="90.6240" RightMargin="774.3960" TopMargin="458.9960" BottomMargin="121.0240" Scale9Enable="True" LeftEage="31" RightEage="31" TopEage="19" BottomEage="19" Scale9OriginX="31" Scale9OriginY="19" Scale9Width="33" Scale9Height="22" ctype="ImageViewObjectData">
            <Size X="94.9800" Y="59.9800" />
            <AnchorPoint />
            <Position X="90.6240" Y="121.0240" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0944" Y="0.1891" />
            <PreSize X="0.0989" Y="0.0937" />
            <FileData Type="Normal" Path="wenzhouShowHand/res/table/chat_bubble.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="chat_bubble_5" ActionTag="-705870781" Tag="8322" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="866.5920" RightMargin="-1.5720" TopMargin="72.1160" BottomMargin="507.9040" FlipX="True" Scale9Enable="True" LeftEage="31" RightEage="31" TopEage="19" BottomEage="19" Scale9OriginX="31" Scale9OriginY="19" Scale9Width="33" Scale9Height="22" ctype="ImageViewObjectData">
            <Size X="94.9800" Y="59.9800" />
            <AnchorPoint />
            <Position X="866.5920" Y="507.9040" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.9027" Y="0.7936" />
            <PreSize X="0.0989" Y="0.0937" />
            <FileData Type="Normal" Path="wenzhouShowHand/res/table/chat_bubble.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="chat_bubble_6" ActionTag="-1446909451" Tag="8323" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="866.4000" RightMargin="-1.3799" TopMargin="197.2360" BottomMargin="382.7840" FlipX="True" Scale9Enable="True" LeftEage="31" RightEage="31" TopEage="19" BottomEage="19" Scale9OriginX="31" Scale9OriginY="19" Scale9Width="33" Scale9Height="22" ctype="ImageViewObjectData">
            <Size X="94.9800" Y="59.9800" />
            <AnchorPoint />
            <Position X="866.4000" Y="382.7840" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.9025" Y="0.5981" />
            <PreSize X="0.0989" Y="0.0937" />
            <FileData Type="Normal" Path="wenzhouShowHand/res/table/chat_bubble.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="chat_bubble_7" ActionTag="110944810" Tag="8324" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="867.1680" RightMargin="-2.1479" TopMargin="324.8520" BottomMargin="255.1680" FlipX="True" Scale9Enable="True" LeftEage="31" RightEage="31" TopEage="19" BottomEage="19" Scale9OriginX="31" Scale9OriginY="19" Scale9Width="33" Scale9Height="22" ctype="ImageViewObjectData">
            <Size X="94.9800" Y="59.9800" />
            <AnchorPoint />
            <Position X="867.1680" Y="255.1680" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.9033" Y="0.3987" />
            <PreSize X="0.0989" Y="0.0937" />
            <FileData Type="Normal" Path="wenzhouShowHand/res/table/chat_bubble.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="chat_bubble_8" ActionTag="2030152543" Tag="8325" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="866.0161" RightMargin="-0.9960" TopMargin="459.3160" BottomMargin="120.7040" FlipX="True" Scale9Enable="True" LeftEage="31" RightEage="31" TopEage="19" BottomEage="19" Scale9OriginX="31" Scale9OriginY="19" Scale9Width="33" Scale9Height="22" ctype="ImageViewObjectData">
            <Size X="94.9800" Y="59.9800" />
            <AnchorPoint />
            <Position X="866.0161" Y="120.7040" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.9021" Y="0.1886" />
            <PreSize X="0.0989" Y="0.0937" />
            <FileData Type="Normal" Path="wenzhouShowHand/res/table/chat_bubble.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="chat_bubble_9" ActionTag="680329012" Tag="3555" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="36.0000" RightMargin="829.0200" TopMargin="509.8760" BottomMargin="70.1440" Scale9Enable="True" LeftEage="31" RightEage="31" TopEage="19" BottomEage="19" Scale9OriginX="31" Scale9OriginY="19" Scale9Width="33" Scale9Height="22" ctype="ImageViewObjectData">
            <Size X="94.9800" Y="59.9800" />
            <AnchorPoint />
            <Position X="36.0000" Y="70.1440" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0375" Y="0.1096" />
            <PreSize X="0.0989" Y="0.0937" />
            <FileData Type="Normal" Path="wenzhouShowHand/res/table/chat_bubble.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="chat_bubble_10" ActionTag="232381908" Tag="3098" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="137.9520" RightMargin="727.0680" TopMargin="487.7960" BottomMargin="92.2240" Scale9Enable="True" LeftEage="31" RightEage="31" TopEage="19" BottomEage="19" Scale9OriginX="31" Scale9OriginY="19" Scale9Width="33" Scale9Height="22" ctype="ImageViewObjectData">
            <Size X="94.9800" Y="59.9800" />
            <AnchorPoint />
            <Position X="137.9520" Y="92.2240" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.1437" Y="0.1441" />
            <PreSize X="0.0989" Y="0.0937" />
            <FileData Type="Normal" Path="wenzhouShowHand/res/table/chat_bubble.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="top_info" ActionTag="-979928734" Tag="2980" IconVisible="True" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="144.0000" RightMargin="816.0000" TopMargin="6.4000" BottomMargin="633.6000" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="144.0000" Y="633.6000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.1500" Y="0.9900" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="game_public/csb/top_info.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="step_txt" ActionTag="-2005167084" Tag="8060" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="388.2925" RightMargin="415.7075" TopMargin="291.8915" BottomMargin="322.1085" FontSize="26" LabelText="游戏即将开始" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="156.0000" Y="26.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="388.2925" Y="335.1085" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="125" G="250" B="255" />
            <PrePosition X="0.4045" Y="0.5236" />
            <PreSize X="0.1625" Y="0.0406" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>