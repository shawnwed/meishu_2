<GameFile>
  <PropertyGroup Name="banker_list_item0" Type="Node" ID="4c6a69ec-5f9f-408e-8b4f-af3de025543c" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="3979" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="bg" ActionTag="-196473247" Tag="3987" IconVisible="False" LeftMargin="-0.0002" RightMargin="-704.9998" TopMargin="-93.6963" BottomMargin="-0.3037" Scale9Enable="True" LeftEage="85" RightEage="85" TopEage="31" BottomEage="31" Scale9OriginX="85" Scale9OriginY="31" Scale9Width="91" Scale9Height="32" ctype="ImageViewObjectData">
            <Size X="705.0000" Y="94.0000" />
            <AnchorPoint />
            <Position X="-0.0002" Y="-0.3037" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="wenzhouShowHand/res/table/bg_other.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="name" ActionTag="22638187" Tag="3983" IconVisible="False" LeftMargin="97.9998" RightMargin="-247.9998" TopMargin="-82.1964" BottomMargin="57.1964" FontSize="25" LabelText="那么多为什么" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="150.0000" Y="25.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="97.9998" Y="69.6964" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="81" G="70" B="69" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="head" ActionTag="-1112918094" Tag="3985" IconVisible="False" LeftMargin="6.9999" RightMargin="-82.9999" TopMargin="-86.6964" BottomMargin="10.6964" LeftEage="25" RightEage="25" TopEage="25" BottomEage="25" Scale9OriginX="25" Scale9OriginY="25" Scale9Width="30" Scale9Height="30" ctype="ImageViewObjectData">
            <Size X="76.0000" Y="76.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="44.9999" Y="48.6964" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="wenzhouShowHand/res/table/seat_avatar.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="frame" ActionTag="-967684453" Tag="3984" IconVisible="False" LeftMargin="2.9999" RightMargin="-86.9999" TopMargin="-90.6968" BottomMargin="6.6968" ctype="SpriteObjectData">
            <Size X="84.0000" Y="84.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="44.9999" Y="48.6968" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="wenzhouShowHand/res/table/seat_avatar_frame.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="banker" ActionTag="-1365625533" Tag="3980" IconVisible="False" LeftMargin="-1.5001" RightMargin="-39.4999" TopMargin="-96.6964" BottomMargin="54.6964" ctype="SpriteObjectData">
            <Size X="41.0000" Y="42.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="18.9999" Y="75.6964" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="wenzhouShowHand/res/table/banker.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="Text_3" ActionTag="860692506" Tag="3988" IconVisible="False" LeftMargin="508.2527" RightMargin="-585.2527" TopMargin="-34.6959" BottomMargin="12.6959" FontSize="22" LabelText="总带入:" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="77.0000" Y="22.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="546.7527" Y="23.6959" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="81" G="70" B="69" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Text_3_0" ActionTag="-1094985277" Tag="3991" IconVisible="False" LeftMargin="413.2527" RightMargin="-512.2527" TopMargin="-82.6961" BottomMargin="60.6961" FontSize="22" LabelText="坐庄局数:" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="99.0000" Y="22.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="462.7527" Y="71.6961" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="81" G="70" B="69" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="total_time" ActionTag="1421729428" Tag="3992" IconVisible="False" LeftMargin="520.4692" RightMargin="-580.4692" TopMargin="-86.6962" BottomMargin="56.6962" FontSize="30" LabelText="20局" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="60.0000" Y="30.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="550.4692" Y="71.6962" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="165" G="42" B="42" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="leave_time" ActionTag="-919302520" Tag="3993" IconVisible="False" LeftMargin="587.5958" RightMargin="-695.5958" TopMargin="-80.6961" BottomMargin="62.6961" FontSize="18" LabelText="(16局后下庄)" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="108.0000" Y="18.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="587.5958" Y="71.6961" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="82" G="52" B="49" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="level" ActionTag="1635408383" Tag="4044" IconVisible="False" LeftMargin="99.9999" RightMargin="-155.9999" TopMargin="-45.1963" BottomMargin="12.1963" ctype="SpriteObjectData">
            <Size X="56.0000" Y="33.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="99.9999" Y="28.6963" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="hall/res/level/level1.png" Plist="hall/res/level.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="Text_3_1" ActionTag="482281158" Tag="631" IconVisible="False" LeftMargin="250.2527" RightMargin="-349.2527" TopMargin="-34.6958" BottomMargin="12.6958" FontSize="22" LabelText="当前输赢:" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="99.0000" Y="22.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="299.7527" Y="23.6958" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="81" G="70" B="69" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="coin_icon" ActionTag="-1222247105" Tag="632" IconVisible="False" LeftMargin="348.1020" RightMargin="-391.1020" TopMargin="-45.1958" BottomMargin="2.1958" ctype="SpriteObjectData">
            <Size X="43.0000" Y="43.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="369.6020" Y="23.6958" />
            <Scale ScaleX="0.5500" ScaleY="0.5500" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="hall/res/first/coin_money.png" Plist="hall/res/first.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="coin_txt" ActionTag="1684827143" Tag="1139" IconVisible="False" LeftMargin="588.0704" RightMargin="-733.0704" TopMargin="-39.7464" BottomMargin="6.7464" LabelText="540.32" ctype="TextBMFontObjectData">
            <Size X="145.0000" Y="33.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="588.0704" Y="23.2464" />
            <Scale ScaleX="0.6100" ScaleY="0.6100" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <LabelBMFontFile_CNB Type="Normal" Path="hall/res/rank_num.fnt" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="win_txt" ActionTag="251255559" Tag="1140" IconVisible="False" LeftMargin="384.8543" RightMargin="-529.8543" TopMargin="-40.3668" BottomMargin="7.3668" LabelText="540.32" ctype="TextBMFontObjectData">
            <Size X="145.0000" Y="33.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="384.8543" Y="23.8668" />
            <Scale ScaleX="0.6100" ScaleY="0.6100" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <LabelBMFontFile_CNB Type="Normal" Path="hall/res/rank_num.fnt" Plist="" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>