<GameFile>
  <PropertyGroup Name="player_self" Type="Node" ID="da7123eb-83bf-4051-a30d-2f68a4c93891" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="188" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="win" ActionTag="1223193734" Tag="1480" IconVisible="False" LeftMargin="-48.5000" RightMargin="-48.5000" TopMargin="-48.5000" BottomMargin="-48.5000" ctype="SpriteObjectData">
            <Size X="97.0000" Y="97.0000" />
            <Children>
              <AbstractNodeData Name="light" ActionTag="1064377946" Tag="1481" IconVisible="False" LeftMargin="-26.9742" RightMargin="-26.0258" TopMargin="-30.8873" BottomMargin="-22.1127" ctype="SpriteObjectData">
                <Size X="150.0000" Y="150.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="48.0258" Y="52.8873" />
                <Scale ScaleX="1.1391" ScaleY="1.1391" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4951" Y="0.5452" />
                <PreSize X="1.5464" Y="1.5464" />
                <FileData Type="Normal" Path="wenzhouShowHand/res/table/win_light.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="0.7667" ScaleY="0.7667" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="wenzhouShowHand/res/table/win_frame.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="head" ActionTag="-2095433036" Tag="189" IconVisible="False" LeftMargin="-38.0000" RightMargin="-38.0000" TopMargin="-38.0000" BottomMargin="-38.0000" LeftEage="25" RightEage="25" TopEage="25" BottomEage="25" Scale9OriginX="25" Scale9OriginY="25" Scale9Width="30" Scale9Height="30" ctype="ImageViewObjectData">
            <Size X="76.0000" Y="76.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="wenzhouShowHand/res/table/seat_avatar.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="coin_icon" ActionTag="1939168953" Tag="192" IconVisible="False" LeftMargin="-54.2337" RightMargin="-57.7663" TopMargin="18.8197" BottomMargin="-64.8197" ctype="SpriteObjectData">
            <Size X="112.0000" Y="46.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1.7663" Y="-41.8197" />
            <Scale ScaleX="1.0205" ScaleY="1.0205" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="wenzhouShowHand/res/table/card_type_bg.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="frame" ActionTag="1445185761" Tag="190" IconVisible="False" LeftMargin="-42.0000" RightMargin="-42.0000" TopMargin="-42.0000" BottomMargin="-42.0000" ctype="SpriteObjectData">
            <Size X="84.0000" Y="84.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="wenzhouShowHand/res/table/seat_avatar_frame.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="name" ActionTag="2057378791" Tag="191" IconVisible="False" LeftMargin="-43.6389" RightMargin="-37.3611" TopMargin="21.1985" BottomMargin="-39.1985" FontSize="18" LabelText="your name" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="81.0000" Y="18.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="-43.6389" Y="-30.1985" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="coin_txt" ActionTag="504418396" Tag="203" IconVisible="False" LeftMargin="-34.6052" RightMargin="-35.3948" TopMargin="43.1668" BottomMargin="-63.1668" FontSize="20" LabelText="9999999" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="70.0000" Y="20.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="-34.6052" Y="-53.1668" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="245" G="206" B="80" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>