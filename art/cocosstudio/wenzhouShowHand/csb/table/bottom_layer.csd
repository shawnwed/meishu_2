<GameFile>
  <PropertyGroup Name="bottom_layer" Type="Layer" ID="1c5619e7-ad72-40e2-8c3c-e57a746bbced" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="769" ctype="GameLayerObjectData">
        <Size X="1280.0000" Y="120.0000" />
        <Children>
          <AbstractNodeData Name="bet_btn_6" ActionTag="-557690163" Tag="770" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="1120.2166" RightMargin="48.7834" TopMargin="4.9981" BottomMargin="1.0019" ctype="SpriteObjectData">
            <Size X="111.0000" Y="114.0000" />
            <Children>
              <AbstractNodeData Name="frame" ActionTag="901908691" Tag="771" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-7.5000" RightMargin="-7.5000" TopMargin="-9.1546" BottomMargin="-4.8454" ctype="SpriteObjectData">
                <Size X="126.0000" Y="128.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="55.5000" Y="59.1546" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5189" />
                <PreSize X="1.1351" Y="1.1228" />
                <FileData Type="Normal" Path="wenzhouShowHand/res/table/bet_frame.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="bet_txt" ActionTag="-906201974" Tag="1092" IconVisible="False" LeftMargin="25.3662" RightMargin="26.6338" TopMargin="41.1405" BottomMargin="39.8595" LabelText="1234" ctype="TextBMFontObjectData">
                <Size X="59.0000" Y="33.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="54.8662" Y="56.3595" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="148" G="19" B="25" />
                <PrePosition X="0.4943" Y="0.4944" />
                <PreSize X="0.5315" Y="0.2895" />
                <LabelBMFontFile_CNB Type="Normal" Path="hall/res/chips_num.fnt" Plist="" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1175.7166" Y="58.0019" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.9185" Y="0.4833" />
            <PreSize X="0.0867" Y="0.9500" />
            <FileData Type="MarkedSubImage" Path="wenzhouShowHand/res/table/btn_bet_6.png" Plist="wenzhouShowHand/res/table.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="bet_btn_5" ActionTag="-996644681" Tag="773" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="966.3605" RightMargin="202.6395" TopMargin="4.9981" BottomMargin="1.0019" ctype="SpriteObjectData">
            <Size X="111.0000" Y="114.0000" />
            <Children>
              <AbstractNodeData Name="frame" ActionTag="926678375" Tag="774" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-7.5000" RightMargin="-7.5000" TopMargin="-9.1546" BottomMargin="-4.8454" ctype="SpriteObjectData">
                <Size X="126.0000" Y="128.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="55.5000" Y="59.1546" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5189" />
                <PreSize X="1.1351" Y="1.1228" />
                <FileData Type="Normal" Path="wenzhouShowHand/res/table/bet_frame.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="bet_txt" ActionTag="723330640" Tag="1091" IconVisible="False" LeftMargin="25.3662" RightMargin="26.6338" TopMargin="41.1405" BottomMargin="39.8595" LabelText="1234" ctype="TextBMFontObjectData">
                <Size X="59.0000" Y="33.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="54.8662" Y="56.3595" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="84" G="46" B="1" />
                <PrePosition X="0.4943" Y="0.4944" />
                <PreSize X="0.5315" Y="0.2895" />
                <LabelBMFontFile_CNB Type="Normal" Path="hall/res/chips_num.fnt" Plist="" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1021.8605" Y="58.0019" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.7983" Y="0.4833" />
            <PreSize X="0.0867" Y="0.9500" />
            <FileData Type="MarkedSubImage" Path="wenzhouShowHand/res/table/btn_bet_5.png" Plist="wenzhouShowHand/res/table.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="bet_btn_4" ActionTag="-1881913253" Tag="776" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="812.3760" RightMargin="356.6240" TopMargin="5.4981" BottomMargin="1.5019" ctype="SpriteObjectData">
            <Size X="111.0000" Y="113.0000" />
            <Children>
              <AbstractNodeData Name="frame" ActionTag="648905251" Tag="777" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-7.5000" RightMargin="-7.5000" TopMargin="-9.6357" BottomMargin="-5.3643" ctype="SpriteObjectData">
                <Size X="126.0000" Y="128.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="55.5000" Y="58.6357" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5189" />
                <PreSize X="1.1351" Y="1.1327" />
                <FileData Type="Normal" Path="wenzhouShowHand/res/table/bet_frame.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="bet_txt" ActionTag="-550005487" Tag="1090" IconVisible="False" LeftMargin="25.3662" RightMargin="26.6338" TopMargin="40.1405" BottomMargin="39.8595" LabelText="1234" ctype="TextBMFontObjectData">
                <Size X="59.0000" Y="33.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="54.8662" Y="56.3595" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="85" G="15" B="170" />
                <PrePosition X="0.4943" Y="0.4988" />
                <PreSize X="0.5315" Y="0.2920" />
                <LabelBMFontFile_CNB Type="Normal" Path="hall/res/chips_num.fnt" Plist="" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="867.8760" Y="58.0019" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.6780" Y="0.4833" />
            <PreSize X="0.0867" Y="0.9417" />
            <FileData Type="MarkedSubImage" Path="wenzhouShowHand/res/table/btn_bet_4.png" Plist="wenzhouShowHand/res/table.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="bet_btn_3" ActionTag="794971470" Tag="779" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="658.5201" RightMargin="510.4799" TopMargin="4.9981" BottomMargin="1.0019" ctype="SpriteObjectData">
            <Size X="111.0000" Y="114.0000" />
            <Children>
              <AbstractNodeData Name="frame" ActionTag="383295022" Tag="780" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-7.5000" RightMargin="-7.5000" TopMargin="-9.1546" BottomMargin="-4.8454" ctype="SpriteObjectData">
                <Size X="126.0000" Y="128.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="55.5000" Y="59.1546" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5189" />
                <PreSize X="1.1351" Y="1.1228" />
                <FileData Type="Normal" Path="wenzhouShowHand/res/table/bet_frame.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="bet_txt" ActionTag="555035798" Tag="1088" IconVisible="False" LeftMargin="25.3662" RightMargin="26.6338" TopMargin="41.1405" BottomMargin="39.8595" LabelText="1234" ctype="TextBMFontObjectData">
                <Size X="59.0000" Y="33.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="54.8662" Y="56.3595" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="12" G="46" B="133" />
                <PrePosition X="0.4943" Y="0.4944" />
                <PreSize X="0.5315" Y="0.2895" />
                <LabelBMFontFile_CNB Type="Normal" Path="hall/res/chips_num.fnt" Plist="" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="714.0201" Y="58.0019" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5578" Y="0.4833" />
            <PreSize X="0.0867" Y="0.9500" />
            <FileData Type="MarkedSubImage" Path="wenzhouShowHand/res/table/btn_bet_3.png" Plist="wenzhouShowHand/res/table.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="bet_btn_2" ActionTag="-1429507265" Tag="782" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="504.6644" RightMargin="664.3356" TopMargin="5.4981" BottomMargin="1.5019" ctype="SpriteObjectData">
            <Size X="111.0000" Y="113.0000" />
            <Children>
              <AbstractNodeData Name="frame" ActionTag="-575369891" Tag="783" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-7.5000" RightMargin="-7.5000" TopMargin="-9.6357" BottomMargin="-5.3643" ctype="SpriteObjectData">
                <Size X="126.0000" Y="128.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="55.5000" Y="58.6357" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5189" />
                <PreSize X="1.1351" Y="1.1327" />
                <FileData Type="Normal" Path="wenzhouShowHand/res/table/bet_frame.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="bet_txt" ActionTag="-699468413" Tag="1089" IconVisible="False" LeftMargin="25.3662" RightMargin="26.6338" TopMargin="40.1405" BottomMargin="39.8595" LabelText="1234" ctype="TextBMFontObjectData">
                <Size X="59.0000" Y="33.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="54.8662" Y="56.3595" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="38" G="89" B="0" />
                <PrePosition X="0.4943" Y="0.4988" />
                <PreSize X="0.5315" Y="0.2920" />
                <LabelBMFontFile_CNB Type="Normal" Path="hall/res/chips_num.fnt" Plist="" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="560.1644" Y="58.0019" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4376" Y="0.4833" />
            <PreSize X="0.0867" Y="0.9417" />
            <FileData Type="MarkedSubImage" Path="wenzhouShowHand/res/table/btn_bet_2.png" Plist="wenzhouShowHand/res/table.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="bet_btn_1" ActionTag="-1192440428" Tag="785" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="350.8078" RightMargin="818.1922" TopMargin="5.9981" BottomMargin="2.0019" ctype="SpriteObjectData">
            <Size X="111.0000" Y="112.0000" />
            <Children>
              <AbstractNodeData Name="frame" ActionTag="875732160" Tag="786" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-7.5000" RightMargin="-7.5000" TopMargin="-10.1168" BottomMargin="-5.8832" ctype="SpriteObjectData">
                <Size X="126.0000" Y="128.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="55.5000" Y="58.1168" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5189" />
                <PreSize X="1.1351" Y="1.1429" />
                <FileData Type="Normal" Path="wenzhouShowHand/res/table/bet_frame.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="bet_txt" ActionTag="1625889792" Tag="1087" IconVisible="False" LeftMargin="25.3662" RightMargin="26.6338" TopMargin="40.1405" BottomMargin="38.8595" LabelText="1234" ctype="TextBMFontObjectData">
                <Size X="59.0000" Y="33.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="54.8662" Y="55.3595" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="0" G="89" B="55" />
                <PrePosition X="0.4943" Y="0.4943" />
                <PreSize X="0.5315" Y="0.2946" />
                <LabelBMFontFile_CNB Type="Normal" Path="hall/res/chips_num.fnt" Plist="" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="406.3078" Y="58.0019" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3174" Y="0.4833" />
            <PreSize X="0.0867" Y="0.9333" />
            <FileData Type="MarkedSubImage" Path="wenzhouShowHand/res/table/btn_bet_1.png" Plist="wenzhouShowHand/res/table.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="head" ActionTag="2048285484" Tag="972" IconVisible="True" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="119.1680" RightMargin="1160.8320" TopMargin="50.7120" BottomMargin="69.2880" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="119.1680" Y="69.2880" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0931" Y="0.5774" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="wenzhouShowHand/csb/table/player_self.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="banker_info" CanEdit="False" Visible="False" ActionTag="-1714644104" Tag="1393" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="386.0480" RightMargin="243.9520" TopMargin="20.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="0" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="650.0000" Y="100.0000" />
            <Children>
              <AbstractNodeData Name="win_txt" ActionTag="579303459" Tag="1394" IconVisible="False" LeftMargin="304.6052" RightMargin="268.3948" TopMargin="69.3042" BottomMargin="8.6958" FontSize="22" LabelText="9999999" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="77.0000" Y="22.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="304.6052" Y="19.6958" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="227" B="55" />
                <PrePosition X="0.4686" Y="0.1970" />
                <PreSize X="0.1185" Y="0.2200" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="coin_icon" ActionTag="-2061849210" Tag="1395" IconVisible="False" LeftMargin="261.1020" RightMargin="345.8980" TopMargin="58.8042" BottomMargin="-1.8042" ctype="SpriteObjectData">
                <Size X="43.0000" Y="43.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="282.6020" Y="19.6958" />
                <Scale ScaleX="0.5500" ScaleY="0.5500" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4348" Y="0.1970" />
                <PreSize X="0.0662" Y="0.4300" />
                <FileData Type="MarkedSubImage" Path="hall/res/first/coin_money.png" Plist="hall/res/first.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_3_1" ActionTag="914962602" Tag="1396" IconVisible="False" LeftMargin="163.2527" RightMargin="387.7473" TopMargin="69.3042" BottomMargin="8.6958" FontSize="22" LabelText="当前输赢:" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="99.0000" Y="22.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="212.7527" Y="19.6958" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="56" G="211" B="211" />
                <PrePosition X="0.3273" Y="0.1970" />
                <PreSize X="0.1523" Y="0.2200" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="leave_time" ActionTag="1814582289" Tag="1397" IconVisible="False" LeftMargin="506.5958" RightMargin="35.4042" TopMargin="23.3039" BottomMargin="58.6961" FontSize="18" LabelText="(16局后下庄)" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="108.0000" Y="18.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="506.5958" Y="67.6961" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="56" G="211" B="211" />
                <PrePosition X="0.7794" Y="0.6770" />
                <PreSize X="0.1662" Y="0.1800" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="total_time" ActionTag="211486801" Tag="1398" IconVisible="False" LeftMargin="433.4692" RightMargin="156.5308" TopMargin="17.3038" BottomMargin="52.6962" FontSize="30" LabelText="20局" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="60.0000" Y="30.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="463.4692" Y="67.6962" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="227" B="55" />
                <PrePosition X="0.7130" Y="0.6770" />
                <PreSize X="0.0923" Y="0.3000" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_3_0" ActionTag="1737836874" Tag="1399" IconVisible="False" LeftMargin="326.2527" RightMargin="224.7473" TopMargin="21.3039" BottomMargin="56.6961" FontSize="22" LabelText="坐庄局数:" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="99.0000" Y="22.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="375.7527" Y="67.6961" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="56" G="211" B="211" />
                <PrePosition X="0.5781" Y="0.6770" />
                <PreSize X="0.1523" Y="0.2200" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="coin_txt" ActionTag="786023650" Tag="1400" IconVisible="False" LeftMargin="524.6052" RightMargin="48.3948" TopMargin="69.3042" BottomMargin="8.6958" FontSize="22" LabelText="9999999" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="77.0000" Y="22.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="524.6052" Y="19.6958" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="227" B="55" />
                <PrePosition X="0.8071" Y="0.1970" />
                <PreSize X="0.1185" Y="0.2200" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_3" ActionTag="-1029409076" Tag="1401" IconVisible="False" LeftMargin="443.2527" RightMargin="129.7473" TopMargin="69.3042" BottomMargin="8.6958" FontSize="22" LabelText="总带入:" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="77.0000" Y="22.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="481.7527" Y="19.6958" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="56" G="211" B="211" />
                <PrePosition X="0.7412" Y="0.1970" />
                <PreSize X="0.1185" Y="0.2200" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position X="386.0480" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3016" />
            <PreSize X="0.5078" Y="0.8333" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>