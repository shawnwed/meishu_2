<GameFile>
  <PropertyGroup Name="clock" Type="Node" ID="caa55402-c370-4fae-8749-56365b759269" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="7848" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="clock" ActionTag="683253353" Tag="7853" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-43.5000" RightMargin="-43.5000" TopMargin="-45.5000" BottomMargin="-45.5000" ctype="SpriteObjectData">
            <Size X="87.0000" Y="91.0000" />
            <Children>
              <AbstractNodeData Name="time_txt" ActionTag="-752360789" Tag="7858" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="23.9955" RightMargin="25.0045" TopMargin="30.9996" BottomMargin="32.0004" CharWidth="19" CharHeight="28" LabelText="15" StartChar="." ctype="TextAtlasObjectData">
                <Size X="38.0000" Y="28.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="42.9955" Y="46.0004" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4942" Y="0.5055" />
                <PreSize X="0.4368" Y="0.3077" />
                <LabelAtlasFileImage_CNB Type="Normal" Path="wenzhouShowHand/res/table/number_clock.png" Plist="" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="wenzhouShowHand/res/table/clock.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>