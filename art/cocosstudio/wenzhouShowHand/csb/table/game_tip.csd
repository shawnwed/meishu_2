<GameFile>
  <PropertyGroup Name="game_tip" Type="Node" ID="e66f7dd1-5614-4058-948c-7ba50bcb9078" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="1981" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="bg" ActionTag="1474553138" Tag="1983" IconVisible="False" LeftMargin="-397.5000" RightMargin="-397.5000" TopMargin="-95.5000" BottomMargin="-95.5000" ctype="SpriteObjectData">
            <Size X="795.0000" Y="191.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="wenzhouShowHand/res/table/game_tip_bg.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="tip" ActionTag="1754777487" Tag="1984" IconVisible="False" LeftMargin="-139.0000" RightMargin="-139.0000" TopMargin="-35.5000" BottomMargin="-35.5000" ctype="SpriteObjectData">
            <Size X="278.0000" Y="71.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="wenzhouShowHand/res/table/word_start_bet.png" Plist="wenzhouShowHand/res/table.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>