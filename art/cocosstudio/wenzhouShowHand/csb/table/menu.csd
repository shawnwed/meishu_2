<GameFile>
  <PropertyGroup Name="menu" Type="Node" ID="e19e9da4-b3b5-419c-bc23-58c55e441ba1" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="391" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="bg" ActionTag="-1504021837" Tag="411" IconVisible="False" RightMargin="-273.0000" BottomMargin="-720.0000" TouchEnable="True" LeftEage="87" RightEage="87" TopEage="117" BottomEage="117" Scale9OriginX="87" Scale9OriginY="117" Scale9Width="99" Scale9Height="486" ctype="ImageViewObjectData">
            <Size X="273.0000" Y="720.0000" />
            <AnchorPoint ScaleY="1.0000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="wenzhouShowHand/res/table/menu_bg.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="prev_score" ActionTag="1340314479" Tag="493" IconVisible="False" LeftMargin="8.5000" RightMargin="-255.5000" TopMargin="432.9144" BottomMargin="-514.9144" ctype="SpriteObjectData">
            <Size X="247.0000" Y="82.0000" />
            <Children>
              <AbstractNodeData Name="title" ActionTag="-1236398715" Tag="494" IconVisible="False" LeftMargin="75.5000" RightMargin="75.5000" TopMargin="-11.9877" BottomMargin="69.9877" FontSize="24" LabelText="上局输赢" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="96.0000" Y="24.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="123.5000" Y="81.9877" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="162" G="145" B="91" />
                <PrePosition X="0.5000" Y="0.9999" />
                <PreSize X="0.3887" Y="0.2927" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="score" ActionTag="1041582302" Tag="496" IconVisible="False" LeftMargin="60.5000" RightMargin="60.5000" TopMargin="24.0000" BottomMargin="22.0000" FontSize="36" LabelText="-300000" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="126.0000" Y="36.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="123.5000" Y="40.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="48" G="164" B="254" />
                <PrePosition X="0.5000" Y="0.4878" />
                <PreSize X="0.5101" Y="0.4390" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="132.0000" Y="-473.9144" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="wenzhouShowHand/res/table/menu_line2.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="room_score" ActionTag="-1970230254" Tag="497" IconVisible="False" LeftMargin="8.5000" RightMargin="-255.5000" TopMargin="532.2390" BottomMargin="-614.2390" ctype="SpriteObjectData">
            <Size X="247.0000" Y="82.0000" />
            <Children>
              <AbstractNodeData Name="title" ActionTag="-66316042" Tag="498" IconVisible="False" LeftMargin="75.5000" RightMargin="75.5000" TopMargin="-11.9877" BottomMargin="69.9877" FontSize="24" LabelText="本房输赢" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="96.0000" Y="24.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="123.5000" Y="81.9877" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="162" G="145" B="91" />
                <PrePosition X="0.5000" Y="0.9999" />
                <PreSize X="0.3887" Y="0.2927" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="score" ActionTag="-264481755" Tag="499" IconVisible="False" LeftMargin="60.5000" RightMargin="60.5000" TopMargin="24.0000" BottomMargin="22.0000" FontSize="36" LabelText="-300000" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="126.0000" Y="36.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="123.5000" Y="40.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="48" G="164" B="254" />
                <PrePosition X="0.5000" Y="0.4878" />
                <PreSize X="0.5101" Y="0.4390" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="132.0000" Y="-573.2390" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="wenzhouShowHand/res/table/menu_line2.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="day_score" ActionTag="279304464" Tag="500" IconVisible="False" LeftMargin="8.5000" RightMargin="-255.5000" TopMargin="631.5635" BottomMargin="-713.5635" ctype="SpriteObjectData">
            <Size X="247.0000" Y="82.0000" />
            <Children>
              <AbstractNodeData Name="title" ActionTag="1643851908" Tag="501" IconVisible="False" LeftMargin="75.5000" RightMargin="75.5000" TopMargin="-11.9877" BottomMargin="69.9877" FontSize="24" LabelText="今日输赢" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="96.0000" Y="24.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="123.5000" Y="81.9877" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="162" G="145" B="91" />
                <PrePosition X="0.5000" Y="0.9999" />
                <PreSize X="0.3887" Y="0.2927" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="score" ActionTag="-1208656321" Tag="502" IconVisible="False" LeftMargin="60.5000" RightMargin="60.5000" TopMargin="24.0000" BottomMargin="22.0000" FontSize="36" LabelText="-300000" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="126.0000" Y="36.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="123.5000" Y="40.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="48" G="164" B="254" />
                <PrePosition X="0.5000" Y="0.4878" />
                <PreSize X="0.5101" Y="0.4390" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="132.0000" Y="-672.5635" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="wenzhouShowHand/res/table/menu_line2.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="bank_btn" ActionTag="799513440" VisibleForFrame="False" Tag="180" IconVisible="False" LeftMargin="57.0000" RightMargin="-235.0000" TopMargin="130.3642" BottomMargin="-181.3642" ctype="SpriteObjectData">
            <Size X="178.0000" Y="51.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="146.0000" Y="-155.8642" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="wenzhouShowHand/res/table/menu_bank.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="stand_btn" ActionTag="1189497855" Tag="181" IconVisible="False" LeftMargin="57.0001" RightMargin="-211.0001" TopMargin="229.0318" BottomMargin="-278.0318" ctype="SpriteObjectData">
            <Size X="154.0000" Y="49.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="134.0001" Y="-253.5318" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="wenzhouShowHand/res/table/menu_stand.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="setting_btn" ActionTag="-920855420" Tag="182" IconVisible="False" LeftMargin="57.5001" RightMargin="-212.5001" TopMargin="129.1964" BottomMargin="-181.1964" ctype="SpriteObjectData">
            <Size X="155.0000" Y="52.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="135.0001" Y="-155.1964" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="wenzhouShowHand/res/table/menu_setting.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="back_btn" ActionTag="-745683953" Tag="183" IconVisible="False" LeftMargin="49.5000" RightMargin="-208.5000" TopMargin="28.5309" BottomMargin="-80.5309" ctype="SpriteObjectData">
            <Size X="159.0000" Y="52.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="129.0000" Y="-54.5309" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="wenzhouShowHand/res/table/menu_quit.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="line_0" ActionTag="-99876612" Tag="184" IconVisible="False" LeftMargin="2.5000" RightMargin="-261.5000" TopMargin="199.6667" BottomMargin="-201.6667" ctype="SpriteObjectData">
            <Size X="259.0000" Y="2.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="132.0000" Y="-200.6667" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="wenzhouShowHand/res/table/menu_line1.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="line_1" ActionTag="35304860" Tag="185" IconVisible="False" LeftMargin="2.5000" RightMargin="-261.5000" TopMargin="102.0000" BottomMargin="-104.0000" ctype="SpriteObjectData">
            <Size X="259.0000" Y="2.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="132.0000" Y="-103.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="wenzhouShowHand/res/table/menu_line1.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="line_2" ActionTag="-987452778" VisibleForFrame="False" Tag="186" IconVisible="False" LeftMargin="2.5000" RightMargin="-261.5000" TopMargin="297.3333" BottomMargin="-299.3333" ctype="SpriteObjectData">
            <Size X="259.0000" Y="2.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="132.0000" Y="-298.3333" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="wenzhouShowHand/res/table/menu_line1.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="menu_close" ActionTag="-1317107367" Tag="45" IconVisible="False" LeftMargin="268.0067" RightMargin="-318.0067" TopMargin="305.0000" BottomMargin="-395.0000" ctype="SpriteObjectData">
            <Size X="50.0000" Y="90.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="293.0067" Y="-350.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="wenzhouShowHand/res/table/menu_close.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>