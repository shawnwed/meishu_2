<GameFile>
  <PropertyGroup Name="loading_dialog2" Type="Scene" ID="83de287e-71b9-4e27-b956-5fefa7547755" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Scene" Tag="235" ctype="GameNodeObjectData">
        <Size X="960.0000" Y="640.0000" />
        <Children>
          <AbstractNodeData Name="Panel_1" ActionTag="1981040593" Tag="20" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="960.0000" Y="640.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="480.0000" Y="320.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="1.0000" Y="1.0000" />
            <SingleColor A="255" R="0" G="0" B="0" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="icon" ActionTag="2116323122" Tag="167" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="457.0000" RightMargin="457.0000" TopMargin="304.5840" BottomMargin="281.4160" ctype="SpriteObjectData">
            <Size X="46.0000" Y="54.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="480.0000" Y="308.4160" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.4819" />
            <PreSize X="0.0479" Y="0.0844" />
            <FileData Type="MarkedSubImage" Path="login/loading_icon.png" Plist="login/login.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="circle" ActionTag="985905097" Tag="4201" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="408.0000" RightMargin="408.0000" TopMargin="256.4480" BottomMargin="239.5520" ctype="SpriteObjectData">
            <Size X="144.0000" Y="144.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="480.0000" Y="311.5520" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.4868" />
            <PreSize X="0.1500" Y="0.2250" />
            <FileData Type="MarkedSubImage" Path="login/loading_circle.png" Plist="login/login.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="text" ActionTag="1495662579" Tag="4202" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="408.5000" RightMargin="408.5000" TopMargin="418.0400" BottomMargin="195.9600" ctype="SpriteObjectData">
            <Size X="143.0000" Y="26.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="480.0000" Y="208.9600" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.3265" />
            <PreSize X="0.1490" Y="0.0406" />
            <FileData Type="MarkedSubImage" Path="lan/cn/loadint_text.png" Plist="lan/cn/game.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="p1" ActionTag="-1216361839" Tag="4203" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="556.6035" RightMargin="393.3965" TopMargin="419.7386" BottomMargin="194.2614" ctype="SpriteObjectData">
            <Size X="10.0000" Y="26.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="561.6035" Y="207.2614" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5850" Y="0.3238" />
            <PreSize X="0.0104" Y="0.0406" />
            <FileData Type="MarkedSubImage" Path="login/loading_point.png" Plist="login/login.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="p2" ActionTag="-438753900" Tag="4204" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="566.6068" RightMargin="383.3932" TopMargin="419.7386" BottomMargin="194.2614" ctype="SpriteObjectData">
            <Size X="10.0000" Y="26.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="571.6068" Y="207.2614" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5954" Y="0.3238" />
            <PreSize X="0.0104" Y="0.0406" />
            <FileData Type="MarkedSubImage" Path="login/loading_point.png" Plist="login/login.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="p3" ActionTag="1345221101" Tag="4205" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="576.6057" RightMargin="373.3943" TopMargin="419.7386" BottomMargin="194.2614" ctype="SpriteObjectData">
            <Size X="10.0000" Y="26.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="581.6057" Y="207.2614" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.6058" Y="0.3238" />
            <PreSize X="0.0104" Y="0.0406" />
            <FileData Type="MarkedSubImage" Path="login/loading_point.png" Plist="login/login.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>