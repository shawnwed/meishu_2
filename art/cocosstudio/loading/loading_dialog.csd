<GameFile>
  <PropertyGroup Name="loading_dialog" Type="Scene" ID="0b9f0557-49f3-41a9-b7c1-d4db2a8c1b3a" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="60" Speed="1.0000">
        <Timeline ActionTag="1679768486" Property="Position">
          <PointFrame FrameIndex="0" X="219.5003" Y="122.9963">
            <EasingData Type="0" />
          </PointFrame>
        </Timeline>
        <Timeline ActionTag="1679768486" Property="Scale">
          <ScaleFrame FrameIndex="0" X="0.8000" Y="0.8000">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="1679768486" Property="RotationSkew">
          <ScaleFrame FrameIndex="0" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="60" X="360.0000" Y="360.0000">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
      </Animation>
      <ObjectData Name="Scene" Tag="235" ctype="GameNodeObjectData">
        <Size X="960.0000" Y="640.0000" />
        <Children>
          <AbstractNodeData Name="content" ActionTag="-1014074266" Tag="236" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="260.5000" RightMargin="260.5000" TopMargin="227.5000" BottomMargin="227.5000" ctype="SpriteObjectData">
            <Size X="439.0000" Y="185.0000" />
            <Children>
              <AbstractNodeData Name="loading_2" ActionTag="1679768486" Tag="237" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="164.5003" RightMargin="164.4997" TopMargin="7.0037" BottomMargin="67.9963" ctype="SpriteObjectData">
                <Size X="110.0000" Y="110.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="219.5003" Y="122.9963" />
                <Scale ScaleX="0.8000" ScaleY="0.8000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.6648" />
                <PreSize X="0.2506" Y="0.5946" />
                <FileData Type="MarkedSubImage" Path="login/loading.png" Plist="login/login.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="close" ActionTag="-76008523" VisibleForFrame="False" Tag="238" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="940.2452" RightMargin="-581.2452" TopMargin="29.6961" BottomMargin="74.3039" ctype="SpriteObjectData">
                <Size X="80.0000" Y="81.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="980.2452" Y="114.8039" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="2.2329" Y="0.6206" />
                <PreSize X="0.1822" Y="0.4378" />
                <FileData Type="Normal" Path="registersdk/x.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="txt" ActionTag="-930193821" Tag="239" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="115.5000" RightMargin="115.5000" TopMargin="124.4986" BottomMargin="27.5014" FontSize="32" LabelText="正在请求中..." ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="208.0000" Y="33.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="219.5000" Y="44.0014" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.2378" />
                <PreSize X="0.4738" Y="0.1784" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="480.0000" Y="320.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="0.4573" Y="0.2891" />
            <FileData Type="MarkedSubImage" Path="login/loading bg.png" Plist="login/login.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>