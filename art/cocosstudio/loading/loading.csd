<GameFile>
  <PropertyGroup Name="loading" Type="Scene" ID="b6047892-6500-4cb1-bbab-c04b7024313b" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Scene" Tag="3" ctype="GameNodeObjectData">
        <Size X="960.0000" Y="640.0000" />
        <Children>
          <AbstractNodeData Name="bg" ActionTag="-1564113959" Tag="5" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" LeftEage="316" RightEage="316" TopEage="211" BottomEage="211" Scale9OriginX="316" Scale9OriginY="211" Scale9Width="648" Scale9Height="298" ctype="ImageViewObjectData">
            <Size X="960.0000" Y="640.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="480.0000" Y="320.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="1.0000" Y="1.0000" />
            <FileData Type="Normal" Path="bg/login_bg.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="loadingbarBg" ActionTag="-374442718" Tag="21" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="62.7040" RightMargin="53.2960" TopMargin="505.1520" BottomMargin="102.8480" ctype="SpriteObjectData">
            <Size X="844.0000" Y="32.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="484.7040" Y="118.8480" />
            <Scale ScaleX="1.0000" ScaleY="1.2000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5049" Y="0.1857" />
            <PreSize X="0.8792" Y="0.0500" />
            <FileData Type="MarkedSubImage" Path="login/loadingbarBg.png" Plist="login/login.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="loadingbarPro" ActionTag="1870639207" Tag="20" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="61.9360" RightMargin="54.0640" TopMargin="505.1520" BottomMargin="102.8480" ProgressInfo="100" ctype="LoadingBarObjectData">
            <Size X="844.0000" Y="32.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="483.9360" Y="118.8480" />
            <Scale ScaleX="1.0000" ScaleY="1.2000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5041" Y="0.1857" />
            <PreSize X="0.8792" Y="0.0500" />
            <ImageFileData Type="MarkedSubImage" Path="login/loadingbarPro.png" Plist="login/login.plist" />
          </AbstractNodeData>
          <AbstractNodeData Name="loadingProText" ActionTag="-2123769968" VisibleForFrame="False" Tag="23" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="713.8080" RightMargin="210.1920" TopMargin="509.1520" BottomMargin="106.8480" FontSize="24" LabelText="5/6" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="36.0000" Y="24.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="731.8080" Y="118.8480" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.7623" Y="0.1857" />
            <PreSize X="0.0375" Y="0.0375" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="loading_text" ActionTag="605669446" Tag="1195" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="362.7040" RightMargin="353.2960" TopMargin="506.2800" BottomMargin="103.7200" ctype="SpriteObjectData">
            <Size X="244.0000" Y="30.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="484.7040" Y="118.7200" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5049" Y="0.1855" />
            <PreSize X="0.2542" Y="0.0469" />
            <FileData Type="MarkedSubImage" Path="lan/cn/loading_text.png" Plist="lan/cn/game.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="logo_1" ActionTag="962305704" VisibleForFrame="False" Tag="18" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="158.0000" RightMargin="158.0000" TopMargin="-3.5000" BottomMargin="188.5000" ctype="SpriteObjectData">
            <Size X="644.0000" Y="455.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="480.0000" Y="416.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.6500" />
            <PreSize X="0.6708" Y="0.7109" />
            <FileData Type="Normal" Path="login/logo.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>