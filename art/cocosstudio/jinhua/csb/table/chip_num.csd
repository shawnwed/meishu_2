<GameFile>
  <PropertyGroup Name="chip_num" Type="Node" ID="df57a075-6a27-4dc5-bc69-8367c71ad041" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="1502" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="Image_1" ActionTag="2073612147" Tag="1027" IconVisible="False" LeftMargin="-55.0004" RightMargin="-54.9996" TopMargin="-16.0000" BottomMargin="-16.0000" Scale9Enable="True" LeftEage="44" RightEage="44" TopEage="10" BottomEage="10" Scale9OriginX="44" Scale9OriginY="10" Scale9Width="34" Scale9Height="13" ctype="ImageViewObjectData">
            <Size X="110.0000" Y="32.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="-55.0004" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="jinhua/res/table/coin_bg.png" Plist="jinhua/res/table.plist" />
          </AbstractNodeData>
          <AbstractNodeData Name="txt" ActionTag="929907972" Tag="1504" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-30.0000" RightMargin="-30.0000" TopMargin="-10.0000" BottomMargin="-10.0000" FontSize="20" LabelText="1000万" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="60.0000" Y="20.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>