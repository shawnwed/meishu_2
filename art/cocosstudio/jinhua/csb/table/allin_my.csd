<GameFile>
  <PropertyGroup Name="allin_my" Type="Layer" ID="8f68a022-7f0b-4638-9a3d-c55179e9bfcb" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="20" Speed="1.0000">
        <Timeline ActionTag="251470575" Property="FileData">
          <TextureFrame FrameIndex="0" Tween="False" />
          <TextureFrame FrameIndex="10" Tween="False">
            <TextureFile Type="MarkedSubImage" Path="jinhua/res/table/allin1.png" Plist="jinhua/res/table.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="20" Tween="False" />
        </Timeline>
      </Animation>
      <ObjectData Name="Layer" Tag="4388" ctype="GameLayerObjectData">
        <Size X="240.0000" Y="167.0000" />
        <Children>
          <AbstractNodeData Name="Sprite_2" ActionTag="251470575" Tag="4389" RotationSkewX="90.0000" RotationSkewY="90.0000" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="-28.9920" RightMargin="92.9920" TopMargin="-274.0004" BottomMargin="160.0004" ctype="SpriteObjectData">
            <Size X="176.0000" Y="281.0000" />
            <AnchorPoint />
            <Position X="-28.9920" Y="160.0004" />
            <Scale ScaleX="1.0000" ScaleY="1.1700" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="-0.1208" Y="0.9581" />
            <PreSize X="0.7333" Y="1.6826" />
            <FileData Type="Default" Path="Default/Sprite.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="allin" ActionTag="-2124656914" Tag="5340" IconVisible="False" LeftMargin="155.2191" RightMargin="11.7809" TopMargin="90.1374" BottomMargin="34.8626" ctype="SpriteObjectData">
            <Size X="73.0000" Y="42.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="191.7191" Y="55.8626" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.7988" Y="0.3345" />
            <PreSize X="0.3042" Y="0.2515" />
            <FileData Type="MarkedSubImage" Path="jinhua/lan/cn/allin.png" Plist="jinhua/lan/cn/game.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>