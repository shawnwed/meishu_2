<GameFile>
  <PropertyGroup Name="compare" Type="Node" ID="218a02cf-6653-47f5-bb97-4ebb52fd74a0" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="65" Speed="1.0000">
        <Timeline ActionTag="-480780859" Property="Position">
          <PointFrame FrameIndex="0" X="-72.0072" Y="3.0000">
            <EasingData Type="0" />
          </PointFrame>
          <PointFrame FrameIndex="30" X="49.9880" Y="3.0000">
            <EasingData Type="0" />
          </PointFrame>
        </Timeline>
        <Timeline ActionTag="-480780859" Property="FrameEvent">
          <EventFrame FrameIndex="30" Tween="False" Value="end" />
        </Timeline>
        <Timeline ActionTag="-684750486" Property="Position">
          <PointFrame FrameIndex="0" X="103.0000" Y="0.0000">
            <EasingData Type="0" />
          </PointFrame>
          <PointFrame FrameIndex="30" X="-50.0082" Y="0.0000">
            <EasingData Type="0" />
          </PointFrame>
        </Timeline>
        <Timeline ActionTag="-299482116" Property="Scale">
          <ScaleFrame FrameIndex="0" X="1.5000" Y="1.5000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="30" X="1.0000" Y="1.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="40" X="1.0000" Y="1.0000">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="-299482116" Property="Alpha">
          <IntFrame FrameIndex="0" Value="0">
            <EasingData Type="0" />
          </IntFrame>
          <IntFrame FrameIndex="30" Value="255">
            <EasingData Type="0" />
          </IntFrame>
          <IntFrame FrameIndex="40" Value="0">
            <EasingData Type="0" />
          </IntFrame>
        </Timeline>
        <Timeline ActionTag="-299482116" Property="Position">
          <PointFrame FrameIndex="0" X="10.0000" Y="0.0000">
            <EasingData Type="0" />
          </PointFrame>
          <PointFrame FrameIndex="30" X="10.0000" Y="0.0000">
            <EasingData Type="0" />
          </PointFrame>
          <PointFrame FrameIndex="40" X="10.0000" Y="0.0000">
            <EasingData Type="0" />
          </PointFrame>
        </Timeline>
        <Timeline ActionTag="-299482116" Property="RotationSkew">
          <ScaleFrame FrameIndex="40" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="-1450719757" Property="Scale">
          <ScaleFrame FrameIndex="0" X="1.5000" Y="1.5000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="30" X="1.0000" Y="1.0000">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="-1450719757" Property="Alpha">
          <IntFrame FrameIndex="0" Value="0">
            <EasingData Type="0" />
          </IntFrame>
          <IntFrame FrameIndex="30" Value="255">
            <EasingData Type="0" />
          </IntFrame>
        </Timeline>
        <Timeline ActionTag="1306005338" Property="VisibleForFrame">
          <BoolFrame FrameIndex="0" Tween="False" Value="False" />
        </Timeline>
        <Timeline ActionTag="322148530" Property="VisibleForFrame">
          <BoolFrame FrameIndex="0" Tween="False" Value="False" />
        </Timeline>
        <Timeline ActionTag="1779321238" Property="Position">
          <PointFrame FrameIndex="0" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </PointFrame>
          <PointFrame FrameIndex="24" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </PointFrame>
          <PointFrame FrameIndex="65" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </PointFrame>
        </Timeline>
        <Timeline ActionTag="1779321238" Property="Scale">
          <ScaleFrame FrameIndex="0" X="1.0000" Y="1.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="24" X="1.0000" Y="1.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="65" X="1.0000" Y="1.0000">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="1779321238" Property="RotationSkew">
          <ScaleFrame FrameIndex="0" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="24" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="65" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="1779321238" Property="FileData">
          <TextureFrame FrameIndex="0" Tween="False">
            <TextureFile Type="MarkedSubImage" Path="jinhua/res/table/lighting1.png" Plist="jinhua/res/table.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="24" Tween="False">
            <TextureFile Type="MarkedSubImage" Path="jinhua/res/table/lighting1.png" Plist="jinhua/res/table.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="25" Tween="False">
            <TextureFile Type="MarkedSubImage" Path="jinhua/res/table/lighting1.png" Plist="jinhua/res/table.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="35" Tween="False">
            <TextureFile Type="MarkedSubImage" Path="jinhua/res/table/lighting2.png" Plist="jinhua/res/table.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="45" Tween="False">
            <TextureFile Type="MarkedSubImage" Path="jinhua/res/table/lighting3.png" Plist="jinhua/res/table.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="50" Tween="False">
            <TextureFile Type="MarkedSubImage" Path="jinhua/res/table/lighting4.png" Plist="jinhua/res/table.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="60" Tween="False">
            <TextureFile Type="MarkedSubImage" Path="jinhua/res/table/lighting1.png" Plist="jinhua/res/table.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="65" Tween="False">
            <TextureFile Type="MarkedSubImage" Path="jinhua/res/table/lighting1.png" Plist="jinhua/res/table.plist" />
          </TextureFrame>
        </Timeline>
        <Timeline ActionTag="1779321238" Property="Alpha">
          <IntFrame FrameIndex="0" Value="0">
            <EasingData Type="0" />
          </IntFrame>
          <IntFrame FrameIndex="24" Value="0">
            <EasingData Type="0" />
          </IntFrame>
          <IntFrame FrameIndex="25" Value="255">
            <EasingData Type="0" />
          </IntFrame>
          <IntFrame FrameIndex="65" Value="0">
            <EasingData Type="0" />
          </IntFrame>
        </Timeline>
        <Timeline ActionTag="-1376935816" Property="Position">
          <PointFrame FrameIndex="0" X="-538.5073" Y="-89.3396">
            <EasingData Type="0" />
          </PointFrame>
          <PointFrame FrameIndex="65" X="416.4919" Y="-89.3396">
            <EasingData Type="0" />
          </PointFrame>
        </Timeline>
        <Timeline ActionTag="-1376935816" Property="Scale">
          <ScaleFrame FrameIndex="0" X="1.0000" Y="1.0000">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="-1376935816" Property="RotationSkew">
          <ScaleFrame FrameIndex="0" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="-2133242719" Property="Position">
          <PointFrame FrameIndex="0" X="569.4960" Y="101.0976">
            <EasingData Type="0" />
          </PointFrame>
          <PointFrame FrameIndex="65" X="-416.5121" Y="101.0976">
            <EasingData Type="0" />
          </PointFrame>
        </Timeline>
      </Animation>
      <ObjectData Name="Node" Tag="2614" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="click" ActionTag="1904780065" Tag="320" IconVisible="False" LeftMargin="-750.0000" RightMargin="-750.0000" TopMargin="-750.0000" BottomMargin="-750.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="1500.0000" Y="1500.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="compare_left_1" ActionTag="-480780859" Tag="2615" IconVisible="False" LeftMargin="-733.0072" RightMargin="72.0072" TopMargin="-106.5000" BottomMargin="-100.5000" ctype="SpriteObjectData">
            <Size X="661.0000" Y="207.0000" />
            <Children>
              <AbstractNodeData Name="head1" ActionTag="955593052" Tag="16371" IconVisible="True" LeftMargin="268.5264" RightMargin="392.4736" TopMargin="89.9999" BottomMargin="117.0001" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint />
                <Position X="268.5264" Y="117.0001" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4062" Y="0.5652" />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="Normal" Path="game_public/csb/head.csd" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="name1" ActionTag="-792115953" Tag="2620" IconVisible="False" LeftMargin="251.0681" RightMargin="339.9319" TopMargin="154.7687" BottomMargin="24.2313" FontSize="28" LabelText="Alice" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="70.0000" Y="28.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="286.0681" Y="38.2313" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4328" Y="0.1847" />
                <PreSize X="0.1059" Y="0.1353" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="poker" ActionTag="-1407729961" Tag="2622" IconVisible="True" LeftMargin="433.5797" RightMargin="227.4203" TopMargin="192.1975" BottomMargin="14.8025" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint />
                <Position X="433.5797" Y="14.8025" />
                <Scale ScaleX="1.2000" ScaleY="1.2000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.6559" Y="0.0715" />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="Normal" Path="jinhua/csb/table/pokers.csd" Plist="" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
            <Position X="-72.0072" Y="3.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="jinhua/res/table/compare_left.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="compare_right_2" ActionTag="-684750486" Tag="2616" IconVisible="False" LeftMargin="103.0000" RightMargin="-764.0000" TopMargin="-103.5000" BottomMargin="-103.5000" ctype="SpriteObjectData">
            <Size X="661.0000" Y="207.0000" />
            <Children>
              <AbstractNodeData Name="head2" ActionTag="-419584718" Tag="16374" IconVisible="True" LeftMargin="404.5276" RightMargin="256.4724" TopMargin="86.9998" BottomMargin="120.0002" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint />
                <Position X="404.5276" Y="120.0002" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.6120" Y="0.5797" />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="Normal" Path="game_public/csb/head.csd" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="name2" ActionTag="-554031343" Tag="2621" IconVisible="False" LeftMargin="365.1571" RightMargin="225.8429" TopMargin="150.5334" BottomMargin="28.4666" FontSize="28" LabelText="Alice" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="70.0000" Y="28.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="400.1571" Y="42.4666" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.6054" Y="0.2052" />
                <PreSize X="0.1059" Y="0.1353" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="poker" ActionTag="-541172534" Tag="2646" IconVisible="True" LeftMargin="233.6512" RightMargin="427.3488" TopMargin="189.1972" BottomMargin="17.8028" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint />
                <Position X="233.6512" Y="17.8028" />
                <Scale ScaleX="1.2000" ScaleY="1.2000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3535" Y="0.0860" />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="Normal" Path="jinhua/csb/table/pokers.csd" Plist="" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleY="0.5000" />
            <Position X="103.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="jinhua/res/table/compare_right.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="pk_shadow" ActionTag="-299482116" Alpha="0" Tag="16000" IconVisible="False" LeftMargin="-99.5000" RightMargin="-119.5000" TopMargin="-80.5000" BottomMargin="-80.5000" ctype="SpriteObjectData">
            <Size X="219.0000" Y="161.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="10.0000" />
            <Scale ScaleX="1.5000" ScaleY="1.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="jinhua/res/table/pk1.png" Plist="jinhua/res/table.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="pk" ActionTag="-1450719757" Alpha="0" Tag="2617" IconVisible="False" LeftMargin="-80.0000" RightMargin="-100.0000" TopMargin="-73.5000" BottomMargin="-73.5000" ctype="SpriteObjectData">
            <Size X="180.0000" Y="147.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="10.0000" />
            <Scale ScaleX="1.5000" ScaleY="1.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="jinhua/res/table/pk.png" Plist="jinhua/res/table.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="lose2" ActionTag="1306005338" VisibleForFrame="False" Tag="4912" IconVisible="True" LeftMargin="189.5342" RightMargin="-189.5342" TopMargin="-20.4999" BottomMargin="20.4999" ctype="SingleNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <Children>
              <AbstractNodeData Name="compare_x1_39" ActionTag="-998982231" Tag="2673" IconVisible="False" LeftMargin="-74.5754" RightMargin="-69.4246" TopMargin="-50.4244" BottomMargin="-63.5756" ctype="SpriteObjectData">
                <Size X="144.0000" Y="114.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="-2.5754" Y="-6.5756" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="Normal" Path="jinhua/res/table/compare_x1.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="compare_x2_40" ActionTag="-1664427842" Tag="2674" IconVisible="False" LeftMargin="-73.5756" RightMargin="-70.4244" TopMargin="-55.4241" BottomMargin="-58.5759" ctype="SpriteObjectData">
                <Size X="144.0000" Y="114.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="-1.5756" Y="-1.5759" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="Normal" Path="jinhua/res/table/compare_x2.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="status_lose_36" ActionTag="497259795" Tag="2670" IconVisible="False" LeftMargin="-48.2291" RightMargin="-38.7709" TopMargin="-18.6199" BottomMargin="-28.3801" ctype="SpriteObjectData">
                <Size X="87.0000" Y="47.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="-4.7291" Y="-4.8801" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="MarkedSubImage" Path="jinhua/lan/cn/cmp_lose.png" Plist="jinhua/lan/cn/game.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position X="189.5342" Y="20.4999" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="lose1" ActionTag="322148530" VisibleForFrame="False" Tag="4913" IconVisible="True" LeftMargin="-177.2198" RightMargin="177.2198" TopMargin="-20.4999" BottomMargin="20.4999" ctype="SingleNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <Children>
              <AbstractNodeData Name="compare_x1_39" ActionTag="1455110591" Tag="4915" IconVisible="False" LeftMargin="-74.5752" RightMargin="-69.4248" TopMargin="-50.4244" BottomMargin="-63.5756" ctype="SpriteObjectData">
                <Size X="144.0000" Y="114.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="-2.5752" Y="-6.5756" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="Normal" Path="jinhua/res/table/compare_x1.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="compare_x2_40" ActionTag="1739005013" Tag="4916" IconVisible="False" LeftMargin="-74.5756" RightMargin="-69.4244" TopMargin="-52.0000" BottomMargin="-62.0000" ctype="SpriteObjectData">
                <Size X="144.0000" Y="114.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="-2.5756" Y="-5.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="Normal" Path="jinhua/res/table/compare_x2.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="status_lose_36" ActionTag="-685974302" Tag="4914" IconVisible="False" LeftMargin="-48.2285" RightMargin="-38.7715" TopMargin="-18.6199" BottomMargin="-28.3801" ctype="SpriteObjectData">
                <Size X="87.0000" Y="47.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="-4.7285" Y="-4.8801" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="MarkedSubImage" Path="jinhua/lan/cn/cmp_lose.png" Plist="jinhua/lan/cn/game.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position X="-177.2198" Y="20.4999" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="Sprite_49" ActionTag="1779321238" Alpha="0" Tag="9177" IconVisible="False" LeftMargin="-175.0000" RightMargin="-175.0000" TopMargin="-50.0000" BottomMargin="-50.0000" ctype="SpriteObjectData">
            <Size X="350.0000" Y="100.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="jinhua/res/table/lighting1.png" Plist="jinhua/res/table.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="pk_light1" ActionTag="-1376935816" Tag="18060" IconVisible="False" LeftMargin="-733.0073" RightMargin="344.0073" TopMargin="75.8396" BottomMargin="-102.8396" ctype="SpriteObjectData">
            <Size X="389.0000" Y="27.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-538.5073" Y="-89.3396" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="jinhua/res/table/pk_light.png" Plist="jinhua/res/table.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="pk_light2" ActionTag="-2133242719" Tag="18061" IconVisible="False" LeftMargin="374.9960" RightMargin="-763.9960" TopMargin="-114.5976" BottomMargin="87.5976" ctype="SpriteObjectData">
            <Size X="389.0000" Y="27.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="569.4960" Y="101.0976" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="jinhua/res/table/pk_light.png" Plist="jinhua/res/table.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>