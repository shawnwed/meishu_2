<GameFile>
  <PropertyGroup Name="pokers" Type="Node" ID="4bfb5bd8-e570-47db-adff-09b960356e40" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="30" Speed="1.0000">
        <Timeline ActionTag="1071128868" Property="Position">
          <PointFrame FrameIndex="30" X="0.0000" Y="84.0764">
            <EasingData Type="0" />
          </PointFrame>
        </Timeline>
        <Timeline ActionTag="1071128868" Property="VisibleForFrame">
          <BoolFrame FrameIndex="30" Tween="False" Value="False" />
        </Timeline>
      </Animation>
      <ObjectData Name="Node" Tag="43" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="poker_1" ActionTag="-542856340" Tag="199" IconVisible="True" LeftMargin="-35.6306" RightMargin="35.6306" TopMargin="-45.0000" BottomMargin="45.0000" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="-35.6306" Y="45.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="jinhua/csb/table/card.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="poker_2" ActionTag="-1726535012" Tag="206" IconVisible="True" LeftMargin="0.0595" RightMargin="-0.0595" TopMargin="-45.0005" BottomMargin="45.0005" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="0.0595" Y="45.0005" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="jinhua/csb/table/card.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="poker_3" ActionTag="1010663294" Tag="213" IconVisible="True" LeftMargin="35.7414" RightMargin="-35.7414" TopMargin="-45.0005" BottomMargin="45.0005" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="35.7414" Y="45.0005" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="jinhua/csb/table/card.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="status_look" ActionTag="374755430" VisibleForFrame="False" Tag="47" IconVisible="False" LeftMargin="-34.9998" RightMargin="-27.0002" TopMargin="-114.8109" BottomMargin="48.8109" ctype="SpriteObjectData">
            <Size X="62.0000" Y="66.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-3.9998" Y="81.8109" />
            <Scale ScaleX="0.9000" ScaleY="0.9000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Default" Path="Default/Sprite.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="chip" ActionTag="-228986119" VisibleForFrame="False" Tag="11232" IconVisible="True" LeftMargin="0.0004" RightMargin="-0.0004" TopMargin="-26.6175" BottomMargin="26.6175" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="0.0004" Y="26.6175" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="jinhua/csb/table/chip_num.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="allin" ActionTag="1071128868" VisibleForFrame="False" Tag="18268" IconVisible="True" TopMargin="-84.0764" BottomMargin="84.0764" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position Y="84.0764" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="jinhua/csb/table/allin_txt.csd" Plist="" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>