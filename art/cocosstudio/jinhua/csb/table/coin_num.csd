<GameFile>
  <PropertyGroup Name="coin_num" Type="Node" ID="b1ecb2bc-60e6-4f7b-bf0a-e2657d872c05" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="1502" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="Image_1" ActionTag="2073612147" Tag="1027" IconVisible="False" LeftMargin="-55.0004" RightMargin="-54.9996" TopMargin="-16.0000" BottomMargin="-16.0000" Scale9Enable="True" LeftEage="44" RightEage="44" TopEage="10" BottomEage="10" Scale9OriginX="44" Scale9OriginY="10" Scale9Width="12" Scale9Height="10" ctype="ImageViewObjectData">
            <Size X="110.0000" Y="32.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="-55.0004" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="jinhua/res/table/coin_bg.png" Plist="jinhua/res/table.plist" />
          </AbstractNodeData>
          <AbstractNodeData Name="bg_chip_1" ActionTag="-1539119759" Tag="1503" IconVisible="False" LeftMargin="-61.4022" RightMargin="18.4022" TopMargin="-21.5397" BottomMargin="-21.4603" ctype="SpriteObjectData">
            <Size X="43.0000" Y="43.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-39.9022" Y="0.0397" />
            <Scale ScaleX="0.6800" ScaleY="0.6800" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="hall/res/first/coin_money.png" Plist="hall/res/first.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="txt" ActionTag="929907972" Tag="1504" IconVisible="False" LeftMargin="-19.2416" RightMargin="-34.7584" TopMargin="-9.2589" BottomMargin="-8.7411" FontSize="18" LabelText="1000万" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="54.0000" Y="18.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="-19.2416" Y="0.2589" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>