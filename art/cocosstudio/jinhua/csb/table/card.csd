<GameFile>
  <PropertyGroup Name="card" Type="Node" ID="eb08e33d-c6d9-429b-a402-87cb8a7a123d" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="202" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="card" ActionTag="1870775304" Tag="2494" IconVisible="True" TopMargin="-42.0000" BottomMargin="42.0000" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position Y="42.0000" />
            <Scale ScaleX="0.4500" ScaleY="0.4500" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="common/card.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="poker_back" ActionTag="-1459446015" Tag="198" IconVisible="False" LeftMargin="-68.0000" RightMargin="-68.0000" TopMargin="-131.5000" BottomMargin="-47.5000" ctype="SpriteObjectData">
            <Size X="136.0000" Y="179.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="0.0000" Y="42.0000" />
            <Scale ScaleX="0.4500" ScaleY="0.4500" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="common/cards/cards_back.png" Plist="common/cards.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>