<GameFile>
  <PropertyGroup Name="jinhua" Type="Scene" ID="42677281-d268-4e50-ac56-6ab8cd5b07f2" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Scene" Tag="29" ctype="GameNodeObjectData">
        <Size X="960.0000" Y="640.0000" />
        <Children>
          <AbstractNodeData Name="bg" Visible="False" ActionTag="-1994063988" Tag="31" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" LeftEage="422" RightEage="422" TopEage="237" BottomEage="237" Scale9OriginX="422" Scale9OriginY="237" Scale9Width="436" Scale9Height="246" ctype="ImageViewObjectData">
            <Size X="960.0000" Y="640.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="480.0000" Y="320.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="1.0000" Y="1.0000" />
            <FileData Type="Normal" Path="bg/cdd_bg.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="top" Visible="False" ActionTag="-132075891" Tag="1918" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" TouchEnable="True" ClipAble="False" BackColorAlpha="0" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="960.0000" Y="640.0000" />
            <Children>
              <AbstractNodeData Name="bg" ActionTag="-1492203869" Tag="1919" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="47.8960" RightMargin="38.1040" TopMargin="0.3800" BottomMargin="602.6200" ctype="SpriteObjectData">
                <Size X="874.0000" Y="37.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="484.8960" Y="621.1200" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5051" Y="0.9705" />
                <PreSize X="0.9104" Y="0.0578" />
                <FileData Type="MarkedSubImage" Path="landlord/res/table/top_bg.png" Plist="landlord/res/landlord.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="top_info" ActionTag="-1989632817" Tag="1958" IconVisible="True" PositionPercentYEnabled="True" LeftMargin="205.9440" RightMargin="754.0560" BottomMargin="640.0000" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint />
                <Position X="205.9440" Y="640.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2145" Y="1.0000" />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="Normal" Path="game_public/csb/top_info_0.csd" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="score_icon" ActionTag="-1930322676" Tag="1964" IconVisible="False" LeftMargin="408.9650" RightMargin="531.0350" TopMargin="8.7910" BottomMargin="613.2090" ctype="SpriteObjectData">
                <Size X="20.0000" Y="18.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="418.9650" Y="622.2090" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4364" Y="0.9722" />
                <PreSize X="0.0208" Y="0.0281" />
                <FileData Type="MarkedSubImage" Path="landlord/res/table/score_icon.png" Plist="landlord/res/landlord.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="basescore_title" ActionTag="-148554073" Tag="1965" IconVisible="False" LeftMargin="788.3812" RightMargin="116.6188" TopMargin="7.1880" BottomMargin="610.8120" FontSize="22" LabelText="底分:" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="55.0000" Y="22.0000" />
                <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
                <Position X="843.3812" Y="621.8120" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="223" G="223" B="223" />
                <PrePosition X="0.8785" Y="0.9716" />
                <PreSize X="0.0573" Y="0.0344" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="basescore_txt" ActionTag="-982257982" Tag="1966" IconVisible="False" LeftMargin="847.3506" RightMargin="68.6494" TopMargin="7.1880" BottomMargin="610.8120" FontSize="22" LabelText="1000" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="44.0000" Y="22.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="847.3506" Y="621.8120" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="253" G="246" B="128" />
                <PrePosition X="0.8827" Y="0.9716" />
                <PreSize X="0.0458" Y="0.0344" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="roomid_txt" ActionTag="-1997521361" Tag="1967" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="316.9492" RightMargin="577.0508" TopMargin="5.6948" BottomMargin="612.3052" FontSize="22" LabelText="123456" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="66.0000" Y="22.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="316.9492" Y="623.3052" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="253" G="246" B="128" />
                <PrePosition X="0.3302" Y="0.9739" />
                <PreSize X="0.0688" Y="0.0344" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="roomid_title" ActionTag="1328716593" Tag="1968" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="261.4166" RightMargin="643.5834" TopMargin="5.6938" BottomMargin="612.3062" FontSize="22" LabelText="房号:" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="55.0000" Y="22.0000" />
                <AnchorPoint ScaleX="0.5063" ScaleY="0.6527" />
                <Position X="289.2631" Y="626.6656" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="223" G="223" B="223" />
                <PrePosition X="0.3013" Y="0.9792" />
                <PreSize X="0.0573" Y="0.0344" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="round_txt" ActionTag="-1059078785" Tag="1969" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="434.6604" RightMargin="503.3396" TopMargin="5.6948" BottomMargin="612.3052" FontSize="22" LabelText="10" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="22.0000" Y="22.0000" />
                <Children>
                  <AbstractNodeData Name="txt" ActionTag="-1902956469" Tag="1970" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="25.2450" RightMargin="-36.2450" TopMargin="-0.3564" BottomMargin="0.3564" FontSize="22" LabelText="/20" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="33.0000" Y="22.0000" />
                    <AnchorPoint ScaleY="0.6527" />
                    <Position X="25.2450" Y="14.7158" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="223" G="223" B="223" />
                    <PrePosition X="1.1475" Y="0.6689" />
                    <PreSize X="1.5000" Y="1.0000" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleY="0.5000" />
                <Position X="434.6604" Y="623.3052" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="253" G="246" B="128" />
                <PrePosition X="0.4528" Y="0.9739" />
                <PreSize X="0.0229" Y="0.0344" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="limit_title" ActionTag="-654082296" Tag="1971" IconVisible="False" LeftMargin="681.3727" RightMargin="223.6273" TopMargin="7.1880" BottomMargin="610.8120" FontSize="22" LabelText="限注:" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="55.0000" Y="22.0000" />
                <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
                <Position X="736.3727" Y="621.8120" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="223" G="223" B="223" />
                <PrePosition X="0.7671" Y="0.9716" />
                <PreSize X="0.0573" Y="0.0344" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="limit_txt" ActionTag="246049129" Tag="1972" IconVisible="False" LeftMargin="736.5028" RightMargin="179.4972" TopMargin="6.5457" BottomMargin="611.4543" FontSize="22" LabelText="2000" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="44.0000" Y="22.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="736.5028" Y="622.4543" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="253" G="246" B="128" />
                <PrePosition X="0.7672" Y="0.9726" />
                <PreSize X="0.0458" Y="0.0344" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="fee_type_txt" ActionTag="963487402" Tag="1973" IconVisible="False" LeftMargin="507.4030" RightMargin="386.5970" TopMargin="7.1881" BottomMargin="610.8119" FontSize="22" LabelText="AA收费" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="66.0000" Y="22.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="540.4030" Y="621.8119" />
                <Scale ScaleX="1.0000" ScaleY="0.9350" />
                <CColor A="255" R="253" G="246" B="128" />
                <PrePosition X="0.5629" Y="0.9716" />
                <PreSize X="0.0688" Y="0.0344" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="men_txt" ActionTag="-109347806" Tag="1974" IconVisible="False" LeftMargin="596.3647" RightMargin="297.6353" TopMargin="6.5457" BottomMargin="611.4543" FontSize="22" LabelText="闷三圈" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="66.0000" Y="22.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="629.3647" Y="622.4543" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="253" G="246" B="128" />
                <PrePosition X="0.6556" Y="0.9726" />
                <PreSize X="0.0688" Y="0.0344" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="480.0000" Y="320.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="1.0000" Y="1.0000" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="btn_prvData" ActionTag="901549846" VisibleForFrame="False" Tag="429" IconVisible="False" LeftMargin="1184.4998" RightMargin="-293.4998" TopMargin="463.8625" BottomMargin="107.1375" ctype="SpriteObjectData">
            <Size X="69.0000" Y="69.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1218.9998" Y="141.6375" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="1.2698" Y="0.2213" />
            <PreSize X="0.0719" Y="0.1078" />
            <FileData Type="MarkedSubImage" Path="landlord/res/table/top_help.png" Plist="landlord/res/landlord.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="btn_menu" ActionTag="758192081" Tag="32" IconVisible="False" HorizontalEdge="LeftEdge" VerticalEdge="TopEdge" LeftMargin="15.0000" RightMargin="876.0000" TopMargin="15.0000" BottomMargin="556.0000" ctype="SpriteObjectData">
            <Size X="69.0000" Y="69.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="49.5000" Y="590.5000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0516" Y="0.9227" />
            <PreSize X="0.0719" Y="0.1078" />
            <FileData Type="MarkedSubImage" Path="bullfighting/res/table/btn_menu.png" Plist="bullfighting/res/table.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="btn_chat" ActionTag="-409780991" VisibleForFrame="False" Tag="34" IconVisible="False" HorizontalEdge="RightEdge" VerticalEdge="BottomEdge" LeftMargin="-693.4860" RightMargin="1584.4860" TopMargin="573.3590" BottomMargin="-2.3590" ctype="SpriteObjectData">
            <Size X="69.0000" Y="69.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-658.9860" Y="32.1410" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="-0.6864" Y="0.0502" />
            <PreSize X="0.0719" Y="0.1078" />
            <FileData Type="MarkedSubImage" Path="game_public/res/btn_chat.png" Plist="game_public/res/game.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="seat_my" ActionTag="1707075156" Tag="62" IconVisible="True" HorizontalEdge="LeftEdge" VerticalEdge="BottomEdge" LeftMargin="81.0000" RightMargin="879.0000" TopMargin="573.0872" BottomMargin="66.9128" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <Children>
              <AbstractNodeData Name="ok" ActionTag="-2058176116" Tag="1448" IconVisible="False" LeftMargin="86.5357" RightMargin="-193.5357" TopMargin="-80.0595" BottomMargin="23.0595" ctype="SpriteObjectData">
                <Size X="107.0000" Y="57.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="140.0357" Y="51.5595" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="MarkedSubImage" Path="jinhua/lan/cn/landlord_ready_word.png" Plist="jinhua/lan/cn/game.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="chip_allin" ActionTag="-1194298547" VisibleForFrame="False" Tag="18235" IconVisible="False" LeftMargin="486.6272" RightMargin="-631.6272" TopMargin="-351.0123" BottomMargin="225.0123" ctype="SpriteObjectData">
                <Size X="145.0000" Y="126.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="559.1272" Y="288.0123" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="MarkedSubImage" Path="jinhua/res/table/chip_allin.png" Plist="jinhua/res/table.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position X="81.0000" Y="66.9128" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0844" Y="0.1046" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="jinhua/csb/table/seat.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="quit" ActionTag="1672773147" VisibleForFrame="False" Tag="1359" IconVisible="True" LeftMargin="643.7470" RightMargin="316.2530" TopMargin="462.5795" BottomMargin="177.4205" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="0.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="643.7470" Y="177.4205" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.6706" Y="0.2772" />
            <PreSize X="0.0000" Y="0.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="seat_other3" ActionTag="1264512032" Tag="80" IconVisible="True" PositionPercentYEnabled="True" HorizontalEdge="LeftEdge" LeftMargin="81.0000" RightMargin="879.0000" TopMargin="215.4880" BottomMargin="424.5120" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <Children>
              <AbstractNodeData Name="quit" ActionTag="-566664381" VisibleForFrame="False" Tag="1341" IconVisible="True" LeftMargin="218.1403" RightMargin="-218.1403" TopMargin="-73.0000" BottomMargin="73.0000" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="0.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint />
                <Position X="218.1403" Y="73.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="compare_border" CanEdit="False" ActionTag="1094850428" Tag="2693" IconVisible="True" LeftMargin="-74.3831" RightMargin="-305.6169" TopMargin="-157.9945" BottomMargin="-66.0055" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
                <Size X="380.0000" Y="224.0000" />
                <AnchorPoint />
                <Position X="-74.3831" Y="-66.0055" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="Normal" Path="jinhua/csb/table/compare_border1.csd" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="ok" ActionTag="-1273194429" Tag="1447" IconVisible="False" LeftMargin="117.0370" RightMargin="-163.0370" TopMargin="-78.3751" BottomMargin="32.3751" ctype="SpriteObjectData">
                <Size X="46.0000" Y="46.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="140.0370" Y="55.3751" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="Default" Path="Default/Sprite.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="chip_allin" ActionTag="1747690126" VisibleForFrame="False" Tag="18237" IconVisible="False" LeftMargin="232.4653" RightMargin="-377.4653" TopMargin="-101.9541" BottomMargin="-24.0459" FlipX="True" ctype="SpriteObjectData">
                <Size X="145.0000" Y="126.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="304.9653" Y="38.9541" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="MarkedSubImage" Path="jinhua/res/table/chip_allin.png" Plist="jinhua/res/table.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position X="81.0000" Y="424.5120" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0844" Y="0.6633" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="jinhua/csb/table/seat.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="seat_other4" ActionTag="453459489" Tag="74" IconVisible="True" PositionPercentYEnabled="True" HorizontalEdge="LeftEdge" LeftMargin="81.0000" RightMargin="879.0000" TopMargin="392.0640" BottomMargin="247.9360" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <Children>
              <AbstractNodeData Name="quit" ActionTag="419596413" VisibleForFrame="False" Tag="1335" IconVisible="True" LeftMargin="209.9333" RightMargin="-209.9333" TopMargin="-73.8455" BottomMargin="73.8455" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="0.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint />
                <Position X="209.9333" Y="73.8455" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="compare_border" CanEdit="False" ActionTag="-1812722932" Tag="2689" IconVisible="True" LeftMargin="-74.3827" RightMargin="-305.6173" TopMargin="-155.1060" BottomMargin="-68.8940" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
                <Size X="380.0000" Y="224.0000" />
                <AnchorPoint />
                <Position X="-74.3827" Y="-68.8940" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="Normal" Path="jinhua/csb/table/compare_border1.csd" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="ok" ActionTag="612740698" Tag="1446" IconVisible="False" LeftMargin="117.0357" RightMargin="-163.0357" TopMargin="-79.0004" BottomMargin="33.0004" ctype="SpriteObjectData">
                <Size X="46.0000" Y="46.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="140.0357" Y="56.0004" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="Default" Path="Default/Sprite.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="chip_allin" ActionTag="-1668630346" VisibleForFrame="False" Tag="18238" IconVisible="False" LeftMargin="232.4653" RightMargin="-377.4653" TopMargin="-106.9526" BottomMargin="-19.0474" FlipX="True" ctype="SpriteObjectData">
                <Size X="145.0000" Y="126.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="304.9653" Y="43.9526" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="MarkedSubImage" Path="jinhua/res/table/chip_allin.png" Plist="jinhua/res/table.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position X="81.0000" Y="247.9360" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0844" Y="0.3874" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="jinhua/csb/table/seat.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="seat_other2" ActionTag="916709497" Tag="86" IconVisible="True" PositionPercentYEnabled="True" HorizontalEdge="RightEdge" LeftMargin="879.0000" RightMargin="81.0000" TopMargin="215.4880" BottomMargin="424.5120" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <Children>
              <AbstractNodeData Name="quit" ActionTag="537153388" VisibleForFrame="False" Tag="1347" IconVisible="True" LeftMargin="-90.0365" RightMargin="90.0365" TopMargin="-68.1000" BottomMargin="68.1000" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="0.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint />
                <Position X="-90.0365" Y="68.1000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="compare_border" CanEdit="False" ActionTag="-1078060228" Tag="2678" IconVisible="True" LeftMargin="-297.2897" RightMargin="-82.7103" TopMargin="-157.9945" BottomMargin="-66.0055" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
                <Size X="380.0000" Y="224.0000" />
                <AnchorPoint />
                <Position X="-297.2897" Y="-66.0055" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="Normal" Path="jinhua/csb/table/compare_border.csd" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="ok" ActionTag="-1208351618" Tag="1445" IconVisible="False" LeftMargin="-162.0165" RightMargin="116.0165" TopMargin="-78.3745" BottomMargin="32.3745" ctype="SpriteObjectData">
                <Size X="46.0000" Y="46.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="-139.0165" Y="55.3745" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="Default" Path="Default/Sprite.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="chip_allin" ActionTag="-965650771" VisibleForFrame="False" Tag="18239" IconVisible="False" LeftMargin="-376.2430" RightMargin="231.2430" TopMargin="-101.9542" BottomMargin="-24.0458" ctype="SpriteObjectData">
                <Size X="145.0000" Y="126.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="-303.7430" Y="38.9542" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="MarkedSubImage" Path="jinhua/res/table/chip_allin.png" Plist="jinhua/res/table.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position X="879.0000" Y="424.5120" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.9156" Y="0.6633" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="jinhua/csb/table/seat.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="seat_other1" ActionTag="-3099786" Tag="92" IconVisible="True" PositionPercentYEnabled="True" HorizontalEdge="RightEdge" LeftMargin="879.0000" RightMargin="81.0000" TopMargin="392.0640" BottomMargin="247.9360" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <Children>
              <AbstractNodeData Name="quit" ActionTag="-694151406" VisibleForFrame="False" Tag="1353" IconVisible="True" LeftMargin="-90.0359" RightMargin="90.0359" TopMargin="-73.8455" BottomMargin="73.8455" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="0.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint />
                <Position X="-90.0359" Y="73.8455" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="compare_border" CanEdit="False" ActionTag="-1665366733" Tag="2682" IconVisible="True" LeftMargin="-297.3019" RightMargin="-82.6981" TopMargin="-155.1060" BottomMargin="-68.8940" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
                <Size X="380.0000" Y="224.0000" />
                <AnchorPoint />
                <Position X="-297.3019" Y="-68.8940" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="Normal" Path="jinhua/csb/table/compare_border.csd" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="ok" ActionTag="-1342664940" Tag="1443" IconVisible="False" LeftMargin="-163.0162" RightMargin="117.0162" TopMargin="-79.0002" BottomMargin="33.0002" ctype="SpriteObjectData">
                <Size X="46.0000" Y="46.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="-140.0162" Y="56.0002" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="Default" Path="Default/Sprite.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="chip_allin" ActionTag="-281241636" VisibleForFrame="False" Tag="18236" IconVisible="False" LeftMargin="-377.2433" RightMargin="232.2433" TopMargin="-106.9526" BottomMargin="-19.0474" ctype="SpriteObjectData">
                <Size X="145.0000" Y="126.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="-304.7433" Y="43.9526" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="MarkedSubImage" Path="jinhua/res/table/chip_allin.png" Plist="jinhua/res/table.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position X="879.0000" Y="247.9360" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.9156" Y="0.3874" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="jinhua/csb/table/seat.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="poker" CanEdit="False" ActionTag="-373989113" Tag="5897" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="960.0000" Y="640.0000" />
            <Children>
              <AbstractNodeData Name="pokers1" ActionTag="-1742803196" Tag="138" IconVisible="True" PositionPercentXEnabled="True" VerticalEdge="BottomEdge" LeftMargin="480.0000" RightMargin="480.0000" TopMargin="540.0000" BottomMargin="100.0000" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint />
                <Position X="480.0000" Y="100.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.1563" />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="Normal" Path="jinhua/csb/table/pokers_my.csd" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="pokers2" ActionTag="-284029354" Tag="126" IconVisible="True" PositionPercentYEnabled="True" HorizontalEdge="RightEdge" LeftMargin="725.0767" RightMargin="234.9233" TopMargin="417.4720" BottomMargin="222.5280" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint />
                <Position X="725.0767" Y="222.5280" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7553" Y="0.3477" />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="Normal" Path="jinhua/csb/table/pokers.csd" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="pokers3" ActionTag="-77131603" Tag="120" IconVisible="True" PositionPercentYEnabled="True" HorizontalEdge="RightEdge" LeftMargin="725.0789" RightMargin="234.9211" TopMargin="240.1280" BottomMargin="399.8720" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint />
                <Position X="725.0789" Y="399.8720" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7553" Y="0.6248" />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="Normal" Path="jinhua/csb/table/pokers.csd" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="pokers4" ActionTag="1290659158" Tag="108" IconVisible="True" PositionPercentYEnabled="True" HorizontalEdge="LeftEdge" LeftMargin="233.2744" RightMargin="726.7256" TopMargin="240.1280" BottomMargin="399.8720" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint />
                <Position X="233.2744" Y="399.8720" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2430" Y="0.6248" />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="Normal" Path="jinhua/csb/table/pokers.csd" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="pokers5" ActionTag="1064272914" Tag="114" IconVisible="True" PositionPercentYEnabled="True" HorizontalEdge="LeftEdge" LeftMargin="233.2744" RightMargin="726.7256" TopMargin="417.4720" BottomMargin="222.5280" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint />
                <Position X="233.2744" Y="222.5280" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2430" Y="0.3477" />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="Normal" Path="jinhua/csb/table/pokers.csd" Plist="" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="1.0000" Y="1.0000" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="room_info" ActionTag="-2077485645" Tag="1671" IconVisible="True" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="480.0000" RightMargin="480.0000" BottomMargin="640.0000" ctype="SingleNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <Children>
              <AbstractNodeData Name="id_txt" ActionTag="-1892772024" VisibleForFrame="False" Tag="405" IconVisible="False" LeftMargin="-261.4661" RightMargin="191.4661" TopMargin="38.6379" BottomMargin="-66.6379" FontSize="28" LabelText="房号:" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="70.0000" Y="28.0000" />
                <Children>
                  <AbstractNodeData Name="txt" ActionTag="2122787973" Tag="617" IconVisible="False" LeftMargin="73.0000" RightMargin="-87.0000" TopMargin="-2.0000" BottomMargin="2.0000" FontSize="28" LabelText="123456" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="84.0000" Y="28.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="73.0000" Y="16.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="229" B="54" />
                    <PrePosition X="1.0429" Y="0.5714" />
                    <PreSize X="1.2000" Y="1.0000" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="-226.4661" Y="-52.6379" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="bet_title" ActionTag="-289071886" VisibleForFrame="False" Tag="4477" IconVisible="False" LeftMargin="-22.5248" RightMargin="-47.4752" TopMargin="38.6379" BottomMargin="-66.6379" FontSize="28" LabelText="底注:" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="70.0000" Y="28.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="-22.5248" Y="-52.6379" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="bet" ActionTag="329860442" VisibleForFrame="False" Tag="1366" IconVisible="False" LeftMargin="49.0527" RightMargin="-63.0527" TopMargin="38.6379" BottomMargin="-66.6379" FontSize="28" LabelText="1" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="14.0000" Y="28.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="49.0527" Y="-52.6379" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="229" B="54" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="max_title" ActionTag="-2002413483" VisibleForFrame="False" Tag="4476" IconVisible="False" LeftMargin="143.0700" RightMargin="-227.0700" TopMargin="38.6379" BottomMargin="-66.6379" FontSize="28" LabelText="限注 :" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="84.0000" Y="28.0000" />
                <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
                <Position X="227.0700" Y="-52.6379" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="max" ActionTag="-942119276" VisibleForFrame="False" Tag="1367" IconVisible="False" LeftMargin="232.3202" RightMargin="-288.3202" TopMargin="38.6379" BottomMargin="-66.6379" FontSize="28" LabelText="5/20" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="56.0000" Y="28.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="232.3202" Y="-52.6379" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="229" B="54" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="look" ActionTag="-1575497826" VisibleForFrame="False" Tag="1368" IconVisible="False" LeftMargin="282.0164" RightMargin="-534.0164" TopMargin="257.0356" BottomMargin="-285.0356" FontSize="28" LabelText="可看牌轮数：无限制" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="252.0000" Y="28.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="282.0164" Y="-271.0356" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="174" G="242" B="253" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="compare" ActionTag="-1112991315" VisibleForFrame="False" Tag="1369" IconVisible="False" LeftMargin="282.9072" RightMargin="-492.9072" TopMargin="295.8578" BottomMargin="-323.8578" FontSize="28" LabelText="可比牌轮数：0/3" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="210.0000" Y="28.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="282.9072" Y="-309.8578" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="174" G="242" B="253" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="bet_sum" ActionTag="7989002" Tag="18752" IconVisible="False" LeftMargin="-78.5022" RightMargin="-90.4978" TopMargin="147.6005" BottomMargin="-187.6005" ctype="SpriteObjectData">
                <Size X="169.0000" Y="40.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="5.9978" Y="-167.6005" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="MarkedSubImage" Path="jinhua/lan/cn/bet_sum.png" Plist="jinhua/lan/cn/game.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="bets" ActionTag="-2026630740" Tag="1370" IconVisible="False" LeftMargin="-11.5283" RightMargin="-63.4717" TopMargin="153.5808" BottomMargin="-183.5808" FontSize="30" LabelText="12345" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="75.0000" Y="30.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="25.9717" Y="-168.5808" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="229" B="54" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="level" ActionTag="-1734531840" VisibleForFrame="False" Tag="430" IconVisible="False" LeftMargin="160.3701" RightMargin="-226.3701" TopMargin="1.7137" BottomMargin="-23.7137" FontSize="22" LabelText="新手场" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="66.0000" Y="22.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="160.3701" Y="-12.7137" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position X="480.0000" Y="640.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="1.0000" />
            <PreSize X="0.0000" Y="0.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="chips" CanEdit="False" ActionTag="-2109231787" Tag="1524" IconVisible="False" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="960.0000" Y="640.0000" />
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="1.0000" Y="1.0000" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="startPoker" CanEdit="False" ActionTag="1848853568" Tag="6450" IconVisible="False" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="960.0000" Y="640.0000" />
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="1.0000" Y="1.0000" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="dealer" ActionTag="1959344706" VisibleForFrame="False" Tag="3560" IconVisible="False" LeftMargin="1121.9193" RightMargin="-222.9193" TopMargin="-11.9734" BottomMargin="580.9734" ctype="SpriteObjectData">
            <Size X="61.0000" Y="71.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1152.4193" Y="616.4734" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="1.2004" Y="0.9632" />
            <PreSize X="0.0635" Y="0.1109" />
            <FileData Type="MarkedSubImage" Path="jinhua/lan/cn/dealer.png" Plist="jinhua/lan/cn/game.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="buttons" ActionTag="-322985335" Tag="577" IconVisible="True" HorizontalEdge="RightEdge" VerticalEdge="BottomEdge" LeftMargin="855.0315" RightMargin="104.9685" TopMargin="588.2522" BottomMargin="51.7478" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="855.0315" Y="51.7478" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8907" Y="0.0809" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="jinhua/csb/table/buttons.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="bubble" CanEdit="False" ActionTag="-640182755" Tag="1549" IconVisible="False" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="960.0000" Y="640.0000" />
            <Children>
              <AbstractNodeData Name="seat1" ActionTag="-836904320" Tag="1831" IconVisible="True" HorizontalEdge="LeftEdge" VerticalEdge="BottomEdge" LeftMargin="81.0000" RightMargin="879.0000" TopMargin="573.0872" BottomMargin="66.9128" ctype="SingleNodeObjectData">
                <Size X="0.0000" Y="0.0000" />
                <Children>
                  <AbstractNodeData Name="chat_bubble" ActionTag="749459244" Tag="2609" IconVisible="False" LeftMargin="15.0000" RightMargin="-112.0000" TopMargin="-159.5000" BottomMargin="102.5000" Scale9Enable="True" LeftEage="38" RightEage="18" TopEage="18" BottomEage="18" Scale9OriginX="38" Scale9OriginY="18" Scale9Width="41" Scale9Height="21" ctype="ImageViewObjectData">
                    <Size X="97.0000" Y="57.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="15.0000" Y="131.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition />
                    <PreSize X="0.0000" Y="0.0000" />
                    <FileData Type="MarkedSubImage" Path="jinhua/res/table/chat_bubble_1.png" Plist="jinhua/res/table.plist" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint />
                <Position X="81.0000" Y="66.9128" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0844" Y="0.1046" />
                <PreSize X="0.0000" Y="0.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="seat2" ActionTag="360836504" Tag="1832" IconVisible="True" PositionPercentYEnabled="True" HorizontalEdge="RightEdge" LeftMargin="879.0000" RightMargin="81.0000" TopMargin="392.0640" BottomMargin="247.9360" ctype="SingleNodeObjectData">
                <Size X="0.0000" Y="0.0000" />
                <Children>
                  <AbstractNodeData Name="chat_bubble" ActionTag="-1626832053" Tag="2613" IconVisible="False" LeftMargin="-112.0000" RightMargin="15.0000" TopMargin="-159.5000" BottomMargin="102.5000" Scale9Enable="True" LeftEage="18" RightEage="38" TopEage="18" BottomEage="18" Scale9OriginX="18" Scale9OriginY="18" Scale9Width="41" Scale9Height="21" ctype="ImageViewObjectData">
                    <Size X="97.0000" Y="57.0000" />
                    <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
                    <Position X="-15.0000" Y="131.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition />
                    <PreSize X="0.0000" Y="0.0000" />
                    <FileData Type="MarkedSubImage" Path="jinhua/res/table/chat_bubble_2.png" Plist="jinhua/res/table.plist" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint />
                <Position X="879.0000" Y="247.9360" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.9156" Y="0.3874" />
                <PreSize X="0.0000" Y="0.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="seat3" ActionTag="-512658446" Tag="1833" IconVisible="True" PositionPercentYEnabled="True" HorizontalEdge="RightEdge" LeftMargin="879.0000" RightMargin="81.0000" TopMargin="215.4880" BottomMargin="424.5120" ctype="SingleNodeObjectData">
                <Size X="0.0000" Y="0.0000" />
                <Children>
                  <AbstractNodeData Name="chat_bubble" ActionTag="-2116662193" Tag="2612" IconVisible="False" LeftMargin="-112.0000" RightMargin="15.0000" TopMargin="-159.5000" BottomMargin="102.5000" Scale9Enable="True" LeftEage="18" RightEage="38" TopEage="18" BottomEage="18" Scale9OriginX="18" Scale9OriginY="18" Scale9Width="41" Scale9Height="21" ctype="ImageViewObjectData">
                    <Size X="97.0000" Y="57.0000" />
                    <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
                    <Position X="-15.0000" Y="131.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition />
                    <PreSize X="0.0000" Y="0.0000" />
                    <FileData Type="MarkedSubImage" Path="jinhua/res/table/chat_bubble_2.png" Plist="jinhua/res/table.plist" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint />
                <Position X="879.0000" Y="424.5120" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.9156" Y="0.6633" />
                <PreSize X="0.0000" Y="0.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="seat4" ActionTag="1060329961" Tag="1835" IconVisible="True" PositionPercentYEnabled="True" HorizontalEdge="LeftEdge" LeftMargin="81.0000" RightMargin="879.0000" TopMargin="215.4880" BottomMargin="424.5120" ctype="SingleNodeObjectData">
                <Size X="0.0000" Y="0.0000" />
                <Children>
                  <AbstractNodeData Name="chat_bubble" ActionTag="1799688637" Tag="2611" IconVisible="False" LeftMargin="15.0000" RightMargin="-112.0000" TopMargin="-159.5000" BottomMargin="102.5000" Scale9Enable="True" LeftEage="38" RightEage="18" TopEage="18" BottomEage="18" Scale9OriginX="38" Scale9OriginY="18" Scale9Width="41" Scale9Height="21" ctype="ImageViewObjectData">
                    <Size X="97.0000" Y="57.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="15.0000" Y="131.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition />
                    <PreSize X="0.0000" Y="0.0000" />
                    <FileData Type="MarkedSubImage" Path="jinhua/res/table/chat_bubble_1.png" Plist="jinhua/res/table.plist" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint />
                <Position X="81.0000" Y="424.5120" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0844" Y="0.6633" />
                <PreSize X="0.0000" Y="0.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="seat5" ActionTag="-34371994" Tag="1837" IconVisible="True" PositionPercentYEnabled="True" HorizontalEdge="LeftEdge" LeftMargin="81.0000" RightMargin="879.0000" TopMargin="392.0640" BottomMargin="247.9360" ctype="SingleNodeObjectData">
                <Size X="0.0000" Y="0.0000" />
                <Children>
                  <AbstractNodeData Name="chat_bubble" ActionTag="1594349871" Tag="2610" IconVisible="False" LeftMargin="15.0000" RightMargin="-112.0000" TopMargin="-159.5000" BottomMargin="102.5000" Scale9Enable="True" LeftEage="38" RightEage="18" TopEage="18" BottomEage="18" Scale9OriginX="38" Scale9OriginY="18" Scale9Width="41" Scale9Height="21" ctype="ImageViewObjectData">
                    <Size X="97.0000" Y="57.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="15.0000" Y="131.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition />
                    <PreSize X="0.0000" Y="0.0000" />
                    <FileData Type="MarkedSubImage" Path="jinhua/res/table/chat_bubble_1.png" Plist="jinhua/res/table.plist" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint />
                <Position X="81.0000" Y="247.9360" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0844" Y="0.3874" />
                <PreSize X="0.0000" Y="0.0000" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="1.0000" Y="1.0000" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="tip_1" ActionTag="-1814761182" Tag="933" IconVisible="False" PositionPercentXEnabled="True" VerticalEdge="BottomEdge" LeftMargin="261.0000" RightMargin="261.0000" TopMargin="500.0000" BottomMargin="90.0000" ctype="SpriteObjectData">
            <Size X="438.0000" Y="50.0000" />
            <Children>
              <AbstractNodeData Name="Text_86" ActionTag="1938941737" Tag="2215" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="43.0000" RightMargin="43.0000" TopMargin="8.5000" BottomMargin="8.5000" FontSize="32" LabelText="游戏已开始，请等待下局" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="352.0000" Y="33.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="219.0000" Y="25.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5000" />
                <PreSize X="0.8037" Y="0.6600" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="480.0000" Y="115.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.1797" />
            <PreSize X="0.4563" Y="0.0781" />
            <FileData Type="Normal" Path="jinhua/res/table/tip_bg.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="tip_txt_bg" ActionTag="326836549" Tag="2213" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="261.0000" RightMargin="261.0000" TopMargin="240.9758" BottomMargin="349.0242" ctype="SpriteObjectData">
            <Size X="438.0000" Y="50.0000" />
            <Children>
              <AbstractNodeData Name="tip_txt" ActionTag="1085757097" Tag="2214" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="129.0000" RightMargin="129.0000" TopMargin="10.0000" BottomMargin="10.0000" FontSize="30" LabelText="等待游戏开始" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="180.0000" Y="30.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="219.0000" Y="25.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5000" />
                <PreSize X="0.4110" Y="0.6000" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="480.0000" Y="374.0242" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5844" />
            <PreSize X="0.4563" Y="0.0781" />
            <FileData Type="Normal" Path="jinhua/res/table/tip_bg.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="mike_btn" ActionTag="-2035251416" VisibleForFrame="False" Tag="761" IconVisible="False" HorizontalEdge="RightEdge" VerticalEdge="BottomEdge" LeftMargin="989.7329" RightMargin="-103.7329" TopMargin="544.9924" BottomMargin="21.0076" ctype="SpriteObjectData">
            <Size X="74.0000" Y="74.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1026.7329" Y="58.0076" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="1.0695" Y="0.0906" />
            <PreSize X="0.0771" Y="0.1156" />
            <FileData Type="Normal" Path="game_public/res/mike_normal.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="btn_record" ActionTag="-1018978857" Tag="2935" IconVisible="False" PositionPercentYEnabled="True" HorizontalEdge="RightEdge" LeftMargin="890.0005" RightMargin="0.9995" TopMargin="16.2671" BottomMargin="554.7329" ctype="SpriteObjectData">
            <Size X="69.0000" Y="69.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5030" />
            <Position X="924.5005" Y="589.4399" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.9630" Y="0.9210" />
            <PreSize X="0.0719" Y="0.1078" />
            <FileData Type="MarkedSubImage" Path="bullfighting/res/table/btn_record.png" Plist="bullfighting/res/table.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>