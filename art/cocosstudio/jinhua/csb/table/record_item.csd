<GameFile>
  <PropertyGroup Name="record_item" Type="Node" ID="5e144285-e85a-42ee-9c41-0fdb7c1fab8c" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="555" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="name" ActionTag="-1151183794" Tag="573" IconVisible="False" LeftMargin="-229.4184" RightMargin="150.4184" TopMargin="12.2546" BottomMargin="-38.2546" FontSize="22" LabelText="我是谁.." ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="79.0000" Y="26.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="-229.4184" Y="-25.2546" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="229" G="229" B="229" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="cardtype" ActionTag="1143204201" Tag="574" IconVisible="False" LeftMargin="-80.6414" RightMargin="14.6414" TopMargin="61.5640" BottomMargin="-87.5640" FontSize="22" LabelText="同花顺" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="66.0000" Y="26.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-47.6414" Y="-74.5640" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="229" G="229" B="229" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="score" ActionTag="313302744" Tag="575" IconVisible="False" LeftMargin="-327.2276" RightMargin="240.2276" TopMargin="6.4678" BottomMargin="-32.4678" FontSize="22" LabelText="+300000" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="87.0000" Y="26.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-283.7276" Y="-19.4678" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="229" G="229" B="229" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="card_1" ActionTag="-363704946" Tag="576" IconVisible="True" LeftMargin="-203.7835" RightMargin="203.7835" TopMargin="111.4687" BottomMargin="-111.4687" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="-203.7835" Y="-111.4687" />
            <Scale ScaleX="0.9000" ScaleY="0.9000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="jinhua/csb/table/card.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="card_2" ActionTag="-1396608825" Tag="581" IconVisible="True" LeftMargin="-159.6622" RightMargin="159.6622" TopMargin="111.4687" BottomMargin="-111.4687" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="-159.6622" Y="-111.4687" />
            <Scale ScaleX="0.9000" ScaleY="0.9000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="jinhua/csb/table/card.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="card_3" ActionTag="1506801392" Tag="586" IconVisible="True" LeftMargin="-117.5407" RightMargin="117.5407" TopMargin="111.4687" BottomMargin="-111.4687" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="-117.5407" Y="-111.4687" />
            <Scale ScaleX="0.9000" ScaleY="0.9000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="jinhua/csb/table/card.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="head" ActionTag="-526623346" Tag="1444" IconVisible="True" LeftMargin="-283.7281" RightMargin="283.7281" TopMargin="75.3727" BottomMargin="-75.3727" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="-283.7281" Y="-75.3727" />
            <Scale ScaleX="0.7000" ScaleY="0.7000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="game_public/csb/head.csd" Plist="" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>