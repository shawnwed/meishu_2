<GameFile>
  <PropertyGroup Name="allin" Type="Layer" ID="42414c88-f4f1-44ea-921a-7635093e4f5b" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="30" Speed="1.0000">
        <Timeline ActionTag="911379043" Property="FileData">
          <TextureFrame FrameIndex="0" Tween="False">
            <TextureFile Type="MarkedSubImage" Path="jinhua/res/table/deal_bg.png" Plist="jinhua/res/table.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="10" Tween="False">
            <TextureFile Type="MarkedSubImage" Path="jinhua/res/table/allin2.png" Plist="jinhua/res/table.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="20" Tween="False">
            <TextureFile Type="MarkedSubImage" Path="jinhua/res/table/allin3.png" Plist="jinhua/res/table.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="30" Tween="False">
            <TextureFile Type="MarkedSubImage" Path="jinhua/res/table/allin1.png" Plist="jinhua/res/table.plist" />
          </TextureFrame>
        </Timeline>
      </Animation>
      <ObjectData Name="Layer" Tag="4388" ctype="GameLayerObjectData">
        <Size X="160.0000" Y="160.0000" />
        <Children>
          <AbstractNodeData Name="Sprite_1" ActionTag="911379043" Tag="6249" IconVisible="False" LeftMargin="57.0000" RightMargin="57.0000" TopMargin="57.0000" BottomMargin="57.0000" ctype="SpriteObjectData">
            <Size X="120.0000" Y="120.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="80.0000" Y="80.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="0.2875" Y="0.2875" />
            <FileData Type="MarkedSubImage" Path="jinhua/res/table/deal_bg.png" Plist="jinhua/res/table.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>