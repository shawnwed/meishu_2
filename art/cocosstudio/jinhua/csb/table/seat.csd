<GameFile>
  <PropertyGroup Name="seat" Type="Node" ID="39b780e2-ba02-4d36-b7cf-e430f25d40a4" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="534" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="empty_seat" ActionTag="405950549" Alpha="127" Tag="556" IconVisible="False" LeftMargin="-60.0000" RightMargin="-60.0000" TopMargin="-120.0000" ctype="SpriteObjectData">
            <Size X="120.0000" Y="120.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position Y="60.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="jinhua/res/table/empty_seat.png" Plist="jinhua/res/table.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="bg_seat" ActionTag="200545502" Tag="557" IconVisible="False" LeftMargin="-57.5000" RightMargin="-57.5000" TopMargin="-117.5000" BottomMargin="2.5000" ctype="SpriteObjectData">
            <Size X="115.0000" Y="115.0000" />
            <Children>
              <AbstractNodeData Name="head" ActionTag="-2088741995" Tag="562" IconVisible="True" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="57.5000" RightMargin="57.5000" TopMargin="57.5000" BottomMargin="57.5000" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint />
                <Position X="57.5000" Y="57.5000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5000" />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="Normal" Path="game_public/csb/head.csd" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="coin" ActionTag="811531756" Tag="565" IconVisible="False" LeftMargin="-18.5017" RightMargin="-27.4983" TopMargin="95.0022" BottomMargin="-38.0022" ctype="SpriteObjectData">
                <Size X="161.0000" Y="58.0000" />
                <Children>
                  <AbstractNodeData Name="coin_icon" ActionTag="1224345624" Tag="566" IconVisible="False" LeftMargin="14.3127" RightMargin="116.6873" TopMargin="27.5958" BottomMargin="-2.5958" ctype="SpriteObjectData">
                    <Size X="30.0000" Y="33.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="29.3127" Y="13.9042" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.1821" Y="0.2397" />
                    <PreSize X="0.1863" Y="0.5690" />
                    <FileData Type="MarkedSubImage" Path="game_public/res/coin_small_icon.png" Plist="game_public/res/game.plist" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="txt" ActionTag="898573187" Tag="567" IconVisible="False" LeftMargin="45.7324" RightMargin="35.2676" TopMargin="29.2224" BottomMargin="2.7776" FontSize="22" LabelText="1234.56" HorizontalAlignmentType="HT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="80.0000" Y="26.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="85.7324" Y="15.7776" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="227" B="74" />
                    <PrePosition X="0.5325" Y="0.2720" />
                    <PreSize X="0.4969" Y="0.4483" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="61.9983" Y="-9.0022" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5391" Y="-0.0783" />
                <PreSize X="1.4000" Y="0.5043" />
                <FileData Type="MarkedSubImage" Path="landlord/res/table/landlord_score_bg_2.png" Plist="landlord/res/landlord.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="name" ActionTag="1357509793" Tag="568" IconVisible="False" LeftMargin="13.0009" RightMargin="5.9991" TopMargin="96.2716" BottomMargin="-9.2716" FontSize="24" LabelText="洞房不败" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="96.0000" Y="28.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="61.0009" Y="4.7284" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5304" Y="0.0411" />
                <PreSize X="0.8348" Y="0.2435" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="allin" ActionTag="80621467" VisibleForFrame="False" Tag="559" IconVisible="True" LeftMargin="-22.5000" RightMargin="-22.5000" TopMargin="-22.5000" BottomMargin="-22.5000" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
                <Size X="160.0000" Y="160.0000" />
                <AnchorPoint />
                <Position X="-22.5000" Y="-22.5000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="-0.1957" Y="-0.1957" />
                <PreSize X="1.3913" Y="1.3913" />
                <FileData Type="Normal" Path="jinhua/csb/table/allin.csd" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="dissolve" ActionTag="1616358475" Tag="71" IconVisible="False" LeftMargin="5.0000" RightMargin="5.0000" TopMargin="39.0000" BottomMargin="36.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="153" ComboBoxIndex="1" ColorAngle="90.0000" ctype="PanelObjectData">
                <Size X="105.0000" Y="40.0000" />
                <Children>
                  <AbstractNodeData Name="txt" ActionTag="347949748" Tag="72" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="15.0000" RightMargin="15.0000" TopMargin="5.5000" BottomMargin="5.5000" FontSize="25" LabelText="投票中" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="75.0000" Y="29.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="52.5000" Y="20.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="0.7143" Y="0.7250" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint />
                <Position X="5.0000" Y="36.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0435" Y="0.3130" />
                <PreSize X="0.9130" Y="0.3478" />
                <SingleColor A="255" R="0" G="0" B="0" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="win" ActionTag="289732147" VisibleForFrame="False" Tag="558" IconVisible="False" LeftMargin="36.5000" RightMargin="31.5000" TopMargin="86.5000" BottomMargin="13.5000" LabelText="+1000万" ctype="TextBMFontObjectData">
                <Size X="47.0000" Y="15.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="60.0000" Y="21.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5217" Y="0.1826" />
                <PreSize X="0.4087" Y="0.1304" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position Y="60.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="jinhua/res/table/bg_seat.png" Plist="jinhua/res/table.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>