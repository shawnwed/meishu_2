<GameFile>
  <PropertyGroup Name="pokers_my" Type="Node" ID="7605e591-a8dd-4164-9c9b-d6cf17da0152" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="43" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="poker_1" ActionTag="-283986340" Tag="234" IconVisible="True" LeftMargin="-60.0004" RightMargin="60.0004" TopMargin="0.0001" BottomMargin="-0.0001" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="-60.0004" Y="-0.0001" />
            <Scale ScaleX="1.8000" ScaleY="2.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="jinhua/csb/table/card.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="poker_2" ActionTag="-1505040497" Tag="227" IconVisible="True" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.8000" ScaleY="2.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="jinhua/csb/table/card.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="poker_3" ActionTag="-165574490" Tag="220" IconVisible="True" LeftMargin="58.9984" RightMargin="-58.9984" TopMargin="0.0001" BottomMargin="-0.0001" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="58.9984" Y="-0.0001" />
            <Scale ScaleX="1.8000" ScaleY="2.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="jinhua/csb/table/card.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="btn_look" ActionTag="1631670140" Tag="144" IconVisible="False" LeftMargin="-95.0016" RightMargin="-94.9984" TopMargin="-89.0006" BottomMargin="33.0006" ctype="SpriteObjectData">
            <Size X="190.0000" Y="56.0000" />
            <Children>
              <AbstractNodeData Name="txt" ActionTag="1551638538" Tag="145" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="39.0000" RightMargin="39.0000" TopMargin="14.0000" BottomMargin="14.0000" FontSize="28" LabelText="點擊看牌" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="112.0000" Y="28.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="95.0000" Y="28.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5000" />
                <PreSize X="0.5895" Y="0.5000" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-0.0016" Y="61.0006" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="jinhua/res/table/btn_look.png" Plist="jinhua/res/table.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="status_look" ActionTag="1071103151" Tag="2900" IconVisible="False" LeftMargin="-30.9999" RightMargin="-31.0001" TopMargin="-67.2040" BottomMargin="1.2040" ctype="SpriteObjectData">
            <Size X="62.0000" Y="66.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="0.0001" Y="34.2040" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Default" Path="Default/Sprite.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="chip" ActionTag="213095138" Tag="11304" IconVisible="True" TopMargin="-183.8763" BottomMargin="183.8763" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position Y="183.8763" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="jinhua/csb/table/chip_num.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="allin" ActionTag="841606213" VisibleForFrame="False" Tag="18272" IconVisible="True" TopMargin="-88.9735" BottomMargin="88.9735" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position Y="88.9735" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="jinhua/csb/table/allin_txt.csd" Plist="" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>