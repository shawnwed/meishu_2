<GameFile>
  <PropertyGroup Name="compare_border" Type="Layer" ID="c8e3f32a-163a-48ec-bcc4-3a4d4636f979" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="40" Speed="1.0000">
        <Timeline ActionTag="-1214628034" Property="FileData">
          <TextureFrame FrameIndex="0" Tween="False">
            <TextureFile Type="MarkedSubImage" Path="jinhua/res/table/compare_border_arrow1.png" Plist="jinhua/res/table.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="20" Tween="False">
            <TextureFile Type="MarkedSubImage" Path="jinhua/res/table/compare_border_arrow2.png" Plist="jinhua/res/table.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="40" Tween="False">
            <TextureFile Type="MarkedSubImage" Path="jinhua/res/table/compare_border_arrow1.png" Plist="jinhua/res/table.plist" />
          </TextureFrame>
        </Timeline>
      </Animation>
      <ObjectData Name="Layer" Tag="2697" ctype="GameLayerObjectData">
        <Size X="380.0000" Y="224.0000" />
        <Children>
          <AbstractNodeData Name="Sprite_1" ActionTag="-1214628034" Tag="4501" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="2.0001" RightMargin="333.9999" TopMargin="84.3416" BottomMargin="90.6584" ctype="SpriteObjectData">
            <Size X="44.0000" Y="49.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="24.0001" Y="115.1584" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0632" Y="0.5141" />
            <PreSize X="0.1158" Y="0.2188" />
            <FileData Type="MarkedSubImage" Path="jinhua/res/table/compare_border_arrow1.png" Plist="jinhua/res/table.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="Image_45" ActionTag="-392255887" Tag="2346" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="40.4258" RightMargin="1.5742" TopMargin="13.0000" BottomMargin="13.0000" Scale9Enable="True" LeftEage="104" RightEage="104" TopEage="69" BottomEage="69" Scale9OriginX="104" Scale9OriginY="69" Scale9Width="130" Scale9Height="60" ctype="ImageViewObjectData">
            <Size X="338.0000" Y="198.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="209.4258" Y="112.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5511" Y="0.5000" />
            <PreSize X="0.8895" Y="0.8839" />
            <FileData Type="Normal" Path="jinhua/res/table/compare_border.png" Plist="" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>