<GameFile>
  <PropertyGroup Name="private_score" Type="Node" ID="057ecb65-787b-4338-9a53-8bc2bb697cc2" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="13341" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="bg" ActionTag="-454740652" Tag="17170" IconVisible="False" LeftMargin="-342.0006" RightMargin="-4.9994" BottomMargin="-720.0000" LeftEage="114" RightEage="114" TopEage="237" BottomEage="237" Scale9OriginX="114" Scale9OriginY="237" Scale9Width="119" Scale9Height="246" ctype="ImageViewObjectData">
            <Size X="347.0000" Y="720.0000" />
            <AnchorPoint ScaleY="1.0000" />
            <Position X="-342.0006" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="jinhua/res/table/menu_right_bg.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="prev_record" ActionTag="407606066" Tag="14721" IconVisible="False" LeftMargin="-374.0007" RightMargin="1.0007" TopMargin="-0.0003" BottomMargin="-719.9997" TouchEnable="True" ClipAble="False" BackColorAlpha="0" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="373.0000" Y="720.0000" />
            <Children>
              <AbstractNodeData Name="item_1" CanEdit="False" ActionTag="-2004793128" Tag="14722" IconVisible="True" LeftMargin="372.1046" RightMargin="0.8954" TopMargin="19.8710" BottomMargin="700.1290" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint />
                <Position X="372.1046" Y="700.1290" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.9976" Y="0.9724" />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="Normal" Path="jinhua/csb/table/record_item.csd" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="item_2" CanEdit="False" ActionTag="261716793" Tag="14763" IconVisible="True" LeftMargin="372.6380" RightMargin="0.3620" TopMargin="142.3279" BottomMargin="577.6721" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint />
                <Position X="372.6380" Y="577.6721" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.9990" Y="0.8023" />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="Normal" Path="jinhua/csb/table/record_item.csd" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="item_3" CanEdit="False" ActionTag="10280133" Tag="14804" IconVisible="True" LeftMargin="372.6382" RightMargin="0.3618" TopMargin="264.7848" BottomMargin="455.2152" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint />
                <Position X="372.6382" Y="455.2152" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.9990" Y="0.6322" />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="Normal" Path="jinhua/csb/table/record_item.csd" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="item_4" CanEdit="False" ActionTag="643193319" Tag="14845" IconVisible="True" LeftMargin="372.6382" RightMargin="0.3618" TopMargin="387.2418" BottomMargin="332.7582" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint />
                <Position X="372.6382" Y="332.7582" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.9990" Y="0.4622" />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="Normal" Path="jinhua/csb/table/record_item.csd" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="item_5" CanEdit="False" ActionTag="-1191100738" Tag="14886" IconVisible="True" LeftMargin="372.6382" RightMargin="0.3618" TopMargin="509.6987" BottomMargin="210.3013" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint />
                <Position X="372.6382" Y="210.3013" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.9990" Y="0.2921" />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="Normal" Path="jinhua/csb/table/record_item.csd" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="tips" ActionTag="-1717281616" Tag="14927" IconVisible="False" LeftMargin="145.4404" RightMargin="83.5596" TopMargin="293.2039" BottomMargin="384.7961" FontSize="36" LabelText="暂无牌局" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="144.0000" Y="42.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="217.4404" Y="405.7961" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5830" Y="0.5636" />
                <PreSize X="0.3861" Y="0.0583" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position X="-374.0007" Y="-719.9997" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="score_list" ActionTag="1213632412" Tag="18372" IconVisible="False" LeftMargin="-342.0006" RightMargin="-4.9994" TopMargin="-0.0004" BottomMargin="-649.9996" TouchEnable="True" ClipAble="True" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" IsBounceEnabled="True" ScrollDirectionType="0" DirectionType="Vertical" ctype="ListViewObjectData">
            <Size X="347.0000" Y="650.0000" />
            <AnchorPoint />
            <Position X="-342.0006" Y="-649.9996" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <SingleColor A="255" R="150" G="150" B="255" />
            <FirstColor A="255" R="150" G="150" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="btn_1" ActionTag="-922272451" Tag="13545" IconVisible="False" LeftMargin="-323.1557" RightMargin="173.1557" TopMargin="658.4993" BottomMargin="-712.4993" ctype="SpriteObjectData">
            <Size X="150.0000" Y="54.0000" />
            <Children>
              <AbstractNodeData Name="txt1" ActionTag="-560275154" Tag="13543" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="25.0000" RightMargin="25.0000" TopMargin="12.5000" BottomMargin="12.5000" FontSize="25" LabelText="上局回顾" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="100.0000" Y="29.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="75.0000" Y="27.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5000" />
                <PreSize X="0.6667" Y="0.5370" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-248.1557" Y="-685.4993" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="jinhua/res/table/score_tab1_1.png" Plist="jinhua/res/table.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="btn_2" ActionTag="-1641600162" Tag="13544" IconVisible="False" LeftMargin="-173.5352" RightMargin="23.5352" TopMargin="658.5002" BottomMargin="-712.5002" ctype="SpriteObjectData">
            <Size X="150.0000" Y="54.0000" />
            <Children>
              <AbstractNodeData Name="txt2" ActionTag="863250359" Tag="13542" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="25.0000" RightMargin="25.0000" TopMargin="12.5000" BottomMargin="12.5000" FontSize="25" LabelText="牌局总计" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="100.0000" Y="29.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="75.0000" Y="27.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="146" G="146" B="146" />
                <PrePosition X="0.5000" Y="0.5000" />
                <PreSize X="0.6667" Y="0.5370" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-98.5352" Y="-685.5002" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="jinhua/res/table/score_tab2_2.png" Plist="jinhua/res/table.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>