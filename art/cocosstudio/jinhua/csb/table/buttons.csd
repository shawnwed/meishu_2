<GameFile>
  <PropertyGroup Name="buttons" Type="Node" ID="48db1f26-852f-4c35-9404-3cd521682605" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="406" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="btnparent2" Visible="False" ActionTag="371455277" Tag="590" IconVisible="True" ctype="SingleNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <Children>
              <AbstractNodeData Name="bg" ActionTag="-628346624" Tag="1530" IconVisible="False" LeftMargin="-300.9981" RightMargin="-99.0019" TopMargin="-170.1550" BottomMargin="49.1550" ctype="SpriteObjectData">
                <Size X="400.0000" Y="121.0000" />
                <AnchorPoint ScaleX="0.5000" />
                <Position X="-100.9981" Y="49.1550" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="Normal" Path="jinhua/res/table/add_bg.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="btn3" ActionTag="1842633302" Tag="593" IconVisible="False" LeftMargin="-275.9979" RightMargin="165.9979" TopMargin="-170.7199" BottomMargin="60.7199" ctype="SpriteObjectData">
                <Size X="110.0000" Y="110.0000" />
                <Children>
                  <AbstractNodeData Name="txt" ActionTag="262764554" Tag="594" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="47.0000" RightMargin="47.0000" TopMargin="38.5000" BottomMargin="38.5000" FontSize="32" LabelText="8" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="16.0000" Y="33.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="55.0000" Y="55.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="0.1455" Y="0.3000" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="-220.9979" Y="115.7199" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="MarkedSubImage" Path="jinhua/lan/cn/btn_bet.png" Plist="jinhua/lan/cn/game.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="btn2" ActionTag="1556568671" Tag="595" IconVisible="False" LeftMargin="-158.8583" RightMargin="48.8583" TopMargin="-170.7199" BottomMargin="60.7199" ctype="SpriteObjectData">
                <Size X="110.0000" Y="110.0000" />
                <Children>
                  <AbstractNodeData Name="txt" ActionTag="1885084895" Tag="596" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="47.0000" RightMargin="47.0000" TopMargin="38.5000" BottomMargin="38.5000" FontSize="32" LabelText="4" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="16.0000" Y="33.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="55.0000" Y="55.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="0.1455" Y="0.3000" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="-103.8583" Y="115.7199" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="MarkedSubImage" Path="jinhua/lan/cn/btn_bet.png" Plist="jinhua/lan/cn/game.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="btn1" ActionTag="-519953706" Tag="597" IconVisible="False" LeftMargin="-35.9988" RightMargin="-74.0012" TopMargin="-170.7199" BottomMargin="60.7199" ctype="SpriteObjectData">
                <Size X="110.0000" Y="110.0000" />
                <Children>
                  <AbstractNodeData Name="txt" ActionTag="321319652" Tag="598" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="47.0000" RightMargin="47.0000" TopMargin="38.5000" BottomMargin="38.5000" FontSize="32" LabelText="2" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="16.0000" Y="33.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="55.0000" Y="55.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="0.1455" Y="0.3000" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="19.0012" Y="115.7199" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="MarkedSubImage" Path="jinhua/lan/cn/btn_bet.png" Plist="jinhua/lan/cn/game.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="btnparent1" Visible="False" ActionTag="1828613412" Tag="410" IconVisible="True" ctype="SingleNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <Children>
              <AbstractNodeData Name="btn1" ActionTag="615674506" Tag="411" IconVisible="False" LeftMargin="-191.8555" RightMargin="-2.1445" TopMargin="-42.5013" BottomMargin="-42.4987" ctype="SpriteObjectData">
                <Size X="194.0000" Y="85.0000" />
                <Children>
                  <AbstractNodeData Name="hide" ActionTag="185135798" VisibleForFrame="False" Tag="2182" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="79.0000" RightMargin="79.0000" TopMargin="32.5000" BottomMargin="32.5000" ctype="SpriteObjectData">
                    <Size X="36.0000" Y="20.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="97.0000" Y="42.5000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="0.1856" Y="0.2353" />
                    <FileData Type="MarkedSubImage" Path="jinhua/res/table/hide.png" Plist="jinhua/res/table.plist" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="-94.8555" Y="0.0013" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="MarkedSubImage" Path="jinhua/lan/cn/btn_add.png" Plist="jinhua/lan/cn/game.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="btn2" ActionTag="-104935382" Tag="412" IconVisible="False" LeftMargin="-398.1226" RightMargin="204.1226" TopMargin="-42.5002" BottomMargin="-42.4998" ctype="SpriteObjectData">
                <Size X="194.0000" Y="85.0000" />
                <Children>
                  <AbstractNodeData Name="txt" ActionTag="325282511" Tag="417" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="87.5120" RightMargin="49.4880" TopMargin="17.9990" BottomMargin="30.0010" FontSize="37" LabelText="100" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                    <Size X="57.0000" Y="37.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="116.0120" Y="48.5010" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5980" Y="0.5706" />
                    <PreSize X="0.2938" Y="0.4353" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="100" G="100" B="100" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="-301.1226" Y="0.0002" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="MarkedSubImage" Path="jinhua/lan/cn/btn_follow.png" Plist="jinhua/lan/cn/game.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="btn3" ActionTag="1609405448" Tag="413" IconVisible="False" LeftMargin="-810.6567" RightMargin="616.6567" TopMargin="-42.5002" BottomMargin="-42.4998" ctype="SpriteObjectData">
                <Size X="194.0000" Y="85.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="-713.6567" Y="0.0002" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="MarkedSubImage" Path="jinhua/lan/cn/btn_allin.png" Plist="jinhua/lan/cn/game.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="btn4" ActionTag="-1262238750" Tag="414" IconVisible="False" LeftMargin="-1016.9237" RightMargin="822.9237" TopMargin="-42.5003" BottomMargin="-42.4997" ctype="SpriteObjectData">
                <Size X="194.0000" Y="85.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="-919.9237" Y="0.0003" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="MarkedSubImage" Path="jinhua/lan/cn/btn_quit.png" Plist="jinhua/lan/cn/game.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="btn5" ActionTag="-756373641" Tag="415" IconVisible="False" LeftMargin="-604.3896" RightMargin="410.3896" TopMargin="-42.5002" BottomMargin="-42.4998" ctype="SpriteObjectData">
                <Size X="194.0000" Y="85.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="-507.3896" Y="0.0002" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="MarkedSubImage" Path="jinhua/lan/cn/btn_compare.png" Plist="jinhua/lan/cn/game.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="follow_end" ActionTag="1354244097" VisibleForFrame="False" Tag="4202" IconVisible="False" LeftMargin="-403.6226" RightMargin="209.6226" TopMargin="-42.5013" BottomMargin="-42.4987" ctype="SpriteObjectData">
                <Size X="194.0000" Y="85.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="-306.6226" Y="0.0013" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="MarkedSubImage" Path="jinhua/lan/cn/btn_auto_follow_no.png" Plist="jinhua/lan/cn/game.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="btnparent3" ActionTag="949912406" Tag="1525" IconVisible="True" ctype="SingleNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <Children>
              <AbstractNodeData Name="btn2" ActionTag="1494934772" Tag="1204" IconVisible="False" LeftMargin="-333.3246" RightMargin="287.3246" TopMargin="-23.0002" BottomMargin="-22.9998" ctype="SpriteObjectData">
                <Size X="46.0000" Y="46.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="-310.3246" Y="0.0002" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="Default" Path="Default/Sprite.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="btn3" ActionTag="-98149224" Tag="1202" IconVisible="False" LeftMargin="-129.9982" RightMargin="83.9982" TopMargin="-23.9070" BottomMargin="-22.0930" ctype="SpriteObjectData">
                <Size X="46.0000" Y="46.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="-106.9982" Y="0.9070" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="Default" Path="Default/Sprite.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="btn_invite" ActionTag="-1435744416" Tag="14116" IconVisible="False" LeftMargin="-407.3246" RightMargin="213.3246" TopMargin="-42.5002" BottomMargin="-42.4998" ctype="SpriteObjectData">
                <Size X="194.0000" Y="85.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="-310.3246" Y="0.0002" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="MarkedSubImage" Path="jinhua/lan/cn/btn_invite.png" Plist="jinhua/lan/cn/game.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="btn1" ActionTag="-1415719999" Tag="1526" IconVisible="False" LeftMargin="-129.9982" RightMargin="83.9982" TopMargin="-23.9070" BottomMargin="-22.0930" ctype="SpriteObjectData">
                <Size X="194.0000" Y="85.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="-106.9982" Y="0.9070" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="MarkedSubImage" Path="jinhua/lan/cn/btn_ready.png" Plist="jinhua/lan/cn/game.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="btnparent4" Visible="False" ActionTag="-1126107177" Tag="3636" IconVisible="True" LeftMargin="-1.9996" RightMargin="1.9996" TopMargin="3.9998" BottomMargin="-3.9998" ctype="SingleNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <Children>
              <AbstractNodeData Name="btn1" ActionTag="-896566717" Tag="3639" IconVisible="False" LeftMargin="-202.7051" RightMargin="8.7051" TopMargin="-48.7436" BottomMargin="-36.2564" ctype="SpriteObjectData">
                <Size X="194.0000" Y="85.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="-105.7051" Y="6.2436" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="MarkedSubImage" Path="jinhua/lan/cn/btn_show.png" Plist="jinhua/lan/cn/game.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="btn2" ActionTag="1848364210" Tag="3643" IconVisible="False" LeftMargin="-200.2051" RightMargin="6.2051" TopMargin="-48.7436" BottomMargin="-36.2564" ctype="SpriteObjectData">
                <Size X="194.0000" Y="85.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="-103.2051" Y="6.2436" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="MarkedSubImage" Path="jinhua/lan/cn/btn_show_no.png" Plist="jinhua/lan/cn/game.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position X="-1.9996" Y="-3.9998" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>