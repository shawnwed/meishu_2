<GameFile>
  <PropertyGroup Name="chip" Type="Node" ID="09e8019c-f355-45b8-8fbf-03630378bf7e" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="1521" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="bg" ActionTag="1165825946" Tag="1522" IconVisible="False" LeftMargin="-28.5000" RightMargin="-28.5000" TopMargin="-31.0000" BottomMargin="-31.0000" ctype="SpriteObjectData">
            <Size X="57.0000" Y="62.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="jinhua/res/table/chip1.png" Plist="jinhua/res/table.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="chip_circle" ActionTag="-2139579186" Tag="16368" IconVisible="False" LeftMargin="-37.6381" RightMargin="-37.3619" TopMargin="-42.2834" BottomMargin="-37.7166" ctype="SpriteObjectData">
            <Size X="75.0000" Y="80.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-0.1381" Y="2.2834" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="jinhua/res/table/chip_circle1.png" Plist="jinhua/res/table.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>