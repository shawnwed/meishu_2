<GameFile>
  <PropertyGroup Name="compare_border1" Type="Layer" ID="63160a69-00ba-4012-9ce5-bb16ba59205f" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="40" Speed="1.0000">
        <Timeline ActionTag="1929490364" Property="FileData">
          <TextureFrame FrameIndex="0" Tween="False">
            <TextureFile Type="MarkedSubImage" Path="jinhua/res/table/compare_border_arrow1.png" Plist="jinhua/res/table.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="20" Tween="False">
            <TextureFile Type="MarkedSubImage" Path="jinhua/res/table/compare_border_arrow2.png" Plist="jinhua/res/table.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="40" Tween="False">
            <TextureFile Type="MarkedSubImage" Path="jinhua/res/table/compare_border_arrow1.png" Plist="jinhua/res/table.plist" />
          </TextureFrame>
        </Timeline>
      </Animation>
      <ObjectData Name="Layer" Tag="2697" ctype="GameLayerObjectData">
        <Size X="380.0000" Y="224.0000" />
        <Children>
          <AbstractNodeData Name="Sprite_1" ActionTag="1929490364" Tag="4505" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="331.4932" RightMargin="4.5068" TopMargin="87.4999" BottomMargin="87.5001" FlipX="True" ctype="SpriteObjectData">
            <Size X="44.0000" Y="49.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="353.4932" Y="112.0001" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.9302" Y="0.5000" />
            <PreSize X="0.1158" Y="0.2188" />
            <FileData Type="MarkedSubImage" Path="jinhua/res/table/compare_border_arrow1.png" Plist="jinhua/res/table.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="Image_45" ActionTag="542960250" Tag="2347" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="-3.5751" RightMargin="45.5751" TopMargin="12.9999" BottomMargin="13.0001" Scale9Enable="True" LeftEage="104" RightEage="104" TopEage="69" BottomEage="69" Scale9OriginX="104" Scale9OriginY="69" Scale9Width="130" Scale9Height="60" ctype="ImageViewObjectData">
            <Size X="338.0000" Y="198.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="165.4249" Y="112.0001" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4353" Y="0.5000" />
            <PreSize X="0.8895" Y="0.8839" />
            <FileData Type="Normal" Path="jinhua/res/table/compare_border.png" Plist="" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>