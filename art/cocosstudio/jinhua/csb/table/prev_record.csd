<GameFile>
  <PropertyGroup Name="prev_record" Type="Node" ID="74b7319b-7585-46b5-b3d2-c73f4d7bc8e3" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="503" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="bg" ActionTag="1356524907" Tag="17371" IconVisible="False" RightMargin="-380.0000" BottomMargin="-720.0000" FlipX="True" LeftEage="114" RightEage="114" TopEage="237" BottomEage="237" Scale9OriginX="114" Scale9OriginY="237" Scale9Width="119" Scale9Height="246" ctype="ImageViewObjectData">
            <Size X="380.0000" Y="720.0000" />
            <AnchorPoint ScaleY="1.0000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="jinhua/res/table/menu_right_bg.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="cards_type" CanEdit="False" ActionTag="2067329437" Tag="514" IconVisible="False" LeftMargin="-373.0000" TopMargin="-1.0000" BottomMargin="-719.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="0" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="373.0000" Y="720.0000" />
            <Children>
              <AbstractNodeData Name="pokerType_1" ActionTag="-134063717" Tag="233" IconVisible="False" LeftMargin="-0.6161" RightMargin="0.6161" TopMargin="46.1522" BottomMargin="286.8478" ctype="SpriteObjectData">
                <Size X="373.0000" Y="387.0000" />
                <AnchorPoint ScaleX="1.0000" ScaleY="1.0000" />
                <Position X="372.3839" Y="673.8478" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.9983" Y="0.9359" />
                <PreSize X="1.0000" Y="0.5375" />
                <FileData Type="Normal" Path="jinhua/res/table/pokerType.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="txt1" ActionTag="-1221967737" Tag="234" IconVisible="False" LeftMargin="255.5234" RightMargin="61.4766" TopMargin="52.2561" BottomMargin="635.7439" FontSize="28" LabelText="豹子" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="56.0000" Y="32.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="283.5234" Y="651.7439" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7601" Y="0.9052" />
                <PreSize X="0.1501" Y="0.0444" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="txt1_0" ActionTag="376305208" Tag="235" IconVisible="False" LeftMargin="241.5231" RightMargin="47.4769" TopMargin="118.9093" BottomMargin="569.0907" FontSize="28" LabelText="同花顺" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="84.0000" Y="32.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="283.5231" Y="585.0907" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7601" Y="0.8126" />
                <PreSize X="0.2252" Y="0.0444" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="txt1_1" ActionTag="-1222628365" Tag="236" IconVisible="False" LeftMargin="255.5234" RightMargin="61.4766" TopMargin="185.5618" BottomMargin="502.4382" FontSize="28" LabelText="同花" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="56.0000" Y="32.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="283.5234" Y="518.4382" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7601" Y="0.7201" />
                <PreSize X="0.1501" Y="0.0444" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="txt1_2" ActionTag="-1527434499" Tag="237" IconVisible="False" LeftMargin="255.5234" RightMargin="61.4766" TopMargin="252.2150" BottomMargin="435.7850" FontSize="28" LabelText="顺子" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="56.0000" Y="32.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="283.5234" Y="451.7850" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7601" Y="0.6275" />
                <PreSize X="0.1501" Y="0.0444" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="txt1_3" ActionTag="707513849" Tag="238" IconVisible="False" LeftMargin="255.5234" RightMargin="61.4766" TopMargin="318.8692" BottomMargin="369.1308" FontSize="28" LabelText="对子" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="56.0000" Y="32.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="283.5234" Y="385.1308" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7601" Y="0.5349" />
                <PreSize X="0.1501" Y="0.0444" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="txt1_4" ActionTag="1800925872" Tag="239" IconVisible="False" LeftMargin="255.5234" RightMargin="61.4766" TopMargin="385.5221" BottomMargin="302.4779" FontSize="28" LabelText="单张" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="56.0000" Y="32.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="283.5234" Y="318.4779" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7601" Y="0.4423" />
                <PreSize X="0.1501" Y="0.0444" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position X="-373.0000" Y="-719.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="prev_record" Visible="False" ActionTag="1806102453" Tag="537" IconVisible="False" LeftMargin="-373.0000" BottomMargin="-720.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="0" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="373.0000" Y="720.0000" />
            <Children>
              <AbstractNodeData Name="item_1" ActionTag="1473454219" Tag="601" IconVisible="True" LeftMargin="372.1046" RightMargin="0.8954" TopMargin="19.8710" BottomMargin="700.1290" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint />
                <Position X="372.1046" Y="700.1290" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.9976" Y="0.9724" />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="Normal" Path="jinhua/csb/table/record_item.csd" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="item_2" ActionTag="2031947757" Tag="635" IconVisible="True" LeftMargin="372.6380" RightMargin="0.3620" TopMargin="142.3279" BottomMargin="577.6721" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint />
                <Position X="372.6380" Y="577.6721" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.9990" Y="0.8023" />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="Normal" Path="jinhua/csb/table/record_item.csd" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="item_3" ActionTag="-15903914" Tag="8260" IconVisible="True" LeftMargin="372.6382" RightMargin="0.3618" TopMargin="264.7848" BottomMargin="455.2152" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint />
                <Position X="372.6382" Y="455.2152" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.9990" Y="0.6322" />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="Normal" Path="jinhua/csb/table/record_item.csd" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="item_4" ActionTag="696612581" Tag="8281" IconVisible="True" LeftMargin="372.6382" RightMargin="0.3618" TopMargin="387.2418" BottomMargin="332.7582" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint />
                <Position X="372.6382" Y="332.7582" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.9990" Y="0.4622" />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="Normal" Path="jinhua/csb/table/record_item.csd" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="item_5" ActionTag="2051320494" Tag="8302" IconVisible="True" LeftMargin="372.6382" RightMargin="0.3618" TopMargin="509.6987" BottomMargin="210.3013" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint />
                <Position X="372.6382" Y="210.3013" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.9990" Y="0.2921" />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="Normal" Path="jinhua/csb/table/record_item.csd" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="tips" ActionTag="-1474365026" Tag="775" IconVisible="False" LeftMargin="113.7003" RightMargin="115.2997" TopMargin="297.2611" BottomMargin="380.7389" FontSize="36" LabelText="暂无牌局" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="144.0000" Y="42.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="185.7003" Y="401.7389" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4979" Y="0.5580" />
                <PreSize X="0.3861" Y="0.0583" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position X="-373.0000" Y="-720.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="btn_2" ActionTag="324119924" Tag="14517" IconVisible="False" LeftMargin="-184.5342" RightMargin="34.5342" TopMargin="658.5004" BottomMargin="-712.5004" ctype="SpriteObjectData">
            <Size X="150.0000" Y="54.0000" />
            <Children>
              <AbstractNodeData Name="txt2" ActionTag="981629929" Tag="14518" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="25.0000" RightMargin="25.0000" TopMargin="12.5000" BottomMargin="12.5000" FontSize="25" LabelText="上局回顾" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="100.0000" Y="29.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="75.0000" Y="27.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="146" G="146" B="146" />
                <PrePosition X="0.5000" Y="0.5000" />
                <PreSize X="0.6667" Y="0.5370" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-109.5342" Y="-685.5004" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="jinhua/res/table/score_tab2_2.png" Plist="jinhua/res/table.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="btn_1" ActionTag="-2138879440" Tag="14519" IconVisible="False" LeftMargin="-334.1554" RightMargin="184.1554" TopMargin="658.4999" BottomMargin="-712.4999" ctype="SpriteObjectData">
            <Size X="150.0000" Y="54.0000" />
            <Children>
              <AbstractNodeData Name="txt1" ActionTag="-2004563218" Tag="14520" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="25.0000" RightMargin="25.0000" TopMargin="12.5000" BottomMargin="12.5000" FontSize="25" LabelText="牌型介绍" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="100.0000" Y="29.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="75.0000" Y="27.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5000" />
                <PreSize X="0.6667" Y="0.5370" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-259.1554" Y="-685.4999" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="jinhua/res/table/score_tab1_1.png" Plist="jinhua/res/table.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>