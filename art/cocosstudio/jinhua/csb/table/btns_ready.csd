<GameFile>
  <PropertyGroup Name="btns_ready" Type="Node" ID="0c167784-9444-497d-bf90-71c50aadca5c" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="1016" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="start_btn" ActionTag="644663585" Tag="1026" IconVisible="False" LeftMargin="13.0000" RightMargin="-217.0000" TopMargin="-0.5000" BottomMargin="-80.5000" ctype="SpriteObjectData">
            <Size X="204.0000" Y="81.0000" />
            <Children>
              <AbstractNodeData Name="title" ActionTag="2128819188" Tag="5" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-7.0000" RightMargin="-7.0000" TopMargin="-6.5000" BottomMargin="-6.5000" ctype="SpriteObjectData">
                <Size X="218.0000" Y="94.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="102.0000" Y="40.5000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5000" />
                <PreSize X="1.0686" Y="1.1605" />
                <FileData Type="MarkedSubImage" Path="landlord/lan/cn/btn_word_ready.png" Plist="landlord/lan/cn/game.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="115.0000" Y="-40.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="landlord/res/table/landlord_btn_orange.png" Plist="landlord/res/landlord.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="change_btn" ActionTag="-2143780447" Tag="1027" IconVisible="False" LeftMargin="-217.5000" RightMargin="12.5000" TopMargin="-1.0000" BottomMargin="-81.0000" ctype="SpriteObjectData">
            <Size X="205.0000" Y="82.0000" />
            <Children>
              <AbstractNodeData Name="title" ActionTag="-1952355643" Tag="4" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-6.5000" RightMargin="-6.5000" TopMargin="-11.0000" BottomMargin="-1.0000" ctype="SpriteObjectData">
                <Size X="218.0000" Y="94.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="102.5000" Y="46.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5610" />
                <PreSize X="1.0634" Y="1.1463" />
                <FileData Type="MarkedSubImage" Path="landlord/lan/cn/btn_word_changeTable.png" Plist="landlord/lan/cn/game.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-115.0000" Y="-40.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="landlord/res/table/landlord_btn_green.png" Plist="landlord/res/landlord.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>