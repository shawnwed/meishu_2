<GameFile>
  <PropertyGroup Name="score_btn" Type="Node" ID="75233266-2679-452c-a7c6-d2a5ced969c8" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="12579" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="bg" ActionTag="-1496675378" Tag="12580" IconVisible="False" LeftMargin="10.0000" RightMargin="-212.0000" TopMargin="-36.0000" BottomMargin="-36.0000" ctype="SpriteObjectData">
            <Size X="202.0000" Y="72.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="111.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="jinhua/res/table/score_btn.png" Plist="jinhua/res/table.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="tips" ActionTag="-1545604908" VisibleForFrame="False" Tag="12581" IconVisible="False" LeftMargin="35.5000" RightMargin="-326.5000" TopMargin="30.0000" BottomMargin="-102.0000" ctype="SpriteObjectData">
            <Size X="291.0000" Y="72.0000" />
            <Children>
              <AbstractNodeData Name="txt" ActionTag="-991536183" Tag="12583" IconVisible="False" LeftMargin="13.0000" RightMargin="14.0000" TopMargin="25.0000" BottomMargin="19.0000" FontSize="24" LabelText="点击这里可查看实时战绩" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="264.0000" Y="28.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="145.0000" Y="33.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="77" G="77" B="77" />
                <PrePosition X="0.4983" Y="0.4583" />
                <PreSize X="0.9072" Y="0.3889" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="181.0000" Y="-66.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="jinhua/res/table/score_btn_tips_bg.png" Plist="jinhua/res/table.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="txt" ActionTag="349537977" Tag="12582" IconVisible="False" LeftMargin="76.0000" RightMargin="-182.0000" TopMargin="-20.0000" BottomMargin="-14.0000" FontSize="30" LabelText="10/20局" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="106.0000" Y="34.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="129.0000" Y="3.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>