<GameFile>
  <PropertyGroup Name="list_item" Type="Layer" ID="de1688ae-e84c-4c35-87ff-f2a5aabe3f8f" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="849" ctype="GameLayerObjectData">
        <Size X="320.0000" Y="425.0000" />
        <Children>
          <AbstractNodeData Name="bet_bg" ActionTag="1748281418" Tag="1221" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-13.0760" RightMargin="-11.9240" TopMargin="-36.8550" BottomMargin="24.8550" ctype="SpriteObjectData">
            <Size X="301.0000" Y="424.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="159.4240" Y="243.3550" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4982" Y="0.5726" />
            <PreSize X="1.0781" Y="1.0282" />
            <FileData Type="Normal" Path="jinhua/lan/cn/item_bg1.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="bet_icon_0" ActionTag="-864431332" VisibleForFrame="False" Tag="79" IconVisible="False" LeftMargin="62.2405" RightMargin="214.7595" TopMargin="357.6949" BottomMargin="24.3051" ctype="SpriteObjectData">
            <Size X="43.0000" Y="43.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="83.7405" Y="45.8051" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.2617" Y="0.1078" />
            <PreSize X="0.1344" Y="0.1012" />
            <FileData Type="MarkedSubImage" Path="landlord/lan/cn/icon_base_score.png" Plist="landlord/lan/cn/game.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="player_num" ActionTag="810693284" Tag="94" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="106.1760" RightMargin="125.8240" TopMargin="197.0943" BottomMargin="197.9057" ctype="SpriteObjectData">
            <Size X="88.0000" Y="30.0000" />
            <Children>
              <AbstractNodeData Name="txt" ActionTag="987079937" Tag="93" IconVisible="False" LeftMargin="31.5000" RightMargin="12.5000" TopMargin="5.0000" BottomMargin="3.0000" FontSize="22" LabelText="1000" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="44.0000" Y="22.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="53.5000" Y="14.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.6080" Y="0.4667" />
                <PreSize X="0.5000" Y="0.7333" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="150.1760" Y="212.9057" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4693" Y="0.5010" />
            <PreSize X="0.2750" Y="0.0706" />
            <FileData Type="MarkedSubImage" Path="landlord/res/roomlist/player_num_bg.png" Plist="landlord/res/roomlist/roomlist.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="count" ActionTag="-1946733340" Tag="6" IconVisible="False" LeftMargin="118.2019" RightMargin="123.7981" TopMargin="328.9219" BottomMargin="63.0781" LabelText="100" ctype="TextBMFontObjectData">
            <Size X="78.0000" Y="33.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="157.2019" Y="79.5781" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4913" Y="0.1872" />
            <PreSize X="0.2438" Y="0.0776" />
            <LabelBMFontFile_CNB Type="Normal" Path="hall/res/rank_num.fnt" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="take" ActionTag="473581337" Tag="1214" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="126.8000" RightMargin="133.2000" TopMargin="385.3814" BottomMargin="9.6186" FontSize="30" LabelText="5000" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="60.0000" Y="30.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="156.8000" Y="24.6186" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="165" G="42" B="42" />
            <PrePosition X="0.4900" Y="0.0579" />
            <PreSize X="0.1875" Y="0.0706" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>