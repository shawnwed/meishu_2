<GameFile>
  <PropertyGroup Name="main1" Type="Scene" ID="083689eb-767f-4fec-be55-3ff36d754bdf" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Scene" Tag="1846" ctype="GameNodeObjectData">
        <Size X="960.0000" Y="640.0000" />
        <Children>
          <AbstractNodeData Name="bg" ActionTag="-341959962" Tag="1866" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" LeftEage="422" RightEage="422" TopEage="237" BottomEage="237" Scale9OriginX="422" Scale9OriginY="237" Scale9Width="436" Scale9Height="246" ctype="ImageViewObjectData">
            <Size X="960.0000" Y="640.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="480.0000" Y="320.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="1.0000" Y="1.0000" />
            <FileData Type="Normal" Path="bg/bg_hall.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="contentContainor" ActionTag="1744295981" Tag="2171" IconVisible="False" PositionPercentXEnabled="True" VerticalEdge="BothEdge" LeftMargin="-120.0000" RightMargin="-120.0000" TopMargin="20.1680" BottomMargin="69.8320" TouchEnable="True" ClipAble="True" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ScrollDirectionType="0" ctype="PageViewObjectData">
            <Size X="1200.0000" Y="550.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="480.0000" Y="344.8320" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5388" />
            <PreSize X="1.2500" Y="0.8594" />
            <SingleColor A="255" R="150" G="150" B="100" />
            <FirstColor A="255" R="150" G="150" B="100" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="last" ActionTag="1808011632" Tag="132" IconVisible="False" LeftMargin="6.8341" RightMargin="914.1659" TopMargin="217.3042" BottomMargin="357.6958" FlipX="True" ctype="SpriteObjectData">
            <Size X="39.0000" Y="65.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="26.3341" Y="390.1958" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0274" Y="0.6097" />
            <PreSize X="0.0406" Y="0.1016" />
            <FileData Type="Normal" Path="game_public/res/game_room/fanye.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="next" ActionTag="1615860749" Tag="133" IconVisible="False" LeftMargin="1237.6135" RightMargin="-316.6135" TopMargin="217.3042" BottomMargin="357.6958" ctype="SpriteObjectData">
            <Size X="39.0000" Y="65.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1257.1135" Y="390.1958" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="1.3095" Y="0.6097" />
            <PreSize X="0.0406" Y="0.1016" />
            <FileData Type="Normal" Path="game_public/res/game_room/fanye.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="quick" ActionTag="926209342" Tag="696" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="457.0000" RightMargin="457.0000" TopMargin="588.0000" BottomMargin="6.0000" ctype="SpriteObjectData">
            <Size X="46.0000" Y="46.0000" />
            <AnchorPoint ScaleX="0.5000" />
            <Position X="480.0000" Y="6.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.0094" />
            <PreSize X="0.0479" Y="0.0719" />
            <FileData Type="Default" Path="Default/Sprite.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="top" ActionTag="1213614520" Tag="1867" IconVisible="True" PositionPercentXEnabled="True" PercentWidthEnable="True" PercentWidthEnabled="True" VerticalEdge="TopEdge" BottomMargin="543.0000" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="True" ctype="ProjectNodeObjectData">
            <Size X="960.0000" Y="97.0000" />
            <AnchorPoint />
            <Position Y="543.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition Y="0.8484" />
            <PreSize X="1.0000" Y="0.1516" />
            <FileData Type="Normal" Path="" Plist="" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>