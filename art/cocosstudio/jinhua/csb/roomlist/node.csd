<GameFile>
  <PropertyGroup Name="node" Type="Node" ID="6500580b-2d91-489d-bd11-f70b9ee75d25" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="503" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="bet_select_1" ActionTag="-1882213754" Tag="504" IconVisible="False" LeftMargin="-243.1174" RightMargin="-38.8826" TopMargin="-105.7350" BottomMargin="-272.2650" ctype="SpriteObjectData">
            <Size X="282.0000" Y="378.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-102.1174" Y="-83.2650" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="jinhua/res/roomlist/bet_select.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="bet_select_no_1" ActionTag="-1622470514" Tag="507" IconVisible="False" LeftMargin="-112.7214" RightMargin="-169.2786" TopMargin="-88.4536" BottomMargin="-289.5464" ctype="SpriteObjectData">
            <Size X="282.0000" Y="378.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="28.2786" Y="-100.5464" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="jinhua/res/roomlist/bet_select_no.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>