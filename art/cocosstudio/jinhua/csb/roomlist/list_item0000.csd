<GameFile>
  <PropertyGroup Name="list_item" Type="Layer" ID="c2c0d918-52db-480c-8b88-95c340bb3db0" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="849" ctype="GameLayerObjectData">
        <Size X="410.0000" Y="550.0000" />
        <Children>
          <AbstractNodeData Name="bet_bg" ActionTag="1748281418" Tag="1221" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="52.6647" RightMargin="56.3353" TopMargin="26.2996" BottomMargin="99.7004" ctype="SpriteObjectData">
            <Size X="301.0000" Y="424.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="203.1647" Y="311.7004" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4955" Y="0.5667" />
            <PreSize X="0.7341" Y="0.7709" />
            <FileData Type="Normal" Path="jinhua/lan/cn/item_bg1.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="take" ActionTag="473581337" Tag="1214" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="229.5761" RightMargin="150.4239" TopMargin="395.3677" BottomMargin="124.6323" FontSize="30" LabelText="50" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="30.0000" Y="30.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="229.5761" Y="139.6323" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="193" B="0" />
            <PrePosition X="0.5599" Y="0.2539" />
            <PreSize X="0.0732" Y="0.0545" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="count" ActionTag="1390510575" Tag="1218" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="172.8694" RightMargin="177.1306" TopMargin="450.6916" BottomMargin="69.3084" FontSize="30" LabelText="1000" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="60.0000" Y="30.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="172.8694" Y="84.3084" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="193" B="0" />
            <PrePosition X="0.4216" Y="0.1533" />
            <PreSize X="0.1463" Y="0.0545" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="bet_icon" ActionTag="-372935369" Tag="1219" IconVisible="False" LeftMargin="272.0851" RightMargin="94.9149" TopMargin="444.2769" BottomMargin="62.7231" ctype="SpriteObjectData">
            <Size X="43.0000" Y="43.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="293.5851" Y="84.2231" />
            <Scale ScaleX="0.8000" ScaleY="0.8000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.7161" Y="0.1531" />
            <PreSize X="0.1049" Y="0.0782" />
            <FileData Type="MarkedSubImage" Path="hall/res/first/coin_money.png" Plist="hall/res/first.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="txt_bet" ActionTag="-237338611" Tag="1220" IconVisible="False" LeftMargin="91.2566" RightMargin="228.7434" TopMargin="449.5031" BottomMargin="70.4969" FontSize="30" LabelText="底注：" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="90.0000" Y="30.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="136.2566" Y="85.4969" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="193" B="0" />
            <PrePosition X="0.3323" Y="0.1554" />
            <PreSize X="0.2195" Y="0.0545" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Text_1" ActionTag="-67221525" Tag="78" IconVisible="False" LeftMargin="147.6541" RightMargin="172.3459" TopMargin="395.3677" BottomMargin="124.6323" FontSize="30" LabelText="入场：" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="90.0000" Y="30.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="192.6541" Y="139.6323" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="193" B="0" />
            <PrePosition X="0.4699" Y="0.2539" />
            <PreSize X="0.2195" Y="0.0545" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="bet_icon_0" ActionTag="-864431332" Tag="79" IconVisible="False" LeftMargin="270.6624" RightMargin="96.3376" TopMargin="392.5869" BottomMargin="114.4131" ctype="SpriteObjectData">
            <Size X="43.0000" Y="43.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="292.1624" Y="135.9131" />
            <Scale ScaleX="0.8000" ScaleY="0.8000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.7126" Y="0.2471" />
            <PreSize X="0.1049" Y="0.0782" />
            <FileData Type="MarkedSubImage" Path="hall/res/first/coin_money.png" Plist="hall/res/first.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>