<GameFile>
  <PropertyGroup Name="import" Type="Node" ID="34c8ddcc-85c3-45f5-903a-55eb9c59972f" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="2644" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="bet_select_1" ActionTag="1602518771" Tag="2645" IconVisible="False" LeftMargin="-252.9141" RightMargin="-29.0859" TopMargin="-127.8377" BottomMargin="-250.1623" ctype="SpriteObjectData">
            <Size X="282.0000" Y="378.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-111.9141" Y="-61.1623" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Default" Path="Default/Sprite.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="bet_select_no_2" ActionTag="-526617282" Tag="2646" IconVisible="False" LeftMargin="-252.9141" RightMargin="-29.0859" TopMargin="-127.8377" BottomMargin="-250.1623" ctype="SpriteObjectData">
            <Size X="282.0000" Y="378.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-111.9141" Y="-61.1623" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Default" Path="Default/Sprite.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>