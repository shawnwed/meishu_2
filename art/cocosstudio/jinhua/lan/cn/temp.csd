<GameFile>
  <PropertyGroup Name="temp" Type="Scene" ID="c28f4dcf-ebf7-4f8e-9dbb-cce90fac520c" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Scene" Tag="2251" ctype="GameNodeObjectData">
        <Size X="1440.0000" Y="720.0000" />
        <Children>
          <AbstractNodeData Name="BitmapFontLabel_1" ActionTag="1851121595" Tag="2252" IconVisible="False" LeftMargin="176.8098" RightMargin="1014.1902" TopMargin="432.8761" BottomMargin="252.1239" LabelText="1234" ctype="TextBMFontObjectData">
            <Size X="89.0000" Y="35.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="221.3098" Y="269.6239" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.1729" Y="0.3745" />
            <PreSize X="0.0695" Y="0.0486" />
            <LabelBMFontFile_CNB Type="Normal" Path="jinhua/lan/cn/jinhua_lose.fnt" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="BitmapFontLabel_2" ActionTag="-1653690048" Tag="43" IconVisible="False" LeftMargin="626.0000" RightMargin="634.0000" TopMargin="492.5000" BottomMargin="192.5000" LabelText="Fnt Text Label" ctype="TextBMFontObjectData">
            <Size X="20.0000" Y="35.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="636.0000" Y="210.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4969" Y="0.2917" />
            <PreSize X="0.0156" Y="0.0486" />
            <LabelBMFontFile_CNB Type="Normal" Path="jinhua/lan/cn/jinhua_win.fnt" Plist="" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>