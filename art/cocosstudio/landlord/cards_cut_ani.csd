<GameFile>
  <PropertyGroup Name="cards_cut_ani" Type="Node" ID="a0f141ba-d33e-4bc4-a4a3-48ce5a98b5eb" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="124" Speed="1.0000">
        <Timeline ActionTag="-968253917" Property="FileData">
          <TextureFrame FrameIndex="0" Tween="False">
            <TextureFile Type="MarkedSubImage" Path="landlord/res/cards_cut/cards_cut_01.png" Plist="landlord/cards_cut.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="4" Tween="False">
            <TextureFile Type="MarkedSubImage" Path="landlord/res/cards_cut/cards_cut_02.png" Plist="landlord/cards_cut.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="8" Tween="False">
            <TextureFile Type="MarkedSubImage" Path="landlord/res/cards_cut/cards_cut_03.png" Plist="landlord/cards_cut.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="12" Tween="False">
            <TextureFile Type="MarkedSubImage" Path="landlord/res/cards_cut/cards_cut_04.png" Plist="landlord/cards_cut.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="16" Tween="False">
            <TextureFile Type="MarkedSubImage" Path="landlord/res/cards_cut/cards_cut_05.png" Plist="landlord/cards_cut.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="20" Tween="False">
            <TextureFile Type="MarkedSubImage" Path="landlord/res/cards_cut/cards_cut_06.png" Plist="landlord/cards_cut.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="24" Tween="False">
            <TextureFile Type="MarkedSubImage" Path="landlord/res/cards_cut/cards_cut_07.png" Plist="landlord/cards_cut.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="28" Tween="False">
            <TextureFile Type="MarkedSubImage" Path="landlord/res/cards_cut/cards_cut_08.png" Plist="landlord/cards_cut.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="32" Tween="False">
            <TextureFile Type="MarkedSubImage" Path="landlord/res/cards_cut/cards_cut_09.png" Plist="landlord/cards_cut.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="36" Tween="False">
            <TextureFile Type="MarkedSubImage" Path="landlord/res/cards_cut/cards_cut_10.png" Plist="landlord/cards_cut.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="40" Tween="False">
            <TextureFile Type="MarkedSubImage" Path="landlord/res/cards_cut/cards_cut_11.png" Plist="landlord/cards_cut.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="44" Tween="False">
            <TextureFile Type="MarkedSubImage" Path="landlord/res/cards_cut/cards_cut_12.png" Plist="landlord/cards_cut.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="48" Tween="False">
            <TextureFile Type="MarkedSubImage" Path="landlord/res/cards_cut/cards_cut_13.png" Plist="landlord/cards_cut.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="52" Tween="False">
            <TextureFile Type="MarkedSubImage" Path="landlord/res/cards_cut/cards_cut_14.png" Plist="landlord/cards_cut.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="56" Tween="False">
            <TextureFile Type="MarkedSubImage" Path="landlord/res/cards_cut/cards_cut_15.png" Plist="landlord/cards_cut.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="60" Tween="False">
            <TextureFile Type="MarkedSubImage" Path="landlord/res/cards_cut/cards_cut_16.png" Plist="landlord/cards_cut.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="64" Tween="False">
            <TextureFile Type="MarkedSubImage" Path="landlord/res/cards_cut/cards_cut_17.png" Plist="landlord/cards_cut.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="68" Tween="False">
            <TextureFile Type="MarkedSubImage" Path="landlord/res/cards_cut/cards_cut_18.png" Plist="landlord/cards_cut.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="72" Tween="False">
            <TextureFile Type="MarkedSubImage" Path="landlord/res/cards_cut/cards_cut_19.png" Plist="landlord/cards_cut.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="76" Tween="False">
            <TextureFile Type="MarkedSubImage" Path="landlord/res/cards_cut/cards_cut_20.png" Plist="landlord/cards_cut.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="80" Tween="False">
            <TextureFile Type="MarkedSubImage" Path="landlord/res/cards_cut/cards_cut_21.png" Plist="landlord/cards_cut.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="84" Tween="False">
            <TextureFile Type="MarkedSubImage" Path="landlord/res/cards_cut/cards_cut_22.png" Plist="landlord/cards_cut.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="88" Tween="False">
            <TextureFile Type="MarkedSubImage" Path="landlord/res/cards_cut/cards_cut_23.png" Plist="landlord/cards_cut.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="92" Tween="False">
            <TextureFile Type="MarkedSubImage" Path="landlord/res/cards_cut/cards_cut_24.png" Plist="landlord/cards_cut.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="96" Tween="False">
            <TextureFile Type="MarkedSubImage" Path="landlord/res/cards_cut/cards_cut_25.png" Plist="landlord/cards_cut.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="100" Tween="False">
            <TextureFile Type="MarkedSubImage" Path="landlord/res/cards_cut/cards_cut_26.png" Plist="landlord/cards_cut.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="104" Tween="False">
            <TextureFile Type="MarkedSubImage" Path="landlord/res/cards_cut/cards_cut_27.png" Plist="landlord/cards_cut.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="108" Tween="False">
            <TextureFile Type="MarkedSubImage" Path="landlord/res/cards_cut/cards_cut_28.png" Plist="landlord/cards_cut.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="112" Tween="False">
            <TextureFile Type="MarkedSubImage" Path="landlord/res/cards_cut/cards_cut_29.png" Plist="landlord/cards_cut.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="116" Tween="False">
            <TextureFile Type="MarkedSubImage" Path="landlord/res/cards_cut/cards_cut_30.png" Plist="landlord/cards_cut.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="120" Tween="False">
            <TextureFile Type="MarkedSubImage" Path="landlord/res/cards_cut/cards_cut_31.png" Plist="landlord/cards_cut.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="124" Tween="False">
            <TextureFile Type="MarkedSubImage" Path="landlord/res/cards_cut/cards_cut_33.png" Plist="landlord/cards_cut.plist" />
          </TextureFrame>
        </Timeline>
      </Animation>
      <ObjectData Name="Node" Tag="236" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="Sprite_1" ActionTag="-968253917" Tag="237" IconVisible="False" LeftMargin="-125.0000" RightMargin="-125.0000" TopMargin="-82.0000" BottomMargin="-82.0000" ctype="SpriteObjectData">
            <Size X="250.0000" Y="164.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="landlord/res/cards_cut/cards_cut_01.png" Plist="landlord/cards_cut.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>