<GameFile>
  <PropertyGroup Name="list_item_1" Type="Layer" ID="082e997e-eca1-41a6-948b-0f3d2aeecdcc" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="849" ctype="GameLayerObjectData">
        <Size X="320.0000" Y="425.0000" />
        <Children>
          <AbstractNodeData Name="bet_bg" ActionTag="1748281418" Tag="1221" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="137.0000" RightMargin="137.0000" TopMargin="189.5000" BottomMargin="189.5000" ctype="SpriteObjectData">
            <Size X="298.0000" Y="378.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="160.0000" Y="212.5000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="0.1437" Y="0.1082" />
            <FileData Type="MarkedSubImage" Path="landlord/lan/cn/item_bg1.png" Plist="landlord/lan/cn/game.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="take" ActionTag="473581337" Tag="1214" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="136.0000" RightMargin="136.0000" TopMargin="344.1148" BottomMargin="56.8852" FontSize="24" LabelText="5000" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="48.0000" Y="24.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="160.0000" Y="68.8852" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="239" G="220" B="137" />
            <PrePosition X="0.5000" Y="0.1621" />
            <PreSize X="0.1500" Y="0.0565" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="bet_icon_0" Visible="False" ActionTag="-864431332" Tag="79" IconVisible="False" LeftMargin="72.0154" RightMargin="204.9846" TopMargin="281.6152" BottomMargin="100.3848" ctype="SpriteObjectData">
            <Size X="43.0000" Y="43.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="93.5154" Y="121.8848" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.2922" Y="0.2868" />
            <PreSize X="0.1344" Y="0.1012" />
            <FileData Type="MarkedSubImage" Path="landlord/lan/cn/icon_base_score.png" Plist="landlord/lan/cn/game.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="count" Visible="False" ActionTag="-1946733340" Tag="6" IconVisible="False" LeftMargin="117.0000" RightMargin="57.0000" TopMargin="276.0000" BottomMargin="101.0000" LabelText="1001万" ctype="TextBMFontObjectData">
            <Size X="146.0000" Y="48.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="190.0000" Y="125.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5938" Y="0.2941" />
            <PreSize X="0.4563" Y="0.1129" />
            <LabelBMFontFile_CNB Type="Normal" Path="landlord/res/roomlist/num.fnt" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="player_num" ActionTag="810693284" Tag="94" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="116.0000" RightMargin="116.0000" TopMargin="243.0000" BottomMargin="152.0000" ctype="SpriteObjectData">
            <Size X="88.0000" Y="30.0000" />
            <Children>
              <AbstractNodeData Name="txt" ActionTag="987079937" Tag="93" IconVisible="False" LeftMargin="31.5000" RightMargin="12.5000" TopMargin="5.0000" BottomMargin="3.0000" FontSize="22" LabelText="1000" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="44.0000" Y="22.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="53.5000" Y="14.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.6080" Y="0.4667" />
                <PreSize X="0.5000" Y="0.7333" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="160.0000" Y="167.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.3929" />
            <PreSize X="0.2750" Y="0.0706" />
            <FileData Type="MarkedSubImage" Path="landlord/res/roomlist/player_num_bg.png" Plist="landlord/res/roomlist/roomlist.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="bottom" ActionTag="1300010604" Tag="515" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="52.0000" RightMargin="52.0000" TopMargin="310.2485" BottomMargin="90.7515" FontSize="24" LabelText="--  底钱 ：0.10 --" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="216.0000" Y="24.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="160.0000" Y="102.7515" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="239" G="220" B="137" />
            <PrePosition X="0.5000" Y="0.2418" />
            <PreSize X="0.6750" Y="0.0565" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="num" ActionTag="-554467202" Tag="517" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="94.5000" RightMargin="142.5000" TopMargin="249.0400" BottomMargin="127.9600" LabelText="100" ctype="TextBMFontObjectData">
            <Size X="83.0000" Y="48.0000" />
            <Children>
              <AbstractNodeData Name="sheet" ActionTag="-157223145" Tag="518" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="83.0000" RightMargin="-36.0000" TopMargin="16.8000" BottomMargin="7.2000" FontSize="24" LabelText="/张" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="36.0000" Y="24.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="83.0000" Y="19.2000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="239" G="220" B="137" />
                <PrePosition X="1.0000" Y="0.4000" />
                <PreSize X="0.4337" Y="0.5000" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="136.0000" Y="151.9600" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4250" Y="0.3576" />
            <PreSize X="0.2594" Y="0.1129" />
            <LabelBMFontFile_CNB Type="Normal" Path="landlord/res/roomlist/num.fnt" Plist="" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>