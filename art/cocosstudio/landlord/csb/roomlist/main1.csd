<GameFile>
  <PropertyGroup Name="main1" Type="Scene" ID="083689eb-767f-4fec-be55-3ff36d754bdf" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Scene" Tag="1846" ctype="GameNodeObjectData">
        <Size X="960.0000" Y="640.0000" />
        <Children>
          <AbstractNodeData Name="bg" ActionTag="-341959962" Tag="1866" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" TouchEnable="True" LeftEage="422" RightEage="422" TopEage="237" BottomEage="237" Scale9OriginX="422" Scale9OriginY="237" Scale9Width="436" Scale9Height="246" ctype="ImageViewObjectData">
            <Size X="960.0000" Y="640.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="480.0000" Y="320.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="1.0000" Y="1.0000" />
            <FileData Type="Normal" Path="bg/room_bg.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="contentContainor" ActionTag="1744295981" Tag="2171" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-120.0000" RightMargin="-120.0000" TopMargin="50.9280" BottomMargin="89.0720" TouchEnable="True" ClipAble="True" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ScrollDirectionType="0" ctype="PageViewObjectData">
            <Size X="1200.0000" Y="500.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="480.0000" Y="339.0720" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5298" />
            <PreSize X="1.2500" Y="0.7813" />
            <SingleColor A="255" R="150" G="150" B="100" />
            <FirstColor A="255" R="150" G="150" B="100" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="quick" ActionTag="926209342" Tag="696" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="328.5000" RightMargin="328.5000" TopMargin="521.0000" BottomMargin="16.0000" ctype="SpriteObjectData">
            <Size X="303.0000" Y="103.0000" />
            <AnchorPoint ScaleX="0.5000" />
            <Position X="480.0000" Y="16.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.0250" />
            <PreSize X="0.3156" Y="0.1609" />
            <FileData Type="Normal" Path="landlord/lan/cn/quick_start.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="top" ActionTag="-1845681196" Tag="490" IconVisible="True" PercentWidthEnable="True" PercentWidthEnabled="True" TopMargin="-80.0000" BottomMargin="625.0000" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="True" ctype="ProjectNodeObjectData">
            <Size X="960.0000" Y="95.0000" />
            <AnchorPoint />
            <Position Y="625.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition Y="0.9766" />
            <PreSize X="1.0000" Y="0.1484" />
            <FileData Type="Normal" Path="game_public/csb/hall_top.csd" Plist="" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>