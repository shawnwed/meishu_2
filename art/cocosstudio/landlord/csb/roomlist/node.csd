<GameFile>
  <PropertyGroup Name="node" Type="Node" ID="6500580b-2d91-489d-bd11-f70b9ee75d25" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="503" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="bet_select_2" ActionTag="-1446401580" Tag="1169" IconVisible="False" LeftMargin="-382.6849" RightMargin="62.6849" TopMargin="-246.0107" BottomMargin="-178.9893" ctype="SpriteObjectData">
            <Size X="320.0000" Y="425.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-222.6849" Y="33.5107" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="landlord/res/roomlist/bet_select.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="bet_select_no_3" ActionTag="287840094" Tag="1170" IconVisible="False" LeftMargin="-132.9755" RightMargin="-187.0245" TopMargin="-213.5810" BottomMargin="-211.4190" ctype="SpriteObjectData">
            <Size X="320.0000" Y="425.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="27.0245" Y="1.0810" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="landlord/res/roomlist/bet_select_no.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="bet_select2_1" ActionTag="1354684073" Tag="871" IconVisible="False" LeftMargin="19.0000" RightMargin="-339.0000" TopMargin="-227.5000" BottomMargin="-197.5000" ctype="SpriteObjectData">
            <Size X="320.0000" Y="425.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="179.0000" Y="15.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="landlord/res/roomlist/bet_select2.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>