<GameFile>
  <PropertyGroup Name="controlPanel" Type="Node" ID="c66af591-6b66-4eb0-8b5e-884aefbd9420" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="1096" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="play" ActionTag="1692271039" Tag="1101" IconVisible="True" LeftMargin="1.0001" RightMargin="-1.0001" TopMargin="-283.0001" BottomMargin="283.0001" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="1.0001" Y="283.0001" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="landlord/csb/table/btns_play.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="ready" ActionTag="-2130271270" Tag="1108" IconVisible="True" LeftMargin="1.0001" RightMargin="-1.0001" TopMargin="-283.0001" BottomMargin="283.0001" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="1.0001" Y="283.0001" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="landlord/csb/table/btns_ready.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="rob" ActionTag="-1339921810" Tag="61" IconVisible="True" LeftMargin="1.0001" RightMargin="-1.0001" TopMargin="-283.0001" BottomMargin="283.0001" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="1.0001" Y="283.0001" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="landlord/csb/table/btns_rob.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="auto_play_mask" ActionTag="1602360379" Tag="239" IconVisible="False" LeftMargin="-639.0000" RightMargin="-641.0000" TopMargin="-179.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="51" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="1280.0000" Y="179.0000" />
            <Children>
              <AbstractNodeData Name="btn" ActionTag="-1672406371" Tag="240" IconVisible="False" LeftMargin="530.0000" RightMargin="532.0000" TopMargin="42.0000" BottomMargin="43.0000" ctype="SpriteObjectData">
                <Size X="218.0000" Y="94.0000" />
                <Children>
                  <AbstractNodeData Name="title" ActionTag="1821557209" Tag="75" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="86.0000" RightMargin="86.0000" TopMargin="24.0000" BottomMargin="24.0000" ctype="SpriteObjectData">
                    <Size X="218.0000" Y="94.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="109.0000" Y="47.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="0.2110" Y="0.4894" />
                    <FileData Type="MarkedSubImage" Path="landlord/lan/cn/btn_word_quxiaotuoguan.png" Plist="landlord/lan/cn/game.plist" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="639.0000" Y="90.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4992" Y="0.5028" />
                <PreSize X="0.1703" Y="0.5251" />
                <FileData Type="MarkedSubImage" Path="landlord/res/table/landlord_btn_orange.png" Plist="landlord/res/landlord.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" />
            <Position X="1.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <SingleColor A="255" R="26" G="26" B="26" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="no_bigger_mask" Visible="False" ActionTag="-833913979" Tag="1343" IconVisible="False" LeftMargin="-640.0000" RightMargin="-640.0000" TopMargin="-179.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="51" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="1280.0000" Y="179.0000" />
            <Children>
              <AbstractNodeData Name="words" ActionTag="-392115983" Tag="1346" IconVisible="False" LeftMargin="617.0000" RightMargin="617.0000" TopMargin="64.0000" BottomMargin="69.0000" ctype="SpriteObjectData">
                <Size X="395.0000" Y="54.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="640.0000" Y="92.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5140" />
                <PreSize X="0.0359" Y="0.2570" />
                <FileData Type="MarkedSubImage" Path="landlord/lan/cn/no_bigger_cards_tips.png" Plist="landlord/lan/cn/game.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <SingleColor A="255" R="26" G="26" B="26" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>