<GameFile>
  <PropertyGroup Name="btns_play" Type="Node" ID="f83c5ecf-70dd-48a0-8260-b31fc5fd7828" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="391" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="play_btn" ActionTag="-518883829" Tag="392" IconVisible="False" LeftMargin="228.0000" RightMargin="-432.0000" TopMargin="-0.4995" BottomMargin="-80.5005" ctype="SpriteObjectData">
            <Size X="204.0000" Y="81.0000" />
            <Children>
              <AbstractNodeData Name="title" ActionTag="1742912335" Tag="19" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-7.0001" RightMargin="-6.9999" TopMargin="-6.5000" BottomMargin="-6.5000" ctype="SpriteObjectData">
                <Size X="218.0000" Y="94.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="101.9999" Y="40.5000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5000" />
                <PreSize X="1.0686" Y="1.1605" />
                <FileData Type="MarkedSubImage" Path="landlord/lan/cn/btn_word_chupai.png" Plist="landlord/lan/cn/game.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="330.0000" Y="-40.0005" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="landlord/res/table/landlord_btn_orange.png" Plist="landlord/res/landlord.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="reselect_btn" ActionTag="2033638904" Tag="393" IconVisible="False" LeftMargin="-212.5000" RightMargin="7.5000" TopMargin="-0.9995" BottomMargin="-81.0005" ctype="SpriteObjectData">
            <Size X="205.0000" Y="82.0000" />
            <Children>
              <AbstractNodeData Name="title" ActionTag="-1899560127" Tag="18" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-6.5000" RightMargin="-6.5000" TopMargin="-6.0000" BottomMargin="-6.0000" ctype="SpriteObjectData">
                <Size X="218.0000" Y="94.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="102.5000" Y="41.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5000" />
                <PreSize X="1.0634" Y="1.1463" />
                <FileData Type="MarkedSubImage" Path="landlord/lan/cn/btn_word_reselect.png" Plist="landlord/lan/cn/game.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-110.0000" Y="-40.0005" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="landlord/res/table/landlord_btn_blue.png" Plist="landlord/res/landlord.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="hint_btn" ActionTag="-75175297" Tag="394" IconVisible="False" LeftMargin="7.5000" RightMargin="-212.5000" TopMargin="-0.9995" BottomMargin="-81.0005" ctype="SpriteObjectData">
            <Size X="205.0000" Y="82.0000" />
            <Children>
              <AbstractNodeData Name="title" ActionTag="925650470" Tag="17" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-6.5000" RightMargin="-6.5000" TopMargin="-6.0000" BottomMargin="-6.0000" ctype="SpriteObjectData">
                <Size X="218.0000" Y="94.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="102.5000" Y="41.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5000" />
                <PreSize X="1.0634" Y="1.1463" />
                <FileData Type="MarkedSubImage" Path="landlord/lan/cn/btn_word_hint.png" Plist="landlord/lan/cn/game.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="110.0000" Y="-40.0005" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="landlord/res/table/landlord_btn_green.png" Plist="landlord/res/landlord.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="pass_btn" ActionTag="-1075982626" Tag="15" IconVisible="False" LeftMargin="-432.5000" RightMargin="227.5000" TopMargin="-0.9995" BottomMargin="-81.0005" ctype="SpriteObjectData">
            <Size X="205.0000" Y="82.0000" />
            <Children>
              <AbstractNodeData Name="title" ActionTag="-2001164399" Tag="16" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-6.5000" RightMargin="-6.5000" TopMargin="-6.0000" BottomMargin="-6.0000" ctype="SpriteObjectData">
                <Size X="218.0000" Y="94.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="102.5000" Y="41.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5000" />
                <PreSize X="1.0634" Y="1.1463" />
                <FileData Type="MarkedSubImage" Path="landlord/lan/cn/btn_word_pass.png" Plist="landlord/lan/cn/game.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-330.0000" Y="-40.0005" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="landlord/res/table/landlord_btn_blue.png" Plist="landlord/res/landlord.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="clock" ActionTag="1795838567" Tag="4059" IconVisible="True" TopMargin="-55.0000" BottomMargin="55.0000" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position Y="55.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="landlord/csb/table/clock.csd" Plist="" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>