<GameFile>
  <PropertyGroup Name="head" Type="Node" ID="06ae15ce-afe1-417f-959a-2dc318292b72" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="8549" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="Image_1" ActionTag="1393615532" VisibleForFrame="False" Tag="8552" IconVisible="False" LeftMargin="-56.0000" RightMargin="-56.0000" TopMargin="-56.0000" BottomMargin="-56.0000" LeftEage="26" RightEage="26" TopEage="26" BottomEage="26" Scale9OriginX="26" Scale9OriginY="26" Scale9Width="16" Scale9Height="15" ctype="ImageViewObjectData">
            <Size X="112.0000" Y="112.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="hall/res/head/head1.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="img" ActionTag="-82110386" Tag="8550" IconVisible="False" LeftMargin="-56.0000" RightMargin="-56.0000" TopMargin="-56.0000" BottomMargin="-56.0000" LeftEage="21" RightEage="21" TopEage="21" BottomEage="21" Scale9OriginX="21" Scale9OriginY="21" Scale9Width="26" Scale9Height="25" ctype="ImageViewObjectData">
            <Size X="112.0000" Y="112.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="hall/res/head/head1.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="img_bord" ActionTag="496091416" Tag="8609" IconVisible="False" LeftMargin="-67.5000" RightMargin="-67.5000" TopMargin="-65.5000" BottomMargin="-65.5000" ctype="SpriteObjectData">
            <Size X="135.0000" Y="131.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="landlord/res/table/landlord_avatar_frame2.png" Plist="landlord/res/landlord.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>