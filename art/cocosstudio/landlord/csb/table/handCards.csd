<GameFile>
  <PropertyGroup Name="handCards" Type="Node" ID="7e81af96-19c7-4a93-86c6-5c30f5efce41" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="1077" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="bg" ActionTag="-2118834067" Tag="266" IconVisible="False" LeftMargin="-31.0000" RightMargin="-29.0000" TopMargin="-74.0000" BottomMargin="6.0000" ctype="SpriteObjectData">
            <Size X="60.0000" Y="68.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-1.0000" Y="40.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="landlord/res/table/handcards_bg.png" Plist="landlord/res/landlord.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="warming" ActionTag="1012468281" Tag="39" IconVisible="True" TopMargin="-94.9999" BottomMargin="94.9999" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position Y="94.9999" />
            <Scale ScaleX="0.8000" ScaleY="0.8000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="landlord/csb/table/warming_ani.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="cards_num_txt" ActionTag="-1326798038" Tag="149" IconVisible="False" LeftMargin="-20.0000" RightMargin="-22.0000" TopMargin="-54.0000" BottomMargin="20.0000" CharWidth="21" CharHeight="34" LabelText="17" StartChar="." ctype="TextAtlasObjectData">
            <Size X="42.0000" Y="34.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1.0000" Y="37.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <LabelAtlasFileImage_CNB Type="Normal" Path="landlord/res/handcards_num.png" Plist="" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>