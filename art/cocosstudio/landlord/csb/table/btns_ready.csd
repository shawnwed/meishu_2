<GameFile>
  <PropertyGroup Name="btns_ready" Type="Node" ID="d5f3254a-bca2-4a58-8cd7-1ed252c3f2cd" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="1016" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="start_btn" ActionTag="644663585" Tag="1026" IconVisible="False" LeftMargin="6.0000" RightMargin="-224.0000" TopMargin="-7.0000" BottomMargin="-87.0000" ctype="SpriteObjectData">
            <Size X="218.0000" Y="94.0000" />
            <Children>
              <AbstractNodeData Name="title" ActionTag="2128819188" Tag="5" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="86.0000" RightMargin="86.0000" TopMargin="24.0000" BottomMargin="24.0000" ctype="SpriteObjectData">
                <Size X="218.0000" Y="94.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="109.0000" Y="47.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5000" />
                <PreSize X="0.2110" Y="0.4894" />
                <FileData Type="MarkedSubImage" Path="landlord/lan/cn/btn_word_ready.png" Plist="landlord/lan/cn/game.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="115.0000" Y="-40.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="landlord/res/table/landlord_btn_orange.png" Plist="landlord/res/landlord.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="change_btn" ActionTag="-2143780447" Tag="1027" IconVisible="False" LeftMargin="-224.0000" RightMargin="6.0000" TopMargin="-7.0000" BottomMargin="-87.0000" ctype="SpriteObjectData">
            <Size X="218.0000" Y="94.0000" />
            <Children>
              <AbstractNodeData Name="title" ActionTag="-1952355643" Tag="4" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="86.0000" RightMargin="86.0000" TopMargin="24.0000" BottomMargin="24.0000" ctype="SpriteObjectData">
                <Size X="218.0000" Y="94.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="109.0000" Y="47.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5000" />
                <PreSize X="0.2110" Y="0.4894" />
                <FileData Type="MarkedSubImage" Path="landlord/lan/cn/btn_word_changeTable.png" Plist="landlord/lan/cn/game.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-115.0000" Y="-40.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="landlord/res/table/landlord_btn_green.png" Plist="landlord/res/landlord.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="invite_btn" ActionTag="-774559418" VisibleForFrame="False" Tag="7699" IconVisible="False" LeftMargin="-224.0000" RightMargin="6.0000" TopMargin="-7.0000" BottomMargin="-87.0000" ctype="SpriteObjectData">
            <Size X="218.0000" Y="94.0000" />
            <Children>
              <AbstractNodeData Name="title" ActionTag="359644404" Tag="7700" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="86.0000" RightMargin="86.0000" TopMargin="24.0000" BottomMargin="24.0000" ctype="SpriteObjectData">
                <Size X="218.0000" Y="94.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="109.0000" Y="47.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5000" />
                <PreSize X="0.2110" Y="0.4894" />
                <FileData Type="MarkedSubImage" Path="landlord/lan/cn/btn_word_inviteTable.png" Plist="landlord/lan/cn/game.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-115.0000" Y="-40.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="landlord/res/table/landlord_btn_green.png" Plist="landlord/res/landlord.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>