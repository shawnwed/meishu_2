<GameFile>
  <PropertyGroup Name="private_score" Type="Node" ID="36e2a888-8a8d-41c6-8e2b-e4125541755a" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="13341" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="bg" ActionTag="-454740652" Tag="17170" IconVisible="False" RightMargin="-347.0000" BottomMargin="-720.0000" FlipX="True" LeftEage="114" RightEage="114" TopEage="237" BottomEage="237" Scale9OriginX="114" Scale9OriginY="237" Scale9Width="119" Scale9Height="246" ctype="ImageViewObjectData">
            <Size X="347.0000" Y="720.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="1.0000" />
            <Position X="173.5000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="landlord/res/table/menu_right_bg.png" Plist="landlord/res/landlord.plist" />
          </AbstractNodeData>
          <AbstractNodeData Name="prev_record" ActionTag="407606066" Tag="14721" IconVisible="False" LeftMargin="-32.0000" RightMargin="-341.0000" BottomMargin="-720.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="0" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="373.0000" Y="720.0000" />
            <Children>
              <AbstractNodeData Name="item_1" ActionTag="-2004793128" Tag="14722" IconVisible="True" LeftMargin="372.1046" RightMargin="0.8954" TopMargin="-0.1300" BottomMargin="720.1300" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint />
                <Position X="372.1046" Y="720.1300" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.9976" Y="1.0002" />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="Normal" Path="landlord/csb/table/record_item.csd" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="item_2" ActionTag="261716793" Tag="14763" IconVisible="True" LeftMargin="372.6380" RightMargin="0.3620" TopMargin="168.2400" BottomMargin="551.7600" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint />
                <Position X="372.6380" Y="551.7600" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.9990" Y="0.7663" />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="Normal" Path="landlord/csb/table/record_item.csd" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="item_3" ActionTag="10280133" Tag="14804" IconVisible="True" LeftMargin="370.9739" RightMargin="2.0261" TopMargin="339.6000" BottomMargin="380.4000" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint />
                <Position X="370.9739" Y="380.4000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.9946" Y="0.5283" />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="Normal" Path="landlord/csb/table/record_item.csd" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="tips" ActionTag="-1717281616" Tag="14927" IconVisible="False" LeftMargin="140.6118" RightMargin="88.3882" TopMargin="302.2785" BottomMargin="381.7215" FontSize="36" LabelText="暂无牌局" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="144.0000" Y="36.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="212.6118" Y="399.7215" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5700" Y="0.5552" />
                <PreSize X="0.3861" Y="0.0500" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position X="-32.0000" Y="-720.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="score_list" ActionTag="1213632412" Tag="18372" IconVisible="False" RightMargin="-347.0000" BottomMargin="-650.0000" TouchEnable="True" ClipAble="True" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" IsBounceEnabled="True" ScrollDirectionType="0" DirectionType="Vertical" ctype="ListViewObjectData">
            <Size X="347.0000" Y="650.0000" />
            <AnchorPoint />
            <Position Y="-650.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <SingleColor A="255" R="150" G="150" B="255" />
            <FirstColor A="255" R="150" G="150" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="btn_1" ActionTag="-922272451" Tag="13545" IconVisible="False" LeftMargin="70.8446" RightMargin="-116.8446" TopMargin="662.4999" BottomMargin="-708.4999" ctype="SpriteObjectData">
            <Size X="150.0000" Y="54.0000" />
            <Children>
              <AbstractNodeData Name="txt1" ActionTag="-560275154" Tag="13543" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-27.0000" RightMargin="-27.0000" TopMargin="10.5000" BottomMargin="10.5000" FontSize="25" LabelText="上局回顾" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="100.0000" Y="25.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="23.0000" Y="23.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5000" />
                <PreSize X="2.1739" Y="0.5435" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="93.8446" Y="-685.4999" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="landlord/res/table/score_tab1_1.png" Plist="landlord/res/landlord.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="btn_2" ActionTag="-1641600162" Tag="13544" IconVisible="False" LeftMargin="220.4658" RightMargin="-266.4658" TopMargin="662.5004" BottomMargin="-708.5004" ctype="SpriteObjectData">
            <Size X="150.0000" Y="54.0000" />
            <Children>
              <AbstractNodeData Name="txt2" ActionTag="863250359" Tag="13542" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-27.0000" RightMargin="-27.0000" TopMargin="10.5000" BottomMargin="10.5000" FontSize="25" LabelText="牌局总计" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="100.0000" Y="25.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="23.0000" Y="23.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="146" G="146" B="146" />
                <PrePosition X="0.5000" Y="0.5000" />
                <PreSize X="2.1739" Y="0.5435" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="243.4658" Y="-685.5004" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="landlord/res/table/score_tab2_2.png" Plist="landlord/res/landlord.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>