<GameFile>
  <PropertyGroup Name="menu" Type="Node" ID="79859d48-ff67-432a-9fc9-a3238c45edd4" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="175" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="mask" ActionTag="594110671" Tag="143" IconVisible="False" RightMargin="-200.0000" BottomMargin="-200.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="0" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="200.0000" Y="200.0000" />
            <AnchorPoint ScaleY="1.0000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="bg" ActionTag="1461339978" Tag="25" IconVisible="False" RightMargin="-294.0000" BottomMargin="-404.0000" TouchEnable="True" Scale9Enable="True" LeftEage="87" RightEage="87" TopEage="117" BottomEage="117" Scale9OriginX="87" Scale9OriginY="117" Scale9Width="110" Scale9Height="190" ctype="ImageViewObjectData">
            <Size X="294.0000" Y="404.0000" />
            <AnchorPoint ScaleY="1.0000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="landlord/res/table/menu_bg.png" Plist="landlord/res/landlord.plist" />
          </AbstractNodeData>
          <AbstractNodeData Name="back_btn" ActionTag="-970566564" Tag="1287" IconVisible="False" LeftMargin="13.5000" RightMargin="-283.5000" TopMargin="12.5000" BottomMargin="-97.5000" ctype="SpriteObjectData">
            <Size X="270.0000" Y="85.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="148.5000" Y="-55.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="landlord/lan/cn/menu_back.png" Plist="landlord/lan/cn/game.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="setting_btn" ActionTag="-1301401505" Tag="1288" IconVisible="False" LeftMargin="13.5000" RightMargin="-283.5000" TopMargin="107.8333" BottomMargin="-192.8333" ctype="SpriteObjectData">
            <Size X="270.0000" Y="85.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="148.5000" Y="-150.3333" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="landlord/lan/cn/menu_setting.png" Plist="landlord/lan/cn/game.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="auto_btn" ActionTag="1982693816" Tag="1289" IconVisible="False" LeftMargin="13.5000" RightMargin="-283.5000" TopMargin="203.1667" BottomMargin="-288.1667" ctype="SpriteObjectData">
            <Size X="270.0000" Y="85.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="148.5000" Y="-245.6667" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="landlord/lan/cn/menu_auto.png" Plist="landlord/lan/cn/game.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="score_btn" ActionTag="870820101" Tag="1290" IconVisible="False" LeftMargin="125.5000" RightMargin="-171.5000" TopMargin="318.0000" BottomMargin="-364.0000" ctype="SpriteObjectData">
            <Size X="270.0000" Y="85.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="148.5000" Y="-341.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="landlord/lan/cn/menu_score.png" Plist="landlord/lan/cn/game.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>