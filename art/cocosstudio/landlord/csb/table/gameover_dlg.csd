<GameFile>
  <PropertyGroup Name="gameover_dlg" Type="Node" ID="bde830ae-9aaf-42c8-89eb-07e05243ef10" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="3616" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="mask" CanEdit="False" ActionTag="739060565" Tag="4892" IconVisible="False" LeftMargin="-640.0000" RightMargin="-640.0000" TopMargin="-360.0000" BottomMargin="-360.0000" ClipAble="False" BackColorAlpha="51" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="1280.0000" Y="720.0000" />
            <Children>
              <AbstractNodeData Name="coin_txt3" ActionTag="1391133884" Tag="156" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="218.2400" RightMargin="933.7600" TopMargin="113.9360" BottomMargin="562.0640" LabelText="+3465" ctype="TextBMFontObjectData">
                <Size X="128.0000" Y="44.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="218.2400" Y="584.0640" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1705" Y="0.8112" />
                <PreSize X="0.1000" Y="0.0611" />
                <LabelBMFontFile_CNB Type="Normal" Path="landlord/res/win.fnt" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="coin_txt2" ActionTag="381096231" Tag="165" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="935.0400" RightMargin="216.9600" TopMargin="113.9360" BottomMargin="562.0640" LabelText="+3465" ctype="TextBMFontObjectData">
                <Size X="128.0000" Y="44.0000" />
                <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
                <Position X="1063.0400" Y="584.0640" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.8305" Y="0.8112" />
                <PreSize X="0.1000" Y="0.0611" />
                <LabelBMFontFile_CNB Type="Normal" Path="landlord/res/win.fnt" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="coin_txt1" ActionTag="635901813" Tag="166" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="163.5840" RightMargin="988.4160" TopMargin="347.3600" BottomMargin="328.6400" LabelText="+3465" ctype="TextBMFontObjectData">
                <Size X="128.0000" Y="44.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="163.5840" Y="350.6400" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1278" Y="0.4870" />
                <PreSize X="0.1000" Y="0.0611" />
                <LabelBMFontFile_CNB Type="Normal" Path="landlord/res/win.fnt" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="coin_txt2_0" ActionTag="1928451445" Tag="11" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="935.0400" RightMargin="216.9600" TopMargin="113.9360" BottomMargin="562.0640" LabelText="+3465" ctype="TextBMFontObjectData">
                <Size X="128.0000" Y="44.0000" />
                <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
                <Position X="1063.0400" Y="584.0640" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.8305" Y="0.8112" />
                <PreSize X="0.1000" Y="0.0611" />
                <LabelBMFontFile_CNB Type="Normal" Path="landlord/res/lose.fnt" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="coin_txt3_0" ActionTag="105262541" Tag="12" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="218.2400" RightMargin="894.7600" TopMargin="113.9360" BottomMargin="562.0640" LabelText="+3465万" ctype="TextBMFontObjectData">
                <Size X="167.0000" Y="44.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="218.2400" Y="584.0640" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1705" Y="0.8112" />
                <PreSize X="0.1305" Y="0.0611" />
                <LabelBMFontFile_CNB Type="Normal" Path="landlord/res/lose.fnt" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="coin_txt1_0" ActionTag="-1320405216" Tag="13" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="163.5840" RightMargin="949.4160" TopMargin="347.3600" BottomMargin="328.6400" LabelText="+3465万" ctype="TextBMFontObjectData">
                <Size X="167.0000" Y="44.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="163.5840" Y="350.6400" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1278" Y="0.4870" />
                <PreSize X="0.1305" Y="0.0611" />
                <LabelBMFontFile_CNB Type="Normal" Path="landlord/res/lose.fnt" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="spring" ActionTag="-722753887" Tag="1876" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="669.5480" RightMargin="569.4520" TopMargin="320.8400" BottomMargin="361.1600" ctype="SpriteObjectData">
                <Size X="41.0000" Y="38.0000" />
                <Children>
                  <AbstractNodeData Name="txt" ActionTag="-1420494806" Tag="1877" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="117.5813" RightMargin="-104.5813" TopMargin="3.2862" BottomMargin="6.7138" FontSize="28" LabelText="x1" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="28.0000" Y="28.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="131.5813" Y="20.7138" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="3.2093" Y="0.5451" />
                    <PreSize X="0.6829" Y="0.7368" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="word" ActionTag="399514433" Tag="1878" IconVisible="False" LeftMargin="46.4996" RightMargin="-66.4996" TopMargin="2.5003" BottomMargin="0.4997" ctype="SpriteObjectData">
                    <Size X="61.0000" Y="35.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="76.9996" Y="17.9997" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="1.8780" Y="0.4737" />
                    <PreSize X="1.4878" Y="0.9211" />
                    <FileData Type="MarkedSubImage" Path="landlord/lan/cn/gameover_word_spring.png" Plist="landlord/lan/cn/game.plist" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="690.0480" Y="380.1600" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5391" Y="0.5280" />
                <PreSize X="0.0320" Y="0.0528" />
                <FileData Type="MarkedSubImage" Path="landlord/res/table/gameover_spring.png" Plist="landlord/res/landlord.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="bomb" ActionTag="2004701857" Tag="1875" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="474.5080" RightMargin="772.4920" TopMargin="311.3640" BottomMargin="363.6360" ctype="SpriteObjectData">
                <Size X="33.0000" Y="45.0000" />
                <Children>
                  <AbstractNodeData Name="txt" ActionTag="345094715" Tag="10" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="110.4562" RightMargin="-105.4562" TopMargin="11.8660" BottomMargin="5.1340" FontSize="28" LabelText="x1" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="28.0000" Y="28.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="124.4562" Y="19.1340" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="3.7714" Y="0.4252" />
                    <PreSize X="0.8485" Y="0.6222" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="word_0" ActionTag="-1347391260" Tag="1879" IconVisible="False" LeftMargin="38.9996" RightMargin="-67.9996" TopMargin="12.5004" BottomMargin="-2.5004" ctype="SpriteObjectData">
                    <Size X="62.0000" Y="35.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="69.9996" Y="14.9996" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="2.1212" Y="0.3333" />
                    <PreSize X="1.8788" Y="0.7778" />
                    <FileData Type="MarkedSubImage" Path="landlord/lan/cn/gameover_word_bomb.png" Plist="landlord/lan/cn/game.plist" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="491.0080" Y="386.1360" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3836" Y="0.5363" />
                <PreSize X="0.0258" Y="0.0625" />
                <FileData Type="MarkedSubImage" Path="landlord/res/table/gameover_bomb.png" Plist="landlord/res/landlord.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <SingleColor A="255" R="0" G="0" B="0" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="change_btn" ActionTag="894522876" Tag="152" IconVisible="False" LeftMargin="-452.5000" RightMargin="247.5000" TopMargin="241.1633" BottomMargin="-323.1633" ctype="SpriteObjectData">
            <Size X="205.0000" Y="82.0000" />
            <Children>
              <AbstractNodeData Name="title" ActionTag="746424372" Tag="153" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-6.5000" RightMargin="-6.5000" TopMargin="-6.0000" BottomMargin="-6.0000" ctype="SpriteObjectData">
                <Size X="218.0000" Y="94.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="102.5000" Y="41.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5000" />
                <PreSize X="1.0634" Y="1.1463" />
                <FileData Type="MarkedSubImage" Path="landlord/lan/cn/btn_word_changeTable.png" Plist="landlord/lan/cn/game.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-350.0000" Y="-282.1633" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="landlord/res/table/landlord_btn_green.png" Plist="landlord/res/landlord.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="start_btn" ActionTag="727386366" Tag="154" IconVisible="False" LeftMargin="248.0000" RightMargin="-452.0000" TopMargin="241.6633" BottomMargin="-322.6633" ctype="SpriteObjectData">
            <Size X="204.0000" Y="81.0000" />
            <Children>
              <AbstractNodeData Name="title" ActionTag="-1322864336" Tag="155" IconVisible="False" PositionPercentYEnabled="True" RightMargin="-14.0000" TopMargin="-6.5000" BottomMargin="-6.5000" ctype="SpriteObjectData">
                <Size X="218.0000" Y="94.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="109.0000" Y="40.5000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5343" Y="0.5000" />
                <PreSize X="1.0686" Y="1.1605" />
                <FileData Type="MarkedSubImage" Path="landlord/lan/cn/btn_word_ready.png" Plist="landlord/lan/cn/game.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="350.0000" Y="-282.1633" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="landlord/res/table/landlord_btn_orange.png" Plist="landlord/res/landlord.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>