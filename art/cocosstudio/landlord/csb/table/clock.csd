<GameFile>
  <PropertyGroup Name="clock" Type="Node" ID="acd37eed-d2ee-4826-b423-981b63d3f870" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="558" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="clock" ActionTag="-1429230277" Tag="559" IconVisible="False" LeftMargin="-48.5000" RightMargin="-48.5000" TopMargin="-48.0000" BottomMargin="-48.0000" ctype="SpriteObjectData">
            <Size X="97.0000" Y="96.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="landlord/res/table/landlord_clock2.png" Plist="landlord/res/landlord.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="time_txt" ActionTag="1094761308" Tag="278" IconVisible="False" LeftMargin="-18.4999" RightMargin="-20.5001" TopMargin="-20.9999" BottomMargin="-19.0001" FontSize="35" LabelText="00" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="39.0000" Y="40.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1.0001" Y="0.9999" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="51" G="51" B="48" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>