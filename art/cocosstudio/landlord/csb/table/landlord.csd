<GameFile>
  <PropertyGroup Name="landlord" Type="Scene" ID="dfa9660e-0402-4686-b197-ab522ba89caf" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Scene" Tag="19" ctype="GameNodeObjectData">
        <Size X="960.0000" Y="640.0000" />
        <Children>
          <AbstractNodeData Name="bg" CanEdit="False" ActionTag="-1806199359" Tag="20" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" TouchEnable="True" LeftEage="422" RightEage="422" TopEage="237" BottomEage="237" Scale9OriginX="422" Scale9OriginY="237" Scale9Width="436" Scale9Height="246" ctype="ImageViewObjectData">
            <Size X="960.0000" Y="640.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="480.0000" Y="320.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="1.0000" Y="1.0000" />
            <FileData Type="Normal" Path="bg/table_bg.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="top" ActionTag="-1469150010" Tag="2075" IconVisible="False" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentWidthEnabled="True" BottomMargin="440.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="0" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="960.0000" Y="200.0000" />
            <Children>
              <AbstractNodeData Name="bg" ActionTag="1579959784" Tag="2244" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="43.0000" RightMargin="43.0000" TopMargin="-1.0000" BottomMargin="164.0000" ctype="SpriteObjectData">
                <Size X="874.0000" Y="37.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="1.0000" />
                <Position X="480.0000" Y="201.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="1.0050" />
                <PreSize X="0.9104" Y="0.1850" />
                <FileData Type="MarkedSubImage" Path="landlord/res/table/top_bg.png" Plist="landlord/res/landlord.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="cards_3" ActionTag="464083922" Tag="2220" IconVisible="True" PositionPercentXEnabled="True" LeftMargin="528.0000" RightMargin="432.0000" TopMargin="74.0001" BottomMargin="125.9999" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint />
                <Position X="528.0000" Y="125.9999" />
                <Scale ScaleX="0.3800" ScaleY="0.3800" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5500" Y="0.6300" />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="Normal" Path="common/card.csd" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="cards_2" ActionTag="-1433221568" Tag="2228" IconVisible="True" PositionPercentXEnabled="True" LeftMargin="480.0000" RightMargin="480.0000" TopMargin="74.0000" BottomMargin="126.0000" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint />
                <Position X="480.0000" Y="126.0000" />
                <Scale ScaleX="0.3800" ScaleY="0.3800" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.6300" />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="Normal" Path="common/card.csd" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="cards_1" ActionTag="12377704" Tag="2236" IconVisible="True" PositionPercentXEnabled="True" LeftMargin="432.0000" RightMargin="528.0000" TopMargin="74.0002" BottomMargin="125.9998" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint />
                <Position X="432.0000" Y="125.9998" />
                <Scale ScaleX="0.3800" ScaleY="0.3800" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4500" Y="0.6300" />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="Normal" Path="common/card.csd" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="task" Visible="False" ActionTag="1599000412" Tag="2110" IconVisible="True" LeftMargin="379.3920" RightMargin="580.6080" TopMargin="35.7800" BottomMargin="164.2200" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint />
                <Position X="379.3920" Y="164.2200" />
                <Scale ScaleX="0.8000" ScaleY="0.8000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3952" Y="0.8211" />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="Normal" Path="landlord/csb/table/top_task.csd" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="top_info" ActionTag="1153048159" Tag="479" IconVisible="True" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="287.9440" RightMargin="672.0560" TopMargin="7.4907" BottomMargin="192.5093" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint />
                <Position X="287.9440" Y="192.5093" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2999" Y="0.9625" />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="Normal" Path="game_public/csb/top_info_0.csd" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="score_icon" ActionTag="1067471742" Tag="462" IconVisible="False" LeftMargin="545.6797" RightMargin="394.3203" TopMargin="8.8495" BottomMargin="173.1505" ctype="SpriteObjectData">
                <Size X="20.0000" Y="18.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="555.6797" Y="182.1505" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5788" Y="0.9108" />
                <PreSize X="0.0208" Y="0.0900" />
                <FileData Type="MarkedSubImage" Path="landlord/res/table/score_icon.png" Plist="landlord/res/landlord.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="txt1" ActionTag="-1404198241" Tag="2557" IconVisible="False" LeftMargin="829.0960" RightMargin="75.9040" TopMargin="6.2466" BottomMargin="171.7534" FontSize="22" LabelText="底分:" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="55.0000" Y="22.0000" />
                <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
                <Position X="884.0960" Y="182.7534" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="223" G="223" B="223" />
                <PrePosition X="0.9209" Y="0.9138" />
                <PreSize X="0.0573" Y="0.1100" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="score_txt" ActionTag="118862022" Tag="2558" IconVisible="False" LeftMargin="888.0640" RightMargin="27.9360" TopMargin="6.2466" BottomMargin="171.7534" FontSize="22" LabelText="1000" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="44.0000" Y="22.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="888.0640" Y="182.7534" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="253" G="246" B="128" />
                <PrePosition X="0.9251" Y="0.9138" />
                <PreSize X="0.0458" Y="0.1100" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="roomid_txt" ActionTag="-487277682" Tag="1526" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="457.4789" RightMargin="436.5211" TopMargin="4.2466" BottomMargin="173.7534" FontSize="22" LabelText="123456" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="66.0000" Y="22.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="457.4789" Y="184.7534" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="253" G="246" B="128" />
                <PrePosition X="0.4765" Y="0.9238" />
                <PreSize X="0.0688" Y="0.1100" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="roomid_title" ActionTag="-1817866634" Tag="1525" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="401.9463" RightMargin="503.0537" TopMargin="4.2466" BottomMargin="173.7534" FontSize="22" LabelText="房号:" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="55.0000" Y="22.0000" />
                <AnchorPoint ScaleX="0.5063" ScaleY="0.6527" />
                <Position X="429.7928" Y="188.1128" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="223" G="223" B="223" />
                <PrePosition X="0.4477" Y="0.9406" />
                <PreSize X="0.0573" Y="0.1100" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="round_txt" ActionTag="-2013199355" Tag="464" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="570.1899" RightMargin="367.8101" TopMargin="4.2466" BottomMargin="173.7534" FontSize="22" LabelText="10" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="22.0000" Y="22.0000" />
                <Children>
                  <AbstractNodeData Name="txt" ActionTag="1552262905" Tag="463" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="23.2452" RightMargin="-34.2452" TopMargin="-0.3564" BottomMargin="0.3564" FontSize="22" LabelText="/20" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="33.0000" Y="22.0000" />
                    <AnchorPoint ScaleY="0.6527" />
                    <Position X="23.2452" Y="14.7158" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="223" G="223" B="223" />
                    <PrePosition X="1.0566" Y="0.6689" />
                    <PreSize X="1.5000" Y="1.0000" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleY="0.5000" />
                <Position X="570.1899" Y="184.7534" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="253" G="246" B="128" />
                <PrePosition X="0.5939" Y="0.9238" />
                <PreSize X="0.0229" Y="0.1100" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="txt2" ActionTag="1267913724" Tag="2559" IconVisible="False" LeftMargin="951.0800" RightMargin="-46.0800" TopMargin="6.2466" BottomMargin="171.7534" FontSize="22" LabelText="倍數:" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="55.0000" Y="22.0000" />
                <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
                <Position X="1006.0800" Y="182.7534" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="223" G="223" B="223" />
                <PrePosition X="1.0480" Y="0.9138" />
                <PreSize X="0.0573" Y="0.1100" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="multiple_txt" ActionTag="1512550102" Tag="2560" IconVisible="False" LeftMargin="1006.2079" RightMargin="-79.2079" TopMargin="6.2466" BottomMargin="171.7534" FontSize="22" LabelText="108" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="33.0000" Y="22.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="1006.2079" Y="182.7534" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="253" G="246" B="128" />
                <PrePosition X="1.0481" Y="0.9138" />
                <PreSize X="0.0344" Y="0.1100" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="fee_type_txt" ActionTag="-51934403" Tag="465" IconVisible="False" LeftMargin="644.1200" RightMargin="249.8800" TopMargin="6.2466" BottomMargin="171.7534" FontSize="22" LabelText="AA收费" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="66.0000" Y="22.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="677.1200" Y="182.7534" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="253" G="246" B="128" />
                <PrePosition X="0.7053" Y="0.9138" />
                <PreSize X="0.0688" Y="0.1100" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="deal_type_txt" ActionTag="2128929237" Tag="466" IconVisible="False" LeftMargin="722.0800" RightMargin="149.9200" TopMargin="6.2466" BottomMargin="171.7534" FontSize="22" LabelText="单张发牌" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="88.0000" Y="22.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="766.0800" Y="182.7534" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="253" G="246" B="128" />
                <PrePosition X="0.7980" Y="0.9138" />
                <PreSize X="0.0917" Y="0.1100" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleY="1.0000" />
            <Position Y="640.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition Y="1.0000" />
            <PreSize X="1.0000" Y="0.3125" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="controlPanel" ActionTag="-1706410790" Tag="1164" IconVisible="True" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="480.0000" RightMargin="480.0000" TopMargin="640.0000" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="480.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="landlord/csb/table/controlPanel.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="score_btn" ActionTag="-847954222" Tag="1017" IconVisible="False" PositionPercentYEnabled="True" HorizontalEdge="RightEdge" LeftMargin="890.0000" RightMargin="1.0000" TopMargin="2.6880" BottomMargin="568.3120" ctype="SpriteObjectData">
            <Size X="69.0000" Y="69.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="1.0000" />
            <Position X="924.5000" Y="637.3120" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.9630" Y="0.9958" />
            <PreSize X="0.0719" Y="0.1078" />
            <FileData Type="MarkedSubImage" Path="bullfighting/res/table/btn_record.png" Plist="bullfighting/res/table.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="top_help" ActionTag="1901989386" Tag="1869" IconVisible="False" LeftMargin="103.3347" RightMargin="787.6653" TopMargin="-67.5936" BottomMargin="638.5936" ctype="SpriteObjectData">
            <Size X="69.0000" Y="69.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="137.8347" Y="673.0936" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.1436" Y="1.0517" />
            <PreSize X="0.0719" Y="0.1078" />
            <FileData Type="MarkedSubImage" Path="landlord/res/table/top_help.png" Plist="landlord/res/landlord.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="menu_btn" ActionTag="-1624669527" Tag="21" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="10.0000" RightMargin="881.0000" TopMargin="8.8320" BottomMargin="562.1680" ctype="SpriteObjectData">
            <Size X="69.0000" Y="69.0000" />
            <AnchorPoint ScaleY="1.0000" />
            <Position X="10.0000" Y="631.1680" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0104" Y="0.9862" />
            <PreSize X="0.0719" Y="0.1078" />
            <FileData Type="MarkedSubImage" Path="bullfighting/res/table/btn_menu.png" Plist="bullfighting/res/table.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="player_avatar_2" ActionTag="456951019" Tag="219" IconVisible="True" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="112.2240" RightMargin="847.7760" TopMargin="216.4480" BottomMargin="423.5520" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="112.2240" Y="423.5520" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.1169" Y="0.6618" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="landlord/csb/table/player_avatar.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="player_avatar_1" ActionTag="1383286788" Tag="228" IconVisible="True" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="847.7760" RightMargin="112.2240" TopMargin="216.4480" BottomMargin="423.5520" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="847.7760" Y="423.5520" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8831" Y="0.6618" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="landlord/csb/table/player_avatar.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="player_avatar_0" ActionTag="1839838682" Tag="237" IconVisible="True" PositionPercentXEnabled="True" LeftMargin="67.2000" RightMargin="892.8000" TopMargin="399.7098" BottomMargin="240.2902" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="67.2000" Y="240.2902" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0700" Y="0.3755" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="landlord/csb/table/player_avatar.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="hand_cards_2" ActionTag="1762489946" Tag="1383" IconVisible="True" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="179.4240" RightMargin="780.5760" TopMargin="216.4480" BottomMargin="423.5520" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="179.4240" Y="423.5520" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.1869" Y="0.6618" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="landlord/csb/table/handCards.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="hand_cards_1" ActionTag="-707027553" Tag="244" IconVisible="True" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="780.5760" RightMargin="179.4240" TopMargin="216.4480" BottomMargin="423.5520" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="780.5760" Y="423.5520" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8131" Y="0.6618" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="landlord/csb/table/handCards.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="player_word_0" ActionTag="247457413" Tag="346" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="426.5000" RightMargin="426.5000" TopMargin="363.5001" BottomMargin="219.4999" ctype="SpriteObjectData">
            <Size X="107.0000" Y="57.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="480.0000" Y="247.9999" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.3875" />
            <PreSize X="0.1115" Y="0.0891" />
            <FileData Type="MarkedSubImage" Path="landlord/lan/cn/landlord_ready_word.png" Plist="landlord/lan/cn/game.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="player_word_1" ActionTag="1032158989" Tag="347" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="639.5240" RightMargin="213.4760" TopMargin="139.2440" BottomMargin="443.7560" ctype="SpriteObjectData">
            <Size X="107.0000" Y="57.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="693.0240" Y="472.2560" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.7219" Y="0.7379" />
            <PreSize X="0.1115" Y="0.0891" />
            <FileData Type="MarkedSubImage" Path="landlord/lan/cn/landlord_ready_word.png" Plist="landlord/lan/cn/game.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="player_word_2" ActionTag="1281625898" Tag="348" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="213.5720" RightMargin="639.4280" TopMargin="139.2440" BottomMargin="443.7560" ctype="SpriteObjectData">
            <Size X="107.0000" Y="57.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="267.0720" Y="472.2560" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.2782" Y="0.7379" />
            <PreSize X="0.1115" Y="0.0891" />
            <FileData Type="MarkedSubImage" Path="landlord/lan/cn/landlord_ready_word.png" Plist="landlord/lan/cn/game.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="player_clock_2" ActionTag="601078169" Tag="826" IconVisible="True" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="271.6800" RightMargin="688.3200" TopMargin="169.6640" BottomMargin="470.3360" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="271.6800" Y="470.3360" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.2830" Y="0.7349" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="landlord/csb/table/clock.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="player_clock_1" ActionTag="-625919306" Tag="830" IconVisible="True" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="699.3600" RightMargin="260.6400" TopMargin="176.7040" BottomMargin="463.2960" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="699.3600" Y="463.2960" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.7285" Y="0.7239" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="landlord/csb/table/clock.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="title" ActionTag="1408425540" VisibleForFrame="False" Tag="1527" IconVisible="False" PositionPercentXEnabled="True" VerticalEdge="TopEdge" LeftMargin="335.0120" RightMargin="349.9880" TopMargin="112.6500" BottomMargin="505.3500" FontSize="22" LabelText="  本房间炸弹翻倍上限：   " ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="275.0000" Y="22.0000" />
            <Children>
              <AbstractNodeData Name="txt" ActionTag="1168107273" Tag="1528" IconVisible="False" LeftMargin="168.7089" RightMargin="-58.7089" TopMargin="-2.3249" BottomMargin="2.3249" FontSize="22" LabelText="      不限炸   " ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="165.0000" Y="22.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="251.2089" Y="13.3249" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="229" B="54" />
                <PrePosition X="0.9135" Y="0.6057" />
                <PreSize X="0.6000" Y="1.0000" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="472.5120" Y="516.3500" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4922" Y="0.8068" />
            <PreSize X="0.2865" Y="0.0344" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>