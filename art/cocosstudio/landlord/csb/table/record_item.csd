<GameFile>
  <PropertyGroup Name="record_item" Type="Node" ID="ed8993ed-c750-4139-9ac8-a504c4518f05" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="555" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="name" ActionTag="-1151183794" Tag="573" IconVisible="False" LeftMargin="-229.4184" RightMargin="141.4184" TopMargin="14.2546" BottomMargin="-36.2546" FontSize="22" LabelText="我是谁.." ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="88.0000" Y="22.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="-229.4184" Y="-25.2546" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="229" G="229" B="229" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="score" ActionTag="313302744" Tag="575" IconVisible="False" LeftMargin="-333.9895" RightMargin="256.9895" TopMargin="12.6688" BottomMargin="-34.6688" FontSize="22" LabelText="+300000" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="77.0000" Y="22.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-295.4895" Y="-23.6688" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="229" G="229" B="229" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="head" ActionTag="-526623346" Tag="1444" IconVisible="True" LeftMargin="-291.7449" RightMargin="291.7449" TopMargin="90.2607" BottomMargin="-90.2607" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="-291.7449" Y="-90.2607" />
            <Scale ScaleX="0.7000" ScaleY="0.7000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="game_public/csb/head.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="card_1" ActionTag="188994268" Tag="9818" IconVisible="True" LeftMargin="-213.1045" RightMargin="213.1045" TopMargin="70.9624" BottomMargin="-70.9624" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="-213.1045" Y="-70.9624" />
            <Scale ScaleX="0.3200" ScaleY="0.3200" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="common/card.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="card_2" ActionTag="1379481393" Tag="9826" IconVisible="True" LeftMargin="-195.5249" RightMargin="195.5249" TopMargin="70.9624" BottomMargin="-70.9624" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="-195.5249" Y="-70.9624" />
            <Scale ScaleX="0.3200" ScaleY="0.3200" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="common/card.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="card_3" ActionTag="1632470279" Tag="9834" IconVisible="True" LeftMargin="-177.9454" RightMargin="177.9454" TopMargin="70.9624" BottomMargin="-70.9624" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="-177.9454" Y="-70.9624" />
            <Scale ScaleX="0.3200" ScaleY="0.3200" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="common/card.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="card_4" ActionTag="483474596" Tag="9842" IconVisible="True" LeftMargin="-160.3658" RightMargin="160.3658" TopMargin="70.9624" BottomMargin="-70.9624" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="-160.3658" Y="-70.9624" />
            <Scale ScaleX="0.3200" ScaleY="0.3200" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="common/card.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="card_5" ActionTag="1671169701" Tag="9850" IconVisible="True" LeftMargin="-142.7863" RightMargin="142.7863" TopMargin="70.9624" BottomMargin="-70.9624" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="-142.7863" Y="-70.9624" />
            <Scale ScaleX="0.3200" ScaleY="0.3200" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="common/card.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="card_6" ActionTag="-1773153597" Tag="9858" IconVisible="True" LeftMargin="-125.2067" RightMargin="125.2067" TopMargin="70.9624" BottomMargin="-70.9624" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="-125.2067" Y="-70.9624" />
            <Scale ScaleX="0.3200" ScaleY="0.3200" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="common/card.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="card_7" ActionTag="1794519339" Tag="9866" IconVisible="True" LeftMargin="-107.6271" RightMargin="107.6271" TopMargin="70.9624" BottomMargin="-70.9624" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="-107.6271" Y="-70.9624" />
            <Scale ScaleX="0.3200" ScaleY="0.3200" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="common/card.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="card_8" ActionTag="-878670221" Tag="9874" IconVisible="True" LeftMargin="-90.0476" RightMargin="90.0476" TopMargin="70.9624" BottomMargin="-70.9624" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="-90.0476" Y="-70.9624" />
            <Scale ScaleX="0.3200" ScaleY="0.3200" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="common/card.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="card_9" ActionTag="-1817853493" Tag="9882" IconVisible="True" LeftMargin="-72.4680" RightMargin="72.4680" TopMargin="70.9624" BottomMargin="-70.9624" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="-72.4680" Y="-70.9624" />
            <Scale ScaleX="0.3200" ScaleY="0.3200" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="common/card.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="card_10" ActionTag="-772809329" Tag="9890" IconVisible="True" LeftMargin="-54.8884" RightMargin="54.8884" TopMargin="70.9624" BottomMargin="-70.9624" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="-54.8884" Y="-70.9624" />
            <Scale ScaleX="0.3200" ScaleY="0.3200" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="common/card.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="card_11" ActionTag="-1920652564" Tag="9898" IconVisible="True" LeftMargin="-213.1046" RightMargin="213.1046" TopMargin="112.2409" BottomMargin="-112.2409" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="-213.1046" Y="-112.2409" />
            <Scale ScaleX="0.3200" ScaleY="0.3200" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="common/card.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="card_12" ActionTag="105072637" Tag="9906" IconVisible="True" LeftMargin="-195.5251" RightMargin="195.5251" TopMargin="112.2409" BottomMargin="-112.2409" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="-195.5251" Y="-112.2409" />
            <Scale ScaleX="0.3200" ScaleY="0.3200" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="common/card.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="card_13" ActionTag="-1953205051" Tag="9914" IconVisible="True" LeftMargin="-177.9459" RightMargin="177.9459" TopMargin="112.2409" BottomMargin="-112.2409" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="-177.9459" Y="-112.2409" />
            <Scale ScaleX="0.3200" ScaleY="0.3200" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="common/card.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="card_14" ActionTag="1033204634" Tag="9922" IconVisible="True" LeftMargin="-160.3660" RightMargin="160.3660" TopMargin="112.2409" BottomMargin="-112.2409" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="-160.3660" Y="-112.2409" />
            <Scale ScaleX="0.3200" ScaleY="0.3200" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="common/card.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="card_15" ActionTag="-1956829955" Tag="9930" IconVisible="True" LeftMargin="-142.7866" RightMargin="142.7866" TopMargin="112.2409" BottomMargin="-112.2409" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="-142.7866" Y="-112.2409" />
            <Scale ScaleX="0.3200" ScaleY="0.3200" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="common/card.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="card_16" ActionTag="-1784720835" Tag="9938" IconVisible="True" LeftMargin="-125.2070" RightMargin="125.2070" TopMargin="112.2409" BottomMargin="-112.2409" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="-125.2070" Y="-112.2409" />
            <Scale ScaleX="0.3200" ScaleY="0.3200" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="common/card.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="card_17" ActionTag="-573762062" Tag="9946" IconVisible="True" LeftMargin="-107.6275" RightMargin="107.6275" TopMargin="112.2409" BottomMargin="-112.2409" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="-107.6275" Y="-112.2409" />
            <Scale ScaleX="0.3200" ScaleY="0.3200" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="common/card.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="card_18" ActionTag="444884703" Tag="9954" IconVisible="True" LeftMargin="-90.0479" RightMargin="90.0479" TopMargin="112.2409" BottomMargin="-112.2409" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="-90.0479" Y="-112.2409" />
            <Scale ScaleX="0.3200" ScaleY="0.3200" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="common/card.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="card_19" ActionTag="165788423" Tag="9962" IconVisible="True" LeftMargin="-72.4684" RightMargin="72.4684" TopMargin="112.2409" BottomMargin="-112.2409" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="-72.4684" Y="-112.2409" />
            <Scale ScaleX="0.3200" ScaleY="0.3200" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="common/card.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="card_20" ActionTag="1555329310" Tag="9970" IconVisible="True" LeftMargin="-54.8889" RightMargin="54.8889" TopMargin="112.2409" BottomMargin="-112.2409" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="-54.8889" Y="-112.2409" />
            <Scale ScaleX="0.3200" ScaleY="0.3200" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="common/card.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="landlordTitle" ActionTag="1319793120" VisibleForFrame="False" Tag="522" IconVisible="False" LeftMargin="-286.4924" RightMargin="212.4924" TopMargin="-11.1693" BottomMargin="-102.8307" ctype="SpriteObjectData">
            <Size X="74.0000" Y="114.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-249.4924" Y="-45.8307" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="landlord/lan/cn/gameover_land_flag.png" Plist="landlord/lan/cn/game.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>