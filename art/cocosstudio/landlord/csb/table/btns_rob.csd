<GameFile>
  <PropertyGroup Name="btns_rob" Type="Node" ID="264758e7-4966-4b86-8a4a-ba64b3a71601" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="26" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="pass_btn" ActionTag="537298199" Tag="36" IconVisible="False" LeftMargin="-432.0000" RightMargin="228.0000" TopMargin="-0.4995" BottomMargin="-80.5005" ctype="SpriteObjectData">
            <Size X="204.0000" Y="81.0000" />
            <Children>
              <AbstractNodeData Name="title" ActionTag="1753401617" Tag="15" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-7.0000" RightMargin="-7.0000" TopMargin="-8.9902" BottomMargin="-4.0098" ctype="SpriteObjectData">
                <Size X="218.0000" Y="94.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="102.0000" Y="42.9902" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5307" />
                <PreSize X="1.0686" Y="1.1605" />
                <FileData Type="MarkedSubImage" Path="landlord/lan/cn/btn_word_bujiao.png" Plist="landlord/lan/cn/game.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-330.0000" Y="-40.0005" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="landlord/res/table/landlord_btn_orange.png" Plist="landlord/res/landlord.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="rob_btn_1" ActionTag="-370147901" Tag="37" IconVisible="False" LeftMargin="-212.5000" RightMargin="7.5000" TopMargin="-0.9995" BottomMargin="-81.0005" ctype="SpriteObjectData">
            <Size X="205.0000" Y="82.0000" />
            <Children>
              <AbstractNodeData Name="title" ActionTag="-1670306532" Tag="18" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-5.5570" RightMargin="-7.4430" TopMargin="-8.4902" BottomMargin="-3.5098" ctype="SpriteObjectData">
                <Size X="218.0000" Y="94.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="103.4430" Y="43.4902" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5046" Y="0.5304" />
                <PreSize X="1.0634" Y="1.1463" />
                <FileData Type="MarkedSubImage" Path="landlord/lan/cn/btn_word_score1.png" Plist="landlord/lan/cn/game.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-110.0000" Y="-40.0005" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="landlord/res/table/landlord_btn_blue.png" Plist="landlord/res/landlord.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="rob_btn_2" ActionTag="1207648692" Tag="1618" IconVisible="False" LeftMargin="7.5000" RightMargin="-212.5000" TopMargin="-0.9995" BottomMargin="-81.0005" ctype="SpriteObjectData">
            <Size X="205.0000" Y="82.0000" />
            <Children>
              <AbstractNodeData Name="title" ActionTag="1433086744" Tag="17" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-4.6141" RightMargin="-8.3860" TopMargin="-8.4902" BottomMargin="-3.5098" ctype="SpriteObjectData">
                <Size X="218.0000" Y="94.0000" />
                <AnchorPoint ScaleX="0.5226" ScaleY="0.6674" />
                <Position X="109.3127" Y="59.2258" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5332" Y="0.7223" />
                <PreSize X="1.0634" Y="1.1463" />
                <FileData Type="MarkedSubImage" Path="landlord/lan/cn/btn_word_score2.png" Plist="landlord/lan/cn/game.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="110.0000" Y="-40.0005" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="landlord/res/table/landlord_btn_blue.png" Plist="landlord/res/landlord.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="rob_btn_3" ActionTag="-1484471195" Tag="1619" IconVisible="False" LeftMargin="227.5000" RightMargin="-432.5000" TopMargin="-0.9995" BottomMargin="-81.0005" ctype="SpriteObjectData">
            <Size X="205.0000" Y="82.0000" />
            <Children>
              <AbstractNodeData Name="title" ActionTag="1227165144" Tag="16" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-3.6708" RightMargin="-9.3292" TopMargin="-8.4902" BottomMargin="-3.5098" ctype="SpriteObjectData">
                <Size X="218.0000" Y="94.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="105.3292" Y="43.4902" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5138" Y="0.5304" />
                <PreSize X="1.0634" Y="1.1463" />
                <FileData Type="MarkedSubImage" Path="landlord/lan/cn/btn_word_score3.png" Plist="landlord/lan/cn/game.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="330.0000" Y="-40.0005" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="landlord/res/table/landlord_btn_blue.png" Plist="landlord/res/landlord.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="clock" ActionTag="-1710966866" Tag="4073" IconVisible="True" TopMargin="-55.0000" BottomMargin="55.0000" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position Y="55.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="landlord/csb/table/clock.csd" Plist="" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>