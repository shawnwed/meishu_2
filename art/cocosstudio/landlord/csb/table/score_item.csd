<GameFile>
  <PropertyGroup Name="score_item_0" Type="Node" ID="f6166a10-1883-445a-a2fd-2bd00474e1d4" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="15168" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="head" ActionTag="1486503528" Tag="15201" IconVisible="True" LeftMargin="67.2719" RightMargin="-67.2719" TopMargin="-49.0000" BottomMargin="49.0000" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="67.2719" Y="49.0000" />
            <Scale ScaleX="0.7000" ScaleY="0.7000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="game_public/csb/head.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="score" ActionTag="-1190284600" Tag="15204" IconVisible="False" LeftMargin="242.7723" RightMargin="-329.7723" TopMargin="-62.0000" BottomMargin="36.0000" FontSize="22" LabelText="+300000" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="87.0000" Y="26.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="286.2723" Y="49.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="229" G="229" B="229" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="name" ActionTag="-978836174" Tag="15205" IconVisible="False" LeftMargin="121.5815" RightMargin="-200.5815" TopMargin="-62.0000" BottomMargin="36.0000" FontSize="22" LabelText="我是谁.." ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="79.0000" Y="26.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="121.5815" Y="49.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="229" G="229" B="229" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>