<GameFile>
  <PropertyGroup Name="player_avatar" Type="Node" ID="039916af-5499-485c-a98d-0ed42891d295" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="179" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="head" ActionTag="-34219702" Tag="58" IconVisible="True" TopMargin="-60.0004" BottomMargin="60.0004" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position Y="60.0004" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="game_public/csb/head.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="auto_play" ActionTag="-582296964" Tag="4923" IconVisible="False" LeftMargin="-20.5001" RightMargin="-20.4999" TopMargin="-83.0000" BottomMargin="37.0000" ctype="SpriteObjectData">
            <Size X="41.0000" Y="46.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-0.0001" Y="60.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="landlord/res/table/auto_robot.png" Plist="landlord/res/landlord.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="identity_flag" ActionTag="-325514391" Tag="189" IconVisible="False" LeftMargin="37.0000" RightMargin="-83.0000" TopMargin="-95.9999" BottomMargin="49.9999" ctype="SpriteObjectData">
            <Size X="46.0000" Y="75.0000" />
            <AnchorPoint ScaleX="0.5000" />
            <Position X="60.0000" Y="49.9999" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="landlord/lan/cn/landlord_identity_1.png" Plist="landlord/lan/cn/game.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="score_bg" ActionTag="482654116" Tag="185" IconVisible="False" LeftMargin="-81.5000" RightMargin="-79.5000" TopMargin="-19.9978" BottomMargin="-38.0022" ctype="SpriteObjectData">
            <Size X="161.0000" Y="58.0000" />
            <Children>
              <AbstractNodeData Name="coin_icon" ActionTag="695299137" Tag="184" IconVisible="False" LeftMargin="14.3127" RightMargin="116.6873" TopMargin="27.5958" BottomMargin="-2.5958" ctype="SpriteObjectData">
                <Size X="30.0000" Y="33.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="29.3127" Y="13.9042" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1821" Y="0.2397" />
                <PreSize X="0.1863" Y="0.5690" />
                <FileData Type="MarkedSubImage" Path="game_public/res/coin_small_icon.png" Plist="game_public/res/game.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="coin_txt" ActionTag="541890381" Tag="186" IconVisible="False" LeftMargin="47.2324" RightMargin="36.7676" TopMargin="31.2224" BottomMargin="4.7776" FontSize="22" LabelText="1234.56" HorizontalAlignmentType="HT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="77.0000" Y="22.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="85.7324" Y="15.7776" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="227" B="74" />
                <PrePosition X="0.5325" Y="0.2720" />
                <PreSize X="0.4783" Y="0.3793" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-1.0000" Y="-9.0022" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="landlord/res/table/landlord_score_bg_2.png" Plist="landlord/res/landlord.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="name_txt" ActionTag="475586430" Tag="183" IconVisible="False" LeftMargin="-50.0000" RightMargin="-46.0000" TopMargin="-16.7280" BottomMargin="-7.2720" FontSize="24" LabelText="洞房不败" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="96.0000" Y="24.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-2.0000" Y="4.7280" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="dissolve" ActionTag="1050447524" VisibleForFrame="False" Tag="934" IconVisible="False" LeftMargin="-50.2080" RightMargin="-54.7920" TopMargin="-84.8330" BottomMargin="44.8330" TouchEnable="True" ClipAble="False" BackColorAlpha="153" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="105.0000" Y="40.0000" />
            <Children>
              <AbstractNodeData Name="txt" ActionTag="490529041" Tag="935" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="15.0000" RightMargin="15.0000" TopMargin="7.5000" BottomMargin="7.5000" FontSize="25" LabelText="投票中" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="75.0000" Y="25.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="52.5000" Y="20.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5000" />
                <PreSize X="0.7143" Y="0.6250" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position X="-50.2080" Y="44.8330" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <SingleColor A="255" R="0" G="0" B="0" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>