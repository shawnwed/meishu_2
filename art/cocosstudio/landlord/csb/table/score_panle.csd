<GameFile>
  <PropertyGroup Name="score_panle" Type="Node" ID="40c413b7-de99-4f61-bd2f-f2b70eefa9f4" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="25" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="mask" ActionTag="-1617179873" Tag="15" IconVisible="False" LeftMargin="-640.0000" RightMargin="-640.0000" TopMargin="-360.0000" BottomMargin="-360.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="1280.0000" Y="720.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <SingleColor A="255" R="0" G="0" B="0" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="bg" ActionTag="-114651577" Tag="49" IconVisible="False" LeftMargin="-457.5000" RightMargin="-457.5000" TopMargin="-287.0000" BottomMargin="-289.0000" ctype="SpriteObjectData">
            <Size X="915.0000" Y="576.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position Y="-1.0000" />
            <Scale ScaleX="0.8900" ScaleY="0.8900" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="game_public/res/setting_bg.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="line1" ActionTag="1513028570" Tag="50" IconVisible="False" LeftMargin="-221.4999" RightMargin="80.4999" TopMargin="-218.1511" BottomMargin="196.1511" ctype="SpriteObjectData">
            <Size X="141.0000" Y="22.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-150.9999" Y="207.1511" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="game_public/res/title_line.png" Plist="game_public/res/game.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="line2" ActionTag="-572748722" Tag="53" IconVisible="False" LeftMargin="80.5000" RightMargin="-221.5000" TopMargin="-218.1511" BottomMargin="196.1511" FlipX="True" ctype="SpriteObjectData">
            <Size X="141.0000" Y="22.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="151.0000" Y="207.1511" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="game_public/res/title_line.png" Plist="game_public/res/game.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="title" ActionTag="-2065311283" Tag="52" IconVisible="False" LeftMargin="-47.0000" RightMargin="-47.0000" TopMargin="-230.6512" BottomMargin="183.6512" ctype="SpriteObjectData">
            <Size X="94.0000" Y="47.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position Y="207.1512" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="landlord/lan/cn/word_score.png" Plist="landlord/lan/cn/game.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="prev_panel" ActionTag="-438514933" Tag="54" IconVisible="False" LeftMargin="-359.0779" RightMargin="-361.9221" TopMargin="-156.5260" BottomMargin="6.5260" TouchEnable="True" ClipAble="False" BackColorAlpha="182" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="721.0000" Y="150.0000" />
            <Children>
              <AbstractNodeData Name="txt" ActionTag="-2028113230" Tag="55" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="284.5248" RightMargin="156.4752" TopMargin="37.0000" BottomMargin="43.0000" FontSize="70" LabelText="+9999999" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                <Size X="280.0000" Y="70.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="424.5248" Y="78.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="227" B="69" />
                <PrePosition X="0.5888" Y="0.5200" />
                <PreSize X="0.3883" Y="0.4667" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="word" ActionTag="-1806129836" Tag="51" IconVisible="False" LeftMargin="88.5000" RightMargin="537.5000" TopMargin="27.5000" BottomMargin="27.5000" ctype="SpriteObjectData">
                <Size X="95.0000" Y="95.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="136.0000" Y="75.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1886" Y="0.5000" />
                <PreSize X="0.1318" Y="0.6333" />
                <FileData Type="MarkedSubImage" Path="landlord/lan/cn/word_prev_score.png" Plist="landlord/lan/cn/game.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position X="-359.0779" Y="6.5260" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <SingleColor A="255" R="192" G="159" B="143" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="h_line_6" ActionTag="1173232572" Tag="56" IconVisible="False" LeftMargin="-3.1612" RightMargin="-0.8388" TopMargin="-83.7298" BottomMargin="-293.2702" ctype="SpriteObjectData">
            <Size X="4.0000" Y="377.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-1.1612" Y="-104.7702" />
            <Scale ScaleX="1.0000" ScaleY="0.6700" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="game_public/res/h_line.png" Plist="game_public/res/game.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="title_room" ActionTag="1628298773" Tag="57" IconVisible="False" LeftMargin="-278.3234" RightMargin="128.3234" TopMargin="131.8041" BottomMargin="-161.8041" FontSize="30" LabelText="本房間輸贏" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="150.0000" Y="30.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-203.3234" Y="-146.8041" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="81" G="70" B="69" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="title_day" ActionTag="348977844" Tag="58" IconVisible="False" LeftMargin="125.0000" RightMargin="-245.0000" TopMargin="130.6757" BottomMargin="-160.6757" FontSize="30" LabelText="今日輸贏" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="120.0000" Y="30.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="185.0000" Y="-145.6757" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="81" G="70" B="69" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="score_room" ActionTag="1381265412" Tag="59" IconVisible="False" LeftMargin="-253.3232" RightMargin="153.3232" TopMargin="56.8040" BottomMargin="-96.8040" FontSize="40" LabelText="50000" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
            <Size X="100.0000" Y="40.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-203.3232" Y="-76.8040" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="165" G="42" B="42" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="score_day" ActionTag="1422837305" Tag="60" IconVisible="False" LeftMargin="122.5000" RightMargin="-247.5000" TopMargin="58.6756" BottomMargin="-108.6756" FontSize="50" LabelText="50000" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
            <Size X="125.0000" Y="50.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="185.0000" Y="-83.6756" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="165" G="42" B="42" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="close" ActionTag="-258197931" Tag="14" IconVisible="False" LeftMargin="349.9793" RightMargin="-429.9793" TopMargin="-270.1409" BottomMargin="189.1409" ctype="SpriteObjectData">
            <Size X="80.0000" Y="81.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="389.9793" Y="229.6409" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="game_public/res/user_info_close.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>