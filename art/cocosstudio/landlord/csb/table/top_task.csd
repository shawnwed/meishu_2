<GameFile>
  <PropertyGroup Name="top_task" Type="Node" ID="d5ab9491-a53b-4cac-bc90-f757e9ece567" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="1985" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="bg" ActionTag="-2045599891" Tag="1987" IconVisible="False" LeftMargin="-157.0000" RightMargin="-157.0000" BottomMargin="-90.0000" ctype="SpriteObjectData">
            <Size X="314.0000" Y="90.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="1.0000" />
            <Position />
            <Scale ScaleX="1.1000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="landlord/res/table/top_task_bg.png" Plist="landlord/res/landlord.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="title" ActionTag="2015227455" Tag="1988" IconVisible="False" LeftMargin="-150.1348" RightMargin="-101.8652" TopMargin="9.7409" BottomMargin="-41.7409" FontSize="28" LabelText="打出两个炸弹并获胜" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="252.0000" Y="32.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="-150.1348" Y="-25.7409" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="progress" ActionTag="120938811" Tag="1989" IconVisible="False" LeftMargin="137.3284" RightMargin="-158.3284" TopMargin="13.4127" BottomMargin="-42.4128" FontSize="25" LabelText=" 2" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="21.0000" Y="29.0000" />
            <Children>
              <AbstractNodeData Name="txt" ActionTag="516073027" Tag="1990" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="-18.0000" RightMargin="25.0000" FontSize="25" LabelText="1" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="14.0000" Y="29.0000" />
                <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
                <Position X="-4.0000" Y="14.5000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="35" G="222" B="58" />
                <PrePosition X="-0.1905" Y="0.5000" />
                <PreSize X="0.6667" Y="1.0000" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="txt_0" ActionTag="-837884318" Tag="591" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="-2.0000" RightMargin="16.0000" TopMargin="1.5000" BottomMargin="1.5000" FontSize="22" LabelText="/" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="7.0000" Y="26.0000" />
                <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
                <Position X="5.0000" Y="14.5000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2381" Y="0.5000" />
                <PreSize X="0.3333" Y="0.8966" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.7000" ScaleY="0.5382" />
            <Position X="152.0288" Y="-26.8047" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="awardtxt" ActionTag="959204524" Tag="1991" IconVisible="False" LeftMargin="-121.1343" RightMargin="46.1343" TopMargin="51.9579" BottomMargin="-80.9579" FontSize="25" LabelText="獎勵：" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="75.0000" Y="29.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-83.6343" Y="-66.4579" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="diamond" ActionTag="887269617" Tag="1992" IconVisible="False" LeftMargin="-46.1354" RightMargin="18.1354" TopMargin="51.9582" BottomMargin="-80.9582" FontSize="25" LabelText="20" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="28.0000" Y="29.0000" />
            <Children>
              <AbstractNodeData Name="icon" ActionTag="-1099235199" Tag="1993" IconVisible="False" LeftMargin="41.5483" RightMargin="-46.5483" TopMargin="-3.2138" BottomMargin="0.2138" ctype="SpriteObjectData">
                <Size X="33.0000" Y="32.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="58.0483" Y="16.2138" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="2.0732" Y="0.5591" />
                <PreSize X="1.1786" Y="1.1034" />
                <FileData Type="MarkedSubImage" Path="landlord/res/table/top_task_diamond.png" Plist="landlord/res/landlord.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleY="0.5000" />
            <Position X="-46.1354" Y="-66.4582" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="251" G="226" B="91" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="coin" ActionTag="-964298120" Tag="1994" IconVisible="False" LeftMargin="44.8638" RightMargin="-72.8638" TopMargin="51.9582" BottomMargin="-80.9582" FontSize="25" LabelText="20" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="28.0000" Y="29.0000" />
            <Children>
              <AbstractNodeData Name="icon" ActionTag="289695805" Tag="1995" IconVisible="False" LeftMargin="41.5480" RightMargin="-46.5480" TopMargin="-1.2137" BottomMargin="-1.7863" ctype="SpriteObjectData">
                <Size X="33.0000" Y="32.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="58.0480" Y="14.2137" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="2.0731" Y="0.4901" />
                <PreSize X="1.1786" Y="1.1034" />
                <FileData Type="MarkedSubImage" Path="landlord/res/table/top_task_coin.png" Plist="landlord/res/landlord.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleY="0.5000" />
            <Position X="44.8638" Y="-66.4582" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="251" G="226" B="91" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="light" ActionTag="-754009566" Tag="2121" IconVisible="False" LeftMargin="93.0000" RightMargin="-161.0000" TopMargin="83.5600" BottomMargin="-91.5600" ctype="SpriteObjectData">
            <Size X="68.0000" Y="8.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="127.0000" Y="-87.5600" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="landlord/res/table/top_task_light.png" Plist="landlord/res/landlord.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="complete" ActionTag="-842237665" Tag="256" IconVisible="False" LeftMargin="115.0198" RightMargin="-164.0198" TopMargin="1.2409" BottomMargin="-50.2409" ctype="SpriteObjectData">
            <Size X="49.0000" Y="49.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="139.5198" Y="-25.7409" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="landlord/lan/cn/top_task_complete.png" Plist="landlord/lan/cn/game.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>