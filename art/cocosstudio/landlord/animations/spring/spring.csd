<GameFile>
  <PropertyGroup Name="spring" Type="Node" ID="9bca7c24-b8ae-496d-b7c5-5b806ae44265" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="120" Speed="0.7500">
        <Timeline ActionTag="-1125642064" Property="FileData">
          <TextureFrame FrameIndex="0" Tween="False">
            <TextureFile Type="MarkedSubImage" Path="landlord/animations/spring/spring01.png" Plist="landlord/animations/spring/spring_tex.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="5" Tween="False">
            <TextureFile Type="MarkedSubImage" Path="landlord/animations/spring/spring02.png" Plist="landlord/animations/spring/spring_tex.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="10" Tween="False">
            <TextureFile Type="MarkedSubImage" Path="landlord/animations/spring/spring03.png" Plist="landlord/animations/spring/spring_tex.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="15" Tween="False">
            <TextureFile Type="MarkedSubImage" Path="landlord/animations/spring/spring04.png" Plist="landlord/animations/spring/spring_tex.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="20" Tween="False">
            <TextureFile Type="MarkedSubImage" Path="landlord/animations/spring/spring05.png" Plist="landlord/animations/spring/spring_tex.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="25" Tween="False">
            <TextureFile Type="MarkedSubImage" Path="landlord/animations/spring/spring06.png" Plist="landlord/animations/spring/spring_tex.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="30" Tween="False">
            <TextureFile Type="MarkedSubImage" Path="landlord/animations/spring/spring07.png" Plist="landlord/animations/spring/spring_tex.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="35" Tween="False">
            <TextureFile Type="MarkedSubImage" Path="landlord/animations/spring/spring08.png" Plist="landlord/animations/spring/spring_tex.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="40" Tween="False">
            <TextureFile Type="MarkedSubImage" Path="landlord/animations/spring/spring09.png" Plist="landlord/animations/spring/spring_tex.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="45" Tween="False">
            <TextureFile Type="MarkedSubImage" Path="landlord/animations/spring/spring10.png" Plist="landlord/animations/spring/spring_tex.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="50" Tween="False">
            <TextureFile Type="MarkedSubImage" Path="landlord/animations/spring/spring11.png" Plist="landlord/animations/spring/spring_tex.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="55" Tween="False">
            <TextureFile Type="MarkedSubImage" Path="landlord/animations/spring/spring16.png" Plist="landlord/animations/spring/spring_tex.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="60" Tween="False">
            <TextureFile Type="MarkedSubImage" Path="landlord/animations/spring/spring13.png" Plist="landlord/animations/spring/spring_tex.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="65" Tween="False">
            <TextureFile Type="MarkedSubImage" Path="landlord/animations/spring/spring14.png" Plist="landlord/animations/spring/spring_tex.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="70" Tween="False">
            <TextureFile Type="MarkedSubImage" Path="landlord/animations/spring/spring15.png" Plist="landlord/animations/spring/spring_tex.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="75" Tween="False">
            <TextureFile Type="MarkedSubImage" Path="landlord/animations/spring/spring01.png" Plist="landlord/animations/spring/spring_tex.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="120" Tween="False">
            <TextureFile Type="MarkedSubImage" Path="landlord/animations/spring/spring16.png" Plist="landlord/animations/spring/spring_tex.plist" />
          </TextureFrame>
        </Timeline>
        <Timeline ActionTag="-1125642064" Property="Position">
          <PointFrame FrameIndex="120" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </PointFrame>
        </Timeline>
        <Timeline ActionTag="-1125642064" Property="Scale">
          <ScaleFrame FrameIndex="120" X="1.0000" Y="1.0000">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="-1125642064" Property="RotationSkew">
          <ScaleFrame FrameIndex="120" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
      </Animation>
      <ObjectData Name="Node" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="Sprite_1" ActionTag="-1125642064" Tag="8257" IconVisible="False" LeftMargin="-200.0000" RightMargin="-200.0000" TopMargin="-115.0000" BottomMargin="-115.0000" ctype="SpriteObjectData">
            <Size X="400.0000" Y="230.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="landlord/animations/spring/spring01.png" Plist="landlord/animations/spring/spring_tex.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>