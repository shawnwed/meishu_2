<GameFile>
  <PropertyGroup Name="animations" Type="Node" ID="a18ea437-a7a0-4195-a2bf-33389379c4dd" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="11" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="light_1" ActionTag="584956169" Tag="8410" IconVisible="False" LeftMargin="-255.1391" RightMargin="-258.8609" TopMargin="-112.7937" BottomMargin="-107.2063" ctype="SpriteObjectData">
            <Size X="514.0000" Y="220.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1.8609" Y="2.7937" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="landlord/animations/pairStraight/light.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="word_2" ActionTag="1496096374" Tag="8411" IconVisible="False" LeftMargin="-63.0014" RightMargin="-62.9986" TopMargin="-132.4019" BottomMargin="59.4019" ctype="SpriteObjectData">
            <Size X="126.0000" Y="73.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-0.0014" Y="95.9019" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="landlord/animations/pairStraight/pair_straight_word.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="single_straight_word_4" ActionTag="-1841040873" Tag="8413" IconVisible="False" LeftMargin="-52.7598" RightMargin="-73.2402" TopMargin="32.8997" BottomMargin="-104.8997" ctype="SpriteObjectData">
            <Size X="126.0000" Y="72.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="10.2402" Y="-68.8997" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="landlord/animations/pairStraight/single_straight_word.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>