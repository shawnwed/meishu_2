<GameFile>
  <PropertyGroup Name="plane_ani" Type="Node" ID="77e0c63b-fb14-45de-a8f1-784f584997fd" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="7947" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="smoke" ActionTag="45895219" Tag="7952" IconVisible="True" LeftMargin="-108.9431" RightMargin="108.9431" TopMargin="-39.1063" BottomMargin="39.1063" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="-108.9431" Y="39.1063" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="landlord/animations/warplane/smoke.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="plane_1" ActionTag="2042357186" Tag="7948" RotationSkewX="-4.9003" RotationSkewY="-4.9004" IconVisible="False" LeftMargin="-183.0002" RightMargin="-182.9998" TopMargin="-161.0000" BottomMargin="-161.0000" ctype="SpriteObjectData">
            <Size X="366.0000" Y="322.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-0.0002" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="landlord/animations/warplane/plane.png" Plist="landlord/animations/warplane/warplane.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>