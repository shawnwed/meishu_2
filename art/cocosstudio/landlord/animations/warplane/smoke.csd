<GameFile>
  <PropertyGroup Name="smoke" Type="Node" ID="77a9b3f2-7010-453a-9d3e-e107411009cc" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="20" Speed="1.0000">
        <Timeline ActionTag="-813491939" Property="FileData">
          <TextureFrame FrameIndex="0" Tween="False">
            <TextureFile Type="MarkedSubImage" Path="landlord/animations/warplane/smoke01.png" Plist="landlord/animations/warplane/warplane.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="5" Tween="False">
            <TextureFile Type="MarkedSubImage" Path="landlord/animations/warplane/smoke02.png" Plist="landlord/animations/warplane/warplane.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="10" Tween="False">
            <TextureFile Type="MarkedSubImage" Path="landlord/animations/warplane/smoke03.png" Plist="landlord/animations/warplane/warplane.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="15" Tween="False">
            <TextureFile Type="MarkedSubImage" Path="landlord/animations/warplane/smoke04.png" Plist="landlord/animations/warplane/warplane.plist" />
          </TextureFrame>
          <TextureFrame FrameIndex="20" Tween="False">
            <TextureFile Type="MarkedSubImage" Path="landlord/animations/warplane/smoke05.png" Plist="landlord/animations/warplane/warplane.plist" />
          </TextureFrame>
        </Timeline>
      </Animation>
      <ObjectData Name="Node" Tag="7951" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="Sprite_2" ActionTag="-813491939" Tag="7950" IconVisible="False" LeftMargin="-492.0857" RightMargin="-7.9143" TopMargin="-75.0001" BottomMargin="-74.9999" FlipX="True" ctype="SpriteObjectData">
            <Size X="500.0000" Y="150.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-242.0857" Y="0.0001" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="landlord/animations/warplane/smoke03.png" Plist="landlord/animations/warplane/warplane.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>