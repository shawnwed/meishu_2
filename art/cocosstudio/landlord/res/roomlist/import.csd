<GameFile>
  <PropertyGroup Name="import" Type="Node" ID="257086e8-146c-4b32-bb81-7de439e6b2b0" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="1353" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="bet_select_1" ActionTag="-734443977" Tag="1354" IconVisible="False" LeftMargin="55.0599" RightMargin="-351.0599" TopMargin="-189.6733" BottomMargin="-187.3267" ctype="SpriteObjectData">
            <Size X="296.0000" Y="377.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="203.0599" Y="1.1733" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="landlord/res/roomlist/bet_select.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="bet_select_no_2" ActionTag="-2137607322" Tag="1355" IconVisible="False" LeftMargin="-1.9401" RightMargin="-408.0599" TopMargin="-276.1733" BottomMargin="-273.8267" ctype="SpriteObjectData">
            <Size X="410.0000" Y="550.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="203.0599" Y="1.1733" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="landlord/res/roomlist/bet_select_no.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>