<GameFile>
  <PropertyGroup Name="import" Type="Scene" ID="79a1c21a-b665-42e2-8580-53ab85718dcf" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Scene" Tag="1196" ctype="GameNodeObjectData">
        <Size X="960.0000" Y="640.0000" />
        <Children>
          <AbstractNodeData Name="BitmapFontLabel_1" Visible="False" ActionTag="-1937286481" Tag="1197" IconVisible="False" LeftMargin="289.6358" RightMargin="613.3642" TopMargin="222.3302" BottomMargin="377.6698" LabelText="111" ctype="TextBMFontObjectData">
            <Size X="57.0000" Y="40.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="318.1358" Y="397.6698" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3314" Y="0.6214" />
            <PreSize X="0.0594" Y="0.0625" />
            <LabelBMFontFile_CNB Type="Normal" Path="lan/cn/bottom3.fnt" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="BitmapFontLabel_2" Visible="False" ActionTag="1185886643" Tag="1198" IconVisible="False" LeftMargin="677.7371" RightMargin="229.2629" TopMargin="190.5166" BottomMargin="409.4834" LabelText="111" ctype="TextBMFontObjectData">
            <Size X="53.0000" Y="40.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="704.2371" Y="429.4834" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.7336" Y="0.6711" />
            <PreSize X="0.0552" Y="0.0625" />
            <LabelBMFontFile_CNB Type="Normal" Path="lan/cn/bottom1.fnt" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="bg_1" Visible="False" ActionTag="1846385188" Tag="655" IconVisible="False" LeftMargin="162.2852" RightMargin="11.7148" TopMargin="-4.0742" BottomMargin="136.0742" ctype="SpriteObjectData">
            <Size X="786.0000" Y="508.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="555.2852" Y="390.0742" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5784" Y="0.6095" />
            <PreSize X="0.8188" Y="0.7937" />
            <FileData Type="Normal" Path="lan/cn/bg.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="bg3_2" Visible="False" ActionTag="1779548616" Tag="656" IconVisible="False" LeftMargin="97.2852" RightMargin="-53.2852" TopMargin="-20.0742" BottomMargin="120.0742" ctype="SpriteObjectData">
            <Size X="916.0000" Y="540.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="555.2852" Y="390.0742" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5784" Y="0.6095" />
            <PreSize X="0.9542" Y="0.8438" />
            <FileData Type="Normal" Path="lan/cn/bg3.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="bg4_3" Visible="False" ActionTag="184594449" Tag="657" IconVisible="False" LeftMargin="97.2852" RightMargin="-53.2852" TopMargin="-20.0742" BottomMargin="120.0742" ctype="SpriteObjectData">
            <Size X="916.0000" Y="540.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="555.2852" Y="390.0742" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5784" Y="0.6095" />
            <PreSize X="0.9542" Y="0.8438" />
            <FileData Type="Default" Path="Default/Sprite.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="bg5_4" ActionTag="977270847" Tag="658" IconVisible="False" LeftMargin="67.2852" RightMargin="-83.2852" TopMargin="-21.5742" BottomMargin="118.5742" ctype="SpriteObjectData">
            <Size X="976.0000" Y="543.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="555.2852" Y="390.0742" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5784" Y="0.6095" />
            <PreSize X="1.0167" Y="0.8484" />
            <FileData Type="Default" Path="Default/Sprite.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="sign_bg_9" Visible="False" ActionTag="77251969" Tag="876" IconVisible="False" LeftMargin="85.4183" RightMargin="-41.4183" TopMargin="14.1521" BottomMargin="85.8479" ctype="SpriteObjectData">
            <Size X="916.0000" Y="540.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="543.4183" Y="355.8479" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5661" Y="0.5560" />
            <PreSize X="0.9542" Y="0.8438" />
            <FileData Type="Default" Path="Default/Sprite.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="bg5_2_1" Visible="False" ActionTag="-214841095" Tag="260" IconVisible="False" LeftMargin="-156.0000" RightMargin="140.0000" TopMargin="81.5000" BottomMargin="15.5000" ctype="SpriteObjectData">
            <Size X="976.0000" Y="543.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="332.0000" Y="287.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3458" Y="0.4484" />
            <PreSize X="1.0167" Y="0.8484" />
            <FileData Type="Default" Path="Default/Sprite.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>