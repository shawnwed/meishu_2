<GameFile>
  <PropertyGroup Name="mini_card" Type="Node" ID="fedce542-5bba-4ecd-a4a4-db7089b3c06b" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="565" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="white_bg" ActionTag="-29453791" Tag="566" IconVisible="False" LeftMargin="-23.0000" RightMargin="-23.0000" TopMargin="-23.0000" BottomMargin="-23.0000" ctype="SpriteObjectData">
            <Size X="136.0000" Y="179.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="common/cards/cards_white_bg.png" Plist="common/cards.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="cards_suit_1" ActionTag="-2008121995" Tag="568" IconVisible="False" LeftMargin="-10.7437" RightMargin="-23.2563" TopMargin="-9.8032" BottomMargin="-31.1968" ctype="SpriteObjectData">
            <Size X="34.0000" Y="41.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="6.2563" Y="-10.6968" />
            <Scale ScaleX="0.7500" ScaleY="0.7500" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="common/cards/cards_suit_0.png" Plist="common/cards.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="cards_points_1" ActionTag="268099707" Tag="570" IconVisible="False" LeftMargin="-32.0222" RightMargin="-6.9778" TopMargin="-43.9316" BottomMargin="-10.0684" ctype="SpriteObjectData">
            <Size X="39.0000" Y="54.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-12.5222" Y="16.9316" />
            <Scale ScaleX="0.4500" ScaleY="0.4500" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="common/cards/cards_points_1_1.png" Plist="common/cards.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>