<GameFile>
  <PropertyGroup Name="card" Type="Node" ID="7c20706e-cc3e-4e26-b279-3d17e6903aeb" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="31" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="white_bg" ActionTag="-308745915" Tag="33" IconVisible="False" LeftMargin="-68.0000" RightMargin="-68.0000" TopMargin="-89.5000" BottomMargin="-89.5000" ctype="SpriteObjectData">
            <Size X="136.0000" Y="179.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="common/cards/cards_white_bg.png" Plist="common/cards.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="cards_points_1" ActionTag="1626535007" Tag="34" IconVisible="False" LeftMargin="-63.4928" RightMargin="24.4928" TopMargin="-85.0000" BottomMargin="31.0000" ctype="SpriteObjectData">
            <Size X="39.0000" Y="54.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-43.9928" Y="58.0000" />
            <Scale ScaleX="0.9000" ScaleY="0.9000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="common/cards/cards_points_1_1.png" Plist="common/cards.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="cards_suit_3" ActionTag="715064988" Tag="397" IconVisible="False" LeftMargin="-66.0000" RightMargin="-66.0000" TopMargin="-87.5000" BottomMargin="-87.5000" ctype="SpriteObjectData">
            <Size X="132.0000" Y="175.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="0.9000" ScaleY="0.9000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="common/cards/k1.png" Plist="common/cards.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="cards_suit_1" ActionTag="-1792543279" Tag="36" IconVisible="False" LeftMargin="-60.9928" RightMargin="26.9928" TopMargin="-30.4698" BottomMargin="-10.5302" ctype="SpriteObjectData">
            <Size X="34.0000" Y="41.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-43.9928" Y="9.9698" />
            <Scale ScaleX="0.9000" ScaleY="0.9000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="common/cards/cards_suit_0.png" Plist="common/cards.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="cards_suit_2" ActionTag="888522440" Tag="37" IconVisible="False" LeftMargin="-27.8700" RightMargin="-59.1300" TopMargin="-15.4816" BottomMargin="-83.5184" ctype="SpriteObjectData">
            <Size X="87.0000" Y="99.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="15.6300" Y="-34.0184" />
            <Scale ScaleX="0.9000" ScaleY="0.9000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="common/cards/big_cards_suit_0.png" Plist="common/cards.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="landlord_flag" ActionTag="-1570570315" VisibleForFrame="False" Tag="94" IconVisible="False" LeftMargin="-18.5174" RightMargin="-66.4826" TopMargin="-88.0219" BottomMargin="2.0219" ctype="SpriteObjectData">
            <Size X="85.0000" Y="86.0000" />
            <AnchorPoint ScaleX="1.0000" ScaleY="1.0000" />
            <Position X="66.4826" Y="88.0219" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="common/cards/landlord_flag2.png" Plist="common/cards.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>