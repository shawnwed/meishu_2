<GameFile>
  <PropertyGroup Name="login" Type="Scene" ID="0dcef6a6-e460-4998-8bb6-3ec2ca508c46" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Scene" Tag="3" ctype="GameNodeObjectData">
        <Size X="960.0000" Y="640.0000" />
        <Children>
          <AbstractNodeData Name="bg" ActionTag="-1564113959" Tag="5" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" LeftEage="15" RightEage="15" TopEage="15" BottomEage="15" Scale9OriginX="15" Scale9OriginY="15" Scale9Width="1250" Scale9Height="690" ctype="ImageViewObjectData">
            <Size X="960.0000" Y="640.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="480.0000" Y="320.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="1.0000" Y="1.0000" />
            <FileData Type="Normal" Path="bg/login_bg.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="didi" ActionTag="-1214532963" Tag="1653" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-158.6474" RightMargin="-161.3525" TopMargin="428.2578" BottomMargin="99.7422" LeftEage="15" RightEage="15" TopEage="15" BottomEage="15" Scale9OriginX="15" Scale9OriginY="15" Scale9Width="1250" Scale9Height="82" ctype="ImageViewObjectData">
            <Size X="1280.0000" Y="112.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="481.3526" Y="155.7422" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5014" Y="0.2433" />
            <PreSize X="1.3333" Y="0.1750" />
            <FileData Type="Normal" Path="login/didi.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="debug" ActionTag="1283437040" Tag="1221" IconVisible="False" LeftMargin="19.9534" RightMargin="868.0466" TopMargin="581.9441" BottomMargin="34.0559" FontSize="24" LabelText="调试版" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="72.0000" Y="24.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="19.9534" Y="46.0559" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0208" Y="0.0720" />
            <PreSize X="0.0750" Y="0.0375" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="login_guest" ActionTag="1336227624" Tag="1675" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="485.0760" RightMargin="147.9240" TopMargin="476.0440" BottomMargin="66.9560" ctype="SpriteObjectData">
            <Size X="327.0000" Y="97.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="648.5760" Y="115.4560" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.6756" Y="0.1804" />
            <PreSize X="0.3406" Y="0.1516" />
            <FileData Type="Normal" Path="registersdk/kuaisuyouxi.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="login_fb" ActionTag="-825511865" VisibleForFrame="False" Tag="1676" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="180.6480" RightMargin="485.3520" TopMargin="436.0960" BottomMargin="107.9040" ctype="SpriteObjectData">
            <Size X="294.0000" Y="96.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="327.6480" Y="155.9040" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3413" Y="0.2436" />
            <PreSize X="0.3063" Y="0.1500" />
            <FileData Type="Normal" Path="lan/cn/fb_login.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="login_wx" Visible="False" ActionTag="-1315811965" Tag="36" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="207.8712" RightMargin="458.1288" TopMargin="437.0528" BottomMargin="106.9472" ctype="SpriteObjectData">
            <Size X="294.0000" Y="96.0000" />
            <AnchorPoint ScaleX="0.3852" ScaleY="0.5193" />
            <Position X="321.1200" Y="156.8000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3345" Y="0.2450" />
            <PreSize X="0.3063" Y="0.1500" />
            <FileData Type="MarkedSubImage" Path="lan/cn/tencent_wx.png" Plist="lan/cn/game.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="login_qq" Visible="False" ActionTag="1457242589" Tag="102" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="333.0000" RightMargin="333.0000" TopMargin="436.0960" BottomMargin="107.9040" ctype="SpriteObjectData">
            <Size X="294.0000" Y="96.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="480.0000" Y="155.9040" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.2436" />
            <PreSize X="0.3063" Y="0.1500" />
            <FileData Type="MarkedSubImage" Path="lan/cn/tencent_qq.png" Plist="lan/cn/game.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="login_zh" ActionTag="-858334878" Tag="185" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="148.0200" RightMargin="484.9800" TopMargin="476.0400" BottomMargin="66.9600" ctype="SpriteObjectData">
            <Size X="327.0000" Y="97.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="311.5200" Y="115.4600" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3245" Y="0.1804" />
            <PreSize X="0.3406" Y="0.1516" />
            <FileData Type="Normal" Path="registersdk/DL_shouji.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="logo_1" ActionTag="-1305388697" VisibleForFrame="False" Tag="11" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="157.2320" RightMargin="158.7680" TopMargin="-3.5000" BottomMargin="188.5000" ctype="SpriteObjectData">
            <Size X="644.0000" Y="455.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="479.2320" Y="416.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4992" Y="0.6500" />
            <PreSize X="0.6708" Y="0.7109" />
            <FileData Type="Normal" Path="login/logo.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="agreement" ActionTag="2063970484" Tag="1178" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="240.9280" RightMargin="319.0720" TopMargin="549.8960" BottomMargin="20.1040" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="400.0000" Y="70.0000" />
            <Children>
              <AbstractNodeData Name="check" ActionTag="-1116035234" Tag="1196" IconVisible="False" LeftMargin="41.5990" RightMargin="238.4010" TopMargin="5.5312" BottomMargin="-5.5312" ctype="SpriteObjectData">
                <Size X="120.0000" Y="70.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="101.5990" Y="29.4688" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2540" Y="0.4210" />
                <PreSize X="0.3000" Y="1.0000" />
                <FileData Type="MarkedSubImage" Path="lan/cn/check2.png" Plist="lan/cn/game.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="word3" ActionTag="-1388769915" Tag="1199" IconVisible="False" LeftMargin="306.7071" RightMargin="-22.7071" TopMargin="9.5312" BottomMargin="-9.5312" ctype="SpriteObjectData">
                <Size X="116.0000" Y="70.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="364.7071" Y="25.4688" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.9118" Y="0.3638" />
                <PreSize X="0.2900" Y="1.0000" />
                <FileData Type="MarkedSubImage" Path="lan/cn/agree_word3.png" Plist="lan/cn/game.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="word2" ActionTag="-346383769" Tag="1198" IconVisible="False" LeftMargin="278.6706" RightMargin="93.3294" TopMargin="9.5312" BottomMargin="-9.5312" ctype="SpriteObjectData">
                <Size X="28.0000" Y="70.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="292.6706" Y="25.4688" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7317" Y="0.3638" />
                <PreSize X="0.0700" Y="1.0000" />
                <FileData Type="MarkedSubImage" Path="lan/cn/agree_word2.png" Plist="lan/cn/game.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="word1" ActionTag="57076315" Tag="1197" IconVisible="False" LeftMargin="163.6344" RightMargin="125.3656" TopMargin="9.5312" BottomMargin="-9.5312" ctype="SpriteObjectData">
                <Size X="111.0000" Y="70.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="219.1344" Y="25.4688" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5478" Y="0.3638" />
                <PreSize X="0.2775" Y="1.0000" />
                <FileData Type="MarkedSubImage" Path="lan/cn/agree_word1.png" Plist="lan/cn/game.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="440.9280" Y="55.1040" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4593" Y="0.0861" />
            <PreSize X="0.4167" Y="0.1094" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="copyrightinfo" ActionTag="1787714391" Tag="14" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="38.5280" RightMargin="25.4720" TopMargin="618.8922" BottomMargin="5.1078" FontSize="16" LabelText="抵制不良游戏，拒绝盗版游戏。注意自我保护，谨防受骗上当。适度游戏益脑，沉迷游戏伤身。合理安排时间，享受健康生活。" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="896.0000" Y="16.0000" />
            <Children>
              <AbstractNodeData Name="txt2" ActionTag="-2081194654" Tag="15" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="-28.0000" RightMargin="-28.0000" TopMargin="35.9964" BottomMargin="-35.9964" FontSize="16" LabelText="软著登记号 2017SR252469 文网游备字 【2017】M-CBG 1681号 审批文号 新广出审【2017】7140号 出版物号 ISBN 978-7-498-00215-0" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="952.0000" Y="16.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="448.0000" Y="-27.9964" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="-1.7498" />
                <PreSize X="1.0625" Y="1.0000" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="486.5280" Y="13.1078" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="200" G="180" B="179" />
            <PrePosition X="0.5068" Y="0.0205" />
            <PreSize X="0.9333" Y="0.0250" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>