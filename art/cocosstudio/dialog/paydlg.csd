<GameFile>
  <PropertyGroup Name="paydlg" Type="Layer" ID="da127ef0-3b1c-4ef7-9ed4-c7abd94abff4" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="49" ctype="GameLayerObjectData">
        <Size X="1280.0000" Y="720.0000" />
        <Children>
          <AbstractNodeData Name="mask" ActionTag="-2123008681" Tag="55" IconVisible="False" PositionPercentXEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="1280.0000" Y="720.0000" />
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="1.0000" Y="1.0000" />
            <SingleColor A="255" R="0" G="0" B="0" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="bg" ActionTag="1732652221" Tag="56" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="250.5000" RightMargin="250.5000" TopMargin="72.0000" BottomMargin="72.0000" TouchEnable="True" LeftEage="259" RightEage="259" TopEage="167" BottomEage="167" Scale9OriginX="259" Scale9OriginY="167" Scale9Width="261" Scale9Height="242" ctype="ImageViewObjectData">
            <Size X="779.0000" Y="576.0000" />
            <Children>
              <AbstractNodeData Name="close" ActionTag="1408509273" Tag="57" IconVisible="False" LeftMargin="720.2120" RightMargin="-18.2120" TopMargin="62.8004" BottomMargin="436.1996" ctype="SpriteObjectData">
                <Size X="77.0000" Y="77.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="758.7120" Y="474.6996" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.9740" Y="0.8241" />
                <PreSize X="0.0988" Y="0.1337" />
                <FileData Type="MarkedSubImage" Path="dialog/res/broken/close.png" Plist="dialog/broken.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="pay_btn" ActionTag="-1071511118" Tag="58" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="443.1538" RightMargin="60.8462" TopMargin="449.1212" BottomMargin="31.8789" ctype="SpriteObjectData">
                <Size X="275.0000" Y="95.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="580.6538" Y="79.3789" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7454" Y="0.1378" />
                <PreSize X="0.3530" Y="0.1649" />
                <FileData Type="MarkedSubImage" Path="lan/cn/pay_btn.png" Plist="lan/cn/game.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="quick_start" ActionTag="28364451" Tag="59" IconVisible="False" LeftMargin="67.0000" RightMargin="438.0000" TopMargin="449.1212" BottomMargin="31.8789" ctype="SpriteObjectData">
                <Size X="274.0000" Y="95.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="204.0000" Y="79.3789" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2619" Y="0.1378" />
                <PreSize X="0.3517" Y="0.1649" />
                <FileData Type="MarkedSubImage" Path="lan/cn/quick_start.png" Plist="lan/cn/game.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="icon" ActionTag="1287396549" Tag="65" IconVisible="False" LeftMargin="105.5000" RightMargin="522.5000" TopMargin="290.9832" BottomMargin="179.0168" ctype="SpriteObjectData">
                <Size X="151.0000" Y="106.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="181.0000" Y="232.0168" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2323" Y="0.4028" />
                <PreSize X="0.1938" Y="0.1840" />
                <FileData Type="MarkedSubImage" Path="hall/res/store/money1.png" Plist="hall/res/store.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="change_btn" ActionTag="264437071" Tag="66" IconVisible="False" LeftMargin="609.0000" RightMargin="80.0000" TopMargin="299.0000" BottomMargin="187.0000" ctype="SpriteObjectData">
                <Size X="90.0000" Y="90.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="654.0000" Y="232.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.8395" Y="0.4028" />
                <PreSize X="0.1155" Y="0.1563" />
                <FileData Type="Normal" Path="dialog/res/paydlg/change_btn.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="name" ActionTag="-1174806968" Tag="67" IconVisible="False" LeftMargin="284.0000" RightMargin="399.0000" TopMargin="303.5000" BottomMargin="239.5000" FontSize="32" LabelText="15金币" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="96.0000" Y="33.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="284.0000" Y="256.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="254" G="235" B="118" />
                <PrePosition X="0.3646" Y="0.4444" />
                <PreSize X="0.1232" Y="0.0573" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="title" ActionTag="-508953136" Tag="69" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="194.5000" RightMargin="194.5000" TopMargin="185.0000" BottomMargin="361.0000" FontSize="30" LabelText="10金幣以上才能進入，請儲值" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="390.0000" Y="30.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="389.5000" Y="376.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="139" G="73" B="61" />
                <PrePosition X="0.5000" Y="0.6528" />
                <PreSize X="0.5006" Y="0.0521" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="price" ActionTag="2000055044" Tag="68" IconVisible="False" LeftMargin="285.0000" RightMargin="414.0000" TopMargin="358.5000" BottomMargin="184.5000" FontSize="32" LabelText="HK$15" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="80.0000" Y="33.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="285.0000" Y="201.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3659" Y="0.3490" />
                <PreSize X="0.1027" Y="0.0573" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="640.0000" Y="360.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="0.6086" Y="0.8000" />
            <FileData Type="Normal" Path="lan/cn/bg.png" Plist="" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>