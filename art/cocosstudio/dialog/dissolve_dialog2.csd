<GameFile>
  <PropertyGroup Name="dissolve_dialog2" Type="Layer" ID="2d7c21ed-9dbc-4238-9ddd-5b473002b41a" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="1261" ctype="GameLayerObjectData">
        <Size X="774.0000" Y="469.0000" />
        <Children>
          <AbstractNodeData Name="bg" ActionTag="1904987290" Tag="1269" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-70.5000" RightMargin="-70.5000" TopMargin="-53.5000" BottomMargin="-53.5000" ctype="SpriteObjectData">
            <Size X="915.0000" Y="576.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="387.0000" Y="234.5000" />
            <Scale ScaleX="0.8200" ScaleY="0.8200" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="1.1822" Y="1.2281" />
            <FileData Type="Normal" Path="game_public/res/setting_bg.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="cancel" ActionTag="1682407902" Tag="1262" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="424.8000" RightMargin="115.2000" TopMargin="340.5000" BottomMargin="38.5000" ctype="SpriteObjectData">
            <Size X="234.0000" Y="90.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="541.8000" Y="83.5000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.7000" Y="0.1780" />
            <PreSize X="0.3023" Y="0.1919" />
            <FileData Type="MarkedSubImage" Path="lan/cn/reject.png" Plist="lan/cn/game.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="ok" ActionTag="-159975279" Tag="1263" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="115.2000" RightMargin="424.8000" TopMargin="340.5000" BottomMargin="38.5000" ctype="SpriteObjectData">
            <Size X="234.0000" Y="90.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="232.2000" Y="83.5000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3000" Y="0.1780" />
            <PreSize X="0.3023" Y="0.1919" />
            <FileData Type="MarkedSubImage" Path="lan/cn/aggree.png" Plist="lan/cn/game.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="close" ActionTag="-1396456440" Tag="1264" IconVisible="False" LeftMargin="702.9786" RightMargin="-8.9786" TopMargin="-16.6343" BottomMargin="404.6343" ctype="SpriteObjectData">
            <Size X="80.0000" Y="81.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="742.9786" Y="445.1343" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.9599" Y="0.9491" />
            <PreSize X="0.1034" Y="0.1727" />
            <FileData Type="Normal" Path="game_public/res/user_info_close.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="time_txt" ActionTag="379533592" Tag="1265" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="339.6701" RightMargin="344.3299" TopMargin="150.6109" BottomMargin="258.3891" FontSize="60" LabelText="60s" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="90.0000" Y="60.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="384.6701" Y="288.3891" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="81" G="70" B="69" />
            <PrePosition X="0.4970" Y="0.6149" />
            <PreSize X="0.1163" Y="0.1279" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="title" ActionTag="2017572699" Tag="1266" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="304.0000" RightMargin="304.0000" TopMargin="19.5000" BottomMargin="406.5000" ctype="SpriteObjectData">
            <Size X="166.0000" Y="43.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="387.0000" Y="428.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.9126" />
            <PreSize X="0.2145" Y="0.0917" />
            <FileData Type="MarkedSubImage" Path="lan/cn/dissolve_title.png" Plist="lan/cn/game.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="line2" ActionTag="1117469845" Tag="1267" IconVisible="False" LeftMargin="489.5000" RightMargin="143.5000" TopMargin="30.0000" BottomMargin="417.0000" FlipX="True" ctype="SpriteObjectData">
            <Size X="141.0000" Y="22.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="560.0000" Y="428.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.7235" Y="0.9126" />
            <PreSize X="0.1822" Y="0.0469" />
            <FileData Type="MarkedSubImage" Path="game_public/res/title_line.png" Plist="game_public/res/game.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="line1" ActionTag="-1479948032" Tag="1268" IconVisible="False" LeftMargin="144.5000" RightMargin="488.5000" TopMargin="30.0000" BottomMargin="417.0000" ctype="SpriteObjectData">
            <Size X="141.0000" Y="22.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="215.0000" Y="428.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.2778" Y="0.9126" />
            <PreSize X="0.1822" Y="0.0469" />
            <FileData Type="MarkedSubImage" Path="game_public/res/title_line.png" Plist="game_public/res/game.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="txt" ActionTag="1919348119" Tag="1270" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="357.0000" RightMargin="357.0000" TopMargin="230.5215" BottomMargin="208.4785" FontSize="30" LabelText="xxxx" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="60.0000" Y="30.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="387.0000" Y="223.4785" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="81" G="70" B="69" />
            <PrePosition X="0.5000" Y="0.4765" />
            <PreSize X="0.0775" Y="0.0640" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>