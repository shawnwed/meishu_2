<GameFile>
  <PropertyGroup Name="login_award" Type="Layer" ID="6e57df12-ac43-4c93-8a71-ff690c2d62f9" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="327" ctype="GameLayerObjectData">
        <Size X="1280.0000" Y="720.0000" />
        <Children>
          <AbstractNodeData Name="mask" ActionTag="-344530894" Tag="318" IconVisible="False" PositionPercentXEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" LeftEage="302" RightEage="302" TopEage="178" BottomEage="178" Scale9OriginX="302" Scale9OriginY="178" Scale9Width="312" Scale9Height="184" ctype="PanelObjectData">
            <Size X="1280.0000" Y="720.0000" />
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="1.0000" Y="1.0000" />
            <FileData Type="Normal" Path="" Plist="" />
            <SingleColor A="255" R="0" G="0" B="0" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="bg" ActionTag="-1252266337" Tag="319" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="110.5000" RightMargin="110.5000" TopMargin="68.0000" BottomMargin="68.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" LeftEage="349" RightEage="349" TopEage="192" BottomEage="192" Scale9OriginX="-349" Scale9OriginY="-192" Scale9Width="698" Scale9Height="384" ctype="PanelObjectData">
            <Size X="1059.0000" Y="584.0000" />
            <Children>
              <AbstractNodeData Name="signBtn" ActionTag="-116424664" Tag="341" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="412.5000" RightMargin="412.5000" TopMargin="451.5647" BottomMargin="42.4353" ctype="SpriteObjectData">
                <Size X="234.0000" Y="90.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="529.5000" Y="87.4353" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.1497" />
                <PreSize X="0.2210" Y="0.1541" />
                <FileData Type="MarkedSubImage" Path="lan/cn/get_btn3.png" Plist="lan/cn/game.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="tip" ActionTag="1482900803" Tag="27" IconVisible="False" LeftMargin="669.8101" RightMargin="89.1899" TopMargin="460.8400" BottomMargin="73.1600" IsCustomSize="True" FontSize="20" LabelText="招財每日可獲得1000金幣，連續七天可獲得2000金幣，中途不可斷" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="300.0000" Y="50.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="819.8101" Y="98.1600" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="221" G="170" B="86" />
                <PrePosition X="0.7741" Y="0.1681" />
                <PreSize X="0.2833" Y="0.0856" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="cat" ActionTag="1954681322" Tag="167" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="418.5000" RightMargin="418.5000" TopMargin="144.5000" BottomMargin="158.5000" ctype="SpriteObjectData">
                <Size X="222.0000" Y="281.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="529.5000" Y="299.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5120" />
                <PreSize X="0.2096" Y="0.4812" />
                <FileData Type="MarkedSubImage" Path="dialog/res/loginaward/cat.png" Plist="dialog/loginaward.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="bar1" ActionTag="1467548051" Tag="169" IconVisible="False" LeftMargin="304.0000" RightMargin="705.0000" TopMargin="126.5000" BottomMargin="158.5000" ctype="SpriteObjectData">
                <Size X="50.0000" Y="299.0000" />
                <Children>
                  <AbstractNodeData Name="bar" ActionTag="-1993332049" Tag="171" RotationSkewX="-90.0000" RotationSkewY="-90.0000" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-124.5000" RightMargin="-124.5000" TopMargin="124.5000" BottomMargin="124.5000" ProgressInfo="50" ctype="LoadingBarObjectData">
                    <Size X="299.0000" Y="50.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="25.0000" Y="149.5000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="5.9800" Y="0.1672" />
                    <ImageFileData Type="MarkedSubImage" Path="dialog/res/loginaward/bar_fill_1.png" Plist="dialog/loginaward.plist" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="top" ActionTag="453995240" Tag="174" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="10.0000" RightMargin="10.0000" TopMargin="15.0000" BottomMargin="264.0000" FontSize="20" LabelText="6天" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="30.0000" Y="20.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="25.0000" Y="274.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="0" G="128" B="255" />
                    <PrePosition X="0.5000" Y="0.9164" />
                    <PreSize X="0.6000" Y="0.0669" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="middle" ActionTag="-806030072" Tag="176" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="10.0000" RightMargin="10.0000" TopMargin="148.0000" BottomMargin="131.0000" FontSize="20" LabelText="3天" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="30.0000" Y="20.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="25.0000" Y="141.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="253" G="209" B="192" />
                    <PrePosition X="0.5000" Y="0.4716" />
                    <PreSize X="0.6000" Y="0.0669" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="329.0000" Y="308.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3107" Y="0.5274" />
                <PreSize X="0.0472" Y="0.5120" />
                <FileData Type="MarkedSubImage" Path="dialog/res/loginaward/bar_bg_1.png" Plist="dialog/loginaward.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="closeBtn" ActionTag="1510951209" Tag="42" IconVisible="False" LeftMargin="936.3239" RightMargin="42.6761" TopMargin="39.6519" BottomMargin="464.3481" ctype="SpriteObjectData">
                <Size X="80.0000" Y="80.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="976.3239" Y="504.3481" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.9219" Y="0.8636" />
                <PreSize X="0.0755" Y="0.1370" />
                <FileData Type="MarkedSubImage" Path="dialog/res/loginaward/close.png" Plist="dialog/loginaward.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="bar2" ActionTag="-299310848" Tag="172" IconVisible="False" LeftMargin="700.0000" RightMargin="309.0000" TopMargin="126.5000" BottomMargin="158.5000" ctype="SpriteObjectData">
                <Size X="50.0000" Y="299.0000" />
                <Children>
                  <AbstractNodeData Name="bar" CanEdit="False" ActionTag="-914967487" Tag="173" RotationSkewX="-90.0000" RotationSkewY="-90.0000" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-124.5000" RightMargin="-124.5000" TopMargin="124.5000" BottomMargin="124.5000" ProgressInfo="100" ctype="LoadingBarObjectData">
                    <Size X="299.0000" Y="50.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="25.0000" Y="149.5000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="5.9800" Y="0.1672" />
                    <ImageFileData Type="MarkedSubImage" Path="dialog/res/loginaward/bar_fill_2.png" Plist="dialog/loginaward.plist" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="top" CanEdit="False" ActionTag="-78040608" Tag="175" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="10.0000" RightMargin="10.0000" TopMargin="15.0000" BottomMargin="264.0000" FontSize="20" LabelText="7天" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="30.0000" Y="20.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="25.0000" Y="274.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="0" G="128" B="255" />
                    <PrePosition X="0.5000" Y="0.9164" />
                    <PreSize X="0.6000" Y="0.0669" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="725.0000" Y="308.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.6846" Y="0.5274" />
                <PreSize X="0.0472" Y="0.5120" />
                <FileData Type="MarkedSubImage" Path="dialog/res/loginaward/bar_bg_2.png" Plist="dialog/loginaward.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="word2_5" ActionTag="-1486962747" Tag="178" IconVisible="False" LeftMargin="669.8101" RightMargin="283.1899" TopMargin="429.5000" BottomMargin="125.5000" ctype="SpriteObjectData">
                <Size X="106.0000" Y="29.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="722.8101" Y="140.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.6825" Y="0.2397" />
                <PreSize X="0.1001" Y="0.0497" />
                <FileData Type="MarkedSubImage" Path="lan/cn/word2.png" Plist="lan/cn/game.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="word1_6" ActionTag="1280256036" Tag="179" IconVisible="False" LeftMargin="288.5000" RightMargin="686.5000" TopMargin="422.5000" BottomMargin="118.5000" ctype="SpriteObjectData">
                <Size X="106.0000" Y="29.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="330.5000" Y="140.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3121" Y="0.2397" />
                <PreSize X="0.0793" Y="0.0736" />
                <FileData Type="MarkedSubImage" Path="lan/cn/word1.png" Plist="lan/cn/game.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="item1" ActionTag="-1451817560" Tag="180" IconVisible="False" LeftMargin="211.0000" RightMargin="775.0000" TopMargin="119.0000" BottomMargin="389.0000" ctype="SpriteObjectData">
                <Size X="73.0000" Y="76.0000" />
                <Children>
                  <AbstractNodeData Name="grey" ActionTag="264443942" Tag="192" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="1.0000" RightMargin="1.0000" TopMargin="2.5000" BottomMargin="2.5000" ctype="SpriteObjectData">
                    <Size X="71.0000" Y="71.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="36.5000" Y="38.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="0.9726" Y="0.9342" />
                    <FileData Type="MarkedSubImage" Path="dialog/res/loginaward/icon_grey.png" Plist="dialog/loginaward.plist" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="flag" ActionTag="-1185505607" Tag="182" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-23.0000" RightMargin="-23.0000" TopMargin="8.0000" BottomMargin="8.0000" ctype="SpriteObjectData">
                    <Size X="119.0000" Y="60.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="36.5000" Y="38.0000" />
                    <Scale ScaleX="0.6800" ScaleY="0.6800" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="1.6301" Y="0.7895" />
                    <FileData Type="MarkedSubImage" Path="lan/cn/got_flag.png" Plist="lan/cn/game.plist" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="txt" ActionTag="-1979004350" Tag="183" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="20.0000" RightMargin="20.0000" TopMargin="45.0000" BottomMargin="9.0000" FontSize="22" LabelText="188" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="33.0000" Y="22.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="36.5000" Y="20.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.2632" />
                    <PreSize X="0.4521" Y="0.2895" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="247.5000" Y="427.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2337" Y="0.7312" />
                <PreSize X="0.0689" Y="0.1301" />
                <FileData Type="MarkedSubImage" Path="dialog/res/loginaward/diamond_icon.png" Plist="dialog/loginaward.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="item2" ActionTag="-1707205470" Tag="187" IconVisible="False" LeftMargin="210.0000" RightMargin="776.0000" TopMargin="246.0000" BottomMargin="262.0000" ctype="SpriteObjectData">
                <Size X="73.0000" Y="76.0000" />
                <Children>
                  <AbstractNodeData Name="grey" ActionTag="1393976481" Tag="190" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="1.0000" RightMargin="1.0000" TopMargin="2.5000" BottomMargin="2.5000" ctype="SpriteObjectData">
                    <Size X="71.0000" Y="71.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="36.5000" Y="38.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="0.9726" Y="0.9342" />
                    <FileData Type="MarkedSubImage" Path="dialog/res/loginaward/icon_grey.png" Plist="dialog/loginaward.plist" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="flag" ActionTag="-1296754272" Tag="188" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-23.0000" RightMargin="-23.0000" TopMargin="8.0000" BottomMargin="8.0000" ctype="SpriteObjectData">
                    <Size X="119.0000" Y="60.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="36.5000" Y="38.0000" />
                    <Scale ScaleX="0.6800" ScaleY="0.6800" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="1.6301" Y="0.7895" />
                    <FileData Type="MarkedSubImage" Path="lan/cn/got_flag.png" Plist="lan/cn/game.plist" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="txt" ActionTag="-2004770126" Tag="189" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="20.0000" RightMargin="20.0000" TopMargin="45.0000" BottomMargin="9.0000" FontSize="22" LabelText="188" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="33.0000" Y="22.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="36.5000" Y="20.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.2632" />
                    <PreSize X="0.4521" Y="0.2895" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="246.5000" Y="300.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2328" Y="0.5137" />
                <PreSize X="0.0689" Y="0.1301" />
                <FileData Type="MarkedSubImage" Path="dialog/res/loginaward/diamond_icon.png" Plist="dialog/loginaward.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="item3" ActionTag="-1915687226" Tag="184" IconVisible="False" LeftMargin="770.0000" RightMargin="216.0000" TopMargin="124.0000" BottomMargin="384.0000" ctype="SpriteObjectData">
                <Size X="73.0000" Y="76.0000" />
                <Children>
                  <AbstractNodeData Name="grey" ActionTag="-1237557531" Tag="191" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="1.0000" RightMargin="1.0000" TopMargin="2.5000" BottomMargin="2.5000" ctype="SpriteObjectData">
                    <Size X="71.0000" Y="71.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="36.5000" Y="38.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="0.9726" Y="0.9342" />
                    <FileData Type="MarkedSubImage" Path="dialog/res/loginaward/icon_grey.png" Plist="dialog/loginaward.plist" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="flag" ActionTag="915612608" Tag="185" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-23.0000" RightMargin="-23.0000" TopMargin="8.0000" BottomMargin="8.0000" ctype="SpriteObjectData">
                    <Size X="119.0000" Y="60.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="36.5000" Y="38.0000" />
                    <Scale ScaleX="0.6800" ScaleY="0.6800" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="1.6301" Y="0.7895" />
                    <FileData Type="MarkedSubImage" Path="lan/cn/got_flag.png" Plist="lan/cn/game.plist" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="txt" ActionTag="1789523626" Tag="186" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="20.0000" RightMargin="20.0000" TopMargin="45.0000" BottomMargin="9.0000" FontSize="22" LabelText="188" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="33.0000" Y="22.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="36.5000" Y="20.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.2632" />
                    <PreSize X="0.4521" Y="0.2895" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="806.5000" Y="422.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7616" Y="0.7226" />
                <PreSize X="0.0689" Y="0.1301" />
                <FileData Type="MarkedSubImage" Path="dialog/res/loginaward/coin_icon.png" Plist="dialog/loginaward.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="640.0000" Y="360.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="0.8273" Y="0.8111" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>