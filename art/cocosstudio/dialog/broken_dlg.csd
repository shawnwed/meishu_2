<GameFile>
  <PropertyGroup Name="broken_dlg" Type="Layer" ID="8531b3b9-31ec-41db-a78e-0122153e3bfb" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="249" ctype="GameLayerObjectData">
        <Size X="1280.0000" Y="720.0000" />
        <Children>
          <AbstractNodeData Name="mask" ActionTag="529981729" Tag="251" IconVisible="False" PositionPercentXEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" ClipAble="False" BackColorAlpha="153" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="1280.0000" Y="720.0000" />
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="1.0000" Y="1.0000" />
            <SingleColor A="255" R="0" G="0" B="0" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="bg" ActionTag="-661023" Tag="75" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="250.5000" RightMargin="250.5000" TopMargin="72.0000" BottomMargin="72.0000" TouchEnable="True" LeftEage="302" RightEage="302" TopEage="178" BottomEage="178" Scale9OriginX="302" Scale9OriginY="178" Scale9Width="175" Scale9Height="220" ctype="ImageViewObjectData">
            <Size X="779.0000" Y="576.0000" />
            <Children>
              <AbstractNodeData Name="timer_bg" ActionTag="1269876206" Tag="253" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="134.6447" RightMargin="132.3553" TopMargin="228.4409" BottomMargin="190.5591" ctype="SpriteObjectData">
                <Size X="512.0000" Y="157.0000" />
                <Children>
                  <AbstractNodeData Name="t_1" ActionTag="-2057437403" Tag="257" IconVisible="False" LeftMargin="37.0063" RightMargin="436.9937" TopMargin="33.6495" BottomMargin="48.3505" FontSize="75" LabelText="0" ShadowOffsetX="1.0000" ShadowOffsetY="-1.0000" ShadowEnabled="True" ctype="TextObjectData">
                    <Size X="38.0000" Y="75.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="56.0063" Y="85.8505" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="143" G="18" B="33" />
                    <PrePosition X="0.1094" Y="0.5468" />
                    <PreSize X="0.0742" Y="0.4777" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="255" G="255" B="255" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="t_2" ActionTag="1742139049" Tag="258" IconVisible="False" LeftMargin="106.7141" RightMargin="367.2859" TopMargin="33.6495" BottomMargin="48.3505" FontSize="75" LabelText="0" ShadowOffsetX="1.0000" ShadowOffsetY="-1.0000" ShadowEnabled="True" ctype="TextObjectData">
                    <Size X="38.0000" Y="75.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="125.7141" Y="85.8505" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="143" G="18" B="33" />
                    <PrePosition X="0.2455" Y="0.5468" />
                    <PreSize X="0.0742" Y="0.4777" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="255" G="255" B="255" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="t_3" ActionTag="-1832447528" Tag="259" IconVisible="False" LeftMargin="200.4216" RightMargin="273.5784" TopMargin="33.6495" BottomMargin="48.3505" FontSize="75" LabelText="0" ShadowOffsetX="1.0000" ShadowOffsetY="-1.0000" ShadowEnabled="True" ctype="TextObjectData">
                    <Size X="38.0000" Y="75.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="219.4216" Y="85.8505" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="143" G="18" B="33" />
                    <PrePosition X="0.4286" Y="0.5468" />
                    <PreSize X="0.0742" Y="0.4777" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="255" G="255" B="255" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="t_4" ActionTag="-1695205302" Tag="260" IconVisible="False" LeftMargin="273.1295" RightMargin="200.8705" TopMargin="33.6495" BottomMargin="48.3505" FontSize="75" LabelText="0" ShadowOffsetX="1.0000" ShadowOffsetY="-1.0000" ShadowEnabled="True" ctype="TextObjectData">
                    <Size X="38.0000" Y="75.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="292.1295" Y="85.8505" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="143" G="18" B="33" />
                    <PrePosition X="0.5706" Y="0.5468" />
                    <PreSize X="0.0742" Y="0.4777" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="255" G="255" B="255" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="t_5" ActionTag="1693498619" Tag="261" IconVisible="False" LeftMargin="367.8370" RightMargin="106.1630" TopMargin="33.6495" BottomMargin="48.3505" FontSize="75" LabelText="0" ShadowOffsetX="1.0000" ShadowOffsetY="-1.0000" ShadowEnabled="True" ctype="TextObjectData">
                    <Size X="38.0000" Y="75.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="386.8370" Y="85.8505" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="143" G="18" B="33" />
                    <PrePosition X="0.7555" Y="0.5468" />
                    <PreSize X="0.0742" Y="0.4777" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="255" G="255" B="255" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="t_6" ActionTag="840982801" Tag="262" IconVisible="False" LeftMargin="437.5443" RightMargin="36.4557" TopMargin="33.6495" BottomMargin="48.3505" FontSize="75" LabelText="0" ShadowOffsetX="1.0000" ShadowOffsetY="-1.0000" ShadowEnabled="True" ctype="TextObjectData">
                    <Size X="38.0000" Y="75.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="456.5443" Y="85.8505" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="143" G="18" B="33" />
                    <PrePosition X="0.8917" Y="0.5468" />
                    <PreSize X="0.0742" Y="0.4777" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="255" G="255" B="255" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="390.6447" Y="269.0591" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5015" Y="0.4671" />
                <PreSize X="0.6573" Y="0.2726" />
                <FileData Type="MarkedSubImage" Path="dialog/res/broken/timer_bg.png" Plist="dialog/broken.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="close" ActionTag="-170258974" Tag="263" IconVisible="False" LeftMargin="724.9574" RightMargin="-22.9574" TopMargin="62.5875" BottomMargin="436.4125" ctype="SpriteObjectData">
                <Size X="77.0000" Y="77.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="763.4574" Y="474.9125" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.9800" Y="0.8245" />
                <PreSize X="0.0988" Y="0.1337" />
                <FileData Type="MarkedSubImage" Path="dialog/res/broken/close.png" Plist="dialog/broken.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="tip_bg" ActionTag="1630294942" Tag="265" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="148.5002" RightMargin="148.4999" TopMargin="402.0893" BottomMargin="138.9107" ctype="SpriteObjectData">
                <Size X="482.0000" Y="35.0000" />
                <Children>
                  <AbstractNodeData Name="tip1" ActionTag="-1241453159" Tag="266" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="4.0520" RightMargin="237.9480" TopMargin="5.5966" BottomMargin="5.4034" FontSize="24" LabelText="領取條件：低于%s金幣" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="240.0000" Y="24.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="4.0520" Y="17.4034" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="139" G="73" B="61" />
                    <PrePosition X="0.0084" Y="0.4972" />
                    <PreSize X="0.4979" Y="0.6857" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="tip2" ActionTag="-1711545221" Tag="267" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="277.3992" RightMargin="12.6008" TopMargin="5.0355" BottomMargin="5.9645" FontSize="24" LabelText="領取金額：%s金幣" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="192.0000" Y="24.0000" />
                    <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
                    <Position X="469.3992" Y="17.9645" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="139" G="73" B="61" />
                    <PrePosition X="0.9739" Y="0.5133" />
                    <PreSize X="0.3983" Y="0.6857" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="389.5002" Y="156.4107" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.2715" />
                <PreSize X="0.6187" Y="0.0608" />
                <FileData Type="MarkedSubImage" Path="dialog/res/broken/tip_bg.png" Plist="dialog/broken.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="get_btn" ActionTag="-730485193" Tag="264" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="249.7097" RightMargin="254.2903" TopMargin="449.3498" BottomMargin="31.6502" ctype="SpriteObjectData">
                <Size X="275.0000" Y="95.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="387.2097" Y="79.1502" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4971" Y="0.1374" />
                <PreSize X="0.3004" Y="0.1563" />
                <FileData Type="Normal" Path="dialog/res/broken/get_btn.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="640.0000" Y="360.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="0.6086" Y="0.8000" />
            <FileData Type="Normal" Path="lan/cn/bg3.png" Plist="" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>