<GameFile>
  <PropertyGroup Name="dissolve_dialog1" Type="Layer" ID="9ad084cd-1eb4-4e7b-9110-e61d2e935be9" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="1215" ctype="GameLayerObjectData">
        <Size X="774.0000" Y="469.0000" />
        <Children>
          <AbstractNodeData Name="bg" ActionTag="2027281412" Tag="1216" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-70.5000" RightMargin="-70.5000" TopMargin="-53.5000" BottomMargin="-53.5000" ctype="SpriteObjectData">
            <Size X="915.0000" Y="576.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="387.0000" Y="234.5000" />
            <Scale ScaleX="0.8200" ScaleY="0.8200" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="1.1822" Y="1.2281" />
            <FileData Type="Normal" Path="game_public/res/setting_bg.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="line1" ActionTag="227888409" Tag="1252" IconVisible="False" LeftMargin="144.5000" RightMargin="488.5000" TopMargin="30.0000" BottomMargin="417.0000" ctype="SpriteObjectData">
            <Size X="141.0000" Y="22.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="215.0000" Y="428.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.2778" Y="0.9126" />
            <PreSize X="0.1822" Y="0.0469" />
            <FileData Type="MarkedSubImage" Path="game_public/res/title_line.png" Plist="game_public/res/game.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="line2" ActionTag="-328785598" Tag="1253" IconVisible="False" LeftMargin="489.5000" RightMargin="143.5000" TopMargin="30.0000" BottomMargin="417.0000" FlipX="True" ctype="SpriteObjectData">
            <Size X="141.0000" Y="22.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="560.0000" Y="428.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.7235" Y="0.9126" />
            <PreSize X="0.1822" Y="0.0469" />
            <FileData Type="MarkedSubImage" Path="game_public/res/title_line.png" Plist="game_public/res/game.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="title" ActionTag="824966848" Tag="1254" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="304.0000" RightMargin="304.0000" TopMargin="19.5000" BottomMargin="406.5000" ctype="SpriteObjectData">
            <Size X="166.0000" Y="43.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="387.0000" Y="428.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.9126" />
            <PreSize X="0.2145" Y="0.0917" />
            <FileData Type="MarkedSubImage" Path="lan/cn/dissolve_title.png" Plist="lan/cn/game.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="txt" ActionTag="1978266281" Tag="1255" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="67.0000" RightMargin="67.0000" TopMargin="134.5000" BottomMargin="134.5000" IsCustomSize="True" FontSize="30" LabelText="本房间已经开了5局，中途解散系统会扣除相应房卡，是否发起投票解散？" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="640.0000" Y="200.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="387.0000" Y="234.5000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="81" G="70" B="69" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="0.8269" Y="0.4264" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="close" ActionTag="212466525" Tag="1257" IconVisible="False" LeftMargin="695.9893" RightMargin="-1.9893" TopMargin="-21.2934" BottomMargin="409.2934" ctype="SpriteObjectData">
            <Size X="80.0000" Y="81.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="735.9893" Y="449.7934" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.9509" Y="0.9590" />
            <PreSize X="0.1034" Y="0.1727" />
            <FileData Type="Normal" Path="game_public/res/user_info_close.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="ok" ActionTag="499469687" Tag="1258" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="115.2000" RightMargin="424.8000" TopMargin="340.5000" BottomMargin="38.5000" ctype="SpriteObjectData">
            <Size X="234.0000" Y="90.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="232.2000" Y="83.5000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3000" Y="0.1780" />
            <PreSize X="0.3023" Y="0.1919" />
            <FileData Type="MarkedSubImage" Path="lan/cn/ok.png" Plist="lan/cn/game.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="cancel" ActionTag="1763967622" Tag="1260" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="424.8000" RightMargin="115.2000" TopMargin="340.5000" BottomMargin="38.5000" ctype="SpriteObjectData">
            <Size X="234.0000" Y="90.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="541.8000" Y="83.5000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.7000" Y="0.1780" />
            <PreSize X="0.3023" Y="0.1919" />
            <FileData Type="MarkedSubImage" Path="lan/cn/cancel.png" Plist="lan/cn/game.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>