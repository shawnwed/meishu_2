<GameFile>
  <PropertyGroup Name="payaward" Type="Layer" ID="0f55c8de-08ac-444e-90dc-d79b96c8c9ce" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="303" ctype="GameLayerObjectData">
        <Size X="1280.0000" Y="720.0000" />
        <Children>
          <AbstractNodeData Name="mask" ActionTag="529855271" Tag="304" IconVisible="False" PositionPercentXEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="1280.0000" Y="720.0000" />
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="1.0000" Y="1.0000" />
            <SingleColor A="255" R="0" G="0" B="0" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="bg" ActionTag="1591955841" Tag="627" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="152.0000" RightMargin="152.0000" TopMargin="88.5000" BottomMargin="88.5000" TouchEnable="True" LeftEage="322" RightEage="322" TopEage="179" BottomEage="179" Scale9OriginX="322" Scale9OriginY="179" Scale9Width="332" Scale9Height="185" ctype="ImageViewObjectData">
            <Size X="976.0000" Y="543.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="640.0000" Y="360.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="0.7625" Y="0.7542" />
            <FileData Type="Default" Path="Default/ImageFile.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="close" ActionTag="-1413172181" Tag="306" IconVisible="False" LeftMargin="1075.9788" RightMargin="124.0212" TopMargin="104.1519" BottomMargin="535.8481" ctype="SpriteObjectData">
            <Size X="80.0000" Y="80.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1115.9788" Y="575.8481" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8719" Y="0.7998" />
            <PreSize X="0.0625" Y="0.1111" />
            <FileData Type="Normal" Path="dialog/res/payaward/close.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="pay_btn" ActionTag="-2013966238" Tag="307" IconVisible="False" LeftMargin="741.8522" RightMargin="492.1478" TopMargin="410.0233" BottomMargin="263.9767" ctype="SpriteObjectData">
            <Size X="234.0000" Y="90.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="764.8522" Y="286.9767" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5975" Y="0.3986" />
            <PreSize X="0.0359" Y="0.0639" />
            <FileData Type="MarkedSubImage" Path="lan/cn/pay_btn2.png" Plist="lan/cn/game.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="item_1" ActionTag="-1622998069" Tag="325" IconVisible="True" LeftMargin="304.2297" RightMargin="975.7703" TopMargin="552.8677" BottomMargin="167.1323" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="304.2297" Y="167.1323" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.2377" Y="0.2321" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="dialog/pay_award_item.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="item_2" ActionTag="320530557" Tag="331" IconVisible="True" LeftMargin="425.8770" RightMargin="854.1230" TopMargin="552.8677" BottomMargin="167.1323" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="425.8770" Y="167.1323" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3327" Y="0.2321" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="dialog/pay_award_item.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="item_3" ActionTag="1723554162" Tag="337" IconVisible="True" LeftMargin="547.5243" RightMargin="732.4757" TopMargin="552.8677" BottomMargin="167.1323" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="547.5243" Y="167.1323" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4278" Y="0.2321" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="dialog/pay_award_item.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="item_4" ActionTag="231099979" Tag="343" IconVisible="True" LeftMargin="669.1716" RightMargin="610.8284" TopMargin="552.8677" BottomMargin="167.1323" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="669.1716" Y="167.1323" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5228" Y="0.2321" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="dialog/pay_award_item.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="item_5" ActionTag="1933494629" Tag="349" IconVisible="True" LeftMargin="790.8188" RightMargin="489.1812" TopMargin="552.8677" BottomMargin="167.1323" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="790.8188" Y="167.1323" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.6178" Y="0.2321" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="dialog/pay_award_item.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="item_6" ActionTag="127168078" Tag="355" IconVisible="True" LeftMargin="912.4661" RightMargin="367.5339" TopMargin="552.8677" BottomMargin="167.1323" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="912.4661" Y="167.1323" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.7129" Y="0.2321" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="dialog/pay_award_item.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="item_7" ActionTag="-227045056" Tag="361" IconVisible="True" LeftMargin="1034.1133" RightMargin="245.8867" TopMargin="552.8677" BottomMargin="167.1323" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="1034.1133" Y="167.1323" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8079" Y="0.2321" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="dialog/pay_award_item.csd" Plist="" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>