<GameFile>
  <PropertyGroup Name="diamondaward" Type="Layer" ID="d0601886-7ce0-4768-86f4-fe4b10b22882" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="268" ctype="GameLayerObjectData">
        <Size X="1280.0000" Y="720.0000" />
        <Children>
          <AbstractNodeData Name="mask" ActionTag="-63282984" Tag="297" IconVisible="False" PositionPercentXEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="1280.0000" Y="720.0000" />
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="1.0000" Y="1.0000" />
            <SingleColor A="255" R="0" G="0" B="0" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="bg" ActionTag="1091745624" Tag="686" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="182.0000" RightMargin="182.0000" TopMargin="90.0000" BottomMargin="90.0000" TouchEnable="True" LeftEage="302" RightEage="302" TopEage="178" BottomEage="178" Scale9OriginX="302" Scale9OriginY="178" Scale9Width="312" Scale9Height="184" ctype="ImageViewObjectData">
            <Size X="916.0000" Y="540.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="640.0000" Y="360.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="0.7156" Y="0.7500" />
            <FileData Type="Normal" Path="lan/cn/bg4.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="close" ActionTag="-158683178" Tag="302" IconVisible="False" LeftMargin="1046.4587" RightMargin="153.5413" TopMargin="106.6420" BottomMargin="533.3580" ctype="SpriteObjectData">
            <Size X="80.0000" Y="80.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1086.4587" Y="573.3580" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8488" Y="0.7963" />
            <PreSize X="0.0625" Y="0.1111" />
            <FileData Type="MarkedSubImage" Path="dialog/res/diamondaward/close.png" Plist="dialog/dialog.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="sign" ActionTag="-278563832" Tag="301" IconVisible="False" LeftMargin="342.9073" RightMargin="703.0927" TopMargin="507.5558" BottomMargin="122.4442" ctype="SpriteObjectData">
            <Size X="234.0000" Y="90.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="459.9073" Y="167.4442" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3593" Y="0.2326" />
            <PreSize X="0.1828" Y="0.1250" />
            <FileData Type="MarkedSubImage" Path="lan/tw/sign.png" Plist="lan/tw/game.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="start" ActionTag="524999123" Tag="300" IconVisible="False" LeftMargin="726.9068" RightMargin="319.0932" TopMargin="507.5558" BottomMargin="122.4442" ctype="SpriteObjectData">
            <Size X="234.0000" Y="90.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="843.9068" Y="167.4442" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.6593" Y="0.2326" />
            <PreSize X="0.1828" Y="0.1250" />
            <FileData Type="MarkedSubImage" Path="lan/tw/start.png" Plist="lan/tw/game.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>