<GameFile>
  <PropertyGroup Name="dialog2" Type="Layer" ID="dbbb1189-30da-4283-b1ae-2c13064c2f01" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="17" ctype="GameLayerObjectData">
        <Size X="774.0000" Y="469.0000" />
        <Children>
          <AbstractNodeData Name="bg_1" ActionTag="1534142753" Tag="18" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-70.5000" RightMargin="-70.5000" TopMargin="-53.5000" BottomMargin="-53.5000" ctype="SpriteObjectData">
            <Size X="915.0000" Y="576.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="387.0000" Y="234.5000" />
            <Scale ScaleX="0.8200" ScaleY="0.8200" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="1.1822" Y="1.2281" />
            <FileData Type="Normal" Path="game_public/res/setting_bg.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="content" ActionTag="988237524" Tag="20" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="69.5078" RightMargin="54.4922" TopMargin="137.1808" BottomMargin="211.8192" IsCustomSize="True" FontSize="36" LabelText="今天的离开是为了更好的相聚！" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="650.0000" Y="120.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="394.5078" Y="271.8192" />
            <Scale ScaleX="0.8200" ScaleY="0.8200" />
            <CColor A="255" R="81" G="70" B="69" />
            <PrePosition X="0.5097" Y="0.5796" />
            <PreSize X="0.8398" Y="0.2559" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="btn_left" ActionTag="-804064975" Tag="44" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="81.2680" RightMargin="418.7320" TopMargin="329.8677" BottomMargin="45.1323" ctype="SpriteObjectData">
            <Size X="274.0000" Y="94.0000" />
            <Children>
              <AbstractNodeData Name="txt" ActionTag="-1620919177" Tag="45" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="65.0000" RightMargin="65.0000" TopMargin="24.3000" BottomMargin="33.7000" FontSize="36" LabelText="再玩一会" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="144.0000" Y="36.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="137.0000" Y="51.7000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5500" />
                <PreSize X="0.5255" Y="0.3830" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="218.2680" Y="92.1323" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.2820" Y="0.1964" />
            <PreSize X="0.3540" Y="0.2004" />
            <FileData Type="Normal" Path="dialog/res/btn_right.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="btn_right" ActionTag="-1809231127" Tag="22" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="423.0664" RightMargin="76.9336" TopMargin="329.8675" BottomMargin="45.1325" ctype="SpriteObjectData">
            <Size X="274.0000" Y="94.0000" />
            <Children>
              <AbstractNodeData Name="txt" ActionTag="180260636" Tag="23" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="65.0000" RightMargin="65.0000" TopMargin="24.3000" BottomMargin="33.7000" FontSize="36" LabelText="退出游戏" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="144.0000" Y="36.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="137.0000" Y="51.7000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5500" />
                <PreSize X="0.5255" Y="0.3830" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="560.0664" Y="92.1325" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.7236" Y="0.1964" />
            <PreSize X="0.3540" Y="0.2004" />
            <FileData Type="Normal" Path="dialog/res/btn_left.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="title" ActionTag="1282821312" Tag="37" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="312.5728" RightMargin="301.4272" TopMargin="20.6879" BottomMargin="408.3121" FontSize="40" LabelText="退出提示" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="160.0000" Y="40.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="392.5728" Y="428.3121" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="201" B="102" />
            <PrePosition X="0.5072" Y="0.9132" />
            <PreSize X="0.2067" Y="0.0853" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>