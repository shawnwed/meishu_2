<GameFile>
  <PropertyGroup Name="dialog_edit_info" Type="Layer" ID="506045b4-6356-409b-80f0-a278d09f0ff6" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="37" ctype="GameLayerObjectData">
        <Size X="774.0000" Y="469.0000" />
        <Children>
          <AbstractNodeData Name="bg" ActionTag="-1593925076" Tag="681" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-72.5124" RightMargin="-68.4875" TopMargin="-54.5318" BottomMargin="-52.4682" TouchEnable="True" LeftEage="255" RightEage="255" TopEage="154" BottomEage="154" Scale9OriginX="255" Scale9OriginY="154" Scale9Width="405" Scale9Height="268" ctype="ImageViewObjectData">
            <Size X="915.0000" Y="576.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="384.9876" Y="235.5318" />
            <Scale ScaleX="0.8200" ScaleY="0.8200" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4974" Y="0.5022" />
            <PreSize X="1.1822" Y="1.2281" />
            <FileData Type="Normal" Path="game_public/res/setting_bg.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="close" ActionTag="-508850618" Tag="53" IconVisible="False" LeftMargin="705.0000" RightMargin="-11.0000" TopMargin="-17.5000" BottomMargin="405.5000" ctype="SpriteObjectData">
            <Size X="80.0000" Y="81.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="745.0000" Y="446.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.9625" Y="0.9510" />
            <PreSize X="0.1034" Y="0.1727" />
            <FileData Type="Normal" Path="game_public/res/user_info_close.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="title" ActionTag="-1896994565" Tag="55" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="299.0000" RightMargin="299.0000" TopMargin="17.5000" BottomMargin="404.5000" ctype="SpriteObjectData">
            <Size X="176.0000" Y="47.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="387.0000" Y="428.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.9126" />
            <PreSize X="0.2274" Y="0.1002" />
            <FileData Type="MarkedSubImage" Path="lan/cn/edit_title.png" Plist="lan/cn/game.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="line2" ActionTag="1362770283" Tag="56" IconVisible="False" LeftMargin="489.5000" RightMargin="143.5000" TopMargin="30.0000" BottomMargin="417.0000" FlipX="True" ctype="SpriteObjectData">
            <Size X="141.0000" Y="22.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="560.0000" Y="428.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.7235" Y="0.9126" />
            <PreSize X="0.1822" Y="0.0469" />
            <FileData Type="MarkedSubImage" Path="game_public/res/title_line.png" Plist="game_public/res/game.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="line1" ActionTag="671307664" Tag="57" IconVisible="False" LeftMargin="144.5000" RightMargin="488.5000" TopMargin="30.0000" BottomMargin="417.0000" ctype="SpriteObjectData">
            <Size X="141.0000" Y="22.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="215.0000" Y="428.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.2778" Y="0.9126" />
            <PreSize X="0.1822" Y="0.0469" />
            <FileData Type="MarkedSubImage" Path="game_public/res/title_line.png" Plist="game_public/res/game.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="input_bg" ActionTag="305329432" Tag="58" IconVisible="False" LeftMargin="131.5001" RightMargin="143.4999" TopMargin="101.9999" BottomMargin="305.0001" ctype="SpriteObjectData">
            <Size X="499.0000" Y="62.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="381.0001" Y="336.0001" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4922" Y="0.7164" />
            <PreSize X="0.6447" Y="0.1322" />
            <FileData Type="Normal" Path="hall/res/info/edt_white.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="input" ActionTag="-1479957602" Tag="59" IconVisible="False" LeftMargin="146.5000" RightMargin="162.5000" TopMargin="114.0001" BottomMargin="314.9999" TouchEnable="True" FontSize="36" IsCustomSize="True" LabelText="" PlaceHolderText="請輸入您的暱稱" MaxLengthText="10" ctype="TextFieldObjectData">
            <Size X="465.0000" Y="40.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="379.0000" Y="334.9999" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="82" G="52" B="49" />
            <PrePosition X="0.4897" Y="0.7143" />
            <PreSize X="0.6008" Y="0.0853" />
          </AbstractNodeData>
          <AbstractNodeData Name="tips" ActionTag="1613335664" Tag="9" IconVisible="False" LeftMargin="144.0001" RightMargin="509.9999" TopMargin="175.0000" BottomMargin="270.0000" FontSize="24" LabelText="不符合规则" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="120.0000" Y="24.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="144.0001" Y="282.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="252" G="92" B="69" />
            <PrePosition X="0.1860" Y="0.6013" />
            <PreSize X="0.1550" Y="0.0512" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="submit_btn" ActionTag="-1858453855" Tag="10" IconVisible="False" LeftMargin="250.0000" RightMargin="290.0000" TopMargin="328.0000" BottomMargin="51.0000" ctype="SpriteObjectData">
            <Size X="234.0000" Y="90.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="367.0000" Y="96.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4742" Y="0.2047" />
            <PreSize X="0.3023" Y="0.1919" />
            <FileData Type="MarkedSubImage" Path="lan/cn/submit_btn.png" Plist="lan/cn/game.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="sex2" ActionTag="725947604" Tag="33" IconVisible="False" LeftMargin="129.5000" RightMargin="573.5000" TopMargin="210.0000" BottomMargin="195.0000" ctype="SpriteObjectData">
            <Size X="71.0000" Y="64.0000" />
            <Children>
              <AbstractNodeData Name="icon" ActionTag="1163271762" Tag="35" IconVisible="False" LeftMargin="103.0000" RightMargin="-70.0000" TopMargin="13.5000" BottomMargin="11.5000" ctype="SpriteObjectData">
                <Size X="38.0000" Y="39.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="122.0000" Y="31.0000" />
                <Scale ScaleX="1.3684" ScaleY="1.3684" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="1.7183" Y="0.4844" />
                <PreSize X="0.5352" Y="0.6094" />
                <FileData Type="MarkedSubImage" Path="game_public/res/info_sex2.png" Plist="game_public/res/game.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="165.0000" Y="227.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.2132" Y="0.4840" />
            <PreSize X="0.0917" Y="0.1365" />
            <FileData Type="MarkedSubImage" Path="game_public/res/setting_check1.png" Plist="game_public/res/game.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="sex1" ActionTag="1828186853" Tag="37" IconVisible="False" LeftMargin="449.5000" RightMargin="253.5000" TopMargin="210.0000" BottomMargin="195.0000" ctype="SpriteObjectData">
            <Size X="71.0000" Y="64.0000" />
            <Children>
              <AbstractNodeData Name="icon" ActionTag="-300474246" Tag="38" IconVisible="False" LeftMargin="103.9999" RightMargin="-70.9999" TopMargin="14.5000" BottomMargin="10.5000" ctype="SpriteObjectData">
                <Size X="38.0000" Y="39.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="122.9999" Y="30.0000" />
                <Scale ScaleX="1.3158" ScaleY="1.3158" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="1.7324" Y="0.4687" />
                <PreSize X="0.5352" Y="0.6094" />
                <FileData Type="MarkedSubImage" Path="game_public/res/info_sex1.png" Plist="game_public/res/game.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="485.0000" Y="227.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.6266" Y="0.4840" />
            <PreSize X="0.0917" Y="0.1365" />
            <FileData Type="MarkedSubImage" Path="game_public/res/setting_check1.png" Plist="game_public/res/game.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>