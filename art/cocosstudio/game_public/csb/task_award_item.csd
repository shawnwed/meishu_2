<GameFile>
  <PropertyGroup Name="task_award_item" Type="Node" ID="f7723c1c-3489-448b-964d-05b9789a2153" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="2430" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="icon" ActionTag="459551749" Tag="2431" IconVisible="False" LeftMargin="-98.0000" RightMargin="-98.0000" TopMargin="-88.0000" BottomMargin="-88.0000" ctype="SpriteObjectData">
            <Size X="196.0000" Y="176.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="game_public/res/task_award/diamond.png" Plist="game_public/task_award.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="txt" ActionTag="-1110308942" Tag="2441" IconVisible="False" LeftMargin="-28.4196" RightMargin="-30.5804" TopMargin="83.9765" BottomMargin="-128.9765" LabelText="50" ctype="TextBMFontObjectData">
            <Size X="59.0000" Y="45.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1.0804" Y="-106.4765" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <LabelBMFontFile_CNB Type="Normal" Path="game_public/res/task_award/num.fnt" Plist="" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>