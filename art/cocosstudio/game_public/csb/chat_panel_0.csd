<GameFile>
  <PropertyGroup Name="chat_panel_0" Type="Scene" ID="d9bc75af-00b9-471b-9cd2-c882c12cc667" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Scene" Tag="2625" ctype="GameNodeObjectData">
        <Size X="1440.0000" Y="720.0000" />
        <Children>
          <AbstractNodeData Name="mask" ActionTag="1535251053" Alpha="229" Tag="496" IconVisible="False" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" TouchEnable="True" ClipAble="False" BackColorAlpha="229" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="1280.0000" Y="720.0000" />
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="1.0000" Y="1.0000" />
            <SingleColor A="255" R="0" G="0" B="0" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="bg" ActionTag="-435249422" Tag="2626" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentWidthEnabled="True" TopMargin="640.0000" TouchEnable="True" ClipAble="False" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="1280.0000" Y="80.0000" />
            <Children>
              <AbstractNodeData Name="input_bg" ActionTag="-1754577181" Tag="2634" IconVisible="False" LeftMargin="114.5200" RightMargin="155.4800" TopMargin="8.0635" BottomMargin="16.9365" Scale9Enable="True" LeftEage="33" RightEage="33" TopEage="18" BottomEage="18" Scale9OriginX="33" Scale9OriginY="18" Scale9Width="34" Scale9Height="19" ctype="ImageViewObjectData">
                <Size X="1010.0000" Y="55.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="114.5200" Y="44.4365" />
                <Scale ScaleX="0.9513" ScaleY="0.9513" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0895" Y="0.5555" />
                <PreSize X="0.7891" Y="0.6875" />
                <FileData Type="MarkedSubImage" Path="game_public/res/chat_input.png" Plist="game_public/res/game.plist" />
              </AbstractNodeData>
              <AbstractNodeData Name="face_list" ActionTag="237692539" VisibleForFrame="False" Tag="450" IconVisible="False" PercentWidthEnable="True" PercentWidthEnabled="True" TopMargin="-136.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="1280.0000" Y="216.0000" />
                <AnchorPoint />
                <Position />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="1.0000" Y="2.7000" />
                <SingleColor A="255" R="217" G="221" B="223" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="input_txt" ActionTag="-2101953674" Tag="2636" IconVisible="False" LeftMargin="114.5200" RightMargin="205.4800" TopMargin="20.4721" BottomMargin="24.5279" TouchEnable="True" FontSize="26" IsCustomSize="True" LabelText="" PlaceHolderText="请输入聊天内容..." MaxLengthEnable="True" MaxLengthText="400" ctype="TextFieldObjectData">
                <Size X="960.0000" Y="35.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="594.5200" Y="42.0279" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="127" G="127" B="127" />
                <PrePosition X="0.4645" Y="0.5253" />
                <PreSize X="0.7500" Y="0.4375" />
              </AbstractNodeData>
              <AbstractNodeData Name="send_btn" ActionTag="-2122741546" Tag="2635" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="1110.0160" RightMargin="41.9840" TopMargin="11.0549" BottomMargin="13.9451" ctype="SpriteObjectData">
                <Size X="128.0000" Y="55.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="1174.0160" Y="41.4451" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.9172" Y="0.5181" />
                <PreSize X="0.1000" Y="0.6875" />
                <FileData Type="MarkedSubImage" Path="game_public/lan/cn/chat_send_1.png" Plist="game_public/lan/cn/game.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="face_btn" ActionTag="1443753396" VisibleForFrame="False" Tag="2628" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="168.2320" RightMargin="1065.7681" TopMargin="-198.5000" BottomMargin="231.5000" ctype="SpriteObjectData">
                <Size X="46.0000" Y="47.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="191.2320" Y="255.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1494" Y="3.1875" />
                <PreSize X="0.0359" Y="0.5875" />
                <FileData Type="MarkedSubImage" Path="game_public/res/chat_face_2.png" Plist="game_public/res/game.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="quick_btn" ActionTag="249643172" VisibleForFrame="False" Tag="2627" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="50.0320" RightMargin="1177.9680" TopMargin="-197.0000" BottomMargin="233.0000" ctype="SpriteObjectData">
                <Size X="52.0000" Y="44.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="76.0320" Y="255.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0594" Y="3.1875" />
                <PreSize X="0.0406" Y="0.5500" />
                <FileData Type="MarkedSubImage" Path="game_public/res/chat_quick_1.png" Plist="game_public/res/game.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="quick_list" ActionTag="1141936843" VisibleForFrame="False" Tag="2629" IconVisible="False" PositionPercentXEnabled="True" PercentWidthEnable="True" PercentWidthEnabled="True" TopMargin="-136.0000" TouchEnable="True" ClipAble="True" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" IsBounceEnabled="True" ScrollDirectionType="0" ItemMargin="1" DirectionType="Vertical" ctype="ListViewObjectData">
                <Size X="1280.0000" Y="216.0000" />
                <AnchorPoint />
                <Position />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="1.0000" Y="2.7000" />
                <SingleColor A="255" R="217" G="221" B="223" />
                <FirstColor A="255" R="150" G="150" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="horn_btn" ActionTag="111484906" Tag="17493" IconVisible="False" LeftMargin="35.2496" RightMargin="1204.7504" TopMargin="17.8346" BottomMargin="22.1654" ctype="SpriteObjectData">
                <Size X="40.0000" Y="40.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="55.2496" Y="42.1654" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0432" Y="0.5271" />
                <PreSize X="0.0313" Y="0.5000" />
                <FileData Type="MarkedSubImage" Path="game_public/res/chat_horn_1.png" Plist="game_public/res/game.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="1.0000" Y="0.1111" />
            <SingleColor A="255" R="229" G="234" B="238" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="list" ActionTag="-1474861611" Tag="16917" IconVisible="False" PositionPercentXEnabled="True" VerticalEdge="BothEdge" BottomMargin="80.0000" TouchEnable="True" StretchHeightEnable="True" ClipAble="True" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" IsBounceEnabled="True" ScrollDirectionType="0" DirectionType="Vertical" ctype="ListViewObjectData">
            <Size X="1280.0000" Y="640.0000" />
            <AnchorPoint />
            <Position Y="80.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition Y="0.1111" />
            <PreSize X="1.0000" Y="0.8889" />
            <SingleColor A="255" R="150" G="150" B="255" />
            <FirstColor A="255" R="150" G="150" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>