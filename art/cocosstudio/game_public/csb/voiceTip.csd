<GameFile>
  <PropertyGroup Name="voiceTip" Type="Node" ID="46b41f8e-d674-44d6-91ea-2e6717ccdae4" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="1024" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="voice_img" ActionTag="152492121" Tag="1952" IconVisible="False" LeftMargin="-34.2549" RightMargin="10.2549" TopMargin="-17.3909" BottomMargin="-14.6091" ctype="SpriteObjectData">
            <Size X="24.0000" Y="32.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-22.2549" Y="1.3909" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="game_public/res/ripple3.png" Plist="game_public/res/game.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="voice_duration" ActionTag="-634113121" Tag="1953" IconVisible="False" LeftMargin="-7.2032" RightMargin="-37.7968" TopMargin="-17.6590" BottomMargin="-12.3410" FontSize="30" LabelText="60&quot;" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="45.0000" Y="30.0000" />
            <AnchorPoint />
            <Position X="-7.2032" Y="-12.3410" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>