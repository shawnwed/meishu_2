<GameFile>
  <PropertyGroup Name="task_award" Type="Node" ID="ad9c36f4-3c07-49d5-98fb-d3d00b82e04b" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="2423" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="bg" ActionTag="1227391553" Tag="2424" IconVisible="False" LeftMargin="-220.5000" RightMargin="-220.5000" TopMargin="-221.0000" BottomMargin="-221.0000" ctype="SpriteObjectData">
            <Size X="441.0000" Y="442.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="game_public/res/task_award/bg.png" Plist="game_public/task_award.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="light1" ActionTag="-409093146" Tag="2425" IconVisible="False" LeftMargin="-114.0000" RightMargin="-114.0000" TopMargin="-114.0000" BottomMargin="-114.0000" ctype="SpriteObjectData">
            <Size X="228.0000" Y="228.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="game_public/res/task_award/light.png" Plist="game_public/task_award.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="title" ActionTag="1007851637" Tag="2427" IconVisible="False" LeftMargin="-165.5000" RightMargin="-165.5000" TopMargin="-186.5000" BottomMargin="93.5000" ctype="SpriteObjectData">
            <Size X="331.0000" Y="93.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position Y="140.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="game_public/lan/cn/title.png" Plist="game_public/lan/cn/game.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="light2" ActionTag="-912857647" Tag="2449" IconVisible="False" LeftMargin="-114.0000" RightMargin="-114.0000" TopMargin="-114.0000" BottomMargin="-114.0000" ctype="SpriteObjectData">
            <Size X="228.0000" Y="228.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="game_public/res/task_award/light.png" Plist="game_public/task_award.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>