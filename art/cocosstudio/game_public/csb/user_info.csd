<GameFile>
  <PropertyGroup Name="user_info" Type="Node" ID="8778bd63-37d6-4a37-9735-2efa8fcc598d" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="22" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="mask" ActionTag="-384014981" Tag="64" IconVisible="False" LeftMargin="-640.0000" RightMargin="-640.0000" TopMargin="-359.0000" BottomMargin="-361.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="118" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="1280.0000" Y="720.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position Y="-1.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <SingleColor A="255" R="26" G="26" B="26" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="bg" ActionTag="-994867078" Tag="63" IconVisible="False" LeftMargin="-267.0000" RightMargin="-267.0000" TopMargin="-169.0000" BottomMargin="-169.0000" TouchEnable="True" LeftEage="161" RightEage="161" TopEage="130" BottomEage="130" Scale9OriginX="161" Scale9OriginY="130" Scale9Width="212" Scale9Height="78" ctype="ImageViewObjectData">
            <Size X="534.0000" Y="338.0000" />
            <Children>
              <AbstractNodeData Name="score_txt" ActionTag="1194981903" Tag="27" IconVisible="False" LeftMargin="270.8900" RightMargin="159.1100" TopMargin="93.2641" BottomMargin="211.7359" LabelText="2032" ctype="TextBMFontObjectData">
                <Size X="104.0000" Y="33.0000" />
                <AnchorPoint ScaleY="0.5235" />
                <Position X="270.8900" Y="229.0127" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5073" Y="0.6776" />
                <PreSize X="0.1948" Y="0.0976" />
                <LabelBMFontFile_CNB Type="Normal" Path="hall/res/rank_num.fnt" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="level" ActionTag="-137454787" VisibleForFrame="False" Tag="47" IconVisible="False" LeftMargin="58.5000" RightMargin="429.5000" TopMargin="82.0000" BottomMargin="210.0000" ctype="SpriteObjectData">
                <Size X="46.0000" Y="46.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="58.5000" Y="233.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1096" Y="0.6893" />
                <PreSize X="0.0861" Y="0.1361" />
                <FileData Type="Default" Path="Default/Sprite.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="head" ActionTag="902822702" Tag="48" IconVisible="True" LeftMargin="113.8773" RightMargin="420.1227" TopMargin="107.8760" BottomMargin="230.1240" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint />
                <Position X="113.8773" Y="230.1240" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2133" Y="0.6808" />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="Normal" Path="game_public/csb/head.csd" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="name_txt" ActionTag="-1832915376" Tag="62" IconVisible="False" LeftMargin="265.7651" RightMargin="88.2349" TopMargin="41.2053" BottomMargin="266.7947" FontSize="30" LabelText="玩家昵称一二" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="180.0000" Y="30.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="265.7651" Y="281.7947" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="167" G="69" B="52" />
                <PrePosition X="0.4977" Y="0.8337" />
                <PreSize X="0.3371" Y="0.0888" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="score_icon" ActionTag="121849800" Tag="82" IconVisible="False" LeftMargin="212.7454" RightMargin="283.2546" TopMargin="91.2169" BottomMargin="208.7831" ctype="SpriteObjectData">
                <Size X="38.0000" Y="38.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="231.7454" Y="227.7831" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4340" Y="0.6739" />
                <PreSize X="0.0712" Y="0.1124" />
                <FileData Type="MarkedSubImage" Path="game_public/res/coin_small_icon2.png" Plist="game_public/res/game.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="info_sex" ActionTag="1333403026" Tag="51" IconVisible="False" LeftMargin="211.4078" RightMargin="284.5922" TopMargin="36.0296" BottomMargin="262.9704" ctype="SpriteObjectData">
                <Size X="38.0000" Y="39.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="230.4078" Y="282.4704" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4315" Y="0.8357" />
                <PreSize X="0.0712" Y="0.1154" />
                <FileData Type="MarkedSubImage" Path="game_public/res/info_sex1.png" Plist="game_public/res/game.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="id_txt" ActionTag="1010050738" Tag="52" IconVisible="False" LeftMargin="43.8964" RightMargin="355.1036" TopMargin="201.8519" BottomMargin="106.1481" FontSize="30" LabelText="ID:123456" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="135.0000" Y="30.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="111.3964" Y="121.1481" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="81" G="70" B="69" />
                <PrePosition X="0.2086" Y="0.3584" />
                <PreSize X="0.2528" Y="0.0888" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="copyid" ActionTag="145930586" Tag="57" IconVisible="False" LeftMargin="38.6279" RightMargin="345.3721" TopMargin="246.6079" BottomMargin="40.3921" ctype="SpriteObjectData">
                <Size X="150.0000" Y="51.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="113.6279" Y="65.8921" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2128" Y="0.1949" />
                <PreSize X="0.2809" Y="0.1509" />
                <FileData Type="MarkedSubImage" Path="lan/cn/landlord_btn_copyid.png" Plist="lan/cn/game.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="msg_bg" ActionTag="589895395" Tag="3554" IconVisible="False" LeftMargin="217.5283" RightMargin="36.4717" TopMargin="149.8519" BottomMargin="38.1481" Scale9Enable="True" LeftEage="33" RightEage="33" TopEage="18" BottomEage="18" Scale9OriginX="33" Scale9OriginY="18" Scale9Width="34" Scale9Height="19" ctype="ImageViewObjectData">
                <Size X="280.0000" Y="150.0000" />
                <AnchorPoint />
                <Position X="217.5283" Y="38.1481" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="192" G="159" B="143" />
                <PrePosition X="0.4074" Y="0.1129" />
                <PreSize X="0.5243" Y="0.4438" />
                <FileData Type="MarkedSubImage" Path="game_public/res/chat_input.png" Plist="game_public/res/game.plist" />
              </AbstractNodeData>
              <AbstractNodeData Name="msg_txt" ActionTag="-1649122731" Tag="3567" IconVisible="False" LeftMargin="234.4201" RightMargin="219.5800" TopMargin="170.0210" BottomMargin="67.9790" FontSize="20" LabelText="获奖感言&#xA;111&#xA;11\&#xA;1&#xA;1" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="80.0000" Y="100.0000" />
                <AnchorPoint ScaleY="1.0000" />
                <Position X="234.4201" Y="167.9790" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="81" G="70" B="69" />
                <PrePosition X="0.4390" Y="0.4970" />
                <PreSize X="0.1498" Y="0.2959" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="close" ActionTag="760814985" Tag="27" IconVisible="False" LeftMargin="484.1155" RightMargin="-30.1155" TopMargin="-23.5707" BottomMargin="280.5707" ctype="SpriteObjectData">
                <Size X="80.0000" Y="81.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="524.1155" Y="321.0707" />
                <Scale ScaleX="0.7800" ScaleY="0.7800" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.9815" Y="0.9499" />
                <PreSize X="0.1498" Y="0.2396" />
                <FileData Type="Normal" Path="game_public/res/user_info_close.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="game_public/res/user_info_bg.png" Plist="" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>