<GameFile>
  <PropertyGroup Name="menu" Type="Node" ID="287b6357-61dd-4fda-8fde-be6be04114ed" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="391" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="mask" ActionTag="-2041122508" Tag="3221" IconVisible="False" RightMargin="-200.0000" BottomMargin="-200.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="204" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="200.0000" Y="200.0000" />
            <AnchorPoint ScaleY="1.0000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="bg" ActionTag="1930384059" Tag="3220" IconVisible="False" RightMargin="-284.0000" BottomMargin="-424.0000" TouchEnable="True" Scale9Enable="True" LeftEage="87" RightEage="87" TopEage="117" BottomEage="117" Scale9OriginX="87" Scale9OriginY="117" Scale9Width="110" Scale9Height="190" ctype="ImageViewObjectData">
            <Size X="284.0000" Y="424.0000" />
            <AnchorPoint ScaleY="1.0000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="game_public/res/menu_bg.png" Plist="game_public/res/game.plist" />
          </AbstractNodeData>
          <AbstractNodeData Name="score_btn" ActionTag="-482608823" Tag="3216" IconVisible="False" LeftMargin="4.0000" RightMargin="-274.0000" TopMargin="298.5000" BottomMargin="-383.5000" ctype="SpriteObjectData">
            <Size X="270.0000" Y="85.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="139.0000" Y="-341.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="game_public/lan/cn/menu_score.png" Plist="game_public/lan/cn/game.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="auto_btn" ActionTag="780172477" Tag="3217" IconVisible="False" LeftMargin="8.5000" RightMargin="-278.5000" TopMargin="203.1667" BottomMargin="-288.1667" ctype="SpriteObjectData">
            <Size X="270.0000" Y="85.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="143.5000" Y="-245.6667" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="game_public/lan/cn/menu_auto.png" Plist="game_public/lan/cn/game.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="setting_btn" ActionTag="1731426023" Tag="3218" IconVisible="False" LeftMargin="8.5000" RightMargin="-278.5000" TopMargin="107.8333" BottomMargin="-192.8333" ctype="SpriteObjectData">
            <Size X="270.0000" Y="85.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="143.5000" Y="-150.3333" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="game_public/lan/cn/menu_setting.png" Plist="game_public/lan/cn/game.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="back_btn" ActionTag="-1139592132" Tag="3219" IconVisible="False" LeftMargin="8.5000" RightMargin="-278.5000" TopMargin="12.5000" BottomMargin="-97.5000" ctype="SpriteObjectData">
            <Size X="270.0000" Y="85.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="143.5000" Y="-55.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="game_public/lan/cn/menu_back.png" Plist="game_public/lan/cn/game.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="stand_btn" Visible="False" ActionTag="1536276713" Tag="3233" IconVisible="False" LeftMargin="8.5000" RightMargin="-278.5000" TopMargin="203.1667" BottomMargin="-288.1667" ctype="SpriteObjectData">
            <Size X="270.0000" Y="85.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="143.5000" Y="-245.6667" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="game_public/lan/cn/menu_stand.png" Plist="game_public/lan/cn/game.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="change_btn" Visible="False" ActionTag="-234086555" Tag="3234" IconVisible="False" LeftMargin="5.0000" RightMargin="-275.0000" TopMargin="203.1667" BottomMargin="-288.1667" ctype="SpriteObjectData">
            <Size X="270.0000" Y="85.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="140.0000" Y="-245.6667" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="game_public/lan/cn/menu_change.png" Plist="game_public/lan/cn/game.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="dissolve_btn" Visible="False" ActionTag="-500933972" Tag="1214" IconVisible="False" LeftMargin="4.0000" RightMargin="-274.0000" TopMargin="298.5000" BottomMargin="-383.5000" ctype="SpriteObjectData">
            <Size X="270.0000" Y="85.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="139.0000" Y="-341.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="game_public/lan/cn/menu_dissolve.png" Plist="game_public/lan/cn/game.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>