<GameFile>
  <PropertyGroup Name="hall_top" Type="Layer" ID="e01d31e7-82e5-4020-8032-ea634a251b40" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="25" ctype="GameLayerObjectData">
        <Size X="1280.0000" Y="95.0000" />
        <Children>
          <AbstractNodeData Name="bg" ActionTag="-636079683" Tag="2" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentWidthEnabled="True" LeftMargin="-0.6726" RightMargin="0.6726" TopMargin="-0.0001" BottomMargin="-22.9999" LeftEage="330" RightEage="330" TopEage="29" BottomEage="29" Scale9OriginX="330" Scale9OriginY="29" Scale9Width="620" Scale9Height="60" ctype="ImageViewObjectData">
            <Size X="1280.0000" Y="118.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="1.0000" />
            <Position X="639.3274" Y="95.0001" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4995" Y="1.0000" />
            <PreSize X="1.0000" Y="1.2421" />
            <FileData Type="Normal" Path="game_public/res/bgtop.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="back" ActionTag="-305005430" Tag="3" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" RightMargin="1108.0000" BottomMargin="-3.0000" ctype="SpriteObjectData">
            <Size X="172.0000" Y="98.0000" />
            <AnchorPoint ScaleY="1.0000" />
            <Position Y="95.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition Y="1.0000" />
            <PreSize X="0.1344" Y="1.0316" />
            <FileData Type="MarkedSubImage" Path="game_public/res/back.png" Plist="game_public/res/game.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="title" ActionTag="1194938180" Tag="4" IconVisible="False" LeftMargin="163.5000" RightMargin="954.5000" TopMargin="26.0001" BottomMargin="26.9999" ctype="SpriteObjectData">
            <Size X="162.0000" Y="42.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="163.5000" Y="47.9999" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.1277" Y="0.5053" />
            <PreSize X="0.1266" Y="0.4421" />
            <FileData Type="Normal" Path="lan/cn/title_info.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="coin1" Visible="False" ActionTag="-1633699658" Tag="52" IconVisible="True" PositionPercentXEnabled="True" LeftMargin="989.9520" RightMargin="60.0480" TopMargin="24.0000" BottomMargin="25.0000" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="230.0000" Y="46.0000" />
            <AnchorPoint />
            <Position X="989.9520" Y="25.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.7734" Y="0.2632" />
            <PreSize X="0.1797" Y="0.4842" />
            <FileData Type="Normal" Path="hall/csb/top_coin.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="coin2" ActionTag="1976246523" Tag="58" IconVisible="True" PositionPercentXEnabled="True" LeftMargin="971.0080" RightMargin="78.9919" TopMargin="24.0000" BottomMargin="25.0000" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="230.0000" Y="46.0000" />
            <AnchorPoint />
            <Position X="971.0080" Y="25.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.7586" Y="0.2632" />
            <PreSize X="0.1797" Y="0.4842" />
            <FileData Type="Normal" Path="hall/csb/top_coin.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="coin3" ActionTag="602985686" VisibleForFrame="False" Tag="457" IconVisible="True" PositionPercentXEnabled="True" LeftMargin="989.9520" RightMargin="60.0480" TopMargin="24.0000" BottomMargin="25.0000" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="230.0000" Y="46.0000" />
            <AnchorPoint />
            <Position X="989.9520" Y="25.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.7734" Y="0.2632" />
            <PreSize X="0.1797" Y="0.4842" />
            <FileData Type="Normal" Path="hall/csb/top_coin.csd" Plist="" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>