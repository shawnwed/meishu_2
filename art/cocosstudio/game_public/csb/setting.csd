<GameFile>
  <PropertyGroup Name="setting" Type="Node" ID="196ed50a-410d-4635-a61f-2cce2d74b8bd" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="1291" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="mask" ActionTag="2132154227" Alpha="128" Tag="42" IconVisible="False" LeftMargin="-640.0000" RightMargin="-640.0000" TopMargin="-360.0000" BottomMargin="-360.0000" ClipAble="False" BackColorAlpha="102" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="1280.0000" Y="720.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="0" G="0" B="0" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="bg" ActionTag="449101089" Tag="43" IconVisible="False" LeftMargin="-457.5000" RightMargin="-457.5000" TopMargin="-288.0000" BottomMargin="-288.0000" TouchEnable="True" LeftEage="49" RightEage="49" TopEage="49" BottomEage="49" Scale9OriginX="49" Scale9OriginY="49" Scale9Width="817" Scale9Height="478" ctype="ImageViewObjectData">
            <Size X="915.0000" Y="576.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="game_public/res/setting_bg.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="music_txt" ActionTag="833550069" Tag="1297" IconVisible="False" LeftMargin="-370.3917" RightMargin="298.3917" TopMargin="-96.4839" BottomMargin="60.4839" FontSize="36" LabelText="音樂" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="72.0000" Y="36.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-334.3917" Y="78.4839" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="81" G="70" B="69" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="sound_txt" ActionTag="1343504542" Tag="1298" IconVisible="False" LeftMargin="-370.3917" RightMargin="298.3917" TopMargin="8.5160" BottomMargin="-44.5160" FontSize="36" LabelText="音效" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="72.0000" Y="36.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-334.3917" Y="-26.5160" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="81" G="70" B="69" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="switch_music" ActionTag="-932515446" Tag="1303" IconVisible="False" LeftMargin="-253.8896" RightMargin="46.8896" TopMargin="-113.4847" BottomMargin="43.4847" ctype="SpriteObjectData">
            <Size X="207.0000" Y="70.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-150.3896" Y="78.4847" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="game_public/lan/cn/setting_switch_1.png" Plist="game_public/lan/cn/game.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="switch_sound" ActionTag="-287053153" Tag="1304" IconVisible="False" LeftMargin="-253.8896" RightMargin="46.8896" TopMargin="-8.4846" BottomMargin="-61.5154" ctype="SpriteObjectData">
            <Size X="207.0000" Y="70.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-150.3896" Y="-26.5154" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="game_public/lan/cn/setting_switch_1.png" Plist="game_public/lan/cn/game.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="close_btn" ActionTag="729602407" Tag="1808" IconVisible="False" LeftMargin="396.0289" RightMargin="-476.0289" TopMargin="-297.4554" BottomMargin="216.4554" ctype="SpriteObjectData">
            <Size X="80.0000" Y="81.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="436.0289" Y="256.9554" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="game_public/res/user_info_close.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="shake_txt" ActionTag="683390682" Tag="14" IconVisible="False" LeftMargin="-370.3917" RightMargin="298.3917" TopMargin="113.5160" BottomMargin="-149.5160" FontSize="36" LabelText="震動" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="72.0000" Y="36.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-334.3917" Y="-131.5160" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="81" G="70" B="69" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="switch_shake" ActionTag="-2107454020" Tag="15" IconVisible="False" LeftMargin="-253.8896" RightMargin="46.8896" TopMargin="96.5161" BottomMargin="-166.5161" ctype="SpriteObjectData">
            <Size X="207.0000" Y="70.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-150.3896" Y="-131.5161" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="game_public/lan/cn/setting_switch_1.png" Plist="game_public/lan/cn/game.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="title_line_6" ActionTag="-1772593298" Tag="17" IconVisible="False" LeftMargin="-214.5004" RightMargin="73.5004" TopMargin="-253.9995" BottomMargin="231.9995" ctype="SpriteObjectData">
            <Size X="141.0000" Y="22.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-144.0004" Y="242.9995" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="game_public/res/title_line.png" Plist="game_public/res/game.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="title" ActionTag="1297413440" Tag="15" IconVisible="False" LeftMargin="-47.5004" RightMargin="-47.4996" TopMargin="-265.4995" BottomMargin="218.4995" ctype="SpriteObjectData">
            <Size X="95.0000" Y="47.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-0.0004" Y="241.9995" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="lan/cn/word_setting.png" Plist="lan/cn/game.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="title_line_6_0" ActionTag="-2122807209" Tag="16" IconVisible="False" LeftMargin="73.5000" RightMargin="-214.5000" TopMargin="-253.9995" BottomMargin="231.9995" FlipX="True" ctype="SpriteObjectData">
            <Size X="141.0000" Y="22.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="144.0000" Y="242.9995" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="game_public/res/title_line.png" Plist="game_public/res/game.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="lan2" Visible="False" ActionTag="-1274562252" Tag="19" IconVisible="False" LeftMargin="66.5000" RightMargin="-137.5000" TopMargin="-102.0000" BottomMargin="38.0000" ctype="SpriteObjectData">
            <Size X="71.0000" Y="64.0000" />
            <Children>
              <AbstractNodeData Name="title" ActionTag="-1550729650" Tag="20" IconVisible="False" LeftMargin="98.0000" RightMargin="-99.0000" TopMargin="3.0000" BottomMargin="25.0000" FontSize="36" LabelText="粵語" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="72.0000" Y="36.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="98.0000" Y="43.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="1.3803" Y="0.6719" />
                <PreSize X="1.0141" Y="0.5625" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="102.0000" Y="70.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="game_public/res/setting_check1.png" Plist="game_public/res/game.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="lan1" ActionTag="1772313868" Tag="21" IconVisible="False" LeftMargin="108.4379" RightMargin="-179.4379" TopMargin="-120.3151" BottomMargin="56.3151" ctype="SpriteObjectData">
            <Size X="71.0000" Y="64.0000" />
            <Children>
              <AbstractNodeData Name="title" ActionTag="-1384867284" Tag="22" IconVisible="False" LeftMargin="98.8367" RightMargin="-135.8367" TopMargin="14.8133" BottomMargin="13.1867" FontSize="36" LabelText="普通話" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="108.0000" Y="36.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="98.8367" Y="31.1867" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="81" G="70" B="69" />
                <PrePosition X="1.3921" Y="0.4873" />
                <PreSize X="1.5211" Y="0.5625" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="143.9379" Y="88.3151" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="game_public/res/setting_check1.png" Plist="game_public/res/game.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="lan3" Visible="False" ActionTag="357415588" Tag="23" IconVisible="False" LeftMargin="66.5000" RightMargin="-137.5000" TopMargin="106.0000" BottomMargin="-170.0000" ctype="SpriteObjectData">
            <Size X="71.0000" Y="64.0000" />
            <Children>
              <AbstractNodeData Name="title" ActionTag="-2018271522" Tag="24" IconVisible="False" LeftMargin="98.0000" RightMargin="-135.0000" TopMargin="3.0000" BottomMargin="25.0000" FontSize="36" LabelText="臺灣話" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="108.0000" Y="36.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="98.0000" Y="43.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="1.3803" Y="0.6719" />
                <PreSize X="1.5211" Y="0.5625" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="102.0000" Y="-138.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="game_public/res/setting_check1.png" Plist="game_public/res/game.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="title_line_6_1" ActionTag="-51648554" Tag="30" IconVisible="False" LeftMargin="4.4831" RightMargin="-8.4831" TopMargin="-155.3760" BottomMargin="-221.6240" ctype="SpriteObjectData">
            <Size X="4.0000" Y="377.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="6.4831" Y="-33.1240" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="game_public/res/h_line.png" Plist="game_public/res/game.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>