<GameFile>
  <PropertyGroup Name="audioRecorderNode" Type="Node" ID="5287e1bb-40e6-4c06-9a95-f5e464c16cca" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="535" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="audioRecorderLayer" ActionTag="-897922849" Tag="1018" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-135.0000" RightMargin="-135.0000" TopMargin="-125.0000" BottomMargin="-125.0000" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" LeftEage="89" RightEage="89" TopEage="82" BottomEage="82" Scale9OriginX="89" Scale9OriginY="82" Scale9Width="92" Scale9Height="86" ctype="PanelObjectData">
            <Size X="270.0000" Y="250.0000" />
            <Children>
              <AbstractNodeData Name="audioRecordImg" ActionTag="-672951995" Tag="3429" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="70.4970" RightMargin="124.5030" TopMargin="42.0000" BottomMargin="92.0000" LeftEage="24" RightEage="24" TopEage="38" BottomEage="38" Scale9OriginX="24" Scale9OriginY="38" Scale9Width="27" Scale9Height="40" ctype="ImageViewObjectData">
                <Size X="75.0000" Y="116.0000" />
                <AnchorPoint />
                <Position X="70.4970" Y="92.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2611" Y="0.3680" />
                <PreSize X="0.2778" Y="0.4640" />
                <FileData Type="MarkedSubImage" Path="game_public/res/audioRecordImg.png" Plist="game_public/res/game.plist" />
              </AbstractNodeData>
              <AbstractNodeData Name="audioVolume" ActionTag="2042574969" Tag="5358" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="156.6000" RightMargin="75.4000" TopMargin="70.0000" BottomMargin="92.0000" ctype="SpriteObjectData">
                <Size X="38.0000" Y="88.0000" />
                <AnchorPoint />
                <Position X="156.6000" Y="92.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5800" Y="0.3680" />
                <PreSize X="0.1407" Y="0.3520" />
                <FileData Type="MarkedSubImage" Path="game_public/res/volume5.png" Plist="game_public/res/game.plist" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="audioRecordTip" ActionTag="659328424" Tag="6805" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="24.5000" RightMargin="24.5000" TopMargin="187.0000" BottomMargin="37.0000" FontSize="26" LabelText="手指上滑 取消发送" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="221.0000" Y="26.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="135.0000" Y="50.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.2000" />
                <PreSize X="0.8185" Y="0.1040" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="audio_record_cancel" Visible="False" ActionTag="2105452967" Tag="8734" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="99.0000" RightMargin="99.0000" TopMargin="53.5000" BottomMargin="103.5000" LeftEage="23" RightEage="23" TopEage="30" BottomEage="30" Scale9OriginX="23" Scale9OriginY="30" Scale9Width="26" Scale9Height="33" ctype="ImageViewObjectData">
                <Size X="72.0000" Y="93.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="135.0000" Y="150.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.6000" />
                <PreSize X="0.2667" Y="0.3720" />
                <FileData Type="MarkedSubImage" Path="game_public/res/audio_record_cancel.png" Plist="game_public/res/game.plist" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="game_public/res/voice_bg.png" Plist="" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>