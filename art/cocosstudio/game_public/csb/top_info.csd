<GameFile>
  <PropertyGroup Name="top_info" Type="Node" ID="865d090e-3365-425e-8ef2-ea742d379431" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="486" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="net" ActionTag="1924621613" Tag="487" IconVisible="False" LeftMargin="-89.0005" RightMargin="55.0005" TopMargin="37.0008" BottomMargin="-61.0008" ctype="SpriteObjectData">
            <Size X="34.0000" Y="24.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-72.0005" Y="-49.0008" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="game_public/res/net_wifi_1.png" Plist="game_public/res/game.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="battery_track" ActionTag="591407501" Tag="489" IconVisible="False" LeftMargin="-48.5005" RightMargin="4.5005" TopMargin="39.0005" BottomMargin="-59.0005" ctype="SpriteObjectData">
            <Size X="44.0000" Y="20.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="-48.5005" Y="-49.0005" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="game_public/res/battery_track.png" Plist="game_public/res/game.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="bettary" ActionTag="-483353384" Tag="488" IconVisible="False" LeftMargin="-46.5005" RightMargin="10.5005" TopMargin="41.5006" BottomMargin="-56.5006" ProgressInfo="100" ctype="LoadingBarObjectData">
            <Size X="36.0000" Y="15.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="-46.5005" Y="-49.0006" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <ImageFileData Type="MarkedSubImage" Path="game_public/res/bettery_fill.png" Plist="game_public/res/game.plist" />
          </AbstractNodeData>
          <AbstractNodeData Name="time_txt" ActionTag="1399264745" Tag="490" IconVisible="False" LeftMargin="-80.6790" RightMargin="19.6790" TopMargin="3.0789" BottomMargin="-31.0789" FontSize="24" LabelText="00:00" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="61.0000" Y="28.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-50.1790" Y="-17.0789" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="198" G="167" B="123" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>