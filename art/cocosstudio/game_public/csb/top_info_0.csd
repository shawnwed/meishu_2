<GameFile>
  <PropertyGroup Name="top_info_0" Type="Node" ID="17310905-714d-438d-b56f-8d13960dba37" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="486" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="net" ActionTag="1924621613" Tag="487" IconVisible="False" LeftMargin="-123.9886" RightMargin="93.9886" TopMargin="3.4952" BottomMargin="-26.4952" ctype="SpriteObjectData">
            <Size X="30.0000" Y="23.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-108.9886" Y="-14.9952" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="game_public/res/net_wifi_1.png" Plist="game_public/res/game.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="battery_track" ActionTag="591407501" Tag="489" IconVisible="False" LeftMargin="-9.5007" RightMargin="-34.4993" TopMargin="3.9990" BottomMargin="-23.9990" ctype="SpriteObjectData">
            <Size X="44.0000" Y="20.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="-9.5007" Y="-13.9990" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="MarkedSubImage" Path="game_public/res/battery_track.png" Plist="game_public/res/game.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="bettary" ActionTag="-483353384" Tag="488" IconVisible="False" LeftMargin="-7.5006" RightMargin="-28.4994" TopMargin="6.4989" BottomMargin="-21.4989" ProgressInfo="100" ctype="LoadingBarObjectData">
            <Size X="36.0000" Y="15.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="-7.5006" Y="-13.9989" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <ImageFileData Type="MarkedSubImage" Path="game_public/res/bettery_fill.png" Plist="game_public/res/game.plist" />
          </AbstractNodeData>
          <AbstractNodeData Name="time_txt" ActionTag="1399264745" Tag="490" IconVisible="False" LeftMargin="-78.1714" RightMargin="22.1714" TopMargin="1.0747" BottomMargin="-27.0747" FontSize="22" LabelText="00:00" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="56.0000" Y="26.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-50.1714" Y="-14.0747" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="223" G="223" B="223" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>